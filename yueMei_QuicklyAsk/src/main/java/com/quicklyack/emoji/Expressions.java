package com.quicklyack.emoji;

import com.quicklyask.activity.R;

public class Expressions {

	/**
	 * 表情图片1
	 */
	public static int[] expressionImgs = new int[] { R.drawable.f000,
			R.drawable.f001, R.drawable.f002, R.drawable.f003, R.drawable.f004,
			R.drawable.f005, R.drawable.f006, R.drawable.f007, R.drawable.f008,
			R.drawable.f009, R.drawable.f010, R.drawable.f011, R.drawable.f012,
			R.drawable.f013, R.drawable.f014, R.drawable.f015, R.drawable.f016,
			R.drawable.f017, R.drawable.f018, R.drawable.f019, R.drawable.f020,
			R.drawable.f021, R.drawable.f022, R.drawable.f023, R.drawable.f024,
			R.drawable.f025, R.drawable.f026, R.drawable.f0000 };

	/**
	 * 本地表情1的替换的图片名
	 */
	public static String[] expressionImgNames = new String[] { "[[微笑]]",
			"[[羞涩]]", "[[惊呆]]", "[[可爱]]", "[[大爱]]", "[[龇牙]]", "[[大笑]]",
			"[[坏笑]]", "[[腼腆]]", "[[抓狂]]", "[[黑脸]]", "[[得意]]", "[[亲亲]]",
			"[[鼓掌]]", "[[挖鼻]]", "[[忿忿]]", "[[泪]]", "[[笑]]", "[[飞吻]]",
			"[[汪星人]]", "[[喵星人]]", "[[汗]]", "[[疑惑]]", "[[思考]]", "[[俏皮]]",
			"[[财迷]]", "[[可怜]]", "[[删除]]" };

	/**
	 * 表情图片2
	 */
	public static int[] expressionImgs1 = new int[] { R.drawable.f027,
			R.drawable.f028, R.drawable.f029, R.drawable.f030, R.drawable.f031,
			R.drawable.f032, R.drawable.f033, R.drawable.f034, R.drawable.f035,
			R.drawable.f036, R.drawable.f037, R.drawable.f038, R.drawable.f039,
			R.drawable.f040, R.drawable.f041, R.drawable.f042, R.drawable.f044,
			R.drawable.f045, R.drawable.f046, R.drawable.f047, R.drawable.f048,
			R.drawable.f049, R.drawable.f050, R.drawable.f051, R.drawable.f052,
			R.drawable.f053, R.drawable.f054, R.drawable.f0000 };

	/**
	 * 本地表情2的替换的图片名
	 */
	public static String[] expressionImgNames1 = new String[] { "[[馋]]",
			"[[泪汪汪]]", "[[黑线]]", "[[烦躁]]", "[[鄙视]]", "[[楞神]]", "[[再见]]",
			"[[晕]]", "[[哈欠]]", "[[礼物]]", "[[春风]]", "[[雨天]]", "[[灰机]]",
			"[[鲜花]]", "[[太阳]]", "[[月亮]]", "[[音符]]", "[[爱心]]", "[[心碎]]",
			"[[棒棒哒]]", "[[胜利]]", "[[握手]]", "[[拱手]]", "[[差劲]]", "[[没问题]]",
			"[[蛋糕]]", "[[蜡烛]]", "[[删除]]" };

	public static int [] expressionImgs2 = new int [] {
			R.drawable.f055, R.drawable.f056, R.drawable.f057,
			R.drawable.f058, R.drawable.f059, R.drawable.f060,
			R.drawable.f061, R.drawable.f062, R.drawable.f063,
			R.drawable.f064, R.drawable.f065, R.drawable.f066,
			R.drawable.f067, R.drawable.f068, R.drawable.f069
	};
	public static String[] expressionImgNames2= new String []{
			"[[菜刀]]","[[红唇]]","[[口红]]",
			"[[礼盒]]","[[闪电]]", "[[闪闪]]",
			"[[闪心]]","[[水滴]]","[[五星]]",
			"[[向日葵]]","[[心]]","[[星环]]",
			"[[星星]]","[[叶子]]","[[钻戒]]"
	};

}
