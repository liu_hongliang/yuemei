package com.cn.demo.pinyin;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;

public class AssortView2 extends Button {

	public interface OnTouchAssortListener {
		void onTouchAssortListener(String s);

		void onTouchAssortUP();
	}

	public AssortView2(Context context) {
		super(context);
	}

	public AssortView2(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AssortView2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	// 分类
	private String[] assort = { "定位","热门","周边","A", "B", "C", "D", "E", "F", "G", "H", "I",
			"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
			"W", "X", "Y", "Z" };

	private Paint paint = new Paint();
	// 选择的索引
	private int selectIndex = -1;
	// 字母监听器
	private AssortView.OnTouchAssortListener onTouch;

	public void setOnTouchAssortListener(AssortView.OnTouchAssortListener onTouch) {
		this.onTouch = onTouch;
	}


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int height = getHeight();
		int width = getWidth();
		int interval = height / assort.length;

		for (int i = 0, length = assort.length; i < length; i++) {
			// 抗锯齿
			paint.setAntiAlias(true);
			// 默认粗体
			// paint.setTypeface(Typeface.DEFAULT_BOLD);
			paint.setTextSize(30);
			// 白色
			paint.setColor(Color.parseColor("#FF527F"));
			if (i == selectIndex) {
				// 被选择的字母改变颜色和粗体
				// paint.setColor(Color.parseColor("#FF5072"));
				paint.setColor(Color.parseColor("#FF527F"));
				paint.setFakeBoldText(true);
				paint.setTextSize(34);
			}
			// 计算字母的X坐标
			float xPos = width / 2 - paint.measureText(assort[i]) / 2;
			// 计算字母的Y坐标
			float yPos = interval * i + interval;
			canvas.drawText(assort[i], xPos, yPos, paint);
			paint.reset();
		}

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		float y = event.getY();
		int index = (int) (y / getHeight() * assort.length);
		if (index >= 0 && index < assort.length) {

			switch (event.getAction()) {
				case MotionEvent.ACTION_MOVE:
					// 如果滑动改变
					if (selectIndex != index) {
						selectIndex = index;
						if (onTouch != null) {
							onTouch.onTouchAssortListener(assort[selectIndex]);
						}

					}
					break;
				case MotionEvent.ACTION_DOWN:
					selectIndex = index;
					if (onTouch != null) {
						onTouch.onTouchAssortListener(assort[selectIndex]);
					}

					break;
				case MotionEvent.ACTION_UP:
					if (onTouch != null) {
						onTouch.onTouchAssortUP();
					}
					selectIndex = -1;
					break;
			}
		} else {
			selectIndex = -1;
			if (onTouch != null) {
				onTouch.onTouchAssortUP();
			}
		}
		invalidate();

		return true;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}
}
