package com.quicklyask.adpter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.Coupons;
import com.module.home.model.bean.NewZtCouponsBean;
import com.quicklyask.activity.R;

import java.util.List;

public class NewuserConponsAdapter extends BaseQuickAdapter<NewZtCouponsBean, BaseViewHolder> {
    public NewuserConponsAdapter(int layoutResId, @Nullable List<NewZtCouponsBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewZtCouponsBean item) {
        Glide.with(mContext)
                .load(item.getImg())
                .into((ImageView) helper.getView(R.id.newuser_conpons_project));
        Coupons coupons = item.getCoupons().get(0);
        helper.setText(R.id.conpons_price1,coupons.getMoney())
                .setText(R.id.conpons_limit1,"满"+coupons.getLimit_money()+"使用");

        Coupons coupons2 = item.getCoupons().get(1);
        helper.setText(R.id.conpons_price2,coupons2.getMoney())
                .setText(R.id.conpons_limit2,"满"+coupons2.getLimit_money()+"使用");

        Coupons coupons3 = item.getCoupons().get(2);
        helper.setText(R.id.conpons_price3,coupons3.getMoney())
                .setText(R.id.conpons_limit3,"满"+coupons3.getLimit_money()+"使用");
    }
}
