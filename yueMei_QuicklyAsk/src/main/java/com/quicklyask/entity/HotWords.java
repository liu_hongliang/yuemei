package com.quicklyask.entity;

import com.module.home.model.bean.HotWordsData;

import java.util.List;

public class HotWords {
	private String code;
	private String message;
	private List<HotWordsData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<HotWordsData> getData() {
		return data;
	}

	public void setData(List<HotWordsData> data) {
		this.data = data;
	}
}
