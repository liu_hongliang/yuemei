package com.quicklyask.entity;

/**
 * 视频状态
 * 开始播放，重新播放，加载中，网络中断，视频源错误，非wifi下播放
 * Created by 裴成浩 on 2019/5/7
 */
public enum VideoViewState {
    START_PLAY, TO_PLAY, IN_LOAD, NETWORK_ERROR, VIDEO_ERROR, NOT_WIFI
}
