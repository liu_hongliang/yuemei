package com.quicklyask.entity;

import java.util.HashMap;

public class UserBrowseTao {
    private String id;  //淘整形ID
    private String img;  //淘整形图片
    private String price_discount;  //展示价格
    private String is_show_member;  //	是否展示会员价 -1:不展示
    private String member_price;  //	会员价
    private String title;  //	淘整形标题
    private HashMap<String,String> event_params;  //	埋点数据

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getIs_show_member() {
        return is_show_member;
    }

    public void setIs_show_member(String is_show_member) {
        this.is_show_member = is_show_member;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
