package com.quicklyask.entity;


import com.module.doctor.model.bean.DocListData;
import com.module.taodetail.model.bean.LocationData;

import java.util.List;

public class SearchResultData4 {
	private List<DocListData> list;
	private LocationData city_location;

	public LocationData getCity_location() {
		return city_location;
	}

	public void setCity_location(LocationData city_location) {
		this.city_location = city_location;
	}

	public List<DocListData> getList() {
		return list;
	}

	public void setList(List<DocListData> list) {
		this.list = list;
	}

}
