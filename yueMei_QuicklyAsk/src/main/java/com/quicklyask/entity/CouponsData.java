package com.quicklyask.entity;

import java.util.List;

public class CouponsData {
    private List<String> desc;
    private List<Integer> money;

    public List<String> getDesc() {
        return desc;
    }

    public void setDesc(List<String> desc) {
        this.desc = desc;
    }

    public List<Integer> getMoney() {
        return money;
    }

    public void setMoney(List<Integer> money) {
        this.money = money;
    }
}
