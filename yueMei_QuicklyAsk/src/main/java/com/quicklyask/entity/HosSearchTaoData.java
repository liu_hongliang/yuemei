package com.quicklyask.entity;

import com.module.taodetail.model.bean.HomeTaoData;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/6/13
 */
public class HosSearchTaoData {
    private String total;
    private String type;
    private List<HomeTaoData> list;
    private List<HomeTaoData> data;


    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<HomeTaoData> getList() {
        return list;
    }

    public void setList(List<HomeTaoData> list) {
        this.list = list;
    }

    public List<HomeTaoData> getData() {
        return data;
    }

    public void setData(List<HomeTaoData> data) {
        this.data = data;
    }
}
