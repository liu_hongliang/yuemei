/**
 * 
 */
package com.quicklyask.entity;

import java.util.List;

/**
 * 
 * 写日记关联数据
 * 
 * @author Robin
 * 
 */
public class TaoLink {

	private String code;
	private String message;

	private List<TaoLinkData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<TaoLinkData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<TaoLinkData> data) {
		this.data = data;
	}

}
