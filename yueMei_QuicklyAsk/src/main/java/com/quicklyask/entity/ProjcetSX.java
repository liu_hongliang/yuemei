package com.quicklyask.entity;


import com.module.my.model.bean.ProjcetData;

/**
 * Created by dwb on 16/5/10.
 */
public class ProjcetSX {

    private String code;
    private String message;
    private ProjcetData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProjcetData getData() {
        return data;
    }

    public void setData(ProjcetData data) {
        this.data = data;
    }
}
