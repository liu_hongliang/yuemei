/**
 *
 */
package com.quicklyask.entity;


import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.Pic;
import com.module.commonview.module.bean.TaoShareData;

import java.util.List;

/**
 * 淘整形详情页
 *
 * @author pch
 */
public class TaoDetailData639 implements Parcelable {

    private List<Pic> pic;
    private String _id;
    private String title;
    private String price_discount;//悦美价格（大促时为价格）
    private String price;           //医院价格
    private String zekou;
    private String time_str;
    private String nowTime;
    private String feeScale;
    private String injection;
    private String city;
    private String iteminfo;
    private String cateid;
    private String order_num;
    private String status;
    private String doc_name;
    private String doc_user_id;
    private String doc_title;
    private String doc_img;
    private String hospital_name;
    private String hospital_id;
    private String distance;
    private String subhead;
    private String use_time;
    private String is_order;
    private String dingjin;
    private String hos_price;
    private String discountPrice;
    private String contain_cost;
    private String not_contain_cost;
    private String post_num;
    private String fee_explain;

    private String sku_type;// 淘整形类型

    private String refund;// 不可退款设置

    private String is_rongyun;
    private String hos_userid;
    private String phone;

    private List<Rel_Tao> rel_tao;// sku 组合功能 规格

    private String rel_title;

    private String dacu66_id;//为1时是大促销，
    private String dacu66_img; //大促销图
    private String dacu66_title; //大促销title
    private String dacu66_url; //大促销title

    private String start_number;//起购数量
    private String number;//限购数量

    private String is_group;//是否是拼团

    private TaoMemberData member_data;

    private TaoPayPrice pay_price;

    protected TaoDetailData639(Parcel in) {
        _id = in.readString();
        title = in.readString();
        price_discount = in.readString();
        price = in.readString();
        zekou = in.readString();
        time_str = in.readString();
        nowTime = in.readString();
        feeScale = in.readString();
        injection = in.readString();
        city = in.readString();
        iteminfo = in.readString();
        cateid = in.readString();
        order_num = in.readString();
        status = in.readString();
        doc_name = in.readString();
        doc_user_id = in.readString();
        doc_title = in.readString();
        doc_img = in.readString();
        hospital_name = in.readString();
        hospital_id = in.readString();
        distance = in.readString();
        subhead = in.readString();
        use_time = in.readString();
        is_order = in.readString();
        dingjin = in.readString();
        hos_price = in.readString();
        discountPrice = in.readString();
        contain_cost = in.readString();
        not_contain_cost = in.readString();
        post_num = in.readString();
        fee_explain = in.readString();
        sku_type = in.readString();
        refund = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        phone = in.readString();
        rel_title = in.readString();
        dacu66_id = in.readString();
        dacu66_img = in.readString();
        dacu66_title = in.readString();
        dacu66_url = in.readString();
        start_number = in.readString();
        number = in.readString();
        is_group = in.readString();
        member_data = in.readParcelable(TaoMemberData.class.getClassLoader());
        pay_price = in.readParcelable(TaoPayPrice.class.getClassLoader());
        is_seg = in.readString();
        group_dingjin = in.readString();
        group_price = in.readString();
        group_number = in.readString();
        group_is_order = in.readString();
        group = in.createTypedArrayList(GroupBean.CREATOR);
        limit_str = in.readString();
        warm_tips = in.readString();
        endTime = in.readString();
        hospital_address = in.readString();
        kind = in.readString();
        hos_img = in.readString();
        specialPrice = in.readString();
        is_bao = in.readString();
        baoxian = in.readString();
        bao_price = in.readString();
        repayment = in.readString();
        use_time2 = in.readString();
        alert_title = in.readString();
        alert_subtitle = in.readString();
        alert_button = in.readString();
        show_hospital = in.readString();
        new_lijian = in.readString();
        is_fanxian = in.readString();
        share = in.readParcelable(TaoShareData.class.getClassLoader());
        new_lijian_alert = in.readString();
        postStr = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(title);
        dest.writeString(price_discount);
        dest.writeString(price);
        dest.writeString(zekou);
        dest.writeString(time_str);
        dest.writeString(nowTime);
        dest.writeString(feeScale);
        dest.writeString(injection);
        dest.writeString(city);
        dest.writeString(iteminfo);
        dest.writeString(cateid);
        dest.writeString(order_num);
        dest.writeString(status);
        dest.writeString(doc_name);
        dest.writeString(doc_user_id);
        dest.writeString(doc_title);
        dest.writeString(doc_img);
        dest.writeString(hospital_name);
        dest.writeString(hospital_id);
        dest.writeString(distance);
        dest.writeString(subhead);
        dest.writeString(use_time);
        dest.writeString(is_order);
        dest.writeString(dingjin);
        dest.writeString(hos_price);
        dest.writeString(discountPrice);
        dest.writeString(contain_cost);
        dest.writeString(not_contain_cost);
        dest.writeString(post_num);
        dest.writeString(fee_explain);
        dest.writeString(sku_type);
        dest.writeString(refund);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
        dest.writeString(phone);
        dest.writeString(rel_title);
        dest.writeString(dacu66_id);
        dest.writeString(dacu66_img);
        dest.writeString(dacu66_title);
        dest.writeString(dacu66_url);
        dest.writeString(start_number);
        dest.writeString(number);
        dest.writeString(is_group);
        dest.writeParcelable(member_data, flags);
        dest.writeParcelable(pay_price, flags);
        dest.writeString(is_seg);
        dest.writeString(group_dingjin);
        dest.writeString(group_price);
        dest.writeString(group_number);
        dest.writeString(group_is_order);
        dest.writeTypedList(group);
        dest.writeString(limit_str);
        dest.writeString(warm_tips);
        dest.writeString(endTime);
        dest.writeString(hospital_address);
        dest.writeString(kind);
        dest.writeString(hos_img);
        dest.writeString(specialPrice);
        dest.writeString(is_bao);
        dest.writeString(baoxian);
        dest.writeString(bao_price);
        dest.writeString(repayment);
        dest.writeString(use_time2);
        dest.writeString(alert_title);
        dest.writeString(alert_subtitle);
        dest.writeString(alert_button);
        dest.writeString(show_hospital);
        dest.writeString(new_lijian);
        dest.writeString(is_fanxian);
        dest.writeParcelable(share, flags);
        dest.writeString(new_lijian_alert);
        dest.writeString(postStr);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaoDetailData639> CREATOR = new Creator<TaoDetailData639>() {
        @Override
        public TaoDetailData639 createFromParcel(Parcel in) {
            return new TaoDetailData639(in);
        }

        @Override
        public TaoDetailData639[] newArray(int size) {
            return new TaoDetailData639[size];
        }
    };

    public String getIs_seg() {
        return is_seg;
    }

    public void setIs_seg(String is_seg) {
        this.is_seg = is_seg;
    }

    private String is_seg;//是否是秒杀
    private String group_dingjin;    //拼团定金
    private String group_price;    //拼团全款
    private String group_number;    //起拼人数
    private String group_is_order;    //拼团支持 全款或订金(2.全款和订金；3.订金；4.全款)

    private List<GroupBean> group;//拼图团列表


    private String limit_str;
    private String warm_tips;

    private String endTime;
    private String hospital_address;
    private String kind;
    private String hos_img;
    private String specialPrice;

    private String is_bao;
    private String baoxian;
    private String bao_price;

    private String repayment;
    private String use_time2;

    private String alert_title;
    private String alert_subtitle;
    private String alert_button;
    private String show_hospital;

    private String new_lijian;

    private List<Hos_Red_Packet> hos_red_packet;

    private List<Alert> alert;
    private TaoAskData tao_ask;
    private String is_fanxian;
    private TaoShareData share;
    private String new_lijian_alert;
    private String postStr;


    public List<Pic> getPic() {
        return pic;
    }

    public void setPic(List<Pic> pic) {
        this.pic = pic;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getZekou() {
        return zekou;
    }

    public void setZekou(String zekou) {
        this.zekou = zekou;
    }

    public String getTime_str() {
        return time_str;
    }

    public void setTime_str(String time_str) {
        this.time_str = time_str;
    }

    public String getNowTime() {
        return nowTime;
    }

    public void setNowTime(String nowTime) {
        this.nowTime = nowTime;
    }

    public String getFeeScale() {
        return feeScale;
    }

    public void setFeeScale(String feeScale) {
        this.feeScale = feeScale;
    }

    public String getInjection() {
        return injection;
    }

    public void setInjection(String injection) {
        this.injection = injection;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getIteminfo() {
        return iteminfo;
    }

    public void setIteminfo(String iteminfo) {
        this.iteminfo = iteminfo;
    }

    public String getCateid() {
        return cateid;
    }

    public void setCateid(String cateid) {
        this.cateid = cateid;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_user_id() {
        return doc_user_id;
    }

    public void setDoc_user_id(String doc_user_id) {
        this.doc_user_id = doc_user_id;
    }

    public String getDoc_title() {
        return doc_title;
    }

    public void setDoc_title(String doc_title) {
        this.doc_title = doc_title;
    }

    public String getDoc_img() {
        return doc_img;
    }

    public void setDoc_img(String doc_img) {
        this.doc_img = doc_img;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getSubhead() {
        return subhead;
    }

    public void setSubhead(String subhead) {
        this.subhead = subhead;
    }

    public String getUse_time() {
        return use_time;
    }

    public void setUse_time(String use_time) {
        this.use_time = use_time;
    }

    public String getIs_order() {
        return is_order;
    }

    public void setIs_order(String is_order) {
        this.is_order = is_order;
    }

    public String getDingjin() {
        return dingjin;
    }

    public void setDingjin(String dingjin) {
        this.dingjin = dingjin;
    }

    public String getHos_price() {
        return hos_price;
    }

    public void setHos_price(String hos_price) {
        this.hos_price = hos_price;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getContain_cost() {
        return contain_cost;
    }

    public void setContain_cost(String contain_cost) {
        this.contain_cost = contain_cost;
    }

    public String getNot_contain_cost() {
        return not_contain_cost;
    }

    public void setNot_contain_cost(String not_contain_cost) {
        this.not_contain_cost = not_contain_cost;
    }

    public String getPost_num() {
        return post_num;
    }

    public void setPost_num(String post_num) {
        this.post_num = post_num;
    }

    public String getFee_explain() {
        return fee_explain;
    }

    public void setFee_explain(String fee_explain) {
        this.fee_explain = fee_explain;
    }

    public String getSku_type() {
        return sku_type;
    }

    public void setSku_type(String sku_type) {
        this.sku_type = sku_type;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Rel_Tao> getRel_tao() {
        return rel_tao;
    }

    public void setRel_tao(List<Rel_Tao> rel_tao) {
        this.rel_tao = rel_tao;
    }

    public String getRel_title() {
        return rel_title;
    }

    public void setRel_title(String rel_title) {
        this.rel_title = rel_title;
    }

    public String getDacu66_id() {
        return dacu66_id;
    }

    public void setDacu66_id(String dacu66_id) {
        this.dacu66_id = dacu66_id;
    }

    public String getDacu66_img() {
        return dacu66_img;
    }

    public void setDacu66_img(String dacu66_img) {
        this.dacu66_img = dacu66_img;
    }

    public String getDacu66_title() {
        return dacu66_title;
    }

    public void setDacu66_title(String dacu66_title) {
        this.dacu66_title = dacu66_title;
    }

    public String getDacu66_url() {
        return dacu66_url;
    }

    public void setDacu66_url(String dacu66_url) {
        this.dacu66_url = dacu66_url;
    }

    public String getStart_number() {
        return start_number;
    }

    public void setStart_number(String start_number) {
        this.start_number = start_number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLimit_str() {
        return limit_str;
    }

    public void setLimit_str(String limit_str) {
        this.limit_str = limit_str;
    }

    public String getWarm_tips() {
        return warm_tips;
    }

    public void setWarm_tips(String warm_tips) {
        this.warm_tips = warm_tips;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHospital_address() {
        return hospital_address;
    }

    public void setHospital_address(String hospital_address) {
        this.hospital_address = hospital_address;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getHos_img() {
        return hos_img;
    }

    public void setHos_img(String hos_img) {
        this.hos_img = hos_img;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(String baoxian) {
        this.baoxian = baoxian;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getUse_time2() {
        return use_time2;
    }

    public void setUse_time2(String use_time2) {
        this.use_time2 = use_time2;
    }

    public String getAlert_title() {
        return alert_title;
    }

    public void setAlert_title(String alert_title) {
        this.alert_title = alert_title;
    }

    public String getAlert_subtitle() {
        return alert_subtitle;
    }

    public void setAlert_subtitle(String alert_subtitle) {
        this.alert_subtitle = alert_subtitle;
    }

    public String getAlert_button() {
        return alert_button;
    }

    public void setAlert_button(String alert_button) {
        this.alert_button = alert_button;
    }

    public String getShow_hospital() {
        return show_hospital;
    }

    public void setShow_hospital(String show_hospital) {
        this.show_hospital = show_hospital;
    }

    public String getNew_lijian() {
        return new_lijian;
    }

    public void setNew_lijian(String new_lijian) {
        this.new_lijian = new_lijian;
    }

    public List<Hos_Red_Packet> getHos_red_packet() {
        return hos_red_packet;
    }

    public void setHos_red_packet(List<Hos_Red_Packet> hos_red_packet) {
        this.hos_red_packet = hos_red_packet;
    }

    public List<Alert> getAlert() {
        return alert;
    }

    public void setAlert(List<Alert> alert) {
        this.alert = alert;
    }

    public class Hos_Red_Packet {
        private String id;
        private String money;
        private String lowest_consumption;
        private String coupons_status;
        private String is_get;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLowest_consumption() {
            return lowest_consumption;
        }

        public void setLowest_consumption(String lowest_consumption) {
            this.lowest_consumption = lowest_consumption;
        }

        public String getCoupons_status() {
            return coupons_status;
        }

        public void setCoupons_status(String coupons_status) {
            this.coupons_status = coupons_status;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getIs_get() {
            return is_get;
        }

        public void setIs_get(String is_get) {
            this.is_get = is_get;
        }
    }

    public class Alert {
        private String img;
        private String title;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public TaoAskData getTao_ask() {
        return tao_ask;
    }

    public void setTao_ask(TaoAskData tao_ask) {
        this.tao_ask = tao_ask;
    }

    public String getIs_group() {
        return is_group;
    }

    public void setIs_group(String is_group) {
        this.is_group = is_group;
    }

    public List<GroupBean> getGroup() {
        return group;
    }

    public void setGroup(List<GroupBean> group) {
        this.group = group;
    }

    public String getGroup_dingjin() {
        return group_dingjin;
    }

    public void setGroup_dingjin(String group_dingjin) {
        this.group_dingjin = group_dingjin;
    }

    public String getGroup_is_order() {
        return group_is_order;
    }

    public void setGroup_is_order(String group_is_order) {
        this.group_is_order = group_is_order;
    }

    public String getGroup_number() {
        return group_number;
    }

    public void setGroup_number(String group_number) {
        this.group_number = group_number;
    }

    public String getGroup_price() {
        return group_price;
    }

    public void setGroup_price(String group_price) {
        this.group_price = group_price;
    }

    public String getIs_fanxian() {
        return is_fanxian;
    }

    public void setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
    }

    public String getIs_bao() {
        return is_bao;
    }

    public void setIs_bao(String is_bao) {
        this.is_bao = is_bao;
    }

    public String getBao_price() {
        return bao_price;
    }

    public void setBao_price(String bao_price) {
        this.bao_price = bao_price;
    }

    public TaoShareData getShare() {
        return share;
    }

    public void setShare(TaoShareData share) {
        this.share = share;
    }

    public String getNew_lijian_alert() {
        return new_lijian_alert;
    }

    public void setNew_lijian_alert(String new_lijian_alert) {
        this.new_lijian_alert = new_lijian_alert;
    }

    public String getPostStr() {
        return postStr;
    }

    public void setPostStr(String postStr) {
        this.postStr = postStr;
    }

    public TaoMemberData getMember_data() {
        return member_data;
    }

    public void setMember_data(TaoMemberData member_data) {
        this.member_data = member_data;
    }

    public TaoPayPrice getPay_price() {
        return pay_price;
    }

    public void setPay_price(TaoPayPrice pay_price) {
        this.pay_price = pay_price;
    }
}
