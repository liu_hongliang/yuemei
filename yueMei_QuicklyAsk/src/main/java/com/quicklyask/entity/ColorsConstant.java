package com.quicklyask.entity;/**
 * Created by cpoopc on 2015/7/31.
 */


import com.quicklyask.activity.R;

/**
 * User: cpoopc
 * Date: 2015-07-31
 * Time: 16:23
 * Ver.: 0.1
 */
public class ColorsConstant {
    public final static int[] colors = new int[]{R.color._00,R.color._33,R.color._bb,R.color._c7};
}
