package com.quicklyask.entity;

import com.module.community.model.bean.BBsListData550;

import java.util.List;

public class SearchResultData23 {

	private List<BBsListData550> list;

	public List<BBsListData550> getList() {
		return list;
	}

	public void setList(List<BBsListData550> list) {
		this.list = list;
	}

}
