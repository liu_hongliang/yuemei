package com.quicklyask.entity;

public class RepaymentBean {

    /**
     * title : 花呗
     * desc : 支持花呗分期
     * replace :
     */

    private String title;
    private String desc;
    private String replace;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getReplace() {
        return replace;
    }

    public void setReplace(String replace) {
        this.replace = replace;
    }
}
