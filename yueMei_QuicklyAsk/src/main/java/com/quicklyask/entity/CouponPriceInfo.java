package com.quicklyask.entity;

public class CouponPriceInfo {
    private String couponPrice;//券后价
    private int coupon_type;//>0 显示券后价
    private String promotion_type;//0普通券后价 1大促券后价 2秒杀券后价 3拼团券后价
    private String coupon_explain;//说明
    private Coupons coupons;

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public int getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(int coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getPromotion_type() {
        return promotion_type;
    }

    public void setPromotion_type(String promotion_type) {
        this.promotion_type = promotion_type;
    }

    public String getCoupon_explain() {
        return coupon_explain;
    }

    public void setCoupon_explain(String coupon_explain) {
        this.coupon_explain = coupon_explain;
    }

    public Coupons getCoupons() {
        return coupons;
    }

    public void setCoupons(Coupons coupons) {
        this.coupons = coupons;
    }
}
