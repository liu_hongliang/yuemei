package com.quicklyask.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class EventParamsBean implements Parcelable {
    /**
     * event_name : cold_start_budget
     * type : 0
     * event_pos : 1
     */

    private String event_name;
    private int type;
    private int event_pos;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(int event_pos) {
        this.event_pos = event_pos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.event_name);
        dest.writeInt(this.type);
        dest.writeInt(this.event_pos);
    }

    public EventParamsBean() {
    }

    protected EventParamsBean(Parcel in) {
        this.event_name = in.readString();
        this.type = in.readInt();
        this.event_pos = in.readInt();
    }

    public static final Parcelable.Creator<EventParamsBean> CREATOR = new Parcelable.Creator<EventParamsBean>() {
        @Override
        public EventParamsBean createFromParcel(Parcel source) {
            return new EventParamsBean(source);
        }

        @Override
        public EventParamsBean[] newArray(int size) {
            return new EventParamsBean[size];
        }
    };
}
