package com.quicklyask.entity;

import com.module.taodetail.model.bean.Promotion;

import java.util.List;

/**
 * 文 件 名: TaoPKBean
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-10 21:06
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class TaoPKBean {

    /**
     * tao_list : [{"sale_type":0,"appmurl":"https://m.yuemei.com/tao/115081/","coupon_type":0,"is_show_member":"1","pay_dingjin":"1760","number":"1","start_number":"1","pay_price_discount":"8800","member_price":"8360","m_list_logo":"","bmsid":"0","seckilling":"0","img":"https://p24.yuemei.com/tao/2019/1130/200_200/jt191130153145_1bff11.jpg","img360":"https://p24.yuemei.com/tao/360_360/2019/1130/jt191130153145_1bff11.jpg","title":"玻尿酸","subtitle":"广州曙光 原装进口乔雅登极致玻尿酸0.8ml  玻尿酸中的爱马仕 院长操作","hos_name":"广州曙光医学美容医院","doc_name":"张小芸","price":"14888","price_discount":"8800","price_range_max":"0","id":"115081","_id":"115081","showprice":"1","specialPrice":"0","show_hospital":"1","invitation":"0","lijian":"0","baoxian":"","insure":{"is_insure":"0","insure_pay_money":"0","title":""},"img66":"","app_slide_logo":"","repayment":"最高可享12期分期付款：花呗分期","bilateral_coupons":"满6999减600","hos_red_packet":"满1999减100,满2999减200,满3999减300,满6999减600","mingyi":"0","hot":"0","newp":"0","shixiao":"0","extension_user":"","postStr":"","depreciate":"","rate":"135人预订","rateNum":135,"service":"5.0","feeScale":"/支(0.8ml)","is_fanxian":"1","hospital_id":"4066","doctor_id":"59347","is_rongyun":"3","hos_userid":"85280097","business_district":"","hospital_top":{"level":0,"desc":""},"promotion":[],"userImg":["https://p21.yuemei.com/avatar/085/18/97/97_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/19/00/49_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/19/01/23_avatar_50_50.jpg"],"surgInfo":{"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"},"highlight_title":"","videoTaoTitle":"","event_params":{"to_page_type":2,"to_page_id":115081,"event_others":0},"pk_id":98}]
     * is_has_more : 0 没有更多了  1 可以继续加载
     */

    private String is_has_more;
    private List<TaoListBean> tao_list;

    public String getIs_has_more() {
        return is_has_more;
    }

    public void setIs_has_more(String is_has_more) {
        this.is_has_more = is_has_more;
    }

    public List<TaoListBean> getTao_list() {
        return tao_list;
    }

    public void setTao_list(List<TaoListBean> tao_list) {
        this.tao_list = tao_list;
    }

    public static class TaoListBean {
        /**
         * sale_type : 0
         * appmurl : https://m.yuemei.com/tao/115081/
         * coupon_type : 0
         * is_show_member : 1
         * pay_dingjin : 1760
         * number : 1
         * start_number : 1
         * pay_price_discount : 8800
         * member_price : 8360
         * m_list_logo :
         * bmsid : 0
         * seckilling : 0
         * img : https://p24.yuemei.com/tao/2019/1130/200_200/jt191130153145_1bff11.jpg
         * img360 : https://p24.yuemei.com/tao/360_360/2019/1130/jt191130153145_1bff11.jpg
         * title : 玻尿酸
         * subtitle : 广州曙光 原装进口乔雅登极致玻尿酸0.8ml  玻尿酸中的爱马仕 院长操作
         * hos_name : 广州曙光医学美容医院
         * doc_name : 张小芸
         * price : 14888
         * price_discount : 8800
         * price_range_max : 0
         * id : 115081
         * _id : 115081
         * showprice : 1
         * specialPrice : 0
         * show_hospital : 1
         * invitation : 0
         * lijian : 0
         * baoxian :
         * insure : {"is_insure":"0","insure_pay_money":"0","title":""}
         * img66 :
         * app_slide_logo :
         * repayment : 最高可享12期分期付款：花呗分期
         * bilateral_coupons : 满6999减600
         * hos_red_packet : 满1999减100,满2999减200,满3999减300,满6999减600
         * mingyi : 0
         * hot : 0
         * newp : 0
         * shixiao : 0
         * extension_user :
         * postStr :
         * depreciate :
         * rate : 135人预订
         * rateNum : 135
         * service : 5.0
         * feeScale : /支(0.8ml)
         * is_fanxian : 1
         * hospital_id : 4066
         * doctor_id : 59347
         * is_rongyun : 3
         * hos_userid : 85280097
         * business_district :
         * hospital_top : {"level":0,"desc":""}
         * promotion : []
         * userImg : ["https://p21.yuemei.com/avatar/085/18/97/97_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/19/00/49_avatar_50_50.jpg","https://p21.yuemei.com/avatar/085/19/01/23_avatar_50_50.jpg"]
         * surgInfo : {"id":"18157","title":"填充塑形","name":"填充塑形","alias":"填充塑形","url_name":"fill"}
         * highlight_title :
         * videoTaoTitle :
         * event_params : {"to_page_type":2,"to_page_id":115081,"event_others":0}
         * pk_id : 98
         */

        private int sale_type;
        private String appmurl;
        private int coupon_type;
        private String is_show_member;
        private String pay_dingjin;
        private String number;
        private String start_number;
        private String pay_price_discount;
        private String member_price;
        private String m_list_logo;
        private String bmsid;
        private String seckilling;
        private String img;
        private String img360;
        private String title;
        private String subtitle;
        private String hos_name;
        private String doc_name;
        private String price;
        private String price_discount;
        private String price_range_max;
        private String id;
        private String _id;
        private String showprice;
        private String specialPrice;
        private String show_hospital;
        private String invitation;
        private String lijian;
        private String baoxian;
        private InsureBean insure;
        private String img66;
        private String app_slide_logo;
        private String repayment;
        private String bilateral_coupons;
        private String hos_red_packet;
        private String mingyi;
        private String hot;
        private String newp;
        private String shixiao;
        private String extension_user;
        private String postStr;
        private String depreciate;
        private String rate;
        private int rateNum;
        private String service;
        private String feeScale;
        private String is_fanxian;
        private String hospital_id;
        private String doctor_id;
        private String is_rongyun;
        private String hos_userid;
        private String business_district;
        private HospitalTopBean hospital_top;
        private SurgInfoBean surgInfo;
        private String highlight_title;
        private String videoTaoTitle;
        private EventParamsBean event_params;
        private int pk_id;
        private String tick_time;
        private String tao_pk_select;
        private List<?> promotion;
        private List<String> userImg;
        private String is_have_video;

        public String getTick_time() {
            return tick_time;
        }

        public void setTick_time(String tick_time) {
            this.tick_time = tick_time;
        }

        public String getIs_have_video() {
            return is_have_video;
        }

        public void setIs_have_video(String is_have_video) {
            this.is_have_video = is_have_video;
        }

        public String getTao_pk_select() {
            return tao_pk_select;
        }

        public void setTao_pk_select(String tao_pk_select) {
            this.tao_pk_select = tao_pk_select;
        }

        public int getSale_type() {
            return sale_type;
        }

        public void setSale_type(int sale_type) {
            this.sale_type = sale_type;
        }

        public String getAppmurl() {
            return appmurl;
        }

        public void setAppmurl(String appmurl) {
            this.appmurl = appmurl;
        }

        public int getCoupon_type() {
            return coupon_type;
        }

        public void setCoupon_type(int coupon_type) {
            this.coupon_type = coupon_type;
        }

        public String getIs_show_member() {
            return is_show_member;
        }

        public void setIs_show_member(String is_show_member) {
            this.is_show_member = is_show_member;
        }

        public String getPay_dingjin() {
            return pay_dingjin;
        }

        public void setPay_dingjin(String pay_dingjin) {
            this.pay_dingjin = pay_dingjin;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStart_number() {
            return start_number;
        }

        public void setStart_number(String start_number) {
            this.start_number = start_number;
        }

        public String getPay_price_discount() {
            return pay_price_discount;
        }

        public void setPay_price_discount(String pay_price_discount) {
            this.pay_price_discount = pay_price_discount;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getM_list_logo() {
            return m_list_logo;
        }

        public void setM_list_logo(String m_list_logo) {
            this.m_list_logo = m_list_logo;
        }

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getSeckilling() {
            return seckilling;
        }

        public void setSeckilling(String seckilling) {
            this.seckilling = seckilling;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg360() {
            return img360;
        }

        public void setImg360(String img360) {
            this.img360 = img360;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice_range_max() {
            return price_range_max;
        }

        public void setPrice_range_max(String price_range_max) {
            this.price_range_max = price_range_max;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getShowprice() {
            return showprice;
        }

        public void setShowprice(String showprice) {
            this.showprice = showprice;
        }

        public String getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(String specialPrice) {
            this.specialPrice = specialPrice;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getInvitation() {
            return invitation;
        }

        public void setInvitation(String invitation) {
            this.invitation = invitation;
        }

        public String getLijian() {
            return lijian;
        }

        public void setLijian(String lijian) {
            this.lijian = lijian;
        }

        public String getBaoxian() {
            return baoxian;
        }

        public void setBaoxian(String baoxian) {
            this.baoxian = baoxian;
        }

        public InsureBean getInsure() {
            return insure;
        }

        public void setInsure(InsureBean insure) {
            this.insure = insure;
        }

        public String getImg66() {
            return img66;
        }

        public void setImg66(String img66) {
            this.img66 = img66;
        }

        public String getApp_slide_logo() {
            return app_slide_logo;
        }

        public void setApp_slide_logo(String app_slide_logo) {
            this.app_slide_logo = app_slide_logo;
        }

        public String getRepayment() {
            return repayment;
        }

        public void setRepayment(String repayment) {
            this.repayment = repayment;
        }

        public String getBilateral_coupons() {
            return bilateral_coupons;
        }

        public void setBilateral_coupons(String bilateral_coupons) {
            this.bilateral_coupons = bilateral_coupons;
        }

        public String getHos_red_packet() {
            return hos_red_packet;
        }

        public void setHos_red_packet(String hos_red_packet) {
            this.hos_red_packet = hos_red_packet;
        }

        public String getMingyi() {
            return mingyi;
        }

        public void setMingyi(String mingyi) {
            this.mingyi = mingyi;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getNewp() {
            return newp;
        }

        public void setNewp(String newp) {
            this.newp = newp;
        }

        public String getShixiao() {
            return shixiao;
        }

        public void setShixiao(String shixiao) {
            this.shixiao = shixiao;
        }

        public String getExtension_user() {
            return extension_user;
        }

        public void setExtension_user(String extension_user) {
            this.extension_user = extension_user;
        }

        public String getPostStr() {
            return postStr;
        }

        public void setPostStr(String postStr) {
            this.postStr = postStr;
        }

        public String getDepreciate() {
            return depreciate;
        }

        public void setDepreciate(String depreciate) {
            this.depreciate = depreciate;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public int getRateNum() {
            return rateNum;
        }

        public void setRateNum(int rateNum) {
            this.rateNum = rateNum;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoctor_id() {
            return doctor_id;
        }

        public void setDoctor_id(String doctor_id) {
            this.doctor_id = doctor_id;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getBusiness_district() {
            return business_district;
        }

        public void setBusiness_district(String business_district) {
            this.business_district = business_district;
        }

        public HospitalTopBean getHospital_top() {
            return hospital_top;
        }

        public void setHospital_top(HospitalTopBean hospital_top) {
            this.hospital_top = hospital_top;
        }

        public SurgInfoBean getSurgInfo() {
            return surgInfo;
        }

        public void setSurgInfo(SurgInfoBean surgInfo) {
            this.surgInfo = surgInfo;
        }

        public String getHighlight_title() {
            return highlight_title;
        }

        public void setHighlight_title(String highlight_title) {
            this.highlight_title = highlight_title;
        }

        public String getVideoTaoTitle() {
            return videoTaoTitle;
        }

        public void setVideoTaoTitle(String videoTaoTitle) {
            this.videoTaoTitle = videoTaoTitle;
        }

        public EventParamsBean getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBean event_params) {
            this.event_params = event_params;
        }

        public int getPk_id() {
            return pk_id;
        }

        public void setPk_id(int pk_id) {
            this.pk_id = pk_id;
        }

        public List<?> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<?> promotion) {
            this.promotion = promotion;
        }

        public List<String> getUserImg() {
            return userImg;
        }

        public void setUserImg(List<String> userImg) {
            this.userImg = userImg;
        }

        public static class InsureBean {
            /**
             * is_insure : 0
             * insure_pay_money : 0
             * title :
             */

            private String is_insure;
            private String insure_pay_money;
            private String title;

            public String getIs_insure() {
                return is_insure;
            }

            public void setIs_insure(String is_insure) {
                this.is_insure = is_insure;
            }

            public String getInsure_pay_money() {
                return insure_pay_money;
            }

            public void setInsure_pay_money(String insure_pay_money) {
                this.insure_pay_money = insure_pay_money;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class HospitalTopBean {
            /**
             * level : 0
             * desc :
             */

            private int level;
            private String desc;

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }
        }

        public static class SurgInfoBean {
            /**
             * id : 18157
             * title : 填充塑形
             * name : 填充塑形
             * alias : 填充塑形
             * url_name : fill
             */

            private String id;
            private String title;
            private String name;
            private String alias;
            private String url_name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public String getUrl_name() {
                return url_name;
            }

            public void setUrl_name(String url_name) {
                this.url_name = url_name;
            }
        }

        public static class EventParamsBean {
            /**
             * to_page_type : 2
             * to_page_id : 115081
             * event_others : 0
             */

            private int to_page_type;
            private int to_page_id;
            private int event_others;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }

            public int getEvent_others() {
                return event_others;
            }

            public void setEvent_others(int event_others) {
                this.event_others = event_others;
            }
        }
    }
}
