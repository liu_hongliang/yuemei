package com.quicklyask.entity;

import java.util.HashMap;
import java.util.List;

public class AppRaisalBean {

    private String title;
    private String rate;
    private String jumpUrl;
    private String num;
    private List<AppraisalListBean> appraisalList;
    private HashMap<String,String> event_params;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String jumpUrl) {
        this.jumpUrl = jumpUrl;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public List<AppraisalListBean> getAppraisalList() {
        return appraisalList;
    }

    public void setAppraisalList(List<AppraisalListBean> appraisalList) {
        this.appraisalList = appraisalList;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
