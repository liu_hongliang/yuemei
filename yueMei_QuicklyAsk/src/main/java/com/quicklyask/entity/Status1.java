package com.quicklyask.entity;

public class Status1 {
	private String code;
	private String message;
	private Status1Data data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status1Data getData() {
		return data;
	}

	public void setData(Status1Data data) {
		this.data = data;
	}


}
