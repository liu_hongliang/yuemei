/**
 * 
 */
package com.quicklyask.entity;

import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class ProjectHomeData {

	private List<ProjectPart> part;

	public List<ProjectPart> getPart() {
		return part;
	}

	public void setPart(List<ProjectPart> part) {
		this.part = part;
	}
}
