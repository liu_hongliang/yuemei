package com.quicklyask.entity;

import java.util.HashMap;
import java.util.List;

public class Coupons {
    private CouponsData data;
    private String url;
    private HashMap<String,String> event_params;

    public CouponsData getData() {
        return data;
    }

    public void setData(CouponsData data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
