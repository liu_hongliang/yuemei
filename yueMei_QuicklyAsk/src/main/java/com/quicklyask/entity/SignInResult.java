package com.quicklyask.entity;

import java.util.List;

/**
 * Created by ${裴成浩} on 2017/6/6.
 */

public class SignInResult {

    /**
     * code : 1
     * message : 请先登录
     * data : {"rule":"1.用户每日签到可获得颜值币奖励，每连续签到7天可获得一次抽奖。 2.连续签到过程中，如果天数断开，再次签到自动从第一天算起。 3.连续签到1到7天，分别可获得1、2、3、4、6、8、10个颜值币，获得的颜值币可在颜值币商城进行商品兑换。 ","topImg":[{"qid":"1613","flag":"0","url":"http://m.yuemei.com/tao_zt/1613.html","img":"https://p31.yuemei.com/tag/1496632782bf016.jpg","title":"悦美","type":"0"}],"is_signin":"0","isDaySignin":[{"url":"","day":"1","integral":"1","is_signin":"1"},{"url":"","day":"2","integral":"2","is_signin":"1"},{"url":"","day":"3","integral":"3","is_signin":"0"},{"url":"","day":"4","integral":"4","is_signin":"0"},{"url":"","day":"5","integral":"6","is_signin":"0"},{"url":"","day":"6","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"7","integral":"10","is_signin":"0"},{"url":"","day":"8","integral":"1","is_signin":"0"},{"url":"","day":"9","integral":"2","is_signin":"0"},{"url":"","day":"10","integral":"3","is_signin":"0"},{"url":"","day":"11","integral":"4","is_signin":"0"},{"url":"","day":"12","integral":"6","is_signin":"0"},{"url":"","day":"13","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"14","integral":"10","is_signin":"0"},{"url":"","day":"15","integral":"1","is_signin":"0"},{"url":"","day":"16","integral":"2","is_signin":"0"},{"url":"","day":"17","integral":"3","is_signin":"0"},{"url":"","day":"18","integral":"4","is_signin":"0"},{"url":"","day":"19","integral":"6","is_signin":"0"},{"url":"","day":"20","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"21","integral":"10","is_signin":"0"},{"url":"","day":"22","integral":"1","is_signin":"0"},{"url":"","day":"23","integral":"2","is_signin":"0"},{"url":"","day":"24","integral":"3","is_signin":"0"},{"url":"","day":"25","integral":"4","is_signin":"0"},{"url":"","day":"26","integral":"6","is_signin":"0"},{"url":"","day":"27","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"28","integral":"10","is_signin":"0"}],"integral":"705","continue_signin":"2"}
     */

    private String code;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * rule : 1.用户每日签到可获得颜值币奖励，每连续签到7天可获得一次抽奖。 2.连续签到过程中，如果天数断开，再次签到自动从第一天算起。 3.连续签到1到7天，分别可获得1、2、3、4、6、8、10个颜值币，获得的颜值币可在颜值币商城进行商品兑换。
         * topImg : [{"qid":"1613","flag":"0","url":"http://m.yuemei.com/tao_zt/1613.html","img":"https://p31.yuemei.com/tag/1496632782bf016.jpg","title":"悦美","type":"0"}]
         * is_signin : 0
         * isDaySignin : [{"url":"","day":"1","integral":"1","is_signin":"1"},{"url":"","day":"2","integral":"2","is_signin":"1"},{"url":"","day":"3","integral":"3","is_signin":"0"},{"url":"","day":"4","integral":"4","is_signin":"0"},{"url":"","day":"5","integral":"6","is_signin":"0"},{"url":"","day":"6","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"7","integral":"10","is_signin":"0"},{"url":"","day":"8","integral":"1","is_signin":"0"},{"url":"","day":"9","integral":"2","is_signin":"0"},{"url":"","day":"10","integral":"3","is_signin":"0"},{"url":"","day":"11","integral":"4","is_signin":"0"},{"url":"","day":"12","integral":"6","is_signin":"0"},{"url":"","day":"13","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"14","integral":"10","is_signin":"0"},{"url":"","day":"15","integral":"1","is_signin":"0"},{"url":"","day":"16","integral":"2","is_signin":"0"},{"url":"","day":"17","integral":"3","is_signin":"0"},{"url":"","day":"18","integral":"4","is_signin":"0"},{"url":"","day":"19","integral":"6","is_signin":"0"},{"url":"","day":"20","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"21","integral":"10","is_signin":"0"},{"url":"","day":"22","integral":"1","is_signin":"0"},{"url":"","day":"23","integral":"2","is_signin":"0"},{"url":"","day":"24","integral":"3","is_signin":"0"},{"url":"","day":"25","integral":"4","is_signin":"0"},{"url":"","day":"26","integral":"6","is_signin":"0"},{"url":"","day":"27","integral":"8","is_signin":"0"},{"url":"http://www.yuemei.com/api/duiba/duiba.php?act=murl&dbredirect=https%3A%2F%2Factivity.m.duiba.com.cn%2Fnewtools%2Findex%3Fid%3D2260838%26dblanding%3Dhttps%253A%252F%252Factivity.m.duiba.com.cn%252FactivityShare%252FgetActivityShareInfo%253FoperatingActivityId%253D2260838","day":"28","integral":"10","is_signin":"0"}]
         * integral : 705
         * continue_signin : 2
         */

        private String rule;
        private String is_signin;
        private String integral;
        private String continue_signin;
        private String dui_record;
        private List<TopImgBean> topImg;
        private List<IsDaySigninBean> isDaySignin;

        public String getDui_record() {
            return dui_record;
        }

        public void setDui_record(String dui_record) {
            this.dui_record = dui_record;
        }

        public String getRule() {
            return rule;
        }

        public void setRule(String rule) {
            this.rule = rule;
        }

        public String getIs_signin() {
            return is_signin;
        }

        public void setIs_signin(String is_signin) {
            this.is_signin = is_signin;
        }

        public String getIntegral() {
            return integral;
        }

        public void setIntegral(String integral) {
            this.integral = integral;
        }

        public String getContinue_signin() {
            return continue_signin;
        }

        public void setContinue_signin(String continue_signin) {
            this.continue_signin = continue_signin;
        }

        public List<TopImgBean> getTopImg() {
            return topImg;
        }

        public void setTopImg(List<TopImgBean> topImg) {
            this.topImg = topImg;
        }

        public List<IsDaySigninBean> getIsDaySignin() {
            return isDaySignin;
        }

        public void setIsDaySignin(List<IsDaySigninBean> isDaySignin) {
            this.isDaySignin = isDaySignin;
        }

        public static class TopImgBean {
            /**
             * qid : 1613
             * flag : 0
             * url : http://m.yuemei.com/tao_zt/1613.html
             * img : https://p31.yuemei.com/tag/1496632782bf016.jpg
             * title : 悦美
             * type : 0
             */

            private String qid;
            private String flag;
            private String url;
            private String img;
            private String title;
            private String type;

            public String getQid() {
                return qid;
            }

            public void setQid(String qid) {
                this.qid = qid;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }

        public static class IsDaySigninBean {
            /**
             * url :
             * day : 1
             * integral : 1
             * is_signin : 1
             */

            private String url;
            private String day;
            private String integral;
            private String is_signin;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getIntegral() {
                return integral;
            }

            public void setIntegral(String integral) {
                this.integral = integral;
            }

            public String getIs_signin() {
                return is_signin;
            }

            public void setIs_signin(String is_signin) {
                this.is_signin = is_signin;
            }
        }
    }
}
