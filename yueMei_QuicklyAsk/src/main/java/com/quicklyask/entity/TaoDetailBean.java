package com.quicklyask.entity;

import com.module.commonview.module.bean.SharePictorial;
import com.module.commonview.module.bean.ShareWechat;

import java.util.HashMap;
import java.util.List;

public class TaoDetailBean {


    /**
     * pic : [{"img":"https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg"},{"img":"https://p34.yuemei.com/tao/2018/0512/jt180512165638_63030e.jpg"},{"img":"https://p34.yuemei.com/tao/2018/0512/jt180512165646_e30b1a.jpg"},{"img":"https://p34.yuemei.com/tao/2018/0512/jt180512165652_44e315.jpg"},{"img":"https://p24.yuemei.com/tao/2018/0512/jt180512165722_2e8bae.jpg"}]
     * video : [{"img":"https://p22.yuemei.com/postimg/20181018/500_500/20181018192409_2308e6.jpg","url":"https://player.yuemei.com/video/6ea2/201810/c3e214deeefe3b46774b07b9acc8495a.mp4"}]
     * basedata : {"id":"119749","number":"20","start_number":"1","title":"【小阴唇整形手术】小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus","subtitle":"隐私保护 医师一对一 权威机构 品牌保证","price_discount":"3500","price":"8800","feeScale":"/次","order_num":"938人已预订","refund":"1","status":"1","is_fanxian":"1"}
     * member_data : {"user_is_member":"1","is_show_member":"1","desc":"享会员价，立即节省175元，消费后可得多倍返现。","frist_lijian_price":"0","member_price":"3325","member_dingjin":"670","member_hos_price":"2655"}
     * service : [{"imgname":"YMTaoAnytimeRefund","title":"未消费随时退"},{"imgname":"YMTaoCashBack","title":"术后写日记得返现"}]
     * groupInfo : {"is_group":"0","group_number":"0","group_price":"0","group_dingjin":"0","group_is_order":"3","group":[]}
     * proInfo : {"nowTime":"1543895156","endTime":"0","time_str":"距结束仅剩","show_str":"","isBigPromotion":"0","bigPromotionImg":"","bigPromotionTitle":"","bigPromotionUrl":""}
     * regInfo : {"is_seg":"0","endTime":"0","time_str":"距结束仅剩","nowTime":"1543895156","bigPromotionImg":"","bigPromotionTitle":"","bigPromotionUrl":"","show_str":""}
     * repayment : ["花呗分期：最低￥313.54×12期"]
     * baoxian : ["没有保险"]
     * promotion : ["新人优惠：首次购买该医院服务，第1件享￥xx优惠价","首件0元","首件优惠：首次购买，第1件享￥xx优惠价","每第2件半价","第3件8折"]
     * coupons : [{"title":"订金满减：","data":["满20000减5000","满20000减5000"],"num":"2个"},{"title":"医院红包：","data":["满10000减1000","满5000减500","满1500减200","满500减50"],"num":"4个"}]
     * rel_tao : {"rel_title":"规格","rel_tao":[{"tao_id":"70391","spe_name":"私密嫩红","status":"4","checked":"0"},{"tao_id":"119749","spe_name":"小阴唇矫正","status":"4","checked":"1"},{"tao_id":"119854","spe_name":"处女膜修复","status":"4","checked":"0"},{"tao_id":"122029","spe_name":"MEC微创阴道紧缩","status":"4","checked":"0"},{"tao_id":"120136","spe_name":"阴蒂提升术","status":"4","checked":"0"},{"tao_id":"120847","spe_name":"激光紧缩阴道","status":"4","checked":"0"}]}
     * iteminfo : {"cateid":"722","title":"女性私密"}
     * fee_explain : 单侧
     * use_time : 支付后60天内到院
     * warm_tips :
     来院前请先与我们的在线客服联系取得预约号，方可到院享受优惠喔~
     备注：此项目包含局麻麻醉费用
     使用花呗分期，申请退款时，已产生的手续费不退还
     单个商品一次购买多个，一旦到院确认消费后，此商品没有使用完的部分不支持退款，但仍然可以继续使用
     * comment : [{"show_type":"text","title":"口碑评价5.0分","data":[{"userdata":{"name":"听一半的歌曲","id":"85606045","avatar":"https://p21.yuemei.com/avatar/085/60/60/45_avatar_50_50.jpg","talent":"0","group_id":"1","lable":"北京 11-23","url":"https://m.yuemei.com/u/home/85606045/","obj_id":"85606045","obj_type":"6","is_following":"0"},"rateSale":"","appmurl":"https://m.yuemei.com/c/2501452.html","title":"连续上了１５天的班，好累，好乏，也没来给你们更新，跟你们说哈，做小阴唇手术之后，这鬼真是越来越那个了，还时不时的夸我，做小阴唇手术真的是很好的选择，感觉他就是粘合剂，给爱情的另一种行为增添了几分激情，也让他越来越爱我，越来越宠着我，做了之后改变的并不是性生活的提高，更多改变的是两个人的心情，真是太感谢长虹的医生了，把我之前的主动改变带哦现在的被动，跟你们说最近下班一直在追一部剧\u201c我和你的倾城时光\u201d强烈推荐非常好看的一部电视剧，我已经迷的不要不要的了，我两天时间从第１集追到第１８集，吃饭看，早晨起来上班化妆的时间也不能闲着，一直都很喜欢兵哥哥，金瀚真的是太帅了，这果断关注他围脖，等着他翻牌子，诶呦，说的我都不好意思了，嘘，别让我男朋友看见，看见了不好"}],"linkdata":{"urltitle":"查看全部 (371)","url":"type:eq:6550:and:sort:eq:1:and:flag:eq:0"}}]
     * tao_ask : [{"show_type":"text","title":"有问题，问大家","data":[{"question":"请问做过小阴唇手术的姑娘们手术期间会疼吗之后有不适或者其它不良反应吗？多谢啦","answer":"没有除了打麻醉又点疼可以忍受","answer_num":"1","question_num":"1"}],"linkdata":{"urltitle":"查看全部 (1)","url":"type:eq:6551"}}]
     * hos_doc : {"show_hospital":"1","doc_name":"张守玲","doc_user_id":"654589","doc_title":"副主任医师","doc_url":"https://m.yuemei.com/dr/00654589/","hospital_name":"北京长虹医疗美容医院","hospital_id":"3379","hospital_address":"地址：北京市朝阳区东三环北路甲17号","kind":"2","hours":"","hos_img":"https://ymt.yuemei.com/upload/vip/120_120/15235817649584.jpg","phone":"4000567118","alert_title":"您将拨打客服电话","alert_subtitle":"客服咨询时间：周一至周日9:30-18:30","alert_button":"拨打客服电话"}
     * share : {"url_new":"https://m.yuemei.com/tao/119749/?uu=DDJ4zwHrz9JE1cmm98ivKw%3D%3D","url":"https://m.yuemei.com/tao/119749/","title":"【悦美】小阴唇整形手术","content":"小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus 隐私保护 医师一对一 权威机构 品牌保证","img_weibo":"https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg","img_weix":"https://p34.yuemei.com/tao/2018/1118/200_200/jt181118101505_9117b1.jpg","Wechat":{"title":"【悦美】小阴唇整形手术 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus","description":"小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus 隐私保护 医师一对一 权威机构 品牌保证","webpageUrl":"https://m.yuemei.com/tao/119749/?uu=DDJ4zwHrz9JE1cmm98ivKw%3D%3D","path":"/pages/index/index?tao_id=119749&from_user_id=675727&type=4","thumbImage":"https://p34.yuemei.com/tao/2018/1118/200_200/jt181118101505_9117b1.jpg","userName":"gh_4ce0355fc6ab"},"Pictorial":{"img":"https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg","title":"【小阴唇整形手术】 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus","price_discount":"3500","price":"8800","order_num":"938人已预约","feeScale":"/次","hos_name":"北京长虹医疗美容医院","hos_rate":"5.0","hos_rateScale":"100","sun_img":"https://p11.yuemei.com/wx_small_sun/119749/119749.jpg"}}
     * postStr : lkw8ryqGpG4b60zo6Dgh%2FfNUCMUrjDimS7yryFZY%2Bdl5QPDSOLb0rkt4WWvDqdLt
     * chatData : {"is_rongyun":"3","hos_userid":"85282559"}
     * sale_rec_tao : []
     * alert : [{"img":"https://p24.yuemei.com/tao/2018/1129/200_200/jt181129140454_613690.jpg","title":"有果酸焕肤新订单，来自悦**3"},{"img":"https://p24.yuemei.com/tao/2018/1120/200_200/jt181120154404_4e58ec.jpg","title":"有水光针新订单，来自哈**尔"},{"img":"https://p24.yuemei.com/tao/2018/0427/200_200/jt180427170849_19d066.jpg","title":"有水动力吸脂瘦腰腹新订单，来自雪**飞"}]
     * pay_price : {"dingjin":"670","is_order":"3","hos_price":"2655","payPrice":"3325","discountPrice":"0","is_member":"1"}
     */

    private BasedataBean basedata;
    private MemberDataBean member_data;
    private GroupInfoBean groupInfo;
    private ProInfoBean proInfo;
    private RegInfoBean regInfo;
    private RelTaoBeanX rel_tao;
    private IteminfoBean iteminfo;
    private String fee_explain;
    private String use_time;
    private String warm_tips;
    private HosDocBean hos_doc;
    private ShareBean share;
    private String postStr;
    private ChatDataBean chatData;
    private ChatDataBean chatDataAlert;
    private PayPriceBean pay_price;
    private List<PicBean> pic;
    private List<VideoBean> video;
    private List<ServiceBean> service;
    private List<RepaymentBean> repayment;
    private List<String> baoxian;
    private List<String> promotion;
    private List<CouponsBean> coupons;
    private List<CommentBean> comment;
    private TaoAskBean tao_ask;
    private List<SaleRecTaoBean> sale_rec_tao;
    private List<AlertBean> alert;
    private AlertCouponsBean alertCoupons;
    private TaoUserCoupons taoUseCoupons;
    private int baike_id;
    private String baike_title;
    private String low_price;
    private CouponPriceInfo couponPriceInfo;
    private AppRaisalBean appraisal;
    private DiaryListBean diaryList;
    private List<TaoQuickReply> taoQuickReply;
    private TaoPsoter taoPoster;
    private String is_presence_tao_pk;
    private HospitalTop hospital_top;
    private List<UserBrowseTao> user_browse_tao;
    private String bargain_url;
    private String is_face_video;//是否有视频面诊（1有）
    private String face_video_url;

    public String getFace_video_url() {
        return face_video_url;
    }

    public void setFace_video_url(String face_video_url) {
        this.face_video_url = face_video_url;
    }

    public String getIs_face_video() {
        return is_face_video;
    }

    public void setIs_face_video(String is_face_video) {
        this.is_face_video = is_face_video;
    }

    public String getBargain_url() {
        return bargain_url;
    }

    public void setBargain_url(String bargain_url) {
        this.bargain_url = bargain_url;
    }

    public List<UserBrowseTao> getUser_browse_tao() {
        return user_browse_tao;
    }

    public void setUser_browse_tao(List<UserBrowseTao> user_browse_tao) {
        this.user_browse_tao = user_browse_tao;
    }

    public String getIs_presence_tao_pk() {
        return is_presence_tao_pk;
    }

    public void setIs_presence_tao_pk(String is_presence_tao_pk) {
        this.is_presence_tao_pk = is_presence_tao_pk;
    }

    public HospitalTop getHospital_top() {
        return hospital_top;
    }

    public void setHospital_top(HospitalTop hospital_top) {
        this.hospital_top = hospital_top;
    }

    public BasedataBean getBasedata() {
        return basedata;
    }

    public void setBasedata(BasedataBean basedata) {
        this.basedata = basedata;
    }

    public MemberDataBean getMember_data() {
        return member_data;
    }

    public void setMember_data(MemberDataBean member_data) {
        this.member_data = member_data;
    }

    public GroupInfoBean getGroupInfo() {
        return groupInfo;
    }

    public void setGroupInfo(GroupInfoBean groupInfo) {
        this.groupInfo = groupInfo;
    }

    public ProInfoBean getProInfo() {
        return proInfo;
    }

    public void setProInfo(ProInfoBean proInfo) {
        this.proInfo = proInfo;
    }

    public RegInfoBean getRegInfo() {
        return regInfo;
    }

    public void setRegInfo(RegInfoBean regInfo) {
        this.regInfo = regInfo;
    }

    public RelTaoBeanX getRel_tao() {
        return rel_tao;
    }

    public void setRel_tao(RelTaoBeanX rel_tao) {
        this.rel_tao = rel_tao;
    }

    public IteminfoBean getIteminfo() {
        return iteminfo;
    }

    public void setIteminfo(IteminfoBean iteminfo) {
        this.iteminfo = iteminfo;
    }

    public String getFee_explain() {
        return fee_explain;
    }

    public void setFee_explain(String fee_explain) {
        this.fee_explain = fee_explain;
    }

    public String getUse_time() {
        return use_time;
    }

    public void setUse_time(String use_time) {
        this.use_time = use_time;
    }

    public String getWarm_tips() {
        return warm_tips;
    }

    public void setWarm_tips(String warm_tips) {
        this.warm_tips = warm_tips;
    }

    public HosDocBean getHos_doc() {
        return hos_doc;
    }

    public void setHos_doc(HosDocBean hos_doc) {
        this.hos_doc = hos_doc;
    }

    public ShareBean getShare() {
        return share;
    }

    public void setShare(ShareBean share) {
        this.share = share;
    }

    public String getPostStr() {
        return postStr;
    }

    public void setPostStr(String postStr) {
        this.postStr = postStr;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public PayPriceBean getPay_price() {
        return pay_price;
    }

    public void setPay_price(PayPriceBean pay_price) {
        this.pay_price = pay_price;
    }

    public List<PicBean> getPic() {
        return pic;
    }

    public void setPic(List<PicBean> pic) {
        this.pic = pic;
    }

    public List<VideoBean> getVideo() {
        return video;
    }

    public void setVideo(List<VideoBean> video) {
        this.video = video;
    }

    public List<ServiceBean> getService() {
        return service;
    }

    public void setService(List<ServiceBean> service) {
        this.service = service;
    }

    public List<RepaymentBean> getRepayment() {
        return repayment;
    }

    public void setRepayment(List<RepaymentBean> repayment) {
        this.repayment = repayment;
    }

    public List<String> getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(List<String> baoxian) {
        this.baoxian = baoxian;
    }

    public List<String> getPromotion() {
        return promotion;
    }

    public void setPromotion(List<String> promotion) {
        this.promotion = promotion;
    }

    public List<CouponsBean> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponsBean> coupons) {
        this.coupons = coupons;
    }

    public List<CommentBean> getComment() {
        return comment;
    }

    public void setComment(List<CommentBean> comment) {
        this.comment = comment;
    }

    public TaoAskBean getTao_ask() {
        return tao_ask;
    }

    public void setTao_ask(TaoAskBean tao_ask) {
        this.tao_ask = tao_ask;
    }

    public List<SaleRecTaoBean> getSale_rec_tao() {
        return sale_rec_tao;
    }

    public void setSale_rec_tao(List<SaleRecTaoBean> sale_rec_tao) {
        this.sale_rec_tao = sale_rec_tao;
    }

    public List<AlertBean> getAlert() {
        return alert;
    }

    public void setAlert(List<AlertBean> alert) {
        this.alert = alert;
    }

    public ChatDataBean getChatDataAlert() {
        return chatDataAlert;
    }

    public void setChatDataAlert(ChatDataBean chatDataAlert) {
        this.chatDataAlert = chatDataAlert;
    }

    public AlertCouponsBean getAlertCoupons() {
        return alertCoupons;
    }

    public void setAlertCoupons(AlertCouponsBean alertCoupons) {
        this.alertCoupons = alertCoupons;
    }

    public TaoUserCoupons getTaoUseCoupons() {
        return taoUseCoupons;
    }

    public void setTaoUseCoupons(TaoUserCoupons taoUseCoupons) {
        this.taoUseCoupons = taoUseCoupons;
    }

    public int getBaike_id() {
        return baike_id;
    }

    public void setBaike_id(int baike_id) {
        this.baike_id = baike_id;
    }

    public String getLow_price() {
        return low_price;
    }

    public void setLow_price(String low_price) {
        this.low_price = low_price;
    }

    public String getBaike_title() {
        return baike_title;
    }

    public void setBaike_title(String baike_title) {
        this.baike_title = baike_title;
    }

    public CouponPriceInfo getCouponPriceInfo() {
        return couponPriceInfo;
    }

    public void setCouponPriceInfo(CouponPriceInfo couponPriceInfo) {
        this.couponPriceInfo = couponPriceInfo;
    }

    public AppRaisalBean getAppraisal() {
        return appraisal;
    }

    public void setAppraisal(AppRaisalBean appraisal) {
        this.appraisal = appraisal;
    }

    public DiaryListBean getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(DiaryListBean diaryList) {
        this.diaryList = diaryList;
    }

    public List<TaoQuickReply> getTaoQuickReply() {
        return taoQuickReply;
    }

    public void setTaoQuickReply(List<TaoQuickReply> taoQuickReply) {
        this.taoQuickReply = taoQuickReply;
    }


    public TaoPsoter getTaoPoster() {
        return taoPoster;
    }

    public void setTaoPoster(TaoPsoter taoPoster) {
        this.taoPoster = taoPoster;
    }

    public static class BasedataBean {
        /**
         * id : 119749
         * number : 20
         * start_number : 1
         * title : 【小阴唇整形手术】小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus
         * subtitle : 隐私保护 医师一对一 权威机构 品牌保证
         * price_discount : 3500
         * price : 8800
         * feeScale : /次
         * order_num : 938人已预订
         * refund : 1
         * status : 1
         * is_fanxian : 1
         */

        private String id;
        private String number;
        private String start_number;
        private String title;
        private String subtitle;
        private String price_discount;
        private String price;
        private String lable;
        private String feeScale;
        private String order_num;
        private String refund;
        private String status;
        private String is_fanxian;
        private String tao_city_name;
        private String show_str;
        private String zekou;
        private String discount;
        private String isMonthBigPromotion;

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStart_number() {
            return start_number;
        }

        public void setStart_number(String start_number) {
            this.start_number = start_number;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getOrder_num() {
            return order_num;
        }

        public void setOrder_num(String order_num) {
            this.order_num = order_num;
        }

        public String getRefund() {
            return refund;
        }

        public void setRefund(String refund) {
            this.refund = refund;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }

        public String getTao_city_name() {
            return tao_city_name;
        }

        public void setTao_city_name(String tao_city_name) {
            this.tao_city_name = tao_city_name;
        }

        public String getShow_str() {
            return show_str;
        }

        public void setShow_str(String show_str) {
            this.show_str = show_str;
        }

        public String getZekou() {
            return zekou;
        }

        public void setZekou(String zekou) {
            this.zekou = zekou;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getIsMonthBigPromotion() {
            return isMonthBigPromotion;
        }

        public void setIsMonthBigPromotion(String isMonthBigPromotion) {
            this.isMonthBigPromotion = isMonthBigPromotion;
        }
    }

    public static class MemberDataBean {
        /**
         * user_is_member : 1
         * is_show_member : 1
         * desc : 享会员价，立即节省175元，消费后可得多倍返现。
         * frist_lijian_price : 0
         * member_price : 3325
         * member_dingjin : 670
         * member_hos_price : 2655
         */

        private String user_is_member;
        private String is_show_member;
        private String desc;
        private String frist_lijian_price;
        private String member_price;
        private String member_dingjin;
        private String member_hos_price;

        public String getUser_is_member() {
            return user_is_member;
        }

        public void setUser_is_member(String user_is_member) {
            this.user_is_member = user_is_member;
        }

        public String getIs_show_member() {
            return is_show_member;
        }

        public void setIs_show_member(String is_show_member) {
            this.is_show_member = is_show_member;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getFrist_lijian_price() {
            return frist_lijian_price;
        }

        public void setFrist_lijian_price(String frist_lijian_price) {
            this.frist_lijian_price = frist_lijian_price;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getMember_dingjin() {
            return member_dingjin;
        }

        public void setMember_dingjin(String member_dingjin) {
            this.member_dingjin = member_dingjin;
        }

        public String getMember_hos_price() {
            return member_hos_price;
        }

        public void setMember_hos_price(String member_hos_price) {
            this.member_hos_price = member_hos_price;
        }
    }

    public static class GroupInfoBean {
        /**
         * is_group : 0
         * group_number : 0
         * group_price : 0
         * group_dingjin : 0
         * group_is_order : 3
         * group : []
         */

        private String is_group;
        private String group_number;
        private String group_price;
        private String group_dingjin;
        private String group_is_order;
        private String nowTime;
        private String endTime;
        private List<GroupBean> group;

        public String getIs_group() {
            return is_group;
        }

        public void setIs_group(String is_group) {
            this.is_group = is_group;
        }

        public String getGroup_number() {
            return group_number;
        }

        public void setGroup_number(String group_number) {
            this.group_number = group_number;
        }

        public String getGroup_price() {
            return group_price;
        }

        public void setGroup_price(String group_price) {
            this.group_price = group_price;
        }

        public String getGroup_dingjin() {
            return group_dingjin;
        }

        public void setGroup_dingjin(String group_dingjin) {
            this.group_dingjin = group_dingjin;
        }

        public String getGroup_is_order() {
            return group_is_order;
        }

        public void setGroup_is_order(String group_is_order) {
            this.group_is_order = group_is_order;
        }

        public String getNowTime() {
            return nowTime;
        }

        public void setNowTime(String nowTime) {
            this.nowTime = nowTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public List<GroupBean> getGroup() {
            return group;
        }

        public void setGroup(List<GroupBean> group) {
            this.group = group;
        }


    }

    public static class ProInfoBean {
        /**
         * nowTime : 1543895156
         * endTime : 0
         * time_str : 距结束仅剩
         * show_str :
         * isBigPromotion : 0
         * bigPromotionImg :
         * bigPromotionTitle :
         * bigPromotionUrl :
         */

        private String nowTime;
        private String endTime;
        private String time_str;
        private String show_str;
        private String isBigPromotion;
        private String bigPromotionImg;
        private String bigPromotionTitle;
        private String bigPromotionUrl;

        public String getNowTime() {
            return nowTime;
        }

        public void setNowTime(String nowTime) {
            this.nowTime = nowTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getTime_str() {
            return time_str;
        }

        public void setTime_str(String time_str) {
            this.time_str = time_str;
        }

        public String getShow_str() {
            return show_str;
        }

        public void setShow_str(String show_str) {
            this.show_str = show_str;
        }

        public String getIsBigPromotion() {
            return isBigPromotion;
        }

        public void setIsBigPromotion(String isBigPromotion) {
            this.isBigPromotion = isBigPromotion;
        }

        public String getBigPromotionImg() {
            return bigPromotionImg;
        }

        public void setBigPromotionImg(String bigPromotionImg) {
            this.bigPromotionImg = bigPromotionImg;
        }

        public String getBigPromotionTitle() {
            return bigPromotionTitle;
        }

        public void setBigPromotionTitle(String bigPromotionTitle) {
            this.bigPromotionTitle = bigPromotionTitle;
        }

        public String getBigPromotionUrl() {
            return bigPromotionUrl;
        }

        public void setBigPromotionUrl(String bigPromotionUrl) {
            this.bigPromotionUrl = bigPromotionUrl;
        }

    }

    public static class RegInfoBean {
        /**
         * is_seg : 0
         * endTime : 0
         * time_str : 距结束仅剩
         * nowTime : 1543895156
         * bigPromotionImg :
         * bigPromotionTitle :
         * bigPromotionUrl :
         * show_str :
         */

        private String is_seg;
        private String endTime;
        private String time_str;
        private String nowTime;
        private String bigPromotionImg;
        private String bigPromotionTitle;
        private String bigPromotionUrl;
        private String show_str;

        public String getIs_seg() {
            return is_seg;
        }

        public void setIs_seg(String is_seg) {
            this.is_seg = is_seg;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getTime_str() {
            return time_str;
        }

        public void setTime_str(String time_str) {
            this.time_str = time_str;
        }

        public String getNowTime() {
            return nowTime;
        }

        public void setNowTime(String nowTime) {
            this.nowTime = nowTime;
        }

        public String getBigPromotionImg() {
            return bigPromotionImg;
        }

        public void setBigPromotionImg(String bigPromotionImg) {
            this.bigPromotionImg = bigPromotionImg;
        }

        public String getBigPromotionTitle() {
            return bigPromotionTitle;
        }

        public void setBigPromotionTitle(String bigPromotionTitle) {
            this.bigPromotionTitle = bigPromotionTitle;
        }

        public String getBigPromotionUrl() {
            return bigPromotionUrl;
        }

        public void setBigPromotionUrl(String bigPromotionUrl) {
            this.bigPromotionUrl = bigPromotionUrl;
        }

        public String getShow_str() {
            return show_str;
        }

        public void setShow_str(String show_str) {
            this.show_str = show_str;
        }
    }

    public static class RelTaoBeanX {
        /**
         * rel_title : 规格
         * rel_tao : [{"tao_id":"70391","spe_name":"私密嫩红","status":"4","checked":"0"},{"tao_id":"119749","spe_name":"小阴唇矫正","status":"4","checked":"1"},{"tao_id":"119854","spe_name":"处女膜修复","status":"4","checked":"0"},{"tao_id":"122029","spe_name":"MEC微创阴道紧缩","status":"4","checked":"0"},{"tao_id":"120136","spe_name":"阴蒂提升术","status":"4","checked":"0"},{"tao_id":"120847","spe_name":"激光紧缩阴道","status":"4","checked":"0"}]
         */

        private String rel_title;
        private List<RelTaoBean> rel_tao;

        public String getRel_title() {
            return rel_title;
        }

        public void setRel_title(String rel_title) {
            this.rel_title = rel_title;
        }

        public List<RelTaoBean> getRel_tao() {
            return rel_tao;
        }

        public void setRel_tao(List<RelTaoBean> rel_tao) {
            this.rel_tao = rel_tao;
        }

        public static class RelTaoBean {
            /**
             * tao_id : 70391
             * spe_name : 私密嫩红
             * status : 4
             * checked : 0
             */

            private String tao_id;
            private String spe_name;
            private String status;
            private String checked;
            private String img;
            private BasedataBean basedata;
            private MemberDataBean member_data;
            private List<ServiceBean> service;
            private GroupInfoBean groupInfo;
            private ProInfoBean proInfo;
            private RegInfoBean regInfo;
            private CouponPriceInfo couponPriceInfo;
            private HashMap<String,String> event_params;

            public String getTao_id() {
                return tao_id;
            }

            public void setTao_id(String tao_id) {
                this.tao_id = tao_id;
            }

            public String getSpe_name() {
                return spe_name;
            }

            public void setSpe_name(String spe_name) {
                this.spe_name = spe_name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getChecked() {
                return checked;
            }

            public void setChecked(String checked) {
                this.checked = checked;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public BasedataBean getBasedata() {
                return basedata;
            }

            public void setBasedata(BasedataBean basedata) {
                this.basedata = basedata;
            }

            public MemberDataBean getMember_data() {
                return member_data;
            }

            public void setMember_data(MemberDataBean member_data) {
                this.member_data = member_data;
            }

            public List<ServiceBean> getService() {
                return service;
            }

            public void setService(List<ServiceBean> service) {
                this.service = service;
            }

            public GroupInfoBean getGroupInfo() {
                return groupInfo;
            }

            public void setGroupInfo(GroupInfoBean groupInfo) {
                this.groupInfo = groupInfo;
            }

            public ProInfoBean getProInfo() {
                return proInfo;
            }

            public void setProInfo(ProInfoBean proInfo) {
                this.proInfo = proInfo;
            }

            public RegInfoBean getRegInfo() {
                return regInfo;
            }

            public void setRegInfo(RegInfoBean regInfo) {
                this.regInfo = regInfo;
            }

            public CouponPriceInfo getCouponPriceInfo() {
                return couponPriceInfo;
            }

            public void setCouponPriceInfo(CouponPriceInfo couponPriceInfo) {
                this.couponPriceInfo = couponPriceInfo;
            }

            public HashMap<String, String> getEvent_params() {
                return event_params;
            }

            public void setEvent_params(HashMap<String, String> event_params) {
                this.event_params = event_params;
            }
        }
    }

    public static class IteminfoBean {
        /**
         * cateid : 722
         * title : 女性私密
         */

        private String cateid;
        private String title;

        public String getCateid() {
            return cateid;
        }

        public void setCateid(String cateid) {
            this.cateid = cateid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class HosDocBean {
        /**
         * show_hospital : 1
         * doc_name : 张守玲
         * doc_user_id : 654589
         * doc_title : 副主任医师
         * doc_url : https://m.yuemei.com/dr/00654589/
         * hospital_name : 北京长虹医疗美容医院
         * hospital_id : 3379
         * hospital_address : 地址：北京市朝阳区东三环北路甲17号
         * kind : 2
         * hours :
         * hos_img : https://ymt.yuemei.com/upload/vip/120_120/15235817649584.jpg
         * phone : 4000567118
         * alert_title : 您将拨打客服电话
         * alert_subtitle : 客服咨询时间：周一至周日9:30-18:30
         * alert_button : 拨打客服电话
         */

        private String show_hospital;
        private String doc_name;
        private String doc_user_id;
        private String doc_title;
        private String doc_url;
        private String hospital_name;
        private String hospital_id;
        private String hospital_address;
        private String kind;
        private String hours;
        private String hos_img;
        private String doc_img;
        private String phone;
        private String alert_title;
        private String alert_subtitle;
        private String alert_button;
        private String zzrz;
        private String rateScale;
        private String distance;
        private List<String>hos_attr;
        private String doc_sku_order_num;
        private List<String>good_board;
        private String doc_score;
        private String doc_type;
        private ClickData addressClickData;
        private ClickData telClickData;
        private ClickData hospitalClickData;
        private ChatClickData chatClickData;
        private ClickData doctorClickData;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getDoc_type() {
            return doc_type;
        }

        public void setDoc_type(String doc_type) {
            this.doc_type = doc_type;
        }

        public String getDoc_sku_order_num() {
            return doc_sku_order_num;
        }

        public void setDoc_sku_order_num(String doc_sku_order_num) {
            this.doc_sku_order_num = doc_sku_order_num;
        }

        public List<String> getGood_board() {
            return good_board;
        }

        public void setGood_board(List<String> good_board) {
            this.good_board = good_board;
        }

        public String getDoc_score() {
            return doc_score;
        }

        public void setDoc_score(String doc_score) {
            this.doc_score = doc_score;
        }

        public List<String> getHos_attr() {
            return hos_attr;
        }

        public void setHos_attr(List<String> hos_attr) {
            this.hos_attr = hos_attr;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getDoc_user_id() {
            return doc_user_id;
        }

        public void setDoc_user_id(String doc_user_id) {
            this.doc_user_id = doc_user_id;
        }

        public String getDoc_title() {
            return doc_title;
        }

        public void setDoc_title(String doc_title) {
            this.doc_title = doc_title;
        }

        public String getDoc_url() {
            return doc_url;
        }

        public void setDoc_url(String doc_url) {
            this.doc_url = doc_url;
        }

        public String getHospital_name() {
            return hospital_name;
        }

        public void setHospital_name(String hospital_name) {
            this.hospital_name = hospital_name;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getHospital_address() {
            return hospital_address;
        }

        public void setHospital_address(String hospital_address) {
            this.hospital_address = hospital_address;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getHos_img() {
            return hos_img;
        }

        public void setHos_img(String hos_img) {
            this.hos_img = hos_img;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAlert_title() {
            return alert_title;
        }

        public void setAlert_title(String alert_title) {
            this.alert_title = alert_title;
        }

        public String getAlert_subtitle() {
            return alert_subtitle;
        }

        public void setAlert_subtitle(String alert_subtitle) {
            this.alert_subtitle = alert_subtitle;
        }

        public String getAlert_button() {
            return alert_button;
        }

        public void setAlert_button(String alert_button) {
            this.alert_button = alert_button;
        }

        public String getZzrz() {
            return zzrz;
        }

        public void setZzrz(String zzrz) {
            this.zzrz = zzrz;
        }

        public String getRateScale() {
            return rateScale;
        }

        public void setRateScale(String rateScale) {
            this.rateScale = rateScale;
        }

        public ClickData getAddressClickData() {
            return addressClickData;
        }

        public void setAddressClickData(ClickData addressClickData) {
            this.addressClickData = addressClickData;
        }

        public ClickData getTelClickData() {
            return telClickData;
        }

        public void setTelClickData(ClickData telClickData) {
            this.telClickData = telClickData;
        }

        public ClickData getHospitalClickData() {
            return hospitalClickData;
        }

        public void setHospitalClickData(ClickData hospitalClickData) {
            this.hospitalClickData = hospitalClickData;
        }

        public ChatClickData getChatClickData() {
            return chatClickData;
        }

        public void setChatClickData(ChatClickData chatClickData) {
            this.chatClickData = chatClickData;
        }

        public String getDoc_img() {
            return doc_img;
        }

        public void setDoc_img(String doc_img) {
            this.doc_img = doc_img;
        }

        public ClickData getDoctorClickData() {
            return doctorClickData;
        }

        public void setDoctorClickData(ClickData doctorClickData) {
            this.doctorClickData = doctorClickData;
        }
    }

    public static class ShareBean {
        /**
         * url_new : https://m.yuemei.com/tao/119749/?uu=DDJ4zwHrz9JE1cmm98ivKw%3D%3D
         * url : https://m.yuemei.com/tao/119749/
         * title : 【悦美】小阴唇整形手术
         * content : 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus 隐私保护 医师一对一 权威机构 品牌保证
         * img_weibo : https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg
         * img_weix : https://p34.yuemei.com/tao/2018/1118/200_200/jt181118101505_9117b1.jpg
         * Wechat : {"title":"【悦美】小阴唇整形手术 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus","description":"小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus 隐私保护 医师一对一 权威机构 品牌保证","webpageUrl":"https://m.yuemei.com/tao/119749/?uu=DDJ4zwHrz9JE1cmm98ivKw%3D%3D","path":"/pages/index/index?tao_id=119749&from_user_id=675727&type=4","thumbImage":"https://p34.yuemei.com/tao/2018/1118/200_200/jt181118101505_9117b1.jpg","userName":"gh_4ce0355fc6ab"}
         * Pictorial : {"img":"https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg","title":"【小阴唇整形手术】 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus","price_discount":"3500","price":"8800","order_num":"938人已预约","feeScale":"/次","hos_name":"北京长虹医疗美容医院","hos_rate":"5.0","hos_rateScale":"100","sun_img":"https://p11.yuemei.com/wx_small_sun/119749/119749.jpg"}
         */

        private String url_new;
        private String url;
        private String title;
        private String content;
        private String img_weibo;
        private String img_weix;
        private ShareWechat Wechat;
        private SharePictorial Pictorial;

        public String getUrl_new() {
            return url_new;
        }

        public void setUrl_new(String url_new) {
            this.url_new = url_new;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImg_weibo() {
            return img_weibo;
        }

        public void setImg_weibo(String img_weibo) {
            this.img_weibo = img_weibo;
        }

        public String getImg_weix() {
            return img_weix;
        }

        public void setImg_weix(String img_weix) {
            this.img_weix = img_weix;
        }

        public ShareWechat getWechat() {
            return Wechat;
        }

        public void setWechat(ShareWechat Wechat) {
            this.Wechat = Wechat;
        }

        public SharePictorial getPictorial() {
            return Pictorial;
        }

        public void setPictorial(SharePictorial Pictorial) {
            this.Pictorial = Pictorial;
        }

        public static class WechatBean {
            /**
             * title : 【悦美】小阴唇整形手术 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus
             * description : 小阴唇整形手术 阴唇肥大矫正 小阴唇矫正 私信预约专家赠送会员plus 隐私保护 医师一对一 权威机构 品牌保证
             * webpageUrl : https://m.yuemei.com/tao/119749/?uu=DDJ4zwHrz9JE1cmm98ivKw%3D%3D
             * path : /pages/index/index?tao_id=119749&from_user_id=675727&type=4
             * thumbImage : https://p34.yuemei.com/tao/2018/1118/200_200/jt181118101505_9117b1.jpg
             * userName : gh_4ce0355fc6ab
             */

            private String title;
            private String description;
            private String webpageUrl;
            private String path;
            private String thumbImage;
            private String userName;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getWebpageUrl() {
                return webpageUrl;
            }

            public void setWebpageUrl(String webpageUrl) {
                this.webpageUrl = webpageUrl;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public String getThumbImage() {
                return thumbImage;
            }

            public void setThumbImage(String thumbImage) {
                this.thumbImage = thumbImage;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }
        }

    }

    public static class ChatDataBean {
        /**
         * is_rongyun : 3
         * hos_userid : 85282559
         */

        private String is_rongyun;
        private String hos_userid;
        private String event_name;
        private String event_pos;
        private String ymaq_class;
        private String ymaq_id;
        private int obj_type;
        private String obj_id;
        private String group_id;
        private String show_message;
        private HashMap<String,String> event_params;
        private HashMap<String,String> guiji;

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public String getEvent_pos() {
            return event_pos;
        }

        public void setEvent_pos(String event_pos) {
            this.event_pos = event_pos;
        }

        public String getYmaq_class() {
            return ymaq_class;
        }

        public void setYmaq_class(String ymaq_class) {
            this.ymaq_class = ymaq_class;
        }

        public String getYmaq_id() {
            return ymaq_id;
        }

        public void setYmaq_id(String ymaq_id) {
            this.ymaq_id = ymaq_id;
        }

        public int getObj_type() {
            return obj_type;
        }

        public void setObj_type(int obj_type) {
            this.obj_type = obj_type;
        }

        public String getObj_id() {
            return obj_id;
        }

        public void setObj_id(String obj_id) {
            this.obj_id = obj_id;
        }

        public HashMap<String, String> getGuiji() {
            return guiji;
        }

        public void setGuiji(HashMap<String, String> guiji) {
            this.guiji = guiji;
        }

        public String getShow_message() {
            return show_message;
        }

        public void setShow_message(String show_message) {
            this.show_message = show_message;
        }

        public String getGroup_id() {
            return group_id;
        }

        public void setGroup_id(String group_id) {
            this.group_id = group_id;
        }
    }

    public static class PayPriceBean {
        /**
         * dingjin : 670
         * is_order : 3
         * hos_price : 2655
         * payPrice : 3325
         * discountPrice : 0
         * is_member : 1
         */

        private String dingjin;
        private String is_order;
        private String hos_price;
        private String payPrice;
        private String discountPrice;
        private String is_member;

        public String getDingjin() {
            return dingjin;
        }

        public void setDingjin(String dingjin) {
            this.dingjin = dingjin;
        }

        public String getIs_order() {
            return is_order;
        }

        public void setIs_order(String is_order) {
            this.is_order = is_order;
        }

        public String getHos_price() {
            return hos_price;
        }

        public void setHos_price(String hos_price) {
            this.hos_price = hos_price;
        }

        public String getPayPrice() {
            return payPrice;
        }

        public void setPayPrice(String payPrice) {
            this.payPrice = payPrice;
        }

        public String getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(String discountPrice) {
            this.discountPrice = discountPrice;
        }

        public String getIs_member() {
            return is_member;
        }

        public void setIs_member(String is_member) {
            this.is_member = is_member;
        }
    }

    public static class PicBean {
        /**
         * img : https://p34.yuemei.com/tao/2018/1118/jt181118101505_9117b1.jpg
         */

        private String img;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }

    public static class VideoBean {
        /**
         * img : https://p22.yuemei.com/postimg/20181018/500_500/20181018192409_2308e6.jpg
         * url : https://player.yuemei.com/video/6ea2/201810/c3e214deeefe3b46774b07b9acc8495a.mp4
         */

        private String img;
        private String url;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class ServiceBean {
        /**
         * imgname : YMTaoAnytimeRefund
         * title : 未消费随时退
         */

        private String imgname;
        private String title;

        public String getImgname() {
            return imgname;
        }

        public void setImgname(String imgname) {
            this.imgname = imgname;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class CouponsBean {
        /**
         * title : 订金满减：
         * data : ["满20000减5000","满20000减5000"]
         * num : 2个
         */

        private String title;
        private String num;
        private List<String> data;
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class CommentBean {
        /**
         * show_type : text
         * title : 口碑评价5.0分
         * data : [{"userdata":{"name":"听一半的歌曲","id":"85606045","avatar":"https://p21.yuemei.com/avatar/085/60/60/45_avatar_50_50.jpg","talent":"0","group_id":"1","lable":"北京 11-23","url":"https://m.yuemei.com/u/home/85606045/","obj_id":"85606045","obj_type":"6","is_following":"0"},"rateSale":"","appmurl":"https://m.yuemei.com/c/2501452.html","title":"连续上了１５天的班，好累，好乏，也没来给你们更新，跟你们说哈，做小阴唇手术之后，这鬼真是越来越那个了，还时不时的夸我，做小阴唇手术真的是很好的选择，感觉他就是粘合剂，给爱情的另一种行为增添了几分激情，也让他越来越爱我，越来越宠着我，做了之后改变的并不是性生活的提高，更多改变的是两个人的心情，真是太感谢长虹的医生了，把我之前的主动改变带哦现在的被动，跟你们说最近下班一直在追一部剧\u201c我和你的倾城时光\u201d强烈推荐非常好看的一部电视剧，我已经迷的不要不要的了，我两天时间从第１集追到第１８集，吃饭看，早晨起来上班化妆的时间也不能闲着，一直都很喜欢兵哥哥，金瀚真的是太帅了，这果断关注他围脖，等着他翻牌子，诶呦，说的我都不好意思了，嘘，别让我男朋友看见，看见了不好"}]
         * linkdata : {"urltitle":"查看全部 (371)","url":"type:eq:6550:and:sort:eq:1:and:flag:eq:0"}
         */

        private String show_type;
        private String title;
        private LinkdataBean linkdata;
        private List<DataBean> data;

        public String getShow_type() {
            return show_type;
        }

        public void setShow_type(String show_type) {
            this.show_type = show_type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public LinkdataBean getLinkdata() {
            return linkdata;
        }

        public void setLinkdata(LinkdataBean linkdata) {
            this.linkdata = linkdata;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class LinkdataBean {
            /**
             * urltitle : 查看全部 (371)
             * url : type:eq:6550:and:sort:eq:1:and:flag:eq:0
             */

            private String urltitle;
            private String url;

            public String getUrltitle() {
                return urltitle;
            }

            public void setUrltitle(String urltitle) {
                this.urltitle = urltitle;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        public static class DataBean {
            /**
             * userdata : {"name":"听一半的歌曲","id":"85606045","avatar":"https://p21.yuemei.com/avatar/085/60/60/45_avatar_50_50.jpg","talent":"0","group_id":"1","lable":"北京 11-23","url":"https://m.yuemei.com/u/home/85606045/","obj_id":"85606045","obj_type":"6","is_following":"0"}
             * rateSale :
             * appmurl : https://m.yuemei.com/c/2501452.html
             * title : 连续上了１５天的班，好累，好乏，也没来给你们更新，跟你们说哈，做小阴唇手术之后，这鬼真是越来越那个了，还时不时的夸我，做小阴唇手术真的是很好的选择，感觉他就是粘合剂，给爱情的另一种行为增添了几分激情，也让他越来越爱我，越来越宠着我，做了之后改变的并不是性生活的提高，更多改变的是两个人的心情，真是太感谢长虹的医生了，把我之前的主动改变带哦现在的被动，跟你们说最近下班一直在追一部剧“我和你的倾城时光”强烈推荐非常好看的一部电视剧，我已经迷的不要不要的了，我两天时间从第１集追到第１８集，吃饭看，早晨起来上班化妆的时间也不能闲着，一直都很喜欢兵哥哥，金瀚真的是太帅了，这果断关注他围脖，等着他翻牌子，诶呦，说的我都不好意思了，嘘，别让我男朋友看见，看见了不好
             */

            private UserdataBean userdata;
            private String rateSale;
            private String appmurl;
            private String title;
            private String img;

            public UserdataBean getUserdata() {
                return userdata;
            }

            public void setUserdata(UserdataBean userdata) {
                this.userdata = userdata;
            }

            public String getRateSale() {
                return rateSale;
            }

            public void setRateSale(String rateSale) {
                this.rateSale = rateSale;
            }

            public String getAppmurl() {
                return appmurl;
            }

            public void setAppmurl(String appmurl) {
                this.appmurl = appmurl;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public static class UserdataBean {
                /**
                 * name : 听一半的歌曲
                 * id : 85606045
                 * avatar : https://p21.yuemei.com/avatar/085/60/60/45_avatar_50_50.jpg
                 * talent : 0
                 * group_id : 1
                 * lable : 北京 11-23
                 * url : https://m.yuemei.com/u/home/85606045/
                 * obj_id : 85606045
                 * obj_type : 6
                 * is_following : 0
                 */

                private String name;
                private String id;
                private String avatar;
                private String talent;
                private String group_id;
                private String lable;
                private String url;
                private String obj_id;
                private String obj_type;
                private String is_following;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public String getTalent() {
                    return talent;
                }

                public void setTalent(String talent) {
                    this.talent = talent;
                }

                public String getGroup_id() {
                    return group_id;
                }

                public void setGroup_id(String group_id) {
                    this.group_id = group_id;
                }

                public String getLable() {
                    return lable;
                }

                public void setLable(String lable) {
                    this.lable = lable;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getObj_id() {
                    return obj_id;
                }

                public void setObj_id(String obj_id) {
                    this.obj_id = obj_id;
                }

                public String getObj_type() {
                    return obj_type;
                }

                public void setObj_type(String obj_type) {
                    this.obj_type = obj_type;
                }

                public String getIs_following() {
                    return is_following;
                }

                public void setIs_following(String is_following) {
                    this.is_following = is_following;
                }
            }
        }
    }

    public static class TaoAskBean {
        /**
         * show_type : text
         * title : 有问题，问大家
         * data : [{"question":"请问做过小阴唇手术的姑娘们手术期间会疼吗之后有不适或者其它不良反应吗？多谢啦","answer":"没有除了打麻醉又点疼可以忍受","answer_num":"1","question_num":"1"}]
         * linkdata : {"urltitle":"查看全部 (1)","url":"type:eq:6551"}
         */

        private String show_type;
        private String title;
        private LinkdataBeanX linkdata;
        private List<DataBeanX> data;

        public String getShow_type() {
            return show_type;
        }

        public void setShow_type(String show_type) {
            this.show_type = show_type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public LinkdataBeanX getLinkdata() {
            return linkdata;
        }

        public void setLinkdata(LinkdataBeanX linkdata) {
            this.linkdata = linkdata;
        }

        public List<DataBeanX> getData() {
            return data;
        }

        public void setData(List<DataBeanX> data) {
            this.data = data;
        }

        public static class LinkdataBeanX {
            /**
             * urltitle : 查看全部 (1)
             * url : type:eq:6551
             */

            private String urltitle;
            private String url;

            public String getUrltitle() {
                return urltitle;
            }

            public void setUrltitle(String urltitle) {
                this.urltitle = urltitle;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

        public static class DataBeanX {
            /**
             * question : 请问做过小阴唇手术的姑娘们手术期间会疼吗之后有不适或者其它不良反应吗？多谢啦
             * answer : 没有除了打麻醉又点疼可以忍受
             * answer_num : 1
             * question_num : 1
             */

            private String question;
            private String answer;
            private String answer_num;
            private String question_num;

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getAnswer() {
                return answer;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }

            public String getAnswer_num() {
                return answer_num;
            }

            public void setAnswer_num(String answer_num) {
                this.answer_num = answer_num;
            }

            public String getQuestion_num() {
                return question_num;
            }

            public void setQuestion_num(String question_num) {
                this.question_num = question_num;
            }
        }
    }

    public static class AlertBean {
        /**
         * img : https://p24.yuemei.com/tao/2018/1129/200_200/jt181129140454_613690.jpg
         * title : 有果酸焕肤新订单，来自悦**3
         */

        private String img;
        private String title;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
