package com.quicklyask.entity;

/**
 * Created by 裴成浩 on 2017/6/20.
 */

public class ShouKuan {


    /**
     * code : 1
     * message : 保存成功
     * data : {"alipay":"*30*30*994***233*7","real_name":"***","card_id":"130************317"}
     */

    private String code;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * alipay : *30*30*994***233*7
         * real_name : ***
         * card_id : 130************317
         */

        private String alipay;
        private String real_name;
        private String card_id;

        public String getAlipay() {
            return alipay;
        }

        public void setAlipay(String alipay) {
            this.alipay = alipay;
        }

        public String getReal_name() {
            return real_name;
        }

        public void setReal_name(String real_name) {
            this.real_name = real_name;
        }

        public String getCard_id() {
            return card_id;
        }

        public void setCard_id(String card_id) {
            this.card_id = card_id;
        }
    }
}
