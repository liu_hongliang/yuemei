package com.quicklyask.entity;

public class OrderInfoData {


	/**
	 * server_id : 73564002
	 * title : 测试淘
	 * status : 2
	 * status_title : 待销费
	 * goods_id : 1198042
	 * is_refund : 1
	 * pay_total_fee : 0.00
	 * balance_pay_money : 4.77
	 */

	private String server_id;
	private String title;
	private String status;
	private String status_title;
	private String goods_id;
	private String is_refund;//是否能退款 1能
	private String pay_total_fee;//三方支付金额
	private String balance_pay_money;//	钱包支付金额
	private boolean ischecked;


	public String getServer_id() {
		return server_id;
	}

	public void setServer_id(String server_id) {
		this.server_id = server_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus_title() {
		return status_title;
	}

	public void setStatus_title(String status_title) {
		this.status_title = status_title;
	}

	public String getGoods_id() {
		return goods_id;
	}

	public void setGoods_id(String goods_id) {
		this.goods_id = goods_id;
	}

	public String getIs_refund() {
		return is_refund;
	}

	public void setIs_refund(String is_refund) {
		this.is_refund = is_refund;
	}

	public String getPay_total_fee() {
		return pay_total_fee;
	}

	public void setPay_total_fee(String pay_total_fee) {
		this.pay_total_fee = pay_total_fee;
	}

	public String getBalance_pay_money() {
		return balance_pay_money;
	}

	public void setBalance_pay_money(String balance_pay_money) {
		this.balance_pay_money = balance_pay_money;
	}

	public boolean isIschecked() {
		return ischecked;
	}

	public void setIschecked(boolean ischecked) {
		this.ischecked = ischecked;
	}
}
