/**
 * 
 */
package com.quicklyask.entity;

import java.util.List;

/**
 * 淘整形收藏
 * 
 * @author Rubin
 * 
 */
public class TaoCollect {
	private String code;
	private String message;
	private List<TaoCollectData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<TaoCollectData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<TaoCollectData> data) {
		this.data = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaoCollect [code=" + code + ", message=" + message + ", data="
				+ data + "]";
	}

}
