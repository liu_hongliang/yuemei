package com.quicklyask.entity;

import com.module.home.model.bean.SearchTitleData;

/**
 * Created by dwb on 16/12/6.
 */
public class SearchTitle {

    private String code;
    private String message;
    private SearchTitleData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SearchTitleData getData() {
        return data;
    }

    public void setData(SearchTitleData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SearchTitle{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
