package com.quicklyask.entity;


import com.module.doctor.model.bean.TaoPopItemIvData;

import java.util.List;

public class TaoPopItemIv {

	private String code;
	private String message;
	private List<TaoPopItemIvData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<TaoPopItemIvData> getData() {
		return data;
	}

	public void setData(List<TaoPopItemIvData> data) {
		this.data = data;
	}



}
