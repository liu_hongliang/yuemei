package com.quicklyask.entity;

/**
 * Created by dwb on 16/10/19.
 */
public class DataUrl {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
