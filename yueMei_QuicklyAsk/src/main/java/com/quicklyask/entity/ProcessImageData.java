package com.quicklyask.entity;

import com.quicklyask.view.ProcessImageView;

/**
 * Created by 裴成浩 on 2017/6/21.
 */

public class ProcessImageData {
    private ProcessImageView processImage;      //当前图片
    private String imgUrl;

    public ProcessImageData(){

    }

    public ProcessImageView getProcessImage() {
        return processImage;
    }

    public void setProcessImage(ProcessImageView processImage) {
        this.processImage = processImage;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
