package com.quicklyask.entity;

import com.module.community.model.bean.ZhuanTi;
import com.module.my.model.bean.RecoveryReminder;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dwb on 16/6/30.
 */
public class AdertAdv {

    private ZhuanTi alert;
    private ZhuanTi adv;
    private StartTabbar tabbar;
    private SneakGuest sneakGuest;
    private String foramSetType;
    private String loadDoctorHtml;
    private RecoveryReminder recoveryReminder;

    private SecondFloorBean secondFloor;
    private OtherBackgroundImgBean otherBackgroundImg;
    private List<BigPromotionBallBean> bigPromotionBall;


    public ZhuanTi getAlert() {
        return alert;
    }

    public void setAlert(ZhuanTi alert) {
        this.alert = alert;
    }

    public ZhuanTi getAdv() {
        return adv;
    }

    public void setAdv(ZhuanTi adv) {
        this.adv = adv;
    }

    public StartTabbar getTabbar() {
        return tabbar;
    }

    public void setTabbar(StartTabbar tabbar) {
        this.tabbar = tabbar;
    }

    public SneakGuest getSneakGuest() {
        return sneakGuest;
    }

    public void setSneakGuest(SneakGuest sneakGuest) {
        this.sneakGuest = sneakGuest;
    }

    public String getForamSetType() {
        return foramSetType;
    }

    public void setForamSetType(String foramSetType) {
        this.foramSetType = foramSetType;
    }

    public String getLoadDoctorHtml() {
        return loadDoctorHtml;
    }

    public void setLoadDoctorHtml(String loadDoctorHtml) {
        this.loadDoctorHtml = loadDoctorHtml;
    }

    public RecoveryReminder getRecoveryReminder() {
        return recoveryReminder;
    }

    public void setRecoveryReminder(RecoveryReminder recoveryReminder) {
        this.recoveryReminder = recoveryReminder;
    }

    public SecondFloorBean getSecondFloor() {
        return secondFloor;
    }

    public void setSecondFloor(SecondFloorBean secondFloor) {
        this.secondFloor = secondFloor;
    }

    public OtherBackgroundImgBean getOtherBackgroundImg() {
        return otherBackgroundImg;
    }

    public void setOtherBackgroundImg(OtherBackgroundImgBean otherBackgroundImg) {
        this.otherBackgroundImg = otherBackgroundImg;
    }

    public List<BigPromotionBallBean> getBigPromotionBall() {
        return bigPromotionBall;
    }

    public void setBigPromotionBall(List<BigPromotionBallBean> bigPromotionBall) {
        this.bigPromotionBall = bigPromotionBall;
    }


    public static class SecondFloorBean {
        /**
         * loadingImg : {"bmsid":"507454","metro_type":"0","qid":"0","url":"","flag":"0","img":"https://p11.yuemei.com/tag/1580806123bf971.jpg","img_new":"","title":"悦美","type":"0","metro_line":"0","h_w":"1.3333333333333","desc":"","event_params":{"to_page_type":0,"to_page_id":0}}
         * placeholderImg : {"bmsid":"507454","metro_type":"0","qid":"0","url":"","flag":"0","img":"https://p11.yuemei.com/tag/1580806331a1910.jpg","img_new":"","title":"悦美","type":"0","metro_line":"0","h_w":"1.3333333333333","desc":"","event_params":{"to_page_type":0,"to_page_id":0}}
         * loadData : {"bmsid":"507455","metro_type":"0","qid":"8584","url":"https://m.yuemei.com/tao_zt/8584.html","flag":"0","img":"","img_new":"","title":"悦美","type":"0","metro_line":"0","h_w":"0","desc":"","event_params":{"to_page_type":16,"to_page_id":8584}}
         */

        private LoadingImgBean loadingImg;
        private PlaceholderImgBean placeholderImg;
        private LoadDataBean loadData;

        public LoadingImgBean getLoadingImg() {
            return loadingImg;
        }

        public void setLoadingImg(LoadingImgBean loadingImg) {
            this.loadingImg = loadingImg;
        }

        public PlaceholderImgBean getPlaceholderImg() {
            return placeholderImg;
        }

        public void setPlaceholderImg(PlaceholderImgBean placeholderImg) {
            this.placeholderImg = placeholderImg;
        }

        public LoadDataBean getLoadData() {
            return loadData;
        }

        public void setLoadData(LoadDataBean loadData) {
            this.loadData = loadData;
        }

        public static class LoadingImgBean {
            /**
             * bmsid : 507454
             * metro_type : 0
             * qid : 0
             * url :
             * flag : 0
             * img : https://p11.yuemei.com/tag/1580806123bf971.jpg
             * img_new :
             * title : 悦美
             * type : 0
             * metro_line : 0
             * h_w : 1.3333333333333
             * desc :
             * event_params : {"to_page_type":0,"to_page_id":0}
             */

            private String bmsid;
            private String metro_type;
            private String qid;
            private String url;
            private String flag;
            private String img;
            private String img_new;
            private String title;
            private String type;
            private String metro_line;
            private String h_w;
            private String desc;
            private EventParamsBeanXXX event_params;

            public String getBmsid() {
                return bmsid;
            }

            public void setBmsid(String bmsid) {
                this.bmsid = bmsid;
            }

            public String getMetro_type() {
                return metro_type;
            }

            public void setMetro_type(String metro_type) {
                this.metro_type = metro_type;
            }

            public String getQid() {
                return qid;
            }

            public void setQid(String qid) {
                this.qid = qid;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getImg_new() {
                return img_new;
            }

            public void setImg_new(String img_new) {
                this.img_new = img_new;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getMetro_line() {
                return metro_line;
            }

            public void setMetro_line(String metro_line) {
                this.metro_line = metro_line;
            }

            public String getH_w() {
                return h_w;
            }

            public void setH_w(String h_w) {
                this.h_w = h_w;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public EventParamsBeanXXX getEvent_params() {
                return event_params;
            }

            public void setEvent_params(EventParamsBeanXXX event_params) {
                this.event_params = event_params;
            }

            public static class EventParamsBeanXXX {
                /**
                 * to_page_type : 0
                 * to_page_id : 0
                 */

                private int to_page_type;
                private int to_page_id;

                public int getTo_page_type() {
                    return to_page_type;
                }

                public void setTo_page_type(int to_page_type) {
                    this.to_page_type = to_page_type;
                }

                public int getTo_page_id() {
                    return to_page_id;
                }

                public void setTo_page_id(int to_page_id) {
                    this.to_page_id = to_page_id;
                }
            }
        }

        public static class PlaceholderImgBean {
            /**
             * bmsid : 507454
             * metro_type : 0
             * qid : 0
             * url :
             * flag : 0
             * img : https://p11.yuemei.com/tag/1580806331a1910.jpg
             * img_new :
             * title : 悦美
             * type : 0
             * metro_line : 0
             * h_w : 1.3333333333333
             * desc :
             * event_params : {"to_page_type":0,"to_page_id":0}
             */

            private String bmsid;
            private String metro_type;
            private String qid;
            private String url;
            private String flag;
            private String img;
            private String img_new;
            private String title;
            private String type;
            private String metro_line;
            private String h_w;
            private String desc;
            private EventParamsBeanXXXX event_params;

            public String getBmsid() {
                return bmsid;
            }

            public void setBmsid(String bmsid) {
                this.bmsid = bmsid;
            }

            public String getMetro_type() {
                return metro_type;
            }

            public void setMetro_type(String metro_type) {
                this.metro_type = metro_type;
            }

            public String getQid() {
                return qid;
            }

            public void setQid(String qid) {
                this.qid = qid;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getImg_new() {
                return img_new;
            }

            public void setImg_new(String img_new) {
                this.img_new = img_new;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getMetro_line() {
                return metro_line;
            }

            public void setMetro_line(String metro_line) {
                this.metro_line = metro_line;
            }

            public String getH_w() {
                return h_w;
            }

            public void setH_w(String h_w) {
                this.h_w = h_w;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public EventParamsBeanXXXX getEvent_params() {
                return event_params;
            }

            public void setEvent_params(EventParamsBeanXXXX event_params) {
                this.event_params = event_params;
            }

            public static class EventParamsBeanXXXX {
                /**
                 * to_page_type : 0
                 * to_page_id : 0
                 */

                private int to_page_type;
                private int to_page_id;

                public int getTo_page_type() {
                    return to_page_type;
                }

                public void setTo_page_type(int to_page_type) {
                    this.to_page_type = to_page_type;
                }

                public int getTo_page_id() {
                    return to_page_id;
                }

                public void setTo_page_id(int to_page_id) {
                    this.to_page_id = to_page_id;
                }
            }
        }

        public static class LoadDataBean {
            /**
             * bmsid : 507455
             * metro_type : 0
             * qid : 8584
             * url : https://m.yuemei.com/tao_zt/8584.html
             * flag : 0
             * img :
             * img_new :
             * title : 悦美
             * type : 0
             * metro_line : 0
             * h_w : 0
             * desc :
             * event_params : {"to_page_type":16,"to_page_id":8584}
             */

            private String bmsid;
            private String metro_type;
            private String qid;
            private String url;
            private String flag;
            private String img;
            private String img_new;
            private String title;
            private String type;
            private String metro_line;
            private String h_w;
            private String desc;
            private HashMap<String, String> event_params;
            private String location_url;

            public String getLocation_url() {
                return location_url;
            }

            public void setLocation_url(String location_url) {
                this.location_url = location_url;
            }

            public String getBmsid() {
                return bmsid;
            }

            public void setBmsid(String bmsid) {
                this.bmsid = bmsid;
            }

            public String getMetro_type() {
                return metro_type;
            }

            public void setMetro_type(String metro_type) {
                this.metro_type = metro_type;
            }

            public String getQid() {
                return qid;
            }

            public void setQid(String qid) {
                this.qid = qid;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getFlag() {
                return flag;
            }

            public void setFlag(String flag) {
                this.flag = flag;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getImg_new() {
                return img_new;
            }

            public void setImg_new(String img_new) {
                this.img_new = img_new;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getMetro_line() {
                return metro_line;
            }

            public void setMetro_line(String metro_line) {
                this.metro_line = metro_line;
            }

            public String getH_w() {
                return h_w;
            }

            public void setH_w(String h_w) {
                this.h_w = h_w;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public HashMap<String, String> getEvent_params() {
                return event_params;
            }

            public void setEvent_params(HashMap<String, String> event_params) {
                this.event_params = event_params;
            }
//            public EventParamsBeanXXXXX getEvent_params() {
//                return event_params;
//            }
//
//            public void setEvent_params(EventParamsBeanXXXXX event_params) {
//                this.event_params = event_params;
//            }

            public static class EventParamsBeanXXXXX {
                /**
                 * to_page_type : 16
                 * to_page_id : 8584
                 */

                private int to_page_type;
                private int to_page_id;

                public int getTo_page_type() {
                    return to_page_type;
                }

                public void setTo_page_type(int to_page_type) {
                    this.to_page_type = to_page_type;
                }

                public int getTo_page_id() {
                    return to_page_id;
                }

                public void setTo_page_id(int to_page_id) {
                    this.to_page_id = to_page_id;
                }
            }
        }
    }

    public static class OtherBackgroundImgBean {
        /**
         * bmsid : 507459
         * metro_type : 0
         * qid : 0
         * url :
         * flag : 0
         * img : https://p11.yuemei.com/tag/1580811397cdb69.jpg
         * img_new : https://p11.yuemei.com/tag/1580811397cdb69.jpg
         * title : 悦美
         * type : 0
         * metro_line : 0
         * h_w : 0.72
         * desc :
         * event_params : {"to_page_type":0,"to_page_id":0}
         */

        private String bmsid;
        private String metro_type;
        private String qid;
        private String url;
        private String flag;
        private String img;
        private String img_new;
        private String title;
        private String type;
        private String metro_line;
        private String h_w;
        private String desc;
        private EventParamsBeanXXXXXX event_params;

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getMetro_type() {
            return metro_type;
        }

        public void setMetro_type(String metro_type) {
            this.metro_type = metro_type;
        }

        public String getQid() {
            return qid;
        }

        public void setQid(String qid) {
            this.qid = qid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg_new() {
            return img_new;
        }

        public void setImg_new(String img_new) {
            this.img_new = img_new;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMetro_line() {
            return metro_line;
        }

        public void setMetro_line(String metro_line) {
            this.metro_line = metro_line;
        }

        public String getH_w() {
            return h_w;
        }

        public void setH_w(String h_w) {
            this.h_w = h_w;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public EventParamsBeanXXXXXX getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBeanXXXXXX event_params) {
            this.event_params = event_params;
        }

        public static class EventParamsBeanXXXXXX {
            /**
             * to_page_type : 0
             * to_page_id : 0
             */

            private int to_page_type;
            private int to_page_id;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }
        }
    }

    public static class BigPromotionBallBean {
        /**
         * bmsid : 507664
         * metro_type : 0
         * qid : 8584
         * url : https://m.yuemei.com/tao_zt/8584.html
         * flag : 0
         * img : https://p11.yuemei.com/tag/15809731861a153.png
         * img_new : https://p11.yuemei.com/tag/15809731861a153.png
         * title : 悦美
         * type : 0
         * metro_line : 0
         * h_w : 0.3768115942029
         * desc :
         * event_params : {"to_page_type":16,"to_page_id":8584}
         */

        private String bmsid;
        private String metro_type;
        private String qid;
        private String url;
        private String flag;
        private String img;
        private String img_new;
        private String title;
        private String type;
        private String metro_line;
        private String h_w;
        private String desc;
        private HashMap<String, String> event_params;

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getMetro_type() {
            return metro_type;
        }

        public void setMetro_type(String metro_type) {
            this.metro_type = metro_type;
        }

        public String getQid() {
            return qid;
        }

        public void setQid(String qid) {
            this.qid = qid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg_new() {
            return img_new;
        }

        public void setImg_new(String img_new) {
            this.img_new = img_new;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMetro_line() {
            return metro_line;
        }

        public void setMetro_line(String metro_line) {
            this.metro_line = metro_line;
        }

        public String getH_w() {
            return h_w;
        }

        public void setH_w(String h_w) {
            this.h_w = h_w;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }

        public static class EventParamsBeanXXXXXXX {
            /**
             * to_page_type : 16
             * to_page_id : 8584
             */

            private int to_page_type;
            private int to_page_id;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }
        }
    }
}
