package com.quicklyask.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * IO流工具
 * 
 * @author Administrator
 * 
 */
public class IOUtil {

	/**
	 * 根据输入流获得字节数组
	 * 
	 * @param
	 *            is
	 * @param   ，则需要指定。如果不知道需填入0
	 * @return
	 */
	public static byte[] getBytes(InputStream is) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		int len = -1;
		byte[] bytes = new byte[1024 * 8];
		try {
			while ((len = is.read(bytes)) != -1) {
				bos.write(bytes, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close(); // 关闭流
					is = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bos.toByteArray();
	}
}
