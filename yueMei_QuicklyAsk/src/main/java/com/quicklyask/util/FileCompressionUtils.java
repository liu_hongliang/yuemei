package com.quicklyask.util;

import android.os.Environment;

import com.quicklyack.photo.FileUtils;

import java.io.File;
import java.util.Random;

/**
 * 图片压缩
 * Created by 裴成浩 on 2018/8/9.
 */
public class FileCompressionUtils {

    private static String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    /**
     * 压缩图片
     */
    public static File compressionFile(String path) {
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);               // 建立这个路径的文件或文件夹
        if (!path1.exists()) {                      // 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, imageNameRules());

        // 压缩图片
        FileUtils.compressPicture(path, file.getPath());

        return file;
    }

    /**
     * 图片名字不重复规则
     *
     * @return
     */
    private static String imageNameRules() {
        Random rand = new Random();
        String chs = "";
        for (int i = 0; i < 5; i++) {
            chs = chs + letters[rand.nextInt(letters.length)];
        }
        return "yuemei_" + chs + System.currentTimeMillis() + ".jpg";
    }
}
