package com.quicklyask.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.module.doctor.model.bean.RecentVisitCityData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 轻型的数据存储方式工具
 *
 * @author Administrator
 */
public class Cfg {

    private static SharedPreferences mSharedPreferences;
    private static String FILE_NAME = "com.quicklyask.activity";

    public static String loadStr(Context ctx, String key, String defStr) {
        if (mSharedPreferences == null) {
            mSharedPreferences = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        String value = mSharedPreferences.getString(key, defStr);
        return value;
    }

    public static void saveStr(Context ctx, String key, String value) {
        if (mSharedPreferences == null) {
            mSharedPreferences = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        //记录最近定位城市
        if (key.equals("dw_city") && !value.equals("失败") && !value.equals(Cfg.loadStr(ctx, "location_city", ""))) {
            RecentVisitCityData recentVisitCityData = new RecentVisitCityData();
            recentVisitCityData.setName(value);
            if (!TextUtils.isEmpty(Cfg.loadStr(ctx, "recent_visit_city", ""))) {
                ArrayList<RecentVisitCityData> list = JSONUtil.jsonToArrayList(Cfg.loadStr(ctx, "recent_visit_city", ""), RecentVisitCityData.class);
                if (list != null && list.size() > 0) {
                    if (!list.get(list.size() - 1).getName().equals(value)) {
                        list.add(recentVisitCityData);
                    }
                } else {
                    list.add(recentVisitCityData);
                }
                Cfg.saveStr(ctx, "recent_visit_city", new Gson().toJson(list));
            } else {
                ArrayList<RecentVisitCityData> list = new ArrayList<>();
                list.add(recentVisitCityData);
                Cfg.saveStr(ctx, "recent_visit_city", new Gson().toJson(list));
            }

        }
        mSharedPreferences.edit().putString(key, value).apply();
    }

    public static int loadInt(Context ctx, String key, int defVal) {
        if (mSharedPreferences == null) {
            mSharedPreferences = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        return mSharedPreferences.getInt(key, defVal);

    }

    public static void saveInt(Context ctx, String key, int value) {

        if (mSharedPreferences == null) {
            mSharedPreferences = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        mSharedPreferences.edit().putInt(key, value).apply();

    }

    @SuppressLint("CommitPrefEdits")
    public static void clear(Context ctx) {
        if (mSharedPreferences == null) {
            mSharedPreferences = ctx.getSharedPreferences("config", Context.MODE_PRIVATE);
        }
        mSharedPreferences.edit().clear();
    }

    public static void putHashMapData(Context context, String key, Map<String, String> datas) {
        JSONArray mJsonArray = new JSONArray();
        Iterator<Map.Entry<String, String>> iterator = datas.entrySet().iterator();

        JSONObject object = new JSONObject();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            try {
                object.put(entry.getKey(), entry.getValue());
            } catch (JSONException e) {

            }
        }
        mJsonArray.put(object);

        SharedPreferences sp = context.getSharedPreferences("config",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, mJsonArray.toString());
        editor.apply();
    }

    public static Map<String, String> getHashMapData(Context context, String key) {

        Map<String, String> datas = new HashMap<>();
        SharedPreferences sp = context.getSharedPreferences("config",
                Context.MODE_PRIVATE);
        String result = sp.getString(key, "");
        try {
            JSONArray array = new JSONArray(result);
            for (int i = 0; i < array.length(); i++) {
                JSONObject itemObject = array.getJSONObject(i);
                JSONArray names = itemObject.names();
                if (names != null) {
                    for (int j = 0; j < names.length(); j++) {
                        String name = names.getString(j);
                        String value = itemObject.getString(name);
                        datas.put(name, value);
                    }
                }
            }
        } catch (JSONException e) {

        }

        return datas;
    }


}
