package com.quicklyask.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.ishumei.smantifraud.SmAntiFraud;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.module.MainTableActivity;
import com.module.MyApplication;
import com.module.SplashActivity;
import com.module.api.GetInfoApi;
import com.module.api.ZTRecordHttpApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.my.controller.activity.BindingPhoneActivity;
import com.module.my.controller.other.AutoLoginControler;
import com.module.my.model.api.AutoBindApi;
import com.module.my.model.api.JPushBindApi;
import com.module.my.model.api.JPushClosedApi;
import com.module.my.model.bean.UserData;
import com.module.other.module.api.GetCityOneToHttpApi;
import com.module.other.module.bean.SessionidData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.api.CartSkuNumberApi;
import com.module.shopping.model.bean.CartSkuNumberData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.BuildConfig;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.view.MyToast;
import com.tendcloud.appcpa.TalkingDataAppCpa;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.jiguang.verifysdk.api.AuthPageEventListener;
import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.JVerifyUIClickCallback;
import cn.jiguang.verifysdk.api.JVerifyUIConfig;
import cn.jiguang.verifysdk.api.VerifyListener;
import cn.jpush.android.api.JPushInterface;
import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;

import static com.quicklyask.util.Cfg.loadStr;
import static com.umeng.socialize.utils.ContextUtil.getPackageName;

public class Utils {

    public static final String TAG = "BaseUtils";
    public static String logStringCache = "";
    public static final String BAIDU_PUSHE_KEY = "GT3kpgOdhNOdyF3GdymhId0C";

    public static String userId = "";
    public static String channelId = "";


    // 用share preference来实现是否绑定的开关。在ionBind且成功时设置true，unBind且成功时设置false
    public static boolean hasBind(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String flag = sp.getString("bind_flag", "");
        return "ok".equalsIgnoreCase(flag);
    }

    public static void setBind(Context context, boolean flag) {
        String flagStr = "not";
        if (flag) {
            flagStr = "ok";
        }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sp.edit();
        editor.putString("bind_flag", flagStr);
        editor.commit();
    }

    public static String getLogText(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString("log_text", "");
    }


    /**
     * 手机号验证
     *
     * @param str
     * @return 验证通过返回true
     */

    public static boolean isMobile(String str) {

        if (str.length() == 11) {
            Pattern p = null;
            Matcher m = null;
            boolean b = false;
            p = Pattern.compile("[0-9]+"); // 验证手机号
            m = p.matcher(str);
            b = m.matches();
            return b;
        } else {
            return false;
        }

    }

    /**
     * 验证输入的邮箱格式是否符合
     *
     * @param email
     * @return 是否合法
     */
    public static boolean emailFormat(String email) {
        boolean tag = true;
        final String pattern1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        final Pattern pattern = Pattern.compile(pattern1);
        final Matcher mat = pattern.matcher(email);
        if (!mat.find()) {
            tag = false;
        }
        return tag;
    }

    /**
     * 验证非汉字、字母、数字、下划线
     *
     * @param str
     * @return
     */
    public static boolean isStringFormatCorrect(String str) {
        String strPattern = "[\u4e00-\u9fa5\\w]+";
        Pattern p = Pattern.compile(strPattern);
        Matcher m = p.matcher(str);
        return m.matches();
    }


    /**
     * 校验银行卡卡号
     *
     * @param cardId
     * @return
     */
    public static boolean checkBankCard(String cardId) {
        return cardId.length() == 16 || cardId.length() == 19;
    }

    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }


    public static String getTokenStr() {
        String time = (System.currentTimeMillis() / 1000) + "";
        String res = "time/" + time + "/";
        return res;
    }

    public static void getCityOneToHttp(Context context, String flag) {

        Log.e(TAG, "params==" + userId + "/id==" + channelId);
        String channelId_ = loadStr(context, FinalConstant.BD_CHID, "");
        String userId_ = loadStr(context, FinalConstant.BD_USERID, "");

        Log.e(TAG, "params==" + userId_ + "/id==" + channelId_);

        if (channelId_.length() > 0) {

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("channelid", channelId_);
            hashMap.put("token", userId_);
            hashMap.put("flag", flag);
            new GetCityOneToHttpApi().getCallBack(context, hashMap, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {

                }
            });

        }
    }


    /**
     * activity 是否还存在
     *
     * @param c
     * @return
     */
    public static boolean isValidContext(Context c) {

        Activity a = (Activity) c;

        return !a.isFinishing();
    }


    /**
     * 专题进入统计
     *
     * @param mContext
     * @param source
     */
    public static void ztrecordHttp(Context mContext, String source) {

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("source", source);
        new ZTRecordHttpApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

            }
        });
    }

    /**
     * 本地的统计
     *
     * @param mContext
     * @param event_name：统计事件名
     * @param event_pos：接口名，或者说是某个接口的统计。
     * @param event_others：这是个啥？
     * @param referrer：app页面来源
     */
    public static void tongjiApp(Context mContext, String event_name, String event_pos, String event_others, String referrer) {
        String imei = getImei();        //当前设备唯一标识
        String timec = System.currentTimeMillis() + "";         //当前时间（时间戳）
        String city = Cfg.loadStr(mContext, FinalConstant.DWCITY, "全国");


        String sss = "";
        if (referrer.length() > 0) {
            sss = "&referrer=" + referrer;
        }

        String urls = "https://empty.yuemei.com/?source=1&event=1&utm_source=" + FinalConstant1.YUEMEI_MARKET + "&v=" + BuildConfig.VERSION_NAME + "&event_name=" + event_name + "&event_pos=" + event_pos + "&event_others=" + event_others + sss + "&ym_onlyk=" + imei + "&refresh=" + timec + "&t=" + timec + "&user_id=" + Utils.getUid() + "&from=4" + "&others=" + city;

        if (event_name.equals("search_tao")) {
            String searchs = loadStr(mContext, "search_key", "search_" + System.currentTimeMillis());
            urls = urls + "&url=" + searchs;
        }

        Log.e(TAG, "urls === " + urls);
        OkGo.get(urls).execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {
                Log.e(TAG, "s == " + s);
            }
        });
    }


    public static void chatTongJi(Context context, TongJiParams tongJiParams) {
        String timec = System.currentTimeMillis() + "";         //当前时间（时间戳）
        String city = Cfg.loadStr(context, FinalConstant.DWCITY, "全国");
        StringBuilder url = new StringBuilder();
        url.append("https://empty.yuemei.com/?source=1&event=1&utm_source=" + FinalConstant1.YUEMEI_MARKET + "&v=" + BuildConfig.VERSION_NAME);
        if (!TextUtils.isEmpty(tongJiParams.getEvent_name())) {
            url.append("&event_name=" + tongJiParams.getEvent_name());
        }
        if (!TextUtils.isEmpty(tongJiParams.getEvent_pos())) {
            url.append("&event_pos=" + tongJiParams.getEvent_pos());
        }
        if (!TextUtils.isEmpty(tongJiParams.getHos_id())) {
            url.append("&hos_id=" + tongJiParams.getHos_id());
        }
        if (!TextUtils.isEmpty(tongJiParams.getDoc_id())) {
            url.append("&doc_id=" + tongJiParams.getDoc_id());
        }
        if (!TextUtils.isEmpty(tongJiParams.getTao_id())) {
            url.append("&tao_id=" + tongJiParams.getTao_id());
        }
        if (!TextUtils.isEmpty(tongJiParams.getEvent_others())) {
            url.append("&event_others=" + tongJiParams.getEvent_others());
        }
        if (!TextUtils.isEmpty(tongJiParams.getId())) {
            url.append("&id=" + tongJiParams.getId());
        }
        if (!TextUtils.isEmpty(tongJiParams.getReferrer())) {
            url.append("&referrer=" + tongJiParams.getReferrer());
        }
        if (!TextUtils.isEmpty(tongJiParams.getType())) {
            url.append("&type=" + tongJiParams.getType());
        }
        url.append("&ym_onlyk=" + getImei())
                .append("&t=" + timec)
                .append("&from=4")
                .append("&user_id=" + Utils.getUid())
                .append("&others=" + city);
        Log.e(TAG, "url === " + url.toString());
        OkGo.get(url.toString()).execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {

            }
        });
    }


    //&hos_id={医院Id}&doc_id={医生Id}&tao_id={taoId}&type={2淘,3医生,4医院}
    public static void tongjiApp(Context mContext, String event_name, String event_pos, String event_others, String referrer, String hosid, String docid, String taoid, String type) {
        String imei = getImei();        //当前设备唯一标识
        String timec = System.currentTimeMillis() + "";         //当前时间（时间戳）
        String city = Cfg.loadStr(mContext, FinalConstant.DWCITY, "全国");

        String sss = "";
        if (referrer.length() > 0) {
            sss = "&referrer=" + referrer;
        }

        String urls = "https://empty.yuemei.com/?source=1&event=1&utm_source=" + FinalConstant1.YUEMEI_MARKET + "&v=" + BuildConfig.VERSION_NAME + "&event_name=" + event_name + "&event_pos=" + event_pos + "&event_others=" + event_others + sss + "&ym_onlyk=" + imei + "&refresh=" + timec + "&user_id=" + Utils.getUid() + "&from=4" + "&others=" + city + "&hos_id=" + hosid + "&doc_id=" + docid + "&tao_id=" + taoid + "&type=" + type;

        if (event_name.equals("search_tao")) {
            String searchs = loadStr(mContext, "search_key", "search_" + System.currentTimeMillis());
            urls = urls + "&url=" + searchs;
        }

        OkGo.get(urls).execute(new StringCallback() {
            @Override
            public void onSuccess(String s, Call call, Response response) {

            }
        });
    }

    /**
     * 获取当前的网络状态 ：没有网络-0：WIFI网络1：4G网络-4：3G网络-3：2G网络-2
     * 自定义
     *
     * @param context
     * @return
     */
    public static int getAPNType(Context context) {
        //结果返回值
        int netType = 0;
        //获取手机所有连接管理对象
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //获取NetworkInfo对象
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        //NetworkInfo对象为空 则代表没有网络
        if (networkInfo == null) {
            return netType;
        }
        //否则 NetworkInfo对象不为空 则获取该networkInfo的类型
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_WIFI) {
            //WIFI
            netType = 1;
        } else if (nType == ConnectivityManager.TYPE_MOBILE) {
            int nSubType = networkInfo.getSubtype();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            //3G   联通的3G为UMTS或HSDPA 电信的3G为EVDO
            if (nSubType == TelephonyManager.NETWORK_TYPE_LTE && !telephonyManager.isNetworkRoaming()) {
                netType = 4;
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_UMTS || nSubType == TelephonyManager.NETWORK_TYPE_HSDPA || nSubType == TelephonyManager.NETWORK_TYPE_EVDO_0 && !telephonyManager.isNetworkRoaming()) {
                netType = 3;
                //2G 移动和联通的2G为GPRS或EGDE，电信的2G为CDMA
            } else if (nSubType == TelephonyManager.NETWORK_TYPE_GPRS || nSubType == TelephonyManager.NETWORK_TYPE_EDGE || nSubType == TelephonyManager.NETWORK_TYPE_CDMA && !telephonyManager.isNetworkRoaming()) {
                netType = 2;
            } else {
                netType = 2;
            }
        }
        return netType;
    }

    /**
     * 获取IMEI号，IESI号，手机型号
     */
    public static void getInfo(final Context mContext) {
        Log.e("getInfo", "getInfo ==== ");
        try {
            String token = Utils.getShuMeiImei();
            String version = android.os.Build.VERSION.RELEASE;// String
            String mtype = android.os.Build.MODEL; // 手机型号
            String mtyb = android.os.Build.BRAND;// 手机品牌
            String brand = mtyb + "_" + mtype;
            brand = brand.replace(" ", "");
            Map<String, Object> keyValues = new HashMap<>();
            if (!"0".equals(token)) {
                keyValues.put("token", token);
            }
            keyValues.put("brand", brand);
            keyValues.put("system", version);
            if (NotificationManagerCompat.from(mContext).areNotificationsEnabled()) {
                keyValues.put("is_notice", "1");
            } else {
                keyValues.put("is_notice", "0");
            }
            String deviceId = "";
            String sessionId = "";
            if (!TextUtils.isEmpty(deviceId)) {
                keyValues.put("device_id", deviceId);
            }
            if (!TextUtils.isEmpty(sessionId)) {
                keyValues.put("session_id", sessionId);
            }

            new GetInfoApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData data) {
                    Log.e("getInfo", "data == " + data.toString());
                    getCartNumber(mContext);
                    if ("yingyongbao".equals(FinalConstant1.YUEMEI_MARKET)) {
                        YybOcpa.yybAdvertising();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("getInfo", "e == " + e.toString());
        }

        DeviceUuidFactory de = new DeviceUuidFactory(mContext);
        String uuid = de.getDeviceUuid() + "";
        Cfg.saveStr(mContext, FinalConstant.UUID, uuid);

        String time_id = loadStr(mContext, FinalConstant.TIME_ID, "");

        if (time_id.length() == 0) {
            String timestr = "yuemeitimeid" + (System.currentTimeMillis() / 1000);
            Cfg.saveStr(mContext, FinalConstant.TIME_ID, timestr);
        }
    }

    /**
     * 获取购物车数量
     */
    public static void getCartNumber(final Context mContext) {
        CartSkuNumberApi cartSkuNumberApi = new CartSkuNumberApi();
        cartSkuNumberApi.getCallBack(mContext, cartSkuNumberApi.getmCartSkuNumberHashMap(), new BaseCallBackListener<CartSkuNumberData>() {
            @Override
            public void onSuccess(CartSkuNumberData data) {
                if (Integer.parseInt(data.getCart_number()) >= 100) {
                    Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, "99+");
                } else {
                    Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, data.getCart_number());
                }
            }
        });
    }

    /**
     * Ho
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(float dpValue) {
        final float scale = MyApplication.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int dip2px(int dpValue) {
        final float scale = MyApplication.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int dip2px(Context context, int dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static float px2dip(float pxValue) {
        final float scale = MyApplication.getContext().getResources().getDisplayMetrics().density;
        return pxValue / scale + 0.5f;
    }


    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(int pxValue) {
        final float scale = MyApplication.getContext().getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int sp2px(int spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spVal, MyApplication.getContext().getResources().getDisplayMetrics());
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param context
     * @param pxValue
     * @return
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 判断手机分辨率是否大于240
     *
     * @param context
     * @return
     */
    public static boolean isOldSmallPhone(Context context) {
        DisplayMetrics metric = context.getResources().getDisplayMetrics();
        int densityDpi = metric.densityDpi;  // 屏幕密度DPI（120 / 160 / 240,小米4的是480）
        Log.e("fenbianlu", "densityDpi == " + densityDpi);
        return densityDpi < 400;
    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
        return bitmap;
    }

    public static String getTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    public static String getYear(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        return format.format(date);
    }

    public static String getMonths(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MM");
        return format.format(date);
    }

    public static String getDay(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        return format.format(date);
    }

    public static Date getDate(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date dates = format.parse(date);
            return dates;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取年月日时分秒
     *
     * @return
     */
    public static String getTime() {
        Calendar calendar = Calendar.getInstance();  //获取当前时间，作为图标的名字
        String year = calendar.get(Calendar.YEAR) + "";
        String month = calendar.get(Calendar.MONTH) + 1 + "";
        String day = calendar.get(Calendar.DAY_OF_MONTH) + "";
        String hour = calendar.get(Calendar.HOUR_OF_DAY) + "";
        String minute = calendar.get(Calendar.MINUTE) + "";
        String second = calendar.get(Calendar.SECOND) + "";
        String time = year + month + day + hour + minute + second;
        return time;
    }

    /**
     * 获取当天年月日拼接串
     *
     * @return
     */
    public static String getDay() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);    //获取年
        int month = cal.get(Calendar.MONTH) + 1;   //获取月份，0表示1月份
        int day = cal.get(Calendar.DAY_OF_MONTH);    //获取当前天数

        return year + "" + month + "" + day;
    }

    /**
     * 获取当天年月日拼接串
     *
     * @return
     */
    public static String getDay1() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);    //获取年
        String month = (cal.get(Calendar.MONTH) + 1) + "";   //获取月份，0表示1月份
        String day = cal.get(Calendar.DAY_OF_MONTH) + "";    //获取当前天数
        if (month.length() == 1) {
            month = "0" + month;
        }
        if (day.length() == 1) {
            day = "0" + day;
        }
        return year + "-" + month + "-" + day;
    }

    /**
     * 时间相减得到天数
     *
     * @param beginDateStr：开始的时间
     * @param endDateStr:结束的时间
     * @return long
     */
    public static long getDaySub(String beginDateStr, String endDateStr) {
        long day = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate;
        Date endDate;
        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
            day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day;
    }

    /**
     * 获取IMEI号
     *
     * @return
     */
    public static String getImei() {
        String imei = ImeiUtils.getInstance().getImei();
        if (TextUtils.isEmpty(imei)) {
            imei = ImeiUtils.getBringItem2();
            Log.e(TAG,"imei 第二种获取方式"+imei);
            ImeiUtils.getInstance().saveLocalItem(imei);
        }
        return imei;
    }


    /**
     * 判断是否是新用户
     *
     * @return :true是新用户，false老用户
     */
    public static boolean getNewOrOldUser() {
        String localItem = "";
        if (ImeiUtils.getInstance().isStoragePermissions() && ExternalStorage.isFileExist(ImeiUtils.getInstance().getmPath())) {
            localItem = ImeiUtils.getInstance().getLocalItem();
        }

        boolean isHaveItem = TextUtils.isEmpty(localItem);
        String isNewOrOld = Cfg.loadStr(MyApplication.getContext(), FinalConstant.IS_NEW_OR_OLD, "0");

        return !isHaveItem && "0".equals(isNewOrOld);
    }

    /**
     * 修改新老用户
     *
     * @param value
     */
    public static void saveNewOrOldUser(String value) {
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.IS_NEW_OR_OLD, value);
    }

    /**
     * 数美设备唯一标识
     *
     * @return
     */
    public static String getShuMeiImei() {
        String deviceId = SmAntiFraud.getDeviceId();
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = "0";
        }
        Log.e("getShuMeiImei", "deviceId == " + deviceId);
        return deviceId;
    }

    /**
     * EditText获取焦点并显示软键盘
     */
    public static void showSoftInputFromWindow(Activity activity, EditText editText) {

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.findFocus();
        editText.requestFocus();                    //获取焦点
        editText.setCursorVisible(true);            //光标显示

        //显示键盘
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    /**
     * 隐藏软键盘
     *
     * @param activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            if (activity.getCurrentFocus().getWindowToken() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    /**
     * 判断程序是否实在前台运行
     *
     * @param context
     * @return:true代表在后台运行，false代表正在app中
     */
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                    /*
                    BACKGROUND=400 EMPTY=500 FOREGROUND=100
                    GONE=1000 PERCEPTIBLE=130 SERVICE=300 ISIBLE=200
                     */
                Log.i(context.getPackageName(), "此appimportace =" + appProcess.importance + ",context.getClass().getName()=" + context.getClass().getName());
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i(context.getPackageName(), "处于后台" + appProcess.processName);
                    return true;
                } else {
                    Log.i(context.getPackageName(), "处于前台" + appProcess.processName);
                    return false;
                }
            }
        }
        return false;
    }


    /**
     * 判断当前程序是否前台进程
     *
     * @param context
     * @return
     */
    public static boolean isCurAppTop(Context context) {
        if (context == null) {
            return false;
        }
        String curPackageName = context.getPackageName();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ActivityManager.RunningTaskInfo info = list.get(0);
            String topPackageName = info.topActivity.getPackageName();
            String basePackageName = info.baseActivity.getPackageName();
            return topPackageName.equals(curPackageName) && basePackageName.equals(curPackageName);
        }
        return false;
    }


    /**
     * 方法描述：判断某一应用是否正在运行
     * Created by cafeting on 2017/2/4.
     *
     * @param context     上下文
     * @param packageName 应用的包名
     * @return true 表示正在运行，false 表示没有运行
     */
    public static boolean isAppRunning(Context context, String packageName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(100);
        if (list.size() <= 0) {
            return false;
        }
        for (ActivityManager.RunningTaskInfo info : list) {
            if (info.baseActivity.getPackageName().equals(packageName)) {
                return true;
            }
        }
        return false;
    }


    /**
     * EditText失去焦点并显示软键盘
     *
     * @param activity
     * @param editText
     */
    public static void hideSoftKeyboardFromWindow(Activity activity, EditText editText) {

//        editText.clearFocus();              //失去焦点
        editText.setCursorVisible(false);   //光标隐藏

        //隐藏键盘
        ((InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);


    }

    /**
     * 将php时间戳转换为时间
     *
     * @param s
     * @return
     */
    public static String stampToPhpDate(String s) {

        String result = null;
        long lt = new Long(s);
        Date date = new Date(lt * 1000);
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        result = sd.format(date);

        return result;
    }

    /**
     * 将时间戳转换为时间(判断是java时间戳还是php时间戳)
     *
     * @param s
     * @return
     */
    public static String stampToJavaAndPhpDate(String s) {

        String result = null;
        long lt = new Long(s);

        Date date;
        if (s.length() == 10) {
            date = new Date(lt * 1000);
        } else {
            date = new Date(lt);
        }

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        result = sd.format(date);

        return result;
    }

    /**
     * php 时间戳转 java时间戳
     *
     * @param timeStame
     * @return
     */
    public static long phpToJavaTimeStamp(String timeStame) {
        long lt = Long.valueOf(timeStame);
        Date date;
        if (timeStame.length() == 10) {
            date = new Date(lt * 1000);
        } else {
            date = new Date(lt);
        }
        return date.getTime();
    }


    /**
     * 将时间相减得到年月日时分秒
     *
     * @param startDateStr    ：开始时间 类型："yyyy-MM-dd HH:mm:ss"
     * @param endDateStr：结束事件 类型："yyyy-MM-dd HH:mm:ss"
     * @return :是一个数组：分别是 天、时、分 、秒
     * @throws ParseException
     */
    public static long[] getTimeSub(String startDateStr, String endDateStr) {
        Log.e(TAG, "getTimeSub -> startDateStr == " + startDateStr);
        Log.e(TAG, "getTimeSub -> endDateStr == " + endDateStr);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long[] longs = new long[4];
        try {
            Date d1 = df.parse(endDateStr);
            Log.e(TAG, "d1 === " + d1);
            Date d2 = df.parse(startDateStr);
            Log.e(TAG, "d2 === " + d2);
            long diff = d1.getTime() - d2.getTime();//这样得到的差值是微秒级别
            Log.e(TAG, "diff === " + diff);
            long days = diff / (1000 * 60 * 60 * 24);
            Log.e(TAG, "days === " + days);

            long hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
            Log.e(TAG, "hours === " + hours);
            long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60);
            Log.e(TAG, "minutes === " + minutes);

            long second = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60) - minutes * (1000 * 60)) / 1000;
            Log.e(TAG, "second === " + second);

            System.out.println("" + days + "天" + hours + "小时" + minutes + "分" + second + "秒");

            longs[0] = days;
            longs[1] = hours;
            longs[2] = minutes;
            longs[3] = second;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return longs;
    }

    /**
     * 将时间戳转为Long数组
     *
     * @param dataStr
     * @return
     */
    public static long[] timeToLong(String dataStr) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long[] longs = new long[4];
        try {

            Long longData = Long.valueOf(dataStr);
            String format = df.format(new Date(longData));
            Date date = df.parse(format);
            long days = (long) date.getDay();
            long hours = (long) date.getHours();
            long minutes = (long) date.getHours();
            long second = (long) date.getHours();
//            long time = date.getTime();
//            long days = time / (1000 * 60 * 60 * 24);
//            long hours = (time - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
//            long minutes = (time - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60);
//            long second = (time - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60) - minutes * (1000 * 60)) / 1000;
            longs[0] = days;
            longs[1] = hours;
            longs[2] = minutes;
            longs[3] = second;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return longs;
    }

    /**
     * 用户增加积分
     *
     * @param flag: 9：分享增加
     */
    public static void sumitHttpCode(final Context mContext, String flag) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        new SumitHttpAip().getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {

            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                String jifenNu = jfjyData.getIntegral();
                String jyNu = jfjyData.getExperience();

                if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                    MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, MyToast.SHOW_TIME).show();
                } else {
                    if (!jifenNu.equals("0")) {
                        MyToast.makeTexttext2Toast(mContext, jifenNu, MyToast.SHOW_TIME).show();
                    } else {
                        if (!jyNu.equals("0")) {
                            MyToast.makeTexttext3Toast(mContext, jyNu, MyToast.SHOW_TIME).show();
                        }
                    }
                }
            }
        });
    }


    /**
     * 获取精确到毫秒的时间戳
     *
     * @return
     */
    public static long getSecondTimestamp() {
        long l = System.currentTimeMillis();
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        long time = new Date().getTime();
        Log.e("getSecondTimestamp", "l === " + l);
        Log.e("getSecondTimestamp", "timeInMillis === " + timeInMillis);
        Log.e("getSecondTimestamp", "time === " + time);
        return timeInMillis;
    }


    /**
     * 判断nowDate时间，是否在startDate~endDate时间段内
     *
     * @param nowTime   要比较的时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return true在时间段内，false不在时间段内
     * @throws Exception
     */
    public static boolean hourMinuteBetween(String nowTime, String startTime, String endTime) {
        try {
            Log.e(TAG, "nowTime == " + nowTime);
            Log.e(TAG, "startTime == " + startTime);
            Log.e(TAG, "endTime == " + endTime);

            SimpleDateFormat format = new SimpleDateFormat("HH:mm");

            Date nowDate = format.parse(nowTime);

            Date startDate = format.parse(startTime);
            Date endDate = format.parse(endTime);

            Calendar date = Calendar.getInstance();
            date.setTime(nowDate);

            Calendar begin = Calendar.getInstance();
            begin.setTime(startDate);

            Calendar end = Calendar.getInstance();
            end.setTime(endDate);

            return date.after(begin) && date.before(end);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 判断nowDay当天，是否在startDay~endDay时间段内
     *
     * @param nowDay   要比较的时间
     * @param startDay 开始时间
     * @param endDay   结束时间
     * @return true在时间段内，false不在时间段内
     * @throws Exception
     */
    public static boolean hourDayBetween(String nowDay, String startDay, String endDay) {
        try {
            Log.e(TAG, "nowTime == " + nowDay);
            Log.e(TAG, "startTime == " + startDay);
            Log.e(TAG, "endTime == " + endDay);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            Date nowDate = format.parse(nowDay);

            Date startDate = format.parse(startDay);
            Date endDate = format.parse(endDay);

            Calendar date = Calendar.getInstance();
            date.setTime(nowDate);

            Calendar begin = Calendar.getInstance();
            begin.setTime(startDate);

            Calendar end = Calendar.getInstance();
            end.setTime(endDate);

            Log.e(TAG, "date.after(begin) === " + date.after(begin));
            Log.e(TAG, "date.before(end) === " + date.before(end));
            return date.after(begin) && date.before(end);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "e == " + e.toString());
            return false;
        }
    }

    /**
     * 时间转为时间戳
     *
     * @param time ：2016-01-12 12:05:00
     * @return ：时间戳
     */
    public static Integer StringToTimestamp(String time) {
        int times = 0;
        try {
            times = (int) ((Timestamp.valueOf(time).getTime()) / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (times == 0) {
            System.out.println("String转10位时间戳失败");
        }
        return times;
    }

    /**
     * 检查当前网络是否可用
     *
     * @param activity
     * @return
     */
    public static boolean isNetworkAvailable(Activity activity) {
        Context context = activity.getApplicationContext();
        // 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager == null) {
            return false;
        } else {
            // 获取NetworkInfo对象
            NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();

            if (networkInfo != null && networkInfo.length > 0) {
                for (int i = 0; i < networkInfo.length; i++) {
                    System.out.println(i + "===状态===" + networkInfo[i].getState());
                    System.out.println(i + "===类型===" + networkInfo[i].getTypeName());
                    // 判断当前网络状态是否为连接状态
                    if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 获取用户id
     *
     * @return
     */
    public static String getUid() {
        String uid = Cfg.loadStr(MyApplication.getContext(), FinalConstant.UID, "0");
        if (TextUtils.isEmpty(uid)) {
            uid = "0";
        }
        return uid;
    }

    public static String getUid(Activity activity) {
        String uid = Cfg.loadStr(activity, FinalConstant.UID, "0");
        if (TextUtils.isEmpty(uid)) {
            uid = "0";
        }
        return uid;
    }


    /**
     * @return 经度
     */
    public static String getLongitude() {

        String longitude = Cfg.loadStr(MyApplication.getContext(), FinalConstant.DW_LONGITUDE, "0");
        if (TextUtils.isEmpty(longitude)) {
            longitude = "0";
        }
        return longitude;
    }

    /**
     * @return 纬度
     */
    public static String getLatitude() {

        String latitude = Cfg.loadStr(MyApplication.getContext(), FinalConstant.DW_LATITUDE, "0");
        if (TextUtils.isEmpty(latitude)) {
            latitude = "0";
        }
        return latitude;
    }

    //获取手机号码
    public static String getLoginPhone() {
        String loginInPhone = Cfg.loadStr(MyApplication.getContext(), FinalConstant.ULOGINPHONE, "");
        return loginInPhone;
    }

    /**
     * 保存用户id
     *
     * @param uid
     */
    public static void setUid(String uid) {
        if (TextUtils.isEmpty(uid)) {
            uid = "0";
        }
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.UID, uid);
    }

    public static void setUid(Activity activity, String uid) {
        if (TextUtils.isEmpty(uid)) {
            uid = "0";
        }
        Cfg.saveStr(activity, FinalConstant.UID, uid);
    }


    /**
     * 判断是否登录
     *
     * @return:true:已登录，false未登录
     */
    public static boolean isLogin() {
        return !"0".equals(getUid());
    }

    /**
     * 登录后是否绑定了手机号
     *
     * @return
     */
    public static boolean isBind() {
        String loginInPhone = Cfg.loadStr(MyApplication.getContext(), FinalConstant.USERISPHONE, "0");
        if ("1".equals(loginInPhone)) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * 私信游客判断 true 游客跳过登录验证
     *
     * @return
     */
    public static boolean noLoginChat() {
        String noLoginChat = Cfg.loadStr(MyApplication.getContext(), SplashActivity.NO_LOGIN_CHAT, "0");
        if ("1".equals(noLoginChat)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断
     *
     * @param context
     */
    public static boolean isLoginAndBind(Context context) {
        if (isLogin()) {
            if (isBind()) {
                return true;
            } else {
                jumpBindingPhone(context);
                return false;
            }
        } else {
            jumpLogin(context);
            return false;
        }
    }

    public static boolean isLoginAndBind() {
        if (isLogin()) {
            return isBind();
        } else {
            return false;
        }
    }



    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }


    /**
     * 跳转到登录页面
     *
     * @return
     */
    public static void jumpLogin(Context context) {
//        AutoLoginControler.getInstance().autoLogin(context);
        Intent intent = new Intent(context, AutoLoginControler.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 跳转到绑定手机号页面
     *
     * @param context
     */
    public static void jumpBindingPhone(Context context) {
        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(context);
        if (verifyEnable) {
            jumpAutoBind(context);
        } else {
            Intent it = new Intent(context, BindingPhoneActivity.class);
            context.startActivity(it);
        }

    }

    /**
     * 一键绑定
     *
     * @param context
     */
    public static void jumpAutoBind(final Context context) {
        int height = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        float marginBottom = (float) (height * 0.15);

        TextView otherLogin = new TextView(context);
        otherLogin.setText("绑定其他号码");
        otherLogin.setTextColor(ContextCompat.getColor(context, R.color._99));
        otherLogin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        otherLogin.setCompoundDrawables(ContextCompat.getDrawable(context, R.drawable.auto_bind_phone), null, null, null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.setMargins(0, 0, 0, (int) marginBottom + Utils.dip2px(57));
        otherLogin.setLayoutParams(layoutParams);

        JVerifyUIConfig uiConfig = new JVerifyUIConfig.Builder()
                .setAuthBGImgPath("main_bg")
                .setNavColor(ContextCompat.getColor(context, R.color.white))
                .setNavText("登录")
                .setNavTextColor(ContextCompat.getColor(context, R.color._33))
                .setNavReturnImgPath("back_black")
                .setLogoWidth(75)
                .setLogoHeight(75)
                .setLogoHidden(false)
                .setNumberColor(ContextCompat.getColor(context, R.color._33))
                .setNumFieldOffsetY(160)
                .setLogBtnText("一键绑定")
                .setLogBtnTextColor(0xffffffff)
                .setLogBtnImgPath("auto_login_btn")
                .setLogBtnHeight(47)
                .setLogBtnTextSize(15)
                .setAppPrivacyOne("悦美隐私政策", FinalConstant.USERAGREMMENT)
                .setAppPrivacyColor(0xff666666, 0xff0085d0)
                .setUncheckedImgPath("umcsdk_uncheck_image")
                .setCheckedImgPath("umcsdk_check_image")
                .setSloganTextColor(0xff999999)
                .setLogoOffsetY(60)
                .setLogoImgPath("ic_launcher")
                .setSloganOffsetY(180)
                .setLogBtnOffsetY(224)
                .setPrivacyState(true)
                .addCustomView(otherLogin, true, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {
                        Intent it = new Intent(context, BindingPhoneActivity.class);
                        context.startActivity(it);
                    }
                }).setPrivacyOffsetY(35).build();
        JVerificationInterface.setCustomUIWithConfig(uiConfig);
        JVerificationInterface.loginAuth(context, true, new VerifyListener() {
            @Override
            public void onResult(int code, String content, String operator) {
                if (code == 6000) {
                    Log.e(TAG, "code=" + code + ", token=" + content + " ,operator=" + operator);
                    Toast.makeText(context, "绑定中...", Toast.LENGTH_SHORT).show();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("loginToken", content);
                    map.put("uid", Utils.getUid());
                    new AutoBindApi().getCallBack(context, map, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            if ("1".equals(serverData.code)) {
                                getUserInfoLogin((Activity) context, getUid(), "1");
                            } else {
                                Toast.makeText(context, serverData.message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                } else {
                    Log.e(TAG, "code=" + code + ", message=" + content);
                    if (code != 6002) {
                        Intent it = new Intent(context, BindingPhoneActivity.class);
                        context.startActivity(it);
                    }
                }
            }
        }, new AuthPageEventListener() {
            @Override
            public void onEvent(int cmd, String msg) {
                Log.e(TAG, "[onEvent]. [" + cmd + "]message=" + msg);
            }
        });
    }


    /**
     * 获取城市信息
     *
     * @return
     */
    public static String getCity() {
        String mCity = Cfg.loadStr(MyApplication.getContext(), FinalConstant.DWCITY, "全国");
        if ("失败".equals(mCity) || TextUtils.isEmpty(mCity)) {
            mCity = "全国";
        }
        return mCity;
    }

    /**
     * 保存城市信息
     *
     * @param city
     */
    public static void setCity(String city) {
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.DWCITY, city);
    }


    /**
     * 获取版本名称
     */
    public static String getVersionName() {
        String vserIntLoc = "0";
        try {
            PackageInfo packageInfo = MyApplication.getContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = packageInfo.versionName;
            Log.e("banben", "本软件的版本号名称 == " + versionName);

            vserIntLoc = versionName.replace(".", "");
            Log.e("banben", "vserIntLoc == " + vserIntLoc);

        } catch (PackageManager.NameNotFoundException e) {
            Log.e("banben", "e == " + e.toString());
            e.printStackTrace();
            return "720";
        }
        return vserIntLoc;
    }

    /**
     * 获取Sessionid
     *
     * @return
     */
    public static SessionidData getSessionid() {
        //判断会话id是否超时
        String loadStr = Cfg.loadStr(MyApplication.getContext(), FinalConstant1.SESSIONID, "");
        if (!TextUtils.isEmpty(loadStr)) {
            return JSONUtil.TransformSingleBean(loadStr, SessionidData.class);
        } else {
            return null;
        }
    }

    /**
     * 保存获取Sessionid
     */
    public static void setSessionid() {
        String sessionid = System.currentTimeMillis() + Utils.getImei() + Utils.getVersionName() + FinalConstant1.YUEMEI_MARKET;
        SessionidData sessionidData = new SessionidData(System.currentTimeMillis(), Utils.StringInMd5(sessionid));
        Cfg.saveStr(MyApplication.getContext(), FinalConstant1.SESSIONID, new Gson().toJson(sessionidData));
    }

    /**
     * 获取YuemeiInfo
     *
     * @return
     */
    public static String getYuemeiInfo() {
        return Cfg.loadStr(MyApplication.getContext(), FinalConstant.YUEMEIINFO, "0").replace("+", "%2B");
    }

    /**
     * 保存YuemeiInfo
     */
    public static void setYuemeiInfo(String yuemeiInfo) {
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.YUEMEIINFO, yuemeiInfo);
    }


    /**
     * 获取本地颜色
     *
     * @param color
     * @return
     */
    public static int getLocalColor(Context context, int color) {
        return ContextCompat.getColor(context, color);
    }

    /**
     * 获取本地图片
     *
     * @param drawable
     * @return
     */
    public static Drawable getLocalDrawable(Context context, int drawable) {
        return ContextCompat.getDrawable(context, drawable);
    }

    /**
     * 自定义颜色
     *
     * @param color
     * @return
     */
    public static int setCustomColor(String color) {
        return Color.parseColor(color);
    }


    /**
     * @param mContext
     * @param uid
     * @param loginType 1原生登录,2 第三方登录
     */
    public static void getUserInfoLogin(final Activity mContext, String uid, final String loginType) {
        BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
        Map<String, Object> params = new HashMap<>();
        params.put("id", uid);
        userInfoApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    String id = userData.get_id();
                    TalkingDataAppCpa.onLogin(id);
                    String img = userData.getImg();
                    String nickName = userData.getNickname();
                    String province = userData.getProvince();
                    String city = userData.getCity();
                    String sex = userData.getSex();
                    String birthday = userData.getBirthday();
                    String yuemeiinfo = userData.getYuemeiinfo();
                    String group_id = userData.getGroup_id();
                    String phoneStr = userData.getPhone();
                    String isphone = userData.getIsphone();
                    String loginPhone = userData.getLoginphone();
                    String is_show = userData.getIs_show();
                    String real_name = userData.getReal_name();
                    String user_more = userData.getUser_more();
                    Cfg.saveStr(mContext, "real_name", real_name);
                    setUid(id);
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.HOME_PERSON_UID, id);
                    Cfg.saveStr(mContext, FinalConstant.UHEADIMG, img);
                    Cfg.saveStr(mContext, FinalConstant.UNAME, nickName);
                    Cfg.saveStr(mContext, FinalConstant.UPROVINCE, province);
                    Cfg.saveStr(mContext, FinalConstant.UCITY, city);
                    Cfg.saveStr(mContext, FinalConstant.USEX, sex);
                    Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, birthday);
                    Log.e("adasdas", "is_show........... == " + is_show);
                    Cfg.saveStr(mContext, FinalConstant.ISSHOW, is_show);
                    Utils.setYuemeiInfo(yuemeiinfo);
                    Utils.getCartNumber(mContext);
                    Cfg.saveStr(mContext, FinalConstant.GROUP_ID, group_id);
                    Cfg.saveStr(mContext, FinalConstant.USER_MORE, user_more);
                    if (null == loginPhone || loginPhone.equals("0") || loginPhone.equals("")) {
                        jumpAutoBind(mContext);
                    }
                    Log.e("AutoLoginControler", "=======initWebSocket");
                    IMManager.getInstance(mContext).getIMNetInstance().connWebSocket(FinalConstant.baseService);
                    String registrationID = JPushInterface.getRegistrationID(mContext);
                    String mCity = Cfg.loadStr(mContext, "city_dingwei", "");
                    String mProvince = Cfg.loadStr(mContext, FinalConstant.DWPROVINCE, "");
                    HashMap<String, Object> maps = new HashMap<>();
                    String version = android.os.Build.VERSION.RELEASE;// String
                    String mtype = android.os.Build.MODEL; // 手机型号
                    String mtyb = android.os.Build.BRAND;// 手机品牌
                    String brand = mtyb + "_" + mtype;
                    maps.put("reg_id", registrationID);
                    maps.put("location_city", mCity);
                    maps.put("location_Area", mProvince);
                    maps.put("brand", brand);
                    maps.put("system", version);
                    new JPushBindApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                        }
                    });
                    if (phoneStr.equals("0")) {
                        Cfg.saveStr(mContext, FinalConstant.UPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.UPHONE, phoneStr);
                    }

                    if ("".equals(loginPhone) || "0".equals(loginPhone)) {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, loginPhone);
                    }

                    if ("".equals(isphone) || "0".equals(isphone)) {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, isphone);
                    }

                    if (Utils.getNewOrOldUser()) {              //如果是新用户
                        Utils.saveNewOrOldUser("1");        //改为老用户
                    }
                    mContext.finish();
                } else {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * 清除视频缓存文件
     */
    public static void clearVideoCacheFile() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String sdCardPrivateCacheDir = ExternalStorage.getSDCardPrivateCacheDir(MyApplication.getContext());
                Log.e(TAG, "sdCardPrivateCacheDir == " + sdCardPrivateCacheDir + "/video-cache");
                ExternalStorage.removeFileFolderFromSDCard(sdCardPrivateCacheDir + "/video-cache", false);
            }
        }).start();
    }

    /**
     * 判断是否是中文
     *
     * @param title
     * @return ：true:是。false:不是
     */
    public static boolean isChinese(String title) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(title);
        return m.find();
    }

    /**
     * 中文转Unicode
     *
     * @param string
     * @return
     */
    public static String unicodeEncode(String string) {
        try {
            return URLEncoder.encode(string, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return string;
        }
    }

    /**
     * Unicode转中文转
     *
     * @param string
     * @return
     */
    public static String unicodeDecode(String string) {
        try {
            return URLDecoder.decode(string, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return string;
        }
    }

    /**
     * 获取屏幕的宽高
     *
     * @param activity
     * @return
     */
    public static int[] getScreenSize(Activity activity) {
        int[] ints = new int[2];
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        Class c;
        try {
            c = Class.forName("android.view.Display");
            Method method = c.getMethod("getRealMetrics", DisplayMetrics.class);
            method.invoke(display, dm);
            ints[0] = dm.widthPixels;
            ints[1] = dm.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
            DisplayMetrics metric = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metric);
            ints[0] = metric.widthPixels;
            ints[1] = metric.heightPixels;
        }
        return ints;
    }


    /**
     * 百度统计
     *
     * @param eventId:事件ID,注意要先在mtj.baidu.com中注册此事件ID(产品注册)
     * @param label:自定义事件标签
     */
    public static void baiduTongJi(Context context, String eventId, String label) {
        StatService.onEvent(context, eventId, label, 1);
    }

    /**
     * md5加密
     *
     * @param s
     * @return
     */
    public static String StringInMd5(String s) {
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        try {
            // 按照相应编码格式获取byte[]
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式

            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;

            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return "-1";
        }
    }

    /**
     * 清空用户信息
     */
    public static void clearUserData() {
        //关闭极光推送
        String registrationID = JPushInterface.getRegistrationID(MyApplication.getContext());
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("reg_id", registrationID);
        new JPushClosedApi().getCallBack(MyApplication.getContext(), maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "message===" + serverData.message);
            }
        });

        Utils.setUid("0");
        Utils.setYuemeiInfo("");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.HOME_PERSON_UID, "0");
        Utils.getCartNumber(MyApplication.getContext());
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.ULOGINPHONE, "");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.UHEADIMG, "");
        Log.e("adasdas", "..............................");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.ISSHOW, "1");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.DACU_FLOAT_CLOAS, "");
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.NEWUSER_CLOSE, "");
        Cfg.clear(MyApplication.getContext());
        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
        HttpUrl httpUrl = new HttpUrl.Builder().scheme("https").host("chat.yuemei.com").build();

        List<Cookie> cookies = cookieStore.loadCookie(httpUrl);
        Log.e(TAG, "cookies == " + cookies);
        cookieStore.removeCookie(httpUrl);
        Cookie yuemeiinfo = new Cookie.Builder().name("yuemeiinfo").value("").domain("chat.yuemei.com").expiresAt(1544493729973L).path("/").build();
        cookieStore.saveCookie(httpUrl, yuemeiinfo);
//        IMManager.getInstance(MyApplication.getContext()).getIMNetInstance().closeWebSocket();
        Log.d("MainTableActivity", "退出登录");
        MainTableActivity.mainBottomBar.setMessageNum(0);
    }

    /**
     * 判断是否为汉字
     *
     * @param str
     * @return
     */
    public static Boolean isGB2312(String str) {
        for (int i = 0; i < str.length(); i++) {
            String bb = str.substring(i, i + 1);
            // 生成一个Pattern,同时编译一个正则表达式,其中的u4E00("一"的unicode编码)-\u9FA5("龥"的unicode编码)
            boolean cc = java.util.regex.Pattern.matches("[\u4E00-\u9FA5]", bb);
            if (cc == false) {
                return cc;
            }
        }
        return true;
    }

    /*时间戳转换成字符窜*/
    public static String getDateToString(long time) {
        Date d = new Date(time);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy年MM月dd日");
        return sf.format(d);
    }
}
