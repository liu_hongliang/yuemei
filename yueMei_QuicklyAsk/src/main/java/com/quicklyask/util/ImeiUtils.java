package com.quicklyask.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.module.MyApplication;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.weixin.MD5;
import com.quicklyask.view.MyToast;

import java.io.File;
import java.util.List;
import java.util.UUID;


/**
 * 获取手机唯一标识的类
 * Created by 裴成浩 on 2018/2/28.
 */

public class ImeiUtils {

    private Context mContext;
    private String TAG = "ImeiUtils";
    private String mPath = ExternalStorage.getSDCardBaseDir() + File.separator + "YueMei" + File.separator + "YueMeiImei";

    /**
     * 单例
     */
    private ImeiUtils() {
        mContext = MyApplication.getContext();
    }

    private static ImeiUtils imeiUtils = null;

    public synchronized static ImeiUtils getInstance() {
        if (imeiUtils == null) {
            imeiUtils = new ImeiUtils();
        }
        return imeiUtils;
    }


    /**
     * 获取唯一标识的统一入口
     *
     * @return
     */
//    public String getImei() {
//        String mItem;
//        //版本判断
//        if (Build.VERSION.SDK_INT >= 29){
//            if (isPhonePermissions()) {       //是否有手机状态权限
//                if (isStoragePermissions()) { //是否有存储权限
//                    if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
//                        mItem = getLocalItem();
//                    } else {
//                        mItem = getBringItem();
//                        saveLocalItem(mItem);
//                    }
//                } else {
//                    mItem = getBringItem();
//                }
//            } else {
//                if (isStoragePermissions()) {
//                    if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
//                        mItem = getLocalItem();
//                    } else {
//                        mItem = getBringItem();
//                        saveLocalItem(mItem);
//                    }
//
//                } else {
//                    if(TextUtils.isEmpty(getLocalItemCache())){
//                        mItem = getBringItem();
//                        saveLocalItemCache(mItem);
//                    }else{
//                        mItem = getLocalItemCache();
//                    }
//                }
//            }
//        }else if (Build.VERSION.SDK_INT >= 23){ //从6.0开始有的手机获取不到唯一标识
//            if (isPhonePermissions()) {       //是否有手机状态权限
//                if (isStoragePermissions()) { //是否有存储权限
//                    if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
//                        mItem = getLocalItem();
//                    } else {
//                        mItem = getBringItem();
//                        saveLocalItem(mItem);
//                    }
//                } else {
//                    mItem = getBringItem();
//                }
//            } else {
//                if (isStoragePermissions()) {
//                    if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
//                        mItem = getLocalItem();
//                    } else {
//                        mItem = getBringItem();
//                        saveLocalItem(mItem);
//                    }
//
//                } else {
//                    if(TextUtils.isEmpty(getLocalItemCache())){
//                        mItem = getBringItem();
//                        saveLocalItemCache(mItem);
//                    }else{
//                        mItem = getLocalItemCache();
//                    }
//                }
//            }
//        } else {
//            if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
//                mItem = getLocalItem();
//            } else {
//                mItem = getBringItem();
//                saveLocalItem(mItem);
//            }
//        }
//
//        return mItem;
//    }



    public String getImei() {
        String mItem = null;
        if (ExternalStorage.isFileExist(mPath)) {     //本地有存储
            mItem = getLocalItem();
        } else {
            mItem = getDeviceId();
            Log.e(TAG,"本地没存储 新生成->>>>>"+mItem);
            if (!TextUtils.isEmpty(mItem)){
                saveLocalItem(mItem);
            }else {
                Log.e(TAG,"mItem is null");
            }
        }
        return mItem;
    }



    public String getDeviceId(){
        return Cfg.loadStr(MyApplication.getContext(), "device_id", "");
    }




    /**
     * 是否有手机状态权限
     *
     * @return
     */
    private boolean isPhonePermissions() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 是否有外部存储权限
     *
     * @return
     */
    public boolean isStoragePermissions() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    public boolean isStoragePermissions2() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 保存标识到本地
     */
    public boolean saveLocalItem(String mItem) {
        Utils.saveNewOrOldUser("0");    //保存是新用户
        return ExternalStorage.saveFileToSDCardCustomDir(mItem.getBytes(), "YueMei", "YueMeiImei");
    }

    /**
     * 保存标识到缓存
     */
    private void saveLocalItemCache(String mItem) {
        Utils.saveNewOrOldUser("0");    //保存是新用户
        Cfg.saveStr(MyApplication.getContext(), FinalConstant.YUEMEI_ITEM, mItem);
    }

    /**
     *  获取本地缓存数据
     */
    public String getLocalItemCache() {
        return Cfg.loadStr(MyApplication.getContext(), FinalConstant.YUEMEI_ITEM, "");
    }

    /**
     * 获取本地存储的标识
     */
    public String getLocalItem() {
        byte[] bytes = ExternalStorage.loadFileFromSDCard(mPath);
        if (bytes != null){
            return new String(bytes);
        }
        return null;
    }

    /**
     * 获取手机自带的唯一标识
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String getBringItem() {
        String result;
        try {
            TelephonyManager TelephonyMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            result = TelephonyMgr.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"获取设备唯一标识异常 ===="+e.toString());
            result = getBringItem2();
        }

        if (TextUtils.isEmpty(result) || "0".equals(result)) {
            Log.e(TAG,"获取设备唯一标识为空 NULL");
            result = getBringItem2();
        }

        return result;
    }



    @SuppressLint("MissingPermission")
    public static String getBringItem2() {

        String serial = null;

        String m_szDevIDShort = "35" +
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +

                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +

                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +

                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +

                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +

                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +

                Build.USER.length() % 10; //13 位

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                serial = android.os.Build.getSerial();
            } else {
                serial = Build.SERIAL;
            }
            //API>=9 使用serial号
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            //serial需要一个初始化
            serial = "serial"; // 随便一个初始化
        }
        //使用硬件信息拼凑出来的15位号码
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }




    /**
     * 自己生成的标识
     */
//    public String generateItem() {
//        String carrier = Build.MANUFACTURER + System.currentTimeMillis();
//        return MD5.getMessageDigest(carrier.getBytes());
//    }

    /**
     * 唯一标识存储路径
     *
     * @return
     */
    public String getmPath() {
        return mPath;
    }




}
