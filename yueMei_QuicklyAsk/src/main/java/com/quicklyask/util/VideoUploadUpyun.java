package com.quicklyask.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.upyun.UpYun;
import com.upyun.library.common.Params;
import com.upyun.library.common.ResumeUploader;
import com.upyun.library.common.UploadEngine;
import com.upyun.library.exception.UpYunException;
import com.upyun.library.listener.UpCompleteListener;
import com.upyun.library.listener.UpProgressListener;
import com.upyun.library.utils.UpYunUtils;

import net.sf.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by 裴成浩 on 2017/7/6.
 * 视频上传类
 */

public class VideoUploadUpyun {

    private final String TAG = "VideoUploadUpyun";
    //空间名
    public String SPACE = "yuemei";
    //操作员
    public String OPERATER = "guanliyuan01";
    //密码
    public String PASSWORD = "yuemei2017_01";
    private final Handler mHandler;

    public String savePath;                //网络请求路径

    public ArrayList<Map<String, String>> watermark = new ArrayList<>();

    private Context mContext;
    private ResumeUploader uploader;
    private String _id;

    public static VideoUploadUpyun getVideoUploadUpyun(Context context, Handler handler) {
        return new VideoUploadUpyun(context, handler);
    }


    private VideoUploadUpyun(Context context, Handler handler) {
        this.mContext = context;
        this.mHandler = handler;
        savePath = "/{year}{mon}/" + md5(System.currentTimeMillis() + new Random(10000).nextInt() + "android") + "{.suffix}";
//        savePath = "/201708/701efe33fecc9dea97558ce4645b85ca.mp4";
        // voicepath = /voice/{year}{mon}/
    }



    /**
     * 上传视频
     */
    public void uploadVideo(String videoPath) {
        Log.e(TAG, "videoPath == " + videoPath);
        //判断upyun端有没重名的文件
        UpYun upyun = new UpYun(SPACE, OPERATER, PASSWORD);
        List<UpYun.FolderItem> items = upyun.readDir(savePath);
        Log.e(TAG, "items == " + items);
        if (items != null && items.size() != 0) {
            savePath = "/{year}{mon}/" + md5(System.currentTimeMillis() + new Random(10000).nextInt() + "android") + "{.suffix}";
        }

        final File videoFile = new File(videoPath);
        //参数键值对
        final Map<String, Object> paramsMap = new HashMap<>();
        //上传空间
        paramsMap.put(Params.BUCKET, SPACE);

        Log.e(TAG, "savePath == " + savePath);
        //保存路径（重命名文件用的。避免文件被覆盖）
        paramsMap.put(Params.SAVE_KEY, savePath);
        //要上传的文件（加密）

        paramsMap.put(Params.CONTENT_MD5, UpYunUtils.md5Hex(videoFile));

        //进度回调，可为空
        UpProgressListener progressListener = new UpProgressListener() {
            @Override
            public void onRequestProgress(final long bytesWrite, final long contentLength) {
                int prog = (int) ((100 * bytesWrite) / contentLength);
                Message obtain = Message.obtain();
                obtain.arg1 = prog;
                obtain.what = 7;
                mHandler.sendMessage(obtain);
            }
        };

        //结束回调，不可为空
        UpCompleteListener completeListener = new UpCompleteListener() {
            @Override
            public void onComplete(boolean isSuccess, String result) {
                Log.e(TAG, isSuccess + ":" + result);
                if (isSuccess) {
                    WriteVideoResult videoResult = JSONUtil.TransformWriteVideoResult(result);

                    Message obtain = Message.obtain();
                    obtain.obj = videoResult;
                    obtain.what = 8;
                    mHandler.sendMessage(obtain);
                } else {

                    Message obtain = Message.obtain();
                    obtain.obj = videoFile;
                    obtain.what = 9;
                    mHandler.sendMessage(obtain);
                }

            }
        };

        //表单上传（本地签名方式）
        UploadEngine.getInstance().formUpload(videoFile, paramsMap, OPERATER, UpYunUtils.md5(PASSWORD), completeListener, progressListener);


        /*****************断电续传初始化***************/
        //初始化断点续传
        uploader = new ResumeUploader(SPACE, OPERATER, UpYunUtils.md5(PASSWORD));

        //设置 MD5 校验
        uploader.setCheckMD5(true);

        //设置进度监听
        uploader.setOnProgressListener(new ResumeUploader.OnProgressListener() {
            @Override
            public void onProgress(int index, int total) {
//                Log.e(TAG, index + ":" + total);
            }
        });


    }


    public void uplodVoice(String voicePath) {
        Log.e(TAG, "voicePath == " + voicePath);
        String upLoadPath = "/voice/{year}{mon}/" + getFileName(voicePath) + "{.suffix}";
        Log.e(TAG, "upLoadPath == " + upLoadPath);
        //判断upyun端有没重名的文件
        UpYun upyun = new UpYun(SPACE, OPERATER, PASSWORD);
        List<UpYun.FolderItem> items = upyun.readDir(upLoadPath);
        Log.e(TAG, "items == " + items);

        final File videoFile = new File(voicePath);
        //参数键值对
        final Map<String, Object> paramsMap = new HashMap<>();
        //上传空间
        paramsMap.put(Params.BUCKET, SPACE);

        //保存路径（重命名文件用的。避免文件被覆盖）
        paramsMap.put(Params.SAVE_KEY, upLoadPath);
        //要上传的文件（加密）
//        paramsMap.put(Params.DATE, videoFile);

        //进度回调，可为空
        UpProgressListener progressListener = new UpProgressListener() {
            @Override
            public void onRequestProgress(final long bytesWrite, final long contentLength) {
                int prog = (int) ((100 * bytesWrite) / contentLength);
                Message obtain = Message.obtain();
                obtain.arg1 = prog;
                obtain.what = 7;
                mHandler.sendMessage(obtain);
            }
        };

        //结束回调，不可为空
        UpCompleteListener completeListener = new UpCompleteListener() {
            @Override
            public void onComplete(boolean isSuccess, String result) {
                Log.e(TAG, isSuccess + ":" + result);
                if (isSuccess) {
                    WriteVideoResult videoResult = JSONUtil.TransformWriteVideoResult(result);

                    Message obtain = Message.obtain();
                    obtain.obj = videoResult;
                    obtain.what = 8;
                    mHandler.sendMessage(obtain);
                } else {

                    Message obtain = Message.obtain();
                    obtain.obj = videoFile;
                    obtain.what = 9;
                    mHandler.sendMessage(obtain);
                }

            }
        };

        //表单上传（本地签名方式）
        UploadEngine.getInstance().formUpload(videoFile, paramsMap, OPERATER, UpYunUtils.md5(PASSWORD), completeListener, progressListener);


        /*****************断电续传初始化***************/
        //初始化断点续传
        uploader = new ResumeUploader(SPACE, OPERATER, UpYunUtils.md5(PASSWORD));

        //设置 MD5 校验
//        uploader.setCheckMD5(true);

        //设置进度监听
        uploader.setOnProgressListener(new ResumeUploader.OnProgressListener() {
            @Override
            public void onProgress(int index, int total) {
//                Log.e(TAG, index + ":" + total);
            }
        });
    }


    public String getFileName(String pathandname) {

        int start = pathandname.lastIndexOf("/");
        int end = pathandname.lastIndexOf(".");
        if (start != -1 && end != -1) {
            return pathandname.substring(start + 1, end);
        } else {
            return null;
        }

    }

    /**
     * 设置视频水印
     *
     * @return
     */
    public String setWatermark() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", "naga");
        map.put("type", "video");
        map.put("avopts", "wmImg//wmGravity/northeast");
        watermark.add(map);
        JSONArray apps = JSONArray.fromObject(watermark);
        return apps.toString();
    }


    //断点续传使用
    public void resumeUpdate(final File file, final Map<String, String> params) {

        new Thread() {
            @Override
            public void run() {
                try {
                    //同步方法网络请求，Android 上自行管理线程
                    uploader.upload(file, "/test1.txt", params);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UpYunException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * md5加密图片名
     *
     * @param string
     * @return
     */
    public static String md5(String string) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(string.getBytes());
            String result = "";
            for (byte b : bytes) {
                String temp = Integer.toHexString(b & 0xff);
                if (temp.length() == 1) {
                    temp = "0" + temp;
                }
                result += temp;
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
