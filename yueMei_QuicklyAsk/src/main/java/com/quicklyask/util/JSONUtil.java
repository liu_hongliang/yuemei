package com.quicklyask.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.module.community.model.bean.SearchAboutData;
import com.module.community.model.bean.ZhuanTi;
import com.module.doctor.model.bean.CaseFinalData;
import com.module.doctor.model.bean.CaseFinalPic;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.DocHeadData;
import com.module.doctor.model.bean.PartAskData;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.my.model.bean.BotherData;
import com.module.my.model.bean.GetCodeData;
import com.module.my.model.bean.JiFenMoenyData;
import com.module.my.model.bean.NoteBookListData;
import com.module.my.model.bean.Province2ListData;
import com.module.my.model.bean.RegisterData;
import com.module.my.model.bean.TaoOrderInfoData;
import com.module.my.model.bean.UserData;
import com.module.my.model.bean.VideoPlay;
import com.module.other.module.bean.TaoPopItemData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.entity.PrePayIdData;
import com.quicklyask.entity.ProjectTwoTree;
import com.quicklyask.entity.ShouKuan;
import com.quicklyask.entity.SignInResult;
import com.quicklyask.entity.UserPhoneData;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.entity.WriteVideoResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 将json串转换面对象列表
 *
 * @author MoringSun
 */
public class JSONUtil {

    /**
     * Map转换成JSON字符?build JSON String
     *
     * @param parameters
     * @return JSONObjectString
     * @throws JSONException
     */
    public static String toJSONString(Map<String, Object> parameters)
            throws Exception {
        JSONObject json = new JSONObject();

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            json.put(entry.getKey(), entry.getValue());
        }
        return json.toString();
    }

    public static String toJSONString2(Map<String, JSONObject> parameters)
            throws Exception {
        JSONObject json = new JSONObject();

        for (Map.Entry<String, JSONObject> entry : parameters.entrySet()) {
            json.put(entry.getKey(), entry.getValue());
        }
        return json.toString();
    }

    /**
     * 解析只有一个键值对的json串
     *
     * @param jsonStr
     */
    public static String resolveJson(String jsonStr, String type) {
        String result = "";
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            result = jsonObject.getString(type);
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解析没有数组的json字符串成javaBean
     *
     * @param jsonStr
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T TransformSingleBean(String jsonStr, Class<T> clazz) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create();
        return gson.fromJson(jsonStr, clazz);
    }

    public static <T> T TransformSingleBean(JsonReader jsonStr, Class<T> clazz) {
        return new Gson().fromJson(jsonStr, clazz);
    }

    /**
     *
     * String转map
     * @param str
     * @return
     */
    public static Map<String,Object> getStringToMap(String str){
        //根据逗号截取字符串数组
        String[] str1 = str.split(",");
        //创建Map对象
        Map<String,Object> map = new HashMap<>();
        //循环加入map集合
        for (int i = 0; i < str1.length; i++) {
            //根据":"截取字符串数组
            String[] str2 = str1[i].split(":");
            //str2[0]为KEY,str2[1]为值
            map.put(str2[0],str2[1]);
        }
        return map;
}


    /**
     * 解析有数组的json字符串成javabean
     *
     * @param jsonStr
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> jsonToArrayList(JsonReader jsonStr, Class<T> clazz) {
        Type type = new TypeToken<ArrayList<JsonObject>>() {
        }.getType();
        ArrayList<JsonObject> jsonObjects = new Gson().fromJson(jsonStr, type);

        ArrayList<T> arrayList = new ArrayList<>();
        for (JsonObject jsonObject : jsonObjects) {
            arrayList.add(new Gson().fromJson(jsonObject, clazz));
        }
        return arrayList;
    }

    /**
     * 解析有数组的json字符串成javabean
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> jsonToArrayList(String json, Class<T> clazz) {
        Type type = new TypeToken<ArrayList<JsonObject>>() {
        }.getType();
        Gson gson = new Gson();
        ArrayList<JsonObject> jsonObjects = gson.fromJson(json, type);

        ArrayList<T> arrayList = new ArrayList<>();
        for (JsonObject jsonObject : jsonObjects) {
            arrayList.add(gson.fromJson(jsonObject, clazz));
        }
        return arrayList;
    }

    public static List<NoteBookListData> transformNoteBookList(String jsonStr) {
        Type typeList = new TypeToken<List<NoteBookListData>>() {
        }.getType();
        Gson gson = new Gson();
        List<NoteBookListData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    public static List<HomeTaoData> transHomeTaoData(String jsonStr) {
        Type typeList = new TypeToken<List<HomeTaoData>>() {
        }.getType();
        Gson gson = new Gson();
        List<HomeTaoData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    public static List<PartAskData> transformPartAsk(String jsonStr) {
        Type typeList = new TypeToken<List<PartAskData>>() {
        }.getType();
        Gson gson = new Gson();
        List<PartAskData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    public static List<ZhuanTi> transformZhuanTi(String jsonStr) {
        Type typeList = new TypeToken<List<ZhuanTi>>() {
        }.getType();
        Gson gson = new Gson();
        List<ZhuanTi> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 医生详情页头部信息
     *
     * @param jsonStr
     * @return
     */
    public static DocHeadData TransformDodecHead(String jsonStr) {
        Type typeList = new TypeToken<DocHeadData>() {
        }.getType();
        Gson gson = new Gson();
        DocHeadData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 项目首页头部数据
     *
     * @param
     * @return list<Object>
     */
    public static List<ProjectTwoTree> TransformProjectTree(String jsonStr) {
        Type typeList = new TypeToken<List<ProjectTwoTree>>() {
        }.getType();
        Gson gson = new Gson();
        List<ProjectTwoTree> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 热门城市列表
     *
     * @param
     * @return list<Object>
     */
    public static List<CityDocDataitem> TransformCityDocDataitem(String jsonStr) {
        Type typeList = new TypeToken<List<CityDocDataitem>>() {
        }.getType();
        Gson gson = new Gson();
        List<CityDocDataitem> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 仅获取防打扰数据
     *
     * @param
     * @return list<Object>
     */
    public static BotherData TransformBotherData(String jsonStr) {
        Type typeList = new TypeToken<BotherData>() {
        }.getType();
        Gson gson = new Gson();
        BotherData object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 搜索关联词
     *
     * @param
     * @return list<Object>
     */
    public static ArrayList<SearchAboutData> TransformSearchAboutData(String jsonStr) {
        Type typeList = new TypeToken<List<SearchAboutData>>() {
        }.getType();
        Gson gson = new Gson();
        ArrayList<SearchAboutData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 颜值币经验
     *
     * @param
     * @return list<Object>
     */
    public static JiFenMoenyData TransformJFMoney(String jsonStr) {
        Type typeList = new TypeToken<JiFenMoenyData>() {
        }.getType();
        Gson gson = new Gson();
        JiFenMoenyData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 微信支付预订id
     *
     * @param
     * @return list<Object>
     */
    public static PrePayIdData TransformWXpayidShare(String jsonStr) {
        Type typeList = new TypeToken<PrePayIdData>() {
        }.getType();
        Gson gson = new Gson();
        PrePayIdData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 或分享数据
     *
     * @param
     * @return list<Object>
     */
    public static TaoOrderInfoData TransformOrderInfo(String jsonStr) {
        Type typeList = new TypeToken<TaoOrderInfoData>() {
        }.getType();
        Gson gson = new Gson();
        TaoOrderInfoData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 或分享数据
     *
     * @param
     * @return list<Object>
     */
    public static UserPhoneData TransformUserPhone(String jsonStr) {
        Type typeList = new TypeToken<UserPhoneData>() {
        }.getType();
        Gson gson = new Gson();
        UserPhoneData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 获取注册返回数据
     *
     * @param
     * @return list<Object>
     */
    public static RegisterData TransformRegister(String jsonStr) {
        Type typeList = new TypeToken<RegisterData>() {
        }.getType();
        Gson gson = new Gson();
        RegisterData object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 首页淘整形列表
     *
     * @param jsonStr
     * @return
     */
    public static List<HomeTaoData> TransformHomeTao548(String jsonStr) {
        Type typeList = new TypeToken<List<HomeTaoData>>() {
        }.getType();
        Gson gson = new Gson();
        List<HomeTaoData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 获取城市列表
     *
     * @param jsonStr
     * @return
     */
    public static List<Province2ListData> TransformCity2(String jsonStr) {
        Type typeList = new TypeToken<List<Province2ListData>>() {
        }.getType();
        Gson gson = new Gson();
        List<Province2ListData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 获取淘整形下拉列表
     *
     * @param jsonStr
     * @return
     */
    public static TaoPopItemData TransformTaoPop(String jsonStr) {
        Type typeList = new TypeToken<TaoPopItemData>() {
        }.getType();
        Gson gson = new Gson();
        TaoPopItemData object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 获取淘整形下拉列表
     *
     * @param jsonStr
     * @return
     */
    public static List<TaoPopItemIvData> TransformProjectPop(String jsonStr) {
        Type typeList = new TypeToken<List<TaoPopItemIvData>>() {
        }.getType();
        Gson gson = new Gson();
        List<TaoPopItemIvData> object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 案例最终页
     *
     * @param jsonStr
     * @return
     */
    public static CaseFinalData TransformCaseFinalData(String jsonStr) {
        Type typeList = new TypeToken<CaseFinalData>() {
        }.getType();
        Gson gson = new Gson();
        CaseFinalData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 医院图片
     *
     * @param jsonStr
     * @return
     */
    public static List<CaseFinalPic> TransformHosPic(String jsonStr) {
        Type typeList = new TypeToken<List<CaseFinalPic>>() {
        }.getType();
        Gson gson = new Gson();
        List<CaseFinalPic> object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 收款信息
     *
     * @param jsonStr
     * @return
     */
    public static ShouKuan.DataBean TransformShouKuan(String jsonStr) {
        Type typeList = new TypeToken<ShouKuan.DataBean>() {
        }.getType();
        Gson gson = new Gson();
        ShouKuan.DataBean object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 邀请好友数据
     *
     * @param jsonStr
     * @return
     */
    public static GetCodeData TransformInvi(String jsonStr) {
        Type typeList = new TypeToken<GetCodeData>() {
        }.getType();
        Gson gson = new Gson();
        GetCodeData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 登录返回
     *
     * @param
     * @return list<Object>
     */
    public static UserData TransformLogin1(String jsonStr) {
        Type typeList = new TypeToken<UserData>() {
        }.getType();
        Gson gson = new Gson();
        UserData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    public static UserData TransformLogin(String jsonStr) {
        Type typeList = new TypeToken<UserData>() {
        }.getType();
        Gson gson = new Gson();
        UserData object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 发帖 返回数据
     *
     * @param
     * @return list<Object>
     */
    public static WriteResultData TransformWriteResult(String jsonStr) {
        Type typeList = new TypeToken<WriteResultData>() {
        }.getType();
        Gson gson = new Gson();
        WriteResultData object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 发帖视频上传 返回数据
     *
     * @param
     * @return list<Object>
     */
    public static WriteVideoResult TransformWriteVideoResult(String jsonStr) {
        Type typeList = new TypeToken<WriteVideoResult>() {
        }.getType();
        Gson gson = new Gson();
        WriteVideoResult object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 签到 返回数据
     *
     * @param
     * @return list<Object>
     */
    public static SignInResult.DataBean TransformSignResult(String jsonStr) {
        Type typeList = new TypeToken<SignInResult.DataBean>() {
        }.getType();
        Gson gson = new Gson();
        SignInResult.DataBean object = gson.fromJson(jsonStr, typeList);
        return object;
    }

    /**
     * 获取版本号
     *
     * @param
     * @return list<Object>
     */
    public static VideoPlay.DataBean TransformVideoPlay(String jsonStr) {
        Type typeList = new TypeToken<VideoPlay.DataBean>() {
        }.getType();
        Gson gson = new Gson();
        VideoPlay.DataBean object = gson.fromJson(jsonStr, typeList);
        return object;
    }


    /**
     * 将map集合转为json格式
     *
     * @param obj
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static String mapTojson(Object obj) throws IllegalArgumentException, IllegalAccessException {

        StringBuffer buffer = new StringBuffer();
        Class<? extends Object> clazz = obj.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        Field.setAccessible(declaredFields, true);
        buffer.append("[");
        Map<Object, Object> map = (Map<Object, Object>) obj;

        //通过Map.entrySet使用iterator(迭代器)遍历key和value
        Set<Map.Entry<Object, Object>> set = map.entrySet();
        Iterator iterator = set.iterator();
        buffer.append("{");

        while (iterator.hasNext()) {

            //使用Map.Entry接到通过迭代器循环出的set的值
            Map.Entry mapentry = (Map.Entry) iterator.next();
            Object value = mapentry.getValue();

            //使用getKey()获取map的键，getValue()获取键对应的值
            String valuename = value.getClass().getSimpleName();
            if (valuename.equals("String")) {

                buffer.append("\"" + mapentry.getKey() + "\":\"" + mapentry.getValue() + "\",");
            } else if (valuename.equals("Boolean") || valuename.equals("Integer") || valuename.equals("Double") || valuename.equals("Float") || valuename.equals("Long")) {

                buffer.append("\"" + mapentry.getKey() + "\":" + mapentry.getValue() + ",");
            } else if (valuename.equals("Date")) {
                Date date = (Date) value;
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String simdate = simpleDateFormat.format(date);
                buffer.append("\"" + mapentry.getKey() + "\":\"" + simdate + "\",");
            } else if (valuename.equals("ArrayList") || valuename.equals("LinkedList")) {
                List<Object> list = (List<Object>) value;
                buffer.append("\"" + mapentry.getKey() + "\":[");
                buffer = listTojson(buffer, list).append("]");
            } else if (valuename.equals("HashSet") || valuename.equals("TreeSet")) {
                buffer.append("\"" + mapentry.getKey() + "\":[");
                Set<Object> sets = (Set<Object>) value;
                buffer = setTojson(sets, buffer).append("]");
            } else if (valuename.equals("HashMap") || valuename.equals("HashTable")) {
                buffer.append("\"" + mapentry.getKey() + "\":");
                StringBuffer mapbuffer = new StringBuffer(mapTojson(value));
                mapbuffer.deleteCharAt(0);
                buffer.append(mapbuffer);
            } else {
                buffer.append("\"" + mapentry.getKey() + "\":");
                buffer.append("{");

                Class<? extends Object> class1 = value.getClass();
                Field[] fields = class1.getDeclaredFields();
                Field.setAccessible(fields, true);

                for (Field field : fields) {

                    Object object = field.get(value);
                    String fieldName = field.getType().getSimpleName();

                    if (object == null) {
                        if (fieldName.equals("String")) {
                            buffer.append("\"" + field.getName() + "\":\"\",");
                        } else {
                            buffer.append("\"" + field.getName() + "\":null,");
                        }

                    } else {

                        Class<? extends Object> fieldclass = field.get(value).getClass();
                        String simpleName = fieldclass.getSimpleName();
                        if (simpleName.equals("String")) {

                            buffer.append("\"" + field.getName() + "\":\"" + field.get(value) + "\",");
                        } else if (simpleName.equals("Boolean") || simpleName.equals("Integer") || simpleName.equals("Double") || simpleName.equals("Float") || simpleName.equals("Long")) {

                            buffer.append("\"" + field.getName() + "\":" + field.get(value) + ",");
                        } else if (simpleName.equals("Date")) {
                            Date date = (Date) object;
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String simdate = simpleDateFormat.format(date);
                            buffer.append("\"" + field.getName() + "\":\"" + simdate + "\",");
                        } else if (simpleName.equals("ArrayList") || simpleName.equals("LinkedList")) {

                            List<Object> list = (List<Object>) object;
                            buffer.append("\"" + field.getName() + "\":[");
                            StringBuffer append = listTojson(buffer, list).append("]");
                            buffer.append(append);
                        } else if (simpleName.equals("HashSet") || simpleName.equals("TreeSet")) {

                            buffer.append("\"" + field.getName() + "\":[");
                            Set<Object> sets = (Set<Object>) object;
                            buffer = setTojson(sets, buffer).append("]");
                        } else if (simpleName.equals("HashMap") || simpleName.equals("HashTable")) {

                            buffer.append("\"" + field.getName() + "\":");
                            StringBuffer mapbuffer = new StringBuffer(mapTojson(object));
                            mapbuffer.deleteCharAt(0);
                            buffer.append(mapbuffer);
                        } else {
                            buffer = beanTojson(object, buffer).append(",");

                        }
                    }

                }

                buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
                buffer.append("},");
            }

        }

        buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
        return buffer.toString() + "}]";
    }


    /**
     * 将不是基本类型的字段转为json格式
     *
     * @param obj
     * @param buffer
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static StringBuffer beanTojson(Object obj, StringBuffer buffer) throws IllegalArgumentException, IllegalAccessException {

        Class<? extends Object> clazz = obj.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        Field.setAccessible(declaredFields, true);

        buffer.append("\"" + clazz.getSimpleName() + "\":{");

        for (Field field : declaredFields) {

            Object object = field.get(obj);
            String fieldName = field.getType().getSimpleName();

            if (object == null) {
                if (fieldName.equals("String")) {
                    buffer.append("\"" + field.getName() + "\":\"\",");
                } else {
                    buffer.append("\"" + field.getName() + "\":null,");
                }

            } else {

                Class<? extends Object> fieldclass = object.getClass();
                String simpleName = fieldclass.getSimpleName();

                if (simpleName.equals("String")) {

                    buffer.append("\"" + field.getName() + "\":\"" + field.get(obj) + "\",");
                } else if (simpleName.equals("Boolean") || simpleName.equals("Integer") || simpleName.equals("Double") || simpleName.equals("Float") || simpleName.equals("Long")) {

                    buffer.append("\"" + field.getName() + "\":" + field.get(obj) + ",");
                } else if (simpleName.equals("Date")) {

                    Date date = (Date) object;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String simdate = simpleDateFormat.format(date);
                    buffer.append("\"" + field.getName() + "\":\"" + simdate + "\",");
                } else if (simpleName.equals("ArrayList") || simpleName.equals("LinkedList")) {

                    List<Object> list = (List<Object>) object;
                    buffer = listTojson(buffer, list);
                } else if (simpleName.equals("HashSet") || simpleName.equals("TreeSet")) {

                    Set<Object> set = (Set<Object>) object;
                    buffer = setTojson(set, buffer);
                } else if (simpleName.equals("HashMap") || simpleName.equals("HashTable")) {

                    buffer.append("\"" + field.getName() + "\":");
                    StringBuffer mapbuffer = new StringBuffer(mapTojson(object));
                    mapbuffer.deleteCharAt(0);
                    buffer.append(mapbuffer);
                } else {
                    buffer = beanTojson(object, buffer).append("}");
                }
            }

        }

        buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
        buffer.append("}");

        return buffer;
    }


    /**
     * 将list数组转为json格式
     *
     * @param buffer
     * @param list
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static StringBuffer listTojson(StringBuffer buffer, List list) throws IllegalArgumentException, IllegalAccessException {

        //遍历传过来的list数组
        for (Object object : list) {

            //判断遍历出的值是否为空
            if (object == null) {
                buffer.append(",");
            } else {

                Class<? extends Object> class1 = object.getClass();
                String simpleName = class1.getSimpleName();

                if (simpleName.equals("String")) {

                    buffer.append("\"" + object.toString() + "\",");
                } else if (simpleName.equals("Boolean") || simpleName.equals("Integer") || simpleName.equals("Double") || simpleName.equals("Float") || simpleName.equals("Long")) {

                    buffer.append("" + object.toString() + ",");
                } else if (simpleName.equals("Date")) {
                    Date date = (Date) object;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String simdate = simpleDateFormat.format(date);
                    buffer.append("" + simdate + ",");
                } else {

                    Class<? extends Object> class2 = object.getClass();
                    Field[] fields = class2.getDeclaredFields();
                    Field.setAccessible(fields, true);
                    buffer.append("{");
                    //遍历对象中的所有字段获取字段值和字段名称拼成json字符串
                    for (Field field : fields) {

                        Object fieldobj = field.get(object);
                        String fieldName = field.getType().getSimpleName();

                        if (fieldobj == null) {

                            if (fieldName.equals("String")) {
                                buffer.append("\"" + field.getName() + "\":\"\",");
                            } else {
                                buffer.append("\"" + field.getName() + "\":null,");
                            }

                        } else {

                            String fsimpleName = fieldobj.getClass().getSimpleName();

                            if (fsimpleName.equals("String")) {

                                buffer.append("\"" + field.getName() + "\":\"" + field.get(object) + "\",");
                            } else if (fsimpleName.equals("Boolean") || fsimpleName.equals("Integer") || fsimpleName.equals("Double") || fsimpleName.equals("Float") || fsimpleName.equals("Long")) {

                                buffer.append("\"" + field.getName() + "\":" + field.get(object) + ",");
                            } else if (fsimpleName.equals("Date")) {

                                Date date = (Date) object;
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                String simdate = simpleDateFormat.format(date);
                                buffer.append("\"" + field.getName() + "\":" + simdate + ",");
                            } else {

                                buffer = beanTojson(fieldobj, buffer).append(",");
                            }
                        }

                    }

                    buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
                    buffer.append("},");
                }
            }

        }

        buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
        buffer.append("]");

        return buffer;
    }


    /**
     * 将set数组转为json格式
     *
     * @param set
     * @param buffer
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static StringBuffer setTojson(Set set, StringBuffer buffer) throws IllegalArgumentException, IllegalAccessException {

        for (Object object : set) {
            if (object == null) {
                buffer.append("" + "null" + ",");
            } else {

                Class<? extends Object> class1 = object.getClass();

                //判断集合中的值是否为java基本类型
                String simpleName = class1.getSimpleName();
                if (simpleName.equals("String")) {

                    buffer.append("\"" + object.toString() + "\",");
                } else if (simpleName.equals("Boolean") || simpleName.equals("Integer") || simpleName.equals("Double") || simpleName.equals("Float") || simpleName.equals("Long")) {

                    buffer.append("" + object.toString() + ",");
                } else if (simpleName.equals("Date")) {

                    Date date = (Date) object;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String simdate = simpleDateFormat.format(date);
                    buffer.append("" + simdate + ",");
                } else {

                    Class<? extends Object> class2 = object.getClass();
                    Field[] fields = class2.getDeclaredFields();
                    Field.setAccessible(fields, true);
                    buffer.append("{");

                    //遍历对象中的所有字段获取字段值和字段名称拼成json字符串
                    for (Field field : fields) {

                        Object fieldobj = field.get(object);
                        String fieldName = field.getType().getSimpleName();

                        if (object == null) {
                            if (fieldName.equals("String")) {
                                buffer.append("\"" + field.getName() + "\":\"\",");
                            } else {
                                buffer.append("\"" + field.getName() + "\":null,");
                            }

                        } else {

                            String fsimpleName = fieldobj.getClass().getSimpleName();
                            if (fsimpleName.equals("String")) {

                                buffer.append("\"" + field.getName() + "\":\"" + field.get(object) + "\",");
                            } else if (fsimpleName.equals("Boolean") || fsimpleName.equals("Integer") || fsimpleName.equals("Double") || fsimpleName.equals("Float") || fsimpleName.equals("Long")) {

                                buffer.append("\"" + field.getName() + "\":" + field.get(object) + ",");
                            } else if (fsimpleName.equals("Date")) {

                                Date date = (Date) object;
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                String simdate = simpleDateFormat.format(date);
                                buffer.append("\"" + field.getName() + "\":" + simdate + ",");
                            } else {

                                buffer = beanTojson(fieldobj, buffer).append(",");
                            }
                        }
                    }

                    buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
                    buffer.append("},");
                }
            }
        }

        buffer = new StringBuffer("" + buffer.substring(0, buffer.length() - 1) + "");
        return buffer;
    }
    /**
     * Json 转成 Map<>
     * @param jsonStr
     * @return
     */
    public static Map<String, Object> getMapForJson(String jsonStr){
        JSONObject jsonObject ;
        try {
            jsonObject = new JSONObject(jsonStr);

            Iterator<String> keyIter= jsonObject.keys();
            String key;
            Object value ;
            Map<String, Object> valueMap = new HashMap<String, Object>();
            while (keyIter.hasNext()) {
                key = keyIter.next();
                value = jsonObject.get(key);
                valueMap.put(key, value);
            }
            return valueMap;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }


}
