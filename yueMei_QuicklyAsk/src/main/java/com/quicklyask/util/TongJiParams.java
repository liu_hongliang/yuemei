package com.quicklyask.util;

public class TongJiParams {
    private String event_name;
    private String event_pos;
    private String hos_id;
    private String doc_id;
    private String tao_id;
    private String event_others;
    private String id;
    private String referrer;
    private String type;

    public String getEvent_name() {
        return event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public String getHos_id() {
        return hos_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public String getTao_id() {
        return tao_id;
    }

    public String getEvent_others() {
        return event_others;
    }

    public String getId() {
        return id;
    }

    public String getReferrer() {
        return referrer;
    }

    public String getType() {
        return type;
    }

    public static class TongJiParamsBuilder{
        private String event_name;
        private String event_pos;
        private String hos_id;
        private String doc_id;
        private String tao_id;
        private String event_others;
        private String id;
        private String referrer;
        private String type;

        public TongJiParamsBuilder setEvent_name(String event_name) {
            this.event_name = event_name;
            return this;
        }

        public TongJiParamsBuilder setEvent_pos(String event_pos) {
            this.event_pos = event_pos;
            return this;
        }

        public TongJiParamsBuilder setHos_id(String hos_id) {
            this.hos_id = hos_id;
            return this;
        }

        public TongJiParamsBuilder setDoc_id(String doc_id) {
            this.doc_id = doc_id;
            return this;
        }

        public TongJiParamsBuilder setTao_id(String tao_id) {
            this.tao_id = tao_id;
            return this;
        }

        public TongJiParamsBuilder setEvent_others(String event_others) {
            this.event_others = event_others;
            return this;
        }

        public TongJiParamsBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public TongJiParamsBuilder setReferrer(String referrer) {
            this.referrer = referrer;
            return this;
        }

        public TongJiParamsBuilder setType(String type) {
            this.type = type;
            return this;
        }

        public TongJiParams build(){
            return new TongJiParams(this);
        }
    }
    private TongJiParams(TongJiParamsBuilder builder){
        this.event_name=builder.event_name;
        this.event_pos=builder.event_pos;
        this.hos_id=builder.hos_id;
        this.doc_id=builder.doc_id;
        this.tao_id=builder.tao_id;
        this.event_others=builder.event_others;
        this.event_pos=builder.event_pos;
        this.id=builder.id;
        this.referrer=builder.referrer;
        this.type=builder.type;
    }
}
