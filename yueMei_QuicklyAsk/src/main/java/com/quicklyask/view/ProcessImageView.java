package com.quicklyask.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import com.quicklyask.activity.R;

import org.xutils.common.util.DensityUtil;

/**
 * Created by 裴成浩 on 2017/6/8.
 * 图片上传动画效果
 */

public class ProcessImageView extends AppCompatImageView {

    private Paint mPaint;// 画笔
    int width = 0;
    int height = 0;
    Context context = null;
    int progress = 0;
    public static final int DENG_DAI = 1;
    public static final int SAHNG_CHUAN_ZHONG = 2;
    public static final int WANC_HENG = 3;
    public static final int SHI_BAI = 4;

    private int mImgState = DENG_DAI;

    RectF mViewRectB = new RectF();
    RectF mViewRectC = new RectF();
    RectF mViewRectQ = new RectF();

    /**
     * 图片的类型，圆形or圆角
     */
    private int type;
    public static final int TYPE_CIRCLE = 0;
    public static final int TYPE_ROUND = 1;
    /**
     * 圆角大小的默认值
     */
    private static final int BODER_RADIUS_DEFAULT = 10;

    /**
     * 圆角的大小
     */
    private int mBorderRadius;

    /**
     * 绘图的Paint
     */
    private Paint mBitmapPaint;
    /**
     * 圆角的半径
     */
    private int mRadius;
    /**
     * 3x3 矩阵，主要用于缩小放大
     */
    private Matrix mMatrix;
    /**
     * 渲染图像，使用图像为绘制图形着色
     */
    private BitmapShader mBitmapShader;
    /**
     * view的宽度
     */
    private int mWidth;
    private RectF mRoundRect;
    private boolean mVideo = false;
    private Bitmap bitmapVideo;
    private boolean isCover = false;
    private boolean beforeAndAfter = true;
    private String TAG = "ProcessImageView";

    public ProcessImageView(Context context, AttributeSet attrs) {
        
        super(context, attrs);
        this.context = context;
        mMatrix = new Matrix();
        mBitmapPaint = new Paint();
        mBitmapPaint.setAntiAlias(true);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.RoundImageView);

        mBorderRadius = a.getDimensionPixelSize(
                R.styleable.RoundImageView_borderRadius, (int) TypedValue
                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                                BODER_RADIUS_DEFAULT, getResources()
                                        .getDisplayMetrics()));// 默认为10dp
        type = a.getInt(R.styleable.RoundImageView_type, TYPE_CIRCLE);// 默认为Circle

        a.recycle();
        bitmapVideo = BitmapFactory.decodeResource(getResources(), R.drawable.ic_video_play_1x);
    }

    public ProcessImageView(Context context) {
        this(context, null);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        /**
         * 如果类型是圆形，则强制改变view的宽高一致，以小值为准
         */
        mWidth = Math.min(getMeasuredWidth(), getMeasuredHeight());
        if (type == TYPE_CIRCLE) {
            mRadius = mWidth / 2;
            setMeasuredDimension(mWidth, mWidth);
        }

    }

    /**
     * 初始化BitmapShader
     */
    private void setUpShader() {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        Bitmap bmp = drawableToBitamp(drawable);
        // 将bmp作为着色器，就是在指定区域内绘制bmp
        mBitmapShader = new BitmapShader(bmp, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        float scale = 1.0f;
        if (type == TYPE_CIRCLE) {
            // 拿到bitmap宽或高的小值
            int bSize = Math.min(bmp.getWidth(), bmp.getHeight());
            scale = mWidth * 1.0f / bSize;

        } else if (type == TYPE_ROUND) {
            if (!(bmp.getWidth() == getWidth() && bmp.getHeight() == getHeight())) {
                // 如果图片的宽或者高与view的宽高不匹配，计算出需要缩放的比例；缩放后的图片的宽高，一定要大于我们view的宽高；所以我们这里取大值；
                scale = Math.max(getWidth() * 1.0f / bmp.getWidth(),
                        getHeight() * 1.0f / bmp.getHeight());
            }

        }
        // shader的变换矩阵，我们这里主要用于放大或者缩小
        mMatrix.setScale(scale, scale);
        // 设置变换矩阵
        mBitmapShader.setLocalMatrix(mMatrix);
        // 设置shader
        mBitmapPaint.setShader(mBitmapShader);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (getDrawable() == null) {
            return;
        }
        setUpShader();

        if (type == TYPE_ROUND) {
            canvas.drawRoundRect(mRoundRect, mBorderRadius, mBorderRadius,
                    mBitmapPaint);
        } else {
            canvas.drawCircle(mRadius, mRadius, mRadius, mBitmapPaint);
        }
        setProcess(canvas);
    }

    /**
     * 设置进度
     * @param canvas
     */
    private void setProcess(Canvas canvas) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true); // 消除锯齿
        mPaint.setStyle(Paint.Style.FILL);

        String content;
        if(mVideo){
            content = "视频上传中";
            int mBitWidth = bitmapVideo.getWidth();
            int mBitHeight = bitmapVideo.getHeight();

            Rect mSrcRect = new Rect(0, 0, mBitWidth, mBitHeight);

            // 计算左边位置
            int left = mWidth / 2 - mBitWidth / 2;
            // 计算上边位置
            int top = mWidth / 2 - mBitHeight / 2;
            Rect mDestRect = new Rect(left, top, left + mBitWidth, top + mBitHeight);
            canvas.drawBitmap(bitmapVideo,mSrcRect,mDestRect,null);
        }else {
            content = "图片上传中";
            if (isCover) {
                mPaint.setColor(Color.parseColor("#e6ffc6d2"));// 半透明
                mViewRectC.left = 0;
                mViewRectC.top = getHeight() - DensityUtil.dip2px(16);
                mViewRectC.right = getWidth();
                mViewRectC.bottom = getHeight();
                /*圆角的半径，依次为左上角xy半径，右上角，右下角，左下角*/
                float[] rids = {0.0f, 0.0f, 0.0f, 0.0f, DensityUtil.dip2px(4), DensityUtil.dip2px(4), DensityUtil.dip2px(4), DensityUtil.dip2px(4),};
                Path path = new Path();
                path.addRoundRect(mViewRectC, rids, Path.Direction.CW);
                canvas.drawPath(path, mPaint);

                mPaint.setColor(Color.parseColor("#ffffff"));
                mPaint.setTextSize(DensityUtil.dip2px(11));
                Rect rect5 = new Rect();
                String beforeAndAfterText = "";
                if(beforeAndAfter){
                    beforeAndAfterText = "术后封面";
                }else {
                    beforeAndAfterText = "术前封面";
                }

                mPaint.getTextBounds(beforeAndAfterText, 0, beforeAndAfterText.length(), rect5);
               canvas.drawText(beforeAndAfterText, getWidth() / 2 - rect5.width() / 2, (getHeight()) - rect5.height() / 2, mPaint);
//               canvas.drawText(beforeAndAfterText, getWidth() / 2 - rect5.width() / 2, (getHeight() - mViewRectQ.height()) + mViewRectQ.height()/2 - rect5.height() / 2, mPaint);
            }
        }

        mPaint.setColor(Color.parseColor("#e6ffc6d2"));// 半透明
        mViewRectB.left = 0;
        mViewRectB.top = getHeight() * progress / 100;
        mViewRectB.right = getWidth();
        mViewRectB.bottom = getHeight();
        canvas.drawRoundRect(mViewRectB, DensityUtil.dip2px(4), DensityUtil.dip2px(4), mPaint);


        mPaint.setColor(Color.parseColor("#00000000"));// 全透明
        mViewRectQ.left = 0;
        mViewRectQ.top = getHeight() * progress / 100;
        mViewRectQ.right = getWidth();
        mViewRectQ.bottom = getHeight();

        canvas.drawRoundRect(mViewRectQ, DensityUtil.dip2px(4), DensityUtil.dip2px(4), mPaint);

        mPaint.setTextSize(DensityUtil.dip2px(11));
        mPaint.setColor(Color.parseColor("#000000"));
        mPaint.setStrokeWidth(2);


        Rect rect1 = new Rect();
        mPaint.getTextBounds("99%", 0, "99%".length(), rect1);// 确定文字的宽度

        Rect rect2 = new Rect();
        mPaint.getTextBounds(content, 0, content.length(), rect2);

        Rect rect3 = new Rect();
        mPaint.getTextBounds("失败", 0, "失败".length(), rect3);

        Rect rect4 = new Rect();
        mPaint.getTextBounds("点击重试", 0, "点击重试".length(), rect4);

        switch (mImgState) {
            case DENG_DAI:            //等待上传
                canvas.drawText(content, getWidth() / 2 - rect2.width() / 2, getHeight() / 2, mPaint);

                break;
            case SAHNG_CHUAN_ZHONG :         //上传中
                if (progress != 100) {
                    canvas.drawText(content, getWidth() / 2 - rect2.width() / 2, getHeight() / 2 - rect2.height(), mPaint);
                    canvas.drawText(progress + "%", getWidth() / 2 - rect1.width() / 2, getHeight() / 2 + rect2.height(), mPaint);
                }

                break;
            case WANC_HENG:             //上传完成

                break;
            case SHI_BAI:               //上传失败
                mPaint.setColor(Color.parseColor("#ff3333"));
                canvas.drawText("失败", getWidth() / 2 - rect3.width() / 2, getHeight() / 2 - rect3.height(), mPaint);
                canvas.drawText("点击重试", getWidth() / 2 - rect4.width() / 2, getHeight() / 2 + rect4.height(), mPaint);
                break;
            case 0:
                break;
        }
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // 圆角图片的范围
        if (type == TYPE_ROUND)
            mRoundRect = new RectF(0, 0, w, h);
    }

    /**
     * drawable转bitmap
     *
     * @param drawable
     * @return
     */
    private Bitmap drawableToBitamp(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bd = (BitmapDrawable) drawable;
            return bd.getBitmap();
        }
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);
        return bitmap;
    }

    private static final String STATE_INSTANCE = "state_instance";
    private static final String STATE_TYPE = "state_type";
    private static final String STATE_BORDER_RADIUS = "state_border_radius";

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_INSTANCE, super.onSaveInstanceState());
        bundle.putInt(STATE_TYPE, type);
        bundle.putInt(STATE_BORDER_RADIUS, mBorderRadius);
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            super.onRestoreInstanceState(((Bundle) state)
                    .getParcelable(STATE_INSTANCE));
            this.type = bundle.getInt(STATE_TYPE);
            this.mBorderRadius = bundle.getInt(STATE_BORDER_RADIUS);
        } else {
            super.onRestoreInstanceState(state);
        }

    }

    public void setBorderRadius(int borderRadius) {
        int pxVal = dp2px(borderRadius);
        if (this.mBorderRadius != pxVal) {
            this.mBorderRadius = pxVal;
            invalidate();
        }
    }

    public void setType(int type) {
        if (this.type != type) {
            this.type = type;
            if (this.type != TYPE_ROUND && this.type != TYPE_CIRCLE) {
                this.type = TYPE_CIRCLE;
            }
            requestLayout();
        }

    }

    public int dp2px(int dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, getResources().getDisplayMetrics());
    }

    /**
     * 开始画图
     * @param state     状态
     * @param progress  进度
     */
    public void startHua(int state, int progress){
        startHua(state,progress,false,false);
    }

    /**
     * 开始画图
     * @param state     状态
     * @param progress  进度
     * @param isVideo  是否显示封面图
     */
    public void startHua(int state, int progress,boolean isVideo){
        startHua(state,progress,isVideo,false);
    }

    /**
     * 开始画图
     * @param state     状态
     * @param progress  进度
     * @param isVideo  默认false
     * @param isCover  是否显示封面图
     */
    public void startHua(int state, int progress,boolean isVideo,boolean isCover){
        startHua(state,progress,isVideo,isCover,true);
    }

    /**
     * 开始画图
     * @param state     状态
     * @param progress  进度
     * @param isVideo  默认false
     * @param isCover  是否显示封面图
     * @param beforeAndAfter  默认是术后
     */
    public void startHua(int state, int progress,boolean isVideo,boolean isCover,boolean beforeAndAfter){
        this.mImgState = state;
        Log.e("TAG", "startHua --- mImgState == " + mImgState);
        this.progress = progress;
        this.mVideo = isVideo;
        this.isCover = isCover;
        this.beforeAndAfter = beforeAndAfter;
        invalidate();
    }

    /**
     * 获取状态
     * @return
     */
    public int getmImgState() {
        return mImgState;
    }

    /**
     * 设置圆角的大小
     * @param mBorderRadius
     */
    public void setmBorderRadius(int mBorderRadius) {
        this.mBorderRadius = mBorderRadius;
    }

}