package com.quicklyask.view;

import android.os.Handler;
import android.os.Message;

import java.util.Vector;

/**
 * Created by 裴成浩 on 2017/7/27.
 * 带暂停的 Handler
 */

public abstract class PauseHandler extends Handler {

    /**
     * 储存还未处理的消息
     */
    final Vector<Message> messageQueueBuffer = new Vector<Message>();

    /**
     * 标志位，记录创建handler的控件是否可操作
     */
    private boolean paused;

    /**
     * 恢复，重新发送临时储存的消息:恢复sku谈层显示和隐藏
     */
    final public void resume() {
        paused = false;
        while (messageQueueBuffer.size() > 0) {
            final Message msg = messageQueueBuffer.elementAt(0);
            messageQueueBuffer.removeElementAt(0);
            sendMessage(msg);
        }
    }

    /**
     * 暂停操作
     */
    final public void pause() {
        paused = true;
    }

    /**
     * 通知消息即将被保存为活动暂停。如果没有处理，当活动恢复时，消息将被保存并重播。
     *
     * @param message 需要储存的消息
     * @return 消息是否需要储存
     */
    protected abstract boolean storeMessage(Message message);


    /**
     * 对需要处理的消息执行相应的操作
     *
     * @param message 需要执行的消息
     */
    protected abstract void processMessage(Message message);

    /**
     * {@inheritDoc}
     */
    @Override
    final public void handleMessage(Message msg) {
        if (paused) {                                   //是否暂停消息发送
            if (storeMessage(msg)) {                        //控制要暂停的是那个消息(要拦截的消息就是true)
                Message msgCopy = new Message();
                msgCopy.copyFrom(msg);
                messageQueueBuffer.add(msgCopy);
            }else {
                processMessage(msg);
            }
        } else {
            processMessage(msg);
        }
    }
}