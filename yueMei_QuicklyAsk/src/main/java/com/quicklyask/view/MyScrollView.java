/**
 * 自定义滚动view
 */
package com.quicklyask.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.quicklyask.activity.interfaces.ScrollViewScrolCallBack;

/**
 * @author lenovo17
 *
 */
public class MyScrollView extends ScrollView {

    private ScrollViewScrolCallBack mOnScrollChangedCallback = null;

    /**
     * @param context
     */
    public MyScrollView(Context context) {
        super(context);
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnScrollChangedCallback(final ScrollViewScrolCallBack onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedCallback != null)
            mOnScrollChangedCallback.scrolChanged(l, t);
    }

}
