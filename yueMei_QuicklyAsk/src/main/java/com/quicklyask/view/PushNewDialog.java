package com.quicklyask.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;

/**
 * Created by 裴成浩 on 2019/8/28
 */
public class PushNewDialog extends Dialog {

    private final Context mContext;

    public PushNewDialog(Context context) {
        super(context, R.style.mystyle1);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.push_popwindow);
        setCanceledOnTouchOutside(false);

        ImageView mCloseIv = findViewById(R.id.push_new_close);
        Button mBtn = findViewById(R.id.push_new_btn);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEW_SHOW_ALERT, "yes", "0", "1"));
                dismiss();
                Intent localIntent = new Intent();
                localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT >= 9) {
                    localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    localIntent.setData(Uri.fromParts("package", mContext.getPackageName(), null));
                } else if (Build.VERSION.SDK_INT <= 8) {
                    localIntent.setAction(Intent.ACTION_VIEW);
                    localIntent.setClassName("com.android.settings",
                            "com.android.settings.InstalledAppDetails");
                    localIntent.putExtra("com.android.settings.ApplicationPkgName", mContext.getPackageName());
                }
                mContext.startActivity(localIntent);
            }
        });

        mCloseIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEW_SHOW_ALERT, "no", "0", "1"));
            }
        });

    }
}
