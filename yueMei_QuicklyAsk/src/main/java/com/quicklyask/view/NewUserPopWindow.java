package com.quicklyask.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.module.MainTableActivity;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;

public class NewUserPopWindow extends PopupWindow {
    /**
     * 新客弹窗
     */
    private ImageView alertIv;
    private RelativeLayout alerCloseRly;

    public NewUserPopWindow(final Context context, final String url) {
        final View view = View.inflate(context, R.layout.pop_newuser, null);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        alertIv = view.findViewById(R.id.pop_aler_iv);
        alerCloseRly = view.findViewById(R.id.colse_rly);

        Glide.with(context).load(R.drawable.newuser_alert).asGif().into(alertIv);
        //沿x轴放大
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(alertIv, "scaleX", 0.5f, 1f);
        //沿y轴放大
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(alertIv, "scaleY", 0.5f, 1f);
        AnimatorSet set = new AnimatorSet();
        //同时沿X,Y轴放大
        set.play(scaleXAnimator).with(scaleYAnimator);
        //都设置2s，也可以为每个单独设置
        set.setDuration(600);
        set.start();
        alertIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(url)) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", "6025");
                    hashMap.put("to_page_type", "16");
                    hashMap.put("to_page_id", "6025");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "yes"),hashMap);
                    WebUrlTypeUtil.getInstance(context).urlToApp(url, "0", "0");
                } else {
                    MainTableActivity.tancengUrl = "";
                }

                dismiss();
            }
        });

        alerCloseRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id","6025");
                hashMap.put("to_page_type", "16");
                hashMap.put("to_page_id", "6025");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "no"),hashMap);
                ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(alertIv, "scaleX", 1f, 0f);
                //沿y轴放大
                ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(alertIv, "scaleY", 1f, 0f);
                //沿x轴放大
                ObjectAnimator translationXAnimator = ObjectAnimator.ofFloat(alertIv, "translationX", 0f, 1100f);
                //沿y轴放大
                ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(alertIv, "translationY", 0f, 1100f);
                AnimatorSet set = new AnimatorSet();
                //同时沿X,Y轴放大
                set.play(scaleXAnimator).with(scaleYAnimator).with(translationXAnimator).with(translationYAnimator);
                //都设置2s，也可以为每个单独设置
                set.setDuration(1000);
                set.start();
                set.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        dismiss();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                MainTableActivity.tancengUrl = "";

            }
        });
    }
}
