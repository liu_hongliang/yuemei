package com.quicklyask.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.home.model.bean.NewZtCouponsBean;
import com.quicklyask.activity.R;
import com.quicklyask.adpter.NewuserConponsAdapter;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.taobao.accs.utl.UT;

import java.util.List;

public class NewuserConponsPop extends PopupWindow {

    public static final String jumpUrl = "https://m.yuemei.com/tao_zt/7483.htm";

    public NewuserConponsPop (final Context context, List<NewZtCouponsBean> couponsBeanList){
        final View view = View.inflate(context, R.layout.newuser_conpons, null);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setContentView(view);
        update();

        TextView conponsTitle = view.findViewById(R.id.newuser_conpons_title);
        RecyclerView conponsList = view.findViewById(R.id.newuser_conpons_list);
        Button conponsBtn = view.findViewById(R.id.newuser_conpons_btn);
        ImageView conponsClose = view.findViewById(R.id.newuser_conpons_close);

        conponsTitle.setText("恭喜获得"+(couponsBeanList.size())*3+"张新人券");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        conponsList.setLayoutManager(linearLayoutManager);
        NewuserConponsAdapter conponsAdapter = new NewuserConponsAdapter(R.layout.newuser_conpons_list_item, couponsBeanList);
        conponsList.setAdapter(conponsAdapter);
        conponsAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                WebUrlTypeUtil.getInstance(context).urlToApp(jumpUrl);
            }
        });
        conponsClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        conponsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(context).urlToApp(jumpUrl);
            }
        });
    }
}
