package com.quicklyask.view;

import android.content.Context;
import android.util.AttributeSet;
import com.quicklyask.activity.R;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;

/**
 * 文 件 名: NothingVideoView
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-16 21:14
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class NothingVideoView extends StandardGSYVideoPlayer {

    public NothingVideoView(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public NothingVideoView(Context context) {
        super(context);
    }

    public NothingVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void touchSurfaceMoveFullLogic(float absDeltaX, float absDeltaY) {
        super.touchSurfaceMoveFullLogic(absDeltaX, absDeltaY);
        mChangePosition = false;
        mChangeVolume = false;
        mBrightness = false;
    }

    @Override
    protected void touchDoubleUp() {
        //super.touchDoubleUp();
        //不需要双击暂停
    }

    @Override
    public int getLayoutId() {
        return R.layout.list_video;
    }

}
