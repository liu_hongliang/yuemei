package com.quicklyask.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class ItemTimerTextView extends LinearLayout implements Runnable{

    private long[] times;//当前时间
    private long mday, mhour, mmin, msecond;// 天，小时，分钟，秒

    private boolean run = false; // 是否启动了
    private TextView mDayTextView;
    private TextView mHourTextView;
    private TextView mMinuteTextView;
    private TextView mSecondTextView;

    public void setTimes(long[] times) {
        this.times = times;
        mday = times[0];
        mhour = times[1];
        mmin = times[2];
        msecond = times[3];
    }

    public long[] getTimes() {
        return times;
    }

    public void setSkuTimeDesc(String  skuTimeDesc) {
        mDayTextView.setText(skuTimeDesc);
    }



    public ItemTimerTextView(Context context) {
        this(context,null);
    }

    public ItemTimerTextView(Context context,  AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ItemTimerTextView(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_timer_tiextview, this, true);
        mDayTextView = view.findViewById(R.id.sku_time_day);
        mHourTextView = view.findViewById(R.id.sku_time_hour);
        mMinuteTextView = view.findViewById(R.id.sku_time_minute);
        mSecondTextView = view.findViewById(R.id.sku_time_second);

    }

    /**
     * 倒计时计算
     */
    private void ComputeTime() {
        msecond--;
        if (msecond < 0) {
            mmin--;
            msecond = 59;
            if (mmin < 0) {
                mmin = 59;
                mhour--;
                if (mhour < 0) {
                    // 倒计时结束，一天有24个小时
                    mhour = 23;
                    mday--;
                }
            }

        }

    }

    public boolean isRun() {
        return run;
    }

    public void beginRun() {
        this.run = true;
        run();
    }

    public void stopRun() {
        this.run = false;
    }
    @Override
    public void run() {
// 标示已经启动
        if (run) {
            ComputeTime();

            // String mdayStr;
            String mhourStr = null;
            String mminStr = null;
            String msecondStr = null;

            if (msecond < 10) {
                msecondStr = "0" + msecond;
            } else {
                msecondStr = "" + msecond;
            }
            if (mmin < 10) {
                mminStr = "0" + mmin;
            } else {
                mminStr = "" + mmin;
            }
            if (mhour < 10) {
                mhourStr = "0" + mhour;
            } else {
                mhourStr = "" + mhour;
            }

            if (mday == 0) {
                mHourTextView.setText(mhourStr);
                mMinuteTextView.setText(mminStr);
                mSecondTextView.setText(msecondStr);
            } else {
                mHourTextView.setText(mhourStr);
                mMinuteTextView.setText(mminStr);
                mSecondTextView.setText(msecondStr);
            }


            times[0] = mday;
            times[1] = mhour;
            times[2] = mmin;
            times[3] = msecond;
            if (mday == 0 && mhour == 0 && mmin == 0 && msecond == 0) {
                stopRun();
            } else {
                postDelayed(this, 1000);
            }

        } else {
            removeCallbacks(this);
        }
    }
}
