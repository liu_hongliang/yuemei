package com.quicklyask.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

/**
 * Robin
 *
 * Created by dwb on 16/11/14.
 */
public class FanJuanPopWindow extends PopupWindow {

    private Context mContext;
    private RelativeLayout alerCloseRly;
    private TextView fanxianTv;

    public FanJuanPopWindow(final Context mContext) {
        final View view = View.inflate(mContext, R.layout.pop_fanjuan,
                null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext,
                R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        alerCloseRly = view.findViewById(R.id.pop_aler_close_rly);
        fanxianTv = view.findViewById(R.id.fanxian_pop_title_tv);

        this.mContext=mContext;

        fanxianTv.setText(Html.fromHtml("继续更新日记可获得最高1000元现金，"+"<font color=\"#ff5c77\">"+"查看详情>>"+"</font>"));


        fanxianTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it1 = new Intent();
                String url1 = FinalConstant.baseUrl + FinalConstant.VER
                        + "/forum/postinfo/id/939555/";
                it1.putExtra("url", url1);
                it1.putExtra("qid", "939555");
                it1.setClass(mContext, DiariesAndPostsActivity.class);
                mContext.startActivity(it1);
            }
        });

        alerCloseRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}