/**
 * 
 */
package com.quicklyask.activity.interfaces;


import com.module.commonview.module.bean.ProjectFourTree;

/**
 * @author lenovo17
 * 
 */
public interface ProjectFourTreeItemSelectListener {
	void getValue(ProjectFourTree projectItem);
}
