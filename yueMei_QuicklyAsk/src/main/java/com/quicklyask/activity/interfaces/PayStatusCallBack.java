package com.quicklyask.activity.interfaces;

import com.tencent.mm.sdk.modelbase.BaseResp;

public interface PayStatusCallBack {

    void onPayStatusCallBack(BaseResp resp);
}
