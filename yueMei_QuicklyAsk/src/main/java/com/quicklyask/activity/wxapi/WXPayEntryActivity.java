package com.quicklyask.activity.wxapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.module.commonview.activity.SpeltActivity;
import com.module.my.controller.activity.OpeningMemberActivity;
import com.module.my.view.orderpay.OrderZhiFuStatus1Activity;
import com.module.my.view.orderpay.OrderZhiFuStatus2Activity;
import com.module.my.view.orderpay.OrderZhifuSaoFailsActivity;
import com.module.my.view.orderpay.OrderZhifuSaoSuessActivity;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.PayStatusCallBack;
import com.quicklyask.activity.weixin.Constants;
import com.quicklyask.util.Cfg;
import com.quicklyask.view.MyToast;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "WXPayEntryActivity";

    private IWXAPI api;

    private int error_code = 10000;

    private String server_id;
    private String order_id;
    private String price;
    private String taotitle;

    private String pay_sao;
    private String sku_type;
    private String is_repayment;
    private String is_repayment_mimo;
    private String weikuan;
    private String isGroup;
    private String groupId;
    private Context mContext;
    public static PayStatusCallBack mPayStatusCallBack;

    public static void setPayStatusCallBack(PayStatusCallBack payStatusCallBack) {
        mPayStatusCallBack = payStatusCallBack;
    }

    @Override
    public void onReq(BaseReq req) {

    }



    @Override
    public void onResp(BaseResp resp) {
//        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
//            this.error_code = resp.errCode;
//        }
        mPayStatusCallBack.onPayStatusCallBack(resp);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_result);
        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);

        api.handleIntent(getIntent(), this);

        mContext = WXPayEntryActivity.this;

        server_id = Cfg.loadStr(mContext, "server_id", "");
        order_id = Cfg.loadStr(mContext, "order_id", "");
        price = Cfg.loadStr(mContext, "price", "");
        taotitle = Cfg.loadStr(mContext, "taotitle", "");
        sku_type = Cfg.loadStr(mContext, "sku_type", "");
        is_repayment = Cfg.loadStr(mContext, "is_repayment", "");
        is_repayment_mimo = Cfg.loadStr(mContext, "is_repayment_mimo", "");
        weikuan = Cfg.loadStr(mContext, "weikuan", "");
        isGroup = Cfg.loadStr(mContext, "is_group", "0");
        groupId = Cfg.loadStr(mContext, "group_id", "");
        pay_sao = Cfg.loadStr(mContext, "pay_sao", "");

        setJump();
    }

    private void setJump() {
        Log.e(TAG, "pay_sao == " + pay_sao);
        Log.e(TAG, "error_code == " + error_code);
        if (error_code == 0) {
            if ("1".equals(pay_sao)) {
                Log.e(TAG, "server_id == " + server_id);
                Log.e(TAG, "order_id == " + order_id);
                Log.e(TAG, "sku_type == " + sku_type);
                Log.e(TAG, "is_repayment == " + is_repayment);
                Log.e(TAG, "is_repayment_mimo == " + is_repayment_mimo);
                Log.e(TAG, "price == " + price);
                Log.e(TAG, "isGroup == " + isGroup);

                if ("1".equals(isGroup)) {
                    Intent intent = new Intent(mContext, SpeltActivity.class);
                    intent.putExtra("group_id", groupId);
                    intent.putExtra("order_id", order_id);
                    intent.putExtra("type", "2");
                    startActivity(intent);
                } else {
                    Log.e(TAG, "微信支付成功...");
                    MyToast.makeTextToast2(mContext, "微信支付成功", MyToast.SHOW_TIME);

                    Intent intent = new Intent(mContext, OrderZhiFuStatus1Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("payType", "2");
                    bundle.putString("pay_type", "1");
                    bundle.putString("server_id", server_id);
                    bundle.putString("order_id", order_id);
                    bundle.putString("sku_type", sku_type);
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("price", price);

                    intent.putExtra("data", bundle);
                    startActivity(intent);
                }

            } else if ("2".equals(pay_sao)) {
                Log.e(TAG, "22222");
                Intent it = new Intent();
                it.putExtra("order_id", order_id);
                it.putExtra("price", price);
                it.setClass(mContext, OrderZhifuSaoSuessActivity.class);
                startActivity(it);
            } else {
                Cfg.saveInt(mContext, OpeningMemberActivity.PAY_STATE, 1);
            }

        } else if (error_code == -2) {

            if ("1".equals(pay_sao)) {

                Intent intent = new Intent(mContext, OrderZhiFuStatus2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("server_id", server_id);
                bundle.putString("order_id", order_id);
                bundle.putString("taotitle", taotitle);
                bundle.putString("price", price);
                bundle.putString("order_time", "1c");
                bundle.putString("sku_type", sku_type);
                bundle.putString("is_repayment", is_repayment);
                bundle.putString("is_repayment_mimo", is_repayment_mimo);
                bundle.putString("weikuan", weikuan);
                bundle.putString("group_id", groupId);
                bundle.putString("is_group", isGroup);

                intent.putExtra("data", bundle);
                startActivity(intent);

            } else if ("2".equals(pay_sao)) {
                Intent it = new Intent();
                it.putExtra("server_id", server_id);
                it.putExtra("order_id", order_id);
                it.putExtra("taotitle", taotitle);
                it.putExtra("price", price);
                it.putExtra("order_time", "1c");
                it.setClass(mContext, OrderZhifuSaoFailsActivity.class);
                startActivity(it);
            } else {
                Cfg.saveInt(mContext, OpeningMemberActivity.PAY_STATE, 2);
            }

        } else {

            if ("1".equals(pay_sao)) {

                Intent intent = new Intent(mContext, OrderZhiFuStatus2Activity.class);
                Bundle bundle = new Bundle();
                bundle.putString("server_id", server_id);
                bundle.putString("order_id", order_id);
                bundle.putString("taotitle", taotitle);
                bundle.putString("price", price);
                bundle.putString("order_time", "1c");
                bundle.putString("sku_type", sku_type);
                bundle.putString("is_repayment", is_repayment);
                bundle.putString("is_repayment_mimo", is_repayment_mimo);
                bundle.putString("weikuan", weikuan);
                bundle.putString("group_id", groupId);
                bundle.putString("is_group", isGroup);

                intent.putExtra("data", bundle);
                startActivity(intent);

            } else if ("2".equals(pay_sao)) {
                Intent it = new Intent();
                it.putExtra("server_id", server_id);
                it.putExtra("order_id", order_id);
                it.putExtra("taotitle", taotitle);
                it.putExtra("price", price);
                it.putExtra("order_time", "1c");
                it.setClass(mContext, OrderZhifuSaoFailsActivity.class);
                startActivity(it);
            } else {
                Cfg.saveInt(mContext, OpeningMemberActivity.PAY_STATE, 3);
            }
        }

        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void finish() {
        super.finish();
        Intent intent = new Intent();
        if (error_code == 0) {

        } else if (error_code == -1) {

        } else {

        }
        sendBroadcast(intent);
    }
}