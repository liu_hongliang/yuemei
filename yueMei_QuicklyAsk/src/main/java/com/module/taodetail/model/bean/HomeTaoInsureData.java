package com.module.taodetail.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/12/14
 */
public class HomeTaoInsureData implements Parcelable {
    private String is_insure;               //是否有保险。1：有
    private String insure_pay_money;        //保险价格
    private boolean isBaoxianOpen = false;  //是否开通保险

    protected HomeTaoInsureData(Parcel in) {
        is_insure = in.readString();
        insure_pay_money = in.readString();
        isBaoxianOpen = in.readByte() != 0;
    }

    public static final Creator<HomeTaoInsureData> CREATOR = new Creator<HomeTaoInsureData>() {
        @Override
        public HomeTaoInsureData createFromParcel(Parcel in) {
            return new HomeTaoInsureData(in);
        }

        @Override
        public HomeTaoInsureData[] newArray(int size) {
            return new HomeTaoInsureData[size];
        }
    };

    public String getIs_insure() {
        return is_insure;
    }

    public void setIs_insure(String is_insure) {
        this.is_insure = is_insure;
    }

    public String getInsure_pay_money() {
        return insure_pay_money;
    }

    public void setInsure_pay_money(String insure_pay_money) {
        this.insure_pay_money = insure_pay_money;
    }

    public boolean isBaoxianOpen() {
        return isBaoxianOpen;
    }

    public void setBaoxianOpen(boolean baoxianOpen) {
        isBaoxianOpen = baoxianOpen;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(is_insure);
        dest.writeString(insure_pay_money);
        dest.writeByte((byte) (isBaoxianOpen ? 1 : 0));
    }
}
