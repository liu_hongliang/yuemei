/**
 *
 */
package com.module.taodetail.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.ChatDataBean;
import com.module.home.model.bean.HospitalTop;
import com.module.home.model.bean.SearchEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 首页淘整形列表
 *
 * @author Robin
 */
public class HomeTaoData implements Parcelable {

    private String member_price;            //会员价格
    private String bmsid;                   //
    private String seckilling;              //大促
    private String img;                     //图片
    private String title;                   //标题
    private String subtitle;                //副标题
    private String hos_name;                //医院名
    private String doc_name;                //医生名
    private String price;                   //医院价
    private String price_discount;          //悦美价
    private String price_range_max;         //最大悦美价
    private String _id;                     //淘Id
    private String showprice;               //是否显示医院价 1显示
    private String specialPrice;            //是否是超值特卖 1是
    private String show_hospital;           //是否显示医院
    private String invitation;              //是否是特邀 1是
    private String lijian;                  //优惠立减
    private String baoxian;                 //是否有保险 1有
    private HomeTaoInsureData insure;       //保险数据
    private String img66;                   //是否有大促logo 1有
    private String repayment;               //是否 有分期 1有
    private String hos_red_packet;          //医院红包
    private String shixiao;                 //失效
    private String extension_user;          //???
    private String postStr;                 //???
    private String newp;                    //
    private String hot;                     //
    private String mingyi;                  //
    private String rate;                    //
    private String feeScale;                //计费方式（单位）
    private String tuijianTitle;            //推荐标题
    private String is_fanxian;              //是否显示返现 1显示
    private String fanxian;

    private String pay_dingjin;             //订金
    private String pay_price_discount;      //支付价格
    private String number;                  //最大数量（购物车中是最大数量，结算页是购买数量）
    private String start_number;            //最小数量
    private String kucun_number;            //库存
    private int is_show_member;             //是否显示会员
    private String m_list_logo;             //
    private String id;                      //
    private String depreciate;              //比收藏时降价N元
    private List<Promotion> promotion;         //促销方式
    private HashMap<String, String> event_params;
    private String videoTaoTitle;           //视频播放独有的

    private String hospital_id;
    private String hos_userid;
    private String is_rongyun;
    private String is_have_video;
    private String business_district;       //商圈
    private String distance;                //距离
    private SkuLabelLevel hospital_top;     //置顶
    private PromotionNoticeTextData promotionNoticeText;
    private List<String> userImg;
    private ChatDataBean chatData;
    private HashMap<String, String> baoguang_params;
    private String coupon_type;
    private String bilateral_coupons;
    private String highlight_title;
    private String app_slide_logo;
    private String is_user_browse;
    private LocationData location;
    private String service;
    private String hos_effect;
    private String sale_type;

    public String getSale_type() {
        return sale_type;
    }

    public void setSale_type(String sale_type) {
        this.sale_type = sale_type;
    }

    public String getHos_effect() {
        return hos_effect;
    }

    public void setHos_effect(String hos_effect) {
        this.hos_effect = hos_effect;
    }

    public HomeTaoData() {
    }

    protected HomeTaoData(Parcel in) {
        fanxian = in.readString();
        member_price = in.readString();
        bmsid = in.readString();
        seckilling = in.readString();
        img = in.readString();
        title = in.readString();
        subtitle = in.readString();
        hos_name = in.readString();
        doc_name = in.readString();
        price = in.readString();
        price_discount = in.readString();
        price_range_max = in.readString();
        _id = in.readString();
        showprice = in.readString();
        specialPrice = in.readString();
        show_hospital = in.readString();
        invitation = in.readString();
        lijian = in.readString();
        baoxian = in.readString();
        insure = in.readParcelable(HomeTaoInsureData.class.getClassLoader());
        img66 = in.readString();
        repayment = in.readString();
        hos_red_packet = in.readString();
        shixiao = in.readString();
        extension_user = in.readString();
        postStr = in.readString();
        newp = in.readString();
        hot = in.readString();
        mingyi = in.readString();
        rate = in.readString();
        feeScale = in.readString();
        tuijianTitle = in.readString();
        is_fanxian = in.readString();
        pay_dingjin = in.readString();
        pay_price_discount = in.readString();
        number = in.readString();
        start_number = in.readString();
        kucun_number = in.readString();
        is_show_member = in.readInt();
        m_list_logo = in.readString();
        id = in.readString();
        depreciate = in.readString();
        promotion = new ArrayList<>();
        videoTaoTitle = in.readString();
        hospital_id = in.readString();
        hos_userid = in.readString();
        is_rongyun = in.readString();
        is_have_video = in.readString();
        business_district = in.readString();
        distance = in.readString();
        userImg = in.createStringArrayList();
        coupon_type = in.readString();
        bilateral_coupons = in.readString();
        highlight_title = in.readString();
        app_slide_logo = in.readString();
        chatData = in.readParcelable(ChatDataBean.class.getClassLoader());
        location = in.readParcelable(LocationData.class.getClassLoader());
        is_user_browse = in.readString();
        service = in.readString();
        hos_effect = in.readString();
        sale_type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fanxian);
        dest.writeString(member_price);
        dest.writeString(bmsid);
        dest.writeString(seckilling);
        dest.writeString(img);
        dest.writeString(title);
        dest.writeString(subtitle);
        dest.writeString(hos_name);
        dest.writeString(doc_name);
        dest.writeString(price);
        dest.writeString(price_discount);
        dest.writeString(price_range_max);
        dest.writeString(_id);
        dest.writeString(showprice);
        dest.writeString(specialPrice);
        dest.writeString(show_hospital);
        dest.writeString(invitation);
        dest.writeString(lijian);
        dest.writeString(baoxian);
        dest.writeParcelable(insure, flags);
        dest.writeString(img66);
        dest.writeString(repayment);
        dest.writeString(hos_red_packet);
        dest.writeString(shixiao);
        dest.writeString(extension_user);
        dest.writeString(postStr);
        dest.writeString(newp);
        dest.writeString(hot);
        dest.writeString(mingyi);
        dest.writeString(rate);
        dest.writeString(feeScale);
        dest.writeString(tuijianTitle);
        dest.writeString(is_fanxian);
        dest.writeString(pay_dingjin);
        dest.writeString(pay_price_discount);
        dest.writeString(number);
        dest.writeString(start_number);
        dest.writeString(kucun_number);
        dest.writeInt(is_show_member);
        dest.writeString(m_list_logo);
        dest.writeString(id);
        dest.writeString(depreciate);
        dest.writeList(promotion);
        dest.writeString(videoTaoTitle);
        dest.writeString(hospital_id);
        dest.writeString(hos_userid);
        dest.writeString(is_rongyun);
        dest.writeString(is_have_video);
        dest.writeString(business_district);
        dest.writeString(distance);
        dest.writeStringList(userImg);
        dest.writeString(coupon_type);
        dest.writeString(bilateral_coupons);
        dest.writeString(highlight_title);
        dest.writeString(app_slide_logo);
        dest.writeParcelable(chatData, flags);
        dest.writeParcelable(location, flags);
        dest.writeString(is_user_browse);
        dest.writeString(service);
        dest.writeString(hos_effect);
        dest.writeString(sale_type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeTaoData> CREATOR = new Creator<HomeTaoData>() {
        @Override
        public HomeTaoData createFromParcel(Parcel in) {
            return new HomeTaoData(in);
        }

        @Override
        public HomeTaoData[] newArray(int size) {
            return new HomeTaoData[size];
        }
    };

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFanxian() {
        return fanxian;
    }

    public void setFanxian(String fanxian) {
        this.fanxian = fanxian;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getSeckilling() {
        return seckilling;
    }

    public void setSeckilling(String seckilling) {
        this.seckilling = seckilling;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice_range_max() {
        return price_range_max;
    }

    public void setPrice_range_max(String price_range_max) {
        this.price_range_max = price_range_max;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getShowprice() {
        return showprice;
    }

    public void setShowprice(String showprice) {
        this.showprice = showprice;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getShow_hospital() {
        return show_hospital;
    }

    public void setShow_hospital(String show_hospital) {
        this.show_hospital = show_hospital;
    }

    public String getInvitation() {
        return invitation;
    }

    public void setInvitation(String invitation) {
        this.invitation = invitation;
    }

    public String getLijian() {
        return lijian;
    }

    public void setLijian(String lijian) {
        this.lijian = lijian;
    }

    public String getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(String baoxian) {
        this.baoxian = baoxian;
    }

    public HomeTaoInsureData getInsure() {
        return insure;
    }

    public void setInsure(HomeTaoInsureData insure) {
        this.insure = insure;
    }

    public String getImg66() {
        return img66;
    }

    public void setImg66(String img66) {
        this.img66 = img66;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getHos_red_packet() {
        return hos_red_packet;
    }

    public void setHos_red_packet(String hos_red_packet) {
        this.hos_red_packet = hos_red_packet;
    }

    public String getShixiao() {
        return shixiao;
    }

    public void setShixiao(String shixiao) {
        this.shixiao = shixiao;
    }

    public String getExtension_user() {
        return extension_user;
    }

    public void setExtension_user(String extension_user) {
        this.extension_user = extension_user;
    }

    public String getPostStr() {
        return postStr;
    }

    public void setPostStr(String postStr) {
        this.postStr = postStr;
    }

    public String getNewp() {
        return newp;
    }

    public void setNewp(String newp) {
        this.newp = newp;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getMingyi() {
        return mingyi;
    }

    public void setMingyi(String mingyi) {
        this.mingyi = mingyi;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getFeeScale() {
        return feeScale;
    }

    public void setFeeScale(String feeScale) {
        this.feeScale = feeScale;
    }

    public String getTuijianTitle() {
        return tuijianTitle;
    }

    public void setTuijianTitle(String tuijianTitle) {
        this.tuijianTitle = tuijianTitle;
    }

    public String getIs_fanxian() {
        return is_fanxian;
    }

    public void setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
    }

    public String getPay_dingjin() {
        return pay_dingjin;
    }

    public void setPay_dingjin(String pay_dingjin) {
        this.pay_dingjin = pay_dingjin;
    }

    public String getPay_price_discount() {
        return pay_price_discount;
    }

    public void setPay_price_discount(String pay_price_discount) {
        this.pay_price_discount = pay_price_discount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStart_number() {
        return start_number;
    }

    public void setStart_number(String start_number) {
        this.start_number = start_number;
    }

    public String getKucun_number() {
        return kucun_number;
    }

    public void setKucun_number(String kucun_number) {
        this.kucun_number = kucun_number;
    }

    public int getIs_show_member() {
        return is_show_member;
    }

    public void setIs_show_member(int is_show_member) {
        this.is_show_member = is_show_member;
    }

    public String getM_list_logo() {
        return m_list_logo;
    }

    public void setM_list_logo(String m_list_logo) {
        this.m_list_logo = m_list_logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepreciate() {
        return depreciate;
    }

    public void setDepreciate(String depreciate) {
        this.depreciate = depreciate;
    }

    public List<Promotion> getPromotion() {
        return promotion;
    }

    public void setPromotion(List<Promotion> promotion) {
        this.promotion = promotion;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getVideoTaoTitle() {
        return videoTaoTitle;
    }

    public void setVideoTaoTitle(String videoTaoTitle) {
        this.videoTaoTitle = videoTaoTitle;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getIs_have_video() {
        return is_have_video;
    }

    public void setIs_have_video(String is_have_video) {
        this.is_have_video = is_have_video;
    }

    public String getBusiness_district() {
        return business_district;
    }

    public void setBusiness_district(String business_district) {
        this.business_district = business_district;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public SkuLabelLevel getHospital_top() {
        return hospital_top;
    }

    public void setHospital_top(SkuLabelLevel hospital_top) {
        this.hospital_top = hospital_top;
    }

    public PromotionNoticeTextData getPromotionNoticeText() {
        return promotionNoticeText;
    }

    public void setPromotionNoticeText(PromotionNoticeTextData promotionNoticeText) {
        this.promotionNoticeText = promotionNoticeText;
    }

    public List<String> getUserImg() {
        return userImg;
    }

    public void setUserImg(List<String> userImg) {
        this.userImg = userImg;
    }

    public ChatDataBean getChatData() {
        return chatData;
    }

    public void setChatData(ChatDataBean chatData) {
        this.chatData = chatData;
    }

    public HashMap<String, String> getBaoguang_params() {
        return baoguang_params;
    }

    public void setBaoguang_params(HashMap<String, String> baoguang_params) {
        this.baoguang_params = baoguang_params;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getBilateral_coupons() {
        return bilateral_coupons;
    }

    public void setBilateral_coupons(String bilateral_coupons) {
        this.bilateral_coupons = bilateral_coupons;
    }

    public String getHighlight_title() {
        return highlight_title;
    }

    public void setHighlight_title(String highlight_title) {
        this.highlight_title = highlight_title;
    }

    public String getApp_slide_logo() {
        return app_slide_logo;
    }

    public void setApp_slide_logo(String app_slide_logo) {
        this.app_slide_logo = app_slide_logo;
    }

    public String getIs_user_browse() {
        return is_user_browse;
    }

    public void setIs_user_browse(String is_user_browse) {
        this.is_user_browse = is_user_browse;
    }

    public LocationData getLocation() {
        return location;
    }

    public void setLocation(LocationData location) {
        this.location = location;
    }
}
