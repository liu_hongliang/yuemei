package com.module.taodetail.model.bean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/3.
 */

public class TaoZtItem {

    private String url;
    private String img;
    private List<TaoZtItemTao> tao;
    private HashMap<String,String> event_params;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<TaoZtItemTao> getTao() {
        return tao;
    }

    public void setTao(List<TaoZtItemTao> tao) {
        this.tao = tao;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap event_params) {
        this.event_params = event_params;
    }
}
