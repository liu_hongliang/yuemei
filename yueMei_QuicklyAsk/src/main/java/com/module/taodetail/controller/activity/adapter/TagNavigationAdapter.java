package com.module.taodetail.controller.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * 项目类型导航球列表适配
 * Created by 裴成浩 on 2018/1/24.
 */

public class TagNavigationAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private List<TaoTagItem> tagList;

    public TagNavigationAdapter(Context context, List<TaoTagItem> tagList) {
        this.mContext = context;
        this.tagList = tagList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return tagList.size();
    }

    @Override
    public Object getItem(int position) {
        return tagList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_tao_tag, viewGroup, false);

            holder = new ViewHolder();
            holder.tagImg = convertView.findViewById(R.id.tag_img);
            holder.tagTitle = convertView.findViewById(R.id.tag_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TaoTagItem tagItem = tagList.get(position);
        Glide.with(mContext)
                .load(tagItem.getImg())
                .into(holder.tagImg);
        holder.tagTitle.setText(tagItem.getTitle());

        return convertView;
    }

    public TaoTagItem getData(int pos){
        return tagList.get(pos);
    }

    class ViewHolder {
        ImageView tagImg;
        TextView tagTitle;
    }
}
