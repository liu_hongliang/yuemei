package com.module.taodetail.view.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultMethod;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 方法适配器
 * Created by 裴成浩 on 2019/4/18
 */
public class SearhMethodRecyclerAdapter extends RecyclerView.Adapter<SearhMethodRecyclerAdapter.ViewHolder> {

    private Activity mContext;
    private List<SearchResultMethod> mDatas;
    private final LayoutInflater mInflater;
    private boolean isTag;              //是否是tag适配器
    private String TAG = "SearhMethodRecyclerAdapter";

    public SearhMethodRecyclerAdapter(Activity context, List<SearchResultMethod> methods,boolean isTag) {
        this.mContext = context;
        this.mDatas = methods;
        this.isTag = isTag;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public SearhMethodRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos) {
        return new ViewHolder(mInflater.inflate(R.layout.item_searh_method_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SearhMethodRecyclerAdapter.ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.method.getLayoutParams();
        if (pos == 0) {
            layoutParams.leftMargin = Utils.dip2px(0);
            layoutParams.rightMargin = Utils.dip2px(5);
        } else if (pos == mDatas.size()) {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(0);
        } else {
            layoutParams.leftMargin = Utils.dip2px(5);
            layoutParams.rightMargin = Utils.dip2px(5);
        }
        viewHolder.method.setLayoutParams(layoutParams);

        SearchResultMethod data = mDatas.get(pos);
        viewHolder.methodText.setText(data.getTitle());

        Log.e(TAG, "data == " + data.getEvent_params());
        if (data.getEvent_params() != null) {
            Log.e(TAG, "data个数 == " + data.getEvent_params().size());
        }

        if (data.isSelected()) {
            viewHolder.methodText.setTextColor(Utils.getLocalColor(mContext, R.color.tab_tag));
            viewHolder.methodText.setBackgroundResource(R.drawable.home_diary_tab);
        } else {
            viewHolder.methodText.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            viewHolder.methodText.setBackgroundResource(R.drawable.home_diary_tab2);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout method;
        private TextView methodText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            method = itemView.findViewById(R.id.item_searh_method_recycler);
            methodText = itemView.findViewById(R.id.item_searh_method_text);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setSelected(getLayoutPosition());

                   if (onItemClickListener != null) {
                        YmStatistics.getInstance().tongjiApp(mDatas.get(getLayoutPosition()).getEvent_params());
                        if(isTag){
                            onItemClickListener.onItemClick(mDatas.get(getLayoutPosition()));
                        }else{
                            onItemClickListener.onItemClick(getSelectedIdString());
                        }
                    }
                }
            });
        }
    }

    /**
     * 设置选中
     *
     * @param position
     */
    private void setSelected(int position) {
        int typePos = -1;                  //之前选中的
        for (int i = 0; i < mDatas.size(); i++) {
            SearchResultMethod data = mDatas.get(i);
            if (data.isSelected()) {
                typePos = i;
                break;
            }
        }

        if (typePos == position) {
            //之前选中的和现在选中的是同一个
            mDatas.get(position).setSelected(!mDatas.get(position).isSelected());
        } else {
            //之前选中的和现在选中的不是同一个
            if (typePos >= 0) {
                //之前有选中
                mDatas.get(typePos).setSelected(!mDatas.get(typePos).isSelected());
                mDatas.get(position).setSelected(!mDatas.get(position).isSelected());
                notifyItemChanged(typePos);
            } else {
                //之前没有选中
                mDatas.get(position).setSelected(!mDatas.get(position).isSelected());
            }
        }

        notifyItemChanged(position);
    }

    /**
     * 获取选中id
     *
     * @return
     */
    private SearchResultMethod getSelectedIdString() {
        for (SearchResultMethod data : mDatas) {
            if (data.isSelected()) {
                return data;
            }
        }
        return null;
    }


    //item点击
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(SearchResultMethod data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
