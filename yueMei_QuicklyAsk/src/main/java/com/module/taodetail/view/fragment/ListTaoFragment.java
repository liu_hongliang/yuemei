package com.module.taodetail.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.baidu.mobstat.StatService;
import com.cpoopc.scrollablelayoutlib.ScrollableHelper;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.home.view.fragment.ScrollAbleFragment;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.api.LoadDataAapi;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.tendcloud.tenddata.TCAgent;

import org.kymjs.aframe.utils.SystemTool;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplecache.ACache;

public class ListTaoFragment extends ScrollAbleFragment implements ScrollableHelper.ScrollableContainer {

    private DropDownListView mListview;
    private List<HomeTaoData> lvHotIssueData = new ArrayList<>();
    private TaoAdpter623 hotAdpter;
    private String partId;
    private String city;

    private ACache mCache;


    public static ListTaoFragment newInstance(String city, String partId) {
        ListTaoFragment listFragment = new ListTaoFragment();
        Bundle b = new Bundle();
        b.putString("partId", partId);
        b.putString("city", city);
        listFragment.setArguments(b);
        return listFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list1, container, false);
        mListview = view.findViewById(R.id.listview);
        mListview.setHasMore(false);
        mListview.onBottomComplete();

        mCache = ACache.get(getActivity());

        if (isAdded()) {
            city = getArguments().getString("city");
            partId = getArguments().getString("partId");
        }

        if (SystemTool.checkNet(getActivity())) {         //有网络
            loadData();
        } else {
            String homejson = mCache.getAsString("home_taolist_json" + partId);
            if (homejson != null) {
                dataToview(homejson);
            }
        }

        return view;
    }


    void loadData() {

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("partId", partId);
        keyValues.put("city", URLEncoder.encode(city));
        new LoadDataAapi().getCallBack(getActivity(), keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mCache.put("home_taolist_json" + partId, serverData.data, 60 * 60 * 24);
                if ("1".equals(serverData.code)) {
                    dataToview(serverData.data);
                }

            }
        });
    }

    void dataToview(String json) {
        try {
            lvHotIssueData = JSONUtil.transHomeTaoData(json);

            if (null != getActivity()) {
                Log.e("TaoAdpter623", "77777");
                hotAdpter = new TaoAdpter623(getActivity(), lvHotIssueData);
                mListview.setAdapter(hotAdpter);

                mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(
                            AdapterView<?> arg0, View arg1,
                            int pos, long arg3) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        final String _id = lvHotIssueData.get(pos).get_id();
                        String bmsid = lvHotIssueData.get(pos).getBmsid();

                        //统计一
                        TCAgent.onEvent(getActivity(), "首页淘列表", partId + " " + (pos + 1));
                        //自己的统计
//
                        YmStatistics.getInstance().tongjiApp(new EventParamData("tao_tao_" + partId, (pos + 1) + "", "tao"), lvHotIssueData.get(pos).getEvent_params(), new ActivityTypeData("20"));
                        //百度统计
                        StatService.onEvent(getActivity(), "070", (pos + 1) + "", 1);

                        Intent it2 = new Intent(getActivity(), TaoDetailActivity.class);
                        it2.putExtra("id", _id);
                        it2.putExtra("source", "49");
                        it2.putExtra("objid", "0");
                        startActivity(it2);

                    }
                });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View getScrollableView() {
        return mListview;
    }
}