package com.module;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.ExtrasBean;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import cn.jpush.android.api.JPushInterface;

public class OpenClickActivity extends Activity {
    private static final String TAG = "OpenClickActivity";
    /**
     * 消息Id
     **/
    private static final String KEY_MSGID = "msg_id";
    /**
     * 该通知的下发通道
     **/
    private static final String KEY_WHICH_PUSH_SDK = "rom_type";
    /**
     * 通知标题
     **/
    private static final String KEY_TITLE = "n_title";
    /**
     * 通知内容
     **/
    private static final String KEY_CONTENT = "n_content";
    /**
     * 通知附加字段
     **/
    private static final String KEY_EXTRAS = "n_extras";
    private String mExtra;
    private String mFlag = "0"; //app是否存活 1存活
    private String mLink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mExtra = intent.getStringExtra("extra");
        mFlag = intent.getStringExtra("flag");
        mLink = intent.getStringExtra("link");
        Log.e(TAG, "flag" + mFlag);
        if (!TextUtils.isEmpty(mLink)) {
            handleClick();
        } else {
            if (mExtra == null || "".equals(mExtra)) {
                handleOpenClick();
            } else {
                handleOpenClick(mExtra);
            }
        }
    }

    private void handleClick() {
        Log.e(TAG, "33333");
        WebUrlTypeUtil.getInstance(OpenClickActivity.this).urlToApp(mLink, "0", "0");
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 处理点击事件，当前启动配置的Activity都是使用
     * Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
     * 方式启动，只需要在onCreat中调用此方法进行处理
     */
    private void handleOpenClick() {
        Log.d(TAG, "用户点击打开了通知");
        if (getIntent().getData() == null) return;
        String data = getIntent().getData().toString();
        Log.w(TAG, "msg content is " + String.valueOf(data));
        if (TextUtils.isEmpty(data)) return;
        try {
            JSONObject jsonObject = new JSONObject(data);
            String msgId = jsonObject.optString(KEY_MSGID);
            byte whichPushSDK = (byte) jsonObject.optInt(KEY_WHICH_PUSH_SDK);
            String title = jsonObject.optString(KEY_TITLE);
            String content = jsonObject.optString(KEY_CONTENT);
            String extras = jsonObject.optString(KEY_EXTRAS);
            Log.e(TAG, "extras" + extras);
            BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(OpenClickActivity.this, mFlag);
            baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
                @Override
                public void otherJump(String urlStr) throws Exception {
                    JSONObject jsonObject = new JSONObject(urlStr);
                    String link = jsonObject.optString("link");
                    if ("1".equals(mFlag)) {
                        Log.e(TAG, " 111handleOpenClick()=====");
                        WebUrlTypeUtil.getInstance(OpenClickActivity.this).urlToApp(link, "0", "0");
                    } else {
                        Log.e(TAG, " 222handleOpenClick()=====");
                        Intent intent = new Intent(OpenClickActivity.this, MainTableActivity.class);
                        startActivity(intent);
                        WebUrlTypeUtil.getInstance(OpenClickActivity.this).urlToApp(link, "0", "0");
                    }

                }
            });
            baseWebViewClientMessage.showWebDetail(extras);

            //上报点击事件
            JPushInterface.reportNotificationOpened(this, msgId, whichPushSDK);
        } catch (JSONException e) {
            Log.w(TAG, "parse notification error");
        }
        finish();

    }

    private void handleOpenClick(String extras) {
        Log.e(TAG,"handleOpenClick ====extras"+extras);
        //点击统计
        ExtrasBean extrasBean = JSONUtil.TransformSingleBean(extras, ExtrasBean.class);
        HashMap<String, String> params = extrasBean.getParams();
        if (params != null) {
            HashMap<String, String> hashMap = new HashMap<>();

            Iterator<String> iter = params.keySet().iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                if ("u".equals(key)) {
                    hashMap.put("extension_user", params.get(key));
                } else {
                    hashMap.put(key, params.get(key));
                }
            }

            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PUSH_FIVE_CLICK, "0"), hashMap);
        }
        Log.e(TAG,"BaseWebViewClientMessage ===="+mFlag);

        BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(OpenClickActivity.this,mFlag);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) throws Exception {
                Log.e(TAG,"otherJump ====extras"+urlStr);
                JSONObject jsonObject = new JSONObject(urlStr);
                String link = jsonObject.optString("link");
                if ("1".equals(mFlag)) {
                    Log.e(TAG, " 111handleOpenClick(String extras )=====");

                    WebUrlTypeUtil.getInstance(OpenClickActivity.this).urlToApp(link, "0", "0");

                } else {
                    Log.e(TAG, " 222handleOpenClick(String extras )=====");
                    Intent intent = new Intent(OpenClickActivity.this, MainTableActivity.class);
                    startActivity(intent);
                    WebUrlTypeUtil.getInstance(OpenClickActivity.this).urlToApp(link, "0", "0");
                }
            }
        });
        baseWebViewClientMessage.showWebDetail(extras);

        finish();
    }

    private String getPushSDKName(byte whichPushSDK) {
        String name;
        switch (whichPushSDK) {
            case 0:
                name = "jpush";
                break;
            case 1:
                name = "xiaomi";
                break;
            case 2:
                name = "huawei";
                break;
            case 3:
                name = "meizu";
                break;
            case 8:
                name = "fcm";
                break;
            default:
                name = "jpush";
        }
        return name;
    }

    /**
     * 获取统计参数
     *
     * @return
     */
    public String getExtra() {
        return mExtra;
    }
}
