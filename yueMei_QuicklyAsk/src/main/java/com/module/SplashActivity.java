package com.module;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.module.api.SplashApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.PrivacyAgreementDialog;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.bean.PermsissionData;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.commonview.utils.DownloadUtil;
import com.module.commonview.utils.FileUtils;
import com.module.commonview.utils.PermissionManager;
import com.module.commonview.view.SplashPermissionPop;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.community.controller.api.ColdStartApi;
import com.module.community.model.bean.ZhuanTi;
import com.module.community.statistical.StatisticalPath;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.my.model.api.JPushBindApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.NoLoginBean;
import com.module.my.model.bean.RecoveryReminder;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.netWork.BuildConfig;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.Location;
import com.quicklyask.entity.SneakGuest;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.VideoView;
import com.quicklyask.view.YueMeiDialog;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;

import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class SplashActivity extends YMBaseActivity {
    public static final String TAG = "SplashActivity";
    @BindView(R.id.rl_content)
    RelativeLayout mRelativeLayout;
    @BindView(R.id.adver_iv)
    ImageView adverIv;
    @BindView(R.id.adver_iv_click)
    LinearLayout adverIvClick;
    @BindView(R.id.adver_video_view)
    VideoView videoView;
    @BindView(R.id.adver_video_view_click)
    FrameLayout videoViewClick;
    @BindView(R.id.start_log_time_tv)
    TextView startLogTimeTv;
    @BindView(R.id.skip_bt_ly)
    LinearLayout startSkipBt;
    @BindView(R.id.yuemei_logo)
    ImageView yuemeiLogo;
    @BindView(R.id.yuemei_logo_container)
    LinearLayout yuemeiLogoContainer;
    public JSONObject obj_http;
    private Location location;
    private String latitude;
    private String longitude;
    // 定位
    private String mProvinceLocation; //定位省
    private String mCityLocation; //定位市
    private LocationClient locationClient;

    private ZhuanTi zhuanti;
    private ZhuanTi adertZt;
    private SplashApi mSplashApi;


    private boolean isck = false;
    private HashMap<String, String> mEvent_params;

    public static final int SHOW_POP = 0x0111;
    public static final String NO_LOGIN_CHAT ="nologinchat";
    public static final String NO_LOGIN_ID ="nologinid";
    public static final String NO_LOGIN_IMG ="nologinimg";
    private Handler mHandler = new MyHandler(this);
    private ArrayList<PermsissionData> mPermsissionData = new ArrayList<>();
    private PermissionManager mPermissionManager;
    private PrivacyAgreementDialog privacyAgreementDialog;
    private YueMeiDialog yueMeiDialog;
    private final String PRIVACY_TEXT = "根据《电子商务法》规定，为保障您的信息安全，您必须同意此协议才能继续使用，如您确实不同意，可点击“退出应用”，退出此应用。";
    private static class MyHandler extends Handler {
        private final WeakReference<SplashActivity> mActivity;

        private MyHandler(SplashActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SplashActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case SHOW_POP:
                        theActivity.showPop();
                        break;

                }
            }

        }

    }

    @Override
    protected int getLayoutId() {
        return R.layout.acty_start_log;
    }


    @Override
    protected void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            View decorView = window.getDecorView();
            decorView.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
                @Override
                public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                    WindowInsets defaultInsets = v.onApplyWindowInsets(insets);
                    return defaultInsets.replaceSystemWindowInsets(
                            defaultInsets.getSystemWindowInsetLeft(),
                            0,
                            defaultInsets.getSystemWindowInsetRight(),
                            defaultInsets.getSystemWindowInsetBottom());
                }
            });
            ViewCompat.requestApplyInsets(decorView);
            //将状态栏设成透明，如不想透明可设置其他颜色
            window.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
        }
        //给一个淡入动画
        setFadeInAnimation();
    }

    private void setFadeInAnimation() {
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        alpha.setDuration(1000);
        alpha.setFillAfter(true);
        AnimationSet set = new AnimationSet(false);
        set.addAnimation(alpha);
        mRelativeLayout.startAnimation(set);
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        String action = intent.getAction();
        /*在应用程序设置<action android:name="android.intent.action.MAIN" />
        应用程序入口Activity的onCreate方法中加入上面的判断，完美解决应用程序多次重启问题。
		*/
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finished();
            return;
        }
        MobclickAgent.updateOnlineConfig(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        Cfg.saveInt(mContext, FinalConstant.WINDOWS_W, metric.widthPixels);
        Cfg.saveInt(mContext, FinalConstant.WINDOWS_H, metric.heightPixels);
        Log.e(TAG,"widthPixels =="+metric.widthPixels+"/"+"heightPixels =="+metric.heightPixels);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //隐私协议
        privacyAgreementDialog = new PrivacyAgreementDialog(mContext);
        yueMeiDialog = new YueMeiDialog(mContext, PRIVACY_TEXT, "不同意", "同意");
        //是否同意
        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                yueMeiDialog.dismiss();
                privacyAgreementDialog.show();
            }

            @Override
            public void rightBtnClick() {
                yueMeiDialog.dismiss();
                checkPermsission();

            }
        });
        int privacyAgreement = Cfg.loadInt(mContext, "privacy_agreement", 0);
        if (privacyAgreement == 0) {
            privacyAgreementDialog.show();
            privacyAgreementDialog.setOnEventClickListener(new PrivacyAgreementDialog.OnEventClickListener() {
                @Override
                public void onCancelClick(View v) {
                    //不同意
                    privacyAgreementDialog.dismiss();
                    yueMeiDialog.show();
                }

                @Override
                public void onConfirmClick(View v) {
                    //同意
                    privacyAgreementDialog.dismiss();
                    Cfg.saveInt(mContext, "privacy_agreement", 1);
                    checkPermsission();
                }
            });
        }else {
            checkPermsission();
        }

        //        if (Build.VERSION.SDK_INT >= 23) {
//            //减少是否拥有权限
//            int checkCallPhonePermission1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
//            int checkCallPhonePermission2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
//            int checkCallPhonePermission3 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
//
//            Log.e(TAG, "checkCallPhonePermission1 ==" + checkCallPhonePermission1);
//            Log.e(TAG, "checkCallPhonePermission2 ==" + checkCallPhonePermission2);
//            Log.e(TAG, "checkCallPhonePermission3 ==" + checkCallPhonePermission3);
//            if (checkCallPhonePermission1 != PackageManager.PERMISSION_GRANTED) {
//                mPermsissionData.add(new PermsissionData(R.drawable.phone_state_img, "设备信息", "用于提升账号安全，降低盗号及交易中可能遇到的风险"));
//            }
//            if (checkCallPhonePermission2 != PackageManager.PERMISSION_GRANTED) {
//                String firstLoad = Cfg.loadStr(mContext, "isFirstLoad", "");
//                if (TextUtils.isEmpty(firstLoad)) {
//                    Cfg.saveStr(mContext, "isFirstLoad", "yes");
//                    mPermsissionData.add(new PermsissionData(R.drawable.location_img, "定位信息", "用于获得当前地区优惠商品和促销活动"));
//                }
//            }
//            if (checkCallPhonePermission3 != PackageManager.PERMISSION_GRANTED) {
//                mPermsissionData.add(new PermsissionData(R.drawable.external_img, "存储空间", "用于缓存列表，商品图片等信息，将加快您的访问速度并节省流量"));
//            }
//            if (mPermsissionData.size() != 0) {
//                mHandler.sendEmptyMessageDelayed(SHOW_POP, 1000);
//            } else {
//                Init();
//            }
//
//
//        } else {
//            Init();
//        }
    }


    /**
     * 检查权限
     */
    private void checkPermsission() {
        //版本判断 小于6.0直接跳过
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            Init();
        }else {
            mPermsissionData.add(new PermsissionData(R.drawable.phone_state_img, "设备信息", "用于提升账号安全，降低盗号及交易中可能遇到的风险", Manifest.permission.READ_PHONE_STATE));
            String firstLoad = Cfg.loadStr(mContext, "isFirstLoad", "");
                if (TextUtils.isEmpty(firstLoad)) {
                    Cfg.saveStr(mContext, "isFirstLoad", "yes");
                    mPermsissionData.add(new PermsissionData(R.drawable.location_img, "定位信息", "用于获得当前地区优惠商品和促销活动",Manifest.permission.ACCESS_COARSE_LOCATION));
                }
            mPermsissionData.add(new PermsissionData(R.drawable.external_img, "存储空间", "用于缓存列表，商品图片等信息，将加快您的访问速度并节省流量",Manifest.permission.WRITE_EXTERNAL_STORAGE));
            mPermissionManager = new PermissionManager(this, new PermissionManager.PermissionUtilsInter() {
                @Override
                public List<PermsissionData> getApplyPermissions() {
                    return mPermsissionData;
                }
                @Override
                public void goInit(){
                    Init();
                }
            });
            boolean isOk = mPermissionManager.checkPermission();
            if (isOk){
                Init();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mPermissionManager != null){
            boolean isOk = mPermissionManager.onRequestPermissionsResults(this, requestCode, permissions, grantResults);
            if (isOk){
                Init();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @NonNull Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionManager.REQUEST_SETTING_CODE){
            if (mPermissionManager != null){
                boolean isOk = mPermissionManager.onActivityResults(requestCode);
                if (isOk){
                    Init();
                }
            }
        }
    }



    private void initShenHe() {
        new ShenHeApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {

            @Override
            public void onSuccess(ShenHeData shenHeData) {
                if (shenHeData != null) {
                    Log.e(TAG,"nologinchat == "+shenHeData.getNologinchat());
                    Cfg.saveStr(mContext,NO_LOGIN_CHAT,shenHeData.getNologinchat());
                    NoLoginBean user = shenHeData.getUser();
                    if (null != user){
                        String userId = user.getUser_id();//游客用户id
                        Cfg.saveStr(mContext,NO_LOGIN_ID,userId);
                        Cfg.saveStr(mContext,NO_LOGIN_IMG,user.getImg());
                    }else {
                        Cfg.saveStr(mContext,NO_LOGIN_ID,"");
                    }

                }
            }
        });
    }

    private void showPop() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = 0.6f;
        getWindow().setAttributes(lp);
        final SplashPermissionPop permissionPop = new SplashPermissionPop(mContext, mPermsissionData);
        permissionPop.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        permissionPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.alpha = 1f;
                getWindow().setAttributes(lp);
            }
        });
        permissionPop.setPermissionCallBack(new SplashPermissionPop.PermissionCallBack() {
            @Override
            public void onPermissionCallBack() {
                permissionPop.dismiss();
                Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, READ_PHONE_STATE, WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        Init();
                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        if (null != permissions.get(0) && permissions.get(0).equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            mCityLocation = "全国";
                            mProvinceLocation = "全国";
                            Cfg.saveStr(mContext, "city_dingwei", "全国");

                            Cfg.saveStr(mContext, FinalConstant.TAOCITY, "全国");
                        }
                        if (permissions.size() == 2) {
                            if (null != permissions.get(1) && permissions.get(1).equals(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                                mCityLocation = "全国";
                                mProvinceLocation = "全国";
                                Cfg.saveStr(mContext, "city_dingwei", "全国");

                                Cfg.saveStr(mContext, FinalConstant.TAOCITY, "全国");
                            }
                        }
                        Init();
                    }
                });
            }
        });
    }

    private void Init() {
        //联网接口注册
        BuildConfig.registeredInterface();
        initLocation();

        Utils.getInfo(mContext);
        yunxingStart();
        //保存sessionid
        Utils.setSessionid();
        //统计接口初始化
        initStatistical();
        initShenHe();
    }

    @Override
    @OnClick({R.id.adver_iv, R.id.skip_bt_ly, R.id.yuemei_logo_container})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.adver_iv:
                splashJump();
                break;
            case R.id.skip_bt_ly:
                if (!Utils.isNetworkAvailable(mContext)) {
                    showActivity(mContext, MainTableActivity.class);
                    return;
                }
                isck = true;
                redirectTo();
                break;
            case R.id.yuemei_logo_container:
                splashJump();
                break;
        }
    }


    /**
     * 统计接口初始化
     */
    private void initStatistical() {
        //点击统计初始化
        YmStatistics.getInstance().initStatistics();
    }


    private void splashJump() {
        if (null != zhuanti) {
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.START_CLICK, "1"), mEvent_params);
            String type = zhuanti.getType();
            if (!TextUtils.isEmpty(zhuanti.getLink())) {
                isck = true;
            }
            Cfg.saveStr(mContext, "start_bb", "1");
            if ("1".equals(type)) {// 帖子
                String id = zhuanti.get_id();
                String url = zhuanti.getLink();

                StatService.onEvent(mContext, "010", "帖子" + id, 1);

                Intent it2 = new Intent();
                it2.putExtra("url", url);
                it2.putExtra("qid", id);
                it2.setClass(mContext, DiariesAndPostsActivity.class);
                startActivity(it2);
            } else if ("418".equals(type)) {// 淘整形
                String id = zhuanti.get_id();

                StatService.onEvent(mContext, "010", "淘整形" + id, 1);

                Intent it1 = new Intent();
                it1.putExtra("id", id);
                it1.putExtra("source", "0");
                it1.putExtra("objid", "0");
                it1.setClass(mContext, TaoDetailActivity.class);
                startActivity(it1);
            } else if ("999".equals(type)) {
                String url = zhuanti.getLink();
                String shareTitle = zhuanti.getTitle();
                String sharePic = zhuanti.getImg();


                StatService.onEvent(mContext, "010", "999悦美网页" + url, 1);

                Intent it1 = new Intent();
                it1.setClass(mContext, SlidePicTitieWebActivity.class);
                it1.putExtra("shareTitle", shareTitle);
                it1.putExtra("sharePic", sharePic);
                it1.putExtra("url", url);
                startActivity(it1);
            } else if ("1000".equals(type)) {// 专题
                String url = zhuanti.getLink();
                String title = zhuanti.getTitle();
                String ztid = zhuanti.get_id();

                StatService.onEvent(mContext, "010", "专题" + ztid, 1);

                Intent it1 = new Intent();
                it1.setClass(mContext, ZhuanTiWebActivity.class);
                it1.putExtra("url", url);
                it1.putExtra("title", title);
                it1.putExtra("ztid", ztid);
                startActivity(it1);
            } else if ("5701".equals(type)) {// 医生
                String id = zhuanti.get_id();
                String docname = zhuanti.getTitle();

                StatService.onEvent(mContext, "010", "医生" + id, 1);

                Intent it = new Intent();
                it.setClass(mContext, DoctorDetailsActivity592.class);
                it.putExtra("docId", id);
                it.putExtra("docName", docname);
                it.putExtra("partId", "");
                startActivity(it);
            } else if ("511".equals(type)) {// 医院
                String hosid = zhuanti.get_id();

                StatService.onEvent(mContext, "010", "医院" + hosid, 1);

                Intent it = new Intent(mContext, HosDetailActivity.class);
                it.putExtra("hosid", hosid);
                startActivity(it);
            } else {
                if (!TextUtils.isEmpty(zhuanti.getLink())) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(zhuanti.getLink());
                }
            }
        }
    }


    void yunxingStart() {

        PushManager.startWork(mContext, PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);

        //获取要统计的路径
        NetWork.getInstance().call(FinalConstant1.MESSAGE, "gettjArr", new HashMap<String, Object>(), new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Cfg.saveStr(mContext, FinalConstant.MESSAGE_ARR, mData.data);
            }
        });

        if (SystemTool.checkOnlyWifi(mContext)) {
            //获取要统计的接口
            StatisticalPath.getInstance().getStatistical();

            if (Utils.hourDayBetween(Utils.getDay1(), "2019-10-27", "2019-11-19")) {
                Log.e(TAG, "播放视频");
                if (!"1".equals(mFunctionManager.loadStr(FinalConstant.IS_START_VIDEO, "0"))) {
                    String rawPath = "android.resource://" + getPackageName() + "/" + R.raw.double_eleven_video;

                    videoViewClick.setVisibility(View.VISIBLE);
                    Log.e(TAG, "windowsHeight == " + windowsHeight);
                    videoView.setVideoSize(windowsWight, windowsHeight);
                    videoView.setVideoPath(rawPath);
                    videoView.start();

                    //曝光统计
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("to_page_type", "16");
                    hashMap.put("to_page_id", "11553");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.START_VIDEO_CLICK, "show"), hashMap);

                    mFunctionManager.saveStr(FinalConstant.IS_START_VIDEO, "1");

                    //视频点击
                    videoViewClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("to_page_type", "16");
                            hashMap.put("to_page_id", "11553");
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.START_VIDEO_CLICK, "yes"), hashMap);
                            isck = true;
                            Cfg.saveStr(mContext, "start_aa", "1");
                            WebUrlTypeUtil.getInstance(mContext).urlToApp("https://m.yuemei.com/tao_zt/11553.html");
                        }
                    });
                    skip3Miao();
                } else {
                    videoViewClick.setVisibility(View.GONE);
                }
            } else {
                videoViewClick.setVisibility(View.GONE);
            }
            if (!Utils.isNetworkAvailable(mContext)) {
                skip3Miao();
                Toast.makeText(mContext, "网络加载失败，请检查您的网络", Toast.LENGTH_SHORT).show();

            } else {
                initAdvertData();
                skip3Miao();
            }


        } else {
            ViewInject.toast(mContext.getResources().getString(R.string.no_wifi_tips));
            redirectTo();
        }
    }


    public void finished() {
        finish();
        overridePendingTransition(R.anim.pop_enter, R.anim.pop_exit);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        String xx = Cfg.loadStr(mContext, "start_aa", "");
        if ("1".equals(xx)) {
            skipActivity(this, MainTableActivity.class);
            Cfg.saveStr(mContext, "start_aa", "");
        }

        String xx1 = Cfg.loadStr(mContext, "start_bb", "");
        if (xx1.equals("1")) {
            redirectTo();
            Cfg.saveStr(mContext, "start_bb", "");
        }
    }

    public void initAdvertData() {
        mSplashApi = new SplashApi();
        mSplashApi.getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        AdertAdv advertData = JSONUtil.TransformSingleBean(serverData.data, AdertAdv.class);
                        //首页tablayout上banner数据
                        Cfg.saveStr(mContext, "home_banner_tab_top_data", new Gson().toJson(advertData.getBigPromotionBall()));
                        //首页二楼数据
                        Cfg.saveStr(mContext, "home_secondFloor_data", new Gson().toJson(advertData.getSecondFloor()));
                        //大促时候一堆吸顶的背景
                        Cfg.saveStr(mContext, "other_background_data", new Gson().toJson(advertData.getOtherBackgroundImg()));

                        zhuanti = advertData.getAdv();
                        adertZt = advertData.getAlert();
                        SneakGuest sneakGuest = advertData.getSneakGuest();
                        String foramSetType = advertData.getForamSetType();
                        String loadDoctorHtml = advertData.getLoadDoctorHtml();
                        Cfg.saveStr(mContext, FinalConstant.IS_WEBDOCTOR, loadDoctorHtml);
                        mEvent_params = advertData.getAdv().getEvent_params();
                        Log.e(TAG, "adertZt === " + adertZt);

                        Log.e(TAG, "sneakGuest === " + sneakGuest);
                        RecoveryReminder recoveryReminder = advertData.getRecoveryReminder();

                        if (sneakGuest != null) {
                            Cfg.saveStr(mContext, FinalConstant.POTENTIAL_CUSTOMER, new Gson().toJson(sneakGuest));
                        }
                        if (recoveryReminder != null) {
                            String showTitle = recoveryReminder.getShow_title();
                            if (!TextUtils.isEmpty(showTitle)) {
                                if (Utils.isLogin()) {
                                    String homeShowTitle = Cfg.loadStr(mContext, "home_show_title", "");
                                    if (TextUtils.isEmpty(homeShowTitle)) {
                                        Cfg.saveStr(mContext, "home_show_title", showTitle);
                                        Cfg.saveStr(mContext, "show_title", "show");
                                        Log.e(TAG, "show =========");
                                    } else {
                                        if (showTitle.equals(homeShowTitle)) {
                                            Cfg.saveStr(mContext, "show_title", "no");
                                            Log.e(TAG, "no =========");
                                        } else {
                                            Cfg.saveStr(mContext, "show_title", "show");
                                            Log.e(TAG, "show two =========");
                                        }
                                    }
                                }
                            }
                        }
                        if (null != adertZt) {
                            String imgurl = adertZt.getImg();
                            String[] imgs = imgurl.split("/");
                            String img = imgs[imgs.length - 1];
                            String link = adertZt.getLink();
                            String id = adertZt.get_id();
                            String type = adertZt.getType();
                            String title = adertZt.getTitle();
                            HashMap<String, String> event_params = adertZt.getEvent_params();

                            Cfg.saveStr(mContext, "ad_imgurl", imgurl);
                            Cfg.saveStr(mContext, "ad_img", img);
                            Cfg.saveStr(mContext, "ad_link", link);
                            Cfg.saveStr(mContext, "ad_id", id);
                            Cfg.saveStr(mContext, "ad_type", type);
                            Cfg.saveStr(mContext, "ad_title", title);
                            Cfg.saveStr(mContext, FinalConstant.HOME_STYLE, foramSetType);
                            Cfg.putHashMapData(mContext, "ad_event_params", event_params);

                            if (advertData.getTabbar().getTab().size() > 0) {
                                Cfg.saveStr(mContext, FinalConstant.START_TABBAR, serverData.data);
                            } else {
                                Cfg.saveStr(mContext, FinalConstant.START_TABBAR, "");
                            }

                            if (videoViewClick.getVisibility() == View.GONE) {
                                if (!TextUtils.isEmpty(zhuanti.getImg()) && !TextUtils.isEmpty(zhuanti.getImg_new())) {
                                    float scale = (float) windowsWight / (windowsHeight - 198);
                                    Log.e(TAG, "windowsWight == " + windowsWight);
                                    Log.e(TAG, "windowsHeight == " + windowsHeight);
                                    float sclae2 = (float) 750 / 1324;
                                    Log.e(TAG, "scale == " + scale);
                                    final String netImage = scale < sclae2 ? zhuanti.getImg_new() : zhuanti.getImg();

                                    if (FileUtils.fileIsExists(Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/" + netImage.substring(netImage.lastIndexOf("/") + 1))) {
                                        Glide.with(mContext)
                                                .load(Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/" + netImage.substring(netImage.lastIndexOf("/") + 1))
                                                .into(new SimpleTarget<GlideDrawable>() {
                                                    @Override
                                                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                                        //背景宽度
                                                        int intrinsicWidth = resource.getIntrinsicWidth();
                                                        int intrinsicHeight = resource.getIntrinsicHeight();

                                                        ViewGroup.LayoutParams layoutParams = adverIv.getLayoutParams();
                                                        int currHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                                                        layoutParams.height = currHeight;
                                                        Log.e(TAG, "height =" + (windowsWight * intrinsicHeight) / intrinsicWidth);
                                                        adverIv.setLayoutParams(layoutParams);
                                                        ViewGroup.LayoutParams containerParams = yuemeiLogoContainer.getLayoutParams();
                                                        containerParams.height = windowsHeight - currHeight;
                                                        yuemeiLogoContainer.setLayoutParams(containerParams);
                                                        yuemeiLogo.setBackgroundResource(R.drawable.splash_img);
                                                        adverIv.setImageDrawable(resource);
                                                    }
                                                });
                                    } else {
                                        //如果有新的图片 删除文件夹下之前的图片
                                        if (!Cfg.loadStr(mContext, "splash_img_name", "").equals(netImage.substring(netImage.lastIndexOf("/") + 1))) {
                                            FileUtils.delFile(Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/" + Cfg.loadStr(mContext, "splash_img_name", ""));
                                        }
                                        DownloadUtil.get().download(netImage,
                                                Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/",
                                                netImage.substring(netImage.lastIndexOf("/") + 1),
                                                new DownloadUtil.OnDownloadListener() {
                                                    @Override
                                                    public void onDownloadSuccess(File file) {
                                                        Log.i("download", "成功");
                                                        Cfg.saveStr(mContext, "splash_img_name", netImage.substring(netImage.lastIndexOf("/") + 1));
                                                    }

                                                    @Override
                                                    public void onDownloading(int progress) {
                                                        Log.i("download", "进度：" + progress);
                                                    }

                                                    @Override
                                                    public void onDownloadFailed(Exception e) {
                                                        Log.i("download", "失败");
                                                    }
                                                });
                                        Glide.with(mContext)
                                                .load(netImage)
                                                .into(new SimpleTarget<GlideDrawable>() {
                                                    @Override
                                                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                                        //背景宽度
                                                        int intrinsicWidth = resource.getIntrinsicWidth();
                                                        int intrinsicHeight = resource.getIntrinsicHeight();
                                                        Log.e(TAG, "intrinsicWidth =" + intrinsicWidth);
                                                        Log.e(TAG, "intrinsicHeight =" + intrinsicHeight);
                                                        Log.e(TAG, "windowsWight =" + windowsWight);
                                                        Log.e(TAG, "windowsHeight =" + windowsHeight);

                                                        ViewGroup.LayoutParams layoutParams = adverIv.getLayoutParams();
                                                        int currHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                                                        layoutParams.height = currHeight;
                                                        Log.e(TAG, "height =" + (windowsWight * intrinsicHeight) / intrinsicWidth);
                                                        adverIv.setLayoutParams(layoutParams);
                                                        ViewGroup.LayoutParams containerParams = yuemeiLogoContainer.getLayoutParams();
                                                        containerParams.height = windowsHeight - currHeight;
                                                        yuemeiLogoContainer.setLayoutParams(containerParams);
                                                        yuemeiLogo.setBackgroundResource(R.drawable.splash_img);
                                                        adverIv.setImageDrawable(resource);
                                                    }
                                                });
                                    }
                                    adverIvClick.setVisibility(View.VISIBLE);
                                } else {
                                    adverIvClick.setVisibility(View.GONE);
                                }
                            }
//                            skip3Miao();

                        } else {
//                            skip3Miao();
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                } else {
                    if (FileUtils.fileIsExists(Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/" + Cfg.loadStr(mContext, "splash_img_name", ""))) {
                        Glide.with(mContext)
                                .load(Environment.getExternalStorageDirectory().getAbsolutePath() + "/YUEMEI/splash/" + Cfg.loadStr(mContext, "splash_img_name", ""))
                                .into(new SimpleTarget<GlideDrawable>() {
                                    @Override
                                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                        //背景宽度
                                        int intrinsicWidth = resource.getIntrinsicWidth();
                                        int intrinsicHeight = resource.getIntrinsicHeight();

                                        ViewGroup.LayoutParams layoutParams = adverIv.getLayoutParams();
                                        int currHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;
                                        layoutParams.height = currHeight;
                                        Log.e(TAG, "height =" + (windowsWight * intrinsicHeight) / intrinsicWidth);
                                        adverIv.setLayoutParams(layoutParams);
                                        ViewGroup.LayoutParams containerParams = yuemeiLogoContainer.getLayoutParams();
                                        containerParams.height = windowsHeight - currHeight;
                                        yuemeiLogoContainer.setLayoutParams(containerParams);
                                        yuemeiLogo.setBackgroundResource(R.drawable.splash_img);
                                        adverIv.setImageDrawable(resource);
                                    }
                                });

                    }
                    adverIvClick.setVisibility(View.VISIBLE);
//                    skip3Miao();
                }
            }

        });

        new QiNiuTokenApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                        Cfg.saveStr(mContext, FinalConstant.QINIUTOKEN, qiNiuBean.getQiniu_token());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void skip3Miao() {
        if (startSkipBt.getVisibility() == View.GONE) {
            startSkipBt.setVisibility(View.VISIBLE);
            Log.e("sdssadasasdasd", "videoViewClick.getVisibility()== " + (videoViewClick.getVisibility() == View.GONE ? 4500 : 6500));
            new CountDownTimer(videoViewClick.getVisibility() == View.GONE ? 4500 : 6500, 1000) {
                // 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。
                @SuppressLint("SetTextI18n")
                @Override
                public void onTick(long millisUntilFinished) {
                    startLogTimeTv.setText((millisUntilFinished / 1000) + "");
                }

                @Override
                public void onFinish() {
                    if (!isck) {
                        redirectTo();
                    }
                }
            }.start();
        }
    }

    private void redirectTo() {

//        String firstlod = Cfg.loadStr(mContext, "firstload", "");
//        if (TextUtils.isEmpty(firstlod)) {
//            initColdData();
//        } else {
        String scheme = getIntent().getScheme();    // 获得Scheme名称
        String data = getIntent().getDataString();  // 获得Uri全部路径
        Log.e(TAG,"scheme =="+scheme);
        Log.e(TAG,"data =="+data);
        if (videoViewClick.getVisibility() == View.GONE) {
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.START_JUMP, "jump"), mEvent_params);
        } else {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("to_page_type", "16");
            hashMap.put("to_page_id", "11553");
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.START_VIDEO_CLICK, "jump"), hashMap);
        }

        if (!TextUtils.isEmpty(scheme)) {

            data = data.substring(16);

            Cfg.saveStr(mContext, "start_aa", "1");
            try {
                ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                parserWebUrl.parserPagrms(data);
                JSONObject obj = parserWebUrl.jsonObject;
                obj_http = obj;

                if (obj.getString("type").equals("1")) {// 淘整形
                    try {
                        String id = obj.getString("id");
                        Intent it = new Intent();
                        it.setClass(mContext, TaoDetailActivity.class);
                        it.putExtra("id", id);
                        it.putExtra("source", "0");
                        it.putExtra("objid", "0");
                        startActivity(it);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (obj.getString("type").equals("2")) {// 帖子
                    try {
                        String id = obj.getString("id");
                        String url = obj.getString("url");
                        Intent it = new Intent();
                        it.setClass(mContext, DiariesAndPostsActivity.class);
                        it.putExtra("qid", id);
                        it.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + "/" + url);
                        startActivity(it);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (obj.getString("type").equals("0")) {// web
                    try {
                        String link = obj.getString("link");
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(link, "0", "0");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (obj.getString("type").equals("431")) {// 个人信息
                    try {
                        String id = obj.getString("id");
                        Intent it = new Intent();
                        it.setClass(mContext, PersonCenterActivity641.class);
                        it.putExtra("id", id);
                        startActivity(it);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (obj.getString("type").equals("6429")) {        //统计第三方应用启动
                    String source = obj.getString("source");

                    Utils.tongjiApp(mContext, "openAPP", "0", source, "1");

                    showActivity(mContext, MainTableActivity.class);
                    finished();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            showActivity(mContext, MainTableActivity.class);
            finished();
        }
    }

    public void showActivity(Activity mContext, Class<?> cls) {
        Intent intent = new Intent(mContext, cls);
        mContext.startActivity(intent);
    }

    public void skipActivity(Activity aty, Class<?> cls) {
        this.showActivity(aty, cls);
        finished();
    }


    /**
     * 初始话百度的api定位
     */
    private void initLocation() {
        try {
            locationClient = new LocationClient(getApplicationContext());
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(bdLocationListener);
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //百度定位回调接口
    BDLocationListener bdLocationListener = new BDLocationListener() {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            // 设置editTextLocation为返回的百度地图获取到的街道信息
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                location = new Location();
                location.setAddress(bdLocation.getAddrStr());

                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);
                //34.3361690000,108.9569500000

                latitude = bdLocation.getLatitude() + "";
                longitude = bdLocation.getLongitude() + "";
                Log.e(TAG, "保存经度 == " + latitude);
                Log.e(TAG, "保存纬度 == " + longitude);
                int locType = bdLocation.getLocType();
                Log.e(TAG, "locType == " + locType);
                //判断是否定位失败
                if (!"4.9E-324".equals(latitude) && !"4.9E-324".equals(longitude)) {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, latitude);
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, longitude);
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, "0");
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, "0");
                    MyToast.makeTextToast2(mContext, "定位失败，请查看手机是否开启了定位权限", MyToast.SHOW_TIME).show();
                }

                String province = bdLocation.getProvince();
                String city = bdLocation.getCity();
                if (!TextUtils.isEmpty(province)) {
                    if (province.contains("省")) {
                        mProvinceLocation = province.substring(0, province.length() - 1);
                    }
                    if (city.contains("市")) {
                        mProvinceLocation = province.substring(0, province.length() - 1);
                    }
                    if (province.contains("自治区")) {
                        mProvinceLocation = province.substring(0, province.length() - 3);
                    }
                }
                if (!TextUtils.isEmpty(city)) {

                    if (city.contains("省")) {
                        mCityLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (city.contains("市")) {
                        mCityLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (city.contains("自治区")) {
                        mCityLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }
                    HashSet<String> strings = new HashSet<>();
                    strings.add(mProvinceLocation);
                    strings.add(mCityLocation);
//                    JPushInterface.setTags(mContext, 1, strings);
                    Cfg.saveStr(mContext, "city_dingwei", mCityLocation);
                    Cfg.saveStr(mContext, FinalConstant.DWPROVINCE, mProvinceLocation);
                    Cfg.saveStr(mContext, FinalConstant.TAOCITY, mCityLocation);
                    String sss = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
                    if (!TextUtils.isEmpty(sss) && !"失败".equals(sss)) {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, sss);
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, mCityLocation);
                    }


                } else {
                    mCityLocation = "失败";
                    mProvinceLocation = "全国";
                    Cfg.saveStr(mContext, "city_dingwei", "全国");
                    Cfg.saveStr(mContext, FinalConstant.DWPROVINCE, "全国");
                    Cfg.saveStr(mContext, FinalConstant.TAOCITY, "全国");

                    String sss = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
                    if (!TextUtils.isEmpty(sss) && !"失败".equals(sss)) {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, sss);
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, "全国");
                    }

                }
                // 请求定位
                if (!TextUtils.isEmpty(mCityLocation) && !TextUtils.isEmpty(mProvinceLocation)) {
                    String registrationID = JPushInterface.getRegistrationID(mContext);
                    HashMap<String, Object> maps = new HashMap<>();
                    String version = Build.VERSION.RELEASE;// String
                    String mtype = Build.MODEL; // 手机型号
                    String mtyb = Build.BRAND;// 手机品牌
                    String brand = mtyb + "_" + mtype;
                    maps.put("reg_id", registrationID);

                    maps.put("location_city", mCityLocation);
                    maps.put("location_Area", mProvinceLocation);
                    maps.put("brand", brand);
                    maps.put("system", version);
                    new JPushBindApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            Log.e(TAG, "message====" + serverData.message);
                        }
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        if (locationClient != null) {
            locationClient.stop();
        }
        super.onDestroy();
    }
}
