package com.module.api;

/**
 * Created by 裴成浩 on 2019/1/28
 */
public class TaoDetailShow {
    private  String taoid;
    public TaoDetailShow(String taoid) {
        this.taoid = taoid;
    }

    public String getTaoid() {
        return taoid;
    }

    public void setTaoid(String taoid) {
        this.taoid = taoid;
    }
}
