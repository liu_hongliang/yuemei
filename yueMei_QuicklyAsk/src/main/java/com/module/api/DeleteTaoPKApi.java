package com.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.Map;

/**
 * 删除对比
 * Created by Administrator on 2017/10/13.
 */

public class DeleteTaoPKApi implements BaseCallBackApi {
    private String TAG = "DeleteTaoPKApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.TAOPK, "deltaopk", maps, new ServerCallback() {

            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                listener.onSuccess(mData);
            }
        });
    }
}
