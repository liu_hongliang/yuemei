package com.module.commonview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.module.commonview.activity.ChatActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.LookProblemsActivity;
import com.module.commonview.activity.ProjectDetailActivity638;
import com.module.commonview.activity.QuestionsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.my.controller.activity.QuestionAnswerActivity;
import com.module.my.controller.activity.QuestionDetailsActivity;
import com.module.my.controller.activity.WriteNoteActivity;
import com.module.other.activity.CashWebViewActivity;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by 裴成浩 on 2017/10/10.
 */

public class PageJumpManager {
    public static final int DEFAULT_LOGO = -100;
    private Context mContext;
    private Activity mActivity;
    private String TAG = "PageJumpManager";

    public PageJumpManager(Activity activity) {
        this.mActivity = activity;
    }

    public PageJumpManager(Context context) {
        this.mContext = context;
    }


    public void jumpToProjectDetailActivity550(Map<String, String> mMap) {
        Intent mIntent = new Intent();
        mIntent.putExtra("oneid", mMap.get("oneid"));
        mIntent.putExtra("onetitle", mMap.get("onetitle"));
        mIntent.putExtra("twoid", mMap.get("twoid"));
        mIntent.putExtra("twotitle", mMap.get("twotitle"));
        mIntent.putExtra("threeid", mMap.get("threeid"));
        mIntent.putExtra("threetitle", mMap.get("threetitle"));
        mIntent.putExtra("medthod", mMap.get("medthod"));
        startJump(mIntent, ProjectDetailActivity638.class);
    }


    public void jumpToChatBaseActivity(ChatParmarsData chatParmarsData) {
        Intent mIntent = new Intent();
        if (!TextUtils.isEmpty(chatParmarsData.getDirectId())) {
            mIntent.putExtra("directId", chatParmarsData.getDirectId());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getObjId())) {
            mIntent.putExtra("objId", chatParmarsData.getObjId());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getObjType())) {
            mIntent.putExtra("objType", chatParmarsData.getObjType());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getTitle())) {
            mIntent.putExtra("title", chatParmarsData.getTitle());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getPrice())) {
            mIntent.putExtra("price", chatParmarsData.getPrice());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getImg())) {
            mIntent.putExtra("img", chatParmarsData.getImg());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getMemberPrice())) {
            mIntent.putExtra("plus", chatParmarsData.getMemberPrice());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getUrl())) {
            mIntent.putExtra("url", chatParmarsData.getUrl());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getYmClass())) {
            mIntent.putExtra("ymClass", chatParmarsData.getYmClass());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getYmId())) {
            mIntent.putExtra("ymId", chatParmarsData.getYmId());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getSkuId())) {
            mIntent.putExtra("skuId", chatParmarsData.getSkuId());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getZt_title())) {
            mIntent.putExtra("zt_title", chatParmarsData.getZt_title());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getZt_id())) {
            mIntent.putExtra("zt_id", chatParmarsData.getZt_id());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getGroup_id())) {
            mIntent.putExtra("group_id", chatParmarsData.getGroup_id());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getClassid())){
            mIntent.putExtra("classid", chatParmarsData.getClassid());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getContent())){
            mIntent.putExtra("content", chatParmarsData.getContent());
        }
        if (!TextUtils.isEmpty(chatParmarsData.getQuick_reply())){
            mIntent.putExtra("quick_reply", chatParmarsData.getQuick_reply());
        }
        startJump(mIntent, ChatActivity.class);

    }

    /**
     * 提问页跳转
     */
    public void jumpToQuestionsActivity(String taoid, String img, String title, String priceDiscount, String price) {
        Intent mIntent = new Intent();
        mIntent.putExtra("taoid", taoid);
        mIntent.putExtra("img", img);
        mIntent.putExtra("title", title);
        mIntent.putExtra("priceDiscount", priceDiscount);
        mIntent.putExtra("price", price);
        Log.e(TAG, "taoid == " + taoid);
        Log.e(TAG, "img == " + img);
        Log.e(TAG, "priceDiscount == " + priceDiscount);
        Log.e(TAG, "price == " + price);
        startJump(mIntent, QuestionsActivity.class);
    }

    /**
     * 查看问题页跳转
     */
    public void jumpToLookProblemsActivity(String taoid, String title) {
        Intent mIntent = new Intent();
        mIntent.putExtra("taoid", taoid);
        mIntent.putExtra("title", title);

        Log.e(TAG, "taoid 1== " + taoid);
        Log.e(TAG, "title 1== " + title);
        startJump(mIntent, LookProblemsActivity.class);
    }

    /**
     * webView跳转
     *
     * @param url
     * @param title
     */
    public void jumpToCashWebViewActivity(String url, String title) {
        Intent mIntent = new Intent();
        mIntent.putExtra("url", url);
        mIntent.putExtra("title", title);
        startJump(mIntent, CashWebViewActivity.class);
    }


    /**
     * 问答webView跳转
     *
     * @param url
     * @param title
     */
    public void jumpToQuestionAnswerActivity(String url, String title) {
        Intent mIntent = new Intent();
        mIntent.putExtra("url", url);
        mIntent.putExtra("title", title);
        startJump(mIntent, QuestionAnswerActivity.class);
    }

    /**
     * 问题详情页webView跳转
     *
     * @param url
     * @param title
     * @param questionId
     */
    public void jumpToQuestionDetailsActivity(String url, String title, String questionId) {
        Intent mIntent = new Intent();
        mIntent.putExtra("url", url);
        mIntent.putExtra("title", title);
        mIntent.putExtra("questionId", questionId);
        startJump(mIntent, QuestionDetailsActivity.class);
    }


    /**
     * 编辑日记页跳转(待回调的)
     */
    public void jumpToWriteNoteActivity(int uniqueLogo, Map<String, String> mMap) {
        Intent mIntent = new Intent();
        Log.e(TAG, "mMap.get(\"type\") === " + mMap.get("type"));
        mIntent.putExtra("cateid", mMap.get("cateid"));
        mIntent.putExtra("userid", mMap.get("userid"));
        mIntent.putExtra("hosid", mMap.get("hosid"));
        mIntent.putExtra("hosname", mMap.get("hosname"));
        mIntent.putExtra("docname", mMap.get("docname"));
        mIntent.putExtra("fee", mMap.get("fee"));
        mIntent.putExtra("taoid", mMap.get("taoid"));
        mIntent.putExtra("server_id", mMap.get("server_id"));
        mIntent.putExtra("sharetime", mMap.get("sharetime"));
        mIntent.putExtra("type", mMap.get("type"));
        mIntent.putExtra("noteid", mMap.get("noteid"));
        mIntent.putExtra("notetitle", mMap.get("notetitle"));
        mIntent.putExtra("addtype", mMap.get("addtype"));
        mIntent.putExtra("beforemaxday", mMap.get("beforemaxday"));
        mIntent.putExtra("consumer_certificate", mMap.get("consumer_certificate"));//消费凭证，如果是1就显示

        mIntent.putExtra("noteiv", mMap.get("noteiv"));
        mIntent.putExtra("notetitle_", mMap.get("notetitle_"));
        mIntent.putExtra("notefutitle", mMap.get("notefutitle"));
        mIntent.putExtra("page", mMap.get("page"));


        if (uniqueLogo > 0) {
            startCallbackJump(mIntent, WriteNoteActivity.class, uniqueLogo);
        } else {
            startJump(mIntent, WriteNoteActivity.class);
        }
    }

    /**
     * 图片选择器跳转
     *
     * @param uniqueLogo：唯一标识
     * @param maxNumber：要选择的图像的最大数量
     * @param minImageSize：将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
     * @param showCamera：显示摄像机或不
     * @param mResults：将当前选定的图像作为初始值传递
     */
    public void jumpToImagesSelectorActivity(int uniqueLogo, int maxNumber, int minImageSize, boolean showCamera, ArrayList<String> mResults) {
        Intent mIntent = new Intent();
        mIntent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, maxNumber);
        mIntent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, minImageSize);
        mIntent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, showCamera);
        mIntent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);

        startCallbackJump(mIntent, ImagesSelectorActivity.class, uniqueLogo);

    }

    /**
     * 普通跳转
     *
     * @param jumpClass：要跳转的页面
     */
    private void startJump(Intent mIntent, Class jumpClass) {
        if (mActivity != null) {
            mIntent.setClass(mActivity, jumpClass);
            mActivity.startActivity(mIntent);
            return;
        }

        if (mContext != null) {
            mIntent.setClass(mContext, jumpClass);
            mContext.startActivity(mIntent);
            return;
        }

    }

    /**
     * 回调跳转
     *
     * @param jumpClass：要跳转的页面
     * @param uniqueLogo：标识
     */
    private void startCallbackJump(Intent mIntent, Class jumpClass, int uniqueLogo) {
        if (mActivity != null) {
            mIntent.setClass(mActivity, jumpClass);
            mActivity.startActivityForResult(mIntent, uniqueLogo);
        }
    }

    public void jump(String url, String id, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("url", url);
        i.putExtra("qid", id);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }

    public void jump(String id, String source, String objid, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("id", id);
        i.putExtra("source", source);
        i.putExtra("objid", objid);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }

    public void jump1(String shareTitle, String sharePic, String url, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("shareTitle", shareTitle);
        i.putExtra("sharePic", sharePic);
        i.putExtra("url", url);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }

    public void jump2(String url, String title, String ztid, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("url", url);
        i.putExtra("title", title);
        i.putExtra("ztid", ztid);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }

    public void jump3(String id, String docName, String partId, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("docId", id);
        i.putExtra("docName", docName);
        i.putExtra("partId", partId);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }

    public void jump(String hosid, Class jumpClass) {
        Intent i = new Intent();
        i.putExtra("hosid", hosid);
        i.setClass(mContext, jumpClass);
        mContext.startActivity(i);
    }
}
