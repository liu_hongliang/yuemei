package com.module.commonview.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.module.commonview.adapter.CategoryAdapter;
import com.module.commonview.adapter.DiscountExpiredAdapter;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.CategoryBean;
import com.module.my.model.bean.DiscountExpiredInfo;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;


/**
 * 文 件 名: DialogUtils
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-16 16:18
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class DialogUtils {
    private static Dialog dialog;
    private static DiscountExpiredAdapter discountExpiredAdapter;

    //优惠过期dialog
    public static void showDiscountExpiredDialog(final Context context, String hint, final DiscountExpiredInfo discountExpiredInfo, final CallBack callBack) {
        if (context == null)
            return;
        closeDialog();
        dialog = new Dialog(context, R.style.DiscountExpiredDialog);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View inflate = View.inflate(context, R.layout.dialog_discount_expired, null);
        dialog.setContentView(inflate);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    closeDialog();
                    return true;
                }
                return false;
            }
        });
        LinearLayout ll_root = inflate.findViewById(R.id.ll_root);
        LinearLayout ll1 = inflate.findViewById(R.id.ll1);
        LinearLayout ll2 = inflate.findViewById(R.id.ll2);
        TextView tv_title = inflate.findViewById(R.id.tv_title);
        TextView tv_hint = inflate.findViewById(R.id.tv_hint);
        ListView lv = inflate.findViewById(R.id.lv);
        TextView tv_see = inflate.findViewById(R.id.tv_see);
        ImageView iv_close = inflate.findViewById(R.id.iv_close);

        tv_hint.setText(hint);

        discountExpiredAdapter = new DiscountExpiredAdapter(context, discountExpiredInfo);
        lv.setAdapter(discountExpiredAdapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onDismiss();
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callBack.onItemClick(position);
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
            }
        });
        tv_see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onSee();
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
            }
        });
        ll_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
                WebUrlTypeUtil.getInstance(context).urlToApp(discountExpiredInfo.getUrl());
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                YmStatistics.getInstance().tongjiApp(discountExpiredInfo.getEvent_params());
                WebUrlTypeUtil.getInstance(context).urlToApp(discountExpiredInfo.getUrl());
            }
        });
        dialog.show();
    }

    //投票弹窗
    public static void showVoteDialog(final Context context, String hint, String leftButton, String rightButton, final CallBack2 callBack) {
        if (context == null)
            return;
        closeDialog();
        dialog = new Dialog(context, R.style.DiscountExpiredDialog);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View inflate = View.inflate(context, R.layout.dialog_vote, null);
        dialog.setContentView(inflate);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    closeDialog();
                    return true;
                }
                return false;
            }
        });
        LinearLayout ll_root = inflate.findViewById(R.id.ll_root);
        LinearLayout ll1 = inflate.findViewById(R.id.ll1);
        TextView tv_title = inflate.findViewById(R.id.tv_title);
        TextView tv_no = inflate.findViewById(R.id.tv_no);
        TextView tv_yes = inflate.findViewById(R.id.tv_yes);
        tv_title.setText(hint);
        tv_no.setText(leftButton);
        tv_yes.setText(rightButton);
        tv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onNoClick();
            }
        });
        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onYesClick();
            }
        });
        dialog.show();
    }

    //品类页dialog
    public static void showCategoryDialog(final Context context, CategoryBean categoryBean, final CallBack3 callBack) {
        if (context == null)
            return;
        closeDialog();
        dialog = new Dialog(context, R.style.DiscountExpiredDialog);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View inflate = View.inflate(context, R.layout.dialog_category, null);
        dialog.setContentView(inflate);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    closeDialog();
                    return true;
                }
                return false;
            }
        });
        LinearLayout ll_root = inflate.findViewById(R.id.ll_root);
        LinearLayout ll1 = inflate.findViewById(R.id.ll1);
        LinearLayout ll2 = inflate.findViewById(R.id.ll2);
        TextView tv_title = inflate.findViewById(R.id.tv_title);
        TextView tv_hint = inflate.findViewById(R.id.tv_hint);
        ListView lv = inflate.findViewById(R.id.lv);
        final TextView tv_see = inflate.findViewById(R.id.tv_see);
        ImageView iv_close = inflate.findViewById(R.id.iv_close);

        if (TextUtils.isEmpty(categoryBean.getAlert_type()) && categoryBean.getAlert_type().equals("1")) {
            tv_title.setText("品类推荐");
        } else {
            tv_title.setText("猜你喜欢");
        }


        final CategoryAdapter categoryAdapter = new CategoryAdapter(context, categoryBean.getTao_list().get(0), 0);
        lv.setAdapter(categoryAdapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onDismiss();
                dialog.dismiss();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callBack.onItemClick(position,categoryAdapter);
                dialog.dismiss();
            }
        });
        tv_see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onSee(categoryAdapter,tv_see);
//                dialog.dismiss();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public interface CallBack3 {
        void onItemClick(int pos,CategoryAdapter categoryAdapter);

        void onDismiss();

        void onSee(CategoryAdapter categoryAdapter,TextView tv_see);
    }

    public interface CallBack {
        void onItemClick(int pos);

        void onDismiss();

        void onSee();
    }

    public interface CallBack2 {
        void onYesClick();

        void onNoClick();

    }

    public static void closeDialog() {
        try {
            if (dialog != null) {
                dialog.cancel();
                dialog.dismiss();
            }
            if (discountExpiredAdapter != null) {
                discountExpiredAdapter.cancelAllTimers();
            }

        } catch (Exception e) {

        }
    }

}

