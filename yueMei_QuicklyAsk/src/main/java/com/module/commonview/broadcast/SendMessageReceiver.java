package com.module.commonview.broadcast;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RemoteViews;

import com.module.commonview.activity.ChatActivity;
import com.module.my.controller.other.ActivityCollector;
import com.quicklyask.activity.R;


/**
 * 后台私信获取消息发送状态栏
 * Created by Administrator on 2018/1/11.
 */

public class SendMessageReceiver extends BroadcastReceiver {
    public static final String ACTION="com.module.commonview.broadcast.SendMessageReceiver";
    public static final String TAG = "SendMessageReceiver";
    private NotificationManager m_notificationMgr;
    private String mChannelID;

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra("message");
        String uid = intent.getStringExtra("clientid");
        Log.d(TAG,"=========>"+message);
        Log.d(TAG,"=========>"+uid);
            m_notificationMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= 26){
                mChannelID = "1";
                String channelName = "channel_name";
                NotificationChannel channel = new NotificationChannel(mChannelID, channelName, NotificationManager.IMPORTANCE_HIGH);
                m_notificationMgr.createNotificationChannel(channel);
            }
            Notification.Builder builder = new Notification.Builder(context);     //创建通知栏对象
            builder.setContentTitle("悦美");                //设置标题
            builder.setContentText(message);
            builder.setSmallIcon(R.drawable.ic_launcher);       //设置图标
            builder.setTicker("新消息");
            builder.setDefaults(Notification.DEFAULT_ALL);  //设置默认的提示音，振动方式，灯光
            builder.setAutoCancel(true);                    //打开程序后图标消失
            builder.setWhen(System.currentTimeMillis());
            if (Build.VERSION.SDK_INT >= 26){
                //创建通知时指定channelID
                builder.setChannelId(mChannelID);
            }
            PendingIntent pendingIntent = PendingIntent.getActivities(context, 0, makeIntentStack(context,uid), PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            Notification notification1 = builder.build();
            m_notificationMgr.notify(5, notification1); // 通过通知管理器发送通知

    }

    Intent[] makeIntentStack(Context context,String uid) {
        Intent[] intents = new Intent[2];
        intents[0] = Intent.makeRestartActivityTask(new ComponentName(context, com.module.MainTableActivity.class));
        intents[1] = new Intent(context,  ChatActivity.class);
        intents[1].putExtra("directId", uid);
        intents[1].putExtra("objId", "666");
        intents[1].putExtra("objType", "0");
        intents[1].setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intents;
    }
}
