package com.module.commonview.broadcast;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;

import com.module.MyApplication;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.my.controller.activity.SignWebActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by 裴成浩 on 2018/5/15.
 */
public class SignInformReceiver extends BroadcastReceiver {

    private String TAG = "SignInformReceiver";
    private String[] signIns;
    private String mChannelID;
    private static final int PUNCH_CARD_ID = 1;
    private NotificationManager mM_notificationMgr1;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG,"onReceive=========");
        setSignIns();
        checkNotify();
    }

    /**
     * 设置签到提示文案
     */
    private void setSignIns() {
        Resources resources = MyApplication.getContext().getResources();
        signIns = new String[]{resources.getString(R.string.sign_prompt1), resources.getString(R.string.sign_prompt2) + newString(0x1f604) + "~",      //笑表情
                resources.getString(R.string.sign_prompt3), resources.getString(R.string.sign_prompt4), resources.getString(R.string.sign_prompt5) + newString(0x2600),            //太阳表情
                resources.getString(R.string.sign_prompt6), resources.getString(R.string.sign_prompt7) + newString(0x1F436) + "~",      //汪星人表情
                resources.getString(R.string.sign_prompt8) + newString(0x2600) + newString(0x2600) + newString(0x2600),   //太阳表情
                resources.getString(R.string.sign_prompt9), resources.getString(R.string.sign_prompt10) + newString(0x1f609),           //俏皮
                resources.getString(R.string.sign_prompt11) + newString(0x1f601),           //呲牙
        };
    }

    /**
     * 添加系统表情
     *
     * @param codePoint
     * @return
     */
    private String newString(int codePoint) {
        return new String(Character.toChars(codePoint));
    }

    public void checkNotify() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);    //获取年
        int month = cal.get(Calendar.MONTH) + 1;   //获取月份，0表示1月份
        int day = cal.get(Calendar.DAY_OF_MONTH);    //获取当前天数

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        cal.get(Calendar.DAY_OF_YEAR);
        cal.get(Calendar.DAY_OF_MONTH);

        String dates = year + "" + month + "" + day;
        //当前日期
        String currentDate = Cfg.loadStr(MyApplication.getContext(), FinalConstant.CURRENTDATE, "");
        //签到开关
        //是否用户提前手动签到
        String isSign = Cfg.loadStr(MyApplication.getContext(), FinalConstant.IS_SIGN, "0");
        SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
        String signFlag = sign.getString(FinalConstant.SIGN_FLAG, "0");
        Log.e(TAG, "dates == " + dates);
        Log.e(TAG, "currentDate == " + currentDate);
        Log.e(TAG, "signFlag == " + signFlag);
        Log.e(TAG, "hour == " + hour);
        Log.e(TAG, "isSign == " + isSign);

        if ((!dates.equals(currentDate) || "".equals(currentDate)) && "1".equals(signFlag)) {                  //如果本地保存的日期和当前的日期不相同,或者是没有保存日期且开启了推送
            if (hour == 11) {
                if ("0".equals(isSign)){
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.CURRENTDATE, dates);
                    Cfg.saveStr(MyApplication.getContext(), FinalConstant.IS_SIGN, "1");
                    String signIn2 = signIns[new Random().nextInt(10)];
                    notifyShow(signIn2);
                    Log.e(TAG, "notifyShow" + hour);
                }
            }
        }
    }

    public void notifyShow(String strMsg) {
        Log.e(TAG, "新消息！");

        mM_notificationMgr1 = (NotificationManager) MyApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            mChannelID = "1";
            String channelName = "channel_name";
            NotificationChannel channel = new NotificationChannel(mChannelID, channelName, NotificationManager.IMPORTANCE_HIGH);
            mM_notificationMgr1.createNotificationChannel(channel);
        }
        Notification.Builder builder = new Notification.Builder(MyApplication.getContext());     //创建通知栏对象
        builder.setContentTitle("悦美");                //设置标题
        builder.setContentText(strMsg);
        builder.setSmallIcon(R.drawable.ic_launcher);       //设置图标
        builder.setTicker("新消息");
        builder.setDefaults(Notification.DEFAULT_ALL);  //设置默认的提示音，振动方式，灯光
        builder.setAutoCancel(true);                    //打开程序后图标消失
        builder.setWhen(System.currentTimeMillis());
        if (Build.VERSION.SDK_INT >= 26) {
            //创建通知时指定channelID
            builder.setChannelId(mChannelID);
        }

        if (!isMainActivityTop()) {  //如果不是当前页面不是签到页面
            Intent intent = new Intent(MyApplication.getContext(), SignWebActivity.class);
            intent.putExtra("link", FinalConstant.SIGN_WEB);
            PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getContext(), 0, intent, 0);
            builder.setContentIntent(pendingIntent);
        }else {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApplication.getContext(), 0, new Intent(), 0);
            builder.setContentIntent(pendingIntent);
        }


        Notification notification = builder.build();
        mM_notificationMgr1.notify(PUNCH_CARD_ID, notification); // 通过通知管理器发送通知
    }


    private boolean isMainActivityTop(){
        ActivityManager manager = (ActivityManager) MyApplication.getContext().getSystemService(Context.ACTIVITY_SERVICE);
        String name = manager.getRunningTasks(1).get(0).topActivity.getClassName();
        return name.equals(SignWebActivity.class.getName());
    }
}
