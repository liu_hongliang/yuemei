package com.module.commonview.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

public class MessageNumChangeReceive extends BroadcastReceiver {

    public static final String ACTION ="com.module.commonview.broadcast.MessageNumChangeReceive";
    public static final String HOS_ID = "hos_id";

    private String mHosId;
    ReceiveCallBack mReceiveCallBack;


    public MessageNumChangeReceive(String hosId) {
        mHosId = hosId;

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String hosId = intent.getStringExtra(HOS_ID);
        if (!TextUtils.isEmpty(hosId)){
            if (hosId.equals(mHosId)){
                mReceiveCallBack.onReceiveCallBack();
            }
        }

    }

    public void setReceiveCallBack(ReceiveCallBack receiveCallBack) {
        mReceiveCallBack = receiveCallBack;
    }

    public interface ReceiveCallBack{
        void onReceiveCallBack();
    }


}
