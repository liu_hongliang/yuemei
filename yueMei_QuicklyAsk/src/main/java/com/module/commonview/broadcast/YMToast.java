package com.module.commonview.broadcast;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.MyApplication;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.Timer;

public class YMToast implements View.OnTouchListener{


    private final static int CLOSE = 0;

    private final static int ANIM_DURATION = 600;
    private final static int SHOW_DURATION = 5000;

    private final Context mContext;
    private View mHeaderToastView;
    private WindowManager wm;
    private LinearLayout linearLayout;
    private float downY;
    private TextView tvTitle;
    private TextView tvContent;
    private TextView tvEdit;
    private ImageView tvImage;

    public YMToast(Activity context){
        //使用applicationContext保证Activity跳转时Toast不会消失
        this.mContext = context.getApplicationContext();
    }

    public void show(String toast){
        showHeaderToast(toast);
    }

    private synchronized void showHeaderToast(String toast) {

        initHeaderToastView();


        setHeaderViewInAnim();

        //2s后自动关闭
        mHeaderToastHandler.sendEmptyMessageDelayed(CLOSE, SHOW_DURATION);
    }

    public void showWithCustomIcon(String title, String content, String hosimg, String clientid, String hosid, String pos){
        showHeaderToastWithCustomIcon(title, content,hosimg,clientid,hosid,pos);
    }

    private synchronized void showHeaderToastWithCustomIcon(String title, String content, String hosimg, final String clientid, final String hosid,final String pos) {
        initHeaderToastView();

        tvTitle.setText(title);
        tvContent.setText(content);
        Glide.with(mContext)
                .load(hosimg)
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.color._f6)
                .error(R.color._f6).
                into(tvImage);

        setHeaderViewInAnim();

        //2s后自动关闭
        mHeaderToastHandler.sendEmptyMessageDelayed(CLOSE, SHOW_DURATION);
    }

    /**
     * 为mHeaderToastView添加进入动画
     */
    private void setHeaderViewInAnim() {
        ObjectAnimator a = ObjectAnimator.ofFloat(mHeaderToastView, "translationY", -700, 0);
        a.setDuration(ANIM_DURATION);
        a.start();
    }



    private void initHeaderToastView() {
        wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

        //为mHeaderToastView添加parent使其能够展示动画效果
        linearLayout = new LinearLayout(mContext);
        LinearLayout.LayoutParams llParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(llParams);

        mHeaderToastView = View.inflate(mContext, R.layout.notifycation_message, null);
        //为mHeaderToastView添加滑动删除事件
        mHeaderToastView.setOnTouchListener(this);
        tvTitle = mHeaderToastView.findViewById(R.id.notify_title);
        tvContent = mHeaderToastView.findViewById(R.id.notify_content);
        tvEdit = mHeaderToastView.findViewById(R.id.notify_edt);
        tvImage = mHeaderToastView.findViewById(R.id.notify_img);

        WindowManager.LayoutParams wmParams = new WindowManager.LayoutParams();
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        int windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        wmParams.width = windowsWight-Utils.dip2px(20); //设置Toast宽度为屏幕宽度
        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        wmParams.gravity = Gravity.CENTER | Gravity.TOP;
        wmParams.x = 0;
        wmParams.y = 0;
        wmParams.format = PixelFormat.TRANSLUCENT;
        wmParams.type = WindowManager.LayoutParams.TYPE_TOAST;
        linearLayout.addView(mHeaderToastView);
        wm.addView(linearLayout, wmParams);
    }

    @SuppressLint("HandlerLeak")
    public Handler mHeaderToastHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case CLOSE:
                    animDismiss();
                    break;
                default:
                    Log.e("HeaderToast", "no selection matches");
                    break;
            }
        }
    };

    /**
     * HeaderToast消失动画
     */
    private void animDismiss() {
        if(null == linearLayout || null == linearLayout.getParent()){
            //如果linearLayout已经被从wm中移除，直接return
            return;
        }

        ObjectAnimator a = ObjectAnimator.ofFloat(mHeaderToastView, "translationY", 0, -700);
        a.setDuration(ANIM_DURATION);
        a.start();

        a.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                dismiss();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 移除HeaderToast
     */
    private void dismiss() {
        if(null != linearLayout && null != linearLayout.getParent()){
            wm.removeView(linearLayout);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                downY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                float currentY = event.getRawY();
                if((downY - currentY) >= 10){
                    animDismiss();
                }
                break;
        }
        return true;
    }
}
