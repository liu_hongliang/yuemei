package com.module.commonview.activity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.PopupWindow;

import com.quicklyask.activity.R;

public class SkuAlertPopwindow extends PopupWindow {
    private Context mContext;
    private WebView mWebView;
    private Button mButton;

    public SkuAlertPopwindow(Context context) {
        mContext=context;
        final View view = View.inflate(mContext, R.layout.sku_alertpopwindow, null);
        mWebView=view.findViewById(R.id.sku_alert_webview);
        mButton=view.findViewById(R.id.sku_alert_btn);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setContentView(view);
        update();
    }
}
