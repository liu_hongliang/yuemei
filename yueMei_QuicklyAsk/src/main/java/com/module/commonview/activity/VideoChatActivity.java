package com.module.commonview.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alivc.rtc.AliRtcAuthInfo;
import com.alivc.rtc.AliRtcEngine;
import com.alivc.rtc.AliRtcEngineEventListener;
import com.alivc.rtc.AliRtcEngineNotify;
import com.alivc.rtc.AliRtcRemoteUserInfo;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.bean.VideoChatGetTokenBean;
import com.module.commonview.view.SkuVideoChatDialog;
import com.module.commonview.view.TaoDetailPop;
import com.module.commonview.view.VideoChatComplainDialog;
import com.module.commonview.view.VideoChatComplainPop;
import com.module.community.model.api.DisconnectFaceVideoApi;
import com.module.community.model.bean.CreateFaceVideoBean;
import com.module.community.model.bean.FaceVdieoBean;
import com.module.community.model.bean.VideoChatDoctorData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;
import com.quicklyask.view.YueMeiDialog2;

import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.webrtc.ali.ThreadUtils;
import org.webrtc.alirtcInterface.ALI_RTC_INTERFACE;
import org.webrtc.alirtcInterface.AliParticipantInfo;
import org.webrtc.alirtcInterface.AliStatusInfo;
import org.webrtc.alirtcInterface.AliSubscriberInfo;
import org.webrtc.sdk.SophonSurfaceView;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.alivc.rtc.AliRtcEngine.AliRtcAudioTrack.AliRtcAudioTrackNo;
import static com.alivc.rtc.AliRtcEngine.AliRtcRenderMode.AliRtcRenderModeAuto;
import static com.alivc.rtc.AliRtcEngine.AliRtcVideoTrack.AliRtcVideoTrackCamera;
import static com.alivc.rtc.AliRtcEngine.AliRtcVideoTrack.AliRtcVideoTrackNo;
import static com.module.commonview.activity.AliRtcConstants.SOPHON_RESULT_SIGNAL_HEARTBEAT_TIMEOUT;
import static com.module.commonview.activity.AliRtcConstants.SOPHON_SERVER_ERROR_POLLING;

public class VideoChatActivity extends YMBaseActivity {
    public static final String TAG = VideoChatActivity.class.getSimpleName();
    @BindView(R.id.sf_local_view)
    SophonSurfaceView mLocalView;
    @BindView(R.id.change_camera)
    ImageView changeCamera;
    @BindView(R.id.complain_icon)
    ImageView complainIcon;
    @BindView(R.id.video_chat_background)
    ImageView videoChatBackground;
    @BindView(R.id.time)
    Chronometer time;
    @BindView(R.id.finish_chat)
    ImageView finishChat;
    @BindView(R.id.finish_chat_click)
    LinearLayout finishChatClick;
    @BindView(R.id.finish_camera_img)
    ImageView finishCameraImg;
    @BindView(R.id.finish_camera_txt)
    TextView finishCameraTxt;
    @BindView(R.id.finish_camera_click)
    LinearLayout finishCameraClick;
    @BindView(R.id.finish_voice_img)
    ImageView finishVoiceImg;
    @BindView(R.id.finish_voice_txt)
    TextView finishVoiceTxt;
    @BindView(R.id.finish_voice_click)
    LinearLayout finishVoiceClick;
    @BindView(R.id.chart_content_userlist_item_video_layout)
    public LinearLayout mVideoLayout;
    @BindView(R.id.chart_content_userlist_item_surface_container)
    public FrameLayout mSurfaceContainer;
    @BindView(R.id.user_icon)
    public ImageView mUserIcon;
    @BindView(R.id.sf_user_view)
    public SophonSurfaceView mSurfaceUser;
    @BindView(R.id.video_chat_conning)
    public LinearLayout mConningLayout;
    @BindView(R.id.video_chat_countdown)
    public TextView mCountDownTxt;
    @BindView(R.id.doctor_icon)
    ImageView doctorIcon;
    @BindView(R.id.doctor_kind_icon)
    ImageView doctorKindIcon;
    @BindView(R.id.doctor_name)
    TextView doctorName;
    @BindView(R.id.doctor_kind)
    TextView doctorKind;
    @BindView(R.id.hos_name)
    TextView hosName;
    @BindView(R.id.time_connect)
    TextView timeConnect;
    private boolean isCameraOpen;//摄像头是否打开
    private boolean isVoiceOpen;//静音是否打开
    private String mAppId;
    private String mChannelId;
    private String mToken;
    private String mNonce;
    private int mTimestamp;
    private String isConnect = "0";//是否接通 挂断接口要用
    /**
     * SDK提供的对音视频通话处理的引擎类
     */
    private AliRtcEngine mAliRtcEngine;
    /**
     * 前台服务的Intent
     */
    private Intent mForeServiceIntent;
    /**
     * 数据集
     */
    private Bundle mBundle;
    private String mUserId;
    private List<String> mGslb;
    public static final String VIDEO_CHATACTIVITY_VALUE = "need_value";
    private CreateFaceVideoBean mFaceVideoBean;
    private String mDoctorsName;
    private CountDownTimer mTimer;
    private VideoChatComplainPop mVideoChatComplainPop;

    @Override
    protected int getLayoutId() {
        return R.layout.video_chat_layout;
    }

    @Override
    protected void initView() {
        mVideoChatComplainPop = new VideoChatComplainPop(mContext);
    }


    @Override
    protected void initData() {
        mFaceVideoBean = getIntent().getParcelableExtra(VIDEO_CHATACTIVITY_VALUE);
        loadDoctorData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkPermission();

    }

    private static final int COUNT_DOWN_TXT = 1;
    private int countTime = 6;
    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case COUNT_DOWN_TXT:
                    mCountDownTxt.setVisibility(View.VISIBLE);
                    countTime--;
                    mCountDownTxt.setText(mDoctorsName + "医生已接通，" + countTime + "s后开始面诊");
                    if (countTime > 0) {
                        mHandler.sendEmptyMessageDelayed(COUNT_DOWN_TXT, 1000);
                    } else {
                        mAliRtcEngine.muteLocalMic(false);
                        mLocalView.setVisibility(View.VISIBLE);
                        mCountDownTxt.setVisibility(View.GONE);
                        finishCameraClick.setVisibility(View.VISIBLE);
                        finishVoiceClick.setVisibility(View.VISIBLE);
                        time.setVisibility(View.VISIBLE);
                        time.setBase(SystemClock.elapsedRealtime());
                        time.start();
                    }
                    break;
            }
            return false;
        }
    });

    /**
     * 判断是否有相机，音频，读写权限
     */
    private void checkPermission() {
        Acp.getInstance(this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).build(), new AcpListener() {
            @Override
            public void onGranted() {
                initRTCEngineAndStartPreview();
            }

            @Override
            public void onDenied(List<String> permissions) {

            }
        });
    }


    private void getToken() {
        if (null != mFaceVideoBean) {
            VideoChatGetTokenBean tokenData = mFaceVideoBean.getToken_data();
            if (null != tokenData) {
                mAppId = tokenData.getApp_id();
                mChannelId = tokenData.getChannel_id();
                mToken = tokenData.getToken();
                mNonce = tokenData.getNonce();
                mTimestamp = tokenData.getTimestamp();
                mUserId = tokenData.getUser_id();
                mGslb = tokenData.getGslb();
                joinChannel();
            }
        }
    }

    /**
     * 加载医生数据
     */
    private void loadDoctorData() {
        if (null != mFaceVideoBean) {
            VideoChatDoctorData doctorsData = mFaceVideoBean.getDoctors_data();
            if (null != doctorsData) {
                mConningLayout.setVisibility(View.VISIBLE);
                mDoctorsName = doctorsData.getShow_doctors_name();
                String doctorsJob = doctorsData.getShow_doctors_job();
                String hospitalName = doctorsData.getHospital_name();
                String doctorsAvatar = doctorsData.getShow_doctors_avatar();
                String videoBackdrop = doctorsData.getVideo_backdrop();
                String showDoctorsJobKey = doctorsData.getShow_doctors_job_key();
                setDoctorKindData(showDoctorsJobKey);
                doctorName.setText(mDoctorsName);
                doctorKind.setText(doctorsJob);
                hosName.setText(hospitalName);

                String userIcon = Cfg.loadStr(this, FinalConstant.UHEADIMG, "");
                Glide.with(this).load(userIcon).transform(new GlideCircleTransform(this)).into(mUserIcon);
                Glide.with(this).load(doctorsAvatar).transform(new GlideCircleTransform(this)).into(doctorIcon);
                Glide.with(this).load(videoBackdrop).into(videoChatBackground);
                countDown();
            }
        } else {
            mConningLayout.setVisibility(View.GONE);
        }
    }



    /**
     * 倒计时显示
     */
    private void countDown() {
        mTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeConnect.setText("预计 " + millisUntilFinished / 1000 + "s 内接通");
            }

            @Override
            public void onFinish() {
               finish();

            }
        }.start();

    }

    /**
     * 设置医生职称数据
     * @param showDoctorsJobKey
     */
    private void setDoctorKindData(String showDoctorsJobKey) {
        if (!TextUtils.isEmpty(showDoctorsJobKey)){
            doctorKindIcon.setVisibility(View.VISIBLE);
            switch (showDoctorsJobKey){
                case "0"://不显示
                    doctorKindIcon.setVisibility(View.GONE);
                    break;
                case "1": //住院医师
                    doctorKindIcon.setBackgroundResource(R.drawable.video_chat_doctor_kind4);
                    break;
                case "2": //主治医师
                    doctorKindIcon.setBackgroundResource(R.drawable.video_chat_doctor_kind3);
                    break;
                case "3": //副主任医师
                    doctorKindIcon.setBackgroundResource(R.drawable.video_chat_doctor_kind2);
                    break;
                case "4": //主任医师
                    doctorKindIcon.setBackgroundResource(R.drawable.video_chat_doctor_kind1);
                    break;
            }
        }else {
            doctorKindIcon.setVisibility(View.GONE);
        }
    }

    /**
     * 挂断视频面诊更新状态
     */
    private void disconnectFaceVide() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, Object> params = new HashMap<>();
                params.put("channel_id",mChannelId);
                params.put("is_connect",isConnect);
                new DisconnectFaceVideoApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        Log.e(TAG,"DisconnectFaceVideoApi");
                        if ("1".equals(isConnect)){
                            showVideoScoreDialog();//评分弹窗
                        }
                    }
                });
            }
        });

    }


    /**
     * 挂断弹窗
     */
    private void showDisconnectDialog(final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                time.stop();
                final YueMeiDialog2 yueMeiDialog2 = new YueMeiDialog2(VideoChatActivity.this, message, "确定");
                yueMeiDialog2.setCanceledOnTouchOutside(false);
                yueMeiDialog2.show();
                yueMeiDialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                    @Override
                    public void BtnClick() {
                        yueMeiDialog2.dismiss();
                        if ("1".equals(isConnect)){
                            disconnectFaceVide();
                        }else {
                            if (!isFinishing()){
                                finish();
                                Log.e(TAG, "finish ====YueMeiDialog2");
                            }
                        }
                    }
                });

            }
        });

    }


    /**
     * 评分弹窗
     */
    private void showVideoScoreDialog() {
        if (!isFinishing()){
            FaceVdieoBean faceVdieoBean = new FaceVdieoBean();
            faceVdieoBean.setChannel_id(mChannelId);
            SkuVideoChatDialog skuVideoChatDialog = new SkuVideoChatDialog(mContext, FinalConstant.VIDEOSCORE, 0.6f, faceVdieoBean);
            skuVideoChatDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (!isFinishing()){
                        finish();
                        Log.e(TAG,"finish ====ScoreDialog");
                    }
                }
            });
            skuVideoChatDialog.show();
        }
    }


    private void initRTCEngineAndStartPreview() {
        // 防止初始化过多
        if (mAliRtcEngine == null) {
            AliRtcEngine.setH5CompatibleMode(1);//设置H5兼容模式
            //实例化,必须在主线程进行。
            mAliRtcEngine = AliRtcEngine.getInstance(getApplicationContext());
            //设置事件的回调监听
            mAliRtcEngine.setRtcEngineEventListener(mEventListener);
            //设置接受通知事件的回调
            mAliRtcEngine.setRtcEngineNotify(mEngineNotify);

            mAliRtcEngine.enableSpeakerphone(true);

            mAliRtcEngine.muteLocalMic(true);

            // 初始化本地视图
            initLocalView();

            getToken();
        }
    }


    /**
     * 设置本地的预览视图的view
     */
    private void initLocalView() {
        mLocalView.getHolder().setFormat(PixelFormat.TRANSPARENT);
        mLocalView.setZOrderOnTop(false);
        mLocalView.setZOrderMediaOverlay(false);
        if (mAliRtcEngine != null) {
            mAliRtcEngine.setLocalViewConfig(createVideoCanvas(), AliRtcVideoTrackCamera);
        }
    }


    /**
     * 创建播放视频的画布
     *
     * @return
     */
    @NotNull
    private AliRtcEngine.AliVideoCanvas createVideoCanvas() {
        AliRtcEngine.AliVideoCanvas aliVideoCanvas = new AliRtcEngine.AliVideoCanvas();
        mSurfaceUser.setZOrderOnTop(true);
        mSurfaceUser.setZOrderMediaOverlay(true);
        aliVideoCanvas.view = mSurfaceUser;
        aliVideoCanvas.renderMode = AliRtcRenderModeAuto;
        return aliVideoCanvas;
    }


    /**
     * 播放自己的画面
     */
    private void playUserVideo() {
        mVideoLayout.setVisibility(View.VISIBLE);
        mAliRtcEngine.startPreview();
    }


    @OnClick({R.id.change_camera,R.id.complain_icon, R.id.finish_chat_click, R.id.finish_camera_click, R.id.finish_voice_click})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_camera://翻转相机
                if (mAliRtcEngine.isCameraOn()) {
                    mAliRtcEngine.switchCamera();
                }
                break;
            case R.id.complain_icon://投诉
                showComplainPop();
                break;
            case R.id.finish_chat_click://挂断
                if ("0".equals(isConnect)){ //没接通
                    finish();
                    Log.e(TAG,"finish_chat_click");
                }else {
                    showFinishDialog();
                }
                break;
            case R.id.finish_camera_click://关闭/开启摄像头
                changeCamera();
                break;
            case R.id.finish_voice_click://开启/关闭静音
                changeVoice();
                break;
        }
    }


    /**
     * 投诉点开pop
     */
    private void showComplainPop() {
        PopupWindowCompat.showAsDropDown(mVideoChatComplainPop,complainIcon,-Utils.dip2px(10),0, Gravity.END);
        mVideoChatComplainPop.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoChatComplainPop.dismiss();
                showComplainDialog();
            }
        });
    }


    /**
     * 提交投诉弹窗
     */
    private void showComplainDialog() {
        final VideoChatComplainDialog videoChatComplainDialog = new VideoChatComplainDialog(VideoChatActivity.this);
        videoChatComplainDialog.show();
        videoChatComplainDialog.mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoChatComplainDialog.dismiss();
            }
        });
        videoChatComplainDialog.mSurelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoChatComplainDialog.dismiss();
                doDestroy();//挂断
                finish();
                Intent intent = new Intent(VideoChatActivity.this, VideoChatComplainActivity.class);
                intent.putExtra("channel_id",mChannelId);
                startActivity(intent);
            }
        });
    }


    /**
     * 结束的弹窗
     */
    private void showFinishDialog() {
        final YueMeiDialog yueMeiDialog = new YueMeiDialog(VideoChatActivity.this, "结束本次视频面诊？", "我要结束", "再聊一聊");
        yueMeiDialog.setCanceledOnTouchOutside(false);
        yueMeiDialog.show();
        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {//结束
                yueMeiDialog.dismiss();
                mVideoLayout.setVisibility(View.GONE);
                mLocalView.setVisibility(View.GONE);
                time.stop();
                doDestroy();
            }

            @Override
            public void rightBtnClick() {
                yueMeiDialog.dismiss();
            }
        });
    }


    /**
     * 加入频道
     */
    private void joinChannel() {
        if (mAliRtcEngine == null) {
            return;
        }
        AliRtcAuthInfo userInfo = new AliRtcAuthInfo();
        userInfo.setAppid(mAppId);
        userInfo.setNonce(mNonce);
        userInfo.setTimestamp(mTimestamp);
        userInfo.setUserId(mUserId);
        if (CollectionUtils.isNotEmpty(mGslb)) {
            String[] arr = new String[mGslb.size()];
            for (int i = 0; i < mGslb.size(); i++) {
                arr[i] = mGslb.get(i);
            }
            userInfo.setGslb(arr);
        }
        userInfo.setToken(mToken);
        userInfo.setConferenceId(mChannelId);
        /*
         *设置自动发布和订阅，只能在joinChannel之前设置
         *参数1    true表示自动发布；false表示手动发布
         *参数2    true表示自动订阅；false表示手动订阅
         */
        mAliRtcEngine.setAutoPublish(true, true);
        // 加入频道
        mAliRtcEngine.joinChannel(userInfo, Utils.getUid());

    }

    /**
     * 声音开关
     */
    private void changeVoice() {
        if (!isVoiceOpen) {
            finishVoiceImg.setBackgroundResource(R.drawable.video_chat_voice_select);
            finishVoiceTxt.setText("关闭静音");
            isVoiceOpen = true;
            mAliRtcEngine.muteLocalMic(true);
        } else {
            finishVoiceImg.setBackgroundResource(R.drawable.video_chat_voice_unselect);
            finishVoiceTxt.setText("开启静音");
            isVoiceOpen = false;
            mAliRtcEngine.muteLocalMic(false);
        }
    }


    /**
     * 摄像头开关
     */
    private void changeCamera() {
        if (!isCameraOpen) {
            finishCameraImg.setBackgroundResource(R.drawable.video_chat_finish_carmra_select);
            finishCameraTxt.setText("开启摄像头");
            isCameraOpen = true;
            mAliRtcEngine.muteLocalCamera(true, AliRtcVideoTrackCamera);
            mSurfaceUser.setVisibility(View.GONE);

        } else {
            finishCameraImg.setBackgroundResource(R.drawable.video_chat_finish_carmra_unselect);
            finishCameraTxt.setText("关闭摄像头");
            isCameraOpen = false;
            mAliRtcEngine.muteLocalCamera(false, AliRtcVideoTrackCamera);
            mSurfaceUser.setVisibility(View.VISIBLE);
        }
    }


    /**
     * 更新远程展示的视图
     * @param uid
     * @param at
     * @param vt
     */
    private void updateRemoteDisplay(final String uid, AliRtcEngine.AliRtcAudioTrack at, final AliRtcEngine.AliRtcVideoTrack vt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null == mAliRtcEngine) {
                    return;
                }
                AliRtcRemoteUserInfo remoteUserInfo = mAliRtcEngine.getUserInfo(uid);
                // 如果没有，说明已经退出了或者不存在。则不需要添加，并且删除
                if (remoteUserInfo == null) {
                    // remote user exit room
                    Log.e(TAG, "updateRemoteDisplay remoteUserInfo = null, uid = " + uid);
                    return;
                }
                //change
                AliRtcEngine.AliVideoCanvas cameraCanvas = remoteUserInfo.getCameraCanvas();
                //视频情况
                if (vt == AliRtcVideoTrackNo) {
                    //没有视频流
                    cameraCanvas = null;
                    Log.e(TAG, "没有视频流");
                } else if (vt == AliRtcVideoTrackCamera) {
                    //相机流
                    Log.e(TAG, "相机流");
                    //SDK内部提供进行播放的view
                    AliRtcEngine.AliVideoCanvas aliVideoCanvas = new AliRtcEngine.AliVideoCanvas();
                    aliVideoCanvas.view = mLocalView;
                    aliVideoCanvas.renderMode = AliRtcRenderModeAuto;
                    mAliRtcEngine.setRemoteViewConfig(aliVideoCanvas, uid, AliRtcVideoTrackCamera);
                } else {
                    return;
                }

            }
        });
    }


    /**
     * 特殊错误码回调的处理方法
     *
     * @param error 错误码
     */
    private void processOccurError(int error) {
        switch (error) {
            case SOPHON_SERVER_ERROR_POLLING:
            case SOPHON_RESULT_SIGNAL_HEARTBEAT_TIMEOUT:
                noSessionExit(error);
                break;
            default:
                break;
        }
    }

    /**
     * 错误处理
     *
     * @param error 错误码
     */
    private void noSessionExit(final int error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(VideoChatActivity.this)
                        .setTitle("ErrorCode : " + error)
                        .setMessage("网络超时，请退出房间")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                onBackPressed();
                            }
                        }).create()
                        .show();
            }
        });
    }


    /**
     * 对方挂断
     */
    private void removeRemoteUser() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLocalView.setVisibility(View.GONE);
                mAliRtcEngine.stopPreview();
                showDisconnectDialog("对方已挂断，视频面诊结束。");

            }
        });
    }


    /**
     * 用户操作回调监听(回调接口都在子线程)
     */
    private AliRtcEngineEventListener mEventListener = new AliRtcEngineEventListener() {

        /**
         * 加入房间的回调
         * @param i 结果码
         */
        @Override
        public void onJoinChannelResult(final int i) {

        }

        /**
         * 离开房间的回调
         * @param i 结果码
         */
        @Override
        public void onLeaveChannelResult(int i) {

        }

        /**
         * 推流的回调
         * @param i 结果码
         * @param s publishId
         */
        @Override
        public void onPublishResult(int i, String s) {

        }

        /**
         * 取消发布本地流回调
         * @param i 结果码
         */
        @Override
        public void onUnpublishResult(int i) {

        }

        /**
         * 订阅成功的回调
         * @param s userid
         * @param i 结果码
         * @param aliRtcVideoTrack 视频的track
         * @param aliRtcAudioTrack 音频的track
         */
        @Override
        public void onSubscribeResult(String s, int i, AliRtcEngine.AliRtcVideoTrack aliRtcVideoTrack,
                                      AliRtcEngine.AliRtcAudioTrack aliRtcAudioTrack) {

            Log.e(TAG, "onSubscribeResult uid : " + s + " , result : " + i+"__"+aliRtcVideoTrack+"__"+aliRtcAudioTrack);
            if (i == 0) {
                Log.e(TAG, "订阅成功的回调 ==" + s);
                subscribeSussess();
                updateRemoteDisplay(s, aliRtcAudioTrack, aliRtcVideoTrack);
            }
        }

        /**
         * 取消的回调
         * @param i 结果码
         * @param s userid
         */
        @Override
        public void onUnsubscribeResult(int i, String s) {
            updateRemoteDisplay(s, AliRtcAudioTrackNo, AliRtcVideoTrackNo);
        }

        /**
         * 网络状态变化的回调
         * @param aliRtcNetworkQuality
         */
        @Override
        public void onNetworkQualityChanged(String s, AliRtcEngine.AliRtcNetworkQuality aliRtcNetworkQuality, AliRtcEngine.AliRtcNetworkQuality aliRtcNetworkQuality1) {
            Log.e(TAG,"网络状态变化的回调");
            updateRemoteDisplay(s, AliRtcAudioTrackNo, AliRtcVideoTrackNo);
        }

        /**
         * 出现警告的回调
         * @param i
         */
        @Override
        public void onOccurWarning(int i) {

        }

        /**
         * 出现错误的回调
         * @param error 错误码
         */
        @Override
        public void onOccurError(int error) {
            //错误处理
            processOccurError(error);
        }

        /**
         * 当前设备性能不足
         */
        @Override
        public void onPerformanceLow() {

        }

        /**
         * 当前设备性能恢复
         */
        @Override
        public void onPermormanceRecovery() {

        }

        /**
         * 连接丢失
         */
        @Override
        public void onConnectionLost() {
            Log.e(TAG,"连接丢失");
        }

        /**
         * 尝试恢复连接
         */
        @Override
        public void onTryToReconnect() {
            Log.e(TAG,"尝试恢复连接");
        }

        /**
         * 连接已恢复
         */
        @Override
        public void onConnectionRecovery() {
            Log.e(TAG,"连接已恢复");
        }

        /**
         * @param aliRTCSDK_client_role
         * @param aliRTCSDK_client_role1
         * 用户角色更新
         */

        public void onUpdateRoleNotify(ALI_RTC_INTERFACE.AliRTCSDK_Client_Role aliRTCSDK_client_role, ALI_RTC_INTERFACE.AliRTCSDK_Client_Role aliRTCSDK_client_role1) {

        }
    };


    /**
     * 订阅成功改本地样式
     */
    private void subscribeSussess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                isConnect = "1";
                mConningLayout.setVisibility(View.GONE);//
                if (null != mTimer){
                    mTimer.cancel();
                }
                mHandler.sendEmptyMessage(COUNT_DOWN_TXT);
                playUserVideo();

            }
        });

    }


    /**
     * SDK事件通知(回调接口都在子线程)
     */
    private AliRtcEngineNotify mEngineNotify = new AliRtcEngineNotify() {
        /**
         * 远端用户停止发布通知，处于OB（observer）状态
         * @param aliRtcEngine 核心引擎对象
         * @param s userid
         */
        @Override
        public void onRemoteUserUnPublish(AliRtcEngine aliRtcEngine, String s) {
            Log.e(TAG, "远端用户停止发布通知 ==" + s);
            updateRemoteDisplay(s, AliRtcAudioTrackNo, AliRtcVideoTrackNo);
        }

        /**
         * 远端用户上线通知
         * @param s userid
         */
        @Override
        public void onRemoteUserOnLineNotify(String s) {
            Log.e(TAG, "远端用户上线通知 ==" + s);
//            addRemoteUser(s);
        }

        /**
         * 远端用户下线通知
         * @param s userid
         */
        @Override
        public void onRemoteUserOffLineNotify(String s) {
            Log.e(TAG, "远端用户下线通知");
            removeRemoteUser();
        }

        /**
         * 远端用户发布音视频流变化通知
         * @param s userid
         * @param aliRtcAudioTrack 音频流
         * @param aliRtcVideoTrack 相机流
         */
        @Override
        public void onRemoteTrackAvailableNotify(String s, AliRtcEngine.AliRtcAudioTrack aliRtcAudioTrack,
                                                 AliRtcEngine.AliRtcVideoTrack aliRtcVideoTrack) {
        }

        /**
         * 订阅流回调，可以做UI及数据的更新
         * @param s userid
         * @param aliRtcAudioTrack 音频流
         * @param aliRtcVideoTrack 相机流
         */
        @Override
        public void onSubscribeChangedNotify(String s, AliRtcEngine.AliRtcAudioTrack aliRtcAudioTrack,
                                             AliRtcEngine.AliRtcVideoTrack aliRtcVideoTrack) {

        }

        /**
         * 订阅信息
         * @param aliSubscriberInfos 订阅自己这边流的user信息
         * @param i 当前订阅人数
         */
        @Override
        public void onParticipantSubscribeNotify(AliSubscriberInfo[] aliSubscriberInfos, int i) {

        }

        /**
         * 首帧的接收回调
         * @param s callId
         * @param s1 stream_label
         * @param s2 track_label 分为video和audio
         * @param i 时间
         */
        @Override
        public void onFirstFramereceived(String s, String s1, String s2, int i) {

            ThreadUtils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG,"首帧接受成功");
                }
            });


        }

        /**
         * 首包的发送回调
         * @param s callId
         * @param s1 stream_label
         * @param s2 track_label 分为video和audio
         * @param i 时间
         */
        @Override
        public void onFirstPacketSent(String s, String s1, String s2, int i) {
            ThreadUtils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG,"首包发送成功");
                }
            });
        }

        /**
         *首包数据接收成功
         * @param callId 远端用户callId
         * @param streamLabel 远端用户的流标识
         * @param trackLabel 远端用户的媒体标识
         * @param timeCost 耗时
         */
        @Override
        public void onFirstPacketReceived(String callId, String streamLabel, String trackLabel, int timeCost) {

        }

        /**
         * 取消订阅信息回调
         * @param aliParticipantInfos 订阅自己这边流的user信息
         * @param i 当前订阅人数
         */
        @Override
        public void onParticipantUnsubscribeNotify(AliParticipantInfo[] aliParticipantInfos, int i) {

        }

        /**
         * 被服务器踢出或者频道关闭时回调
         * @param i
         */
        @Override
        public void onBye(int i) {

            disconnectFaceVide();
            showDisconnectDialog(getMessage(i));

        }

        @Override
        public void onParticipantStatusNotify(AliStatusInfo[] aliStatusInfos, int i) {

        }

        /**
         * @param aliRtcStats
         * 实时数据回调(2s触发一次)
         */
        public void onAliRtcStats(ALI_RTC_INTERFACE.AliRtcStats aliRtcStats) {

        }
    };

    @NonNull
    private String getMessage(int i) {
        String message = "";
        if (i ==1){
            message = "视频面诊未接通，请稍后再试。";
        }else if (i == 2){
            message = "您的通信涉嫌违反悦美视频面诊相关服务协议，已被强制结束。";
        }
        return message;
    }


    //定义一个标志表示资源是否释放
    private boolean mIsDestroyed = false;

    private void doDestroy() {
        if (mIsDestroyed) {
            return;
        }
        Log.e(TAG,"doDestroy ====");
        // 释放静态资源
        if (mAliRtcEngine != null) {
            mAliRtcEngine.destroy();
        }
        disconnectFaceVide();
        time.stop();
        mIsDestroyed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            doDestroy();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        doDestroy();
    }


}
