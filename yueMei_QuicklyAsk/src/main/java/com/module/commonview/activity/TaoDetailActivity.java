
package com.module.commonview.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.module.MainTableActivity;
import com.module.MyApplication;
import com.module.api.TaoDetailShow;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.broadcast.MessageNumChangeReceive;
import com.module.commonview.broadcast.SendMessageReceiver;
import com.module.commonview.fragment.CommentFragment;
import com.module.commonview.fragment.TaoDeatailFragment;
import com.module.commonview.module.api.AutoSendApi;
import com.module.commonview.module.api.AutoSendApi2;
import com.module.commonview.module.api.BaikeFourApi;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.commonview.module.api.ChatSendMessageApi;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.api.IsCollectApi;
import com.module.commonview.module.api.SkuAddcarApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.module.api.TaoDataApi;
import com.module.commonview.module.api.TongjiClickApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.SharePictorial;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.view.ButtomDialogView;
import com.module.commonview.view.SkuBottomChatPop;
import com.module.commonview.view.SkuCouposPopwindow;
import com.module.commonview.view.SkuFenqiPop;
import com.module.commonview.view.SkuOrdingPopwindow;
import com.module.commonview.view.SkuVideoChatDialog;
import com.module.commonview.view.TaoDetailPop;
import com.module.commonview.view.share.MyShareBoardlistener;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.bean.SearchEntry;
import com.module.my.controller.activity.LoginActivity605;
import com.module.other.activity.ProjectContrastActivity;
import com.module.other.activity.SearchProjectActivity;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.MakeSureOrderActivity;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.module.shopping.model.api.CartSkuNumberApi;
import com.module.shopping.model.bean.CartSkuNumberData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.AlertCouponsBean;
import com.quicklyask.entity.ClickData;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.entity.TaoQuickReply;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CountDownView;
import com.quicklyask.view.DacuCountDownView;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.ui.ViewInject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.PreLoginListener;

import static com.quicklyask.view.NewuserConponsPop.jumpUrl;

public class TaoDetailActivity extends YMBaseActivity {
    public static final String TAG = "TaoDetailActivity";
    @BindView(R.id.sku_act_container)
    RelativeLayout mSkuActContainer;
    @BindView(R.id.sku_container)
    FrameLayout mSkuContainer;
    @BindView(R.id.tao_det_bar_background)
    View mTopBackground;
    @BindView(R.id.dacu_backgroud)
    ImageView dacuBackground ;
    @BindView(R.id.tao_title_bar_title)
    LinearLayout mTopTitle;
    @BindView(R.id.sku_search_click)
    LinearLayout mTaoDetailSearchClick;
    @BindView(R.id.sku_search_name)
    TextView mTaoDetailSearchName;
    @BindView(R.id.tao_det_top_img)
    ImageView taoTopBackImg;
    @BindView(R.id.tao_det_top_title)
    TabLayout mTaoDetailTablayout;
    @BindView(R.id.tao_det_web_back1)
    RelativeLayout mTaoDetWebBack1;
    @BindView(R.id.taotetal_shopcar_img)
    ImageView mTaotetalShopcarImg;
    @BindView(R.id.taotetal_cart_num)
    TextView mTaotetalCartNum;
    @BindView(R.id.taotetal_shopcar_click)
    RelativeLayout mTaotetalShopcarClick;
    @BindView(R.id.taotetal_collect_click)
    RelativeLayout mTaotetalClooectClick;
    @BindView(R.id.taotetal_collect_img)
    ImageView mTaotetalClooectImg;
    @BindView(R.id.tao_det_top_share_img)
    ImageView mTaoDetTopMoreImg;
    //    @BindView(R.id.sku_tip_huabao2)
//    View mSkuTipHuabao2;
    @BindView(R.id.tao_web_share_rly1)
    RelativeLayout mTaoWebShareRly1;
    @BindView(R.id.top_rly1)
    RelativeLayout mTopRly1;

    @BindView(R.id.sku_common_bottom)
    FrameLayout mSkuCommonBottom; //普通吸底
    @BindView(R.id.sku_bottom_common_chat)
    FrameLayout mSkuCommonBottomChat;//咨询
    @BindView(R.id.sku_bottom_common_hos)
    LinearLayout mSkuCommonBottomHos;//医院
    @BindView(R.id.sku_bottom_common_pk)
    LinearLayout mSkuCommonBottomPK;//项目对比
    @BindView(R.id.sku_bottom_common_video_chat)
    LinearLayout mSkuCommonBottomVideoChat;//视频面诊
    @BindView(R.id.sku_bottom_common_yuyue)
    TextView mSkuCommonBottomYunyue;
    @BindView(R.id.sku_bottom_common_daoyuan)
    TextView mSkuCommonBottomDaoyuan;
    @BindView(R.id.sku_bottom_common_detail)
    LinearLayout mSkuCommonBottomDetail;
    @BindView(R.id.sku_bottom_common_btn_visongone)
    LinearLayout mSkuCommonBottomBtnVisorgone;
    @BindView(R.id.sku_bottom_common_addcar)
    Button mSkuCommonBottomAddcar;
    @BindView(R.id.sku_bottom_common_buy)
    Button mSkuCommonBottomBuy;
    @BindView(R.id.sku_bottom_common_sellout_visongone)
    LinearLayout mSkuCommonBottomSelloutVisorgone;
    @BindView(R.id.sku_bottom_common_sellout)
    Button mSkuCommonBottomSellout;
    @BindView(R.id.iv_pk_common)//pk图标
    ImageView iv_pk_common;


    @BindView(R.id.sku_group_single_price1) //拼团吸底
            TextView mSkuGroupSinglePrice1;
    @BindView(R.id.sku_group_single_price)
    TextView mSkuGroupSinglePrice;
    @BindView(R.id.sku_group_single_title)
    TextView mSkuGroupSingleTitle;
    @BindView(R.id.sku_group_single)
    LinearLayout mSkuGroupSingle;
    @BindView(R.id.sku_group_spelt_price1)
    TextView mSkuGroupSpeltPrice1;
    @BindView(R.id.sku_group_spelt_price)
    TextView mSkuGroupSpeltPrice;
    @BindView(R.id.sku_group_spelt_title)
    TextView mSkuGroupSpeltTitle;
    @BindView(R.id.sku_group_spelt)
    LinearLayout mSkuGroupSpelt;
    @BindView(R.id.sku_group_common_chat)
    FrameLayout mSkuGroupChat;
    @BindView(R.id.sku_bottom_group_hos)
    LinearLayout mSkuGroupHos;
    @BindView(R.id.sku_bottom_group_video_chat)
    LinearLayout mSkuGroupVideoChat;
    @BindView(R.id.sku_bottom_group_pk)
    LinearLayout mSkuGroupPK;
    @BindView(R.id.sku_group_bottom)
    FrameLayout mSkuGroupBottom;
    @BindView(R.id.sku_bottom_group_sellout_visongone)
    LinearLayout mSkuGroupSelloutVisorgone;
    @BindView(R.id.sku_bottom_group_sellout)
    Button mSkuGroupSellout;
    @BindView(R.id.sku_bottom_group_btn_visongone)
    LinearLayout mSkuGroupBtnVisorgone;

    //拼团引导
    @BindView(R.id.iv_pk_group)
    ImageView iv_pk_group;
    @BindView(R.id.sku_tips)
    RelativeLayout skuTips;
    @BindView(R.id.sku_tips_skip)
    View mSkuTipsSkip;
    @BindView(R.id.sku_tips_know)
    View mSkuTipsKnow;
    //    @BindView(R.id.sku_bottom_hos_img)
//    ImageView mSkuBottomHosImg;
//    @BindView(R.id.sku_bottom_hos_title)
//    TextView mSkuBottomHosTitle;
//    @BindView(R.id.sku_bottom_hos_content)
//    TextView mSkuBottomHosContent;
//    @BindView(R.id.sku_bottom_visorgone)
//    FrameLayout mSkuBottomVisorgone;
    //弹层提示
    @BindView(R.id.sku_top_visorgone)
    FrameLayout mSkuTopVisorgone;
    @BindView(R.id.sku_top_text)
    TextView mSkuTopTime;
    @BindView(R.id.sku_top_close)
    LinearLayout mSkuTopClose;
    @BindView(R.id.ll_popup_win1)
    LinearLayout llPopupWin;
    @BindView(R.id.add_shopcar_img)
    ImageView mAddShopcarImg;

    @BindView(R.id.iv_popup_win)
    ImageView ivPopupWin;

    @BindView(R.id.tao_bottom_expandview)
    LinearLayout mBottomExpandView;
    @BindView(R.id.tao_bottom_expandtxt)
    TextView mBottomExpandTxt;
    @BindView(R.id.tao_bottom_expandimg)
    ImageView mBottomExpandImg;
    @BindView(R.id.common_message_iv)
    ImageView mCommonMessageIv;
    @BindView(R.id.common_message_num)
    TextView mCommonMessageNumTv;

    @BindView(R.id.group_message_iv)
    ImageView mGroupMessageIv;
    @BindView(R.id.group_message_num)
    TextView mGroupMessageNumTv;

    @BindView(R.id.tv_popup_win)
    TextView tvPopupWin;
    private TaoDetailActivity mContext;
    private TaoDeatailFragment mTaoDeatailFragment;
    private String mStatus;
    private String mIs_group;
    public String taoid = "";// 淘整形id
    public String source = "0";
    public String objid = "0";
    private String mU;
    private String isAutoSend = "";
    private MyShareBoardlistener myShareBoardlistener;
    private ButtomDialogView buttomDialogView2;
    private String SKU_ILLUSTRATED_PROMPT = "illustrated_prompt";
    private final int LOGIN_REFRESH = 999;
    private SharePictorial mPictorial;
    private String uid = "0";
    private String shareUrl = "";
    private String shareContent = "";
    private String shareImgUrl;
    private boolean ifcollect = false;
    private TaoDetailPop mDetailPop;
    private int mySixinNum;
    private boolean commentClick = false;//评论是否点击
    private TaoDetailBean mTaoDetailBean;
    private PageJumpManager pageJumpManager;
    private CountDownView mCountDownView;
    private DacuCountDownView mDacuCountDownView;
    private String phone400;// 400电话
    private String mPhoneFen;
    private TaoDetailBean.HosDocBean mHos_doc;
    public Animation mScalOutAnimation;
    public Animation mScalInAnimation1;
    private Animation mScalInAnimation2;
    private SkuFenqiPop baoxianPop;
    private List<TaoDetailBean.AlertBean> alert;
    private int popupWinInt = 0;
    private int time = 30;
    boolean urserLoginStatus = false;
    private CommentFragment mCommentFragment;
    private boolean mIsExpend; //底部快捷私信是否展开
    private Handler mHandler = new MyHandler(this);
    private SkuBottomChatPop mSkuBottomChatPop;
    private String mSendContent;
    private String mSendQuickReply;
    boolean isFirstSend = true;
    private int mExpandViewWidth;
    private MessageNumChangeReceive mNumChangeReceive;

    private int mMessageNum;

    private static class MyHandler extends Handler {
        private final WeakReference<TaoDetailActivity> mActivity;

        public MyHandler(TaoDetailActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final TaoDetailActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case 0:
//                        ObjectAnimator animator = ObjectAnimator.ofFloat(theActivity.mSkuBottomVisorgone, "alpha", 1f, 0f);
//                        // 表示的是:
//                        // 动画作用对象是mButton
//                        // 动画作用的对象的属性是透明度alpha
//                        // 动画效果是:常规 - 全透明 - 常规
//                        animator.setDuration(800);
//                        animator.start();
//                        animator.addListener(new Animator.AnimatorListener() {
//                            @Override
//                            public void onAnimationStart(Animator animation) {
//
//                            }
//
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                theActivity.mSkuBottomVisorgone.setVisibility(View.GONE);
//                            }
//
//                            @Override
//                            public void onAnimationCancel(Animator animation) {
//
//                            }
//
//                            @Override
//                            public void onAnimationRepeat(Animator animation) {
//
//                            }
//                        });
                        break;
                    case 1:
                        theActivity.tvPopupWin.setText(theActivity.alert.get(theActivity.popupWinInt).getTitle());
                        Log.e(TAG, "alert.get(popupWinInt).getImg() === " + theActivity.alert.get(theActivity.popupWinInt).getImg());
                        Glide.with(MyApplication.getContext()).load(theActivity.alert.get(theActivity.popupWinInt).getImg()).transform(new GlideCircleTransform(theActivity.mContext)).placeholder(R.drawable.radius_gray80).error(R.drawable.radius_gray80).into(theActivity.ivPopupWin);
                        theActivity.llPopupWin.setVisibility(View.VISIBLE);

                        AlphaAnimation animation1 = new AlphaAnimation(0.1f, 1.0f);
                        animation1.setDuration(1000);

                        theActivity.llPopupWin.setAnimation(animation1);

                        theActivity.mHandler.sendEmptyMessageDelayed(2, 3000);
                        break;
                    case 2:
                        AlphaAnimation animation2 = new AlphaAnimation(1.0f, 0.1f);
                        animation2.setDuration(1000);

                        theActivity.llPopupWin.setAnimation(animation2);


                        theActivity.llPopupWin.setVisibility(View.GONE);

                        if (theActivity.popupWinInt < theActivity.alert.size() - 1) {
                            theActivity.popupWinInt++;
                            theActivity.mHandler.sendEmptyMessageDelayed(1, 3000);
                        }
                        break;
                    case 3:
                        theActivity.mSkuTopVisorgone.setVisibility(View.VISIBLE);
                        float translationY = theActivity.mSkuTopVisorgone.getTranslationY();
                        ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(theActivity.mSkuTopVisorgone, "translationY", translationY, 100);
                        translationYAnimator.setDuration(800);
                        translationYAnimator.start();
                        Timer timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                theActivity.time--;
                                if (theActivity.time >= 0) {
                                    if (theActivity.time >= 10) {
                                        theActivity.mSkuTopTime.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                theActivity.mSkuTopTime.setText("00:00:" + theActivity.time);
                                            }
                                        });


                                    } else {
                                        theActivity.mSkuTopTime.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                theActivity.mSkuTopTime.setText("00:00:0" + theActivity.time);
                                            }
                                        });
                                    }
                                } else {
                                    cancel();
                                }
                            }
                        }, 0, 1000);

                        theActivity.mHandler.sendEmptyMessageDelayed(4, 30000);
                        break;
                    case 4:

                        if (theActivity.mSkuTopVisorgone.getVisibility() == View.VISIBLE) {
                            ObjectAnimator animator2 = ObjectAnimator.ofFloat(theActivity.mSkuTopVisorgone, "alpha", 1f, 0f);
                            // 表示的是:
                            // 动画作用对象是mButton
                            // 动画作用的对象的属性是透明度alpha
                            // 动画效果是:常规 - 全透明 - 常规
                            animator2.setDuration(800);
                            animator2.start();

                        }
                    case 5:
                        String url = (String) msg.obj;
                        theActivity.mAddShopcarImg.setVisibility(View.VISIBLE);
                        Glide.with(theActivity).load(url)
                                .transform(new GlideCircleTransform(theActivity))
                                .placeholder(R.drawable.sku_shopping_car)
                                .error(R.drawable.sku_shopping_car)
                                .dontAnimate()
                                .into(theActivity.mAddShopcarImg);

                        //缩放
                        ObjectAnimator scaleX = ObjectAnimator.ofFloat(theActivity.mAddShopcarImg, "scaleX", 1f, 0f);
                        ObjectAnimator scaleY = ObjectAnimator.ofFloat(theActivity.mAddShopcarImg, "scaleY", 1f, 0f);
                        float translationX = theActivity.mAddShopcarImg.getTranslationX();
                        float translationY2 = theActivity.mAddShopcarImg.getTranslationY();

                        DisplayMetrics metric = new DisplayMetrics();
                        theActivity.getWindowManager().getDefaultDisplay().getMetrics(metric);
                        int windowsWight = metric.widthPixels;
                        int startX = windowsWight / 2;
                        int startY = Utils.dip2px(187);

                        int endX = windowsWight - Utils.dip2px(60);
                        int endY = theActivity.statusbarHeight + Utils.dip2px(20);

                        int transX = endX - startX;
                        int transY = endY - startY;

                        ObjectAnimator translationX1 = ObjectAnimator.ofFloat(theActivity.mAddShopcarImg, "translationX", translationX, transX);
                        ObjectAnimator translationY1 = ObjectAnimator.ofFloat(theActivity.mAddShopcarImg, "translationY", translationY2, transY);

                        AnimatorSet animatorSet = new AnimatorSet();
                        animatorSet.play(scaleX)
                                .with(scaleY)
                                .with(translationX1)
                                .with(translationY1);
                        animatorSet.setDuration(700);
                        animatorSet.start();
                        animatorSet.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                theActivity.mAddShopcarImg.setVisibility(View.GONE);
                                theActivity.mAddShopcarImg.setTranslationX(0);
                                theActivity.mAddShopcarImg.setTranslationY(0);

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        break;
                }
            }
        }
    }

    private float downY;
    private float downX;
    private boolean isDacuBackground = false;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_tao_detail;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initView() {
        mContext = TaoDetailActivity.this;
        uid = Utils.getUid();
        urserLoginStatus = Utils.isLogin();
        ViewGroup.LayoutParams layoutParams = mTopRly1.getLayoutParams();
        layoutParams.height = Utils.dip2px(85) + statusbarHeight;
        mTopRly1.setLayoutParams(layoutParams);
        ViewGroup.MarginLayoutParams layoutParams1 = (ViewGroup.MarginLayoutParams) mTopTitle.getLayoutParams();
        layoutParams1.topMargin = statusbarHeight;
        mTopTitle.setLayoutParams(layoutParams1);

        AdertAdv.OtherBackgroundImgBean data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);
        if (data != null && data.getImg() != null) {
            dacuBackground.setVisibility(View.INVISIBLE);
            Glide.with(mContext).load(data.getImg()).into(dacuBackground);
            isDacuBackground = true;
        } else {
            dacuBackground.setVisibility(View.GONE);
        }
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.LayoutParams params = dacuBackground.getLayoutParams();
        params.height = Utils.dip2px(85) + statusbarHeight;
        dacuBackground.setLayoutParams(params);


        mTopBackground.setAlpha(0);
        mTaoDetailTablayout.setAlpha(0);
        setTopIsTransparent(true);

        Intent it = getIntent();
        taoid = it.getStringExtra("id");
        source = it.getStringExtra("source");
        objid = it.getStringExtra("objid");
        mU = it.getStringExtra("u");
        isAutoSend = it.getStringExtra("isAutoSend");
        String illPrompt = Cfg.loadStr(mContext, SKU_ILLUSTRATED_PROMPT, "0");
//        if ("0".equals(illPrompt) && Utils.isLogin()) {
//            mSkuTipHuabao2.setVisibility(View.VISIBLE);
//        } else {
//            mSkuTipHuabao2.setVisibility(View.GONE);
//        }
        pageJumpManager = new PageJumpManager(mContext);

        // 动画初始化
        mScalInAnimation1 = AnimationUtils.loadAnimation(mContext, R.anim.root_in);
        mScalInAnimation2 = AnimationUtils.loadAnimation(mContext, R.anim.root_in2);
        mScalOutAnimation = AnimationUtils.loadAnimation(mContext, R.anim.root_out);

//        mScalInAnimation1.setAnimationListener(new ScalInAnimation1());
        mBottomExpandView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mExpandViewWidth = mBottomExpandView.getWidth();
                Log.e(TAG, "width == " + mExpandViewWidth);
                mBottomExpandView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });


        ExposureLoginData exposureLoginData = new ExposureLoginData("2", taoid);
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));

        //预取号
        JVerificationInterface.preLogin(mContext, 5000, new PreLoginListener() {
            @Override
            public void onResult(int i, String s) {
                Log.e(TAG, "code ==" + i + ",  content===" + s);
            }
        });
    }


    @Override
    protected void initData() {
        mySixinNum = Cfg.loadInt(mContext, FinalConstant.SIXIN_ID, 0);
        mDetailPop = new TaoDetailPop(mContext, mySixinNum + "");
        mTaoDeatailFragment = TaoDeatailFragment.newInstance(taoid, source, objid);
        mTaoDeatailFragment.setTabLayout(mTaoDetailTablayout);
        mTaoDeatailFragment.setSkuContainer(mSkuActContainer);
        mTaoDeatailFragment.setBottomExpandView(mBottomExpandView);
        mTaoDeatailFragment.setBottomExpandTxt(mBottomExpandTxt);

        mTaoDeatailFragment.setOnEventCallBack(new TaoDeatailFragment.OnEventCallBack() {
            @Override
            public void onLoadDataCallBck(TaoDetailBean detailBean) {
                mTaoDetailBean = detailBean;
                TaoDetailBean.BasedataBean basedata = detailBean.getBasedata();
                TaoDetailBean.GroupInfoBean groupInfo = detailBean.getGroupInfo();
                TaoDetailBean.PayPriceBean pay_price = detailBean.getPay_price();
                TaoDetailBean.HosDocBean hos_doc = detailBean.getHos_doc();


                if (!TextUtils.isEmpty(isAutoSend)) {
                    if (Utils.isLogin()) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("hos_id", mTaoDetailBean.getHos_doc().getHospital_id());
                        hashMap.put("pos", isAutoSend);
                        mTaoDeatailFragment.setFlag(false);
                        new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData s) {
                                if ("0".equals(s.code)) {
                                    topRedpackShow();
                                } else {
                                    Log.e(TAG, s.message);
                                }
                            }
                        });
                        HashMap<String, Object> hashMap2 = new HashMap<>();
                        hashMap2.put("obj_type", isAutoSend);
                        hashMap2.put("obj_id", taoid);
                        hashMap2.put("hos_id", mTaoDetailBean.getHos_doc().getHospital_id());
                        new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData s) {
                                if ("1".equals(s.code)) {
                                    Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                }
                            }
                        });

                    }
                }
                taoid = basedata.getId();
                //搜索标题
                String lable = basedata.getLable();
                if (!TextUtils.isEmpty(lable)){
                    mTaoDetailSearchName.setText(lable);
                }else {
                    mTaoDetailSearchName.setText("点击搜索");
                }

                //医院
                mHos_doc = mTaoDetailBean.getHos_doc();
                phone400 = mHos_doc.getPhone();// 400电话
                if (phone400.contains(",")) {
                    String[] split = phone400.split(",");
                    phone400 = split[0];
                    mPhoneFen = split[1];
                }
                mStatus = basedata.getStatus();
                mIs_group = groupInfo.getIs_group();
                int weight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
                Log.e(TAG,"weight ===="+weight);
                if ("1".equals(mIs_group)) {      //拼团
                    //设置拼团吸底样式
                    setGroupBottomUi(weight);
                    homeGuidePage();
                    mSkuGroupBottom.setVisibility(View.VISIBLE);
                    mSkuCommonBottom.setVisibility(View.GONE);
                    mSkuGroupSinglePrice.setText(basedata.getPrice_discount());     //拼团全款
                    mSkuGroupSpeltPrice.setText(groupInfo.getGroup_price());        //拼团价格
                    mSkuGroupSpeltTitle.setText(groupInfo.getGroup_number() + "人起拼"); //起拼人数
                    if (!"1".equals(mStatus)) {
                        mSkuGroupBtnVisorgone.setVisibility(View.GONE);
                        mSkuGroupSelloutVisorgone.setVisibility(View.VISIBLE);
                        ViewGroup.LayoutParams layoutParams = mSkuGroupSellout.getLayoutParams();
                        if ("1".equals(mTaoDetailBean.getIs_face_video())){
                            layoutParams.width = Utils.dip2px(192);
                        }else {
                            layoutParams.width = Utils.dip2px(216);
                        }
                        mSkuGroupSellout.setLayoutParams(layoutParams);

                    } else {
                        mSkuGroupBtnVisorgone.setVisibility(View.VISIBLE);
                        mSkuGroupSelloutVisorgone.setVisibility(View.GONE);
                    }
                    if (mTaoDetailBean.getIs_presence_tao_pk().equals("1")) {
                        //存在对比中
                        iv_pk_group.setImageResource(R.drawable.pk_shopping);
                    } else {
                        //不存在对比中
                        iv_pk_group.setImageResource(R.drawable.pk);
                    }

                } else {

                    //设置正常吸底样式
                    setCommonBottomUi(weight);

                    if (mTaoDetailBean.getIs_presence_tao_pk().equals("1")) {
                        //存在对比中
                        iv_pk_common.setImageResource(R.drawable.pk_shopping);
                    } else {
                        //不存在对比中
                        iv_pk_common.setImageResource(R.drawable.pk);
                    }
                    //常规
                    mSkuGroupBottom.setVisibility(View.GONE);
                    mSkuCommonBottom.setVisibility(View.VISIBLE);
                    mSkuCommonBottomYunyue.setText(pay_price.getDingjin());
                    mSkuCommonBottomDaoyuan.setText("到院再付:¥" + pay_price.getHos_price());
                    if (!"1".equals(mStatus)) {
                        mSkuCommonBottomBtnVisorgone.setVisibility(View.GONE);
                        mSkuCommonBottomSelloutVisorgone.setVisibility(View.VISIBLE);
                        ViewGroup.LayoutParams layoutParams = mSkuCommonBottomSellout.getLayoutParams();
                        if ("1".equals(mTaoDetailBean.getIs_face_video())){
                            layoutParams.width = Utils.dip2px(192);
                        }else {
                            layoutParams.width = Utils.dip2px(216);
                        }
                        mSkuCommonBottomSellout.setLayoutParams(layoutParams);

                    } else {
                        mSkuCommonBottomBtnVisorgone.setVisibility(View.VISIBLE);
                        mSkuCommonBottomSelloutVisorgone.setVisibility(View.GONE);
                    }

                }
                if (mNumChangeReceive == null) {
                    mNumChangeReceive = new MessageNumChangeReceive(hos_doc.getHospital_id());
                    IntentFilter intentFilter = new IntentFilter(MessageNumChangeReceive.ACTION);
                    registerReceiver(mNumChangeReceive, intentFilter);
                    mNumChangeReceive.setReceiveCallBack(new MessageNumChangeReceive.ReceiveCallBack() {
                        @Override
                        public void onReceiveCallBack() {
                            String groupId = Cfg.loadStr(mContext, FinalConstant.GROUP_ID, "");
                            String userMore = Cfg.loadStr(mContext, FinalConstant.USER_MORE, "");
                            Log.e(TAG, "grouid == " + groupId);
                            Log.e(TAG, "userMore == " + userMore);
                            if (!"1".equals(groupId) || "1".equals(userMore)) {
                                return;
                            }
                            mMessageNum++;
                            if ("1".equals(mIs_group)) {
                                mGroupMessageIv.setVisibility(View.GONE);
                                mGroupMessageNumTv.setVisibility(View.VISIBLE);
                                mGroupMessageNumTv.setText("" + mMessageNum);
                            } else {
                                mCommonMessageIv.setVisibility(View.GONE);
                                mCommonMessageNumTv.setVisibility(View.VISIBLE);
                                mCommonMessageNumTv.setText("" + mMessageNum);
                            }
                        }
                    });

                }
                TaoDetailBean.ShareBean share = detailBean.getShare();
                shareUrl = share.getUrl();
                shareContent = share.getContent();
                shareImgUrl = share.getImg_weibo();
                ShareWechat wechat = share.getWechat();
                mPictorial = share.getPictorial();
                myShareBoardlistener = new MyShareBoardlistener(mContext, wechat, mPictorial);
                buttomDialogView2 = new ButtomDialogView.Builder(mContext).setShareBoardListener(myShareBoardlistener).setIscancelable(true).setIsBackCancelable(true).build();
                if (mPictorial == null) {
                    buttomDialogView2.getButtomDialogBeans().remove(0);
//                    mSkuTipHuabao2.setVisibility(View.GONE);
                }
                if (Utils.isLogin()) {
                    initIfCollect();
                }
                //弹层提示
                alert = mTaoDetailBean.getAlert();
                TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
                if (chatData != null) {
                    String is_rongyun = chatData.getIs_rongyun();
                    if ("3".equals(is_rongyun)) {
                        List<TaoQuickReply> taoQuickReply = mTaoDetailBean.getTaoQuickReply();
                        if (CollectionUtils.isNotEmpty(taoQuickReply)) {
                            mBottomExpandView.setVisibility(View.VISIBLE);
                        } else {
                            mBottomExpandView.setVisibility(View.GONE);
                        }

                    } else {
                        mBottomExpandView.setVisibility(View.GONE);
                    }
                }
                mHandler.sendEmptyMessageDelayed(1, 1000);
            }

            @Override
            public void onCountDwonViewCallBack(CountDownView countDownView) {
                mCountDownView = countDownView;
            }

            @Override
            public void onDacuCountDownCallBack(DacuCountDownView dacuCountDownView) {
                mDacuCountDownView = dacuCountDownView;
            }

            @Override
            public void onScrollChanged(int t, int height) {
                setTopAlpha(t, height);
            }

            @Override
            public void onTabClick(int pos) {
                if (pos == 0) {
                    if (isFirstInit) {
                        mTopRly1.setVisibility(View.GONE);
                    } else {
                        mTopRly1.setVisibility(View.VISIBLE);
                        isFirstInit = true;
                    }

                } else {
                    mTopRly1.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCommentClick(boolean b, String title, CommentFragment commentFragment) {
                commentClick = b;
                mCommentFragment = commentFragment;
                if (b) {
                    ViewGroup.LayoutParams layoutParams = mTopRly1.getLayoutParams();
                    layoutParams.height = Utils.dip2px(50) + statusbarHeight;
                    mTopRly1.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void fenqiClick() {


                String urlss = FinalConstant.LEBAIFEN;
                HashMap<String, Object> urlMap = new HashMap<>();
                urlMap.put("tao_id", taoid);
                baoxianPop = new SkuFenqiPop(mContext, urlss, urlMap);
                baoxianPop.setOnDismissListener(new OnPopupDismissListener());
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.alpha = 0.7f;
                getWindow().setAttributes(lp);
                baoxianPop.showAtLocation(mSkuActContainer, Gravity.BOTTOM, 0, 0);


            }

        });
        setActivityFragment(R.id.sku_container, mTaoDeatailFragment);


    }


    private void setGroupBottomUi(int weight) {
        String isFaceVideo = mTaoDetailBean.getIs_face_video();//是否是视频面诊 1是
        LinearLayout.MarginLayoutParams layoutParams = (LinearLayout.MarginLayoutParams) mSkuGroupPK.getLayoutParams();
        ViewGroup.MarginLayoutParams btnVisorgoneLayoutParams = (ViewGroup.MarginLayoutParams) mSkuGroupBtnVisorgone.getLayoutParams();
        ViewGroup.LayoutParams bottomAddcarLayoutParams = mSkuGroupSingle.getLayoutParams();
        ViewGroup.LayoutParams bottomBuyLayoutParams = mSkuGroupSpelt.getLayoutParams();
        if ("1".equals(isFaceVideo)){
            mSkuGroupVideoChat.setVisibility(View.VISIBLE);
            ViewGroup.MarginLayoutParams videoChatLayoutParams = (ViewGroup.MarginLayoutParams) mSkuGroupVideoChat.getLayoutParams();
            videoChatLayoutParams.leftMargin = (int)(weight* 0.24);
            mSkuGroupVideoChat.setLayoutParams(videoChatLayoutParams);
            layoutParams.leftMargin = (int)(weight* 0.33);
            btnVisorgoneLayoutParams.rightMargin = Utils.dip2px(10);
            bottomAddcarLayoutParams.width = Utils.dip2px(96);
            bottomBuyLayoutParams.width = Utils.dip2px(96);
        }else{
            mSkuGroupVideoChat.setVisibility(View.GONE);
            layoutParams.leftMargin = (int)(weight* 0.23);
            btnVisorgoneLayoutParams.rightMargin = Utils.dip2px(15);
            bottomAddcarLayoutParams.width = Utils.dip2px(108);
            bottomBuyLayoutParams.width = Utils.dip2px(108);
        }
        mSkuGroupPK.setLayoutParams(layoutParams);
        mSkuGroupBtnVisorgone.setLayoutParams(btnVisorgoneLayoutParams);
        mSkuGroupSingle.setLayoutParams(bottomAddcarLayoutParams);
        mSkuGroupSpelt.setLayoutParams(bottomBuyLayoutParams);
    }


    private void setCommonBottomUi(int weight) {
        String isFaceVideo = mTaoDetailBean.getIs_face_video();//是否是视频面诊 1是
        LinearLayout.MarginLayoutParams layoutParams = (LinearLayout.MarginLayoutParams) mSkuCommonBottomPK.getLayoutParams();
        ViewGroup.MarginLayoutParams btnVisorgoneLayoutParams = (ViewGroup.MarginLayoutParams) mSkuCommonBottomBtnVisorgone.getLayoutParams();
        ViewGroup.LayoutParams bottomAddcarLayoutParams = mSkuCommonBottomAddcar.getLayoutParams();
        ViewGroup.LayoutParams bottomBuyLayoutParams = mSkuCommonBottomBuy.getLayoutParams();
        if ("1".equals(isFaceVideo)){
            mSkuCommonBottomVideoChat.setVisibility(View.VISIBLE);
            ViewGroup.MarginLayoutParams videoChatLayoutParams = (ViewGroup.MarginLayoutParams) mSkuCommonBottomVideoChat.getLayoutParams();
            videoChatLayoutParams.leftMargin = (int)(weight* 0.24);
            mSkuCommonBottomVideoChat.setLayoutParams(videoChatLayoutParams);
            layoutParams.leftMargin = (int)(weight* 0.33);
            btnVisorgoneLayoutParams.rightMargin = Utils.dip2px(10);
            bottomAddcarLayoutParams.width = Utils.dip2px(96);
            bottomBuyLayoutParams.width = Utils.dip2px(96);
        }else{
            mSkuCommonBottomVideoChat.setVisibility(View.GONE);
            layoutParams.leftMargin = (int)(weight* 0.23);
            btnVisorgoneLayoutParams.rightMargin = Utils.dip2px(15);
            bottomAddcarLayoutParams.width = Utils.dip2px(108);
            bottomBuyLayoutParams.width = Utils.dip2px(108);
        }
        mSkuCommonBottomPK.setLayoutParams(layoutParams);
        mSkuCommonBottomBtnVisorgone.setLayoutParams(btnVisorgoneLayoutParams);
        mSkuCommonBottomAddcar.setLayoutParams(bottomAddcarLayoutParams);
        mSkuCommonBottomBuy.setLayoutParams(bottomBuyLayoutParams);
    }

    private int onclik = 1;
    private boolean isFirstInit = false;


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    /**
     * 顶部红包
     */
    @SuppressLint("ClickableViewAccessibility")
    private void topRedpackShow() {
        //顶部幸运红包
        final AlertCouponsBean alertCoupons = mTaoDetailBean.getAlertCoupons();
        if (alertCoupons != null) {
            int coupons_id = alertCoupons.getCoupons_id();
            if (coupons_id > 0) {
                if (Utils.isLogin()) {
                    String sku_topconpos = Cfg.loadStr(mContext, "sku_topconpos", "");
                    if (TextUtils.isEmpty(sku_topconpos)) {
                        HashMap<String, String> event_params = alertCoupons.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(event_params);
                        mHandler.sendEmptyMessageDelayed(3, 2000);
                        Cfg.saveStr(mContext, "sku_topconpos", "1");
                        Calendar calendar = Calendar.getInstance();
                        String data = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
                        Log.e(TAG, "data == >>" + data);
                        Cfg.saveStr(mContext, "sku_toptime", data);


                    } else {
                        String sku_toptime = Cfg.loadStr(mContext, "sku_toptime", "");
                        Calendar calendar = Calendar.getInstance();
                        String data = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
                        long daySub = Utils.getDaySub(sku_toptime, data);
                        Log.e(TAG, "time ==>>> " + daySub);
                        if (daySub > 7) {
                            mHandler.sendEmptyMessageDelayed(3, 2000);
                            Cfg.saveStr(mContext, "sku_toptime", data);
                            HashMap<String, String> event_params = alertCoupons.getEvent_params();
                            YmStatistics.getInstance().tongjiApp(event_params);
                        }
                    }

                }

            }
        }
        mSkuTopVisorgone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                mSkuTopVisorgone.setVisibility(View.GONE);
                mHandler.removeMessages(3);
                Map<String, Object> maps = new HashMap<>();
                maps.put("id", alertCoupons.getCoupons_id() + "");
                new DaiJinJuanApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            SkuCouposPopwindow skuOrdingPopwindow = new SkuCouposPopwindow(mContext, alertCoupons.getMoney() + "", alertCoupons.getLimit_money() + "");
                            skuOrdingPopwindow.setOnDismissListener(new OnPopupDismissListener());
                            WindowManager.LayoutParams lp = getWindow().getAttributes();
                            lp.alpha = 0.7f;
                            getWindow().setAttributes(lp);
                            skuOrdingPopwindow.showAtLocation(mSkuActContainer, Gravity.CENTER, 0, 0);
//
                        } else {
                            mFunctionManager.showShort(serverData.message);
                        }
                    }

                });
            }
        });
        mSkuTopVisorgone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downY = event.getRawY();
                        downX = event.getRawX();
                        Log.e("onTouch", "downY ==" + downY);

                        break;
                    case MotionEvent.ACTION_MOVE:
                        float currentY = event.getRawY();
                        if ((downY - currentY) >= 10) {
                            ObjectAnimator a = ObjectAnimator.ofFloat(mSkuTopVisorgone, "translationY", 0, -700);
                            a.setDuration(600);
                            a.start();

                            a.addListener(new Animator.AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    mSkuTopVisorgone.setVisibility(View.GONE);
                                    mHandler.removeMessages(3);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        //检测移动的距离，如果很微小可以认为是点击事件
                        if (Math.abs(event.getRawX() - downX) < 10 && Math.abs(event.getRawY() - downY) < 10) {
                            try {
                                Field field = View.class.getDeclaredField("mListenerInfo");
                                field.setAccessible(true);
                                Object object = field.get(mSkuTopVisorgone);
                                field = object.getClass().getDeclaredField("mOnClickListener");
                                field.setAccessible(true);
                                object = field.get(object);
                                if (object != null && object instanceof View.OnClickListener) {
                                    ((View.OnClickListener) object).onClick(mSkuTopVisorgone);
                                }
                            } catch (Exception e) {

                            }
                            return false;
                        } else {
                            Log.i("mandroid.cn", "button已移动");
                        }
                        break;

                }
                return true;
            }
        });

        mSkuTopClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSkuTopVisorgone.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 拼团引导
     */
    @SuppressLint("ClickableViewAccessibility")
    private void homeGuidePage() {

        String ifTips = Cfg.loadStr(mContext, "select_sku645", "");

        if (ifTips.length() > 0) {
            skuTips.setVisibility(View.GONE);
        } else {
            skuTips.setVisibility(View.VISIBLE);
            Cfg.saveStr(mContext, "select_sku645", "1");
        }

        //我知道了，点击事件
        mSkuTipsKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onclik == 1) {
                    skuTips.setBackgroundResource(R.drawable.tao_tips_2);
                    onclik++;

                } else if (onclik == 2) {
                    skuTips.setVisibility(View.GONE);

                }

            }
        });

        /**
         * 消费点击事件
         */
        skuTips.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        //跳过，点击事件
        mSkuTipsSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skuTips.setVisibility(View.GONE);

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            mTaotetalCartNum.setVisibility(View.VISIBLE);
            mTaotetalCartNum.setText(cartNumber);
        } else {
            mTaotetalCartNum.setVisibility(View.GONE);
        }
        if (urserLoginStatus != Utils.isLogin()) {
            if (null != mTaoDeatailFragment) {
                mTaoDeatailFragment.refreshData();
            }
            if (null != mCommentFragment) {
                mCommentFragment.refreshData();
            }
            urserLoginStatus = Utils.isLogin();

        }
        mMessageNum = 0;
        if ("1".equals(mIs_group)) {
            mGroupMessageIv.setVisibility(View.VISIBLE);
            mGroupMessageNumTv.setVisibility(View.GONE);
        } else {
            mCommonMessageIv.setVisibility(View.VISIBLE);
            mCommonMessageNumTv.setVisibility(View.GONE);
        }
        //为了更新pk图标
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", taoid);
        maps.put("source", source);
        maps.put("objid", objid);
        new TaoDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mTaoDetailBean = JSONUtil.TransformSingleBean(serverData.data, TaoDetailBean.class);
                    if(mTaoDetailBean.getIs_presence_tao_pk().equals("1")){
                        iv_pk_common.setImageResource(R.drawable.pk_shopping);
                        iv_pk_group.setImageResource(R.drawable.pk_shopping);
                    }else{
                        iv_pk_common.setImageResource(R.drawable.pk);
                        iv_pk_group.setImageResource(R.drawable.pk);
                    }
                }
            }
        });
    }

    @OnClick({R.id.tao_det_web_back1, R.id.taotetal_shopcar_click, R.id.taotetal_collect_click, R.id.tao_web_share_rly1, R.id.sku_bottom_common_chat, R.id.sku_bottom_common_hos, R.id.sku_bottom_common_pk,
            R.id.sku_bottom_common_addcar, R.id.sku_bottom_common_buy, R.id.sku_bottom_group_hos, R.id.sku_bottom_group_pk, R.id.sku_group_single, R.id.sku_group_common_chat, R.id.sku_group_spelt, R.id.sku_bottom_common_detail, R.id.tao_bottom_expandview,R.id.sku_search_click,R.id.sku_bottom_common_video_chat,R.id.sku_bottom_group_video_chat})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tao_det_web_back1://后退
                if (commentClick) {
                    if (mTaoDetailTablayout != null) {
                        if (mTaoDetailTablayout.getVisibility() == View.GONE) {
                            mTaoDetailTablayout.setVisibility(View.VISIBLE);
                        }
                    }
                    ViewGroup.LayoutParams layoutParams = mTopRly1.getLayoutParams();
                    layoutParams.height = Utils.dip2px(85) + statusbarHeight;
                    mTopRly1.setLayoutParams(layoutParams);
                    getSupportFragmentManager().popBackStack();
                    commentClick = false;
                } else {
                    finish();
                }
                break;
            case R.id.taotetal_shopcar_click://购物车点击
                HashMap<String, String> map = new HashMap<>();
                map.put("id", taoid);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_CART), map, new ActivityTypeData("2"));
                Intent intent = new Intent(mContext, ShoppingCartActivity.class);
                startActivity(intent);
                break;
            case R.id.taotetal_collect_click://收藏点击
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (Utils.isLogin()) {
                    if (ifcollect) {
                        deleCollectHttp();
                    } else {
                        tongjiClick("6");
                        HashMap<String, String> map1 = new HashMap<>();
                        map1.put("id", taoid);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_COLLECT, "tao", taoid), map1, new ActivityTypeData("2"));
                        collectHttp();
                    }
                } else {
                    jumpLogin();
                }

                break;
            case R.id.tao_web_share_rly1:  //更多点击
                mDetailPop.showAtLocation(mTopRly1, Gravity.RIGHT | Gravity.TOP, Utils.dip2px(7), Utils.dip2px(70));
                mDetailPop.setOnPopItemClickListener(new TaoDetailPop.OnPopItemClickListener() {
                    @Override
                    public void share(View view) {
                        mDetailPop.dismiss();
                        if ("1".equals(mStatus)) {
                            tongjiClick("4");
                            HashMap<String, String> map = new HashMap<>();
                            map.put("id", taoid);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_SHARE, "0", taoid), map, new ActivityTypeData("2"));
                            setShare();
                        } else {
                            mFunctionManager.showShort("服务已结束，无法分享");
                        }
                    }

                    @Override
                    public void message(View view) {
                        if (Utils.isLoginAndBind(mContext)) {
                            MainTableActivity.mainBottomBar.setCheckedPos(3);
                            Intent intent = new Intent();
                            intent.setClass(mContext, MainTableActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    }

                    @Override
                    public void home(View view) {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("id", taoid);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_HOME, "0"), map, new ActivityTypeData("2"));
                        MainTableActivity.mainBottomBar.setCheckedPos(0);
                        Intent intent = new Intent();
                        intent.setClass(mContext, MainTableActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                break;
            case R.id.sku_bottom_common_detail: //普通吸底到院付尾款
                if (Utils.isLoginAndBind(mContext)) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("tao_id", taoid);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_YUDING, "left", "normal"), hashMap, new ActivityTypeData("2"));
                    if (mTaoDetailBean != null) {
                        SkuOrdingPopwindow skuOrdingPopwindow = new SkuOrdingPopwindow(mContext, "1", mTaoDetailBean, source, objid, "0", mU);
                        skuOrdingPopwindow.setOnDismissListener(new OnPopupDismissListener());
                        WindowManager.LayoutParams lp = getWindow().getAttributes();
                        lp.alpha = 0.7f;
                        getWindow().setAttributes(lp);
                        skuOrdingPopwindow.showAtLocation(mSkuActContainer, Gravity.BOTTOM, 0, 0);
                    }
                }
                break;
            case R.id.sku_bottom_common_hos:
                ClickData hospitalClickData = mHos_doc.getHospitalClickData();
                HashMap<String, String> event_params1 = hospitalClickData.getEvent_params();
                YmStatistics.getInstance().tongjiApp(new EventParamData("0", "bottom"), event_params1);
                WebUrlTypeUtil.getInstance(mContext).urlToApp(hospitalClickData.getUrl());
                break;
            case R.id.sku_bottom_common_chat://普通吸底咨询
                if (Utils.noLoginChat()) {
                    commonChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        commonChat();
                    }
                }
                break;
            case R.id.sku_bottom_common_addcar://普通吸底加入购物车
                List<TaoDetailBean.VideoBean> video = mTaoDetailBean.getVideo();
                String img = "";
                if (video != null && video.size() > 0) {
                    img = video.get(0).getImg();
                } else {
                    List<TaoDetailBean.PicBean> pic = mTaoDetailBean.getPic();
                    img = pic.get(0).getImg();
                }
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("tao_id", taoid);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_YUDING, "right", "cart"), hashMap, new ActivityTypeData("2"));
                Message message = mHandler.obtainMessage();
                message.obj = img;
                message.what = 5;
                mHandler.sendMessage(message);

                HashMap<String, Object> maps2 = new HashMap<>();
                maps2.put("source", source);
                maps2.put("objid", objid);
                if (!TextUtils.isEmpty(mU)) {
                    maps2.put("u", mU);
                }
                maps2.put("tao_id", taoid);
                maps2.put("referer", "tao/tao");

                new SkuAddcarApi().getCallBack(mContext, maps2, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            CartSkuNumberApi cartSkuNumberApi = new CartSkuNumberApi();
                            cartSkuNumberApi.getCallBack(mContext, cartSkuNumberApi.getmCartSkuNumberHashMap(), new BaseCallBackListener<CartSkuNumberData>() {
                                @Override
                                public void onSuccess(CartSkuNumberData data) {
                                    if (Integer.parseInt(data.getCart_number()) >= 100) {
                                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, "99+");
                                    } else {
                                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, data.getCart_number());
                                    }
                                    //购物车数量
                                    if (!TextUtils.isEmpty(data.getCart_number()) && !"0".equals(data.getCart_number())) {
                                        mTaotetalCartNum.setVisibility(View.VISIBLE);
                                        mTaotetalCartNum.setText(data.getCart_number());
                                    } else {
                                        mTaotetalCartNum.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                        mFunctionManager.showShort(serverData.message);
                    }
                });
                break;
            case R.id.sku_group_single:      //拼团单独购买
            case R.id.sku_bottom_common_buy:   //  普通预定
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        HashMap<String, String> hashMapOrding = new HashMap<>();
                        hashMapOrding.put("tao_id", taoid);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_YUDING, "right", "normal"), hashMapOrding, new ActivityTypeData("2"));
                        if (mTaoDetailBean != null) {
                            jumpGoOrding("1", "", 0);
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }
                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.sku_bottom_group_hos:   //拼团医院
                if (mTaoDetailBean != null) {
                    Intent it3 = new Intent();
                    it3.setClass(mContext, HosDetailActivity.class);
                    it3.putExtra("hosid", mTaoDetailBean.getHos_doc().getHospital_id());
                    startActivity(it3);
                    if (Utils.isLogin()) {
                        HashMap<String, Object> hashMaphos = new HashMap<>();
                        hashMaphos.put("hos_id", mTaoDetailBean.getHos_doc().getHospital_id());
                        hashMaphos.put("pos", "1");
                        new AutoSendApi().getCallBack(mContext, hashMaphos, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData s) {
                                if ("1".equals(s.code)) {
                                    Log.e(TAG, s.message);
                                }
                            }
                        });
                    }

                }
                break;
//            case R.id.sku_group_phone:      //拼团电话
//                String phone = mHos_doc.getPhone();
//                if (!TextUtils.isEmpty(phone)) {//打电话
//                    //版本判断
//                    if (Build.VERSION.SDK_INT >= 23) {
//
//                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
//                            @Override
//                            public void onGranted() {        //判断权限是否开启
//                                phoneCall();
//                            }
//
//                            @Override
//                            public void onDenied(List<String> permissions) {
//                                ViewInject.toast("没有电话权限");
//                            }
//                        });
//                    } else {
//                        phoneCall();
//                    }
//                }
//                break;
            case R.id.sku_group_common_chat:      //拼团咨询
                if (Utils.noLoginChat()) {
                    pintuanChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        pintuanChat();
                    }

                }
                break;
            case R.id.sku_group_spelt:      //发起拼团
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        long mday = 0;
                        long mhour = 0;
                        long mmin = 0;
                        long msecond = 0;


                        if (null != mCountDownView) {
                            long[] times = mCountDownView.getTimes();
                            mday = times[0];
                            mhour = times[1];
                            mmin = times[2];
                            msecond = times[3];
                            Log.e(TAG, "mCountDownView" + mCountDownView);
                            Log.e(TAG, "mCountDownView.getTimes()" + mCountDownView.getTimes());

                        }
                        if (null != mDacuCountDownView) {
                            long[] times = mDacuCountDownView.getTimes();
                            mday = times[0];
                            mhour = times[1];
                            mmin = times[2];
                            msecond = times[3];
                        }

                        if (mday == 0 && mhour == 0 && mmin == 0 && msecond == 0) {
                            mFunctionManager.showShort("该拼团已结束");
                        } else {
                            HashMap<String, String> hashMapgroup = new HashMap<>();
                            hashMapgroup.put("tao_id", taoid);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_YUDING, "right", "group"), hashMapgroup, new ActivityTypeData("2"));

                            jumpGoOrding("2", "1", 0);
                        }
                    } else {

                        Utils.jumpBindingPhone(mContext);
                    }
                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.tao_bottom_expandview:
                if (mBottomExpandView == null) {
                    return;
                }
                Log.e(TAG, "mExpandViewWidth ==" + mExpandViewWidth);

                if (mExpandViewWidth == mBottomExpandView.getWidth()) {
                    changeViewWidthAnimatorStart(mBottomExpandView, mExpandViewWidth, Utils.dip2px(63));
                } else {
                    if (!mIsExpend) {
                        mIsExpend = true;
                        mBottomExpandView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_black));
                        mBottomExpandImg.setBackgroundResource(R.drawable.sku_bottom_bottom);
                        mBottomExpandTxt.setText("收起");
                        showChatPop();
                    } else {
                        mIsExpend = false;
                        mBottomExpandView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_expand));
                        mBottomExpandImg.setBackgroundResource(R.drawable.sku_bottom_top);
                        mBottomExpandTxt.setText("快问");
                        mSkuBottomChatPop.dismiss();
                    }

                }
                break;
            case R.id.sku_bottom_group_pk:
                if (mStatus.equals("1")) {
                    HashMap event_params = new HashMap();
                    event_params.put("to_page_type", "167");
                    event_params.put("to_page_id", taoid);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_TAO_PK), event_params, new ActivityTypeData("2"));
                    ProjectContrastActivity.invoke(mContext, taoid, "1");
                }
                break;
            case R.id.sku_bottom_common_pk:
                if (mStatus.equals("1")) {
                    //加入埋点  SKU详情页{tao_pk_event_params}
                    //    我的对比页-淘整形详情
                    HashMap event_params = new HashMap();
                    event_params.put("to_page_type", "167");
                    event_params.put("to_page_id", taoid);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_TAO_PK), event_params, new ActivityTypeData("2"));
                    ProjectContrastActivity.invoke(mContext, taoid, "1");
                }
                break;
            case R.id.sku_search_click://搜索点击
                Intent it = new Intent(mContext, SearchAllActivity668.class);
                String lable = mTaoDetailBean.getBasedata().getLable();
                if (!TextUtils.isEmpty(lable)) {
                    it.putExtra(SearchAllActivity668.KEYS, lable);
                } else {
                    it.putExtra(SearchAllActivity668.KEYS, "");
                }
                it.putExtra(SearchAllActivity668.TYPE, "1");
                startActivity(it);
                HashMap<String, String> stringHashMap = new HashMap<>();
                stringHashMap.put("id",taoid);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_SEARCH_CLICK),stringHashMap,new ActivityTypeData("2"));
                break;
            case R.id.sku_bottom_common_video_chat://普通视频面诊
            case R.id.sku_bottom_group_video_chat://拼团视频面诊
                if (Utils.isLoginAndBind(mContext)){
                    String faceVideoUrl = mTaoDetailBean.getFace_video_url();
                    WebViewUrlLoading.getInstance().showWebDetail(mContext, faceVideoUrl);
                }

                break;
        }
    }

    private void pintuanChat() {
        TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
        String hos_userid = chatData.getHos_userid();
        String is_rongyun = chatData.getIs_rongyun();
        TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
        String hospital_id = hos_doc.getHospital_id();
        String doc_user_id = hos_doc.getDoc_user_id();
        if ("3".equals(is_rongyun)) {
            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                    .setDirectId(hos_userid)
                    .setObjId(chatData.getObj_id())
                    .setObjType(chatData.getObj_type() + "")
                    .setTitle(mTaoDetailBean.getBasedata().getTitle())
                    .setPrice(mTaoDetailBean.getBasedata().getPrice_discount())
                    .setImg(mTaoDetailBean.getPic().get(0).getImg())
                    .setMemberPrice(mTaoDetailBean.getMember_data().getMember_price())
                    .setYmClass(chatData.getYmaq_class())
                    .setYmId(chatData.getYmaq_id())
                    .setSkuId(taoid)
                    .build();
            new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            tongjiClick("1");
            HashMap<String, String> event_params = chatData.getEvent_params();

            YmStatistics.getInstance().tongjiApp(new EventParamData(chatData.getEvent_name(), ""), event_params);
        } else {
            mFunctionManager.showShort("该服务暂未开通在线客服功能");
        }
    }

    private void commonChat() {
        if (mTaoDetailBean != null) {
            TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
            String hos_userid = chatData.getHos_userid();
            String is_rongyun = chatData.getIs_rongyun();
            TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
            String hospital_id = hos_doc.getHospital_id();
            String doc_user_id = hos_doc.getDoc_user_id();
            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(chatData.getObj_id())
                        .setObjType(chatData.getObj_type() + "")
                        .setTitle(mTaoDetailBean.getBasedata().getTitle())
                        .setPrice(mTaoDetailBean.getBasedata().getPrice_discount())
                        .setImg(mTaoDetailBean.getPic().get(0).getImg())
                        .setMemberPrice(mTaoDetailBean.getMember_data().getMember_price())
                        .setYmClass(chatData.getYmaq_class())
                        .setYmId(chatData.getYmaq_id())
                        .setSkuId(taoid)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                tongjiClick("1");


                //test
                HashMap<String, String> event_params = chatData.getEvent_params();

                YmStatistics.getInstance().tongjiApp(new EventParamData(chatData.getEvent_name(), "0"), event_params);
            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }

    /**
     * 显示弹出的popupwindow
     */
    private void showChatPop() {
        final List<TaoQuickReply> taoQuickReply = mTaoDetailBean.getTaoQuickReply();
        if (CollectionUtils.isNotEmpty(taoQuickReply)) {
            mSkuBottomChatPop = new SkuBottomChatPop(mContext, taoQuickReply);
            mSkuBottomChatPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    WindowManager.LayoutParams lp = getWindow().getAttributes();
                    lp.alpha = 1f;
                    getWindow().setAttributes(lp);
                    mIsExpend = false;
                    mBottomExpandView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_expand));
                    mBottomExpandImg.setBackgroundResource(R.drawable.sku_bottom_top);
                    mBottomExpandTxt.setText("快问");
                    mSkuBottomChatPop.dismiss();
                }
            });
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.alpha = 0.7f;
            getWindow().setAttributes(lp);
            mSkuBottomChatPop.showAtLocation(mSkuActContainer, Gravity.RIGHT | Gravity.BOTTOM, 0, Utils.dip2px(121));
            mSkuBottomChatPop.setPopItemClickListener(new SkuBottomChatPop.PopItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    HashMap<String, String> event_params = taoQuickReply.get(position).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(event_params);
                    if (Utils.noLoginChat()){
                        popToChat(adapter, position, taoQuickReply);
                    }else {
                        if (Utils.isLoginAndBind(mContext)) {
                            popToChat(adapter, position, taoQuickReply);
                        }
                    }
                }
            });
        }
    }

    private void popToChat(BaseQuickAdapter adapter, int position, List<TaoQuickReply> taoQuickReply) {
        mIsExpend = false;
        mBottomExpandView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_expand));
        mBottomExpandImg.setBackgroundResource(R.drawable.sku_bottom_top);
        mBottomExpandTxt.setText("快问");
        TaoQuickReply quickReply = taoQuickReply.get(position);
        if (quickReply != null) {
            mSendContent = quickReply.getSend_content();
            mSendQuickReply = quickReply.getSend_QuickReply();
            isFirstSend = true;
            sendMessage("[我正在看]", mSendQuickReply, "3");
        }
        mSkuBottomChatPop.dismiss();
        taoQuickReply.remove(position);
        adapter.notifyDataSetChanged();
        if (taoQuickReply.isEmpty()) {
            mBottomExpandView.setVisibility(View.GONE);
        }
    }


    private void sendMessage(final String content, String send_QuickReply, String classid) {
        TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
        TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
        if (chatData != null) {
            String ymaqClass = chatData.getYmaq_class();
            String ymaqId = chatData.getYmaq_id();
            String objId = chatData.getObj_id();
            int objType = chatData.getObj_type();
            String hosUserid = chatData.getHos_userid();
            String groupId = chatData.getGroup_id();
            String hospitalId = hos_doc.getHospital_id();
            Map<String, Object> params = new HashMap<>();
            params.put("id", hosUserid);
            params.put("group_id", groupId);
            params.put("hos_id", hospitalId);
            params.put("ymaq_class", ymaqClass);
            params.put("ymaq_id", ymaqId);
            params.put("obj_id", objId);
            params.put("obj_type", objType + "");
            params.put("content", content);
            params.put("classid", classid);
            params.put("send_QuickReply", send_QuickReply);
            new ChatSendMessageApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    if ("1".equals(serverData.code)) {
                        Log.e(TAG, "onSuccess ==" + serverData.toString());
                        Log.e(TAG, "mSendContent ==" + mSendContent + "   mSendQuickReply==" + mSendQuickReply);
                        if (!TextUtils.isEmpty(mSendContent) && !TextUtils.isEmpty(mSendQuickReply)) {
                            if (isFirstSend) { //控制次数不然死循环
                                isFirstSend = false;
                                sendMessage(mSendContent, mSendQuickReply, "4");
                            } else {
                                mFunctionManager.showShort("问题已送达\n" +
                                        "稍后机构会通过私信为您解答");
                            }

                        }

                    }
                }
            });
        }

    }


    /**
     * 弹出动画
     *
     * @param view
     * @param startWidth
     * @param endWidth
     */
    public void changeViewWidthAnimatorStart(final View view, final int startWidth, final int endWidth) {

        if (view != null && startWidth >= 0 && endWidth >= 0) {

            ValueAnimator animator = ValueAnimator.ofInt(startWidth, endWidth);

            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override

                public void onAnimationUpdate(ValueAnimator animation) {

                    ViewGroup.LayoutParams params = view.getLayoutParams();

                    params.width = (int) animation.getAnimatedValue();

                    view.setLayoutParams(params);
                    view.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_expand));
                    mBottomExpandTxt.setVisibility(View.VISIBLE);
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                    marginLayoutParams.rightMargin = Utils.dip2px(15);
                    view.setLayoutParams(marginLayoutParams);
                }

            });

            animator.start();

        }

    }

    @SuppressLint("MissingPermission")
    private void phoneCall() {
        tongjiClick("2");

        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        int minute = t.minute;
        Log.e(TAG, "hour === " + hour);
        if (hour > 9 && hour < 22) {
            Log.e(TAG, "phone400 === " + phone400);
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else if (hour == 9 && minute >= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else if (hour == 21 && minute <= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else {
            showDialogExitEdit();
        }
    }

    public void jumpGoOrding(String type, String flag, int mNumber) {
        //        type 1 正常下单 2，拼团
        //         flag 0 参团 1发起拼团
        Intent intent = new Intent(mContext, MakeSureOrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("tao_id", taoid);
        String start_number = mTaoDetailBean.getBasedata().getStart_number();
        bundle.putString("number", start_number);
        bundle.putString("source", source);
        bundle.putString("objid", objid);
        bundle.putString("buy_for_cart", "0");
        bundle.putString("u", mU);
        TaoDetailBean.GroupInfoBean groupInfo = mTaoDetailBean.getGroupInfo();
        if ("2".equals(type) && groupInfo.getGroup().size() != 0 && !"1".equals(flag)) {
            bundle.putString("group_id", groupInfo.getGroup().get(mNumber).getGroup_id());
            Log.e("SkuOrdingPopwindow", mNumber + "mnumber========>" + groupInfo.getGroup().get(mNumber).getGroup_id());
        }
        if ("2".equals(type) || "1".equals(flag)) {
            bundle.putString("is_group", "1");
        } else {
            bundle.putString("is_group", "0");
        }
        intent.putExtra("data", bundle);
        mContext.startActivity(intent);
    }


    Button cancelBt;
    Button trueBt11;
    TextView titleTv1;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        titleTv1 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv1.setText("客服MM为您服务时间9:30至21:30。此时请留言，客服会尽快帮您解决的哦！");

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
                if (Utils.noLoginChat()) {
                    dialogChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        dialogChat();
                    }
                }
            }
        });

        trueBt11 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt11.setText("去留言");
        trueBt11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
                dialogChat();
            }
        });
    }

    private void dialogChat() {
        if (mTaoDetailBean != null) {
            TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
            String hos_userid = chatData.getHos_userid();
            String is_rongyun = chatData.getIs_rongyun();
            TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(taoid)
                        .setObjType("2")
                        .setTitle(mTaoDetailBean.getBasedata().getTitle())
                        .setPrice(mTaoDetailBean.getBasedata().getPrice_discount())
                        .setImg(mTaoDetailBean.getPic().get(0).getImg())
                        .setMemberPrice(mTaoDetailBean.getMember_data().getMember_price())
                        .setYmClass("2")
                        .setYmId(taoid)
                        .setSkuId(taoid)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                        .setEvent_name("chat_hospital")
                        .setEvent_pos("tao")
                        .setHos_id(hos_doc.getHospital_id())
                        .setDoc_id(hos_doc.getDoc_user_id())
                        .setTao_id(taoid)
                        .setEvent_others(hos_doc.getHospital_id())
                        .setId(taoid)
                        .setReferrer("17")
                        .setType("2")
                        .build();
                Utils.chatTongJi(mContext, tongJiParams);
            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }

    /**
     * 设置是否是透明的
     *
     * @param isTransparent ：是透明的
     */
    private void setTopIsTransparent(boolean isTransparent) {
        if (isTransparent) {
            mFunctionManager.setImgSrc(taoTopBackImg, R.drawable.sku_back_write);
            mFunctionManager.setImgSrc(mTaotetalShopcarImg, R.drawable.shop_car_white);
            mFunctionManager.setImgSrc(mTaoDetTopMoreImg, R.drawable.taodetail_more_white);
            if (ifcollect) {
                mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_select);
            } else {
                mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_clooect_write);
            }
            mFunctionManager.setImgBackground(taoTopBackImg, R.drawable.shape_99444444);
            mFunctionManager.setImgBackground(mTaotetalShopcarImg, R.drawable.shape_99444444);
            mFunctionManager.setImgBackground(mTaoDetTopMoreImg, R.drawable.shape_99444444);
            mFunctionManager.setImgBackground(mTaotetalClooectImg, R.drawable.shape_99444444);

        } else {
            if (isDacuBackground){
                mFunctionManager.setImgSrc(taoTopBackImg, R.drawable.sku_back_write);
                mFunctionManager.setImgSrc(mTaotetalShopcarImg, R.drawable.shop_car_white);
                mFunctionManager.setImgSrc(mTaoDetTopMoreImg, R.drawable.taodetail_more_white);
                if (ifcollect) {
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_select);
                } else {
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_clooect_write);
                }
            }else {
                mFunctionManager.setImgSrc(taoTopBackImg, R.drawable.sku_back_black);
                mFunctionManager.setImgSrc(mTaotetalShopcarImg, R.drawable.shop_car_black);
                mFunctionManager.setImgSrc(mTaoDetTopMoreImg, R.drawable.tao_deatil_more_black);
                if (ifcollect) {
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_select);
                } else {
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_back);
                }
            }
            mFunctionManager.setImgBackground(taoTopBackImg, R.drawable.shape_transparent);
            mFunctionManager.setImgBackground(mTaotetalShopcarImg, R.drawable.shape_transparent);
            mFunctionManager.setImgBackground(mTaoDetTopMoreImg, R.drawable.shape_transparent);
            mFunctionManager.setImgBackground(mTaotetalClooectImg, R.drawable.shape_transparent);
        }

        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            mTaotetalCartNum.setVisibility(View.VISIBLE);
            mTaotetalCartNum.setText(cartNumber);
        } else {
            mTaotetalCartNum.setVisibility(View.GONE);
        }
    }

    /***
     * 设置头部渐变
     * @param t
     */
    private void setTopAlpha(int t, int height) {
        mTopRly1.setVisibility(View.VISIBLE);
        int mHeight = height - mTopRly1.getHeight() < 0 ? 0 : height - mTopRly1.getHeight();

        //设置标题其透明度
        float alpha;
        if (t >= mHeight) {
            alpha = 1;
            mTaoDetailSearchClick.setVisibility(View.VISIBLE);
            setTopIsTransparent(false);
        } else {
            int i = mHeight / 2;
            if (t >= i) {
                alpha = (t - i) / (i * 1.0f); //产生渐变效果
                if (alpha > 0.5) {
                    mTaoDetailSearchClick.setVisibility(View.VISIBLE);
                } else {
                    mTaoDetailSearchClick.setVisibility(View.GONE);
                }

                setTopIsTransparent(false);
            } else {
                alpha = 0;
                mTaoDetailSearchClick.setVisibility(View.GONE);

                setTopIsTransparent(true);
            }

        }
        mTopBackground.setAlpha(alpha);
        mTaoDetailTablayout.setAlpha(alpha);
        if (alpha != 0){
            dacuBackground.setVisibility(View.VISIBLE);
        }else {
            dacuBackground.setVisibility(View.INVISIBLE);
        }

    }

    void tongjiClick(String type) {

        String uid = Utils.getUid();
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", taoid);
        maps.put("uid", uid);
        maps.put("type", type);
        new TongjiClickApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

            }
        });

    }

    /**
     * 自定义控制面板的分享
     */
    private void setShare() {
        myShareBoardlistener.setSinaText("#整形#" + shareContent + shareUrl + "分享自@悦美整形APP").setSinaThumb(new UMImage(mContext, shareImgUrl)).setSmsText(shareContent + "，" + shareUrl).setTencentUrl(shareUrl).setTencentTitle(shareContent).setTencentThumb(new UMImage(mContext, shareImgUrl)).setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {

                    Toast.makeText(mContext, platform + " 分享成功啦", Toast.LENGTH_SHORT).show();

                    tongjiClick("5");

                    if (!"0".equals(uid)) {
                        sumitHttpCode("10");
                    }
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });

        buttomDialogView2.show();
        if (mPictorial != null) {
            String illPrompt = Cfg.loadStr(mContext, SKU_ILLUSTRATED_PROMPT, "0");
            if ("0".equals(illPrompt)) {
//                mSkuTipHuabao2.setVisibility(View.GONE);

                buttomDialogView2.setDialogTipVisible();

                Cfg.saveStr(mContext, SKU_ILLUSTRATED_PROMPT, "1");
            } else {
                buttomDialogView2.setDialogTipGone();
            }
        }
    }

    void sumitHttpCode(final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        maps.put("uid", uid);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    if (!"0".equals(jifenNu) && !"0".equals(jyNu)) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, 1000).show();
                    } else {
                        if (!"0".equals(jifenNu)) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, 1000).show();
                        } else {
                            if (!"0".equals(jyNu)) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, 1000).show();
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * 判断是否收藏
     */
    void initIfCollect() {

        Map<String, Object> maps = new HashMap<>();
        maps.put("objid", taoid);
        maps.put("type", "4");
        new IsCollectApi().getCallBack(mContext, maps, new BaseCallBackListener<String>() {

            @Override
            public void onSuccess(String s) {
                if ("1".equals(s)) {
                    ifcollect = true;
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_select);
//                    mDetailPop.setCollectTextAndImg(ifcollect);
                }
//                else {
//                    ifcollect = false;
//                    mTaotetalClooectImg.setBackgroundResource(R.drawable.sku_collect_back);
////                    mDetailPop.setCollectTextAndImg(ifcollect);
//                }
            }
        });

    }

    /**
     * 收藏接口
     */
    void collectHttp() {
        BaikeFourApi baikeFourApi = new BaikeFourApi();
        Map<String, Object> params = new HashMap<>();
        params.put("objid", taoid);
        params.put("type", "4");
        baikeFourApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    ifcollect = true;
                    if (!serverData.isOtherCode) {
                        mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_select);
                    }
//                    mDetailPop.setCollectTextAndImg(ifcollect);
                    tongjiClick("3");
                    MyToast.makeImgAndTextToast(mContext, ContextCompat.getDrawable(mContext, R.drawable.tips_smile), "收藏成功", 1000).show();
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("hos_id", mTaoDetailBean.getHos_doc().getHospital_id());
                    hashMap.put("pos", "7");
                    new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData s) {
                            if ("1".equals(s.code)) {
                                Log.e(TAG, s.message);
                            }
                        }
                    });
                    HashMap<String, Object> hashMap2 = new HashMap<>();
                    hashMap2.put("obj_type", "7");
                    hashMap2.put("obj_id", taoid);
                    hashMap2.put("hos_id", mTaoDetailBean.getHos_doc().getHospital_id());
                    new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData s) {
                            if ("1".equals(s.code)) {
                                Log.e(TAG, "AutoSendApi2 ==" + s.message);
                            }
                        }
                    });
                } else {
                    mFunctionManager.showShort(serverData.message);
                }

            }
        });
    }

    /**
     * 取消收藏
     */
    void deleCollectHttp() {
        // http://sjapp.yuemei.com/user/delcollect/
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("objid", taoid);
        params.put("type", "4");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    ifcollect = false;
//                    mDetailPop.setCollectTextAndImg(ifcollect);
                    mFunctionManager.setImgSrc(mTaotetalClooectImg, R.drawable.sku_collect_back);
                    MyToast.makeImgAndTextToast(mContext, ContextCompat.getDrawable(mContext, R.drawable.tips_smile), "取消收藏", 1000).show();
                } else {
                    mFunctionManager.showShort(serverData.message);
                }


            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (commentClick) {
                ViewGroup.LayoutParams layoutParams = mTopRly1.getLayoutParams();
                layoutParams.height = Utils.dip2px(85) + statusbarHeight;
                mTopRly1.setLayoutParams(layoutParams);
                getSupportFragmentManager().popBackStack();
                commentClick = false;
                if (mTaoDetailTablayout != null) {
                    if (mTaoDetailTablayout.getVisibility() == View.GONE) {
                        mTaoDetailTablayout.setVisibility(View.VISIBLE);
                    }
                }
//                mTaoDetailTitle.setText("服务详情");
            } else {
                finish();
            }
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    /**
     * 跳转到登录页面
     */
    public void jumpLogin() {
        Intent intent = new Intent(mContext, LoginActivity605.class);
        intent.putExtra("skujump", "1");
        startActivityForResult(intent, LOGIN_REFRESH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == LoginActivity605.REQUEST_CODE) {
            initIfCollect();
        }
        if (mTaoDeatailFragment != null) {
            mTaoDeatailFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * 是否显示提示语
     */
//    private void isShowPrompt() {
//        Calendar cal = Calendar.getInstance();
//
//        String year = cal.get(Calendar.YEAR) + "";
//        String month = cal.get(Calendar.MONTH) + 1 + "";
//        String day = cal.get(Calendar.DAY_OF_MONTH) + "";
//        int hour = cal.get(Calendar.HOUR_OF_DAY);
//
//        Log.e(TAG, "year == " + year);
//        Log.e(TAG, "month == " + month);
//        Log.e(TAG, "day == " + day);
//        Log.e(TAG, "hour == " + hour);
//
//        String taoTime = mFunctionManager.loadStr("taoTime", "");
//        if (!taoTime.equals(year + month + day)) {
//            mFunctionManager.saveStr("taoTime", year + month + day);
//        }
//
//        //加载出保存taoid的集合
//        String taoidList = mFunctionManager.loadStr(taoTime, "");
//        boolean isShow = false;     //默认是没有显示过的
//        List<TaoDetailShow> listData;
//        if (!TextUtils.isEmpty(taoidList)) {
//            listData = JSONUtil.jsonToArrayList(taoidList, TaoDetailShow.class);
//            for (TaoDetailShow data : listData) {
//                if (data.getTaoid().equals(taoid)) {
//                    isShow = true;
//                    break;
//                }
//            }
//        } else {
//            listData = new ArrayList<>();
//        }
//
//        //判断当天是否显示过
//        if (!isShow) {
//            //如果没有显示过的
//            if (hour > 8 && hour < 22) {
//                //时间是早上8点后和晚上10点前
//
//                //保存当前已经加载过
//                listData.add(new TaoDetailShow(taoid));
//                String jsonstr = new Gson().toJson(listData);
//                Log.e(TAG, "jsonstr == " + jsonstr);
//                mFunctionManager.saveStr(taoTime, jsonstr);
//
//                mSkuBottomVisorgone.setVisibility(View.VISIBLE);
//                String show_message = mTaoDetailBean.getChatData().getShow_message();
//                if (!TextUtils.isEmpty(show_message)){
//                    mSkuBottomHosContent.setText(show_message);
//                }
//
//                float translationY = mSkuBottomVisorgone.getTranslationY();
//                ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(mSkuBottomVisorgone, "translationY", 300, translationY);
//                translationYAnimator.setDuration(800);
//                translationYAnimator.start();
//                mHandler.sendEmptyMessageDelayed(0, 5000);
//            }
//        }
//    }


    //    /**
//     * 缩小动画的回调
//     */
//    public class ScalInAnimation1 implements Animation.AnimationListener {
//
//        @Override
//        public void onAnimationStart(Animation animation) {
//
//        }
//
//        @Override
//        public void onAnimationEnd(Animation animation) {
//            mSkuContainer.startAnimation(mScalInAnimation2);
//        }
//
//        @Override
//        public void onAnimationRepeat(Animation animation) {
//
//        }
//    }

    /**
     * popupwindow消失的回调
     */
    private class OnPopupDismissListener implements
            PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            // 标题和主页开始播放动画
//            mSkuContainer.startAnimation(mScalOutAnimation);
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.alpha = 1f;
            getWindow().setAttributes(lp);

        }
    }

    @Override
    protected void onDestroy() {
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();

        if (Util.isOnMainThread() && !this.isFinishing()) {
            Glide.with(MyApplication.getContext()).pauseRequests();
        }
        if (mNumChangeReceive != null) {
            unregisterReceiver(mNumChangeReceive);
        }


    }
}

