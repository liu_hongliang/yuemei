package com.module.commonview.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.CommentsRecyclerAdapter2;
import com.module.commonview.module.api.CommentsListApi;
import com.module.commonview.module.api.DeleteBBsApi;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryReplyInfo;
import com.module.commonview.module.bean.DiaryReplyLisListTao;
import com.module.commonview.module.bean.DiaryReplyList2;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.module.bean.DiaryReplyListPic;
import com.module.commonview.module.bean.DiaryReplyListTao;
import com.module.commonview.module.bean.DiaryReplyListUserdata;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zfdang.multiple_images_selector.SelectorSettings;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 评论列表
 * 热门评论超过3条折叠   回复超过1条折叠  第一次展开5条 第二次展开全部
 */
public class CommentsListActivity extends YMBaseActivity {
    @BindView(R.id.comments_list_top)
    CommonTopBar mTop;
    @BindView(R.id.diary_comments_list_refresh)
    SmartRefreshLayout mCommentsRefresh;
    @BindView(R.id.diary_comments_list_refresh_more)
    YMLoadMore mCommentsRefreshMore;
    @BindView(R.id.comments_list_comments_list)
    RecyclerView mCommentsList;                         //评论列表
    @BindView(R.id.activity_diaries_and_posts_bottom_click)
    FrameLayout mBottomClick;                             //评论

    private String TAG = "CommentsListActivity";
    public static final int RETURN_NUMBER = 100;
    private CommentsListApi mCommentsListApi;           //日记评论列表请求类
    private DeleteBBsApi mDeleteBBsApi;                 //删除评论
    private String mDiaryId;
    private String id;
    private String mAskorshare;
    private String pId;
    private ZanOrJuBaoApi mPointLikeApi;
    private CommentsRecyclerAdapter2 mCommentsRecyclerAdapter2;
    private DiaryCommentDialogView mDiaryCommentDialogView;
    private String userId;
//    private SumbitPhotoData sumbitPhotoData;
    private EditExitDialog editDialog;
    private int mPage = 1;
    private DiaryReplyInfo diaryReplyInfo;
    private List<DiaryReplyList2> diaryReplyLists;
    private List<DiaryReplyList2> NotShowHotLists;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_comments_list;
    }

    @Override
    protected void initView() {
        Bundle bundle = getIntent().getBundleExtra("data");
        id = bundle.getString("id");
        mDiaryId = bundle.getString("diary_id");
        mAskorshare = bundle.getString("askorshare");
        pId = bundle.getString("p_id");
        userId = bundle.getString("user_id");
        Log.e(TAG, "id == " + id);
        Log.e(TAG, "mDiaryId == " + mDiaryId);
        Log.e(TAG, "mDskorshare == " + mAskorshare);
        Log.e(TAG, "pId == " + pId);
        Log.e(TAG, "userId == " + userId);
    }

    @Override
    protected void initData() {
        mCommentsListApi = new CommentsListApi();
        mPointLikeApi = new ZanOrJuBaoApi();
        mDeleteBBsApi = new DeleteBBsApi();
//        sumbitPhotoData = new SumbitPhotoData();

        diaryReplyLists = new ArrayList<DiaryReplyList2>();
        NotShowHotLists = new ArrayList<DiaryReplyList2>();

        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mCommentsList.setLayoutManager(scrollLinearLayoutManager);
        if (!TextUtils.isEmpty(mAskorshare) && "0".equals(mAskorshare)) {
            mTop.setCenterText("全部解答");
        } else {
            mTop.setCenterText("评论列表");
        }

        //返回按钮点击
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //回复主贴
        mBottomClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"0".equals(mAskorshare)) {
                    startComments();
                } else {
                    if (userId.equals(Utils.getUid())) {
                        startComments();
                    } else {
                        mFunctionManager.showShort("问题贴仅限医生和提问者评论");
                    }

                }
            }
        });

        //下拉刷新
        mCommentsRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                Log.e(TAG, "下拉刷新");
                mPage = 1;
//                mCommentsRecyclerAdapter = null;
                if (diaryReplyLists != null) {
                    diaryReplyLists.clear();
                }
                mCommentsRecyclerAdapter2 = null;
                loadCommentsList();
            }
        });

        //上拉加载更多
        mCommentsRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadCommentsList();
            }
        });

        loadCommentsList();
    }

    /**
     * 加载评论列表
     */
    private void loadCommentsList() {
        mCommentsListApi.addData("postuserid", id);                     //帖子用户id
        mCommentsListApi.addData("q_id", mDiaryId);                     //帖子id
        mCommentsListApi.addData("askorshare", mAskorshare);            //帖子类型
        mCommentsListApi.addData("p_id", pId);
        mCommentsListApi.addData("page", mPage + "");
        //最后一条id
        if (diaryReplyInfo == null || TextUtils.isEmpty(diaryReplyInfo.getNew_reply_last_id())) {
            mCommentsListApi.addData("last_id", "0");
        } else {
            mCommentsListApi.addData("last_id", diaryReplyInfo.getNew_reply_last_id());
        }
        mCommentsListApi.getCallBack(mContext, mCommentsListApi.getHashMap(), new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {
                mCommentsRefresh.finishRefresh();
                diaryReplyInfo = (DiaryReplyInfo) o;
                if (mPage == 1) {
                    //取我的评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getMy_reply() != null && diaryReplyInfo.getMy_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getMy_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getMy_reply().get(i).setType("0");
                            diaryReplyLists.add(diaryReplyInfo.getMy_reply().get(i));
                        }
                    }
                    //取最热评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getHot_reply() != null && diaryReplyInfo.getHot_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getHot_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getHot_reply().get(i).setType("1");
                            diaryReplyLists.add(diaryReplyInfo.getHot_reply().get(i));
                        }
                        //为了取出热门未展示部分数据
                        if (diaryReplyInfo.getNew_reply().size() > 3) {
                            for (int i = 3; i < diaryReplyInfo.getHot_reply().size(); i++) {
                                NotShowHotLists.add(diaryReplyInfo.getHot_reply().get(i));
                            }
                        }
                    }
                    //取最新评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getNew_reply() != null && diaryReplyInfo.getNew_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getNew_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getNew_reply().get(i).setType("2");
                            diaryReplyLists.add(diaryReplyInfo.getNew_reply().get(i));
                        }
                    }
                    if (diaryReplyLists.size() < Integer.parseInt(diaryReplyInfo.getReply_page_offect())) {
                        mCommentsRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mCommentsRefresh.finishLoadMore();
                    }
                    initCommentsList(diaryReplyLists);
                } else {
                    List<DiaryReplyList2> diaryReplyListsNew = new ArrayList<>();
                    //取最新评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getNew_reply() != null && diaryReplyInfo.getNew_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getNew_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getNew_reply().get(i).setType("2");
                            diaryReplyListsNew.add(diaryReplyInfo.getNew_reply().get(i));
                        }
                    }
                    if (diaryReplyListsNew.size() < Integer.parseInt(diaryReplyInfo.getReply_page_offect())) {
                        mCommentsRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mCommentsRefresh.finishLoadMore();
                    }
                    initCommentsList(diaryReplyListsNew);
                }
                mPage++;
            }
        });
    }

    private void initCommentsList(final List<DiaryReplyList2> diaryReplyLists) {
        if (mCommentsRecyclerAdapter2 == null) {
            mCommentsRecyclerAdapter2 = new CommentsRecyclerAdapter2(mContext, diaryReplyLists, Utils.dip2px(13), mAskorshare, id);
            mCommentsList.setAdapter(mCommentsRecyclerAdapter2);
            if (mCommentsRecyclerAdapter2.getHotItemCount().size() > 3) {
                int hideCount = mCommentsRecyclerAdapter2.getHotItemCount().size();
                //需要折叠评论
                for (int i = 0; i < hideCount - 3; i++) {
                    mCommentsRecyclerAdapter2.getmDatas().remove(NotShowHotLists.get(i));
                }
                mCommentsRecyclerAdapter2.notifyDataSetChanged();
            }
            mCommentsRecyclerAdapter2.setOnEventClickListener(new CommentsRecyclerAdapter2.OnEventClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                }

                @Override
                public void onItemLikeClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        pointAndReportLike(data);
                    }
                }

                @Override
                public void onItemReplyClick(View v, final int pos, String name) {
                    if (Utils.isLoginAndBind(mContext)) {
                        SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
                        sumbitPhotoData.setUserid(userId);
                        sumbitPhotoData.set_id(diaryReplyLists.get(pos).getUserdata().getId());
                        sumbitPhotoData.setQid(mDiaryId);
                        sumbitPhotoData.setCid(diaryReplyLists.get(pos).getId());
                        sumbitPhotoData.setAskorshare(mAskorshare);
                        sumbitPhotoData.setNickname(name);
                        mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
                        mDiaryCommentDialogView.showDialog();
                        mDiaryCommentDialogView.setPicturesChooseGone(true);

//                        sumbitPhotoData.setUserid("");
//                        sumbitPhotoData.set_id("");
//                        sumbitPhotoData.setQid("");
//                        sumbitPhotoData.setCid("");
//                        sumbitPhotoData.setAskorshare("");
//                        sumbitPhotoData.setNickname("");

                        //回复楼中楼提交完成回调
                        mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                            @Override
                            public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {

                                DiaryReplyListList diaryReplyListList = new DiaryReplyListList();
                                diaryReplyListList.setId(id);
                                diaryReplyListList.setContent(content);
                                diaryReplyListList.setTao(new DiaryReplyLisListTao());

                                //设置当前用户信息
                                DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                                diaryReplyListUserdata.setTalent("0");
                                diaryReplyListUserdata.setLable("");
//                                diaryReplyListUserdata.setName(mFunctionManager.loadStr(FinalConstant.UNAME, "悦美用户"));
                                diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                                diaryReplyListUserdata.setId(Utils.getUid());
//                                diaryReplyListUserdata.setAvatar(mFunctionManager.loadStr(FinalConstant.UHEADIMG, ""));
                                diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                                diaryReplyListList.setUserdata(diaryReplyListUserdata);

                                diaryReplyListList.setUserdata(diaryReplyListUserdata);

                                if (mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().size() == 0) {          //如果当前没有评论
                                    mCommentsRecyclerAdapter2.setCommentsPosReply(pos, diaryReplyListList);
                                } else {
                                    mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).addItem(diaryReplyListList);
                                }
                            }
                        });
                    }
                }

                @Override
                public void onItemDeleteClick(DiariesDeleteData deleteData) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit("确定要删除此评论吗?", deleteData);
                    }
                }

                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit1("确定要举报此内容?", data);
                    }
                }

                @Override
                public void onTaoAndDiaryClick(DiaryReplyListTao tao) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(tao.getUrl(), "0", "0");
                }

                @Override
                public void onCommentMoreClick(View v, int pos) {
                    //展开热门评论 循环添加
                    for (int i = 0; i < NotShowHotLists.size(); i++) {
                        mCommentsRecyclerAdapter2.addItem(NotShowHotLists.get(i), pos + 1 + i);
                    }
                    mCommentsRecyclerAdapter2.upDataOpenHotList("1");
                }
            });
        } else {
            mCommentsRecyclerAdapter2.setAddData(diaryReplyLists);
        }
    }

    /**
     * 评论
     */
    private void startComments() {
        SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
        sumbitPhotoData.setUserid(userId);
        sumbitPhotoData.setQid(mDiaryId);
        sumbitPhotoData.setAskorshare(mAskorshare);
        mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
        mDiaryCommentDialogView.showDialog();
        mDiaryCommentDialogView.setPicturesChooseGone(false);

        //评论提交完成回调
        mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
            @Override
            public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                //评论后的刷新
                DiaryReplyList2 diaryReplyList = new DiaryReplyList2();
                diaryReplyList.setId(id);
                diaryReplyList.setAgree_num("0");
                diaryReplyList.setContent(content);
                diaryReplyList.setList(new ArrayList<DiaryReplyListList>());
                diaryReplyList.setReply_num("0");
                diaryReplyList.setType("0");


//                diaryReplyList.setTao(new DiaryReplyListTao());


                //设置当前用户信息
                DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                diaryReplyListUserdata.setTalent("0");
                diaryReplyListUserdata.setLable("刚刚");
//                diaryReplyListUserdata.setName(mFunctionManager.loadStr(FinalConstant.UNAME, "悦美用户"));
                diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                diaryReplyListUserdata.setId(Utils.getUid());
                Log.e(TAG, "用户头像 === " + mFunctionManager.loadStr(FinalConstant.UHEADIMG, ""));
//                diaryReplyListUserdata.setAvatar(mFunctionManager.loadStr(FinalConstant.UHEADIMG, ""));
                diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                diaryReplyList.setUserdata(diaryReplyListUserdata);

                //设置评论图片
                List<DiaryReplyListPic> pics = new ArrayList<>();
                for (String s : imgLists) {
                    pics.add(new DiaryReplyListPic(s));
                }
                diaryReplyList.setPic(pics);
                mCommentsRecyclerAdapter2.addItem(diaryReplyList);
                mCommentsRecyclerAdapter2.notifyDataSetChanged();
                mCommentsList.scrollToPosition(0);
            }
        });
    }

    /**
     * 删除帖子 或 回复
     *
     * @param deleteData : 要删除的id（评论和回复）
     */
    private void deleteDiaries(DiariesDeleteData deleteData) {

        mDeleteBBsApi.getHashMap().put("id", deleteData.getId());
        mDeleteBBsApi.getHashMap().put("reply", deleteData.getReplystr());
        mDeleteBBsApi.getCallBack(mContext, mDeleteBBsApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String message) {
                mFunctionManager.showShort(message);
            }
        });
    }

    /**
     * 日记本点赞和举报请求接口
     *
     * @param data
     */
    private void pointAndReportLike(DiariesReportLikeData data) {
        final String flag = data.getFlag();
        final String isReply = data.getIs_reply();
        final int pos = data.getPos();
        mPointLikeApi.addData("flag", flag);
        mPointLikeApi.addData("id", data.getId());
        mPointLikeApi.addData("is_reply", isReply);
        mPointLikeApi.addData("puid", pId);
        mPointLikeApi.getCallBack(mContext, mPointLikeApi.getHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData data) {
                if (!data.isOtherCode) {
                    mFunctionManager.showShort(data.message);
                }
                if ("1".equals(flag)) {
                    if ("1".equals(data.code)) {

                        String is_agree = JSONUtil.resolveJson(data.data, "is_agree");

                        List<DiaryReplyList2> mDatas = mCommentsRecyclerAdapter2.getmDatas();
                        int agreeNum = Integer.parseInt(mDatas.get(pos).getAgree_num());
                        if ("1".equals(is_agree)) {
                            agreeNum++;
                        } else {
                            agreeNum--;
                        }
                        mDatas.get(pos).setIs_agree(is_agree);
                        mDatas.get(pos).setAgree_num(agreeNum + "");
                        mCommentsRecyclerAdapter2.notifyItemChanged(pos, "like");
                    }
                }
            }
        });
    }

    /**
     * 删除提示框
     *
     * @param content    ：提示框文案
     * @param deleteData ：要删除的id
     */
    private void showDialogExitEdit(String content, final DiariesDeleteData deleteData) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //删除
                deleteDiaries(deleteData);
                int commentsOrReply = deleteData.getCommentsOrReply();
                int pos = deleteData.getPos();
                int posPos = deleteData.getPosPos();
                if (commentsOrReply == 0) {
                    mCommentsRecyclerAdapter2.deleteItem(pos);
                } else {
                    mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).deleteItem(posPos);
                    if (mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).getmDatas().size() == 0) {
                        mCommentsRecyclerAdapter2.notifyItem(pos);
                    }
                }

                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

    }

    /**
     * 举报提示框
     *
     * @param content ：提示框文案
     * @param data    ：要举报的参数
     */
    private void showDialogExitEdit1(String content, final DiariesReportLikeData data) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //举报
                pointAndReportLike(data);

                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        mDiaryCommentDialogView.setIsPublic("私密", "1");
                    } else {
                        mDiaryCommentDialogView.setIsPublic("公开", "0");
                    }
                }
                break;
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mDiaryCommentDialogView.setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                        mDiaryCommentDialogView.gridviewInit();
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (mContext != null && !mContext.isFinishing()) {
            if (mDiaryCommentDialogView != null) {
                mDiaryCommentDialogView.dismiss();
            }

            if (editDialog != null) {
                editDialog.dismiss();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mCommentsRecyclerAdapter2 != null) {
            Intent intent = new Intent();
            intent.putExtra("number", mCommentsRecyclerAdapter2.getmDatas().size());
            setResult(RETURN_NUMBER, intent);
        }
        super.onBackPressed();
    }
}
