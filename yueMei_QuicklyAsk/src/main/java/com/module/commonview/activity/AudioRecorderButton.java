package com.module.commonview.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyask.activity.R;
import com.quicklyask.util.ExternalStorage;
import com.quicklyask.view.MyToast;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import static android.Manifest.permission.RECORD_AUDIO;

public class AudioRecorderButton extends AppCompatButton implements AudioManager.AudioStateListener {
    public static final String TAG = "AudioRecorderButton";
    //手指滑动 距离
    private static final int DISTANCE_Y_CANCEL = 50;
    //状态
    private static final int STATE_NORMAL = 1;
    private static final int STATE_RECORDING = 2;
    private static final int STATE_WANT_TO_CANCEL = 3;
    //当前状态
    private int mCurState = STATE_NORMAL;
    //已经开始录音
    private boolean isRecording = false;

    private DialogManager mDialogManager;
    private AudioManager mAudioManager;
    private Context mContext;
    private float mTime;
    private float mMaxTime = 60.0f; //录音最长时间60秒
    //是否触发onlongclick
    private boolean mReady;
    private String _id;//聊天对象id

    public void set_id(String _id) {
        this._id = _id;
    }

    public AudioRecorderButton(Context context) {
        this(context, null);
    }

    public AudioRecorderButton(final Context context, AttributeSet attrs) {
        super(context, attrs);
        mDialogManager = new DialogManager(getContext());
    }


    public void init(final Context context) {
        mContext = context;
        if (ExternalStorage.isSDCardMounted()) {
            int writePermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int audioPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
            if (writePermission == PackageManager.PERMISSION_GRANTED && audioPermission == PackageManager.PERMISSION_GRANTED) {
                String dir = Environment.getExternalStorageDirectory() + "/YueMei/voice/" + getYearAndMonth() + "/";
                Log.e(TAG, "dir == " + dir);
                initAudioManager(dir);
            } else {
                Acp.getInstance(context).request(new AcpOptions.Builder().setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, RECORD_AUDIO).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        String dir = Environment.getExternalStorageDirectory() + "/YueMei/voice/" + getYearAndMonth() + "/";
                        initAudioManager(dir);

                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        MyToast.makeTextToast2(context, "没有权限", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            MyToast.makeTextToast2(context, "无法访问储存卡", Toast.LENGTH_SHORT).show();
        }

    }


    private void initAudioManager(String dir) {
        mAudioManager = new AudioManager(dir);
        mAudioManager.setOnAudioStateListener(this);
        //按钮长按 准备录音 包括start
        setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.RECORD_AUDIO).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        mReady = true;
                        if (mAudioManager != null) {
                            mAudioManager.prepareAudio(_id);
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        MyToast.makeTextToast2(mContext, "没有语音权限", Toast.LENGTH_SHORT).show();
                    }
                });
                return false;
            }
        });
    }


    /**
     * 获取年和月
     *
     * @return
     */
    private String getYearAndMonth() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        StringBuilder filePath = new StringBuilder();
        filePath.append(year);
        filePath.append((month + 1));
        return filePath.toString();
    }

    /**
     * 录音完成后的回调
     */
    public interface AudioFinishRecorderListener {
        //时长  和 文件
        void onFinish(int seconds, String filePath);
    }

    private AudioFinishRecorderListener mListener;

    public void setAudioFinishRecorderListener(AudioFinishRecorderListener listener) {
        mListener = listener;
    }

    //获取音量大小的Runnable
    private Runnable mGetVoiceLevelRunnable = new Runnable() {
        @Override
        public void run() {
            while (isRecording) {
                try {
                    Thread.sleep(100);
                    mTime += 0.1;
                    if (mTime > mMaxTime) {
                        mHandler.sendEmptyMessage(MSG_DIALOG_TIMELONG);
                        Log.e(TAG, "mTime ===" + mTime);
                        return;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private static final int MSG_AUDIO_PREPARED = 0X110;
    private static final int MSG_VOICE_CHANGED = 0X111;
    private static final int MSG_DIALOG_DIMISS = 0X112;
    private static final int MSG_DIALOG_TIMELONG = 0X113;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_AUDIO_PREPARED:
                    //TODO 真正现实应该在audio end prepared以后
                    mDialogManager.showRecordingDialog();
                    isRecording = true;

                    new Thread(mGetVoiceLevelRunnable).start();
                    break;
                case MSG_VOICE_CHANGED:  //声音大小
//                    mDialogManager.updateVoiceLevel(mAudioManager.getVoiceLevel(7));

                    break;
                case MSG_DIALOG_DIMISS:
                    mDialogManager.dimissDialog();
                    break;
                case MSG_DIALOG_TIMELONG:
                    mDialogManager.dimissDialog();
                    mAudioManager.release();
                    if (mListener != null) {
                        mListener.onFinish((int) mTime, mAudioManager.getCurrentFilePath());
                    }
                    break;
            }
        }
    };

    @Override
    public void wellPrepared() {
        Log.e(TAG, "wellPrepared");
        mHandler.sendEmptyMessage(MSG_AUDIO_PREPARED);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "ACTION_DOWN");
                //TODO
                isRecording = true;
                changeState(STATE_RECORDING);

                break;
            case MotionEvent.ACTION_MOVE:
                Log.e(TAG, "ACTION_MOVE");

                if (isRecording) {
                    //根据想x,y的坐标，判断是否想要取消
                    if (wantToCancel(x, y)) {

                        changeState(STATE_WANT_TO_CANCEL);
                    } else {

                        changeState(STATE_RECORDING);

                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.e(TAG, "ACTION_UP");
                //如果longclick 没触发
                if (!mReady) {
                    reset();
                    return super.onTouchEvent(event);
                }
                //触发了onlongclick 没准备好，但是已经prepared 已经start
                //所以消除文件夹
                Log.e(TAG, "isRecording " + isRecording);
                Log.e(TAG, "mTime " + mTime);
                if (!isRecording || mTime < 1.0f) {
                    Log.e(TAG, "时间太短");
                    mDialogManager.tooShort();
                    mAudioManager.cancel();
                    mHandler.sendEmptyMessageDelayed(MSG_DIALOG_DIMISS, 1300);
                } else if (mCurState == STATE_RECORDING && mTime < mMaxTime) {//正常录制结束
                    Log.e(TAG, "正常录制结束");
                    mDialogManager.dimissDialog();
                    mAudioManager.release();
                    if (mListener != null) {
                        mListener.onFinish((int) mTime, mAudioManager.getCurrentFilePath());
                    }

                } else if (mCurState == STATE_WANT_TO_CANCEL) {
                    Log.e(TAG, "想要取消");
                    mDialogManager.dimissDialog();
                    mAudioManager.cancel();
                    //cancel
                }

                reset();

                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e(TAG, "ACTION_CANCEL");
                if (!mReady) {
                    reset();
                    return super.onTouchEvent(event);
                }
                //触发了onlongclick 没准备好，但是已经prepared 已经start
                //所以消除文件夹
                Log.e(TAG, "isRecording " + isRecording);
                Log.e(TAG, "mTime " + mTime);
                if (!isRecording || mTime < 1.0f) {
                    Log.e(TAG, "时间太短");
                    mDialogManager.tooShort();
                    mAudioManager.cancel();
                    mHandler.sendEmptyMessageDelayed(MSG_DIALOG_DIMISS, 1300);
                } else if (mCurState == STATE_RECORDING && mTime < mMaxTime) {//正常录制结束
                    Log.e(TAG, "正常录制结束");
                    mDialogManager.dimissDialog();
                    mAudioManager.release();
                    if (mListener != null) {
                        mListener.onFinish((int) mTime, mAudioManager.getCurrentFilePath());
                    }

                } else if (mCurState == STATE_WANT_TO_CANCEL) {
                    Log.e(TAG, "想要取消");
                    mDialogManager.dimissDialog();
                    mAudioManager.cancel();
                    //cancel
                }

                reset();
                break;

        }
        return super.onTouchEvent(event);
    }

    /**
     * 恢复状态 标志位
     */
    private void reset() {
        isRecording = false;
        mReady = false;
        changeState(STATE_NORMAL);
        mTime = 0;

    }

    private boolean wantToCancel(int x, int y) {
        //如果左右滑出 button
        if (x < 0 || x > getWidth()) {
            return true;
        }
        //如果上下滑出 button  加上我们自定义的距离
        if (y < -DISTANCE_Y_CANCEL || y > getHeight() + DISTANCE_Y_CANCEL) {
            return true;
        }
        return false;
    }

    //改变状态
    private void changeState(int state) {
        if (mCurState != state) {
            mCurState = state;
            switch (state) {
                case STATE_NORMAL:
                    setBackgroundResource(R.drawable.chat_input_shap);
                    setTextColor(ContextCompat.getColor(getContext(), R.color.ab));
                    setText(R.string.str_recorder_normal);
                    break;
                case STATE_RECORDING:
                    setBackgroundResource(R.drawable.chat_voice_btn);
                    setTextColor(ContextCompat.getColor(getContext(), R.color._66));
                    setText(R.string.str_recorder_recording);
                    if (isRecording) {
                        mDialogManager.recording();
                    }
                    break;
                case STATE_WANT_TO_CANCEL:
                    setBackgroundResource(R.drawable.chat_voice_btn);
                    setTextColor(ContextCompat.getColor(getContext(), R.color._66));
                    setText(R.string.str_recorder_want_cancel);
                    mDialogManager.wantToCancel();
                    break;
            }
        }
    }
}
