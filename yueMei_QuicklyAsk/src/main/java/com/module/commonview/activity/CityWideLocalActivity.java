package com.module.commonview.activity;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.commonview.view.CommonTopBar;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.ProjectDiaryAdapter;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * 同城页面
 */
public class CityWideLocalActivity extends YMBaseActivity {

    @BindView(R.id.city_wide_local_top)
    CommonTopBar mTop;
    @BindView(R.id.city_wide_local_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.city_wide_local_refresh)
    SmartRefreshLayout mRefresh;
    private String id;
    private BaseNetWorkCallBackApi samecitydiarylist;
    private int page = 1;
    private ProjectDiaryAdapter projectDiaryAdapter;
    private String TAG = "CityWideLocalActivity";
    private int mTempPos = -1;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_city_wide_local;
    }

    @Override
    protected void initView() {
        id = getIntent().getStringExtra("id");
        String title = getIntent().getStringExtra("title");
        mTop.setCenterText(title);

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                projectDiaryAdapter = null;
                page = 1;
                loadingData();
            }
        });
    }

    @Override
    protected void initData() {
        samecitydiarylist = new BaseNetWorkCallBackApi(FinalConstant1.FORUM, "samecitydiarylist");
        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        Log.e(TAG, "id == " + id);
        samecitydiarylist.addData("diary_id", id);
        samecitydiarylist.addData("page", page+"");
        samecitydiarylist.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    ArrayList<BBsListData550> datas = JSONUtil.jsonToArrayList(data.data, BBsListData550.class);

                    page++;
                    mRefresh.finishRefresh();
                    if (datas.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }


                    if (projectDiaryAdapter == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mRecycler.setLayoutManager(linearLayoutManager);

                        projectDiaryAdapter = new ProjectDiaryAdapter(mContext, datas);
                        mRecycler.setAdapter(projectDiaryAdapter);

                        projectDiaryAdapter.setOnItemPersonClickListener(new ProjectDiaryAdapter.OnItemPersonClickListener() {
                            @Override
                            public void onItemClick(int pos) {
                                mTempPos = pos;
                                String appmurl = projectDiaryAdapter.getDatas().get(pos).getAppmurl();
                                if (!TextUtils.isEmpty(appmurl)) {
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl);
                                }
                            }

                            @Override
                            public void onItemPersonClick(String id, int pos) {
                                mTempPos = pos;
                                Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                                intent.putExtra("id", id);
                                startActivityForResult(intent, 18);
                            }
                        });
                    } else {
                        projectDiaryAdapter.addData(datas);
                    }

                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTempPos >= 0) {
                isFocu(projectDiaryAdapter.getmHotIssues().get(mTempPos).getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                projectDiaryAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

}
