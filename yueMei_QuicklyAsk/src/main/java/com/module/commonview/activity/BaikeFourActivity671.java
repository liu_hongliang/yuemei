package com.module.commonview.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.adapter.BaikeFourPagerAdapter;
import com.module.commonview.fragment.BaikeAnswerFragment;
import com.module.commonview.fragment.BaikeDailyFragment;
import com.module.commonview.fragment.BaikeDocFragment;
import com.module.commonview.fragment.BaikeHosFragment;
import com.module.commonview.fragment.BaikePostFragment;
import com.module.commonview.fragment.BaikeTaoFragment;
import com.module.commonview.fragment.BaikeWebFragment671;
import com.module.commonview.module.api.BaikeFourApi;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.commonview.module.api.IsCollectApi;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.view.NoScrollViewPager1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 四级百科页
 * Created by 裴成浩 on 2019/4/29
 */
public class BaikeFourActivity671 extends YMBaseActivity {

    private String TAG = "BaikeFourActivity671";

    @BindView(R.id.baike_four_top)
    CommonTopBar mTop;
    @BindView(R.id.baike_four_latoyt)
    TabLayout mTabLatoyt;
    @BindView(R.id.baike_four_page)
    NoScrollViewPager1 mViewPager;

    private ArrayList<YMBaseFragment> mFragmentList = new ArrayList<>();

    private String id;
    private String parentId;
    private String name;
    private String shareUrl;
    private String shareTitle;
    private String fourUrl;
    private String shareContent;
    private String shareImgUrl;
    private int pageIndex;
    private VideoShareData videoShareData;
    private IsCollectApi mIsCollectApi;                 //判断是否收藏
    private BaikeFourApi mBaikeFourApi;                 //收藏
    private CancelCollectApi mCancelCollectApi;         //取消收藏
    private boolean isCollect = false;                  //是否收藏，默认没有收藏

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baike_four;
    }

    @Override
    protected void initView() {
        Intent it = getIntent();
        id = it.getStringExtra("id");
        parentId = it.getStringExtra("parentId");
        name = it.getStringExtra("name");
        shareUrl = it.getStringExtra("shareurl");
        shareTitle = it.getStringExtra("sharetitle");
        fourUrl = it.getStringExtra("url");
        shareContent = it.getStringExtra("sharecontent");
        shareImgUrl = it.getStringExtra("sharepic");
        String page_index = it.getStringExtra("page_index");
        Log.e(TAG, "page_index111==" + page_index);
        if (TextUtils.isEmpty(page_index)) {
            pageIndex = 0;
        } else {
            try {
                pageIndex = Integer.parseInt(page_index);
            } catch (NumberFormatException e) {
                pageIndex = 0;
            }
        }

        Log.e(TAG, "id==" + id);
        Log.e(TAG, "page_index==" + page_index);
        Log.e(TAG, "parentId==" + parentId);
        Log.e(TAG, "name==" + name);
        Log.e(TAG, "shareUrl==" + shareUrl);
        Log.e(TAG, "shareTitle==" + shareTitle);
        Log.e(TAG, "fourUrl==" + fourUrl);
        Log.e(TAG, "shareContent==" + shareContent);
        Log.e(TAG, "shareImgUrl==" + shareImgUrl);

        //设置标题
        mTop.setCenterText(name);

        videoShareData = new VideoShareData();
        videoShareData.setUrl(shareUrl);
        videoShareData.setTitle(shareTitle);
        videoShareData.setContent(shareContent);
        videoShareData.setImg_weibo(shareImgUrl);
        videoShareData.setImg_weix("");
        videoShareData.setAskorshare("");

        mFragmentList.add(BaikeWebFragment671.newInstance(id, fourUrl, videoShareData));                          //项目介绍
        mFragmentList.add(BaikeDailyFragment.newInstance(name, id, parentId));                                    //相关日记
        mFragmentList.add(BaikeAnswerFragment.newInstance(name, id, parentId));                                   //常见问题
        mFragmentList.add(BaikeTaoFragment.newInstance(name, id, parentId));                                      //相关商品
        mFragmentList.add(BaikeDocFragment.newInstance(name, id, parentId));                                      //相关医生
        mFragmentList.add(BaikeHosFragment.newInstance(name, id, parentId));                                      //相关医院
        mFragmentList.add(BaikePostFragment.newInstance(name, id, parentId));                                     //科普问答

        mViewPager.setScroll(false);
        BaikeFourPagerAdapter mPagerAdapter = new BaikeFourPagerAdapter(mContext, getSupportFragmentManager(), mFragmentList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLatoyt.setupWithViewPager(mViewPager);
        //默认选中项
        mTabLatoyt.getTabAt(pageIndex).select();

        //设置自定义的item
        for (int i = 0; i < mTabLatoyt.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLatoyt.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(mPagerAdapter.getTabView(i));
                if (tab.getCustomView() != null) {
                    View tabView = (View) tab.getCustomView().getParent();
                    tabView.setTag(i);
                }
            }
        }
    }

    @Override
    protected void initData() {
        mIsCollectApi = new IsCollectApi();
        mBaikeFourApi = new BaikeFourApi();
        mCancelCollectApi = new CancelCollectApi();
        setShareAndCollection();
        isCollectApi();
    }

    /**
     * 判断是否收藏
     */
    private void isCollectApi() {
        Map<String, Object> params = new HashMap<>();
        params.put("objid", id);
        params.put("type", "5");
        mIsCollectApi.getCallBack(mContext, params, new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String data) {
                setCollect("1".equals(data));
            }
        });
    }


    /**
     * 收藏接口
     */
    private void collectionApi() {
        Map<String, Object> params = new HashMap<>();
        params.put("objid", id);
        params.put("type", "5");

        mBaikeFourApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    Log.e(TAG, "收藏成功");
                    setCollect(true);
                } else {
                    Log.e(TAG, "收藏失败");
                    mFunctionManager.showShort(data.message);
                }
            }
        });
    }

    /**
     * 取消收藏
     */
    private void cancelCollection() {
        Map<String, Object> params = new HashMap<>();
        params.put("objid", id);
        params.put("type", "5");
        mCancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    Log.e(TAG, "取消收藏成功");
                    setCollect(false);
                } else {
                    Log.e(TAG, "取消收藏失败");
                    mFunctionManager.showShort(data.message);
                }
            }
        });
    }

    /**
     * 设置收藏
     *
     * @param is_collect
     */
    private void setCollect(boolean is_collect) {
        isCollect = is_collect;
        if (is_collect) {
            mTop.setRightImgSrc2(R.drawable.start_pink);
        } else {
            mTop.setRightImgSrc2(R.drawable.start_black);
        }

    }

    /**
     * 分享设置
     */
    private void setShareAndCollection() {

        //分享
        mTop.setRightImageClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                BaseShareView baseShareView = new BaseShareView(mContext);
                baseShareView
                        .setShareContent(shareContent)
                        .ShareAction();
                baseShareView.getShareBoardlistener()
                        .setSinaText(shareContent + " " + shareUrl)
                        .setSinaThumb(new UMImage(mContext, shareImgUrl))
                        .setSmsText(shareTitle + "，" + shareUrl)
                        .setTencentUrl(shareUrl)
                        .setTencentTitle(shareContent)
                        .setTencentThumb(new UMImage(mContext, shareImgUrl))
                        .setTencentDescription(shareContent)
                        .setTencentText(shareContent)
                        .getUmShareListener()
                        .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                            @Override
                            public void onShareResultClick(SHARE_MEDIA platform) {
                                if (!platform.equals(SHARE_MEDIA.SMS)) {
                                    mFunctionManager.showShort("分享成功");
                                }
                            }

                            @Override
                            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                                mFunctionManager.showShort("分享失败");

                            }
                        });
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BAIKE_SHARE_CLICK, "1"), hashMap, new ActivityTypeData("64"));

            }
        });

        //收藏
        mTop.setRightImageClickListener2(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (isCollect) {
                    cancelCollection();
                } else {
                    collectionApi();
                }
            }
        });


    }

}
