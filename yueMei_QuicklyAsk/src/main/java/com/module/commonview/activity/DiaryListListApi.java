package com.module.commonview.activity;

import android.content.Context;
import android.util.Log;

import com.google.gson.stream.JsonReader;
import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.bean.DiaryListListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.XinJsonReader;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryListListApi implements BaseCallBackApi {
    private String TAG = "DiaryListListApi";
    private HashMap<String, Object> hashMap;  //传值容器

    public DiaryListListApi() {
        hashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "diaryshare", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                try {
                    if ("1".equals(mData.code)) {

                        JsonReader jsonReader = new XinJsonReader(new StringReader(mData.data));
                        jsonReader.setLenient(true);

                        List<DiaryListListData> diaryListListData = JSONUtil.jsonToArrayList(jsonReader, DiaryListListData.class);

                        listener.onSuccess(diaryListListData);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }

            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }
}
