package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.http.SslError;
import android.os.Build;
import android.util.Log;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.MapButtomDialogView;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 医院位置
 *
 * @author Rubin
 */
public class MapHospitalWebActivity extends YMBaseActivity {

    private final String TAG = "MapHospitalWebActivity";

    private String hosid;
    private WebView mDocDetWeb;
    private BaseWebViewClientMessage mWebViewClientMessage;
    private MapButtomDialogView mapButtomDialogView;

    @Override
    protected int getLayoutId() {
        return R.layout.web_acty_map_hospital;
    }

    @Override
    protected void initView() {
        mWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        mapButtomDialogView = new MapButtomDialogView(mContext);

        hosid = getIntent().getStringExtra("hosid");
    }

    @Override
    protected void initData() {
        initWebview();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {

        mDocDetWeb = findViewById(R.id.docweb);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDocDetWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        WebSettings settings = mDocDetWeb.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);//支持通过JS打开新窗口
        mDocDetWeb.getSettings().setDatabaseEnabled(true);//开启数据库
        mDocDetWeb.setFocusable(true);//获取焦点
        mDocDetWeb.requestFocus();
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();//设置数据库路径
        mDocDetWeb.getSettings().setCacheMode(mDocDetWeb.getSettings().LOAD_CACHE_ELSE_NETWORK);//本地缓存
        mDocDetWeb.getSettings().setBuiltInZoomControls(true);
        mDocDetWeb.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mDocDetWeb.getSettings().setBlockNetworkImage(false);//显示网络图像
        mDocDetWeb.getSettings().setBlockNetworkLoads(false);
        mDocDetWeb.getSettings().setLoadsImagesAutomatically(true);//显示网络图像
        mDocDetWeb.getSettings().setPluginState(WebSettings.PluginState.ON);//插件支持
        mDocDetWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mDocDetWeb.getSettings().setLoadWithOverviewMode(true);// 缩放至屏幕的大小
        mDocDetWeb.getSettings().setUseWideViewPort(false);// 将图片调整到适合webview大小
        mDocDetWeb.getSettings().setBuiltInZoomControls(true);// 设置支持缩放
        mDocDetWeb.getSettings().setSupportZoom(false);//设置是否支持变焦
        mDocDetWeb.getSettings().setGeolocationEnabled(true);//定位

        mDocDetWeb.getSettings().setGeolocationDatabasePath(dir);//数据库
        mDocDetWeb.getSettings().setAllowFileAccess(true);

        mDocDetWeb.getSettings().setDomStorageEnabled(true);//缓存 （ 远程web数据的本地化存储）

        WebViewClient myWebViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    Log.e(TAG, "url == " + url);
                    if (url.startsWith("type")) {
                        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                        parserWebUrl.parserPagrms(url);
                        JSONObject obj = parserWebUrl.jsonObject;
                        String mType = obj.getString("type");
                        String name = obj.getString("name");
                        String lon = obj.getString("lon");
                        String lat = obj.getString("lat");
                        if ("6752".equals(mType)) {
                            mapButtomDialogView.showView(name, lon, lat);
                        } else {
                            mWebViewClientMessage.showWebDetail(url);
                        }
                    } else {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        };

        //建立对象
        mDocDetWeb.setWebViewClient(myWebViewClient);//调用
        mDocDetWeb.setWebChromeClient(new WebChromeClient() {

            //重写WebChromeClient的onGeolocationPermissionsShowPrompt
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("id", hosid);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(FinalConstant.HOSPITALMAP, keyValues);
        if (null != mDocDetWeb) {
            mDocDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders()); //百度地图地址
        }

    }

}
