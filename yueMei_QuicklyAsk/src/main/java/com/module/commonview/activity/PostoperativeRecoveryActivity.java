package com.module.commonview.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.jcodecraeer.xrecyclerview.CustomFooterViewCallBack;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.module.api.PostoperativeRecoveryApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.ListSkuTopAdapter;
import com.module.commonview.adapter.PostoperativeAdapter;
import com.module.commonview.module.api.PostoperativeRecoverySaveApi;
import com.module.commonview.module.bean.PostoperativeRecoverBean;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage2;
import com.quicklyask.util.Utils;
import com.quicklyask.util.XinJsonReader;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.StringReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class PostoperativeRecoveryActivity extends YMBaseActivity {

    @BindView(R.id.title_bar)
    CommonTopBar mTitleBar;  //标题
    @BindView(R.id.myself_postoperative_suck_top)
    RecyclerView mListSuckTop;                      //吸顶商品详情
    TextView mMyselfItemTitle;  //术前标题
    TextView mMyselfItemStatus;  //状态
    LinearLayout mPostoperativeFrontClick;  //正面点击
    RelativeLayout mPostoperativeFrontRel;  //正面布局
    ImageView mPostoperativePhotoFront;  //正面图片
    LinearLayout mPostoperativeSideClick;  //侧面点击
    RelativeLayout mPostoperativeSideRel;  //侧面点击
    ImageView mPostoperativeSidePhoto;//侧面图片
    LinearLayout mPostoperativeOtherClick;  //其他点击
    RelativeLayout mPostoperativeOtherRel;  //其他点击
    ImageView mPostoperativeOtherPhoto;//其他图片
    @BindView(R.id.myself_postoperative_recyclerview)
    XRecyclerView mMyselfPostoperativeRecyclerview;  //列表
    Button mPostoperativeDeleteFront;  //删除正面
    Button mPostoperativeDeleteSide;    //删除侧面
    Button mPostoperativeDeleteOther;   //删除其他

    LinearLayout mPostoperativeBeforeCover;
    LinearLayout mPostoperativeBeforeCover2;
    LinearLayout mPostoperativeBeforeCover3;
    private String mUid;
    private String mDiaryId;
    private boolean mIsMyself = false;
    private int page = 1;
    private ArrayList<String> mResults = new ArrayList<>();  //图片上传之后返回的本地路径单一的
    private ArrayList<String> mResultsMore = new ArrayList<>();  //图片上传之后返回的本地路好几张的
    HashMap<String, JSONObject> beforeData = new HashMap<>();            //图片上传完成后返回的图片地址集合。key是本地存储路径，vle是服务器连接
    //对比图Json串
    Map<String, Object> contrastData = new HashMap<>();
    public static final int FROM_GALLERY = 777;
    public static final int SUCCESS = 11;
    public static final int ERROR = 12;

    private String mPicId = "";
    private String mPicIdFront = "";
    private String mPicIdSide = "";
    private String mPicIdOther = "";
    private ArrayList<String> mResultData = new ArrayList<>();//从图片选择相册返回的结果集合
    ArrayList<PostoperativeRecoverBean.DataBean> dataBeans = new ArrayList<>(); //相册加载集合
    private PostoperativeAdapter mPostoperativeAdapter;
    private String mSurgeryafterdays;
    private String mBeforeImgUrl;
    private JSONObject mBeforeAdress;//服务器路径
    private JSONObject mAfterAdress;
    private String mBeforeUrl = "";//封面回传的图片路径
    private String mAfterUrl = "";
    private PostoperativeRecoverBean.DataBean mDataBean;
    private String mCompareUrl;
    private boolean mFontNew;
    private boolean mSideNew;
    private boolean mOtherNew;
    private View mHeader;
    private Activity mContext;
    private List<PostoperativeRecoverBean.TaoDataBean> mTaoData;
    private boolean isShowHead = true;
    private boolean isClickable = false;
    private int[] mImageWidthHeight;
    private String mKey;
    private String TAG = "PostoperativeRecoveryActivity";

    private Handler mHandler = new MyHandler(this);
    private static class MyHandler extends Handler {
        private final WeakReference<PostoperativeRecoveryActivity> mActivity;

        public MyHandler(PostoperativeRecoveryActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PostoperativeRecoveryActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case SUCCESS:
                        int pos = msg.arg1;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("width", theActivity.mImageWidthHeight[0]);
                            jsonObject.put("height", theActivity.mImageWidthHeight[1]);
                            jsonObject.put("img", theActivity.mKey);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        theActivity.beforeData.put(pos + 1 + "", jsonObject);            //设置要上传的集合值
                        theActivity.mFunctionManager.showShort("上传成功");
                        theActivity.mTitleBar.setRightTextEnabled(true);
                        theActivity.isClickable = true;

                        break;
                    case ERROR:
                        break;
                }
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_postoperative_recovery;
    }


    @Override
    protected void initView() {
        mContext = PostoperativeRecoveryActivity.this;
        mHeader = LayoutInflater.from(mContext).inflate(R.layout.postoperatice_head, (ViewGroup) findViewById(android.R.id.content), false);
        mMyselfItemTitle = mHeader.findViewById(R.id.myself_item_title);
        mMyselfItemStatus = mHeader.findViewById(R.id.myself_item_status);
        mPostoperativeFrontClick = mHeader.findViewById(R.id.postoperative_front_click);
        mPostoperativeFrontRel = mHeader.findViewById(R.id.postoperative_front_rel);
        mPostoperativePhotoFront = mHeader.findViewById(R.id.postoperative_photo_front);
        mPostoperativeSideClick = mHeader.findViewById(R.id.postoperative_side_click);
        mPostoperativeSideRel = mHeader.findViewById(R.id.postoperative_side_rel);
        mPostoperativeSidePhoto = mHeader.findViewById(R.id.postoperative_side_photo);
        mPostoperativeOtherClick = mHeader.findViewById(R.id.postoperative_other_click);
        mPostoperativeOtherRel = mHeader.findViewById(R.id.postoperative_other_rel);
        mPostoperativeOtherPhoto = mHeader.findViewById(R.id.postoperative_other_photo);
        mPostoperativeDeleteFront = mHeader.findViewById(R.id.postoperative_delete_front);
        mPostoperativeDeleteSide = mHeader.findViewById(R.id.postoperative_delete_side);
        mPostoperativeDeleteOther = mHeader.findViewById(R.id.postoperative_delete_other);
        mPostoperativeBeforeCover = mHeader.findViewById(R.id.postoperative_before_cover);
        mPostoperativeBeforeCover2 = mHeader.findViewById(R.id.postoperative_before_cover2);
        mPostoperativeBeforeCover3 = mHeader.findViewById(R.id.postoperative_before_cover3);
        setMultiOnClickListener(mPostoperativeFrontClick, mPostoperativeSideClick,
                mPostoperativeOtherClick, mPostoperativePhotoFront,
                mPostoperativeSidePhoto, mPostoperativeOtherPhoto,
                mPostoperativeDeleteFront, mPostoperativeDeleteSide, mPostoperativeDeleteOther);

        //返回按钮
        mTitleBar.setRightTextVisibility(View.VISIBLE);
        mTitleBar.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ExposureLoginData exposureLoginData = new ExposureLoginData("149", mDiaryId);
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.postoperative_front_click: //上传正面
                isClickable = false;
                if (mIsMyself) {
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce(0);
                                mFontNew = true;

                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce(0);
                        mFontNew = true;
                    }
                } else {
                    mPostoperativeFrontClick.setClickable(false);
                }
                break;
            case R.id.postoperative_side_click:  //上传侧面
                isClickable = false;
                if (mIsMyself) {
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce(1);
                                mSideNew = true;

                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce(1);
                        mSideNew = true;
                    }
                } else {
                    mPostoperativeSideClick.setClickable(false);
                }
                break;
            case R.id.postoperative_other_click:  //上传其他
                isClickable = false;
                if (mIsMyself) {
                    if (Build.VERSION.SDK_INT >= 23) {

                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                            @Override
                            public void onGranted() {
                                toXIangce(2);
                                mOtherNew = true;

                            }

                            @Override
                            public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        toXIangce(2);
                        mOtherNew = true;
                    }
                } else {
                    mPostoperativeOtherClick.setClickable(false);
                }
                break;
            case R.id.postoperative_delete_front: //删除正面
                mPostoperativeDeleteFront.setVisibility(View.GONE);
                mPostoperativePhotoFront.setVisibility(View.GONE);
                mPostoperativeFrontClick.setVisibility(View.VISIBLE);
                mPostoperativeFrontClick.setClickable(true);
                if (mPostoperativeBeforeCover.getVisibility() == View.VISIBLE) {
                    mPostoperativeBeforeCover.setVisibility(View.GONE);
                }
                if ("".equals(mPicId)) {
                    mPicId = mPicIdFront;
                } else {
                    if (mFontNew) {
                        mPicId = "";
                    } else {
                        mPicId = mPicId + "," + mPicIdFront;
                    }
                }
                if (mBeforeAdress != null) {
                    mBeforeAdress = null;
                }
                break;
            case R.id.postoperative_delete_side:  //删除侧面
                mPostoperativeDeleteSide.setVisibility(View.GONE);
                mPostoperativeSidePhoto.setVisibility(View.GONE);
                mPostoperativeSideClick.setVisibility(View.VISIBLE);
                mPostoperativeSideClick.setClickable(true);
                if (mPostoperativeBeforeCover2.getVisibility() == View.VISIBLE) {
                    mPostoperativeBeforeCover2.setVisibility(View.GONE);
                }
                if ("".equals(mPicId)) {
                    mPicId = mPicIdSide;
                } else {
                    if (mSideNew) {
                        mPicId = "";
                    } else {
                        mPicId = mPicId + "," + mPicIdSide;
                    }

                }
                if (mBeforeAdress != null) {
                    mBeforeAdress = null;
                }
                break;
            case R.id.postoperative_delete_other:  //删除其他
                mPostoperativeDeleteOther.setVisibility(View.GONE);
                mPostoperativeOtherPhoto.setVisibility(View.GONE);
                mPostoperativeOtherClick.setVisibility(View.VISIBLE);
                mPostoperativeOtherClick.setClickable(true);
                if (mPostoperativeBeforeCover3.getVisibility() == View.VISIBLE) {
                    mPostoperativeBeforeCover3.setVisibility(View.GONE);
                }
                if ("".equals(mPicId)) {
                    mPicId = mPicIdOther;
                } else {
                    if (mOtherNew) {
                        mPicId = "";
                    } else {
                        mPicId = mPicId + "," + mPicIdOther;
                    }

                }
                if (mBeforeAdress != null) {
                    mBeforeAdress = null;
                }
                break;
            case R.id.postoperative_photo_front:  //正面图片点击
                if (mIsMyself) {
                    mPostoperativeBeforeCover.setVisibility(View.VISIBLE);
                    mPostoperativeBeforeCover2.setVisibility(View.GONE);
                    mPostoperativeBeforeCover3.setVisibility(View.GONE);
                    if (mFontNew) {
                        mBeforeAdress = beforeData.get("1");
                        mBeforeUrl = mResults.get(0);
                    } else {
                        mBeforeUrl = mDataBean.getPic().get(0).getImg();
                        if (mBeforeAdress == null) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(0).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(0).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(0).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(0).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(0).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(0).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        }

                    }
                } else {
                    //点击跳大图
                    Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                    intent.putExtra("post_id", mDiaryId);
                    intent.putExtra("position", 0);
                    intent.putExtra("flag", 1);
                    startActivity(intent);
                }
                break;
            case R.id.postoperative_side_photo:  //侧面图片点击
                if (mIsMyself) {
                    int i = 0;
                    mPostoperativeBeforeCover.setVisibility(View.GONE);
                    mPostoperativeBeforeCover2.setVisibility(View.VISIBLE);
                    mPostoperativeBeforeCover3.setVisibility(View.GONE);
                    if (mSideNew) {
                        mBeforeAdress = beforeData.get("2");
                        mBeforeUrl = mResults.get(0);
                    } else {
                        if (mDataBean.getPic().size() == 1) {
                            i = 0;
                        } else {
                            i = 1;
                        }
                        mBeforeUrl = mDataBean.getPic().get(i).getImg();
                        if (mBeforeAdress == null) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(i).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(i).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(i).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(i).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(i).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(i).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        }
                    }
                } else {
                    //点击跳大图
                    Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                    intent.putExtra("post_id", mDiaryId);
                    intent.putExtra("position", 1);
                    intent.putExtra("flag", 1);
                    startActivity(intent);
                }
                break;
            case R.id.postoperative_other_photo:  //其他图片点击
                if (mIsMyself) {
                    int i = 0;
                    mPostoperativeBeforeCover.setVisibility(View.GONE);
                    mPostoperativeBeforeCover2.setVisibility(View.GONE);
                    mPostoperativeBeforeCover3.setVisibility(View.VISIBLE);
                    if (mOtherNew) {
                        mBeforeAdress = beforeData.get("3");
                        mBeforeUrl = mResults.get(0);
                    } else {
                        if (mDataBean.getPic().size() == 1) {
                            i = 0;
                        } else if (mDataBean.getPic().size() == 2) {
                            i = 1;
                        } else {
                            i = 2;
                        }
                        mBeforeUrl = mDataBean.getPic().get(i).getImg();
                        if (mBeforeAdress == null) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(i).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(i).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(i).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(i).getWidth()));
                                jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(i).getHeight()));
                                jsonObject.put("img", mDataBean.getPic().get(i).getImages());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mBeforeAdress = jsonObject;
                        }
                    }
                } else {
                    //点击跳大图
                    Intent intent = new Intent(mContext, DiaryPhotoBrowsingActivity.class);
                    intent.putExtra("post_id", mDiaryId);
                    intent.putExtra("position", 2);
                    intent.putExtra("flag", 1);
                    startActivity(intent);
                }
                break;
        }
    }

    void toXIangce(int pos) {
        Log.e(TAG, "mResults === " + mResults.size());
        // 启动多个图片选择器
        Intent intent = new Intent(mContext, ImagesSelectorActivity.class);
        // 要选择的图像的最大数量
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
        // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
        // 显示摄像机或不
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
        intent.putExtra("pos", pos);
        // 开始选择器
        startActivityForResult(intent, FROM_GALLERY);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initData() {
        mUid = Utils.getUid();
        mDiaryId = getIntent().getStringExtra("diary_id");
        Log.e(TAG, "mDiaryId == " + mDiaryId);
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("id", mDiaryId);
        maps.put("page", page + "");
        new PostoperativeRecoveryApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    try {
                        JsonReader jsonReader = new XinJsonReader(new StringReader(s.data));
                        jsonReader.setLenient(true);
                        PostoperativeRecoverBean postoperativeRecoverBean = JSONUtil.TransformSingleBean(jsonReader, PostoperativeRecoverBean.class);
                        List<PostoperativeRecoverBean.DataBean> data = postoperativeRecoverBean.getData();
                        mTaoData = postoperativeRecoverBean.getTaoData();
                        mDataBean = data.get(0);
                        for (int i = 1; i < data.size(); i++) {
                            dataBeans.add(data.get(i));
                        }
                        Log.d(TAG, "size---->" + dataBeans.size());
                        mMyselfItemTitle.setText(mDataBean.getTitle());
                        String user_id = mDataBean.getUser_id();
                        if (mUid.equals(user_id)) {  //作者自己
                            mMyselfItemStatus.setVisibility(View.VISIBLE);
                            mIsMyself = true;
                            mTitleBar.setRightText("保存");
                            mTitleBar.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
                                @Override
                                public void onClick(View v) {

                                    if (mResults.size() > 0) {
                                        if (isClickable) {
                                            uploadSaveData();
                                            isClickable = false;
                                        } else {
                                            mFunctionManager.showShort("图片上传中...");
                                        }


                                    } else {
                                        if (!"".equals(mPicId)) {
                                            uploadSaveData();
                                            return;
                                        }
                                        if (mBeforeAdress == null && mAfterAdress == null) {
                                            mFunctionManager.showShort("请选择一张封面");
                                        } else {
                                            uploadSaveData();
                                        }
                                    }


                                }
                            });

                        }
                        String b_img1 = "";
                        String b_img2 = "";
                        String b_img3 = "";
                        String b_img1_id = "";
                        String b_img2_id = "";
                        String b_img3_id = "";
                        if (mDataBean.getPic().size() > 0) {
                            for (int i = 0; i < mDataBean.getPic().size(); i++) {
                                if ("1".equals(mDataBean.getPic().get(i).getWeight())) {
                                    b_img1 = mDataBean.getPic().get(i).getImg();
                                    b_img1_id = mDataBean.getPic().get(i).getPic_id();
                                }
                                if ("2".equals(mDataBean.getPic().get(i).getWeight())) {
                                    b_img2 = mDataBean.getPic().get(i).getImg();
                                    b_img2_id = mDataBean.getPic().get(i).getPic_id();
                                }
                                if ("3".equals(mDataBean.getPic().get(i).getWeight())) {
                                    b_img3 = mDataBean.getPic().get(i).getImg();
                                    b_img3_id = mDataBean.getPic().get(i).getPic_id();
                                }
                            }
                        } else {
                            if (!mIsMyself) {
                                isShowHead = false;
                            } else {

                            }
                        }
                        Log.e(TAG, "b_img1==" + b_img1);
                        Log.e(TAG, "b_img2==" + b_img2);
                        Log.e(TAG, "b_img3==" + b_img3);
                        if (!TextUtils.isEmpty(b_img1)) {
                            mPostoperativeFrontRel.setVisibility(View.VISIBLE);
                            mPostoperativeFrontClick.setVisibility(View.GONE);
                            mPostoperativePhotoFront.setVisibility(View.VISIBLE);
                            Glide.with(PostoperativeRecoveryActivity.this)
                                    .load(b_img1)
                                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                                    .into(mPostoperativePhotoFront);
                            mPicIdFront = b_img1_id;
                            if (mIsMyself) {
                                mPostoperativeDeleteFront.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mPostoperativeFrontRel.setVisibility(View.VISIBLE);
                        }
                        if (!TextUtils.isEmpty(b_img2)) {
                            mPostoperativeSideRel.setVisibility(View.VISIBLE);
                            mPostoperativeSideClick.setVisibility(View.GONE);
                            mPostoperativeSidePhoto.setVisibility(View.VISIBLE);
                            Glide.with(PostoperativeRecoveryActivity.this)
                                    .load(b_img2)
                                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                                    .into(mPostoperativeSidePhoto);
                            mPicIdSide = b_img2_id;
                            if (mIsMyself) {
                                mPostoperativeDeleteSide.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mPostoperativeSideRel.setVisibility(View.VISIBLE);
                        }
                        if (!TextUtils.isEmpty(b_img3)) {
                            mPostoperativeOtherRel.setVisibility(View.VISIBLE);
                            mPostoperativeOtherClick.setVisibility(View.GONE);
                            mPostoperativeOtherPhoto.setVisibility(View.VISIBLE);
                            Glide.with(PostoperativeRecoveryActivity.this)
                                    .load(b_img3)
                                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                                    .into(mPostoperativeOtherPhoto);
                            mPicIdOther = b_img3_id;
                            if (mIsMyself) {
                                mPostoperativeDeleteOther.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mPostoperativeOtherRel.setVisibility(View.VISIBLE);
                        }

                        //初始化吸顶数据
                        if (mTaoData != null && mTaoData.size() > 0) {
                            LinearLayoutManager linearManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                            mListSuckTop.setLayoutManager(linearManager);
                            ListSkuTopAdapter listSuckTopRecyclerAdapter = new ListSkuTopAdapter(mContext, mTaoData, mDiaryId);
                            mListSuckTop.setAdapter(listSuckTopRecyclerAdapter);
                            listSuckTopRecyclerAdapter.setOnItemCallBackListener(new ListSkuTopAdapter.ItemCallBackListener() {
                                @Override
                                public void onItemClick(View v, int pos) {

                                    String tao_id = mTaoData.get(pos).getId();
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("id", mDiaryId);
                                    hashMap.put("to_page_type", "2");
                                    hashMap.put("to_page_id", tao_id);
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ALBUM_TOP_TAO, "top"), hashMap);

                                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                                    intent.putExtra("id", tao_id);
                                    intent.putExtra("source", "0");
                                    intent.putExtra("objid", "0");
                                    startActivity(intent);

                                }
                            });

                        }
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
                        mMyselfPostoperativeRecyclerview.setHasFixedSize(true);
                        mMyselfPostoperativeRecyclerview.setNestedScrollingEnabled(false);
                        mMyselfPostoperativeRecyclerview.setFocusableInTouchMode(false);
                        mMyselfPostoperativeRecyclerview.setLayoutManager(linearLayoutManager);

                        if (isShowHead) {
                            mMyselfPostoperativeRecyclerview.addHeaderView(mHeader);
                        }

                        mPostoperativeAdapter = new PostoperativeAdapter(PostoperativeRecoveryActivity.this, dataBeans, mIsMyself);
                        mMyselfPostoperativeRecyclerview.setPullRefreshEnabled(false);
                        mMyselfPostoperativeRecyclerview.setLoadingListener(new XRecyclerView.LoadingListener() {
                            @Override
                            public void onRefresh() {

                            }

                            @Override
                            public void onLoadMore() {
                                page++;
                                HashMap<String, Object> maps = new HashMap<>();
                                maps.put("id", mDiaryId);
                                maps.put("page", page + "");
                                new PostoperativeRecoveryApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serverData) {
                                        JsonReader jsonReader = new XinJsonReader(new StringReader(serverData.data));
                                        jsonReader.setLenient(true);
                                        PostoperativeRecoverBean postoperativeRecoverBean = null;
                                        try {
                                            postoperativeRecoverBean = JSONUtil.TransformSingleBean(jsonReader, PostoperativeRecoverBean.class);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        List<PostoperativeRecoverBean.DataBean> data = postoperativeRecoverBean.getData();
                                        dataBeans.addAll(data);
                                        mPostoperativeAdapter.notifyDataSetChanged();
                                        mMyselfPostoperativeRecyclerview.loadMoreComplete();
                                        if (data.size() == 0) {
                                            mMyselfPostoperativeRecyclerview.setNoMore(true);
                                        }
                                    }
                                });
                            }
                        });
                        View footView = LayoutInflater.from(mContext).inflate(R.layout.psotopertive_footerview, (ViewGroup) findViewById(android.R.id.content), false);
                        final TextView footText = footView.findViewById(R.id.foot_text);
                        final View view1 = footView.findViewById(R.id.view1);
                        final View view2 = footView.findViewById(R.id.view2);
                        mMyselfPostoperativeRecyclerview.setFootView(footView, new CustomFooterViewCallBack() {
                            @Override
                            public void onLoadingMore(View yourFooterView) {
                                view1.setVisibility(View.GONE);
                                view2.setVisibility(View.GONE);
                                footText.setText("加载中...");
                            }

                            @Override
                            public void onLoadMoreComplete(View yourFooterView) {
                            }

                            @Override
                            public void onSetNoMore(View yourFooterView, boolean noMore) {
                                view1.setVisibility(View.VISIBLE);
                                view2.setVisibility(View.VISIBLE);
                                footText.setText("这是美的底线");
                            }
                        });
                        mMyselfPostoperativeRecyclerview.setAdapter(mPostoperativeAdapter);
                        mPostoperativeAdapter.setItemCallBack(new PostoperativeAdapter.ItemCallBack() {
                            @Override
                            public void onItemCallBack(String surgeryafterdays, String images, String url, String width, String height) {
                                mSurgeryafterdays = surgeryafterdays;
                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("width", Integer.parseInt(width));
                                    jsonObject.put("height", Integer.parseInt(height));
                                    jsonObject.put("img", images);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                mAfterAdress = jsonObject;
                                mAfterUrl = url;
                                Log.d(TAG, "onItemCallBack=====>" + mSurgeryafterdays + ":" + mAfterAdress);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    mFunctionManager.showShort(s.message);
                }
            }
        });

        mMyselfPostoperativeRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int totalDy = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalDy += dy;
                if (mTaoData != null && mTaoData.size() > 0) {
                    if (totalDy > 70) {
                        mListSuckTop.setVisibility(View.VISIBLE);
                    } else {
                        mListSuckTop.setVisibility(View.GONE);
                    }
                }
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        int pos = data.getIntExtra("pos", 0);
                        mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                        Log.e(TAG, "onActivityResult  mResults === " + mResults.size());
                        if (mResults != null && mResults.size() > 0) {
                            mResultsMore.add(mResults.get(0));
                            upLoadFile(pos);
                            isClickable = false;
                            mTitleBar.setRightTextEnabled(false);

                        }
                    }
                }
                break;
        }
    }


    void upLoadFile(int pos) {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults.get(0), desPath);
        Bitmap bitmap = BitmapFactory.decodeFile(desPath);
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        switch (pos) {
            case 0:
                mPostoperativePhotoFront.setVisibility(View.VISIBLE);
                mPostoperativePhotoFront.setImageBitmap(bitmap);
                mPostoperativeFrontClick.setVisibility(View.GONE);
                mPostoperativeDeleteFront.setVisibility(View.VISIBLE);
                break;
            case 1:
                mPostoperativeSidePhoto.setVisibility(View.VISIBLE);
                mPostoperativeSidePhoto.setImageBitmap(bitmap);
                mPostoperativeSideClick.setVisibility(View.GONE);
                mPostoperativeDeleteSide.setVisibility(View.VISIBLE);
                break;
            case 2:
                mPostoperativeOtherPhoto.setVisibility(View.VISIBLE);
                mPostoperativeOtherPhoto.setImageBitmap(bitmap);
                mPostoperativeOtherClick.setVisibility(View.GONE);
                mPostoperativeDeleteOther.setVisibility(View.VISIBLE);
                break;
        }
        mResultData.add(mResults.get(0));
        isClickable = false;
        mTitleBar.setRightTextEnabled(false);
        mKey = QiNuConfig.getKey();
        MyUploadImage2.getMyUploadImage(mContext, pos, mHandler, desPath).uploadImage(mKey);
    }

    void uploadSaveData() {
        mTitleBar.setRightText("提交中");
        mTitleBar.setRightTextEnabled(false);
        try {
            mBeforeImgUrl = JSONUtil.toJSONString2(beforeData);
            Log.e(TAG, "beforeimage =======> " + mBeforeImgUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!"".equals(mBeforeAdress)) {
            contrastData.put("before", mBeforeAdress);        //术前封面
        } else {
            if (mFontNew) {
                mBeforeAdress = beforeData.get(1);
            } else {
                if (mDataBean.getPic().get(0).getImages() != null) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("width", Integer.parseInt(mDataBean.getPic().get(0).getWidth()));
                        jsonObject.put("height", Integer.parseInt(mDataBean.getPic().get(0).getHeight()));
                        jsonObject.put("img", mDataBean.getPic().get(0).getImages());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mBeforeAdress = jsonObject;
                }
            }
            if (mBeforeAdress != null) {
                contrastData.put("before", mBeforeAdress);        //术前封面
            }
        }
        if (mAfterAdress != null) {
            contrastData.put("after", mAfterAdress);        //术后封面
        }
        Log.e(TAG, "before =======> " + mBeforeAdress);
        Log.e(TAG, "after =======> " + mAfterAdress);
        try {
            mCompareUrl = JSONUtil.toJSONString(contrastData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //提交参数
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("id", mDiaryId);
        maps.put("pic_id", mPicId);
        Log.e(TAG, "pic_id--" + mPicId);
        mPicId = "";

        if (mSurgeryafterdays != null) {
            maps.put("surgeryafterdays", mSurgeryafterdays);
            Log.e(TAG, "surgeryafterdays--" + mSurgeryafterdays);
        }
        if (contrastData.size() > 0) {
            maps.put("compare", mCompareUrl);
            Log.e(TAG, "compare--" + mCompareUrl);
        }
        if (!"{}".equals(mBeforeImgUrl)) {
            maps.put("beforeimage", mBeforeImgUrl);
            Log.e(TAG, "beforeimage--" + mBeforeImgUrl);
        }

        new PostoperativeRecoverySaveApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                try {
                    Log.e(TAG, "serverData == " + serverData.toString());
                    mFunctionManager.showShort(serverData.message);
                    if ("1".equals(serverData.code)) {
                        List<PostoperativeRecoverBean.DataBean.PicBean> pic = mDataBean.getPic();
                        Log.e(TAG, "pic.size() == " + pic.size());

                        List<String> keys = new ArrayList<>(beforeData.keySet());
                        for (int i = 0; i < beforeData.size(); i++) {
                            String key = keys.get(i);
                            JSONObject value = beforeData.get(key);
                            Log.e(TAG, "value === " + value);

                            PostoperativeRecoverBean.DataBean.PicBean picBean = new PostoperativeRecoverBean.DataBean.PicBean();
                            picBean.setPic_id("0");
                            picBean.setWidth(value.getString("width"));
                            picBean.setHeight(value.getString("height"));
                            picBean.setImg(value.getString("img"));
                            picBean.setIs_video("0");
                            picBean.setWeight(key);

                            mDataBean.getPic().add(picBean);
                        }

                        mTitleBar.setRightText("保存");
                        onBackPressed();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
//        List<PostoperativeRecoverBean.DataBean.PicBean> pic = mDataBean.getPic();
//        if (pic != null && pic.size() > 0){
//            if ("".equals(mBeforeUrl)){     ////用户什么都没做不调次方法
//                for (int i = 0; i <pic.size() ; i++) {
//                    mBeforeUrl=pic.get(i).getImg();
//                    if (!TextUtils.isEmpty(mBeforeUrl)){
//                        Log.e(TAG,"mBeforeUrl ==>"+mBeforeUrl);
//                        if (mBeforeUrl.contains("https:")){
//                            break;
//                        }else {
//                            if (mResultsMore.size() > 0){
//                                mBeforeUrl = mResultsMore.get(0);
//                                Log.e(TAG,"mBeforeUrl ==>"+mBeforeUrl);
//                            }
//                        }
//
//                    }
//
//                }
//            }
//        }else {
//            if (mResultsMore.size() > 0){
//                mBeforeUrl = mResultsMore.get(0);
//                Log.e(TAG,"mBeforeUrl ==>"+mBeforeUrl);
//            }
//
//        }
        intent.putExtra("before", mBeforeUrl);
        intent.putExtra("after", mAfterUrl);
        Log.e(TAG,"before ==" +mBeforeUrl);
        Log.e(TAG,"after ==" +mAfterUrl);
        setResult(100, intent);
        super.onBackPressed();

    }


    @Override
    protected void onDestroy() {
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();
    }
}
