/**
 * 
 */
package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 医院服务 淘整形展示
 * 
 * @author Rubin
 * 
 */
public class HosServiceActivity extends BaseActivity {

	private final String TAG = "HosCommentActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview4, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.hos_service_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private HosServiceActivity mContex;

	public JSONObject obj_http;
	private String url;
	private BaseWebViewClientMessage baseWebViewClientMessage;
	private String mFlag;
	private String mHosid;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_hos_service);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = HosServiceActivity.this;

		mTop.setCenterText("医院相册");

		Intent it = getIntent();
		url = it.getStringExtra("url");
		if (!TextUtils.isEmpty(url)){
			String[] split = url.split("/");
			mHosid = split[4];
			mFlag = split[6];
		}
		url = FinalConstant.baseUrl + FinalConstant.VER + url;
		scollwebView.GetLinearLayout(contentWeb);
		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		initWebview();

		LodUrl1(url);

		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						HosServiceActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}


	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}


	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();
		HashMap<String, Object> parms = new HashMap<>();
		parms.put("hosid",mHosid);
		parms.put("flag",mFlag);
		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr,parms);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
