package com.module.commonview.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cookie.store.CookieStore;
import com.module.MyApplication;
import com.module.SplashActivity;
import com.module.api.ChatMessApi;
import com.module.api.TaoDetailShow;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.ChatRecyclerAdapter;
import com.module.commonview.adapter.ChatTipListAdapter;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.module.api.ChatBackApi;
import com.module.commonview.module.api.ChatBackSuccessApi;
import com.module.commonview.module.api.ChatInterfaceApi;
import com.module.commonview.module.api.ChatOnFocusApi;
import com.module.commonview.module.api.ChatSendMessageApi;
import com.module.commonview.module.api.SendTipsApi;
import com.module.commonview.module.bean.ChatBackBean;
import com.module.commonview.module.bean.ChatBackSuccessBean;
import com.module.commonview.module.bean.ChatInterfaceBean;
import com.module.commonview.module.bean.ChatShowLogin;
import com.module.commonview.module.bean.ChatTopListAdapter;
import com.module.commonview.module.bean.ContinueToSend;
import com.module.commonview.module.bean.MessageBean;
import com.module.commonview.module.bean.QuickQuestion;
import com.module.commonview.module.bean.QuickReply;
import com.module.commonview.module.bean.QuickReplyTop;
import com.module.commonview.module.bean.VoiceMessage;
import com.module.commonview.module.bean.WebSocketBean;
import com.module.commonview.module.other.MessageCallBack;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.ChatBackGetPop;
import com.module.commonview.view.ChatBackPopwindow;
import com.module.commonview.view.ChatBottomView;
import com.module.commonview.view.ChatTuiJianView;
import com.module.commonview.view.ExpressionKeyboardPopupwindows;
import com.module.commonview.view.HeadIconSelectorView;
import com.module.commonview.view.PullLoadMoreRecyclerView;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.view.CalendarReminderDialog;
import com.module.community.web.WebViewUrlLoading;
import com.module.community.web.WebViewUrlUtil;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.model.api.XiaoxiChatUpdatereadApi;
import com.module.my.model.api.JPushClosedApi;
import com.module.my.view.RewardAlertToast;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qiniu.android.storage.Recorder;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.FileSaveUtil;
import com.quicklyask.util.ImageCheckoutUtil;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.KeyBoardUtils;
import com.quicklyask.util.PictureUtil;
import com.quicklyask.util.SoftKeyBoardListener;
import com.quicklyask.util.Utils;
import com.quicklyask.util.VideoUploadUpyun;
import com.quicklyask.view.MyToast;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.ui.ViewInject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ChatActivity extends YMBaseActivity implements MessageCallBack {
    public static final String TAG = "ChatActivity";
    @BindView(R.id.chats_back)
    RelativeLayout chatsBack;   //返回点击
    @BindView(R.id.default_background)
    LinearLayout mDefaultBackground;   //返回点击
    @BindView(R.id.chat_back_img)
    ImageView mDefaultBackImg;   //返回图片
    @BindView(R.id.chat_hospital_img)
    ImageView mHospitalImg;   //医院图片
    @BindView(R.id.chats_title_tv)
    TextView chatsTitleTv;      //标题
    @BindView(R.id.chats_icon)
    ImageView mChatsIcon;        //医院标签
    @BindView(R.id.dacu_backgroud)
    ImageView mChatDacuBackground;        //大促背景
    @BindView(R.id.chat_expand_cantainer)
    FrameLayout mChatExpandClick;
    @BindView(R.id.expand_img)
    ImageView mChatExpandImg;
    @BindView(R.id.chats_hospital_iv)
    RelativeLayout mChatsHospital;     //医院按钮
    @BindView(R.id.content_lv)
    PullLoadMoreRecyclerView pullList; //消息列表控件
    @BindView(R.id.chat_tip_container)
    LinearLayout mChatTipContainer;      //私信提示语容器
    @BindView(R.id.voice)
    ImageView mVoice;        //语音
    @BindView(R.id.voice_btn)
    AudioRecorderButton mRecorderButton;        //语音按钮
    @BindView(R.id.mess_et)
    EditText mEditTextContent;        //输入框
    @BindView(R.id.emoji)
    ImageView emoji;        //表情
    @BindView(R.id.mess_iv)
    ImageView mess_iv;       //加号
    @BindView(R.id.mess_send)
    Button mSend;        //发送按钮
    @BindView(R.id.tongbao_utils)
    LinearLayout tongbaoUtils;
    @BindView(R.id.other_lv)
    ChatBottomView tbbv;     //功能框
    @BindView(R.id.keyboard_content)
    LinearLayout mKeyboardcontent;       //装键盘的容器
    @BindView(R.id.reply_container)
    LinearLayout mReplyContainer;       //装键盘的容器
    @BindView(R.id.bottom_container_ll)
    LinearLayout bottomContainerLl;
    @BindView(R.id.layout_tongbao_rl)
    FrameLayout activityRootView;     //整个容器
    @BindView(R.id.chat_tiplist)
    RecyclerView chatTipList;     //消息提示
    @BindView(R.id.quick_reply_top_visorgone)
    LinearLayout quickReplyTopVisorgone;     //顶部提示
    @BindView(R.id.quick_reply_top_list)
    RecyclerView quickReplyTopList;     //顶部列表
    private Activity mContext;
    private String replace;
    private String domain = "chat.yuemei.com";
    private long expiresAt = 1544493729973L;
    private String name = "";
    private String path = "/";
    private String value = "";
    private ExpressionKeyboardPopupwindows instance;
    private String mDirectId;
    private String mObjId = "0";
    private String mObjType = "0";
    private String mHos_name;
    private String mTitle;
    private String mHos_id;
    private String mGroup_id;
    private String mId;  //聊天对象id
    private String mYm_calss = "0";
    private String mYm_id = "0";
    private String mSkuId = "0";
    private String mZTtitle = "";
    private String mZTid = "0";
    private String mRegistrationID;
    private String title = "";
    private String mPrice = "";
    private String mImg = "";
    private String mUrl = "";
    private String mPlus = "";
    private String uid;
    private String mImei;
    private String mDateTime;
    private ChatRecyclerAdapter tbAdapter;
    private List<QuickReply> mQuick_reply;
    private boolean isSelect = true;
    public int position; //加载滚动刷新位置
    private String page = "2"; //页数
    int i = 2;
    private ChatBackBean chatBackBean;
    private String camPicPath;
    private static final int IMAGE_SIZE = 1000 * 1024;// 1M
    private MessageBean.DataBean mDataBean;
    private ChatInterfaceBean mInterfaceBean;
    private boolean flag = false;
    private static IdCallBack mIdCallBack;
    private ArrayList<MessageBean.DataBean> mDataList = new ArrayList<>();
    public List<MessageBean.DataBean> tblist = new ArrayList<>();

    public List<MessageBean.DataBean> pagelist = new ArrayList<>();

    private ArrayList<String> mResults = new ArrayList<>();
    public ArrayList<String> imageList = new ArrayList<>();//adapter图片数据
    public HashMap<Integer, Integer> imagePosition = new HashMap<>();//图片下标位置
    String classid = ""; //消息类型
    private boolean isValidMessage = false;
    private ChatBackGetPop mChatBackGetPop;
    private ChatBackSuccessBean mSuccessBean;
    private boolean isVoice = true;//语音还是键盘
    private int voiceTime = 0;
    private String pathFile = "";
    private WriteVideoResult voiceResult; //又拍云上传返回路径
    private File errorFile = null; //又拍云上传失败的文件路径
    private AudioSensorBinder mAudioSensorBinder;
    private int errorNum = 0;
    private boolean isExpand = false;
    private String mOnline; //客服在线状态 1在线 0 不在线
    private String mOnFoucusStr = "";//输入框正在输入的字
    private boolean mHasFocus;
    private boolean isFlag = false;
    private String mClassid;
    private String mQuickReplyContent;
    private String mQuickReply;
    private String mNoLoginId;

    /**
     * objtype 1 专题 2 淘整形 3医生 4医院 5帖子
     * classid 1 文字 2图片 3 我正在看(右边) 6医院发的我正在看(左边) 8红包
     */
    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat;
    }

    @Override
    protected void initView() {
        mContext = ChatActivity.this;
        pullList.setPushRefreshEnable(false);
        instance = new ExpressionKeyboardPopupwindows(mContext, mEditTextContent);
        replace = Utils.getYuemeiInfo();

        Intent intent = getIntent();
        mDirectId = intent.getStringExtra("directId");
        mObjId = intent.getStringExtra("objId");
        mObjType = intent.getStringExtra("objType");
        title = intent.getStringExtra("title");
        mPrice = intent.getStringExtra("price");
        mImg = intent.getStringExtra("img");
        mUrl = intent.getStringExtra("url");
        mPlus = intent.getStringExtra("plus");
        mYm_calss = intent.getStringExtra("ymClass");
        mYm_id = intent.getStringExtra("ymId");
        mSkuId = intent.getStringExtra("skuId");
        mZTtitle = intent.getStringExtra("zt_title");
        mZTid = intent.getStringExtra("zt_id");
        mGroup_id = intent.getStringExtra("group_id");
        mClassid = intent.getStringExtra("classid");
        mQuickReplyContent = intent.getStringExtra("content");
        mQuickReply = intent.getStringExtra("quick_reply");
        mRegistrationID = JPushInterface.getRegistrationID(mContext);
        uid = Utils.getUid();
        mImei = Utils.getImei();
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        AdertAdv.OtherBackgroundImgBean data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);
        if (data != null && data.getImg() != null) {
            mChatDacuBackground.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams params = mChatDacuBackground.getLayoutParams();
            params.height = Utils.dip2px(50);
            mChatDacuBackground.setLayoutParams(params);
            chatsTitleTv.setTextColor(Color.WHITE);
            mDefaultBackImg.setBackgroundResource(R.drawable.back_white);
            mHospitalImg.setBackgroundResource(R.drawable.hospital_white);
            Glide.with(mContext).load(data.getImg()).into(mChatDacuBackground);
        } else {
            mDefaultBackground.setBackground(ContextCompat.getDrawable(mContext, R.color.title_bg));
            mChatDacuBackground.setVisibility(View.GONE);
            chatsTitleTv.setTextColor(Color.BLACK);
        }

        mNoLoginId = Cfg.loadStr(mContext, SplashActivity.NO_LOGIN_ID, "0");

//        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mDefaultBackground.getLayoutParams();
//        layoutParams.topMargin= statusbarHeight;
//        mDefaultBackground.setLayoutParams(layoutParams);
        initClick();
        IMManager.setMessageCallBack(this);
        if (mIdCallBack != null) {
            mIdCallBack.idCallBack(mDirectId);
        }

        mAudioSensorBinder = new AudioSensorBinder(this);
        ExposureLoginData exposureLoginData = new ExposureLoginData("151", "0");
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));
    }

    @Override
    protected void initData() {
        setChatCookie();
        getPersimmions();
        chatUpdateread(mDirectId);
        initChatBackPopData();
        initChatBackSuccessData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 接收消息的回调
     *
     * @param dataBean
     * @param group_id
     */
    @Override
    public void receiveMessage(MessageBean.DataBean dataBean, String group_id) {
        Log.d(TAG, "receiveMessage" + dataBean.toString());
        if (tblist.size() < 2) {
            tblist.add(dataBean);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pullList.setLinearLayout(false);
                    if (tbAdapter == null) {
                        tbAdapter = new ChatRecyclerAdapter(mContext, tblist, mOnline);
                    }
                    pullList.setAdapter(tbAdapter);
                }
            });

        } else {
            if (flag) {
                Log.d(TAG, "Group_id++++>" + mGroup_id + "group_id" + group_id);
                Log.d(TAG, mId);
                if (group_id.equals(mGroup_id)) {
                    Log.d(TAG, "uid" + uid + "fromuserid" + dataBean.getFromUserId());
                    if (mId.equals(dataBean.getFromUserId())) {
                        Log.d(TAG, "sendMessageOK");
                        tblist.add(dataBean);
                        mHandler.sendEmptyMessage(SEND_OK);
                    }

                }
            }
        }
    }

    @Override
    public void onFocusCallBack(String txt) {
        chatsTitleTv.setText(txt);
        mHandler.sendEmptyMessageDelayed(UPDATE_TITLE, 6000);
    }


    public static final int SEND_OK = 0x1110;
    public static final int REFRESH = 0x0011;
    public static final int RECERIVE_OK = 0x1111;
    public static final int PULL_TO_REFRESH_DOWN = 0x0111;
    public static final int VOICE_PROGRESS = 7;     //语音上传进度
    public static final int VOICE_SUCCESS = 8;     //语音上传成功
    public static final int VOICE_FAILURE = 9;     //语音上传失败
    public static final int UPDATE_TITLE = 10;     //更新标题
    public static final int ON_FOUCS = 11;     //发送焦点
    private Handler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<ChatActivity> mActivity;

        private MyHandler(ChatActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ChatActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case REFRESH:
                        if (theActivity.tbAdapter != null) {
                            theActivity.tbAdapter.notifyDataSetChanged();
                            int position = theActivity.tbAdapter.getItemCount() - 1 < 0 ? 0 : theActivity.tbAdapter.getItemCount() - 1;
                        }
                        break;
                    case SEND_OK:
                        if (theActivity.tbAdapter != null) {
                            theActivity.tbAdapter.notifyItemInserted(theActivity.tblist.size() - 1);
                            theActivity.pullList.smoothScrollToPosition(theActivity.tblist.size() - 1);
                        }
                        break;
                    case RECERIVE_OK:
                        if (theActivity.tbAdapter != null) {
                            theActivity.tbAdapter.notifyItemInserted(theActivity.tblist.size() - 1);
                        }
                        break;
                    case PULL_TO_REFRESH_DOWN:
                        if (theActivity.tbAdapter != null) {
                            List<MessageBean.DataBean> tblist = (List<MessageBean.DataBean>) msg.obj;
                            theActivity.pullList.setPullLoadMoreCompleted();
                            theActivity.tbAdapter.addMessage(tblist);
                            theActivity.tbAdapter.notifyDataSetChanged();
                            theActivity.pullList.smoothScrollToPosition(theActivity.position - 1);
                        }
                        break;
                    case VOICE_PROGRESS: //语音上传进度

                        break;
                    case VOICE_SUCCESS://语音上传成功
                        theActivity.voiceResult = (WriteVideoResult) msg.obj;
                        //又拍云路径
                        String resultUrl = theActivity.voiceResult.getUrl();
                        Log.e(TAG, "resultUrl == " + resultUrl);
                        Log.e(TAG, "voiceTime == " + theActivity.voiceTime);
                        theActivity.classid = "16";
                        theActivity.sendMessage(resultUrl, "0", "", theActivity.voiceTime + "");


                        break;
                    case VOICE_FAILURE://语音上传失败
                        theActivity.errorFile = (File) msg.obj;
                        Log.e(TAG, "errorFile == " + theActivity.errorFile);
                        if (null != theActivity.errorFile) {
                            if (theActivity.errorNum < 2) {
                                theActivity.errorNum++;
                                theActivity.sendMessage(theActivity.errorFile.toString(), "0", "", theActivity.voiceTime + "");
                            } else {
                                theActivity.mFunctionManager.showShort("语音上传失败,请检查网络");
                            }
                        }
                        break;
                    case UPDATE_TITLE:
                        theActivity.chatsTitleTv.setText(theActivity.mTitle);
                        break;
                    case ON_FOUCS:
                        Log.e(TAG, "ON_FOUCS ---" + theActivity.mHasFocus + "&&&&&&&" + theActivity.mOnFoucusStr);
                        if (theActivity.mHasFocus && !TextUtils.isEmpty(theActivity.mOnFoucusStr)) {
                            String str = Cfg.loadStr(theActivity, NetWork.IS_SHOW_LOGIN_CHAT, "0");
                            if (!"1".equals(str)){
                                theActivity.sendFoucs();
                            }
                        }
                        theActivity.mHandler.sendEmptyMessageDelayed(ON_FOUCS, 5000);
                        break;
                    default:
                        break;

                }
            }

        }

    }


    @SuppressLint("ClickableViewAccessibility")
    private void initClick() {
        mEditTextContent.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage(mEditTextContent.getText().toString().trim(), "0", "", "");
                    mEditTextContent.setText("");
                    return true;
                }
                return false;
            }
        });

        pullList.setOnPullLoadMoreListener(new PullLoadMoreRecyclerView.PullLoadMoreListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG, "onRefresh------------------->");
                if (CollectionUtils.isEmpty(mDataList) || mDataList.size()<9){
                    pullList.setPullLoadMoreCompleted();
                    mFunctionManager.showShort("已加载全部历史消息");

                }else {
                    downLoad();
                }

            }

            @Override
            public void onLoadMore() {

            }
        });
        tbbv.setOnHeadIconClickListener(new HeadIconSelectorView.OnHeadIconClickListener() {

            @SuppressLint("InlinedApi")
            @Override
            public void onClick(int from) {
                switch (from) {
                    case ChatBottomView.FROM_CAMERA:
                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA).build(), new AcpListener() {
                            @Override
                            public void onGranted() {        //判断权限是否开启
                                final String state = Environment.getExternalStorageState();
                                if (Environment.MEDIA_MOUNTED.equals(state)) {

                                    camPicPath = getSavePicPath();
                                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    int currentapiVersion = Build.VERSION.SDK_INT;
                                    if (currentapiVersion < 24) {
                                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(camPicPath)));
                                        startActivityForResult(cameraIntent, ChatBottomView.FROM_CAMERA);
                                    } else {
                                        ContentValues contentValues = new ContentValues(1);
                                        contentValues.put(MediaStore.Images.Media.DATA, camPicPath);
                                        Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                                        startActivityForResult(cameraIntent, ChatBottomView.FROM_CAMERA);
                                    }

                                } else {
                                    mFunctionManager.showShort("请检查内存卡");
                                }
                            }

                            @Override
                            public void onDenied(List<String> permissions) {
                                ViewInject.toast("没有相机权限");
                            }
                        });


                        break;
                    case ChatBottomView.FROM_GALLERY:
                        String status = Environment.getExternalStorageState();
                        if (status.equals(Environment.MEDIA_MOUNTED)) {// 判断是否有SD卡
                            //版本判断
                            if (Build.VERSION.SDK_INT >= 23) {
                                Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                                    @Override
                                    public void onGranted() {
                                        toXIangce();
                                    }

                                    @Override
                                    public void onDenied(List<String> permissions) {
                                    }
                                });
                            } else {
                                toXIangce();
                            }
                        } else {
                            mFunctionManager.showShort("没有SD卡");
                        }
                        break;
                }
            }
        });
        instance.setOnColseBiaoqingClickListener(new ExpressionKeyboardPopupwindows.onColseBiaoqingClickListener() {
            @Override
            public void onColseBiaoqingClick() {

                tongbaoUtils.setVisibility(View.VISIBLE);
                mKeyboardcontent.setVisibility(View.GONE);
                setBottomTipIsvisible(true);
                if (mQuick_reply != null && mQuick_reply.size() > 0) {
                    setListMargin(false, 89);
                } else {
                    setListMargin(false, 50);
                }
            }
        });
        mEditTextContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG, "onTouch---------");
                if (!isFlag) {
                    tbbv.setVisibility(View.GONE);
                    instance.dismiss();
                    if (mQuick_reply != null && mQuick_reply.size() > 0) {
                        setListMargin(false, 89);
                    } else {
                        setListMargin(false, 50);
                    }
                    KeyBoardUtils.showKeyBoard(mContext, mEditTextContent);
                    mKeyboardcontent.setVisibility(View.GONE);
                    isFlag = true;
                }
                return false;
            }
        });
        mEditTextContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mOnFoucusStr = s.toString();
                if (!TextUtils.isEmpty(s)) {
                    Log.e(TAG, "CharSequence =====" + s);
                    mSend.setVisibility(View.VISIBLE);
                    mess_iv.setVisibility(View.GONE);

                } else {
                    mess_iv.setVisibility(View.VISIBLE);
                    mSend.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditTextContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mHasFocus = hasFocus;
                if (hasFocus) {
                    instance.dismiss();
                    Log.e(TAG, "hasFocus =====");

                    sendFoucs();
                    mHandler.sendEmptyMessageDelayed(ON_FOUCS, 5000);

                }
            }
        });
        SoftKeyBoardListener.setListener(mContext, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                Log.e(TAG, "keyBoardShow ====");
                setBottomTipIsvisible(false);
                if (mQuick_reply != null && mQuick_reply.size() > 0) {
                    setListMargin(false, 89);
                } else {
                    setListMargin(false, 50);
                }
                mKeyboardcontent.setVisibility(View.GONE);
                instance.dismiss();


            }

            @Override
            public void keyBoardHide(int height) {
                Log.e(TAG, "keyBoardHide ====");
                if (mKeyboardcontent.getVisibility() == View.GONE && tbbv.getVisibility() == View.GONE) {
                    setBottomTipIsvisible(true);
                }
            }
        });


        pullList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    if (chatTipList.getVisibility() == View.VISIBLE) {
                        chatTipList.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });

        //录音按钮回调
        mRecorderButton.setAudioFinishRecorderListener(new AudioRecorderButton.AudioFinishRecorderListener() {
            @Override
            public void onFinish(int seconds, String filePath) {
                Log.e(TAG, "second == " + seconds);
                Log.e(TAG, "filePath == " + filePath);
                voiceTime = seconds;
                pathFile = filePath;
                if (!TextUtils.isEmpty(filePath)) {
                    //把本地生成的.mp3文件上传又拍云
                    VideoUploadUpyun.getVideoUploadUpyun(mContext, mHandler).uplodVoice(filePath);
                } else {
                    MyToast.makeTextToast2(mContext, "录音文件为空", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mChatExpandClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "mChatExpandClick =====   onClick");
                if (isExpand) {
                    isExpand = false;
                    mChatExpandImg.setBackgroundResource(R.drawable.chat_expand_up);
                    quickReplyTopVisorgone.setVisibility(View.VISIBLE);
                    setListMargin(true, 137);
                } else {
                    isExpand = true;
                    mChatExpandImg.setBackgroundResource(R.drawable.chat_expand_down);
                    quickReplyTopVisorgone.setVisibility(View.GONE);
                    setListMargin(true, 50);
                }
            }
        });

    }


    private void sendFoucs() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", mId);
        hashMap.put("group_id", mGroup_id);
        hashMap.put("hos_id", mHos_id);
        new ChatOnFocusApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "sendFoucsSuccess ===");
                }
            }
        });
    }


    @OnClick({R.id.chats_back, R.id.chats_hospital_iv, R.id.voice, R.id.emoji, R.id.mess_iv, R.id.mess_send})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chats_back:
                showChatBackPop();

                break;
            case R.id.chats_hospital_iv:
                Intent intent = new Intent(mContext, HosDetailActivity.class);
                intent.putExtra("hosid", mHos_id);
                startActivity(intent);
                break;
            case R.id.voice:
                if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
                    MyToast.makeTextToast2(mContext, "没有语音权限", Toast.LENGTH_SHORT).show();
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.RECORD_AUDIO).build(), new AcpListener() {
                        @Override
                        public void onGranted() {

                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                            MyToast.makeTextToast2(mContext, "没有语音权限", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
                if (tbbv.getVisibility() == View.VISIBLE) {
                    tbbv.setVisibility(View.GONE);
                    mess_iv.setBackgroundResource(R.drawable.more);
                }
                if (mQuick_reply != null && mQuick_reply.size() > 0) {
                    setListMargin(false, 89);
                } else {
                    setListMargin(false, 50);
                }
                if (isVoice) {
                    if (instance.isShowing()) {
                        emoji.setBackgroundResource(R.drawable.emoji);
                        mKeyboardcontent.setVisibility(View.GONE);
                        instance.dismiss();
                    }
                    mRecorderButton.init(mContext);
                    mVoice.setBackgroundResource(R.drawable.chat_keboard_img);
                    mEditTextContent.setVisibility(View.GONE);
                    mRecorderButton.setVisibility(View.VISIBLE);
                    KeyBoardUtils.hideKeyBoard(mContext, mEditTextContent);
                    isVoice = false;
                } else {
                    mVoice.setBackgroundResource(R.drawable.chat_voice_img);
                    mEditTextContent.setVisibility(View.VISIBLE);
                    mRecorderButton.setVisibility(View.GONE);
                    KeyBoardUtils.showKeyBoard(mContext, mEditTextContent);
                    isVoice = true;
                }
                break;
            case R.id.emoji:
                mVoice.setBackgroundResource(R.drawable.chat_voice_img);
                mEditTextContent.setVisibility(View.VISIBLE);
                mRecorderButton.setVisibility(View.GONE);
                isVoice = true;
                chatTipList.setVisibility(View.GONE);
                if (mKeyboardcontent.getVisibility() == View.GONE) {
                    if (tbbv.getVisibility() == View.VISIBLE) {
                        tbbv.setVisibility(View.GONE);
                        mess_iv.setBackgroundResource(R.drawable.more);
                    }
                    setListMargin(false, 330);
                    emoji.setBackgroundResource(R.drawable.chatting_setmode_keyboard_btn_normal);
                    mKeyboardcontent.setVisibility(View.VISIBLE);
                    instance.showAtLocation(tongbaoUtils, Gravity.BOTTOM, 0, 0);
                    KeyBoardUtils.hideKeyBoard(mContext, mEditTextContent);
                } else {
                    mKeyboardcontent.setVisibility(View.GONE);
                    instance.dismiss();
                    KeyBoardUtils.showKeyBoard(mContext, mEditTextContent);
                    emoji.setBackgroundResource(R.drawable.emoji);
                    if (mQuick_reply != null && mQuick_reply.size() > 0) {
                        setListMargin(false, 89);
                    } else {
                        setListMargin(false, 50);
                    }
                }

                break;
            case R.id.mess_iv:
                mVoice.setBackgroundResource(R.drawable.chat_voice_img);
                mEditTextContent.setVisibility(View.VISIBLE);
                mRecorderButton.setVisibility(View.GONE);
                chatTipList.setVisibility(View.GONE);
                if (tbbv.getVisibility() == View.GONE) {
                    if (mKeyboardcontent.getVisibility() == View.VISIBLE) {
                        mKeyboardcontent.setVisibility(View.GONE);
                        instance.dismiss();
                    }
                    mEditTextContent.setVisibility(View.VISIBLE);
                    mess_iv.setFocusable(true);
                    emoji.setBackgroundResource(R.drawable.emoji);
                    tbbv.setVisibility(View.VISIBLE);
                    setListMargin(false, 280);
                    KeyBoardUtils.hideKeyBoard(mContext, mEditTextContent);
                } else {
                    tbbv.setVisibility(View.GONE);
                    KeyBoardUtils.showKeyBoard(mContext, mEditTextContent);
                    if (mQuick_reply != null && mQuick_reply.size() > 0) {
                        setListMargin(false, 89);
                    } else {
                        setListMargin(false, 50);
                    }
                }
                isVoice = true;
                break;
            case R.id.mess_send:
                sendMessage(mEditTextContent.getText().toString().trim(), "0", "", "");
                mEditTextContent.setText("");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        flag = true;
        mAudioSensorBinder.onResume();

    }


    /**
     * 为连接设置Cookie
     */
    private void setChatCookie() {
        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
        HttpUrl httpUrl = new HttpUrl.Builder().scheme("https").host("chat.yuemei.com").build();

        List<Cookie> cookies = cookieStore.loadCookie(httpUrl);
        Log.e(TAG, "cookies == " + cookies);
        for (Cookie cookie : cookies) {
            domain = cookie.domain();
            expiresAt = cookie.expiresAt();
            name = cookie.name();
            path = cookie.path();
            value = cookie.value();
        }

        String mYuemeiinfo = Utils.getYuemeiInfo();
        cookieStore.removeCookie(httpUrl);
        Cookie yuemeiinfo = new Cookie.Builder().name("yuemeiinfo").value(mYuemeiinfo).domain(domain).expiresAt(expiresAt).path(path).build();
        cookieStore.saveCookie(httpUrl, yuemeiinfo);

        List<Cookie> cookies222 = cookieStore.loadCookie(httpUrl);
        Log.e(TAG, "cookies222 == " + cookies222);
    }

    @TargetApi(23)
    protected void getPersimmions() {
        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.READ_PHONE_STATE, WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
            @Override
            public void onGranted() {
                Log.e(TAG, "mDataList == " + mDataList.size());
                chatInterface();
            }

            @Override
            public void onDenied(List<String> permissions) {
                mFunctionManager.showShort("检测到没有开启读取权限");
                chatInterface();
            }
        });
    }

    /**
     * 设置recyclerview边距
     *
     * @param isTopOrBottom 高或者底部 true为高
     * @param value
     */
    private void setListMargin(boolean isTopOrBottom, int value) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) pullList.getLayoutParams();
        if (isTopOrBottom) {
            layoutParams.topMargin = Utils.dip2px(value);
        } else {
            layoutParams.bottomMargin = Utils.dip2px(value);
        }
        pullList.setLayoutParams(layoutParams);
    }


    /**
     * Chat index  加载聊天界面
     */
    public void chatInterface() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mDirectId);
        if (!TextUtils.isEmpty(mGroup_id)) {
            maps.put("group_id", mGroup_id);
        }
        maps.put("ymaq_class", mYm_calss);
        if (!TextUtils.isEmpty(mYm_id)) {
            maps.put("ymaq_id", mYm_id);
        }
        if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
            maps.put("uid",mNoLoginId);
        }
        new ChatInterfaceApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {


            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "chatinterface--------->" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        mInterfaceBean = JSONUtil.TransformSingleBean(serverData.data, ChatInterfaceBean.class);
                        String pushWelcome = mInterfaceBean.getPushWelcome();
                        String welcome = mInterfaceBean.getWelcome();
                        mTitle = mInterfaceBean.getTitle();
                        mHos_id = mInterfaceBean.getHos_id();
                        mHos_name = mInterfaceBean.getHos_name();
                        mGroup_id = mInterfaceBean.getGroup_id();
                        mId = mInterfaceBean.getId();
                        mOnline = mInterfaceBean.getOnline();
                        mRecorderButton.set_id(mId);
                        chatsTitleTv.setText(mTitle);
                        String icon = mInterfaceBean.getIcon();
                        switch (icon) {
                            case "0":  //不显示
                                mChatsIcon.setVisibility(View.GONE);
                                break;
                            case "1":  //认证
                                mChatsIcon.setVisibility(View.VISIBLE);
                                mChatsIcon.setBackgroundResource(R.drawable.minying_2x);
                                break;
                            case "2":  //官方
                                mChatsIcon.setVisibility(View.VISIBLE);
                                mChatsIcon.setBackgroundResource(R.drawable.yuemei_officia_listl);
                                break;
                        }

                        String gohospital = mInterfaceBean.getGohospital();
                        if ("1".equals(gohospital)) {
                            mChatsHospital.setVisibility(View.VISIBLE);
                        }

                        //输入框上方快速回复
                        mQuick_reply = mInterfaceBean.getQuick_reply();
                        String groupId = Cfg.loadStr(mContext, FinalConstant.GROUP_ID, "");
                        String userMore = Cfg.loadStr(mContext, FinalConstant.USER_MORE, "");
                        Log.e(TAG, "grouid == " + groupId);
                        Log.e(TAG, "userMore == " + userMore);
                        if (Utils.isLogin()){
                            if (!"1".equals(groupId) || "1".equals(userMore)) {
                                mReplyContainer.setVisibility(View.GONE);
                                setListMargin(false, 50);
                            } else {
                                setQuickReplyData();
                            }
                        }else {
                            setQuickReplyData();
                        }
                        //聊天也顶部快速回复
                        List<QuickReplyTop> quick_reply_top = mInterfaceBean.getQuick_reply_top();
                        if (quick_reply_top != null && quick_reply_top.size() > 0) {
                            mChatExpandClick.setVisibility(View.VISIBLE);
                            mChatExpandImg.setBackgroundResource(R.drawable.chat_expand_up);
                            quickReplyTopVisorgone.setVisibility(View.VISIBLE);
                            setListMargin(true, 137);
                            setTopTipListData(quick_reply_top);

                        } else {
                            mChatExpandClick.setVisibility(View.GONE);
                        }

                        setBottomTipIsvisible(true);
                        getMessage();

                        sendChatTips();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d(TAG, e.toString());
                    }
                }  else if ("10001".equals(serverData.code)){
                    finish();
                }else{
                    mFunctionManager.showShort(serverData.message);
                }

            }
        });
    }


    /**
     * 设置快捷回复数据
     */
    private void setQuickReplyData() {
        if (mQuick_reply != null && mQuick_reply.size() > 0) {
            mReplyContainer.setVisibility(View.VISIBLE);
            setListMargin(false, 89);
            for (int j = 0; j < mQuick_reply.size(); j++) {
                ChatTuiJianView chatTuiJianView = new ChatTuiJianView(mContext);
                final String string = mQuick_reply.get(j).getShow_content();
                final String send_content = mQuick_reply.get(j).getSend_content();
                final String event_name = mQuick_reply.get(j).getEvent_name();
                final String event_pos = mQuick_reply.get(j).getEvent_pos();
                final String menu_type = mQuick_reply.get(j).getMenu_type();
                final String jumpUrl = mQuick_reply.get(j).getJump_url();
                chatTuiJianView.setText(string);
                mChatTipContainer.addView(chatTuiJianView);
                chatTuiJianView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) return;
                        if ("21".equals(menu_type)){//视频面诊
                            WebViewUrlLoading.getInstance().showWebDetail(mContext,jumpUrl);
                        }else {
                            sendMessage(send_content, "1", menu_type, "");
                            isSelect = false;
                        }
                        YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos, "0", "0"));
                    }
                });
            }
        } else {
            mReplyContainer.setVisibility(View.GONE);
            setListMargin(false, 50);
        }
    }


    private void setTopTipListData(final List<QuickReplyTop> quick_question) {
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        quickReplyTopList.setLayoutManager(scrollLinearLayoutManager);
        ChatTopListAdapter chatTopListAdapter = new ChatTopListAdapter(R.layout.chat_top_list_item, quick_question);
        quickReplyTopList.setAdapter(chatTopListAdapter);
//        quickReplyTopList.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color._ed)));
        chatTopListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                QuickReplyTop quickQuestion = quick_question.get(position);
                String menuType = quickQuestion.getMenu_type();
                if ("21".equals(menuType)){ //21是视频面诊走type识别
                    WebViewUrlLoading.getInstance().showWebDetail(mContext,quickQuestion.getJump_url());
                }else {
                    mFunctionManager.saveStr(FinalConstant.CHAT_BOTTOM_ISVISIBLE, "1");
                    sendMessage(quickQuestion.getSend_content(), quickQuestion.getQuick_reply(),menuType, "");
                }
            }
        });
    }

    /**
     * 设置底部提示的数据
     */
    private void setTipListData(final List<QuickQuestion> quickQuestionList) {
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(true);
        chatTipList.setLayoutManager(scrollLinearLayoutManager);
        ChatTipListAdapter chatTipListAdapter = new ChatTipListAdapter(R.layout.chat_tip_list_item, quickQuestionList);
        chatTipList.setAdapter(chatTipListAdapter);
        chatTipList.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color._ea)));
        chatTipListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                QuickQuestion quickQuestion = quickQuestionList.get(position);
                sendMessage(quickQuestion.getSend_content(), quickQuestion.getQuick_reply(), "", "");
                chatTipList.setVisibility(View.GONE);
            }
        });

    }

    /**
     * 获取聊天数据
     * calssid   1: 消息    2： 图片   3：我正在看 用户   6 我正在看 医院    8 红包
     */
    public void getMessage() {
        tblist.clear();
        HashMap<String, Object> messageParam = new HashMap<>();
        messageParam.put("imei", mImei);
        messageParam.put("id", mId);
        messageParam.put("page", "1");
        messageParam.put("group_id", mGroup_id);
        messageParam.put("hos_id", mHos_id);
        if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
            messageParam.put("uid",mNoLoginId);
        }
        Log.e(TAG, "imei == " + mImei);
        Log.e(TAG, "id == " + mId);
        Log.e(TAG, "page == " + "1");
        Log.e(TAG, "group_id == " + mGroup_id);
        Log.e(TAG, "hos_id == " + mHos_id);
        new ChatMessApi().getCallBack(mContext, messageParam, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(final ServerData data) {
                Log.e(TAG, "data == " + data.toString());
                if ("1".equals(data.code)) {
                    mDataList = JSONUtil.jsonToArrayList(data.data, MessageBean.DataBean.class);
                    Log.e(TAG, "mDataList == " + mDataList.size());
                    if (CollectionUtils.isNotEmpty(mDataList)) {
                        mDateTime = mDataList.get(0).getDateTime();
                        for (MessageBean.DataBean dataBean : mDataList) {
                            dataBean.handlerMessageTypeAndViewStatus();
                        }
                        tblist = mDataList;
                        if (tblist.size() <= 5) {
                            pullList.setLinearLayout(false);
                        } else {
                            pullList.setLinearLayout(true);
                        }

                        tbAdapter = new ChatRecyclerAdapter(mContext, tblist, mHos_id, mObjId, mObjType, mId, mOnline);
                        pullList.setAdapter(tbAdapter);


                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pullList.setLinearLayout(false);
                                if (tbAdapter == null) {
                                    tbAdapter = new ChatRecyclerAdapter(mContext, tblist, mOnline);
                                }
                                pullList.setAdapter(tbAdapter);

                            }
                        });
                    }

                    if (Integer.parseInt(mObjId) > 0 && Integer.parseInt(mObjType) > 0) {
                        //发送我正在看
                        sendMessage(mEditTextContent.getText().toString().trim(), "0", "", "");

                    }


                }else {
                    mFunctionManager.showShort(data.message);
                }
            }

        });
    }


    /**
     * 底部提示显示或隐藏
     *
     * @param isvisible
     */
    private void setBottomTipIsvisible(boolean isvisible) {
        if (isvisible) {
            if (mInterfaceBean != null) {
                List<QuickQuestion> quick_question = mInterfaceBean.getQuick_question();
                if (quick_question != null && quick_question.size() > 0) {
//                    if (!isShowBottomTip()){
                    HashMap<String, String> questionEventParams = mInterfaceBean.getQuick_question_event_params();
                    questionEventParams.put("id", mHos_id);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_QUICK_QUESTION_EXPOSURE, "0", mHos_id), questionEventParams, new ActivityTypeData("15"));
                    chatTipList.setVisibility(View.VISIBLE);
                    setTipListData(quick_question);
//                    }
                } else {
                    chatTipList.setVisibility(View.GONE);
                }
            }
        } else {
            chatTipList.setVisibility(View.GONE);
        }
    }


    private void sendChatTips() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", mId);
        map.put("group_id", mGroup_id);
        map.put("ymaq_class", mYm_calss);
        if (!TextUtils.isEmpty(mYm_id)) {
            map.put("ymaq_id", mYm_id);
        }
        if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
            map.put("uid",mNoLoginId);
        }

        new SendTipsApi().getCallBack(mContext, map, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    Log.e(TAG, "SendTipsApi");
                }
            }
        });
    }

    private void downLoad() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                loadRecords();
            }
        }).start();

    }

    //下拉加载历史
    protected void loadRecords() {
        Log.d(TAG, "loadRecords");
        if (pagelist != null) {
            pagelist.clear();
        }
        Map<String, Object> params = new HashMap<>();
        params.put("id", mId);
        params.put("page", page);
        if (Integer.parseInt(page) > 1) {
            params.put("msgtime", mDateTime);
        }
        params.put("group_id", mGroup_id);
        params.put("hos_id", mHos_id);
        if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
            params.put("uid",mNoLoginId);
        }
        new ChatMessApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    ArrayList<MessageBean.DataBean> dataBeen = JSONUtil.jsonToArrayList(data.data, MessageBean.DataBean.class);
                    if (CollectionUtils.isNotEmpty(dataBeen)) {
                        mDateTime = dataBeen.get(0).getDateTime();
                        for (MessageBean.DataBean dataBean : dataBeen) {
                            dataBean.handlerMessageTypeAndViewStatus();
                        }
                        pagelist = dataBeen;
                        position = pagelist.size();
                        Log.d(TAG, "position=======>" + position);
                        if (pagelist.size() != 0) {
                            tblist.addAll(0, pagelist);
                            if (pagelist.size() < 10) {
                                //添加提示语
                                MessageBean.DataBean dataBean = new MessageBean.DataBean();
                                dataBean.setType(ChatRecyclerAdapter.TIP);
                                tblist.add(0, dataBean);
                                //插入客服在线状态
                                if ("1".equals(mOnline)) {
                                    MessageBean.DataBean dataBean1 = new MessageBean.DataBean();
                                    dataBean1.setType(ChatRecyclerAdapter.SERVICE_ONLINE);
                                    tblist.add(1, dataBean1);
                                }
                            }
                            Message message = mHandler.obtainMessage();
                            message.obj = tblist;
                            message.what = PULL_TO_REFRESH_DOWN;
                            mHandler.sendMessage(message);
                            i++;
                            page = i + "";
                            Log.d(TAG, "page==----------->" + page);


                        } else {
                            if ("2".equals(page)) {
                                pullList.setPullLoadMoreCompleted();
                            }
                        }
                    } else {
                        pullList.setPullLoadMoreCompleted();
                        mFunctionManager.showShort("已加载全部历史消息");
                    }
                } else {
                    mFunctionManager.showShort(data.message);
                }

            }
        });
    }


    /**
     * @param mContent      文字内容
     * @param isQuick_reply 是否是快捷消息  1是  0不是
     * @param menu_type     互动菜单消息类型（当该值不为空时，是互动菜单消息）
     * @param voiceTime     语音时长(单位秒)
     */
    public void sendMessage(final String mContent, final String isQuick_reply, final String menu_type, final String voiceTime) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String content = "";
                //用户头像
                String img = Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "");
                if (TextUtils.isEmpty(img)){
                    img = Cfg.loadStr(mContext,SplashActivity.NO_LOGIN_IMG,"");
                }
                Calendar cal = Calendar.getInstance();
                String timeSet = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
                final MessageBean.DataBean dataBean = new MessageBean.DataBean(img, content, 0, ChatRecyclerAdapter.TO_USER_MSG, Utils.getSecondTimestamp() + "", timeSet);
                //发送我正在看
                if (Integer.parseInt(mObjId) > 0 && Integer.parseInt(mObjType) > 0) {
                    content = "[我正在看]";
                    classid = "3";
                    dataBean.setType(ChatRecyclerAdapter.LOOKING_RIGHT);
                    List<MessageBean.DataBean.GuijiataAndroidBean> list = new ArrayList<>();
                    //        objtype 1 专题 2 淘整形 3医生 4医院 5帖子
                    String url = "";
                    switch (mObjType) {
                        case "1":
                            url = "https://m.yuemei.com/tao_zt/" + mObjId + "/";
                            break;
                        case "2":
                            if (TextUtils.isEmpty(mUrl)) {
                                url = "https://m.yuemei.com/tao/" + mSkuId + "/";
                            } else {
                                url = mUrl;
                            }
                            break;
                        case "3":
                            url = "https://m.yuemei.com/dr/" + mObjId + "/";
                            break;
                        case "4":
                            url = "https://m.yuemei.com/hospital/" + mObjId + "/";
                            break;
                        case "5":
                            url = mUrl;
                            break;
                    }
                    list.add(new MessageBean.DataBean.GuijiataAndroidBean(title, mImg, mPrice, url, mPlus));
                    dataBean.setGuijiata_android(list);
                    dataBean.setContent(content);
                    dataBean.setClassid("3");
                    tblist.add(dataBean);
                    Map<String, Object> params = new HashMap<>();
                    params.put("id", mId);
                    params.put("group_id", mGroup_id);
                    params.put("hos_id", mHos_id);
                    params.put("hos_name", mHos_name);
                    if (!TextUtils.isEmpty(mYm_calss)) {
                        params.put("ymaq_class", mYm_calss);
                    }
                    if (!TextUtils.isEmpty(mYm_id)) {
                        params.put("ymaq_id", mYm_id);
                    }
                    params.put("obj_id", mObjId);
                    params.put("obj_type", mObjType);
                    params.put("content", content);
                    params.put("classid", classid);
                    params.put("quick_reply", isQuick_reply);
                    if (!TextUtils.isEmpty(mZTtitle)) {
                        params.put("zt_title", mZTtitle);
                        Log.e(TAG, "mZTtitle====>" + mZTtitle);
                    }
                    if (!TextUtils.isEmpty(mZTid)) {
                        params.put("zt_id", mZTid);
                        Log.e(TAG, "mZTid====>" + mZTid);
                    }
                    if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
                        params.put("uid",mNoLoginId);
                    }
                    Log.e(TAG, "MmgobjId====>" + mObjId);
                    Log.e(TAG, "MmgobjType====>" + mObjType);
                    Log.e(TAG, "MmgmYmClass====>" + mYm_calss);
                    new ChatSendMessageApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            if ("1".equals(serverData.code)) {
                                try {
                                    WebSocketBean webSocketBean = JSONUtil.TransformSingleBean(serverData.data, WebSocketBean.class);
                                    List<ContinueToSend> continueToSend = webSocketBean.getContinueToSend();
                                    if (null != continueToSend && continueToSend.size() > 0) {
                                        for (int j = 0; j < continueToSend.size(); j++) {
                                            String classid1 = continueToSend.get(j).getClassid();
                                            String content1 = continueToSend.get(j).getContent();
                                            mObjId = "0";
                                            mObjType = "0";
                                            classid = classid1;
                                            sendMessage(content1, "0", "", "");
                                        }
                                    }
                                    if (!TextUtils.isEmpty(mClassid) && !TextUtils.isEmpty(mQuickReplyContent) && !TextUtils.isEmpty(mQuickReply)) {
                                        sendMessage(mQuickReplyContent, mQuickReply, "", "");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                dataBean.setMessageStatus(-1);
                                if (tbAdapter != null) {
                                    tbAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    });
                    mObjId = "0";
                    mObjType = "0";
                    return;
                } else {
                    content = mContent;
                    if (TextUtils.isEmpty(voiceTime)) {
                        if (!TextUtils.isEmpty(mClassid)) {
                            classid = mClassid;
                        } else {
                            classid = "1";
                        }
                    }
                    isValidMessage = true;
                }


                Log.e(TAG, "content === " + content);
                if (TextUtils.isEmpty(content)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mFunctionManager.showShort("消息不能为空！");
                        }
                    });
                    return;
                } else {
                    if (!TextUtils.isEmpty(voiceTime)) {
                        Log.e(TAG, "voiceTime == " + voiceTime + "\"");
                        VoiceMessage voiceMessage = new VoiceMessage(voiceTime, pathFile);
                        dataBean.setContent(voiceTime + "\"");
                        dataBean.setType(ChatRecyclerAdapter.VOICE_RIGHT);
                        dataBean.setVoiceMessages(voiceMessage);
                    } else {
                        dataBean.setType(ChatRecyclerAdapter.TO_USER_MSG);
                        dataBean.setContent(content);
                    }
                    tblist.add(dataBean);
                }

                Log.e(TAG, "size=================" + tblist.size());
                if (tblist.size() < 2) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pullList.setLinearLayout(false);
                            if (tbAdapter == null) {
                                tbAdapter = new ChatRecyclerAdapter(mContext, tblist, mOnline);
                            }
                            pullList.setAdapter(tbAdapter);
                        }
                    });

                } else {
                    mHandler.sendEmptyMessage(SEND_OK);

                }
                Map<String, Object> params = new HashMap<>();
                params.put("id", mId);
                params.put("group_id", mGroup_id);
                params.put("hos_id", mHos_id);
                params.put("hos_name", mHos_name);
                if (!TextUtils.isEmpty(mYm_calss)) {
                    params.put("ymaq_class", mYm_calss);
                }
                if (!TextUtils.isEmpty(mYm_id)) {
                    params.put("ymaq_id", mYm_id);
                }
                params.put("obj_id", mObjId);
                params.put("obj_type", mObjType);
                params.put("content", content);
                params.put("classid", classid);
                params.put("quick_reply", isQuick_reply);
                if (!TextUtils.isEmpty(menu_type)) {
                    params.put("menu_type", menu_type);
                }
                if (!TextUtils.isEmpty(voiceTime)) {
                    params.put("voiceTime", voiceTime);
                }
                if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
                    params.put("uid",mNoLoginId);
                }

                Log.e(TAG, "MmgobjId====>" + mObjId);
                Log.e(TAG, "MmgobjType====>" + mObjType);
                Log.e(TAG, "MmgmYmClass====>" + mYm_calss);
                new ChatSendMessageApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            Log.e(TAG, "sendMessageSuccess =======");
                            WebSocketBean webSocketBean = JSONUtil.TransformSingleBean(serverData.data, WebSocketBean.class);

                            List<ContinueToSend> continueToSend = webSocketBean.getContinueToSend();
                            if (null != continueToSend && continueToSend.size() > 0) {
                                for (int j = 0; j < continueToSend.size(); j++) {
                                    String classid1 = continueToSend.get(j).getClassid();
                                    String content1 = continueToSend.get(j).getContent();
                                    mObjId = "0";
                                    mObjType = "0";
                                    classid = classid1;
                                    sendMessage(content1, "0", "", "");
                                }
                            }
                            if (!Utils.isLogin()) {
                                if (isShowLogin()){
                                    finish();
                                    Utils.jumpLogin(mContext);
                                }
                            }
                        } else {
                            dataBean.setMessageStatus(-1);
                            if (tbAdapter != null) {
                                tbAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
                //网络异常的回调
                NetWork.getInstance().setOnErrorCallBack(new NetWork.OnErrorCallBack() {
                    @Override
                    public void onErrorCallBack(Call call, Response response, Exception e) {
                        Log.e(TAG, "onErrorCallBack");
                        dataBean.setMessageStatus(-1);
                        if (tbAdapter != null) {
                            Log.e(TAG, "notifyDataSetChanged");
                            tbAdapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }).start();

    }



    /**
     * 是否提示登录
     */
    private boolean isShowLogin() {
        Calendar cal = Calendar.getInstance();

        String year = cal.get(Calendar.YEAR) + "";
        String month = cal.get(Calendar.MONTH) + 1 + "";
        String day = cal.get(Calendar.DAY_OF_MONTH) + "";

        String nowTime = year + month + day;
        Log.e(TAG, "nowTime ==" + nowTime);


        //加载出保存hosid的集合
        String hosidList = mFunctionManager.loadStr(nowTime, "");
        Log.e(TAG, "hosidList ==" + hosidList);
        boolean isShow = true;     //默认是显示
        List<ChatShowLogin> listData;
        if (!TextUtils.isEmpty(hosidList)) {
            listData = JSONUtil.jsonToArrayList(hosidList, ChatShowLogin.class);
            for (ChatShowLogin data : listData) {
                if (data.getHos_id().equals(mHos_id)) {
                    isShow = false;
                    break;
                }
            }
        } else {
            listData = new ArrayList<>();
        }
        if (isShow){
            listData.add(new ChatShowLogin(mHos_id));
            String jsonstr = new Gson().toJson(listData);
            Log.e(TAG, "jsonstr == " + jsonstr);
            mFunctionManager.saveStr(nowTime, jsonstr);
        }
        return isShow;
    }


    private boolean isNetwork = true;

    /**
     * 发送图片
     *
     * @param file
     * @param img_s
     */
    public void sendImage(final File file, String img_s) {
        isValidMessage = true;
        if (isNetwork) {
            String img = Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "");
            if (TextUtils.isEmpty(img)){
                img = Cfg.loadStr(mContext,SplashActivity.NO_LOGIN_IMG,"");
            }
            List<MessageBean.DataBean.ImgdataAndroidBean> list = new ArrayList<>();
            list.add(new MessageBean.DataBean.ImgdataAndroidBean(file.getPath(), img_s));
            Calendar cal = Calendar.getInstance();
            String timeSet = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
            mDataBean = new MessageBean.DataBean(ChatRecyclerAdapter.TO_USER_IMG, 0, img, list, Utils.getSecondTimestamp() + "", "[图片]", timeSet);
            tblist.add(mDataBean);
            imageList.add(tblist.get(tblist.size() - 1).getImgdata_android().get(0).getImg_y());
            imagePosition.put(tblist.size() - 1, imageList.size() - 1);
            mHandler.sendEmptyMessage(SEND_OK);
        }
        if (!Utils.isNetworkAvailable(mContext)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDataBean.setMessageStatus(-1);
                    if (tbAdapter != null) {
                        tbAdapter.notifyDataSetChanged();
                    }
                    isNetwork = false;
                }
            });
            return;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("id", mId);
        map.put("group_id", mGroup_id);
        map.put("hos_id", mHos_id);
        map.put("hos_name", mHos_name);
        map.put("obj_id", mObjId);
        map.put("obj_type", mObjType);
        map.put("ymaq_class", mYm_calss);
        map.put("ymaq_id", mYm_id);

        if ("4".equals(mObjType)) {
            map.put("hos_name", mTitle);
        }
        map.put("classid", "2");
        map.put("content", "[图片]");
        map.put("upfile", file);

        new ChatSendMessageApi().getCallBack(mContext, map, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (!"1".equals(serverData.code)) {
                    mDataBean.setMessageStatus(-1);
                    tbAdapter.notifyDataSetChanged();
                    mFunctionManager.showShort(serverData.message);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("errormessage", serverData.message);
                    StatisticalManage.getInstance().growingIO("chat_failure", hashMap);
                } else {
                    mDataBean.setMessageStatus(0);
                    tbAdapter.notifyDataSetChanged();
                    isNetwork = true;
                }
            }
        });
    }

    /**
     * 刷新消息未读数
     */
    public void chatUpdateread(String id) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", id);
        if (!Utils.isLogin() && !TextUtils.isEmpty(mNoLoginId)){
            maps.put("uid",mNoLoginId);
        }

        new XiaoxiChatUpdatereadApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    Log.e(TAG, "执行到此 === " + serverData.toString());

                } else {

                    mFunctionManager.showShort(serverData.message);
                }
            }
        });
    }

    /**
     * 初始化返回弹窗数据
     */
    public void initChatBackPopData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("taoID", mSkuId);
        new ChatBackApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "BackPopData ==" + serverData.message);
                if ("1".equals(serverData.code)) {

                    chatBackBean = JSONUtil.TransformSingleBean(serverData.data, ChatBackBean.class);

                }
            }
        });
    }


    /**
     * 初始化返回成功领奖数据
     */
    public void initChatBackSuccessData() {
        new ChatBackSuccessApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mSuccessBean = JSONUtil.TransformSingleBean(serverData.data, ChatBackSuccessBean.class);

                }
            }
        });
    }

    /**
     * 显示返回弹窗
     */

    public void showChatBackPop() {
        int messageNum;
        if (chatBackBean != null && chatBackBean.getComparedDoctors().size() > 0) {
            if (!isValidMessage) {
                messageNum = Cfg.loadInt(mContext, FinalConstant.MESSAGE_NUM, 0);
                if (messageNum < 3) {
                    HashMap<String, String> event_params = chatBackBean.getComparedDoctors().get(0).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARE_BAOGUANG, "0"), new ActivityTypeData("63"));
                    messageNum++;
                    Cfg.saveInt(mContext, FinalConstant.MESSAGE_NUM, messageNum);
                    ChatBackPopwindow chatBackPopwindow = new ChatBackPopwindow(mContext, mSkuId, chatBackBean);
                    chatBackPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            WindowManager.LayoutParams lp = getWindow().getAttributes();
                            lp.alpha = 1f;
                            getWindow().setAttributes(lp);
                            Intent intent = new Intent();
                            if (tbAdapter != null) {
                                intent.putExtra("lastData", tbAdapter.getLastData());
                                intent.putExtra("lastTime", tbAdapter.getLastTime());
                            }
                            setResult(100, intent);
                            onBackPressed();
                        }
                    });
                    WindowManager.LayoutParams lp = getWindow().getAttributes();
                    lp.alpha = 0.2f;
                    getWindow().setAttributes(lp);
                    chatBackPopwindow.showAtLocation(activityRootView, Gravity.CENTER, 0, 0);
                } else {
                    jumpCommonActivity();

                }

            } else {
                jumpCommonActivity();
            }

        } else {

            jumpCommonActivity();
        }

    }


    private void showBakcGetPop() {
        HashMap<String, String> parms = new HashMap<>();

        parms.put("id", mHos_id);
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_ALERT, "show"), parms, new ActivityTypeData("151"));
        mChatBackGetPop = new ChatBackGetPop(mContext, mSuccessBean, mHos_id);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = 0.6f;
        getWindow().setAttributes(lp);
        mChatBackGetPop.showAtLocation(activityRootView, Gravity.CENTER, 0, 0);
//        mHandler.sendEmptyMessageDelayed(CHAT_BACK,3000);
        mChatBackGetPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.alpha = 1f;
                getWindow().setAttributes(lp);
                Intent intent = new Intent();
                if (tbAdapter != null) {
                    intent.putExtra("lastData", tbAdapter.getLastData());
                    intent.putExtra("lastTime", tbAdapter.getLastTime());
                }
                setResult(100, intent);
                onBackPressed();
            }
        });
    }

    private void jumpCommonActivity() {
        Intent intent = new Intent();
        if (tbAdapter != null) {
            intent.putExtra("lastData", tbAdapter.getLastData());
            intent.putExtra("lastTime", tbAdapter.getLastTime());
        }
        setResult(100, intent);
        onBackPressed();
    }

    private String getSavePicPath() {
        final String dir = FileSaveUtil.SD_CARD_PATH + "image_data/";
        try {
            FileSaveUtil.createSDDirectory(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String fileName = String.valueOf(System.currentTimeMillis() + ".png");
        return dir + fileName;
    }

    void toXIangce() {
        Log.e(TAG, "mResults === " + mResults.size());
        mResults.clear();
        // 启动多个图片选择器
        Intent intent = new Intent(mContext, ImagesSelectorActivity.class);
        // 要选择的图像的最大数量
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 9);
        // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
        // 显示摄像机或不
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
        // 将当前选定的图像作为初始值传递
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
        // 开始选择器
        startActivityForResult(intent, ChatBottomView.FROM_GALLERY);

    }

    /**
     * 压缩图片以及如果图片旋转了，旋转回去
     *
     * @param path
     * @param bitmapDegree
     */
    private void showDialog(final String path, final int bitmapDegree) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String GalPicPath = getSavePicPath();
                    Bitmap bitmap = PictureUtil.compressSizeImage(path);
                    boolean isSave = FileSaveUtil.saveBitmap(PictureUtil.reviewPicRotate(bitmap, bitmapDegree), GalPicPath);
                    File file = new File(GalPicPath);
                    if (file.exists() && isSave) {
                        sendImage(file, path);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void imageUpload(String img) {
        //压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + System.currentTimeMillis() + ".JPEG");
        String desPath = file.getPath();
        FileUtils.compressPicture(img, desPath);

        try {
            Log.e(TAG, "文件大小是 == " + getFileSize(new File(desPath)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //发送图片
        sendImage(new File(desPath), img);


    }

    /**
     * 获取指定文件大小
     *
     * @param file
     * @return
     * @throws Exception
     */
    private static long getFileSize(File file) throws Exception {
        long size = 0;
        if (file.exists()) {
            FileInputStream fis = null;
            fis = new FileInputStream(file);
            size = fis.available();
        } else {
            file.createNewFile();
            Log.e("获取文件大小", "文件不存在!");
        }
        return size;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            tbbv.setVisibility(View.GONE);
            mess_iv.setBackgroundResource(R.drawable.more);
            if (mQuick_reply != null && mQuick_reply.size() > 0) {
                setListMargin(false, 89);
            } else {
                setListMargin(false, 50);
            }
            switch (requestCode) {
                case ChatBottomView.FROM_CAMERA:
                    FileInputStream is = null;
                    try {
                        is = new FileInputStream(camPicPath);
                        final File camFile = new File(camPicPath); // 图片文件路径
                        if (camFile.exists()) {

                            int size = ImageCheckoutUtil.getImageSize(ImageCheckoutUtil.getLoacalBitmap(camPicPath));

                            int bitmapDegree = PictureUtil.getPicRotate(camPicPath);
                            Log.e(TAG, "bitmapDegree === " + bitmapDegree);
                            if (bitmapDegree != 0) {
                                showDialog(camPicPath, bitmapDegree);
                            } else {
                                if (size > IMAGE_SIZE) {
                                    showDialog(camPicPath, bitmapDegree);
                                } else {
                                    sendImage(camFile, camFile.getPath());
                                }
                            }
                        } else {
                            mFunctionManager.showShort("该文件不存在!");
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } finally {
                        // 关闭流
                        try {
                            is.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case ChatBottomView.FROM_GALLERY:
                    mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                    if (mResults != null && mResults.size() > 0) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (String res : mResults) {
                                    imageUpload(res);
                                }
                            }
                        }).start();

                    }
                    break;
            }
        }

    }

    /**
     * 监听Back键按下事件
     * 注意:
     * 返回值表示:是否能完全处理该事件
     * 在此处返回false,所以会继续传播该事件.
     * 在具体项目中此处的返回值视情况而定.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (chatBackBean != null) {
                showChatBackPop();
            } else {
                jumpCommonActivity();

            }


            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "-------------->onDestroy");
        flag = false;
        if (Util.isOnMainThread() && !this.isFinishing()) {
            Glide.with(MyApplication.getContext()).pauseRequests();
        }
        mAudioSensorBinder.onDestroy();
        MediaManager.stop();
        mHandler.removeCallbacksAndMessages(null);
        if (instance != null) {
            instance.dismiss();
        }
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (chatBackBean == null){
//            if (mSuccessBean != null){
//                RewardAlertToast.showChatBackToast(mSuccessBean,mHos_id);
//            }
//        }
        Log.e(TAG, "onBackPressed==");
    }

    public static void setIdCallBack(IdCallBack idCallBack) {
        mIdCallBack = idCallBack;
    }

    public interface IdCallBack {
        void idCallBack(String chatId);
    }

    public static void setImageProgress(ImageProgress imageProgress) {
        mImageProgress = imageProgress;
    }

    static ImageProgress mImageProgress;

    public interface ImageProgress {
        void setProgress(int progress);
    }


}
