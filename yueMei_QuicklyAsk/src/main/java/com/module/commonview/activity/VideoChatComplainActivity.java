package com.module.commonview.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.module.api.VideoComplainApi;
import com.module.api.VideoComplainSaveApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.ComplainAdapter;
import com.module.commonview.module.bean.ComplainMessageBean;
import com.module.commonview.module.bean.ComplaintOption;
import com.module.commonview.module.bean.DoctorInfo;
import com.module.commonview.module.bean.VideoComplainBean;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.community.model.api.SaveVideoScoreApi;
import com.module.my.controller.other.UploadStatusException;
import com.module.my.view.view.ImageVideoUploadView;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.other.RequestAndResultCode;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoChatComplainActivity extends YMBaseActivity {
    public static final String TAG = VideoChatComplainActivity.class.getSimpleName();
    @BindView(R.id.title_bar)
    CommonTopBar titleBar;
    @BindView(R.id.complian_scrollview)
    ScrollView mScrollView;
    @BindView(R.id.complain_doctor_img)
    ImageView complainDoctorImg;
    @BindView(R.id.complain_doctor_name)
    TextView complainDoctorName;
    @BindView(R.id.complain_doctor_job)
    TextView complainDoctorJob;
    @BindView(R.id.complain_hos_name)
    TextView complainHosName;
    @BindView(R.id.goods_container)
    FlowLayout goodsContainer;
    @BindView(R.id.complain_list)
    RecyclerView complainList;
    @BindView(R.id.complain_edt_input)
    EditText complainEdtInput;
    @BindView(R.id.complain_text_num)
    TextView complainTextNum;
    @BindView(R.id.complain_img)
    ImageVideoUploadView complainImg;
    @BindView(R.id.complain_sure_btn)
    Button complainSureBtn;

    private Context mContext;
    String mChannelId;
    private List<Integer> mSelectedPosition;//投诉列表返回的下标
    private String complainMessage;//投诉输入内容
    private List<ComplaintOption> mComplaintOption;

    @Override
    protected int getLayoutId() {
        return R.layout.video_chat_complain_layout;
    }

    @Override
    protected void initView() {
        mContext = VideoChatComplainActivity.this;
        mChannelId = getIntent().getStringExtra("channel_id");

        initListener();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void initData() {
        loadData();
        complainImg.initView(false);
        complainImg.setMaxImgNum(3);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    private void loadData() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("channel_id",mChannelId);
        new VideoComplainApi().getCallBack(this, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    VideoComplainBean videoComplainBean = JSONUtil.TransformSingleBean(serverData.data, VideoComplainBean.class);
                    DoctorInfo doctorInfo = videoComplainBean.getDoctorInfo();
                    mComplaintOption = videoComplainBean.getComplaintOption();

                    Glide.with(mContext).load(doctorInfo.getShow_doctors_avatar()).transform(new GlideCircleTransform(mContext)).into(complainDoctorImg);
                    complainDoctorName.setText(doctorInfo.getShow_doctors_name());
                    complainDoctorJob.setText(doctorInfo.getShow_doctors_job());
                    complainHosName.setText(doctorInfo.getHospital_name());
                    List<String> goodPart = doctorInfo.getDoctors_good_part();
                    if (CollectionUtils.isNotEmpty(goodPart)){
                        setTagView(goodsContainer,goodPart);
                    }

                    ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 2);
                    gridLayoutManager.setScrollEnable(false);
                    complainList.setLayoutManager(gridLayoutManager);
                    ComplainAdapter complainAdapter = new ComplainAdapter(R.layout.complain_list_item, mComplaintOption);
                    complainList.setAdapter(complainAdapter);
                    complainAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            ComplainAdapter complainAdapter = (ComplainAdapter) adapter;
                            complainAdapter.multiSelect(position);
                            mSelectedPosition = complainAdapter.getMultiSelectedPosition();
                        }
                    });

                } else {
                    MyToast.makeTextToast2(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        complainEdtInput.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    mScrollView.scrollTo(0,200);
                }
                return false;
            }
        });
        complainEdtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                complainMessage = s.toString();
                complainTextNum.setText(s.length()+"/200");
            }
        });
        complainImg.setOnEventClickListener(new ImageVideoUploadView.OnEventClickListener() {
            @Override
            public void onVideoCoverClick(int visibility) {
            }

            @Override
            public void onVideoCoverPathClick(String qiNiukey, String path) {
            }

            @Override
            public void startFragmentForResult(Intent intent, int requestCode) {
                startActivityForResult(intent, requestCode);
            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == RequestAndResultCode.IMAGE_REQUEST_CODE){//选择图片回调
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        complainImg.setRequestImage(data);
                    }
                }
            }
        } catch (UploadStatusException e) {
            Log.e(TAG, "e === " + e.getMsg());
        }
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<String> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++){
                String text = lists.get(i);
                TextView textView = new TextView(mContext);
                textView.setText(text);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                textView.setBackgroundResource(R.drawable.complain_doctor_good_shap);
                textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                mFlowLayout.addView(textView);

            }
        }
    }



    @OnClick(R.id.complain_sure_btn)
    public void onClick() {
        showDialog();
    }


    /**
     * 确认弹窗
     */
    private void showDialog() {
        final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "确认提交", "确认", "取消");
        yueMeiDialog.setCanceledOnTouchOutside(false);
        yueMeiDialog.show();
        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                yueMeiDialog.dismiss();
                saveComplainData();
                saveVideoScore("0");
            }

            @Override
            public void rightBtnClick() {

                yueMeiDialog.dismiss();
            }
        });
    }


    /**
     * 评分接口
     * @param score
     */
    private void saveVideoScore(String score){
        HashMap<String, Object> params = new HashMap<>();
        params.put("channel_id",mChannelId);
        params.put("score_type","1");
        params.put("score",score);
        new SaveVideoScoreApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){

                }
            }
        });
    }


    /**
     * 提价投诉的数据
     */
    private void saveComplainData() {
        if (CollectionUtils.isEmpty(mSelectedPosition)){
            MyToast.makeTextToast2(mContext, "请选择投诉类型", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(complainMessage) && "[]".equals(complainImg.getImageJson())){
            MyToast.makeTextToast2(mContext, "请输入投诉说明", Toast.LENGTH_SHORT).show();
            return;
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("channel_id",mChannelId);
        params.put("complaint_user_type","1");
        if (!"[]".equals(complainImg.getImageJson())){
            params.put("image",complainImg.getImageJson());
        }
        if (!TextUtils.isEmpty(complainMessage)){
            params.put("content",transJsonData());
        }
        params.put("complaint_option_id",getSelectTypeData());
        new VideoComplainSaveApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    finish();
                }
                MyToast.makeTextToast2(mContext, serverData.message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     *
     * @return 输入内容转成的json串
     */
    private String transJsonData(){
        return new Gson().toJson(new ComplainMessageBean(complainMessage));
    }

    /**
     * 根据返回的角标查找数据对应的id
     * @return
     */
    private String getSelectTypeData(){
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < mSelectedPosition.size(); i++) {
            for (int j = 0; j < mComplaintOption.size(); j++) {
                if (mSelectedPosition.get(i) == j){
                    ComplaintOption complaintOption = mComplaintOption.get(j);
                    ids.add(complaintOption.getId());
                }
            }
        }
        return StringUtils.strip(ids.toString(), "[]");
    }
}
