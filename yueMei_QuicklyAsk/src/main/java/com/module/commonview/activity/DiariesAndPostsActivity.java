package com.module.commonview.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.adapter.CommentsRecyclerAdapter;
import com.module.commonview.adapter.DiaryRecyclerAdapter;
import com.module.commonview.fragment.CommentsAndRecommendFragment;
import com.module.commonview.fragment.DiariesAndDetailsFragment;
import com.module.commonview.fragment.DiariesAndDetailsNoThereFragment;
import com.module.commonview.fragment.DiariesPostsFragment648;
import com.module.commonview.fragment.PostBottomFragment;
import com.module.commonview.fragment.PostContentFragment;
import com.module.commonview.fragment.PostHtmlFragment;
import com.module.commonview.fragment.PostTopTitleFragment;
import com.module.commonview.module.api.BaikeFourApi;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.commonview.module.api.DeleteBBsApi;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.InitShareDataApi;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesPostShowData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryListListData;
import com.module.commonview.module.bean.DiaryListListPic;
import com.module.commonview.module.bean.DiaryReplyList;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.view.ButtomDialogView;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.commonview.view.FocusButton2;
import com.module.commonview.view.share.MyShareBoardlistener;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.model.bean.BBsShareData;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.model.bean.ShareDetailPictorial;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 日记日记本帖子页面
 * Created by 裴成浩 on 2018/6/8.
 */
public class DiariesAndPostsActivity extends YMBaseActivity {


    @BindView(R.id.activity_diaries_and_posts)
    FrameLayout diariesPosts;
    @BindView(R.id.bbs_title_bar)
    public RelativeLayout mTop;                         //标题容器
    @BindView(R.id.bbs_title_bar_background)
    View mTopBackground;                         //标题背景
    @BindView(R.id.bbs_title_bar_title)
    LinearLayout mTopTitle;                         //标题
    @BindView(R.id.diary_posts_top_back)
    RelativeLayout webViewBack;                         //返回按钮
    @BindView(R.id.diary_posts_top_img)
    ImageView postsTopBackImg;                         //返回按钮图
    @BindView(R.id.diary_posts_top_docname)
    TextView titleName;                                 //标题
    @BindView(R.id.diary_posts_top_collection_click)
    RelativeLayout viewCollectionClick;                   //收藏
    @BindView(R.id.diary_posts_top_collection_img)
    ImageView viewCollectionImg;                         //收藏图标
    @BindView(R.id.diary_posts_top_share_click)
    RelativeLayout viewShareClick;                        //分享
    @BindView(R.id.diary_posts_top_share_img)
    ImageView postTopShareImg;                          //分享按钮图

    @BindView(R.id.ll_post_detail)
    LinearLayout ll_post_detail;
    @BindView(R.id.iv_back)
    LinearLayout iv_back;//返回键
    @BindView(R.id.adiary_user_image)
    ImageView adiary_user_image;//用户头像
    @BindView(R.id.diary_list_user_name)
    TextView diary_list_user_name;//用户姓名
    @BindView(R.id.diary_list_user_flag)
    ImageView diary_list_user_flag;//用户级别
    @BindView(R.id.diary_list_user_date)
    TextView diary_list_user_date;//时间
    @BindView(R.id.adiary_list_focus)
    FocusButton2 adiary_list_focus;//关注
    @BindView(R.id.adiary_list_positioning_title)
    TextView adiary_list_positioning_title;//删除按钮
    @BindView(R.id.iv_share)
    LinearLayout iv_share;//分享
    private DiaryUserdataBean mUserdata;
    private HashMap<String, String> followClickData;


    @BindView(R.id.diary_posts_top_tip_huabao)
    View tipHuabao;                                     //分享画报提示
    @BindView(R.id.diary_posts_content_click)
    public CardView mContentClick;
    @BindView(R.id.diary_posts_title_text)
    public TextView mTitleTxt;

    private InitShareDataApi mShareDataApi;             //分享数据
    private BaikeFourApi mCollectApi;                   //收藏请求类
    private CancelCollectApi mCancelCollectApi;         //取消收藏请求类
    private ZanOrJuBaoApi mPointLikeApi;                //点赞接口
    private DeleteBBsApi mDeleteBBsApi;                 //删除日记或者评论的接口
    private FocusAndCancelApi mFocusAndCancelApi;       //关注取消关注请求类

    private String TAG = "DiariesAndPostsActivity";
    private final String BBS_ILLUSTRATED_PROMPT = "bbs_illustrated_prompt";
    private String mDiaryId;        //日记帖子id（必传）
    private int isNew;              //默认值是0，（非必传）
    public String mUrl;            //帖子的url（必传）

    //分享数据
    private String shareUrl;
    private String shareContent;
    private String shareImgUrl;
    private String askOrshare1;
    private ShareWechat mWechat;
    private ShareDetailPictorial mPictorial;
    private MyShareBoardlistener myShareBoardlistener;  //分享设置
    private ButtomDialogView buttomDialogView;          //定义分享面板

    private DiariesAndDetailsFragment diariesAndDetailsFragment;        //日记日记本
    private DiariesPostsFragment648 diariesPostsFragment;                  //帖子
    private EditExitDialog editDialog;
    public VideoShareData videoShareData;
    private boolean mChatFlag = false;

    private Handler mHandler = new MyHandler(this);
    private ChatDataBean mChatData;
    private String mAskorshare;
    private String selfPageType;
    private String p_id;
    private String mIsCollect = "0";
    private boolean isTransparent = false;      //头部当前透明度
    private Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_diaries_and_posts;
    }

    @Override
    protected void initView() {
        mDiaryId = getIntent().getStringExtra("qid");
        mUrl = getIntent().getStringExtra("url");
        isNew = getIntent().getIntExtra("is_new", 0);
        Log.e(TAG, "mDiaryId === " + mDiaryId);
        Log.e(TAG, "mUrl === " + mUrl);
        Log.e(TAG, "isNew === " + isNew);
        mGson = new Gson();
        ViewGroup.LayoutParams layoutParams = mTop.getLayoutParams();
        layoutParams.height = Utils.dip2px(50);
        mTop.setLayoutParams(layoutParams);

        mTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    if (diariesAndDetailsFragment != null) {
                        if (diariesAndDetailsFragment.mScroll != null) {
                            diariesAndDetailsFragment.mScroll.smoothScrollTo(0, 0);
                        }
                    } else if (diariesPostsFragment != null) {
                        if (diariesPostsFragment.getPostContentFragment() != null) {
                            if (diariesPostsFragment.getPostContentFragment().mScroll != null) {
                                diariesPostsFragment.getPostContentFragment().mScroll.smoothScrollTo(0, 0);
                            }
                        }
                    }
                }
            }
        });

        //私信入口点击
        mContentClick.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (Utils.noLoginChat()) {
                    toChatActivity();
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        toChatActivity();
                    }
                }

            }
        });

    }

    private void toChatActivity() {
        if (mChatData != null) {
            String is_rongyun = mChatData.getIs_rongyun();
            String hos_userid = mChatData.getHos_userid();
            String ymaq_class = mChatData.getYmaq_class();
            String ymaq_id = mChatData.getYmaq_id();
            String event_name = mChatData.getEvent_name();
            String event_pos = mChatData.getEvent_pos();
            String obj_type = mChatData.getObj_type();
            String obj_id = mChatData.getObj_id();
            ChatDataGuiJi guiji = mChatData.getGuiji();
            String price = guiji.getPrice();
            String image = guiji.getImage();
            String member_price = guiji.getMember_price();
            String url = guiji.getUrl();
            String title = guiji.getTitle();
            HashMap<String, String> event_params = mChatData.getEvent_params();
            if ("3".equals(is_rongyun)) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(obj_id)
                        .setObjType(obj_type)
                        .setYmClass(ymaq_class)
                        .setYmId(ymaq_id)
                        .setPrice(price)
                        .setMemberPrice(member_price)
                        .setImg(image)
                        .setUrl(url)
                        .setTitle(title)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            } else {
                Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initPostsDetailTitle(DiaryListData diaryListData, PostListData postListData) {
        if (diaryListData != null) {
            mUserdata = diaryListData.getUserdata();
            followClickData = diaryListData.getFollowClickData();
        } else {
            mUserdata = postListData.getUserdata();
            followClickData = postListData.getFollowClickData();
        }
        Glide.with(mContext)
                .load(mUserdata.getAvatar())
                .placeholder(R.drawable.home_other_placeholder)
                .error(R.drawable.home_other_placeholder)
                .transform(new GlideCircleTransform(mContext))
                .into(adiary_user_image);
        diary_list_user_name.setText(mUserdata.getName());
        diary_list_user_date.setText(mUserdata.getLable());

        //浏览自己的
        if (mUserdata.getId().equals(Utils.getUid())) {
            //关注隐藏，定位显示
            adiary_list_focus.setVisibility(View.GONE);

            adiary_list_positioning_title.setVisibility(View.VISIBLE);

            if (isDiaryOrDetails()) {
                adiary_list_positioning_title.setText("删除日记");
            } else {
                adiary_list_positioning_title.setText("删除帖子");
            }

        } else {                //浏览他人的
            //关注显示，定位隐藏
            adiary_list_focus.setVisibility(View.VISIBLE);
            adiary_list_positioning_title.setVisibility(View.GONE);

            //设置标签
            switch (mUserdata.getTalent()) {
                case "0":
                    diary_list_user_flag.setVisibility(View.GONE);
                    break;
                case "10":                                      //悦美官方
                    diary_list_user_flag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(diary_list_user_flag, R.drawable.yuemei_officia_listl);
                    break;
                case "11":                                      //悦美达人
                    diary_list_user_flag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(diary_list_user_flag, R.drawable.talent_big);
                    break;
                case "12":                                      //认证医生
                    diary_list_user_flag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(diary_list_user_flag, R.drawable.yuemei_renzheng_2x);
                    break;
                case "13":                                      //认证医院
                    diary_list_user_flag.setVisibility(View.VISIBLE);
                    mFunctionManager.setImgSrc(diary_list_user_flag, R.drawable.user_tag_7_2x);
//                    adiary_list_focus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                    break;
            }
            //设置关注按钮
            setFocusButton();
            //关注按钮点击
            adiary_list_focus.setFocusClickListener(mUserdata.getIs_following(), new FocusButton2.ClickCallBack() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(followClickData);
                    FocusAndCancel();
                }
            });
        }
        //返回按键点击
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //用户头像点击
        adiary_user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_USER_AVATAR_CLICK), mUserdata.getEvent_params(), new ActivityTypeData("45"));
                WebUrlTypeUtil.getInstance(mContext).urlToApp(mUserdata.getUrl(), "0", "0");
            }
        });
        //删除点击
        adiary_list_positioning_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserdata.getId().equals(Utils.getUid())) {
                    DiariesDeleteData deleteData = new DiariesDeleteData();
                    deleteData.setId(mDiaryId);
                    deleteData.setZhuTie(true);
                    deleteData.setDiaries(false);
                    deleteData.setReplystr("0");
                    if (Utils.isLoginAndBind(mContext)) {
                        if ("1".equals(deleteData.getReplystr())) {
                            showDialogExitEdit("确定要删除此评论吗?", deleteData);
                        } else {
                            if (deleteData.isDiaries()) {
                                showDialogExitEdit("确定要删除此日记吗?", deleteData);
                            } else {
                                showDialogExitEdit("确定要删除本帖吗?", deleteData);
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * 设置关注按钮
     */
    public void setFocusButton() {

        switch (mUserdata.getIs_following()) {
            case "0":               //未关注
                adiary_list_focus.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                break;
            case "1":               //已关注
                adiary_list_focus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                break;
            case "2":               //互相关注
                adiary_list_focus.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                break;
        }
    }


    /**
     * 关注和取消关注
     */
    private void FocusAndCancel() {
        mFocusAndCancelApi.addData("objid", mUserdata.getObj_id());
        mFocusAndCancelApi.addData("type", mUserdata.getObj_type());
        mFocusAndCancelApi.getCallBack(mContext, mFocusAndCancelApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData mFocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mFocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mUserdata.setIs_following("0");
                                adiary_list_focus.setFocusType(FocusButton2.FocusType.NOT_FOCUS);
                                MyToast.makeTextToast1(mContext, "取关成功", MyToast.SHOW_TIME).show();
                                break;
                            case "1":          //已关注
                                mUserdata.setIs_following("1");
                                adiary_list_focus.setFocusType(FocusButton2.FocusType.HAS_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                            case "2":          //互相关注
                                mUserdata.setIs_following("2");
                                adiary_list_focus.setFocusType(FocusButton2.FocusType.EACH_FOCUS);
                                MyToast.makeTextToast1(mContext, serverData.message, MyToast.SHOW_TIME).show();
                                break;
                        }

                        //关注按钮点击
                        adiary_list_focus.setFocusClickListener(mUserdata.getIs_following(), new FocusButton2.ClickCallBack() {
                            @Override
                            public void onClick(View v) {
                                YmStatistics.getInstance().tongjiApp(followClickData);
                                FocusAndCancel();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    protected void initData() {
        mShareDataApi = new InitShareDataApi();
        mCollectApi = new BaikeFourApi();
        mCancelCollectApi = new CancelCollectApi();
        mPointLikeApi = new ZanOrJuBaoApi();
        mDeleteBBsApi = new DeleteBBsApi();
        mFocusAndCancelApi = new FocusAndCancelApi();

        String illPrompt = mFunctionManager.loadStr(BBS_ILLUSTRATED_PROMPT, "0");
        if ("0".equals(illPrompt)) {
            if (diaryOrPost() && Utils.isLogin()) {
                tipHuabao.setVisibility(View.VISIBLE);
            } else {
                tipHuabao.setVisibility(View.GONE);
            }
        } else {
            tipHuabao.setVisibility(View.GONE);
        }

        initViewData();
    }

    @Override
    public void onBackPressed() {
        if (diariesAndDetailsFragment != null && diariesAndDetailsFragment.mUserdata != null) {
            Intent intent = new Intent();
            intent.putExtra("is_following", diariesAndDetailsFragment.mUserdata.getIs_following());
            setResult(100, intent);
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", mDiaryId);
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_BACK_CLICK), hashMap, new ActivityTypeData("45"));
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (editDialog != null) {
            editDialog.dismiss();
        }
        mHandler.removeCallbacksAndMessages(null);
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();
    }

    public interface OnEventClickListener {

        void onDeleteClick(DiariesDeleteData deleteData);   //删除点击回调
    }

    private PostTopTitleFragment.OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(PostTopTitleFragment.OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    /**
     * 初始化布局数据
     */
    private void initViewData() {

        //关闭
        webViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //日记日记本
        if (diaryOrPost()) {
            viewCollectionClick.setVisibility(View.VISIBLE);
            diariesAndDetailsFragment = DiariesAndDetailsFragment.newInstance(mDiaryId, isNew + "");
            //事件点击回调
            diariesAndDetailsFragment.setOnEventClickListener(new DiariesAndDetailsFragment.OnEventClickListener() {
                /**
                 * 数据加载完后的回调
                 * @param data
                 */
                @Override
                public void onLoadedClick(DiaryListData data) {
                    if (mContentClick.getVisibility() == View.GONE) {
                        mContentClick.setVisibility(View.VISIBLE);
                    }
                    mChatData = data.getChatData();
                    selfPageType = data.getSelfPageType();
                    mAskorshare = data.getAskorshare();
                    mIsCollect = data.getIs_collect();
                    p_id = data.getP_id();
//                    mTaoData = data.getTaoData();
                    addTongjiEvent(data.getP_id(), data.getAskorshare());
                    loadShareData(data.getShareClickData());
                    if (isDiaryOrDetails()) {
                        ll_post_detail.setVisibility(View.GONE);
                        mTop.setVisibility(View.VISIBLE);
                    } else {
                        initPostsDetailTitle(data, null);
                        ll_post_detail.setVisibility(View.VISIBLE);
                        mTop.setVisibility(View.GONE);
                    }
                    if (data.getPic().size() == 0) {
                        //初始化标题
                        if (isDiaryOrDetails()) {
                            titleName.setText("日记本详情");
                        } else {
                            titleName.setText("日记详情");
                        }
                        mTopBackground.setAlpha(1);
//                        diariesPosts.setPadding(0, mTop.getHeight(), 0, 0);
                        setTopIsTransparent(false);
                    } else {
                        mTopBackground.setAlpha(0);
                        setTopIsTransparent(true);
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) diariesAndDetailsFragment.mListSuckTop.getLayoutParams();
                        layoutParams.topMargin = mTop.getHeight();
                        diariesAndDetailsFragment.mListSuckTop.setLayoutParams(layoutParams);
                    }
//                    addTongjiEvent(data.getP_id(), data.getAskorshare());
//                    loadShareData(data.getShareClickData());

                    mTitleTxt.setText(data.getChatData().getShow_message());

                    setCollection();
                    setTopTitle();
                    ExposureLoginData exposureLoginData = new ExposureLoginData(selfPageType, mDiaryId);
                    Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));
                }

                /**
                 * 收藏
                 * @param b
                 */
                @Override
                public void onCollectionClick(boolean b) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (b) {
                            collectDiary();
                        } else {
                            deleCollectDiary();
                        }
                    }
                }

                /**
                 * 点赞
                 * @param data
                 */
                @Override
                public void onLikeClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        pointAndReportLike(data);
                    }
                }

                /**
                 * 删除日记或者评论的回调
                 * @param deleteData
                 */
                @Override
                public void onItemDeleteClick(DiariesDeleteData deleteData) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if ("1".equals(deleteData.getReplystr())) {
                            showDialogExitEdit("确定要删除此评论吗?", deleteData);
                        } else {
                            showDialogExitEdit("确定要删除此日记吗?", deleteData);
                        }
                    }
                }

                /**
                 * 举报回调
                 * @param data
                 */
                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit1("确定要举报此内容?", data);
                    }
                }

                /**
                 * 日记或者日记本不存在
                 * @param message
                 */
                @Override
                public void onEvent404Click(String message) {
                    titleName.setText(message);
                    setActivityFragment(R.id.activity_diaries_and_posts, DiariesAndDetailsNoThereFragment.newInstance());
                }

                /**
                 * 滚动回调
                 * @param t
                 */
                @Override
                public void onScrollChanged(int t, int height, DiaryListData data) {
                    if (data.getPic().size() != 0) {
                        String title;
                        if (isDiaryOrDetails()) {
                            title = "日记本详情";
                        } else {
                            title = "日记详情";
                        }
                        setTopAlpha(t, height, title);
                    }

                    setRightBottomDocShow();
                }
            });

            setActivityFragment(R.id.activity_diaries_and_posts, diariesAndDetailsFragment);

        } else {

            diariesPostsFragment = DiariesPostsFragment648.newInstance(mDiaryId, isNew + "");
            // dsfsdfsdfds
            //点击事件回调
            diariesPostsFragment.setOnEventClickListener(new DiariesPostsFragment648.OnEventClickListener() {

                /**
                 * 数据加载完成后的回调
                 * @param data
                 */
                @Override
                public void onLoadedClick(PostListData data) {
                    if (mContentClick.getVisibility() == View.GONE) {
                        mContentClick.setVisibility(View.VISIBLE);
                    }
                    mChatData = data.getChatData();
                    mAskorshare = data.getAskorshare();
                    selfPageType = data.getSelfPageType();
                    mIsCollect = data.getIs_collect();
                    p_id = data.getP_id();
                    addTongjiEvent(data.getP_id(), data.getAskorshare());
                    loadShareData(data.getShareClickData());
                    initPostsDetailTitle(null, data);
                    ll_post_detail.setVisibility(View.VISIBLE);
                    mTop.setVisibility(View.GONE);
                    if (TextUtils.isEmpty(data.getLoadHtmlUrl())) {
                        if (data.getPic().size() == 0) {
//                        titleName.setText(data.getTitle());                                //初始化标题
                            mTopBackground.setAlpha(1);
                            diariesPosts.setPadding(0, mTop.getHeight(), 0, 0);
                            setTopIsTransparent(false);
                        } else {
                            mTopBackground.setAlpha(0);
                            setTopIsTransparent(true);
                        }

                    } else {
                        titleName.setText(data.getTitle());                                //初始化标题
                        mTopBackground.setAlpha(1);
                        diariesPosts.setPadding(0, mTop.getHeight(), 0, 0);
                        setTopIsTransparent(false);
                    }

//                    addTongjiEvent(data.getP_id(), data.getAskorshare());
//                    loadShareData(data.getShareClickData());
                    if (null != mChatData && !TextUtils.isEmpty(mChatData.getShow_message())) {
                        mTitleTxt.setText(mChatData.getShow_message());
                    }
                    setCollection();
                    setTopTitle();
                    ExposureLoginData exposureLoginData = new ExposureLoginData(selfPageType, mDiaryId);
                    Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));
                }

                /**
                 * 收藏
                 * @param b
                 */
                @Override
                public void onCollectionClick(boolean b) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (b) {
                            collectDiary();
                        } else {
                            deleCollectDiary();
                        }
                    }
                }

                /**
                 * 点赞
                 * @param data
                 */
                @Override
                public void onLikeClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        pointAndReportLike(data);
                    }
                }

                /**
                 * 删除
                 * @param deleteData
                 */
                @Override
                public void onItemDeleteClick(DiariesDeleteData deleteData) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if ("1".equals(deleteData.getReplystr())) {
                            showDialogExitEdit("确定要删除此评论吗?", deleteData);
                        } else {
                            if (deleteData.isDiaries()) {
                                showDialogExitEdit("确定要删除此日记吗?", deleteData);
                            } else {
                                showDialogExitEdit("确定要删除本帖吗?", deleteData);
                            }
                        }
                    }
                }

                /**
                 * 举报
                 * @param data
                 */
                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit1("确定要举报此内容?", data);
                    }
                }

                /**
                 * 帖子不存在
                 * @param message
                 */
                @Override
                public void onEvent404Click(String message) {
                    titleName.setText(message);
                    setActivityFragment(R.id.activity_diaries_and_posts, DiariesAndDetailsNoThereFragment.newInstance());
                }

                /**
                 * 滚动回调
                 * @param t
                 */
                @Override
                public void onScrollChanged(int t, int height, PostListData data) {
                    if (TextUtils.isEmpty(data.getLoadHtmlUrl())) {
                        if (data.getPic().size() != 0 && height != 0) {
                            setTopAlpha(t, height, data.getTitle());
                        }
                    } else {
                        setRightBottomDocShow();
                    }
                }
            });
            setActivityFragment(R.id.activity_diaries_and_posts, diariesPostsFragment);
        }

    }

    /**
     * 设置头部标题
     */
    private void setTopTitle() {
        if ("11".equals(mAskorshare)) {
            viewShareClick.setVisibility(View.INVISIBLE);
            titleName.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));//加粗
        } else {
            viewShareClick.setVisibility(View.VISIBLE);
            titleName.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));//常规
        }
    }

    /**
     * 设置右下角显示隐藏
     */
    private void setRightBottomDocShow() {
        if (!mChatFlag) {
            String today = Utils.getDay();
            String srt = mFunctionManager.loadStr(FinalConstant.DIARIES_POST_DOC_SHOW, "");
            if (!TextUtils.isEmpty(srt)) {
                DiariesPostShowData diariesPostShowData = mGson.fromJson(srt, DiariesPostShowData.class);
                String data = diariesPostShowData.getData();
                int number = diariesPostShowData.getNumber();
                if (!data.equals(today) || number < 3) {
                    mHandler.sendEmptyMessageDelayed(1, 800);
                    mContentClick.setVisibility(View.VISIBLE);
                    mChatFlag = true;
                    String toJson;
                    if (!data.equals(today)) {
                        toJson = mGson.toJson(new DiariesPostShowData(today, 1));
                    } else {
                        number++;
                        toJson = mGson.toJson(new DiariesPostShowData(today, number));
                    }
                    mFunctionManager.saveStr(FinalConstant.DIARIES_POST_DOC_SHOW, toJson);
                } else {
                    mChatFlag = true;
                }
            } else {
                mHandler.sendEmptyMessageDelayed(1, 800);
                mContentClick.setVisibility(View.VISIBLE);
                mChatFlag = true;
                String toJson = mGson.toJson(new DiariesPostShowData(today, 1));
                mFunctionManager.saveStr(FinalConstant.DIARIES_POST_DOC_SHOW, toJson);
            }
        }
    }

    /**
     * 设置收藏
     */
    private void setCollection() {
        if ("1".equals(mIsCollect)) {
            mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_selected_bai);
        } else {
            if (isTransparent) {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_bai);
            } else {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_pink);
            }
        }
    }

    /***
     * 设置头部渐变
     * @param t
     * @param height
     * @param title
     */
    private void setTopAlpha(int t, int height, String title) {
        height = height - mTop.getHeight() < 0 ? 0 : height - mTop.getHeight();
        //设置标题其透明度
        float alpha;
        if (t >= height) {
            alpha = 1;
            if (TextUtils.isEmpty(titleName.getText().toString())) {
                if (diaryOrPost()) {
                    titleName.setText(title);
                }
                setTopIsTransparent(false);
            }
        } else {
            int i = height / 2;
            if (t >= i) {
                alpha = (t - i) / (i * 1.0f); //产生渐变效果
                if (diaryOrPost()) {
                    if (alpha > 0.5) {
                        titleName.setText(title);
                    } else {
                        titleName.setText("");
                    }
                }

                setTopIsTransparent(false);
            } else {
                alpha = 0;
                if (diaryOrPost()) {
                    titleName.setText("");
                }
                setTopIsTransparent(true);
            }

        }
        mTopBackground.setAlpha(alpha);
    }

    /**
     * 设置是否是透明的
     *
     * @param isTransparent ：是透明的
     */
    private void setTopIsTransparent(boolean isTransparent) {
        this.isTransparent = isTransparent;
        if (isTransparent) {
            if ("1".equals(mIsCollect)) {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_selected_bai);
            } else {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_bai);
            }
            mFunctionManager.setImgSrc(postsTopBackImg, R.drawable.ic_arrow_back_white_24dp);
            mFunctionManager.setImgSrc(postTopShareImg, R.drawable.share_bai);

            mFunctionManager.setImgBackground(viewCollectionImg, R.drawable.shape_99444444);
            mFunctionManager.setImgBackground(postsTopBackImg, R.drawable.shape_99444444);
            mFunctionManager.setImgBackground(postTopShareImg, R.drawable.shape_99444444);
        } else {
            if ("1".equals(mIsCollect)) {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_selected_bai);
            } else {
                mFunctionManager.setImgSrc(viewCollectionImg, R.drawable.collection_pink);
            }
            mFunctionManager.setImgSrc(postsTopBackImg, R.drawable.back_black);
            mFunctionManager.setImgSrc(postTopShareImg, R.drawable.share_balck);

            mFunctionManager.setImgBackground(viewCollectionImg, R.drawable.shape_transparent);
            mFunctionManager.setImgBackground(postsTopBackImg, R.drawable.shape_transparent);
            mFunctionManager.setImgBackground(postTopShareImg, R.drawable.shape_transparent);
        }
    }

    private ServerData mServerData;

    /**
     * 加载分享数据
     */
    private void loadShareData(final HashMap<String, String> shareClickData) {
        mShareDataApi.addData("id", mDiaryId);
        mShareDataApi.getCallBack(mContext, mShareDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                mServerData = serverData;
                if ("1".equals(serverData.code)) {
                    BBsShareData bbsShareData = JSONUtil.TransformSingleBean(serverData.data, BBsShareData.class);
                    shareUrl = bbsShareData.getUrl();
                    shareContent = bbsShareData.getContent();
                    shareImgUrl = bbsShareData.getImg();
                    askOrshare1 = bbsShareData.getAskorshare();
                    mWechat = bbsShareData.getWechat();
                    mPictorial = bbsShareData.getPictorial();

                    myShareBoardlistener = new MyShareBoardlistener(mContext, mWechat, mPictorial);
                    buttomDialogView = new ButtomDialogView.Builder(mContext).setShareBoardListener(myShareBoardlistener).setIscancelable(true).setIsBackCancelable(true).build();

                    videoShareData = new VideoShareData().setUrl(shareUrl).setTitle(shareContent).setContent(shareContent).setImg_weibo(shareImgUrl).setImg_weix(shareImgUrl).setAskorshare(askOrshare1);

                    //分享的点击
                    viewShareClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            YmStatistics.getInstance().tongjiApp(shareClickData);
                            setShare();
                        }
                    });

                    iv_share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            YmStatistics.getInstance().tongjiApp(shareClickData);
                            setShare();
                        }
                    });

                    //收藏点击
                    viewCollectionClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }

                            if (Utils.isLoginAndBind(mContext)) {
                                if ("0".equals(mIsCollect)) {
                                    collectDiary();
                                } else {
                                    deleCollectDiary();
                                }
                            }
                        }
                    });
                }

            }
        });
    }


    void addTongjiEvent(String pid, String askorshare) {
        Log.e(TAG, "askorshare === " + askorshare);
        switch (askorshare) {
            case "0": //问答
                Utils.tongjiApp(mContext, "post_ask", "post", mDiaryId, "10");
                break;
            case "1": //日记
                if ("0".equals(pid)) {
                    Utils.tongjiApp(mContext, "post_diary", "post", mDiaryId, "10");
                } else {
                    Utils.tongjiApp(mContext, "post_share", "post", mDiaryId, "10");
                }
                break;
            case "3": //达人发帖/活动贴
                Utils.tongjiApp(mContext, "post_activity", "post", mDiaryId, "10");
                break;
            case "4": //随聊
                Utils.tongjiApp(mContext, "post_chat", "post", mDiaryId, "10");
                break;
            case "5": //医生案例
                Utils.tongjiApp(mContext, "post_case", "post", mDiaryId, "10");
                break;
            case "6": //医生文章
                Utils.tongjiApp(mContext, "post_article", "post", mDiaryId, "10");
                break;
        }
    }

    /**
     * 收藏接口
     */
    private void collectDiary() {
        mCollectApi.addData("objid", mDiaryId);
        mCollectApi.addData("type", "2");

        mCollectApi.getCallBack(mContext, mCollectApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mIsCollect = "1";
                setCollection();
                if (!serverData.isOtherCode) {
                    MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "收藏成功", MyToast.SHOW_TIME).show();
                }

            }
        });
    }


    /**
     * 取消收藏接口
     */
    private void deleCollectDiary() {
        mCancelCollectApi.addData("objid", mDiaryId);
        mCancelCollectApi.addData("type", "2");
        mCancelCollectApi.getCallBack(mContext, mCancelCollectApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mIsCollect = "0";
                setCollection();
                if (!serverData.isOtherCode) {
                    MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "取消收藏", MyToast.SHOW_TIME).show();
                }
            }
        });
    }

    /**
     * 删除日记 或 回复
     *
     * @param deleteData
     */
    private void deleteDiaries(final DiariesDeleteData deleteData) {
        Log.e(TAG, "deleteData.getId() == " + deleteData.getId());
        Log.e(TAG, "deleteData.getReplystr() == " + deleteData.getReplystr());

        mDeleteBBsApi.getHashMap().put("id", deleteData.getId());
        mDeleteBBsApi.getHashMap().put("reply", deleteData.getReplystr());
        mDeleteBBsApi.getCallBack(mContext, mDeleteBBsApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String message) {
                mFunctionManager.showShort(message);
            }
        });
    }

    /**
     * 日记本点赞和举报请求接口
     *
     * @param data
     */
    private void pointAndReportLike(final DiariesReportLikeData data) {
        String puid = "0";
        if (diariesAndDetailsFragment != null) {
            diariesAndDetailsFragment.getmData().getP_id();
        } else if (diariesPostsFragment != null) {
            diariesPostsFragment.getmData().getP_id();
        } else {
            puid = "0";
        }

        final String flag = data.getFlag();
        String isReply = data.getIs_reply();
        if ("2".equals(isReply)) {
            isReply = "0";
        }

        Log.e(TAG, "flag == " + flag);
        Log.e(TAG, "data.getId() == " + data.getId());
        Log.e(TAG, "isReply == " + isReply);
        Log.e(TAG, "puid == " + puid);

        mPointLikeApi.addData("flag", flag);
        mPointLikeApi.addData("id", data.getId());
        mPointLikeApi.addData("is_reply", isReply);
        mPointLikeApi.addData("puid", puid);
        mPointLikeApi.getCallBack(mContext, mPointLikeApi.getHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if (!serverData.isOtherCode) {
                    mFunctionManager.showShort(serverData.message);
                }
                if ("1".equals(flag)) {
                    if ("1".equals(serverData.code)) {
                        String is_agree = JSONUtil.resolveJson(serverData.data, "is_agree");
                        giveLike(is_agree, data);
                    }
                }
            }
        });
    }

    /**
     * 点赞举报处理
     *
     * @param is_agree : 0取消赞成功 1点赞成功
     * @param data
     */
    @SuppressLint("SetTextI18n")
    private void giveLike(String is_agree, DiariesReportLikeData data) {
        String id = data.getId();
        String isReply = data.getIs_reply();
        int pos = data.getPos();
        String flag = data.getFlag();
        if ("1".equals(flag)) {            //如果是点赞
            switch (isReply) {             //日记列表
                case "0":
                    if (!id.equals(mDiaryId)) {                //日记列表点赞
                        if (diariesAndDetailsFragment.getDiaryListFragment() != null) {
                            DiaryRecyclerAdapter mDiaryRecyclerAdapter = diariesAndDetailsFragment.getDiaryListFragment().mDiaryRecyclerAdapter;
                            List<DiaryListListData> diaryListListData = mDiaryRecyclerAdapter.getmDatas();
                            int agreeNum = Integer.parseInt(diaryListListData.get(pos).getAgree_num());
                            if ("1".equals(is_agree)) {
                                agreeNum++;
                            } else {
                                agreeNum--;
                            }
                            diaryListListData.get(pos).setIs_agree(is_agree);
                            diaryListListData.get(pos).setAgree_num(agreeNum + "");
                            mDiaryRecyclerAdapter.notifyItemChanged(pos, "like");
                        }
                    }
                    break;
                case "1":               //评论列表点赞
                    if (diaryOrPost()) {
                        CommentsRecyclerAdapter mCommentsRecyclerAdapter = diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter;
                        List<DiaryReplyList> mDatas = mCommentsRecyclerAdapter.getmDatas();

                        int agreeNum = Integer.parseInt(mDatas.get(pos).getAgree_num());
                        if ("1".equals(is_agree)) {
                            agreeNum++;
                        } else {
                            agreeNum--;
                        }

                        mDatas.get(pos).setIs_agree(is_agree);
                        mDatas.get(pos).setAgree_num(agreeNum + "");
                        mCommentsRecyclerAdapter.notifyItemChanged(pos, "like");
                    } else {
                        PostContentFragment postContentFragment = diariesPostsFragment.getPostContentFragment();
                        if (postContentFragment != null) {

                            CommentsRecyclerAdapter mCommentsRecyclerAdapter = postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter;

                            List<DiaryReplyList> mDatas = mCommentsRecyclerAdapter.getmDatas();
                            int agreeNum = Integer.parseInt(mDatas.get(pos).getAgree_num());
                            if ("1".equals(is_agree)) {
                                agreeNum++;
                            } else {
                                agreeNum--;
                            }

                            mDatas.get(pos).setIs_agree(is_agree);
                            mDatas.get(pos).setAgree_num(agreeNum + "");
                            mCommentsRecyclerAdapter.notifyItemChanged(pos, "like");

                        } else {
                            //H5页面刷新
                            PostHtmlFragment postHtmlFragment = diariesPostsFragment.getPostHtmlFragment();
                            if (postHtmlFragment != null) {
                                postHtmlFragment.notifyWebView();
                            }
                        }
                    }
                    break;
                case "2":               //吸底列表点赞
                    PostBottomFragment postBottomFragment;

                    if (diaryOrPost()) {
                        postBottomFragment = diariesAndDetailsFragment.getPostBottomFragment();
                    } else {
                        postBottomFragment = diariesPostsFragment.getPostBottomFragment();
                    }

                    if ("1".equals(is_agree)) {
                        postBottomFragment.mAgreeNum++;
                        postBottomFragment.isAgree = "1";
                    } else {
                        postBottomFragment.mAgreeNum--;
                        postBottomFragment.isAgree = "0";
                    }

                    //刷新ui
                    postBottomFragment.setAgree();

                    break;
            }
        }
    }

    /**
     * 分享设置
     */
    private void setShare() {

        myShareBoardlistener
                .setSinaText("#整形#" + shareContent + shareUrl + "分享自@悦美整形APP")
                .setSinaThumb(new UMImage(mContext, shareImgUrl))
                .setSmsText(shareContent + "，" + shareUrl)
                .setTencentUrl(shareUrl)
                .setTencentTitle(shareContent)
                .setTencentThumb(new UMImage(mContext, shareImgUrl))
                .setTencentDescription(shareContent)
                .setTencentText(shareContent)
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {
                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            if (!mServerData.isOtherCode) {
                                mFunctionManager.showShort("分享成功啦");
                            }
                            if (Utils.isLogin()) {
                                Utils.sumitHttpCode(mContext, "9");     //增加积分
                            }
                        }
                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                        mFunctionManager.showShort("分享失败啦");
                    }
                });

        buttomDialogView.show();

        //画报分享提示
        if (mPictorial != null) {
            String illPrompt = mFunctionManager.loadStr(BBS_ILLUSTRATED_PROMPT, "0");
            if ("0".equals(illPrompt)) {
                tipHuabao.setVisibility(View.GONE);
                buttomDialogView.setDialogTipVisible();
                mFunctionManager.saveStr(BBS_ILLUSTRATED_PROMPT, "1");
            } else {
                buttomDialogView.setDialogTipGone();
            }
        }
    }

    /**
     * 删除提示框
     *
     * @param content    ：提示框文案
     * @param deleteData ：要删除的参数
     */
    private void showDialogExitEdit(String content, final DiariesDeleteData deleteData) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //删除
                deleteDiaries(deleteData);
                giveDelete(deleteData);

                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

    }

    /**
     * 刷新删除后的列表
     *
     * @param deleteData
     */
    private void giveDelete(DiariesDeleteData deleteData) {
        if (diaryOrPost()) {
            if (deleteData.isZhuTie()) {
                setActivityFragment(R.id.activity_diaries_and_posts, DiariesAndDetailsNoThereFragment.newInstance());
            } else {
                int pos = deleteData.getPos();
                int posPos = deleteData.getPosPos();
                if ("1".equals(deleteData.getReplystr())) {
                    if (deleteData.getCommentsOrReply() == 0) {
                        int deleteCount = 0;
                        //一楼的删除
                        //判断有没有回复
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas() != null
                                && diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().get(pos).getList() != null) {
                            deleteCount = diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().get(pos).getList().size() + 1;
                        } else {
                            deleteCount = 1;
                        }
                        diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.deleteItem(pos);

                        //热评数删除
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum != 0) {
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum = diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum - deleteCount;
                        }
                        //判断是不是负数
                        if(diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum < 0){
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum = 0;
                        }
                        diariesAndDetailsFragment.getCommentsAndRecommendFragment().setComments();

                        //吸底评论数删除
                        if (diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum != 0) {
                            diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum = diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum - deleteCount;
                        }
                        //判断是不是负数
                        if(diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum < 0){
                            diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum = 0;
                        }

                        diariesAndDetailsFragment.getPostBottomFragment().setComments();
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().size() == 0) {
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerContainer.setVisibility(View.GONE);
                        }
                    } else {
                        //二楼的删除
                        diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).deleteItem(posPos);
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).getmDatas().size() == 0) {
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.notifyItem(pos);
                        }

                        //热评数删除
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum != 0) {
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum--;
                        }
                        //判断是不是负数
                        if(diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum < 0){
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mAnswerNum = 0;
                        }
                        diariesAndDetailsFragment.getCommentsAndRecommendFragment().setComments();

                        //吸底评论数删除
                        if (diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum != 0) {
                            diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum--;
                        }
                        //判断是不是负数
                        if(diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum < 0){
                            diariesAndDetailsFragment.getPostBottomFragment().mAnswerNum = 0;
                        }
                        diariesAndDetailsFragment.getPostBottomFragment().setComments();
                        if (diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().size() == 0) {
                            diariesAndDetailsFragment.getCommentsAndRecommendFragment().mCommentsRecyclerContainer.setVisibility(View.GONE);
                        }
                    }
                } else {
                    //删除标签
                    List<DiaryListListPic> pics = diariesAndDetailsFragment.getDiaryListFragment().mDiaryRecyclerAdapter.getmDatas().get(pos).getPic();
                    if (pics.size() == 1) {
                        diariesAndDetailsFragment.getDiaryListFragment().mDiaryFlowLayout.deleteData(pics.get(0).getIs_video());
                    } else {
                        diariesAndDetailsFragment.getDiaryListFragment().mDiaryFlowLayout.deleteData("0");
                    }
                    diariesAndDetailsFragment.getDiaryListFragment().mDiaryRecyclerAdapter.deleteItem(pos);
                    if (diariesAndDetailsFragment.getDiaryListFragment().mDiaryRecyclerAdapter.getmDatas().size() == 0) {
                        setActivityFragment(R.id.activity_diaries_and_posts, DiariesAndDetailsNoThereFragment.newInstance());
                    }
                }
            }

        } else {
            if (deleteData.isZhuTie()) {
                setActivityFragment(R.id.activity_diaries_and_posts, DiariesAndDetailsNoThereFragment.newInstance());
            } else {
                int pos = deleteData.getPos();
                int posPos = deleteData.getPosPos();
                if ("1".equals(deleteData.getReplystr())) {
                    PostContentFragment postContentFragment = diariesPostsFragment.getPostContentFragment();
                    if (postContentFragment != null) {
                        if (deleteData.getCommentsOrReply() == 0) {
                            //判断有没有回复
                            int deleteCount = 0;
                            if (postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas() != null
                                    && postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().get(pos).getList() != null) {
                                deleteCount = postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().get(pos).getList().size() + 1;
                            } else {
                                deleteCount = 1;
                            }

                            //一楼的删除
                            postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.deleteItem(pos);

                            //热评数删除
                            if (postContentFragment.getCommentsAndRecommendFragment().mAnswerNum != 0) {
                                postContentFragment.getCommentsAndRecommendFragment().mAnswerNum = postContentFragment.getCommentsAndRecommendFragment().mAnswerNum - deleteCount;
                            }
                            //判断是不是负数
                            if(postContentFragment.getCommentsAndRecommendFragment().mAnswerNum < 0){
                                postContentFragment.getCommentsAndRecommendFragment().mAnswerNum = 0;
                            }

                            postContentFragment.getCommentsAndRecommendFragment().setComments();

                            //吸底评论数删除
                            if (diariesPostsFragment.getPostBottomFragment().mAnswerNum != 0) {
                                diariesPostsFragment.getPostBottomFragment().mAnswerNum = diariesPostsFragment.getPostBottomFragment().mAnswerNum - deleteCount;
                            }
                            //判断是不是负数
                            if(diariesPostsFragment.getPostBottomFragment().mAnswerNum < 0){
                                diariesPostsFragment.getPostBottomFragment().mAnswerNum = 0;
                            }

                            diariesPostsFragment.getPostBottomFragment().setComments();
                            if (postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().size() == 0) {
                                postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerContainer.setVisibility(View.GONE);
                            }
                        } else {
                            //二楼的删除
                            postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).deleteItem(posPos);
                            if (postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).getmDatas().size() == 0) {
                                postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.notifyItem(pos);
                            }

                            //热评数删除
                            if (postContentFragment.getCommentsAndRecommendFragment().mAnswerNum != 0) {
                                postContentFragment.getCommentsAndRecommendFragment().mAnswerNum--;
                            }
                            //判断是不是负数
                            if(postContentFragment.getCommentsAndRecommendFragment().mAnswerNum < 0){
                                postContentFragment.getCommentsAndRecommendFragment().mAnswerNum = 0;
                            }
                            postContentFragment.getCommentsAndRecommendFragment().setComments();

                            //吸底评论数删除
                            if (diariesPostsFragment.getPostBottomFragment().mAnswerNum != 0) {
                                diariesPostsFragment.getPostBottomFragment().mAnswerNum--;
                            }
                            //判断是不是负数
                            if(diariesPostsFragment.getPostBottomFragment().mAnswerNum < 0){
                                diariesPostsFragment.getPostBottomFragment().mAnswerNum = 0;
                            }
                            diariesPostsFragment.getPostBottomFragment().setComments();
                            if (postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerAdapter.getmDatas().size() == 0) {
                                postContentFragment.getCommentsAndRecommendFragment().mCommentsRecyclerContainer.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        //H5页面刷新
                        PostHtmlFragment postHtmlFragment = diariesPostsFragment.getPostHtmlFragment();
                        if (postHtmlFragment != null) {
                            postHtmlFragment.notifyWebView();
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                if (diariesAndDetailsFragment != null && diariesAndDetailsFragment.getCommentsAndRecommendFragment() != null) {
                    diariesAndDetailsFragment.getCommentsAndRecommendFragment().onActivityResult(requestCode, resultCode, data);
                } else if (diariesPostsFragment != null) {
                    PostContentFragment postContentFragment = diariesPostsFragment.getPostContentFragment();
                    PostHtmlFragment postHtmlFragment = diariesPostsFragment.getPostHtmlFragment();
                    if (postContentFragment != null) {
                        CommentsAndRecommendFragment commentsAndRecommendFragment = postContentFragment.getCommentsAndRecommendFragment();
                        if (commentsAndRecommendFragment != null) {
                            commentsAndRecommendFragment.onActivityResult(requestCode, resultCode, data);
                        }
                    } else if (postHtmlFragment != null) {
                        //H5页面刷新
                        postHtmlFragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
                break;
            case 10:
                if (diariesAndDetailsFragment != null) {
                    diariesAndDetailsFragment.onActivityResult(requestCode, resultCode, data);
                } else if (diariesPostsFragment != null) {
                    diariesPostsFragment.onActivityResult(requestCode, resultCode, data);
                }
                break;
        }
    }

    /**
     * 举报提示框
     *
     * @param content ：提示框文案
     * @param data    ：要举报的参数
     */
    private void showDialogExitEdit1(String content, final DiariesReportLikeData data) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //举报
                pointAndReportLike(data);
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 是日记还是帖子
     *
     * @return ：true:日记  false 帖子
     */
    private boolean diaryOrPost() {
        return mUrl != null && mUrl.contains("shareinfo");
    }

    /**
     * 判断是日记还是日记本
     *
     * @return ：true 日记本
     */
    private boolean isDiaryOrDetails() {
        if (!TextUtils.isEmpty(p_id)) {
            return "0".equals(p_id);
        } else {
            return false;
        }
    }

    /**
     * tao的上级来源
     *
     * @return
     */
    public String toTaoPageIdentifier() {
        if (!TextUtils.isEmpty(mAskorshare)) {
            if (isDiaryOrDetails()) {
                return "45";
            } else {
                switch (mAskorshare) {
                    case "0":
                        return "71";
                    case "1":
                        return "72";
                    case "3":
                        return "73";
                    case "4":
                        return "38";
                    case "5":
                        return "74";
                    case "6":
                        return "75";
                    default:
                        return TextUtils.isEmpty(selfPageType) ? "0" : selfPageType;
                }
            }
        } else {
            return "0";
        }
    }

    /**
     * 发送Handler消息
     */
    private static class MyHandler extends Handler {
        private final WeakReference<DiariesAndPostsActivity> mActivity;

        public MyHandler(DiariesAndPostsActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            DiariesAndPostsActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case 1:
                        theActivity.mTitleTxt.setVisibility(View.VISIBLE);
                        theActivity.mHandler.sendEmptyMessageDelayed(2, 3000);
                        break;
                    case 2:
                        if (theActivity.mTitleTxt != null) {
                            theActivity.mTitleTxt.setVisibility(View.GONE);
                        }
                        break;

                }
            }
        }
    }


}
