package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.module.api.NewVotePostApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.EasyAdapter;
import com.module.commonview.adapter.EasyAdapter2;
import com.module.commonview.adapter.GoodsGroupViewpagerAdapter;
import com.module.commonview.adapter.PostRecyclerAdapter;
import com.module.commonview.adapter.PostRecyclerPeopleAdapter;
import com.module.commonview.adapter.RecommGoodSkuAdapter;
import com.module.commonview.adapter.VotePostContentAdapter;
import com.module.commonview.adapter.VotePostContentOrdinaryAdapter;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.module.bean.PostOtherpic;
import com.module.commonview.module.bean.VoteListBean;
import com.module.commonview.other.DiaryHeadSkuItemView;
import com.module.commonview.other.DiaryTopSkuItemView;
import com.module.commonview.view.DiariesAndPostPicView1;
import com.module.commonview.view.PostFlowLayoutGroup;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.UnderlinePageIndicator;
import com.module.commonview.view.ViewPagerPageIndicator;
import com.module.community.controller.activity.VideoListActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.view.MzRatingBar;
import com.module.event.VoteMsgEvent;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.other.activity.ProjectContrastActivity;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by 裴成浩 on 2019/4/24
 */
public class PostContentFragment extends YMBaseFragment {
    @BindView(R.id.tv_location_time)
    TextView tv_location_time;                  //时间地点

    @BindView(R.id.post_list_scroll_refresh)
    public SmartRefreshLayout mScrollRefresh;                    //刷新加载更多
    @BindView(R.id.post_list_scroll)
    public NestedScrollView mScroll;                      //滚动布局

    @BindView(R.id.post_details_viodeo_image_container)
    public DiariesAndPostPicView1 mPicView;                     //视频图片轮播

    @BindView(R.id.post_list_suck_top)
    public ViewPagerPageIndicator mListSuckTop;                      //吸顶商品详情

    @BindView(R.id.diary_list_other_goods_group3)
    ViewPager otherGoodsGroup3;
    @BindView(R.id.diary_list_other_goods_indicator3)
    UnderlinePageIndicator otherGoodsIndicator3;

    @BindView(R.id.post_content_text)
    TextView mPostContentText;                          //内容标题
    @BindView(R.id.post_list_recycler_text)
    RecyclerView mPostTextRecycler;                    //富文本列表
    @BindView(R.id.post_list_recycler_tao)
    RecyclerView mPostTaoRecycler;                    //淘整型列表

    @BindView(R.id.tv_title_post)
    TextView tv_title_post;                       //文中关联title

    @BindView(R.id.post_list_people_text)
    TextView mPostPeopleText;                           //参与人数标题
    @BindView(R.id.post_list_recycler_people)
    RecyclerView mPostPeopleRecycler;                   //参与人数列表

    @BindView(R.id.diary_list_post_tag_container)
    FlowLayout mPostFlowLayout;                         //标签

    @BindView(R.id.post_list_hos_container)
    LinearLayout mPostHosContainer;                     //医院模块
    @BindView(R.id.post_list_goods_hos_img)
    ImageView mPostHosImg;                              //医院logo
    @BindView(R.id.post_list_goods_hos_name)
    TextView mPostHosName;                              //医院名称
    @BindView(R.id.post_list_goods_hos_bar)
    MzRatingBar mPostHosBar;                            //医院评分
    @BindView(R.id.post_list_goods_hos_case)
    TextView mPostHosCase;                              //医院案例
    @BindView(R.id.post_list_goods_hos_address)
    TextView mPostHosAddress;                           //医院地址

    @BindView(R.id.tv_title_vote_state)                 //标题
            TextView tvTitleVoteState;
    @BindView(R.id.tv_single_multiple)                  //投票标题后 是单选还是多选
            TextView tvSingleMultiple;
    @BindView(R.id.ll_to_pk)                            //投票标题后 去PK按钮
            LinearLayout llToPk;
    @BindView(R.id.iv_pic1)                             //两个投票 左图
            ImageView ivPic1;
    @BindView(R.id.tv_title1)                           //左图title
            TextView tvTitle1;
    @BindView(R.id.tv_price1)                           //左图价格
            TextView tvPrice1;
    @BindView(R.id.iv_pic2)                             //右图
            ImageView ivPic2;
    @BindView(R.id.tv_title2)                           //右图title
            TextView tvTitle2;
    @BindView(R.id.tv_price2)                           //右图价格
            TextView tvPrice2;
    @BindView(R.id.tv_button1)                          //左侧投票按钮
            TextView tvButton1;
    @BindView(R.id.tv_button2)                          //右侧投票按钮
            TextView tvButton2;
    @BindView(R.id.ll_vote_button_type1)                //投票按钮整体布局
            LinearLayout llVoteButtonType1;
    @BindView(R.id.iv_vote_progress1)                   //投过之后的显示的比例条 左侧
            ImageView ivVoteProgress1;
    @BindView(R.id.progress_interval)                   //比例条中间view 为了占位
            View progressInterval;
    @BindView(R.id.iv_vote_progress2)                   //投过之后的显示的比例条 左侧
            ImageView ivVoteProgress2;
    @BindView(R.id.tv_support_num1)                     //比例条下的数量 左侧
            TextView tvSupportNum1;
    @BindView(R.id.tv_support_num2)                     //比例条下的数量 右侧
            TextView tvSupportNum2;
    @BindView(R.id.ll_vote_prograss_type1)              //投过之后显示的布局
            LinearLayout llVotePrograssType1;
    @BindView(R.id.ll_vote_type1)                       //两个投票整体布局
            LinearLayout llVoteType1;

    //文字投票
    @BindView(R.id.tv_title_vote_state_ordinary)        //标题
            TextView tvTitleVoteStateOrdinary;
    //    @BindView(R.id.tv_single_multiple_ordinary)        //单选
//            TextView tvSingleMultipleOrdinary;
    @BindView(R.id.tv_button1_ordinary)                 //左边投票按钮
            TextView tvButton1Ordinary;
    @BindView(R.id.tv_button2_ordinary)                 //右边投票按钮
            TextView tvButton2Ordinary;
    @BindView(R.id.ll_vote_button_type1_ordinary)       //投票按钮布局
            LinearLayout llVoteButtonType1Ordinary;
    @BindView(R.id.tv1_progress_type1_ordinary)         //进度上面左侧标题
            TextView tv1ProgressType1Ordinary;
    @BindView(R.id.tv2_progress_type1_ordinary)         //进度上面右侧标题
            TextView tv2ProgressType1Ordinary;
    @BindView(R.id.iv_vote_progress1_ordinary)          //左边进度
            ImageView ivVoteProgress1Ordinary;
    @BindView(R.id.progress_interval_ordinary)          //进度中间的view 为了占位
            View progressIntervalOrdinary;
    @BindView(R.id.iv_vote_progress2_ordinary)          //右边进度
            ImageView ivVoteProgress2Ordinary;
    @BindView(R.id.tv_support_num1_ordinary)            //左边进度百分比
            TextView tvSupportNum1Ordinary;
    @BindView(R.id.tv_support_num2_ordinary)            //右边进度百分比
            TextView tvSupportNum2Ordinary;
    @BindView(R.id.ll_vote_prograss_type1_ordinary)     //进度条整体布局
            LinearLayout llVotePrograssType1Ordinary;
    @BindView(R.id.ll_vote_type1_ordinary)              //两个投票整体布局
            LinearLayout llVoteType1Ordinary;


    @BindView(R.id.rv_vote_type2_ordinary)                       //多个投票recycleview
            RecyclerView rvVoteType2Ordinary;
    @BindView(R.id.tv_vote_type2_ordinary)                       //多个投票部分布局
            TextView tvVoteType2Ordinary;
    @BindView(R.id.ll_vote_type2_ordinary)                       //多个投票整体布局
            LinearLayout llVoteType2Ordinary;


    @BindView(R.id.rv_vote_type2)                       //多个投票recycleview
            RecyclerView rvVoteType2;
    @BindView(R.id.tv_vote_type2)                       //多个投票部分布局
            TextView tvVoteType2;
    @BindView(R.id.ll_vote_type2)                       //多个投票整体布局
            LinearLayout llVoteType2;

    @BindView(R.id.ll_vote_root_ordinary)               //文字投票部分整体布局
            LinearLayout llVoteRootOrdinary;
    @BindView(R.id.ll_vote_root_sku)                    //sku投票部分整体布局
            LinearLayout llVoteRootSku;
    @BindView(R.id.ll_vote_root)                        //投票部分整体布局
            LinearLayout llVoteRoot;

    @BindView(R.id.reply_img)                           //投票下面的评论
            ImageView replyImg;
    @BindView(R.id.reply_tv)
    TextView replyTv;
    @BindView(R.id.ll_reply)
    LinearLayout llReply;

    Unbinder unbinder;
    private PostListData mData;
    private String mDiaryId;

    private DisplayMetrics metric;

    private final int VIDEO_CONTROL = 123;
    private int currentPosition;
    private DiaryHeadSkuItemView diaryHeadSkuItemView;
    private PostHeadFragment postHeadFragment;
    private CommentsAndRecommendFragment commentsAndRecommendFragment;
    private String TAG = "PostContentFragment";
    private PageJumpManager pageJumpManager;
    private List<DiaryTaoData> taoData = new ArrayList<>();
    private NewVotePostApi newVotePostApi;
    private VotePostContentAdapter votePostContentAdapter;
    private VotePostContentOrdinaryAdapter votePostContentOrdinaryAdapter;

    public static PostContentFragment newInstance(PostListData data, String diaryId) {
        PostContentFragment fragment = new PostContentFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", diaryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(VoteMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 3:
                //                    投票帖详情--SKU点击
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("tao_id", (String) msgEvent.getData());
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_CLICK), hashMap, new ActivityTypeData("171"));

                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                intent.putExtra("id", (String) msgEvent.getData());
                intent.putExtra("source", "0");
                intent.putExtra("objid", "0");
                mContext.startActivity(intent);
                break;
            case 6:
                MyToast.makeTextToast2(mContext, "可选已达上限", 1000).show();
                break;
            default:
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_content_posts;
    }

    @Override
    protected void initView(View view) {
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");


        //下拉刷新
        mScrollRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                if (onEventClickListener != null) {
                    onEventClickListener.onScrollRefresh();
                }
            }
        });

        //上拉加载更多
        mScrollRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                if (commentsAndRecommendFragment != null) {
                    commentsAndRecommendFragment.loadCommentsList();
                }
            }
        });

        mScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView nestedScrollView, int i, int i1, int i2, int i3) {
                Log.e(TAG, "t == " + i1);
                if (taoData.size() > 0) {                   //有淘数据
                    if (mPicView.getVisibility() != View.VISIBLE) {
                        if (i1 > 500) {
                            mListSuckTop.setVisibility(View.VISIBLE);
                        } else {
                            mListSuckTop.setVisibility(View.GONE);
                        }
                    } else {
                        int mTopHeight = mPicView.getHeight() - (Utils.dip2px(50) + statusbarHeight);
                        mTopHeight = mTopHeight <= 0 ? 750 : mTopHeight;
                        Log.e(TAG, "mTopHeight == " + mTopHeight);
                        if (i1 > mTopHeight) {
                            mListSuckTop.setVisibility(View.VISIBLE);
                        } else {
                            mListSuckTop.setVisibility(View.GONE);
                        }
                    }
                }

                if (onEventClickListener != null) {
                    onEventClickListener.onScrollChanged(i1, mPicView.getHeight());
                }
            }
        });
//        //滚动监听
//        mScroll.addOnScrollChangedListener(new QMUIObservableScrollView.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged(QMUIObservableScrollView scrollView, int l, int t, int oldl, int oldt) {
//                Log.e(TAG, "t == " + t);
//                if (taoData.size() > 0) {                   //有淘数据
//                    if (mPicView.getVisibility() != View.VISIBLE) {
//                        if (t > 500) {
//                            mListSuckTop.setVisibility(View.VISIBLE);
//                        } else {
//                            mListSuckTop.setVisibility(View.GONE);
//                        }
//                    } else {
//                        int mTopHeight = mPicView.getHeight() - (Utils.dip2px(50) + statusbarHeight);
//                        mTopHeight = mTopHeight <= 0 ? 750 : mTopHeight;
//                        Log.e(TAG, "mTopHeight == " + mTopHeight);
//                        if (t > mTopHeight) {
//                            mListSuckTop.setVisibility(View.VISIBLE);
//                        } else {
//                            mListSuckTop.setVisibility(View.GONE);
//                        }
//                    }
//                }
//
//                if (onEventClickListener != null) {
//                    onEventClickListener.onScrollChanged(t, mPicView.getHeight());
//                }
//            }
//        });

    }

    @Override
    protected void initData(View view) {
        metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        pageJumpManager = new PageJumpManager(mContext);
//        diarySkuItemView = new DiarySkuItemView(mContext, mDiaryId, "3");
        diaryHeadSkuItemView = new DiaryHeadSkuItemView(mContext, mDiaryId, "3");
        initPostView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case VIDEO_CONTROL:    //视频请求回调
                if (resultCode == VideoListActivity.RETURN_VIDEO_PROGRESS) {
                    currentPosition = data.getIntExtra("current_position", 0);
                }
                break;
        }
    }

    /**
     * 初始化帖子ui
     */
    @SuppressLint("SetTextI18n")
    private void initPostView() {
        //设置顶部轮播图
        List<PostOtherpic> pic = mData.getPic();
        if (pic.size() > 0) {
            mPicView.setVisibility(View.VISIBLE);
            mPicView.setData(pic, metric, mData, null);

            mPicView.setOnMaxVideoClickListener(new DiariesAndPostPicView1.OnMaxVideoClickListener() {
                @Override
                public void onMaxVideoClick(String videoUrl, int currentPosition) {
                    Intent intent = new Intent(mContext, VideoListActivity.class);
                    intent.putExtra("id", mDiaryId);
                    intent.putExtra("flag", "1");
                    intent.putExtra("progress", currentPosition);
                    startActivityForResult(intent, VIDEO_CONTROL);
                }
            });
        } else {
            if (mPicView != null) {
                mPicView.setVisibility(View.GONE);
            }
        }

        if (mData.getVote_list() != null) {
            setVoteLayout(mData.getVote_list());
        }

        //顶部淘整形数据
        taoData = mData.getTaoDataList();
        if (taoData.size() == 0) {
            taoData = mData.getContentSkuList();
        }
        if (taoData.size() > 0) {
            //初始化吸顶数据
            if (mContext instanceof DiariesAndPostsActivity) {
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mListSuckTop.getLayoutParams();
                if (mData.getPic().size() != 0) {
                    layoutParams.topMargin = ((DiariesAndPostsActivity) mContext).mTop.getHeight();
                }
                mListSuckTop.setLayoutParams(layoutParams);
            }

            DiaryTopSkuItemView diaryTopSkuItemView = new DiaryTopSkuItemView(mContext, mDiaryId);
            List<View> views = diaryTopSkuItemView.initTaoData(taoData);
            mListSuckTop.initView(views, taoData, Utils.dip2px(70));
            mListSuckTop.setOnItemCallBackListener(new ViewPagerPageIndicator.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos, DiaryTaoData data) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData("postinfo_to_tao", "top"), new ActivityTypeData(mData.getSelfPageType()));
                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", data.getId());
                    if (mContext instanceof DiariesAndPostsActivity) {
                        intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                    } else {
                        intent.putExtra("source", "0");
                    }
                    intent.putExtra("objid", mDiaryId);
                    startActivity(intent);
                }
            });

            diaryTopSkuItemView.setOnEventClickListener(new DiaryTopSkuItemView.OnEventClickListener() {
                @Override
                public void onItemClick(View view, DiaryTaoData data) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("hos_id", data.getHospital_id());
                    hashMap.put("doc_id", data.getDoc_id());
                    hashMap.put("tao_id", data.getId());
                    hashMap.put("id", mDiaryId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL, "share_top", data.getHospital_id()), hashMap, new ActivityTypeData("45"));
                }
            });
        }

        //标题下的tao数据
        List<DiaryTaoData> taoDataList = mData.getTaoDataList();
        if (taoDataList.size() > 0) {

            GoodsGroupViewpagerAdapter adapter3 = new GoodsGroupViewpagerAdapter(diaryHeadSkuItemView.initTaoData(taoDataList, null));
            otherGoodsGroup3.setAdapter(adapter3);

            ViewGroup.LayoutParams layoutParams = otherGoodsGroup3.getLayoutParams();
            layoutParams.height = DensityUtil.dip2px(110);
            otherGoodsGroup3.setLayoutParams(layoutParams);


            adapter3.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {

                    String tao_id = mData.getTaoDataList().get(pos).getId();
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", mDiaryId);
                    hashMap.put("to_page_type", "2");
                    hashMap.put("to_page_id", tao_id);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TO_TAO, "in_the_text"), hashMap, new ActivityTypeData(mData.getSelfPageType()));

                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", tao_id);
                    if (mContext instanceof DiariesAndPostsActivity) {
                        intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                    } else {
                        intent.putExtra("source", "0");
                    }
                    intent.putExtra("objid", mDiaryId);
                    startActivity(intent);
                }
            });
        }
        if (taoDataList.size() > 1) {
            otherGoodsGroup3.setVisibility(View.VISIBLE);
            otherGoodsIndicator3.setVisibility(View.VISIBLE);
            otherGoodsIndicator3.setViewPager(otherGoodsGroup3);
            tv_title_post.setVisibility(View.VISIBLE);
        } else if (taoDataList.size() > 0) {
            otherGoodsGroup3.setVisibility(View.VISIBLE);
            otherGoodsIndicator3.setVisibility(View.GONE);
            tv_title_post.setVisibility(View.VISIBLE);
        } else {
            otherGoodsGroup3.setVisibility(View.GONE);
            otherGoodsIndicator3.setVisibility(View.GONE);
            tv_title_post.setVisibility(View.GONE);
        }
        //设置标题
        postHeadFragment = PostHeadFragment.newInstance(mData, mDiaryId);
        setActivityFragment(R.id.post_data_diary_head, postHeadFragment);

//        postTopTitleFragment = PostTopTitleFragment.newInstance(mData,mDiaryId);
//        setActivityFragment(R.id.fl_title, postTopTitleFragment);

        //删除按钮的回调
        postHeadFragment.setOnEventClickListener(new PostHeadFragment.OnEventClickListener() {
            @Override
            public void onDeleteClick(DiariesDeleteData deleteData) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemDeleteClick(deleteData);
                }
            }
        });

        //设置常规贴和综合帖数据模块
        setViewData();

        //设置底部淘列表数据
        List<DiaryTaoData> contentSkuList = mData.getContentSkuList();
        if (contentSkuList != null && contentSkuList.size() > 0) {
            mPostTaoRecycler.setVisibility(View.VISIBLE);
            ScrollLayoutManager scrollLinearLayoutManager1 = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            scrollLinearLayoutManager1.setScrollEnable(false);
            mPostTaoRecycler.setLayoutManager(scrollLinearLayoutManager1);
            final RecommGoodSkuAdapter recommGoodSkuAdapter = new RecommGoodSkuAdapter(mContext, contentSkuList, mData.getSelfPageType());
            mPostTaoRecycler.setAdapter(recommGoodSkuAdapter);

            //点击事件
            recommGoodSkuAdapter.setItemnClickListener(new RecommGoodSkuAdapter.ItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", recommGoodSkuAdapter.getTaoDatas().get(pos).getId());
                    if (mContext instanceof DiariesAndPostsActivity) {
                        intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                    } else {
                        intent.putExtra("source", "0");
                    }
                    intent.putExtra("objid", mDiaryId);
                    mContext.startActivity(intent);
                }
            });
        } else {
            mPostTaoRecycler.setVisibility(View.GONE);
        }

        //设置推荐评论模块
        commentsAndRecommendFragment = CommentsAndRecommendFragment.newInstance(mData, mDiaryId);
        setActivityFragment(R.id.post_data_recom_comm, commentsAndRecommendFragment);

        //评论推荐模块回调
        commentsAndRecommendFragment.setOnEventClickListener(new CommentsAndRecommendFragment.OnEventClickListener() {
            @Override
            public void onCommentsClick(boolean isZhuTie) {
//                if (isZhuTie) {
                    commentsAndRecommendFragment.mAnswerNum++;
                    commentsAndRecommendFragment.setComments();

                    if (onEventClickListener != null) {
                        onEventClickListener.onCommentsClick();
                    }
//                }
            }

            @Override
            public void onLikeClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onLikeClick(data);
                }
            }

            @Override
            public void onItemDeleteClick(DiariesDeleteData deleteData) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemDeleteClick(deleteData);
                }
            }

            @Override
            public void onItemReportClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemReportClick(data);
                }
            }

            @Override
            public void onLoadMoreClick() {
                mScrollRefresh.finishLoadMore();
            }
        });

    }

    //投票部分
    private void setVoteLayout(final VoteListBean voteListBean) {
        //1sku 2普通
        if (voteListBean.getClassX().equals("1")) {
            //sku投票
            llVoteRoot.setVisibility(View.VISIBLE);
            llVoteRootSku.setVisibility(View.VISIBLE);
            llVoteRootOrdinary.setVisibility(View.GONE);
            if (voteListBean.getOption() != null && voteListBean.getOption().size() == 2) {
                //两个投票
                tvSingleMultiple.setVisibility(View.INVISIBLE);
                llVoteType1.setVisibility(View.VISIBLE);
                llVoteButtonType1.setVisibility(View.VISIBLE);
                llVotePrograssType1.setVisibility(View.GONE);
                llReply.setVisibility(View.GONE);

                Glide.with(mContext)
                        .load(voteListBean.getOption().get(0).getTao().getList_cover_image())
                        .override((DensityUtil.getScreenWidth() - DensityUtil.dip2px(61)) / 2, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(61)) / 2)
                        .transform(new CenterCrop(mContext), new GlideRoundTransform(mContext, Utils.dip2px(5)))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivPic1);
                tvTitle1.setText(voteListBean.getOption().get(0).getTao().getTitle());
                tvPrice1.setText(voteListBean.getOption().get(0).getTao().getSale_price());

                Glide.with(mContext)
                        .load(voteListBean.getOption().get(1).getTao().getList_cover_image())
                        .override((DensityUtil.getScreenWidth() - DensityUtil.dip2px(61)) / 2, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(61)) / 2)
                        .transform(new CenterCrop(mContext), new GlideRoundTransform(mContext, Utils.dip2px(5)))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivPic2);
                tvTitle2.setText(voteListBean.getOption().get(1).getTao().getTitle());
                tvPrice2.setText(voteListBean.getOption().get(1).getTao().getSale_price());
                ivPic1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
//                    投票帖详情--SKU点击
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("tao_id", voteListBean.getOption().get(0).getTao_id());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_CLICK), hashMap, new ActivityTypeData("171"));

                        Intent intent = new Intent(mContext, TaoDetailActivity.class);
                        intent.putExtra("id", voteListBean.getOption().get(0).getTao_id());
                        intent.putExtra("source", "0");
                        intent.putExtra("objid", "0");
                        mContext.startActivity(intent);
                    }
                });
                ivPic2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        //                    投票帖详情--SKU点击
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("tao_id", voteListBean.getOption().get(1).getTao_id());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_CLICK), hashMap, new ActivityTypeData("171"));

                        Intent intent = new Intent(mContext, TaoDetailActivity.class);
                        intent.putExtra("id", voteListBean.getOption().get(1).getTao_id());
                        intent.putExtra("source", "0");
                        intent.putExtra("objid", "0");
                        mContext.startActivity(intent);
                    }
                });

                if (voteListBean.getIs_vote_option().equals("0")) {
                    tvTitleVoteState.setText("投票:" + voteListBean.getVote_title());
                    //没投
                    tvButton1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            postVote(Utils.getUid(), voteListBean.getOption().get(0).getId());
                        }
                    });
                    tvButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            postVote(Utils.getUid(), voteListBean.getOption().get(1).getId());
                        }
                    });
                } else {
                    //已投
                    tvTitleVoteState.setText("已投票:" + voteListBean.getVote_title());
                    llVoteButtonType1.setVisibility(View.GONE);
                    llVotePrograssType1.setVisibility(View.VISIBLE);
                    llReply.setVisibility(View.VISIBLE);
                    if (Utils.isLogin()) {
                        Glide.with(mContext).load(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    } else {
                        Glide.with(mContext).load(mFunctionManager.loadStr(FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    }
                    //间隔显示或者隐藏
                    if (voteListBean.getOption().get(0).getVote_option_rate().equals("1") || voteListBean.getOption().get(1).getVote_option_rate().equals("1")) {
                        progressInterval.setVisibility(View.GONE);
                    } else {
                        progressInterval.setVisibility(View.VISIBLE);
                    }

                    LinearLayout.LayoutParams weight1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, Float.parseFloat(voteListBean.getOption().get(0).getVote_option_rate()));
                    ivVoteProgress1.setLayoutParams(weight1);

                    LinearLayout.LayoutParams weight2 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, Float.parseFloat(voteListBean.getOption().get(1).getVote_option_rate()));
                    ivVoteProgress2.setLayoutParams(weight2);

                    if (voteListBean.getOption().get(0).getIs_vote_option().equals("0")) {
                        //左侧未选中
                        tvSupportNum1.setText(voteListBean.getOption().get(0).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(0).getVote_option_rate()) * 100) + "%");
                        tvSupportNum2.setText("我已支持  " + voteListBean.getOption().get(1).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(1).getVote_option_rate()) * 100) + "%");
                    } else {
                        //右侧选中
                        tvSupportNum1.setText("我已支持  " + voteListBean.getOption().get(0).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(0).getVote_option_rate()) * 100) + "%");
                        tvSupportNum2.setText(voteListBean.getOption().get(1).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(1).getVote_option_rate()) * 100) + "%");
                    }
                }
            } else {
                llVoteType2.setVisibility(View.VISIBLE);
                //多个投票
                tvSingleMultiple.setVisibility(View.VISIBLE);
                rvVoteType2.setNestedScrollingEnabled(false);
                YMLinearLayoutManager layoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                rvVoteType2.setLayoutManager(layoutManager);
                if (voteListBean.getVote_type().equals("1")) {
                    tvSingleMultiple.setText("(单选)");
                } else {
                    tvSingleMultiple.setText("(多选)");
                }
                if (voteListBean.getIs_vote_option().equals("0")) {
                    //没投
                    tvTitleVoteState.setText("投票:" + voteListBean.getVote_title());
                    tvVoteType2.setText("点击投票");
                    tvVoteType2.setTextColor(Color.parseColor("#FF527F"));
                    tvVoteType2.setBackgroundResource(R.drawable.shape_ff527f_vote_button_type2);

                    votePostContentAdapter = new VotePostContentAdapter(mContext, voteListBean, "0");
                    rvVoteType2.setAdapter(votePostContentAdapter);

                    if (voteListBean.getVote_type().equals("1")) {
                        //单选模式
                        votePostContentAdapter.setSelectMode(EasyAdapter.SelectMode.SINGLE_SELECT);
                    } else {
                        //多选模式
                        votePostContentAdapter.setSelectMode(EasyAdapter.SelectMode.MULTI_SELECT);
                        votePostContentAdapter.setMaxSelectedCount(voteListBean.getOption().size() - 1);
                    }

                    upUiVoteButton();
                    votePostContentAdapter.setOnItemSingleSelectListener(new EasyAdapter.OnItemSingleSelectListener() {
                        @Override
                        public void onSelected(int itemPosition, boolean isSelected) {
                            upUiVoteButton();
                        }
                    });

                    votePostContentAdapter.setOnItemMultiSelectListener(new EasyAdapter.OnItemMultiSelectListener() {
                        @Override
                        public void onSelected(EasyAdapter.Operation operation, int itemPosition, boolean isSelected) {
                            upUiVoteButton();
                        }
                    });

                    tvVoteType2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            if (tvVoteType2.getText().equals("已投票")) {
                                return;
                            }
                            if (!TextUtils.isEmpty(votePostContentAdapter.getSelectId())) {
                                postVote(Utils.getUid(), votePostContentAdapter.getSelectId());
                            }
                        }
                    });
                } else {
                    //已投
                    if (Utils.isLogin()) {
                        Glide.with(mContext).load(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    } else {
                        Glide.with(mContext).load(mFunctionManager.loadStr(FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    }
                    tvTitleVoteState.setText("已投票:" + voteListBean.getVote_title());
                    tvVoteType2.setText("已投票");
                    tvVoteType2.setTextColor(Color.parseColor("#E5E5E5"));
                    tvVoteType2.setBackgroundResource(R.drawable.shape_e5e5e5_vote_button_type2);
                    tvVoteType2.setEnabled(false);
                    llReply.setVisibility(View.VISIBLE);
                    votePostContentAdapter = new VotePostContentAdapter(mContext, voteListBean, "1");
                    rvVoteType2.setAdapter(votePostContentAdapter);
                }
            }
            final ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < voteListBean.getOption().size(); i++) {
                if (!list.contains(voteListBean.getOption().get(i).getTao_id().trim())) {
                    list.add(voteListBean.getOption().get(i).getTao_id().trim());
                }
            }
            llToPk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
//                投票帖详情---去PK
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_PK), new ActivityTypeData("171"));
                    ProjectContrastActivity.invoke(mContext, (StringUtils.strip(list.toString(), "[]").trim()).replace(" ", ""), "6");
                }
            });
            replyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    commentsAndRecommendFragment.setCommentsCallback();
                }
            });
        } else {
            //文字投票
            llVoteRoot.setVisibility(View.VISIBLE);
            llVoteRootSku.setVisibility(View.GONE);
            llVoteRootOrdinary.setVisibility(View.VISIBLE);
            if (voteListBean.getOption() != null && voteListBean.getOption().size() == 2) {
                //两个的投票
                llVoteType1Ordinary.setVisibility(View.VISIBLE);
                llVoteButtonType1Ordinary.setVisibility(View.VISIBLE);
                llVotePrograssType1Ordinary.setVisibility(View.GONE);
                llReply.setVisibility(View.GONE);
                if (voteListBean.getIs_vote_option().equals("0")) {
                    //没投
                    String title = voteListBean.getVote_title();
                    tvTitleVoteStateOrdinary.setText("投票:" + title);

                    tvButton1Ordinary.setText(voteListBean.getOption().get(0).getTitle());
                    tvButton2Ordinary.setText(voteListBean.getOption().get(1).getTitle());
                    tvButton1Ordinary.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            postVote(Utils.getUid(), voteListBean.getOption().get(0).getId());
                        }
                    });
                    tvButton2Ordinary.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            postVote(Utils.getUid(), voteListBean.getOption().get(1).getId());
                        }
                    });
                } else {
                    //已投
                    String title = voteListBean.getVote_title();
                    tvTitleVoteStateOrdinary.setText("已投票:" + title);

                    llVoteButtonType1Ordinary.setVisibility(View.GONE);
                    llVotePrograssType1Ordinary.setVisibility(View.VISIBLE);
                    llReply.setVisibility(View.VISIBLE);
                    //间隔显示或者隐藏
                    if (voteListBean.getOption().get(0).getVote_option_rate().equals("1") || voteListBean.getOption().get(1).getVote_option_rate().equals("1")) {
                        progressIntervalOrdinary.setVisibility(View.GONE);
                    } else {
                        progressIntervalOrdinary.setVisibility(View.VISIBLE);
                    }
                    LinearLayout.LayoutParams weight1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, Float.parseFloat(voteListBean.getOption().get(0).getVote_option_rate()));
                    ivVoteProgress1Ordinary.setLayoutParams(weight1);

                    LinearLayout.LayoutParams weight2 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, Float.parseFloat(voteListBean.getOption().get(1).getVote_option_rate()));
                    ivVoteProgress2Ordinary.setLayoutParams(weight2);

                    tv1ProgressType1Ordinary.setText(voteListBean.getOption().get(0).getTitle());
                    tv2ProgressType1Ordinary.setText(voteListBean.getOption().get(1).getTitle());

                    if (voteListBean.getOption().get(0).getIs_vote_option().equals("0")) {
                        //左侧未选中
                        tvSupportNum1Ordinary.setText(voteListBean.getOption().get(0).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(0).getVote_option_rate()) * 100) + "%");
                        tvSupportNum2Ordinary.setText("我已支持  " + voteListBean.getOption().get(1).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(1).getVote_option_rate()) * 100) + "%");
                    } else {
                        //右侧选中
                        tvSupportNum1Ordinary.setText("我已支持  " + voteListBean.getOption().get(0).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(0).getVote_option_rate()) * 100) + "%");
                        tvSupportNum2Ordinary.setText(voteListBean.getOption().get(1).getVote_num() + "票  " + new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(1).getVote_option_rate()) * 100) + "%");
                    }
                    if (Utils.isLogin()) {
                        Glide.with(mContext).load(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    } else {
                        Glide.with(mContext).load(mFunctionManager.loadStr(FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    }
                }
            } else {
                //多个的投票
                llVoteType2Ordinary.setVisibility(View.VISIBLE);
                rvVoteType2Ordinary.setNestedScrollingEnabled(false);
                YMLinearLayoutManager layoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                rvVoteType2Ordinary.setLayoutManager(layoutManager);
                if (voteListBean.getIs_vote_option().equals("0")) {
                    //没投
                    if (voteListBean.getVote_type().equals("1")) {
                        String title = voteListBean.getVote_title();
                        SpannableString spannableString = new SpannableString("【单选】" + "投票:" + title);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new TypefaceSpan("default"), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 4, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvTitleVoteStateOrdinary.setText(spannableString);
//                        tvTitleVoteStateOrdinary.setText("投票:" + voteListBean.getVote_title());
                    } else {
                        String title = voteListBean.getVote_title();
                        SpannableString spannableString = new SpannableString("【多选】" + "投票:" + title);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new TypefaceSpan("default"), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 4, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvTitleVoteStateOrdinary.setText(spannableString);
//                        tvTitleVoteStateOrdinary.setText("投票:" + voteListBean.getVote_title());
                    }
                    tvVoteType2Ordinary.setText("点击投票");
                    tvVoteType2Ordinary.setTextColor(Color.parseColor("#FF527F"));
                    tvVoteType2Ordinary.setBackgroundResource(R.drawable.shape_ff527f_vote_button_type2);

                    votePostContentOrdinaryAdapter = new VotePostContentOrdinaryAdapter(mContext, voteListBean, "0", voteListBean.getVote_type().equals("1") ? "0" : "1");
                    rvVoteType2Ordinary.setAdapter(votePostContentOrdinaryAdapter);

                    if (voteListBean.getVote_type().equals("1")) {
                        //单选模式
                        votePostContentOrdinaryAdapter.setSelectMode(EasyAdapter2.SelectMode.SINGLE_SELECT);
                    } else {
                        //多选模式
                        votePostContentOrdinaryAdapter.setSelectMode(EasyAdapter2.SelectMode.MULTI_SELECT);
                        votePostContentOrdinaryAdapter.setMaxSelectedCount(voteListBean.getOption().size() - 1);
                    }

                    upUiVoteButton2();
                    votePostContentOrdinaryAdapter.setOnItemSingleSelectListener(new EasyAdapter2.OnItemSingleSelectListener() {
                        @Override
                        public void onSelected(int itemPosition, boolean isSelected) {
                            upUiVoteButton2();
                        }
                    });

                    votePostContentOrdinaryAdapter.setOnItemMultiSelectListener(new EasyAdapter2.OnItemMultiSelectListener() {
                        @Override
                        public void onSelected(EasyAdapter2.Operation operation, int itemPosition, boolean isSelected) {
                            upUiVoteButton2();
                        }
                    });

                    tvVoteType2Ordinary.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }
                            if (tvVoteType2Ordinary.getText().toString().contains("总计")) {
//                            if (tvVoteType2Ordinary.getText().toString().contains("已投票")) {
                                return;
                            }
                            if (!TextUtils.isEmpty(votePostContentOrdinaryAdapter.getSelectId())) {
                                postVote(Utils.getUid(), votePostContentOrdinaryAdapter.getSelectId());
                            }
                        }
                    });
                } else {
                    //已投
                    if (Utils.isLogin()) {
                        Glide.with(mContext).load(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    } else {
                        Glide.with(mContext).load(mFunctionManager.loadStr(FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(replyImg);
                    }
                    if (voteListBean.getVote_type().equals("1")) {
                        String title = voteListBean.getVote_title();
                        SpannableString spannableString = new SpannableString("【单选】" + "已投票:" + title);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new TypefaceSpan("default"), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 4, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvTitleVoteStateOrdinary.setText(spannableString);
                    } else {

                        String title = voteListBean.getVote_title();
                        SpannableString spannableString = new SpannableString("【多选】" + "已投票:" + title);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#999999")), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new TypefaceSpan("default"), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 5, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableString.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 4, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvTitleVoteStateOrdinary.setText(spannableString);
                    }
                    tvVoteType2Ordinary.setText("总计" + voteListBean.getTotal_vote_num() + "票");
                    tvVoteType2Ordinary.setTextColor(Color.parseColor("#BBBBBB"));
                    tvVoteType2Ordinary.setBackgroundResource(R.drawable.shape_e5e5e5_vote_button_type2);
                    tvVoteType2Ordinary.setEnabled(false);
                    llReply.setVisibility(View.VISIBLE);
                    votePostContentOrdinaryAdapter = new VotePostContentOrdinaryAdapter(mContext, voteListBean, "1", voteListBean.getVote_type().equals("1") ? "0" : "1");
                    rvVoteType2Ordinary.setAdapter(votePostContentOrdinaryAdapter);
                }
            }
            replyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    commentsAndRecommendFragment.setCommentsCallback();
                }
            });
        }
    }

    private void upUiVoteButton() {
        if (votePostContentAdapter.isSelectedState()) {
            tvVoteType2.setTextColor(Color.parseColor("#FF527F"));
            tvVoteType2.setBackgroundResource(R.drawable.shape_ff527f_vote_button_type2);
            tvVoteType2.setEnabled(true);
        } else {
            tvVoteType2.setTextColor(Color.parseColor("#E5E5E5"));
            tvVoteType2.setBackgroundResource(R.drawable.shape_e5e5e5_vote_button_type2);
            tvVoteType2.setEnabled(false);
        }
    }

    private void upUiVoteButton2() {
        if (votePostContentOrdinaryAdapter.isSelectedState()) {
            tvVoteType2Ordinary.setTextColor(Color.parseColor("#FF527F"));
            tvVoteType2Ordinary.setBackgroundResource(R.drawable.shape_ff527f_vote_button_type2);
            tvVoteType2Ordinary.setEnabled(true);
        } else {
            tvVoteType2Ordinary.setTextColor(Color.parseColor("#E5E5E5"));
            tvVoteType2Ordinary.setBackgroundResource(R.drawable.shape_e5e5e5_vote_button_type2);
            tvVoteType2Ordinary.setEnabled(false);
        }
    }

    private void postVote(final String user_id, String option_id) {
        mDialog.startLoading();
        newVotePostApi = new NewVotePostApi();
        HashMap<String, Object> maps = new HashMap<String, Object>();
        maps.put("user_id", user_id);
        maps.put("option_id", option_id);
        newVotePostApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (getActivity() != null) {
                    mDialog.stopLoading();
                    if (serverData != null && serverData.code.equals("1")) {
                        VoteListBean voteListBean = JSONUtil.TransformSingleBean(serverData.data, VoteListBean.class);
                        setVoteLayout(voteListBean);
                    } else {
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    /**
     * 设置常规贴和综合帖数据模块
     */
    private void setViewData() {
        DiaryHosDocBean hosDoc = mData.getHosDoc();         //服务的医生医院

        //设置内容标题
        String title = mData.getTitle();
        if (!TextUtils.isEmpty(title)) {
            mPostContentText.setVisibility(View.VISIBLE);
            mPostContentText.setText(title);
        } else {
            mPostContentText.setVisibility(View.GONE);
        }

        //设置富文本列表
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        Log.e(TAG, "mPostTextRecycler === " + mPostTextRecycler);
        mPostTextRecycler.setLayoutManager(scrollLinearLayoutManager);
        PostRecyclerAdapter mPostRecyclerAdapter = new PostRecyclerAdapter(mContext, mData.getContent(), mDiaryId, mData.getSelfPageType());
        mPostTextRecycler.setAdapter(mPostRecyclerAdapter);

        //设置标题
        if (mData.getPeople() != null) {
            mPostPeopleText.setVisibility(View.VISIBLE);
            mPostPeopleRecycler.setVisibility(View.VISIBLE);
            mPostPeopleText.setText(mData.getPeople().getTitle());

            //设置人数列表
            ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 9);
            gridLayoutManager.setScrollEnable(false);
            mPostPeopleRecycler.setLayoutManager(gridLayoutManager);
            PostRecyclerPeopleAdapter postRecyclerPeopleAdapter = new PostRecyclerPeopleAdapter(mContext, mData.getPeople().getPeople());
            mPostPeopleRecycler.setAdapter(postRecyclerPeopleAdapter);

            postRecyclerPeopleAdapter.setOnItemCallBackListener(new PostRecyclerPeopleAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, String userUrl) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    if (!TextUtils.isEmpty(userUrl)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(userUrl, "0", "0");
                    }
                }
            });
        } else {
            mPostPeopleText.setVisibility(View.GONE);
            mPostPeopleRecycler.setVisibility(View.GONE);
        }

        //加载日记标签
        setTagData(hosDoc);
        //加载时间地点
        setLocationAndTime();
        //初始化服务医院
        if (hosDoc != null) {
            initHosDocData(hosDoc);
        }
    }

    private void setLocationAndTime() {
        tv_location_time.setText(mData.getUserdata().getLable());
    }

    /**
     * 设置日记标签
     *
     * @param hosDoc
     */
    private void setTagData(DiaryHosDocBean hosDoc) {
        List<DiaryTagList> tags = mData.getTag();
        //添加医生标签
        if (hosDoc != null) {
            String doctor_id = hosDoc.getDoctor_id();
            String doctor_name = hosDoc.getDoctor_name();
            String title = hosDoc.getTitle();
            Log.e(TAG, "doctor_id == " + doctor_id);
            Log.e(TAG, "doctor_name == " + doctor_name);
            Log.e(TAG, "title == " + title);

            if (!TextUtils.isEmpty(doctor_name)) {
                String docName;
                if (!TextUtils.isEmpty(title)) {
                    docName = doctor_name + " " + title;
                } else {
                    docName = doctor_name;
                }
                DiaryTagList diaryTagList = new DiaryTagList(doctor_id, hosDoc.getDoctor_url(), docName);
                tags.add(diaryTagList);
            }
        }
        if (tags.size() > 0) {
            mPostFlowLayout.setVisibility(View.VISIBLE);
            PostFlowLayoutGroup mPostFlowLayoutGroup = new PostFlowLayoutGroup(mContext, mPostFlowLayout, tags);
            mPostFlowLayoutGroup.setClickCallBack(new PostFlowLayoutGroup.ClickCallBack() {
                @Override
                public void onClick(View v, int pos, DiaryTagList data) {
                    if (!TextUtils.isEmpty(data.getName())) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        if (event_params != null) {
                            event_params.put("id", mDiaryId);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TAG_CLICK, (pos + 1) + ""), event_params);
                            Intent it = new Intent(mContext, SearchAllActivity668.class);
                            it.putExtra("keys", data.getName());
                            it.putExtra("results", true);
                            it.putExtra("position", 2);
                            startActivity(it);

                        } else {
                            Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                            intent.putExtra("docId", data.getId());
                            intent.putExtra("docName", data.getName());
                            intent.putExtra("partId", "");
                            startActivity(intent);
                        }
                    }
                }
            });
        } else {
            mPostFlowLayout.setVisibility(View.GONE);
        }
    }

    /***
     * 初始化服务医院
     * @param mHosDocData
     */
    @SuppressLint("SetTextI18n")
    private void initHosDocData(DiaryHosDocBean mHosDocData) {
        final String hospitalId = mHosDocData.getHospital_id();
        String hospitalName = mHosDocData.getHospital_name();

        Log.e(TAG, "hospitalId=== " + hospitalId);
        Log.e(TAG, "hospitalName=== " + hospitalName);
        if (!TextUtils.isEmpty(hospitalId)) {
            mPostHosContainer.setVisibility(View.VISIBLE);
            mFunctionManager.setCircleImageSrc(mPostHosImg, mHosDocData.getHosimg());
            mPostHosName.setText(hospitalName);
            mPostHosCase.setText(mHosDocData.getPeople() + "案例");
            mPostHosAddress.setText(mHosDocData.getAddress());

            //评分
            mPostHosBar.setMax(100);
            mPostHosBar.setProgress(Integer.parseInt(mHosDocData.getComment_bili()));    //评分

            //医院点击
            mPostHosContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(mContext, HosDetailActivity.class);
                    it.putExtra("hosid", hospitalId);
                    startActivity(it);
                }
            });
        } else {
            mPostHosContainer.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //是可见，是视频fragment,播放器部位空
        if (mPicView.getYueMeiVideoView() != null) {
            if (currentPosition != 0) {
                mPicView.getYueMeiVideoView().videoRestart(currentPosition);
                currentPosition = 0;
            } else {
                mPicView.getYueMeiVideoView().videoRestart();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mPicView.getYueMeiVideoView() != null) {
            mPicView.getYueMeiVideoView().videoSuspend();
        }
    }

    @Override
    public void onDestroyView() {
        if (mPicView.getYueMeiVideoView() != null) {
            mPicView.getYueMeiVideoView().videoStopPlayback();
        }
        super.onDestroyView();
        unbinder.unbind();
    }

    public PostHeadFragment getPostHeadFragment() {
        return postHeadFragment;
    }

    public CommentsAndRecommendFragment getCommentsAndRecommendFragment() {
        return commentsAndRecommendFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    public interface OnEventClickListener {
        void onScrollRefresh();           //刷新接口回调

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调

        void onCommentsClick();                                 //评论发布后接口回调  true：主贴评论，false：回复楼中楼

        void onScrollChanged(int t, int height);                //滚动回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
