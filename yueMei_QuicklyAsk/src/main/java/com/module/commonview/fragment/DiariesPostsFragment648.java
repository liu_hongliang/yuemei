package com.module.commonview.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.api.PostListApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.PostListData;
import com.module.event.PostMsgEvent;
import com.quicklyask.activity.R;
import com.scwang.smartrefresh.layout.api.RefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * 原生化的帖子模块
 * Created by 裴成浩 on 2018/8/16.
 */
public class DiariesPostsFragment648 extends YMBaseFragment {

    private PostListData mData;
    private String mDiaryId;
    private String mIsNew = "0";
    private PostListApi mPostListApi;

    @BindView(R.id.post_data_diary_content)
    public FrameLayout diaryContent;

    private String TAG = "DiariesPostsFragment648";
    private PostContentFragment postContentFragment;
    private PostBottomFragment postBottomFragment;
    private PostHtmlFragment postHtmlFragment;
    private int deleteCount = 0;

    public static DiariesPostsFragment648 newInstance(String diaryId, String isNew) {
        DiariesPostsFragment648 fragment = new DiariesPostsFragment648();
        Bundle bundle = new Bundle();
        bundle.putString("diaryId", diaryId);
        bundle.putString("isNew", isNew);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(PostMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 0:
//                deleteCount = (int) msgEvent.getData();
                loadPostData();
                break;
            case 1:
//                deleteCount = (int) msgEvent.getData();
                loadPostData();
                break;
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mDiaryId = getArguments().getString("diaryId");
        mIsNew = getArguments().getString("isNew");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_posts648;
    }

    @Override
    protected void initView(View view) {

    }

    @Override
    protected void initData(View view) {
        mPostListApi = new PostListApi();
        loadPostData();
    }

    /**
     * 获取帖子列表数据
     */
    private void loadPostData() {
        mDialog.startLoading();
        mPostListApi.addData("id", mDiaryId);
        mPostListApi.addData("is_new", mIsNew);
        mPostListApi.getCallBack(mContext, mPostListApi.getPostListHashMap(), new BaseCallBackListener<PostListData>() {
            @Override
            public void onSuccess(PostListData data) {
                mDialog.stopLoading();
                //刷新隐藏
                if (postContentFragment != null) {
                    if (postContentFragment.mScrollRefresh != null) {
                        postContentFragment.mScrollRefresh.finishRefresh();
                    }
                }

                mData = data;

                setPostData();

                if (onEventClickListener != null) {
                    onEventClickListener.onLoadedClick(mData);
                }

            }
        });

        //帖子不存在回调
        mPostListApi.setOnEvent404Listener(new PostListApi.OnEvent404Listener() {
            @Override
            public void onEvent404Click(String message) {
                mDialog.stopLoading();
                if (onEventClickListener != null) {
                    onEventClickListener.onEvent404Click(message);
                }
            }
        });
    }

    /**
     * 设置数据
     */
    private void setPostData() {
        String loadHtmlUrl = mData.getLoadHtmlUrl();

        //当前要加载的项
        if (TextUtils.isEmpty(loadHtmlUrl)) {
            if (postContentFragment != null) {
                postContentFragment.mScrollRefresh.setEnableLoadMore(true);
            }

            postContentFragment = PostContentFragment.newInstance(mData, mDiaryId);
            setActivityFragment(R.id.post_data_diary_content, postContentFragment);
            postContentFragment.setOnEventClickListener(new PostContentFragment.OnEventClickListener() {
                @Override
                public void onScrollRefresh() {
                    loadPostData();
                }

                @Override
                public void onLikeClick(DiariesReportLikeData data) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onLikeClick(data);
                    }
                }

                @Override
                public void onItemDeleteClick(DiariesDeleteData deleteData) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemDeleteClick(deleteData);
                    }
                    if (postBottomFragment.mAnswerNum - deleteCount < 0) {
                        postBottomFragment.mAnswerNum = 0;
                    } else {
                        postBottomFragment.mAnswerNum = postBottomFragment.mAnswerNum - deleteCount;
                    }
                    postBottomFragment.setComments();
                }

                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemReportClick(data);
                    }
                    postBottomFragment.mAnswerNum++;
                    postBottomFragment.setComments();
                }

                @Override
                public void onCommentsClick() {
                    postBottomFragment.mAnswerNum++;
                    postBottomFragment.setComments();
                }

                @Override
                public void onScrollChanged(int t, int height) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onScrollChanged(t, height, mData);
                    }
                }
            });
        } else {
            if (postContentFragment != null) {
                postContentFragment.mScrollRefresh.setEnableLoadMore(false);
            }
            postHtmlFragment = PostHtmlFragment.newInstance(mData, mDiaryId);
            setActivityFragment(R.id.post_data_diary_content, postHtmlFragment);
            postHtmlFragment.setOnEventClickListener(new PostHtmlFragment.OnEventClickListener() {
                @Override
                public void onCommentsClick() {
                    postBottomFragment.mAnswerNum++;
                    postBottomFragment.setComments();
                }

                @Override
                public void onScrollChanged(int t) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onScrollChanged(t, 0, mData);
                    }
                }
            });
        }

        //吸底模块
        postBottomFragment = PostBottomFragment.newInstance(mData);
        setActivityFragment(R.id.post_data_list_bottom, postBottomFragment);

        //吸底回调
        postBottomFragment.setOnEventClickListener(new PostBottomFragment.OnEventClickListener() {
            @Override
            public void onCollectionClick(boolean b) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCollectionClick(b);
                }
            }

            @Override
            public void onLikeClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onLikeClick(data);
                }
            }

            @Override
            public void onMessageClick() {
                if (postContentFragment != null) {
                    CommentsAndRecommendFragment commentsAndRecommendFragment = postContentFragment.getCommentsAndRecommendFragment();
                    if (commentsAndRecommendFragment != null) {
                        commentsAndRecommendFragment.setCommentsCallback();
                    }
                } else if (postHtmlFragment != null) {
                    postHtmlFragment.startComments();
                }
            }

            @Override
            public void onWritePageClick(View v, String id) {

            }
        });
    }

    /**
     * 获取帖子数据
     *
     * @return
     */
    public PostListData getmData() {
        return mData;
    }

    public PostContentFragment getPostContentFragment() {
        return postContentFragment;
    }

    public PostHtmlFragment getPostHtmlFragment() {
        return postHtmlFragment;
    }

    public PostBottomFragment getPostBottomFragment() {
        return postBottomFragment;
    }

    public interface OnEventClickListener {

        void onLoadedClick(PostListData data);                 //加载数据完成后接口回调

        void onCollectionClick(boolean b);                      //收藏接口回调

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调

        void onEvent404Click(String message);                   //日记或者日记本不存在

        void onScrollChanged(int t, int height, PostListData data);                //滚动回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
