package com.module.commonview.fragment;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.Time;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.MapHospitalWebActivity;
import com.module.commonview.activity.SpeltActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.SellOutAdapter;
import com.module.commonview.adapter.SkuCommentListAdapter;
import com.module.commonview.adapter.SkuDiaryListAdapter;
import com.module.commonview.module.api.AutoSendApi;
import com.module.commonview.module.api.AutoSendApi2;
import com.module.commonview.module.api.TaoDataApi;
import com.module.commonview.module.api.TongjiClickApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.SerMap;
import com.module.commonview.module.other.TaoDetailWebViewClient;
import com.module.commonview.view.AskallItemView;
import com.module.commonview.view.CouponsPriceAfterPop;
import com.module.commonview.view.DieLinearLayout;
import com.module.commonview.view.DropDownMultiPagerView;
import com.module.commonview.view.IdeaScrollView;
import com.module.commonview.view.LGallery;
import com.module.commonview.view.RedPacketView;
import com.module.commonview.view.RepaymentTextView;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.SkuProjectDetailPop;
import com.module.commonview.view.SkuSpecPopwindow;
import com.module.commonview.view.SkuTagLayout;
import com.module.commonview.view.TagView;
import com.module.commonview.view.YanzhibiPopwindow;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.model.bean.BBsListData550;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.view.StaScoreBar;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.activity.OpeningMemberActivity;
import com.module.my.controller.activity.PlusVipActivity;
import com.module.my.controller.activity.VideoPlayerActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.AppRaisalBean;
import com.quicklyask.entity.AppraisalListBean;
import com.quicklyask.entity.ChatClickData;
import com.quicklyask.entity.ClickData;
import com.quicklyask.entity.CouponPriceInfo;
import com.quicklyask.entity.Coupons;
import com.quicklyask.entity.CouponsData;
import com.quicklyask.entity.DiaryListBean;
import com.quicklyask.entity.GroupBean;
import com.quicklyask.entity.HospitalTop;
import com.quicklyask.entity.RepaymentBean;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.entity.TaoPsoter;
import com.quicklyask.entity.TaoUserCoupons;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CountDownView;
import com.quicklyask.view.DacuCountDownView;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.TimerTextView;
import com.quicklyask.view.YueMeiVideoView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static com.module.commonview.view.IdeaScrollView.OnScrollListener.SCROLL_STATE_IDLE;

public class TaoDeatailFragment extends YMBaseFragment {
    public static final String TAG = "TaoDeatailFragment";
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.tao_detail_refresh)
    SmartRefreshLayout ptrlayout;
    @BindView(R.id.lgallery)
    LGallery mLgallery;
    @BindView(R.id.sku_dacu_price)
    TextView mSkuDacuPrice;
    @BindView(R.id.sku_dacu_price_desc)
    TextView mSkuDacuPriceDesc;
    @BindView(R.id.coupons_price_after_desc)
    TextView mCouponsPriceAfterDesc;
    @BindView(R.id.dacu_plus_visorgone)
    LinearLayout mSkuDacuPlusVisorgone;
    @BindView(R.id.dacu_plus_price)
    TextView mSkuDacuPlusPrice;
    @BindView(R.id.sku_dachu_hosprice_visorgoen)
    LinearLayout mSkuDacuHospriceVisorgone;
    @BindView(R.id.sku_dacu_spec)
    TextView mSkuDacuSpec;
    @BindView(R.id.sku_dacu_hosprice)
    TextView mSkuDacuHosprice;
    @BindView(R.id.sku_dacu_typeprice)
    TextView mSkuDacuTypePrice;
    @BindView(R.id.sku_dacu_background)
    ImageView mSkuDacuBackground;
    @BindView(R.id.sku_price_visorgone)
    LinearLayout mSkuPriceVisorgone;
    @BindView(R.id.sku_conpos_visorgone)
    LinearLayout mSkuConposVisorgone;
    @BindView(R.id.sku_conpns_price)
    TextView mSkuConposPrice;
    @BindView(R.id.sku_time_container)
    LinearLayout mSkuTimeContainer;
    @BindView(R.id.sku_low_price_visorgone)
    FrameLayout mSkuLowPriceVisorgone;
    @BindView(R.id.sku_low_price_txt)
    TextView mSkuLowPriceTxt;
    @BindView(R.id.sku_ask_click)
    FrameLayout mSkuLowAskClick;
    @BindView(R.id.coupons_price_after_click)
    RelativeLayout mCouponsPriceAfterClick;
    @BindView(R.id.coupons_price_after_container)
    LinearLayout mCouponsPriceAfterContainer;
    @BindView(R.id.sku_dacu_visorgone)
    RelativeLayout mSkuDacuVisorgone;
    @BindView(R.id.sku_dacu_shenyu)
    TextView mSkuDacuShenyu;
    @BindView(R.id.coupons_price_after_tip)
    ImageView mCouponsPriceAfterTip;
    @BindView(R.id.sku_title)
    TextView mSkuTitle;
    @BindView(R.id.sku_desc)
    TextView mSkuDesc;
    @BindView(R.id.sku_common_price)
    TextView mSkuCommonPrice;
    @BindView(R.id.sku_common_spec)
    TextView mSkuCommonSpec;
    @BindView(R.id.sku_hosprice)
    TextView mSkuHosprice;
    @BindView(R.id.sku_disscount)
    TextView mSkuDisscount;
    @BindView(R.id.sku_plus_visorgone)
    LinearLayout mSkuCommonPlusVisorgone;
    @BindView(R.id.sku_comprice_visorgone)
    RelativeLayout mSkuCompriceVisorgone;
    @BindView(R.id.sku_hosprice_voiorgone)
    LinearLayout mSkuHsopriceVisorgone;
    @BindView(R.id.sku_plus_price)
    TextView mSkuPlusPrice;
    @BindView(R.id.sku_city)
    TextView mSkuCity;
    @BindView(R.id.sku_click_plus_kaitong)
    LinearLayout mSkuClickPlusKaitong;
    @BindView(R.id.sku_plus_desc)
    TextView mSkuPlusDesc;
    @BindView(R.id.sku_plus_vibility)
    RelativeLayout mSkuPlusVibility;
    @BindView(R.id.sku_bargain_visorgone)
    RelativeLayout mSkuBargainVibility;
    @BindView(R.id.sku_top_vibility)
    RelativeLayout mSkuTopVibility;
    @BindView(R.id.sku_top_title)
    TextView mSkuTopTitle;
    @BindView(R.id.sku_tag_llt)
    SkuTagLayout mSkuTagLlt;
    @BindView(R.id.sku_dacuceo_click)
    RelativeLayout mSkuDacuceoVisorgone;
    @BindView(R.id.sku_ceo_content)
    TextView mSkuDacuceoContent;
    @BindView(R.id.sku_metro_click)
    RelativeLayout mSkuMetroVisorgone;
    @BindView(R.id.sku_metro_content)
    TextView mSkuMetroContent;
    @BindView(R.id.sku_metro_img)
    ImageView mSkuMetroImg;
    @BindView(R.id.group_user_img1)
    ImageView mGroupUserImg1;
    @BindView(R.id.group_user_name1)
    TextView mGroupUserName1;
    @BindView(R.id.group_complete_people1)
    TextView mGroupCompletePeople1;
    @BindView(R.id.group_time1)
    TimerTextView mGroupTime1;
    @BindView(R.id.btn_tuxedo1)
    Button mBtnTuxedo1;
    @BindView(R.id.sku_group1)
    LinearLayout mSkuGroup1;
    @BindView(R.id.group_user_img2)
    ImageView mGroupUserImg2;
    @BindView(R.id.group_user_name2)
    TextView mGroupUserName2;
    @BindView(R.id.group_complete_people2)
    TextView mGroupCompletePeople2;
    @BindView(R.id.group_time2)
    TimerTextView mGroupTime2;
    @BindView(R.id.btn_tuxedo2)
    Button mBtnTuxedo2;
    @BindView(R.id.sku_group2)
    LinearLayout mSkuGroup2;
    @BindView(R.id.is_sku_group)
    LinearLayout mIsSkuGroup;
    @BindView(R.id.sku_sell_out)
    LinearLayout mSkuSellOutVisorgone;
    @BindView(R.id.sku_sellout_list)
    RecyclerView mSkuSelloutList;
    @BindView(R.id.sku_repay)
    TextView mSkuRepay;
    @BindView(R.id.sku_repay_click)
    LinearLayout mSkuRepayClick;
    @BindView(R.id.sku_cuxiao_container)
    LinearLayout mSkuCuxiaoContainer;
    @BindView(R.id.sku_repay_right)
    ImageView mSkuRepayRight;
    @BindView(R.id.sku_repay_right2)
    ImageView mSkuRepayRight2;
    @BindView(R.id.tao_sku_ly_content)
    LinearLayout mTaoSkuLyContent;
    @BindView(R.id.sku_comment_container)
    LinearLayout mSkuCommentContainer;
    @BindView(R.id.sku_comment_koubei_title)
    TextView mSkuCommentKoubeiTitle;
    @BindView(R.id.sku_comment_koubei_check)
    TextView mSkuCommentKoubeiCheck;
    @BindView(R.id.sku_comment_koubei_click)
    LinearLayout mSkuCommentKoubeiClick;
    @BindView(R.id.sku_comment_ask_title)
    TextView mSkuCommentAskTitle;
    @BindView(R.id.sku_comment_ask_check)
    TextView mSkuCommentAskCheck;
    @BindView(R.id.sku_hos_img)
    ImageView mSkuHosImg;
    @BindView(R.id.sku_hos_img_renzheng)
    ImageView mSkuHosImgRenzheng;
    @BindView(R.id.sku_hos_name)
    TextView mSkuHosName;
    @BindView(R.id.sku_hos_phone)
    ImageView mSkuHosPhone;
    @BindView(R.id.sku_hos_chat)
    ImageView mSkuHosChat;
    @BindView(R.id.sku_hos_ratingbar)
    StaScoreBar mHosRatingBar;
    @BindView(R.id.hos_property)
    TextView mHosProperty;
    @BindView(R.id.hos_area)
    TextView mHosArea;
    @BindView(R.id.hos_rank)
    TextView mHosRank;
    @BindView(R.id.hos_bednum)
    TextView mHosBednum;
    @BindView(R.id.hos_address_tv)
    TextView mHosAddressTv;
    @BindView(R.id.hos_distance)
    TextView mHosDistanceTv;
    @BindView(R.id.hos_detaile_togao_ly)
    LinearLayout mHosDetaileTogaoLy;
    @BindView(R.id.taotao_doc_name_tv)
    TextView mTaotaoDocNameTv;
    @BindView(R.id.doc_img)
    ImageView mTaotaoDocImg;
    @BindView(R.id.sku_doc_img_renzheng)
    ImageView mSkuDocImgRenzheng;
    @BindView(R.id.taotao_doc_zhiwei_tv)
    TextView mTaotaoDocZhiweiTv;
    @BindView(R.id.sku_doc_chat)
    ImageView mTaoDocChat;
    @BindView(R.id.sku_doc_ratingbar)
    StaScoreBar mTaoDocRatingbar;
    @BindView(R.id.sku_doc_book)
    TextView mTaoDocBook;
    @BindView(R.id.sku_doc_skilled_visorgone)
    LinearLayout mTaoDocSkilledVisorgone;
    @BindView(R.id.sku_doc_skilled_container)
    LinearLayout mTaoDocSkilledContainer;
    @BindView(R.id.doc_zhuye_rly)
    LinearLayout mDocZhuyeRly;
    @BindView(R.id.sku_repay_container)
    LinearLayout mSkuRePayContainer;
    @BindView(R.id.tao_detail_container)
    IdeaScrollView mTaoDetailContainer;
    @BindView(R.id.sku_coupons_title)
    TextView mSkuCouponsTitle;
    @BindView(R.id.sku_coupons_container)
    DieLinearLayout mSkuCouponsContainer;
    @BindView(R.id.sku_coupon_shengyu)
    TextView mSkuCouponShengyu;
    @BindView(R.id.sku_click_coupons)
    RelativeLayout mSkuClickCoupons;
    @BindView(R.id.sku_hoscoupon_title)
    TextView mSkuHoscouponTitle;
    @BindView(R.id.sku_hoscoupon_container)
    DieLinearLayout mSkuHoscouponContainer;
    @BindView(R.id.sku_hoscoupon_shengyu)
    TextView mSkuHoscouponShengyu;
    @BindView(R.id.sku_spec_content)
    TextView mSkuSelectProjectTxt;
    @BindView(R.id.sku_spec_container)
    FlowLayout mSkuSpecContainer;
    @BindView(R.id.sku_select_project_click)
    RelativeLayout mSkuSelectProjectClick;
    @BindView(R.id.sku_explain)
    TextView mSkuExplainTxt;
    @BindView(R.id.sku_project_explain_click)
    RelativeLayout mSkuProcjetExplainClick;
    @BindView(R.id.sku_baike_click)
    RelativeLayout mSkuProcjetBaikeClick;
    @BindView(R.id.sku_project_baike_text)
    TextView mSkuProcjetBaikeText;
    @BindView(R.id.sku_click_hoscoupons)
    RelativeLayout mSkuClickHoscoupons;
    @BindView(R.id.sku_comment_visorgone)
    LinearLayout mSkuCommentVisorgone;

    @BindView(R.id.sku_comment_list)
    RecyclerView mSkuCommentRecycler;
    @BindView(R.id.sku_diary_visorgone)
    LinearLayout mSkuDiaryVisorgone;
    @BindView(R.id.sku_diary_click)
    RelativeLayout mSkuDiaryClick;
    @BindView(R.id.sku_diary_title)
    TextView mSkuDiaryTitle;
    @BindView(R.id.sku_diary_list)
    RecyclerView mSkuDiaryList;

    @BindView(R.id.sku_comment_askall_visorgone)
    LinearLayout mSkuCommentAskallVisorgone;
    @BindView(R.id.sku_commetn_goask)
    Button mSkuCommetnGoask;
    @BindView(R.id.sku_goask_visorgone)
    RelativeLayout mSkuGoaskVisorgone;
    @BindView(R.id.sku_askall_container)
    LinearLayout mSkuAskallContainer;
    @BindView(R.id.sku_detail_container)
    LinearLayout mSkuDetailCotainer;
    @BindView(R.id.sku_environment_container)
    LinearLayout mSkuEnvironmentCotainer;
    @BindView(R.id.sku_recommend_container)
    LinearLayout mSkuRecommendCotainer;
    @BindView(R.id.sku_coupons_lingquan_container)
    LinearLayout mSkuCouponsLingquanCotainer;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.sku_detail_webview)
    WebView DetailWebview;
    @BindView(R.id.sku_environment_webview)
    WebView mEnvironmentWebwiew;
    @BindView(R.id.sku_recommend_webview)
    WebView LiuchengWebview;
    @BindView(R.id.sku_repay_visorgone)
    RelativeLayout mSkuRepayVisorgone;
    @BindView(R.id.sku_kepu_container)
    LinearLayout mSkuKepuContainer;
    @BindView(R.id.sku_kepu_webview)
    WebView kePuWebview;
    @BindView(R.id.sku_cuxiao_visorgone)
    RelativeLayout mSkuCuxiaoVisorgone;
    @BindView(R.id.hos_address_content)
    ImageButton hosAdressContent;
    @BindView(R.id.sku_plus_line)
    View skuPlusLine;


    private RelativeLayout skuContainer;
    private String mStatus;
    private TaoDetailBean.HosDocBean mHos_doc;
    private String phone400;// 400电话
    private String mPhoneFen;

    //当前正在播放的视频
    private YueMeiVideoView mVideoView;
    private int currentPosition;
    public static final int VIDEO_CONTROL = 111;
    private ArrayList<String> tabTxt = new ArrayList<>(Arrays.asList("商品", "评价", "详情", "环境", "流程"));
    private TaoDetailBean.ProInfoBean proInfo;


    public void setTabLayout(TabLayout tabLayout) {
        mTabLayout = tabLayout;
    }

    public void setSkuContainer(RelativeLayout skuContainer) {
        this.skuContainer = skuContainer;
    }

    TabLayout mTabLayout;


    private TaoDetailBean mTaoDetailBean;
    private String mSkuId;
    public String source = "0";
    public String objid = "0";
    private String kefu_nickName = "";
    //判读是否是scrollview主动引起的滑动，true-是，false-否，由tablayout引起的
    private boolean isScroll;
    private int lastPos = 0;
    private List<View> anchorList = new ArrayList<>();
    private PageJumpManager pageJumpManager;
    HashMap<String, String> showMap = new HashMap<>();

    private BaseWebViewClientMessage baseWebViewClientMessage;
    private TaoDetailWebViewClient taoDetailWebViewClient;
    boolean flag = true;//发送通知的变量 true发送 flase 不发
    private DropDownMultiPagerView dropDownMultiPagerView;
    LinearLayout mBottomExpandView;
    TextView mBottomExpandTxt;


    public TaoDeatailFragment() {

    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public void setBottomExpandView(LinearLayout bottomExpandView) {
        mBottomExpandView = bottomExpandView;
    }

    public void setBottomExpandTxt(TextView bottomExpandTxt) {
        mBottomExpandTxt = bottomExpandTxt;
    }

    public static TaoDeatailFragment newInstance(String skuId, String source, String objid) {
        TaoDeatailFragment taoDeatailFragment = new TaoDeatailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("skuId", skuId);
        bundle.putString("source", source);
        bundle.putString("objid", objid);
        taoDeatailFragment.setArguments(bundle);
        return taoDeatailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSkuId = getArguments().getString("skuId");
        source = getArguments().getString("source");
        objid = getArguments().getString("objid");

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tao_detail;
    }

    private HashMap<String, Object> urlTaoMap = new HashMap<>();
    private HashMap<String, Object> kepuMap = new HashMap<>();


    @Override
    protected void initView(View view) {

        anchorList.add(mLgallery);
        anchorList.add(mSkuCommentContainer);
        anchorList.add(mSkuDetailCotainer);
        anchorList.add(mSkuEnvironmentCotainer);
        anchorList.add(mSkuRecommendCotainer);
        pageJumpManager = new PageJumpManager(mContext);
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        taoDetailWebViewClient = new TaoDetailWebViewClient((TaoDetailActivity) getActivity());
        baseWebViewClientMessage.setBaseWebViewClientCallback(taoDetailWebViewClient);
        initDetailWebview();


    }


    Map<String, Object> maps = new HashMap<>();
    private boolean isDacuBackground = false;

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void initData(View view) {
        initSkuData(mSkuId, true);
        if (null != mTabLayout) {
            AdertAdv.OtherBackgroundImgBean data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);
            if (data != null && data.getImg() != null) {
                isDacuBackground = true;
            }
            if (isDacuBackground) {
                mTabLayout.setTabTextColors(ContextCompat.getColor(mContext, R.color.white), ContextCompat.getColor(mContext, R.color.white));
                mTabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(mContext, R.color.white));
            }
            mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    //点击标签，使scrollview滑动，isScroll置false
                    if (isDacuBackground) {
                        View view = tab.getCustomView();
                        if (null == view) {
                            tab.setCustomView(R.layout.tab_layout_text);
                        }
                        TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                        textView.setTextColor(mTabLayout.getTabTextColors());
                        textView.setTypeface(Typeface.DEFAULT);
                        textView.setTextSize(16);
                    }
                    isScroll = false;
                    int pos = tab.getPosition();
                    int top = anchorList.get(pos).getTop();
                    clickNivagation(pos);
                    Log.e(TAG, "top == " + top);
                    mTaoDetailContainer.smoothScrollTo(0, (top - 300));
                    if (mOnEventCallBack != null) {
                        mOnEventCallBack.onTabClick(top);
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    if (isDacuBackground) {
                        View view = tab.getCustomView();
                        if (null == view) {
                            tab.setCustomView(R.layout.tab_layout_text);
                        }
                        TextView textView = tab.getCustomView().findViewById(android.R.id.text1);
                        textView.setTypeface(Typeface.DEFAULT);
                        textView.setTextSize(14);
                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

        mTaoDetailContainer.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //当滑动由scrollview触发时，isScroll 置true
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    isScroll = true;
                }
                return false;
            }
        });

        mTaoDetailContainer.setOnScrollListener(new IdeaScrollView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(IdeaScrollView view, int scrollState) {
                switch (scrollState) {
                    case SCROLL_STATE_IDLE:
                        changeViewWidthAnimatorStart(mBottomExpandView, 56, Utils.dip2px(63), scrollState);
                        break;
                    case SCROLL_STATE_TOUCH_SCROLL:
                        changeViewWidthAnimatorStart(mBottomExpandView, Utils.dip2px(63), 56, scrollState);
//                    case SCROLL_STATE_FLING:
                        break;
                }
            }

            @Override
            public void onScroll(IdeaScrollView view, boolean isTouchScroll, int x, int y, int oldx, int oldy) {
                if (isScroll) {
                    for (int i = tabTxt.size() - 1; i >= 0; i--) {
                        //根据滑动距离，对比各模块距离父布局顶部的高度判断
                        if (y > anchorList.get(i).getTop()) {
                            setScrollPos(i);
                            if (y > 4300) {
                                Log.e(TAG, "flag====>" + flag);
                                if (flag) {
                                    if (Utils.isLogin()) {
                                        flag = false;
                                        if (mTaoDetailBean != null) {
                                            TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
                                            HashMap<String, Object> hashMap = new HashMap<>();
                                            hashMap.put("hos_id", hos_doc.getHospital_id());
                                            hashMap.put("pos", "6");
                                            new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, s.message);
                                                    }
                                                }
                                            });
                                            HashMap<String, Object> hashMap2 = new HashMap<>();
                                            hashMap2.put("obj_type", "6");
                                            hashMap2.put("obj_id", mSkuId);
                                            hashMap2.put("hos_id", hos_doc.getHospital_id());
                                            new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                                @Override
                                                public void onSuccess(ServerData s) {
                                                    if ("1".equals(s.code)) {
                                                        Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                                    }
                                                }
                                            });

                                        }
                                    }
                                }

                            }
                            break;
                        }
                        if (mOnEventCallBack != null) {
                            mOnEventCallBack.onScrollChanged(y, mLgallery.getHeight());
                        }
                    }
                }
            }
        });


        /**
         * 视频放大
         */
        mLgallery.setOnAdapterClickListener(new LGallery.OnAdapterClickListener() {

            @Override
            public void onMaxVideoClick(YueMeiVideoView videoView, String videoUrl, int currentPosition) {
                mVideoView = videoView;
                Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                intent.putExtra("is_network_video", true);
                intent.putExtra("selectNum", videoUrl);
                intent.putExtra("progress", videoView.getCurrentPosition());
                startActivityForResult(intent, VIDEO_CONTROL);
            }
        });
        hosAdressContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ClickData addressClickData = mHos_doc.getAddressClickData();
                HashMap<String, String> event_params = addressClickData.getEvent_params();
                YmStatistics.getInstance().tongjiApp(event_params);
                Intent intent = new Intent(mContext, MapHospitalWebActivity.class);
                intent.putExtra("hosid", mHos_doc.getHospital_id());
                startActivity(intent);
            }
        });
        mHosAddressTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickData addressClickData = mHos_doc.getAddressClickData();
                HashMap<String, String> event_params = addressClickData.getEvent_params();
                YmStatistics.getInstance().tongjiApp(event_params);
                Intent intent = new Intent(mContext, MapHospitalWebActivity.class);
                intent.putExtra("hosid", mHos_doc.getHospital_id());
                startActivity(intent);
            }
        });

        if (ptrlayout != null) {
            ptrlayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                    ptrlayout.finishRefresh();
                    if (Utils.isLogin()) {
                        dropDownMultiPagerView = new DropDownMultiPagerView(mContext, mSkuId);
                        dropDownMultiPagerView.show();
                        dropDownMultiPagerView.setOnDropDownMultiPagerViewItemClick(new DropDownMultiPagerView.OnDropDownMultiPagerViewItemClick() {
                            @Override
                            public void onItemClick(int position, String skuId, HashMap<String, String> eventParams) {
                                dropDownMultiPagerView.dismiss();
                                eventParams.put("id", mSkuId);
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_BROWSE_TAO_CLICK), eventParams, new ActivityTypeData("2"));
                                Intent it1 = new Intent();
                                it1.putExtra("id", skuId);
                                it1.putExtra("source", "0");
                                it1.putExtra("objid", "0");
                                it1.setClass(mContext, TaoDetailActivity.class);
                                startActivity(it1);
                            }
                        });
                    } else {
                        refreshData();
                    }

                }
            });
        }
    }


    /**
     * 点击导航统计事件
     *
     * @param pos
     */
    private void clickNivagation(int pos) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", mSkuId);
        switch (pos) {
            case 0:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "tao"), hashMap, new ActivityTypeData("2"));
                break;
            case 1:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "post"), hashMap, new ActivityTypeData("2"));
                break;
            case 2:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "taoinfo"), hashMap, new ActivityTypeData("2"));
                break;
            case 3:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "environment"), hashMap, new ActivityTypeData("2"));
                break;
        }
        if (mTaoDetailBean.getBaike_id() > 0) {
            if (pos == 4) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "baike"), hashMap, new ActivityTypeData("2"));
            }
            if (pos == 5) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "appointment"), hashMap, new ActivityTypeData("2"));
            }
        } else {
            if (pos == 4) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAONAV_CLICK, "appointment"), hashMap, new ActivityTypeData("2"));
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        //是可见，是视频fragment,播放器部位空
        if (mVideoView != null) {
            if (currentPosition != 0) {
                mVideoView.videoRestart(currentPosition);
                currentPosition = 0;
            } else {
                mVideoView.videoRestart();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mVideoView != null) {
            mVideoView.videoSuspend();
        }
    }

    @Override
    public void onDestroyView() {
        if (mVideoView != null) {
            mVideoView.videoStopPlayback();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case VIDEO_CONTROL:    //视频请求回调
                if (resultCode == VideoPlayerActivity.RETURN_VIDEO_PROGRESS) {
                    currentPosition = data.getIntExtra("current_position", 0);
                }
                break;
        }
    }

    private boolean firstLoad = true;

    void initSkuData(final String skuid, final boolean isFirst) {
        maps.put("id", skuid);//
//        maps.put("id", "168814");//168814 正常    23417已售光
//        maps.put("lon", "116.495784");
//        maps.put("lat", "40.009156");
        maps.put("source", source);
        maps.put("objid", objid);
        mDialog.startLoading();
        new TaoDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

            private List<TaoDetailBean.VideoBean> videoBeanList = new ArrayList<>();
            private List<TaoDetailBean.PicBean> picBeanList = new ArrayList<>();
            private List<TaoDetailBean.RelTaoBeanX.RelTaoBean> mRel_tao1 = new ArrayList<>();
            private List<RepaymentBean> mRepayment = new ArrayList<>();
            private List<TaoDetailBean.ServiceBean> mService = new ArrayList<>();
            private List<String> mPromotion = new ArrayList<>();

            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    try {

                        mTaoDetailBean = JSONUtil.TransformSingleBean(serverData.data, TaoDetailBean.class);

                        if (isFirst) {
                            if (mTaoDetailBean != null) {
                                mOnEventCallBack.onLoadDataCallBck(mTaoDetailBean);
                                if (firstLoad) {
                                    int baike_id = mTaoDetailBean.getBaike_id();
                                    if (baike_id > 0) {
                                        tabTxt.add(4, "科普");
                                        anchorList.add(4, mSkuKepuContainer);
                                        kepuMap.put("baike_id", baike_id + "");
                                        kepuMap.put("id", mSkuId);
                                        LodUrl(kePuWebview, FinalConstant.YUYUE_KEPU, kepuMap);
                                    }

                                    AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();
                                    if (null != appraisal) {
                                        List<AppraisalListBean> appraisalList = appraisal.getAppraisalList();
                                        if (null != appraisalList && appraisalList.size() > 0) {
                                            tabTxt.set(1, "评价");
                                        } else {
                                            tabTxt.set(1, "日记");
                                        }
                                    } else {
                                        tabTxt.remove(1);
                                    }

                                    //初始化标题
                                    for (int i = 0; i < tabTxt.size(); i++) {
                                        mTabLayout.addTab(mTabLayout.newTab().setText(tabTxt.get(i)));
                                    }
                                    firstLoad = false;


                                }
                            }
                        } else {
                            if (mTaoDetailBean != null) {
//                                String is_group = mTaoDetailBean.getGroupInfo().getIs_group();
//                                if ("1".equals(is_group)) {
                                mOnEventCallBack.onLoadDataCallBck(mTaoDetailBean);
//                                }
                            }
                        }


                        picBeanList.clear();
                        videoBeanList.clear();
                        picBeanList = mTaoDetailBean.getPic();
                        videoBeanList = mTaoDetailBean.getVideo();
                        TaoDetailBean.BasedataBean basedata = mTaoDetailBean.getBasedata();
                        if (mService != null && mService.size() > 0) {
                            mService.clear();
                        }
                        mService = mTaoDetailBean.getService();
                        mLgallery.setData(picBeanList, videoBeanList, skuid); //图片视频

                        mSkuTitle.setText(basedata.getTitle());         //标题
                        String subtitle = basedata.getSubtitle();   //副标题
                        mSkuId = basedata.getId();
                        //sku状态1正常 0非正常
                        mStatus = basedata.getStatus();
                        mLgallery.setSellOut(mStatus);
                        if (!TextUtils.isEmpty(subtitle)) {
                            mSkuDesc.setVisibility(View.VISIBLE);
                            mSkuDesc.setText(subtitle);
                        } else {
                            mSkuDesc.setVisibility(View.GONE);
                        }

                        String content = "";
                        String tao_city_name = basedata.getTao_city_name();
                        String order_num = basedata.getOrder_num();
                        if (!TextUtils.isEmpty(tao_city_name) && !TextUtils.isEmpty(order_num)) {
                            mSkuCity.setVisibility(View.VISIBLE);
                            content = tao_city_name + " " + order_num;
                        } else {
                            mSkuCity.setVisibility(View.VISIBLE);
                            if (!TextUtils.isEmpty(tao_city_name)) {
                                content = tao_city_name;
                            } else {
                                if (!TextUtils.isEmpty(order_num)) {
                                    content = order_num;
                                } else {
                                    mSkuCity.setVisibility(View.GONE);
                                }
                            }

                        }

                        mSkuCity.setText(content); //sku城市和已预约人数

                        //大促、拼团初始化
                        setPintuanStyle();
                        if (!"1".equals(mStatus)) {
                            mSkuSellOutVisorgone.setVisibility(View.VISIBLE);
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
                            SellOutAdapter sellOutAdapter = new SellOutAdapter(mContext, R.layout.sku_sell_out_item, mTaoDetailBean.getSale_rec_tao());
                            mSkuSelloutList.setHasFixedSize(true);
                            mSkuSelloutList.setNestedScrollingEnabled(false);
                            mSkuSelloutList.setFocusableInTouchMode(false);
                            mSkuSelloutList.setLayoutManager(gridLayoutManager);
                            mSkuSelloutList.setAdapter(sellOutAdapter);
                            sellOutAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                    String id = mTaoDetailBean.getSale_rec_tao().get(position).getId();
                                    Intent it1 = new Intent();
                                    it1.putExtra("id", id);
                                    it1.putExtra("source", "0");
                                    it1.putExtra("objid", "0");
                                    it1.setClass(mContext, TaoDetailActivity.class);
                                    mContext.startActivity(it1);
                                }
                            });

                        } else {
                            mSkuSellOutVisorgone.setVisibility(View.GONE);
                        }

                        //plus会员显示'
                        String is_show_member = mTaoDetailBean.getMember_data().getIs_show_member();
                        String user_is_member = mTaoDetailBean.getMember_data().getUser_is_member();
                        String member_price = mTaoDetailBean.getMember_data().getMember_price();
                        int price = Integer.parseInt(member_price);
                        if ("1".equals(is_show_member)) {
                            mSkuPlusVibility.setVisibility(View.VISIBLE);
                            skuPlusLine.setVisibility(View.VISIBLE);
                            if ("1".equals(user_is_member)) {
                                mSkuClickPlusKaitong.setVisibility(View.GONE);
                            } else {
                                mSkuClickPlusKaitong.setVisibility(View.VISIBLE);
                            }

                            mSkuPlusDesc.setText(mTaoDetailBean.getMember_data().getDesc());
                        } else {
                            mSkuPlusVibility.setVisibility(View.GONE);
                            skuPlusLine.setVisibility(View.VISIBLE);
                        }

                        //砍价
                        String bargainUrl = mTaoDetailBean.getBargain_url();
                        if (!TextUtils.isEmpty(bargainUrl)) {
                            mSkuBargainVibility.setVisibility(View.VISIBLE);
                        } else {
                            mSkuBargainVibility.setVisibility(View.GONE);
                        }

                        //top榜
                        HospitalTop hospitalTop = mTaoDetailBean.getHospital_top();
                        if (hospitalTop != null && !TextUtils.isEmpty(hospitalTop.getDesc())) {
                            mSkuTopVibility.setVisibility(View.VISIBLE);
                            String desc = hospitalTop.getDesc();
                            String level = hospitalTop.getLevel();
                            mSkuTopTitle.setText("入选『" + desc + "』NO." + level);
                        } else {
                            mSkuTopVibility.setVisibility(View.GONE);
                        }
                        Log.e(TAG, "mService" + mService.size());
                        for (int i = 0; i < mService.size(); i++) {
                            TagView tagView = new TagView(mContext);
                            tagView.setContent(mService.get(i).getTitle());
                            mSkuTagLlt.addView(tagView);
                        }

                        //sku metro
                        final TaoPsoter taoPoster = mTaoDetailBean.getTaoPoster();
                        if (taoPoster != null) {
                            String metroType = taoPoster.getMetro_type();
                            if ("2".equals(metroType)) { //显示图片的
                                String photo = taoPoster.getPhoto();
                                String height = taoPoster.getHeight();
                                mSkuMetroVisorgone.setVisibility(View.GONE);
                                mSkuMetroImg.setVisibility(View.VISIBLE);
                                ViewGroup.LayoutParams imgLayoutParams = mSkuMetroImg.getLayoutParams();
                                if (!TextUtils.isEmpty(height) && !"0".equals(height)) {
                                    imgLayoutParams.height = (int) (Integer.parseInt(height) * (windowsWight / 750f));
                                    mSkuMetroImg.setLayoutParams(imgLayoutParams);
                                } else {
                                    imgLayoutParams.height = Utils.dip2px(43);
                                    mSkuMetroImg.setLayoutParams(imgLayoutParams);
                                }

                                Glide.with(mContext)
                                        .load(photo)
                                        .into(mSkuMetroImg);
                                mSkuMetroImg.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        skuMetroJump(taoPoster);
                                    }
                                });

                            } else { //显示文字喇叭+ >
                                String startColor = taoPoster.getColor_value();
                                String endColor = taoPoster.getColour_value();
                                String description = taoPoster.getDescription();
                                if (!TextUtils.isEmpty(startColor) && !TextUtils.isEmpty(endColor)) {
                                    mSkuMetroVisorgone.setVisibility(View.VISIBLE);
                                    mSkuMetroImg.setVisibility(View.GONE);
                                    GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
                                            new int[]{Color.parseColor(startColor), Color.parseColor(endColor)});
                                    mSkuMetroVisorgone.setBackground(gradientDrawable);
                                    mSkuMetroContent.setText(description);
                                    mSkuMetroVisorgone.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            skuMetroJump(taoPoster);
                                        }
                                    });
                                }
                            }
                        } else {
                            mSkuMetroVisorgone.setVisibility(View.GONE);
                            mSkuMetroImg.setVisibility(View.GONE);
                        }

                        //优惠信息（分期）
                        mRepayment = mTaoDetailBean.getRepayment();
                        if (mRepayment.size() != 0) {
                            mSkuRepayVisorgone.setVisibility(View.VISIBLE);
                            for (int i = 0; i < mRepayment.size(); i++) {
                                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                lp.setMargins(0, 0, 15, 7);
                                RepaymentTextView repaymentTextView = new RepaymentTextView(mContext);
                                String desc = mRepayment.get(i).getDesc();
                                String replace = mRepayment.get(i).getReplace();
                                String[] split = desc.split("[replace]");
                                int start = split[0].length() - 1;
                                int end = start + replace.length();
                                String replace1 = desc.replace("[replace]", replace);
                                TextView skuRepayTitle = repaymentTextView.getSkuRepayTitle();
                                skuRepayTitle.setText(mRepayment.get(i).getTitle());
                                TextView skuRepayContent = repaymentTextView.getSkuRepayContent();
                                skuRepayContent.setTextSize(12);
                                skuRepayContent.setTextColor(ContextCompat.getColor(mContext, R.color._3));
                                SpannableString spannableString = new SpannableString(replace1);
                                spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FF92B3")), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                skuRepayContent.setText(spannableString);
                                skuRepayContent.setLayoutParams(lp);
                                mSkuRePayContainer.addView(repaymentTextView);
                            }
                            mRepayment.clear();
                        } else {
                            mSkuRepayVisorgone.setVisibility(View.GONE);
                        }

                        //已选
                        TaoDetailBean.RelTaoBeanX rel_tao = mTaoDetailBean.getRel_tao();
                        if (null != rel_tao) {
                            mSkuSelectProjectClick.setVisibility(View.VISIBLE);
                            String spe_name = "";
                            mSkuSpecContainer.removeAllViews();
                            mSkuSpecContainer.setMaxLine(1);
                            List<TaoDetailBean.RelTaoBeanX.RelTaoBean> rel_tao1 = rel_tao.getRel_tao();
                            for (int i = 0; i < rel_tao1.size(); i++) {
                                if ("1".equals(rel_tao1.get(i).getChecked())) {
                                    spe_name = rel_tao1.get(i).getSpe_name();
                                    addTag(spe_name);
                                    if (i == 0 && i + 1 < rel_tao1.size()) {
                                        addTag(rel_tao1.get(i + 1).getSpe_name());
                                    } else {
                                        if (i - 1 >= 0) {
                                            addTag(rel_tao1.get(i - 1).getSpe_name());
                                        }
                                    }
                                }

                            }
                            addTag("共" + rel_tao1.size() + "个项目可选");

                            mSkuSelectProjectTxt.setText(spe_name);
                        } else {
                            mSkuSelectProjectClick.setVisibility(View.GONE);
                        }


                        //优惠信息（促销)
                        mPromotion = mTaoDetailBean.getPromotion();
                        if (mPromotion.size() != 0) {
                            mSkuCuxiaoVisorgone.setVisibility(View.VISIBLE);
                            for (int i = 0; i < mPromotion.size(); i++) {
                                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                lp.setMargins(0, 0, 15, 7);
                                TextView textView = new TextView(mContext);
                                textView.setTextSize(12);
                                textView.setTextColor(ContextCompat.getColor(mContext, R.color._3));
                                textView.setText(mPromotion.get(i));
                                textView.setLayoutParams(lp);
                                mSkuCuxiaoContainer.addView(textView);
                            }
                            mPromotion.clear();
                        } else {
                            mSkuCuxiaoVisorgone.setVisibility(View.GONE);
                        }
                        CouponPriceInfo couponPriceInfo = mTaoDetailBean.getCouponPriceInfo();
                        int coupon_type = couponPriceInfo.getCoupon_type();
                        if (coupon_type <= 0) {
                            List<TaoDetailBean.CouponsBean> coupons = mTaoDetailBean.getCoupons();//优惠信息（领券)
                            switch (coupons.size()) {
                                case 0:
                                    mSkuCouponsLingquanCotainer.setVisibility(View.GONE);
                                    break;
                                case 1:
                                    mSkuCouponsLingquanCotainer.setVisibility(View.VISIBLE);
                                    if ("订金满减：".equals(coupons.get(0).getTitle())) {
                                        mSkuClickCoupons.setVisibility(View.VISIBLE);
                                        mSkuClickHoscoupons.setVisibility(View.GONE);
                                        TaoDetailBean.CouponsBean couponsBean = coupons.get(0);//定价满减
                                        List<String> data = couponsBean.getData();
                                        mSkuCouponsTitle.setText(couponsBean.getTitle());
                                        mSkuCouponShengyu.setText(couponsBean.getNum());
                                        for (int i = 0; i < data.size(); i++) {
                                            if (i < 2) {
                                                RedPacketView redPacketView = new RedPacketView(mContext);
                                                redPacketView.setTextView(data.get(i));
                                                mSkuCouponsContainer.addView(redPacketView);
                                            }

                                        }

                                    } else {
                                        mSkuClickCoupons.setVisibility(View.GONE);
                                        mSkuClickHoscoupons.setVisibility(View.VISIBLE);
                                        TaoDetailBean.CouponsBean hosCouponsBean = coupons.get(0);//医院红包
                                        List<String> hosData = hosCouponsBean.getData();
                                        mSkuHoscouponTitle.setText(hosCouponsBean.getTitle());
                                        mSkuHoscouponShengyu.setText(hosCouponsBean.getNum());
                                        for (int i = 0; i < hosData.size(); i++) {
                                            if (i < 2) {
                                                RedPacketView redPacketView = new RedPacketView(mContext);
                                                redPacketView.setTextView(hosData.get(i));
                                                mSkuHoscouponContainer.addView(redPacketView);
                                            }

                                        }

                                    }
                                    break;
                                case 2:
                                    mSkuCouponsLingquanCotainer.setVisibility(View.VISIBLE);
                                    mSkuClickCoupons.setVisibility(View.VISIBLE);
                                    mSkuClickHoscoupons.setVisibility(View.VISIBLE);
                                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mSkuClickHoscoupons.getLayoutParams();
                                    layoutParams.topMargin = Utils.dip2px(11);
                                    TaoDetailBean.CouponsBean couponsBean = coupons.get(0);//定价满减
                                    List<String> data = couponsBean.getData();

                                    TaoDetailBean.CouponsBean hosCouponsBean = coupons.get(1);//医院红包
                                    List<String> hosData = hosCouponsBean.getData();
                                    mSkuCouponsTitle.setText(couponsBean.getTitle());
                                    mSkuCouponShengyu.setText("剩余" + couponsBean.getNum());
                                    for (int i = 0; i < data.size(); i++) {
                                        if (i < 2) {
                                            RedPacketView redPacketView = new RedPacketView(mContext);
                                            redPacketView.setTextView(data.get(i));
                                            mSkuCouponsContainer.addView(redPacketView);
                                        }

                                    }

                                    mSkuHoscouponTitle.setText(hosCouponsBean.getTitle());
                                    mSkuHoscouponShengyu.setText("剩余" + hosCouponsBean.getNum());
                                    for (int i = 0; i < hosData.size(); i++) {
                                        if (i < 2) {
                                            RedPacketView redPacketView = new RedPacketView(mContext);
                                            redPacketView.setTextView(hosData.get(i));
                                            mSkuHoscouponContainer.addView(redPacketView);
                                        }
                                    }
                                    break;
                            }
                        } else {
                            mSkuCouponsLingquanCotainer.setVisibility(View.GONE);
                        }


                        mSkuExplainTxt.setText(mTaoDetailBean.getIteminfo().getTitle());


                        int baike_id = mTaoDetailBean.getBaike_id();
                        if (baike_id > 0) {
                            mSkuProcjetBaikeClick.setVisibility(View.VISIBLE);
                            mSkuProcjetBaikeText.setText("医美百科-" + mTaoDetailBean.getBaike_title());
                        } else {
                            mSkuProcjetBaikeClick.setVisibility(View.GONE);
                        }


                        AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();

                        SetCommentStyle(appraisal); //设置评论数据

                        DiaryListBean diaryList = mTaoDetailBean.getDiaryList();

                        setDiaryData(diaryList); //设置日记数据

                        TaoDetailBean.TaoAskBean tao_ask = mTaoDetailBean.getTao_ask();
                        SetAskallStyle(tao_ask);    //问问买过的人

                        //医院
                        mHos_doc = mTaoDetailBean.getHos_doc();
                        phone400 = mHos_doc.getPhone();// 400电话
                        if (phone400.contains(",")) {
                            String[] split = phone400.split(",");
                            phone400 = split[0];
                            mPhoneFen = split[1];
                        }
                        Glide.with(mContext).load(mHos_doc.getHos_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.ten_ball_placeholder).error(R.drawable.ten_ball_placeholder).into(mSkuHosImg);
                        mSkuHosName.setText(mHos_doc.getHospital_name());

                        String rateScale = mHos_doc.getRateScale();
                        if (!TextUtils.isEmpty(rateScale)) {
                            int progress = Integer.parseInt(rateScale);
                            mHosRatingBar.setProgressBar(progress);
                        }
                        //医院属性赋值
                        List<String> hosAttr = mHos_doc.getHos_attr();
                        if (CollectionUtils.isNotEmpty(hosAttr)) {
                            String property = hosAttr.get(0);
                            if (!TextUtils.isEmpty(property)) {
                                mHosProperty.setText(property);
                            }

                            String area = hosAttr.get(1);
                            if (!TextUtils.isEmpty(area)) {
                                mHosArea.setText(area);
                            }

                            String rank = hosAttr.get(2);
                            if (!TextUtils.isEmpty(rank)) {
                                mHosRank.setText(rank);
                            }

                            String bednum = hosAttr.get(3);
                            if (!TextUtils.isEmpty(bednum)) {
                                mHosBednum.setText(bednum);
                            }
                        }
                        mHosAddressTv.setText(mHos_doc.getHospital_address());
                        String distance = mHos_doc.getDistance();
                        if (!TextUtils.isEmpty(distance)) {
                            mHosDistanceTv.setText(distance);
                        }

                        String doc_name = mHos_doc.getDoc_name();
                        String docUserId = mHos_doc.getDoc_user_id();
                        mTaotaoDocZhiweiTv.setText(mHos_doc.getDoc_title());
                        if (!TextUtils.isEmpty(docUserId) && !"0".equals(docUserId)) {
                            mDocZhuyeRly.setVisibility(View.VISIBLE);
                            Glide.with(mContext).load(mHos_doc.getDoc_img())
                                    .placeholder(R.drawable.sku_doc_img)
                                    .error(R.drawable.sku_doc_img)
                                    .transform(new GlideCircleTransform(mContext))
                                    .into(mTaotaoDocImg);
                            mTaotaoDocNameTv.setText(doc_name);
                            String kind = mHos_doc.getKind();
                            if ("1".equals(kind) || "2".equals(kind)) {
                                mSkuHosImgRenzheng.setVisibility(View.VISIBLE);
                            } else {
                                mSkuHosImgRenzheng.setVisibility(View.GONE);
                            }
                            String docScore = mHos_doc.getDoc_score();
                            if (!TextUtils.isEmpty(docScore)) {
                                int progress = Integer.parseInt(docScore);
                                mTaoDocRatingbar.setProgressBar(progress);
                            }

                            String docSkuOrderNum = mHos_doc.getDoc_sku_order_num();
                            if (!TextUtils.isEmpty(docSkuOrderNum)) {
                                mTaoDocBook.setText(docSkuOrderNum);
                            } else {
                                mTaoDocBook.setText("");
                            }
                            String docType = mHos_doc.getDoc_type();
                            if (!TextUtils.isEmpty(docType)) {//医生标识: 0:无标识 1:认证 2:特邀(悦美好医的为特邀)（V706）
                                mSkuDocImgRenzheng.setVisibility(View.VISIBLE);
                                if ("1".equals(docType)) {
                                    mSkuDocImgRenzheng.setBackgroundResource(R.drawable.sku_doc_vrenzheng);
                                } else if ("2".equals(docType)) {
                                    mSkuDocImgRenzheng.setBackgroundResource(R.drawable.sku_doc_teyao);
                                } else {
                                    mSkuDocImgRenzheng.setVisibility(View.GONE);
                                }
                            } else {
                                mSkuDocImgRenzheng.setVisibility(View.GONE);
                            }

                            List<String> goodBoard = mHos_doc.getGood_board();
                            if (CollectionUtils.isNotEmpty(goodBoard)) {
                                mTaoDocSkilledVisorgone.setVisibility(View.VISIBLE);
                                mTaoDocSkilledContainer.removeAllViews();
                                for (int i = 0; i < goodBoard.size(); i++) {
                                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    lp.setMargins(0, 0, 7, 0);
                                    TextView textView = new TextView(mContext);
                                    textView.setLayoutParams(lp);
                                    textView.setTextSize(11);
                                    textView.setTextColor(ContextCompat.getColor(mContext, R.color._7b));
                                    textView.setText(goodBoard.get(i));
                                    textView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_doc_good_shap));
                                    textView.setPadding(8, 1, 8, 1);
                                    mTaoDocSkilledContainer.addView(textView);

                                }
                            } else {
                                mTaoDocSkilledVisorgone.setVisibility(View.GONE);
                            }

                        } else {
                            mDocZhuyeRly.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (ll_root != null) {
                        ll_root.setVisibility(View.VISIBLE);
                    }
                } else {
                    mFunctionManager.showShort(serverData.message);
                    if (getActivity() != null) {
                        getActivity().finish();
                    }

                }
            }
        });

    }


    public void LodUrl(WebView webView, String url, Map<String, Object> map) {
        Log.e(TAG, "LodUrl1 === " + url);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, map);
        if (null != webView) {
            webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }


    /**
     * 拼团样式
     */
    private void setPintuanStyle() {
        //优先级   拼团>秒杀>大促>常规
        TaoDetailBean.BasedataBean basedata = mTaoDetailBean.getBasedata();//常规数据
        TaoDetailBean.MemberDataBean member_data = mTaoDetailBean.getMember_data();//plus会员数据
        TaoDetailBean.GroupInfoBean groupInfo = mTaoDetailBean.getGroupInfo();//拼团
        TaoDetailBean.RegInfoBean regInfo = mTaoDetailBean.getRegInfo();//秒杀数据

        String isMonthBigPromotion = basedata.getIsMonthBigPromotion();

        CouponPriceInfo couponPriceInfo = mTaoDetailBean.getCouponPriceInfo();//券后价数据
        //大促数据
        proInfo = mTaoDetailBean.getProInfo();
        TaoUserCoupons taoUseCoupons = mTaoDetailBean.getTaoUseCoupons();//常规优惠价

        String is_group = groupInfo.getIs_group(); //是否是拼团
        String is_seg = regInfo.getIs_seg();//是否是秒杀
        String isBigPromotion = proInfo.getIsBigPromotion();//是否是大促
        int coupon_type = couponPriceInfo.getCoupon_type();//	>0 显示券后价
        String dacuPrice = "";
        String status = "";
        String nowTime = "";
        String endTime = "";
        String timeDesc = "";
        String bigPromotionTitle = "";
        String dacuType = "";
        boolean isActivity = true; //是否是活动样式默认是
        boolean isRedpack = false;//是否是常规红包优惠券
        if ("1".equals(is_group)) {
            dacuPrice = groupInfo.getGroup_price();
            status = "拼团价";
            timeDesc = "距结束仅剩";
            dacuType = "拼团价";
            nowTime = groupInfo.getNowTime();
            endTime = groupInfo.getEndTime();
            bigPromotionTitle = proInfo.getBigPromotionTitle();
            mSkuLowPriceVisorgone.setVisibility(View.GONE);
        } else {
            if ("1".equals(is_seg)) {
                dacuPrice = basedata.getPrice_discount();
                status = "秒杀价";
                dacuType = "秒杀价";
                timeDesc = regInfo.getTime_str();
                nowTime = regInfo.getNowTime();
                endTime = regInfo.getEndTime();
                bigPromotionTitle = regInfo.getBigPromotionTitle();
                mSkuLowPriceVisorgone.setVisibility(View.GONE);
            } else {
                if ("1".equals(isBigPromotion)) {
                    dacuPrice = basedata.getPrice_discount();
                    status = "促销价";
                    dacuType = "促销价";
                    timeDesc = proInfo.getTime_str();
                    nowTime = proInfo.getNowTime();
                    endTime = proInfo.getEndTime();
                    bigPromotionTitle = proInfo.getBigPromotionTitle();
                    mSkuLowPriceVisorgone.setVisibility(View.GONE);
                } else {
                    if (coupon_type > 0) {
                        dacuType = "悦美价";
                        dacuPrice = basedata.getPrice_discount();
                    } else {
                        String low_price = mTaoDetailBean.getLow_price();
                        if (!TextUtils.isEmpty(low_price)) {
                            status = basedata.getZekou() + "折";
                            dacuPrice = basedata.getPrice_discount();
                            mSkuLowPriceVisorgone.setVisibility(View.VISIBLE);
                            mSkuLowPriceTxt.setText(low_price);
                        } else {
                            isActivity = false;
                            mSkuLowPriceVisorgone.setVisibility(View.GONE);
                            if (null != taoUseCoupons) {
                                String money = taoUseCoupons.getMoney();
                                if (!"0".equals(money) && "1".equals(mStatus)) {
                                    isRedpack = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        if (isActivity) {
            mSkuDacuVisorgone.setVisibility(View.VISIBLE);  //活动样式头显示
            String bigPromotionImg = proInfo.getBigPromotionImg(); //大促背景
            Glide.with(mContext).load(bigPromotionImg).placeholder(R.drawable.sku_dacu_background2).error(R.drawable.sku_dacu_background2).into(mSkuDacuBackground);
            mSkuPriceVisorgone.setVisibility(View.VISIBLE); //活动价格显示
            mSkuConposVisorgone.setVisibility(View.GONE);  //普通红包不显示
            mSkuHsopriceVisorgone.setVisibility(View.GONE); //普通医院价格不显示

            String show_str = basedata.getShow_str();
            if (!TextUtils.isEmpty(show_str)) {  //大促剩余显示
                mSkuDacuShenyu.setVisibility(View.VISIBLE);
                mSkuDacuShenyu.setText(show_str);
            } else {
                mSkuDacuShenyu.setVisibility(View.GONE);
            }
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mSkuTitle.getLayoutParams();
            layoutParams.topMargin = Utils.dip2px(8);
            mSkuTitle.setLayoutParams(layoutParams);
            String feeScale = basedata.getFeeScale();
            if (!TextUtils.isEmpty(feeScale)) {  //活动规格
                mSkuDacuSpec.setVisibility(View.VISIBLE);
                mSkuDacuSpec.setText(feeScale);
            } else {
                mSkuDacuSpec.setVisibility(View.GONE);
            }

            if (coupon_type > 0) {
                mSkuDacuPrice.setText(couponPriceInfo.getCouponPrice());  //券后价
                mSkuDacuPriceDesc.setVisibility(View.GONE);
                mSkuDacuPlusVisorgone.setVisibility(View.GONE);
                mCouponsPriceAfterDesc.setVisibility(View.VISIBLE);
                mCouponsPriceAfterTip.setVisibility(View.VISIBLE);
                mSkuDacuHospriceVisorgone.setVisibility(View.VISIBLE);
                mSkuDacuTypePrice.setText(dacuType);
//                mSkuDacuHosprice.setText("¥" + dacuPrice);
                List<Integer> money = couponPriceInfo.getCoupons().getData().getMoney();
                int sumMoney=0;
                if (CollectionUtils.isNotEmpty(money)){
                    for (int i = 0; i < money.size(); i++) {
                        sumMoney+=money.get(i);
                    }
                }
                String member_price = member_data.getMember_price();
                int plusPrice = Integer.parseInt(member_price);
                if (plusPrice >= 0) {     //plus会员价和医院价显示
                    mSkuDacuPlusVisorgone.setVisibility(View.VISIBLE);
                    mSkuDacuHospriceVisorgone.setVisibility(View.GONE);
                    mSkuDacuPlusPrice.setText("¥" + member_price);
                    mSkuDacuPriceDesc.setVisibility(View.GONE);
                } else {
                    mSkuDacuPlusVisorgone.setVisibility(View.GONE);
                    mSkuDacuTypePrice.setText("= "+dacuType+"¥"+basedata.getPrice_discount()+" - "+"促销券¥"+sumMoney);
                }

            } else {
                mSkuDacuPrice.setText(dacuPrice);  //活动价
                mCouponsPriceAfterClick.setVisibility(View.GONE);
                mCouponsPriceAfterDesc.setVisibility(View.GONE);
                mCouponsPriceAfterTip.setVisibility(View.GONE);
                mSkuDacuHospriceVisorgone.setVisibility(View.VISIBLE);
                String member_price = member_data.getMember_price();
                int plusPrice = Integer.parseInt(member_price);
                if (plusPrice >= 0) {     //plus会员价和医院价显示
                    mSkuDacuPlusVisorgone.setVisibility(View.VISIBLE);
                    mSkuDacuHospriceVisorgone.setVisibility(View.GONE);
                    mSkuDacuPlusPrice.setText("¥" + member_price);
                    mSkuDacuPriceDesc.setVisibility(View.GONE);
                } else {
                    mSkuDacuPlusVisorgone.setVisibility(View.GONE);
                    String hosPrice = basedata.getPrice();//医院价
                    int hosPrice1 = Integer.parseInt(hosPrice);
                    if (hosPrice1 > 0) {
                        mSkuDacuHospriceVisorgone.setVisibility(View.VISIBLE);
                        mSkuDacuHosprice.setText("原价 ¥" + hosPrice);
                        mSkuDacuHosprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//添加横划线
                        mSkuDacuHosprice.getPaint().setAntiAlias(true);  //抗锯齿
                    } else {
                        mSkuDacuHospriceVisorgone.setVisibility(View.GONE);
                    }
                }
                if (!TextUtils.isEmpty(status)) {    //活动标识
                    mSkuDacuPriceDesc.setVisibility(View.VISIBLE);
                    if ("1".equals(isMonthBigPromotion)) {
                        mSkuDacuPriceDesc.setTextColor(ContextCompat.getColor(mContext, R.color.dacu_text));
                        mSkuDacuPriceDesc.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_coupons_price_after_desc_bg));
                    } else {
                        mSkuDacuPriceDesc.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                        mSkuDacuPriceDesc.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_ff1848));
                    }
                    mSkuDacuPriceDesc.setText(status);
                } else {
                    mSkuDacuPriceDesc.setVisibility(View.GONE);
                }
            }
            CouponsData data = couponPriceInfo.getCoupons().getData();
            if (data != null){
                List<String> desc = data.getDesc();
                if (CollectionUtils.isNotEmpty(desc)) {
                    mCouponsPriceAfterContainer.removeAllViews();
                    mCouponsPriceAfterClick.setVisibility(View.VISIBLE);  //券后价显示
                    for (int i = 0; i < desc.size(); i++) {
                        if (i < 2) {
                            TextView textView = new TextView(mContext);
                            LinearLayout.LayoutParams textLayPams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            textLayPams.setMargins(0, 0, 10, 0);//4个参数按顺序分别是左上右下
                            textView.setLayoutParams(textLayPams);
                            textView.setTextColor(ContextCompat.getColor(mContext, R.color._ff002a));
                            textView.setTextSize(13);
                            textView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_coupons_price));
                            textView.setPadding(Utils.dip2px(4), Utils.dip2px(1), Utils.dip2px(4), Utils.dip2px(1));
                            textView.setText(desc.get(i));
                            mCouponsPriceAfterContainer.addView(textView);
                        }
                    }
                } else {
                    mCouponsPriceAfterClick.setVisibility(View.GONE);
                }
            }



            if (!TextUtils.isEmpty(nowTime) && !TextUtils.isEmpty(endTime)) {
                mSkuTimeContainer.setVisibility(View.VISIBLE);
                String startTimeAct = Utils.stampToPhpDate(nowTime);
                String endTimeAct = Utils.stampToPhpDate(endTime);
                long[] timeSubAct = Utils.getTimeSub(startTimeAct, endTimeAct);
                long[] timesAct = {timeSubAct[0], timeSubAct[1], timeSubAct[2], timeSubAct[3]};
                mSkuTimeContainer.removeAllViews();
                if ("1".equals(isMonthBigPromotion)) {
                    DacuCountDownView dacuCountDownView = new DacuCountDownView(mContext);
                    dacuCountDownView.setTimes(timesAct);
                    if (!dacuCountDownView.isRun()) {
                        dacuCountDownView.beginRun();
                    }
                    mSkuTimeContainer.addView(dacuCountDownView);
                    mOnEventCallBack.onDacuCountDownCallBack(dacuCountDownView);

                } else {
                    CountDownView countDownView = new CountDownView(mContext); //倒计时
                    countDownView.setSkuTimeDesc(timeDesc);
                    countDownView.setTimes(timesAct);
                    if (!countDownView.isRun()) {
                        countDownView.beginRun();
                    }
                    mSkuTimeContainer.addView(countDownView);
                    mOnEventCallBack.onCountDwonViewCallBack(countDownView);
                }


            } else {
                mSkuTimeContainer.setVisibility(View.GONE);
            }
            //大促CEO显示
            if (!TextUtils.isEmpty(bigPromotionTitle.trim())) {
                mSkuDacuceoVisorgone.setVisibility(View.VISIBLE);
                mSkuDacuceoContent.setText(bigPromotionTitle);
            } else {
                mSkuDacuceoVisorgone.setVisibility(View.GONE);
            }


            List<GroupBean> group = groupInfo.getGroup();
            if (group.size() > 0) {      //有拼团列表
                mIsSkuGroup.setVisibility(View.VISIBLE);
                if (group.size() == 1) {
                    mSkuGroup1.setVisibility(View.VISIBLE);
                    mSkuGroup2.setVisibility(View.GONE);
                    final GroupBean groupBean = group.get(0);
                    Glide.with(mContext).load(groupBean.getGroup_user_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mGroupUserImg1);
                    mGroupUserName1.setText(groupBean.getGroup_user_name());
                    mGroupCompletePeople1.setText(groupBean.getGroup_complete_people());
                    String startTime = Utils.stampToPhpDate(groupBean.getGroup_start_time());
                    String endTimee = Utils.stampToPhpDate(groupBean.getGroup_end_time());
                    long[] timeSub = Utils.getTimeSub(startTime, endTimee);

                    long[] times = {timeSub[0], timeSub[1], timeSub[2], timeSub[3]};
                    mGroupTime1.setTimes(times);
                    mGroupTime1.setCopywriting("仅剩");
                    if (!mGroupTime1.isRun()) {
                        mGroupTime1.beginRun();
                    }
                    //拼团一
                    mBtnTuxedo1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isLogin()) {
                                if (Utils.isBind()) {
                                    kefu_nickName = Cfg.loadStr(mContext, FinalConstant.UNAME, "");
                                    SerMap serMap = new SerMap();
                                    serMap.setMap(showMap);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("serMap", serMap);

                                    Intent intent = new Intent(mContext, SpeltActivity.class);
                                    intent.putExtra("sku_id", mSkuId);
                                    intent.putExtra("type", "0");
                                    intent.putExtra("source", source);
                                    intent.putExtra("objid", objid);
                                    intent.putExtra("kefu_nickName", kefu_nickName);
                                    intent.putExtra("group_id", groupBean.getGroup_id());
                                    intent.putExtra("num", 0);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } else {
                                    Utils.jumpBindingPhone(mContext);
                                }
                            } else {
                                Utils.jumpLogin(mContext);
                            }
                        }
                    });

                } else {
                    mSkuGroup1.setVisibility(View.VISIBLE);
                    mSkuGroup2.setVisibility(View.VISIBLE);
                    final GroupBean groupBean1 = group.get(0);

                    Glide.with(mContext).load(groupBean1.getGroup_user_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mGroupUserImg1);

                    mGroupUserName1.setText(groupBean1.getGroup_user_name());
                    mGroupCompletePeople1.setText(groupBean1.getGroup_complete_people());

                    String startTime1 = Utils.stampToPhpDate(groupBean1.getGroup_start_time());
                    String endTime1 = Utils.stampToPhpDate(groupBean1.getGroup_end_time());
                    long[] timeSub1 = Utils.getTimeSub(startTime1, endTime1);

                    long[] times1 = {timeSub1[0], timeSub1[1], timeSub1[2], timeSub1[3]};
                    mGroupTime1.setTimes(times1);
                    mGroupTime1.setCopywriting("仅剩");
                    if (!mGroupTime1.isRun()) {
                        mGroupTime1.beginRun();
                    }

                    //拼团一
                    mBtnTuxedo1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Log.e(TAG, "source == " + source);
                            Log.e(TAG, "objid == " + objid);
                            Log.e(TAG, "groupId == " + groupBean1.getGroup_id());
                            Log.e(TAG, "kefu_nickName == " + kefu_nickName);

                            Log.e(TAG, "fenqi == " + showMap.get("fenqi"));
                            Log.e(TAG, "refund == " + showMap.get("refund"));
                            Log.e(TAG, "taoid == " + showMap.get("taoid"));
                            Log.e(TAG, "subhead == " + showMap.get("subhead"));
                            Log.e(TAG, "is_rongyun == " + showMap.get("is_rongyun"));
                            Log.e(TAG, "hos_userid == " + showMap.get("hos_userid"));

                            Log.e(TAG, "mTitle == " + showMap.get("mTitle"));
                            Log.e(TAG, "priceDiscount == " + showMap.get("priceDiscount"));
                            Log.e(TAG, "mImg == " + showMap.get("mImg"));

                            Log.e(TAG, "relTitle == " + showMap.get("relTitle"));
                            Log.e(TAG, "sku_type == " + showMap.get("sku_type"));

                            if (Utils.isLogin()) {
                                if (Utils.isBind()) {
                                    SerMap serMap = new SerMap();
                                    serMap.setMap(showMap);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("serMap", serMap);

                                    Intent intent = new Intent(mContext, SpeltActivity.class);
                                    intent.putExtra("sku_id", mSkuId);
                                    intent.putExtra("type", "0");
                                    intent.putExtra("source", source);
                                    intent.putExtra("objid", objid);
                                    intent.putExtra("kefu_nickName", kefu_nickName);
                                    intent.putExtra("group_id", groupBean1.getGroup_id());
                                    intent.putExtra("num", 0);
                                    intent.putExtras(bundle);

                                    startActivity(intent);
                                } else {
                                    Utils.jumpBindingPhone(mContext);
                                }
                            } else {
                                Utils.jumpLogin(mContext);
                            }
                        }
                    });


                    final GroupBean groupBean2 = group.get(1);

                    Glide.with(mContext).load(groupBean2.getGroup_user_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mGroupUserImg2);

                    mGroupUserName2.setText(groupBean2.getGroup_user_name());
                    mGroupCompletePeople2.setText(groupBean2.getGroup_complete_people());

                    String startTime2 = Utils.stampToPhpDate(groupBean2.getGroup_start_time());
                    String endTime2 = Utils.stampToPhpDate(groupBean2.getGroup_end_time());
                    long[] timeSub2 = Utils.getTimeSub(startTime2, endTime2);

                    long[] times2 = {timeSub2[0], timeSub2[1], timeSub2[2], timeSub2[3]};
                    mGroupTime2.setTimes(times2);
                    mGroupTime2.setCopywriting("仅剩");
                    if (!mGroupTime2.isRun()) {
                        mGroupTime2.beginRun();
                    }

                    //拼团二
                    mBtnTuxedo2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Log.e(TAG, "source == " + source);
                            Log.e(TAG, "objid == " + objid);
                            Log.e(TAG, "groupId == " + groupBean1.getGroup_id());
                            Log.e(TAG, "kefu_nickName == " + kefu_nickName);

                            Log.e(TAG, "fenqi == " + showMap.get("fenqi"));
                            Log.e(TAG, "refund == " + showMap.get("refund"));
                            Log.e(TAG, "taoid == " + showMap.get("taoid"));
                            Log.e(TAG, "subhead == " + showMap.get("subhead"));
                            Log.e(TAG, "is_rongyun == " + showMap.get("is_rongyun"));
                            Log.e(TAG, "hos_userid == " + showMap.get("hos_userid"));

                            Log.e(TAG, "mTitle == " + showMap.get("mTitle"));
                            Log.e(TAG, "priceDiscount == " + showMap.get("priceDiscount"));
                            Log.e(TAG, "mImg == " + showMap.get("mImg"));

                            Log.e(TAG, "relTitle == " + showMap.get("relTitle"));
                            Log.e(TAG, "sku_type == " + showMap.get("sku_type"));

                            if (Utils.isLogin()) {
                                if (Utils.isBind()) {
                                    SerMap serMap = new SerMap();
                                    serMap.setMap(showMap);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("serMap", serMap);

                                    Intent intent = new Intent(mContext, SpeltActivity.class);
                                    intent.putExtra("sku_id", mSkuId);
                                    intent.putExtra("type", "0");
                                    intent.putExtra("source", source);
                                    intent.putExtra("objid", objid);
                                    intent.putExtra("kefu_nickName", kefu_nickName);
                                    intent.putExtra("group_id", groupBean2.getGroup_id());
                                    intent.putExtra("num", 1);
                                    intent.putExtras(bundle);

                                    startActivity(intent);
                                } else {
                                    Utils.jumpBindingPhone(mContext);
                                }
                            } else {
                                Utils.jumpLogin(mContext);
                            }
                        }
                    });
                }
            } else {
                mIsSkuGroup.setVisibility(View.GONE);
            }
        } else {
            mSkuDacuVisorgone.setVisibility(View.GONE);  //活动样式头不显示
            mSkuHsopriceVisorgone.setVisibility(View.VISIBLE); //普通医院价格显示
            mSkuCommonPrice.setText(basedata.getPrice_discount());  //悦美价
            String feeScale = basedata.getFeeScale();//规格
            if (!TextUtils.isEmpty(feeScale)) {
                mSkuCommonSpec.setVisibility(View.VISIBLE);
                mSkuCommonSpec.setText(feeScale);
            } else {
                mSkuCommonSpec.setVisibility(View.GONE);
            }

            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mSkuTitle.getLayoutParams();
            layoutParams.topMargin = Utils.dip2px(4);
            mSkuTitle.setLayoutParams(layoutParams);

            String member_price = member_data.getMember_price();
            int plusPrice = Integer.parseInt(member_price);
            if (plusPrice >= 0) {     //plus会员价
                mSkuCommonPlusVisorgone.setVisibility(View.VISIBLE);
                mSkuPlusPrice.setText(member_price);
                mSkuHosprice.setVisibility(View.GONE);
                mSkuDisscount.setVisibility(View.GONE);
            } else {
                mSkuCommonPlusVisorgone.setVisibility(View.GONE);
                String hosPrice = basedata.getPrice();//医院价
                int hosPrice1 = Integer.parseInt(hosPrice);
                if (hosPrice1 > 0) {
                    mSkuHosprice.setVisibility(View.VISIBLE);
                    mSkuHosprice.setText("¥" + hosPrice);
                    mSkuHosprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//添加横划线
                    mSkuHosprice.getPaint().setAntiAlias(true);  //抗锯齿
                } else {
                    mSkuHosprice.setVisibility(View.GONE);
                }

                String zekou = basedata.getZekou();
                if (!TextUtils.isEmpty(zekou)) {
                    mSkuDisscount.setVisibility(View.VISIBLE);
                    mSkuDisscount.setText(zekou + "折");
                } else {
                    mSkuDisscount.setVisibility(View.GONE);
                }

            }


            mSkuPriceVisorgone.setVisibility(View.GONE); //活动价格不显示
            if (isRedpack) {
                mSkuDacuVisorgone.setVisibility(View.VISIBLE);  //活动样式头不显示
                mSkuConposVisorgone.setVisibility(View.VISIBLE);  //普通红包显示
                int coupons_type = taoUseCoupons.getCoupons_type();
                String money = taoUseCoupons.getMoney();
                String otherContnet = "";
                if (coupons_type == 1) {
                    otherContnet = "您有一张" + money + "元订金券下单可立减";
                } else {
                    otherContnet = "您有一张" + money + "元尾款红包下单可立减";
                }
                mSkuConposPrice.setText(otherContnet);
                String endtime = taoUseCoupons.getEndtime();
                if (!TextUtils.isEmpty(endtime)) {
                    String startTime = Utils.stampToJavaAndPhpDate(System.currentTimeMillis() + "");
                    String endTimer = Utils.stampToJavaAndPhpDate(endtime);
                    long[] time = Utils.getTimeSub(startTime, endTimer);
                    long[] timesAct = {time[0], time[1], time[2], time[3]};
                    if (time[0] < 2) {
                        mSkuConposPrice.setMaxEms(9);
                        mSkuTimeContainer.setVisibility(View.VISIBLE);
                        CountDownView countDownView = new CountDownView(mContext); //倒计时
                        countDownView.setSkuTimeDesc("即将到期");
                        countDownView.setTimes(timesAct);
                        if (!countDownView.isRun()) {
                            countDownView.beginRun();
                        }
                        mSkuTimeContainer.removeAllViews();
                        mSkuTimeContainer.addView(countDownView);
                    } else {
                        mSkuTimeContainer.setVisibility(View.GONE);
                    }

                } else {
                    mSkuTimeContainer.setVisibility(View.GONE);
                }

            }

        }


    }


    /**
     * sku metro 跳转
     *
     * @param taoPoster
     */
    private void skuMetroJump(TaoPsoter taoPoster) {
        String flagId = taoPoster.getFlag_id();
        String toLink = taoPoster.getTo_link();
        if ("3".equals(flagId)) { //专题
            WebUrlTypeUtil.getInstance(mContext).urlToApp(toLink);
        } else {
            if (Utils.noLoginChat()) {
                metroChat();
            } else {
                if (Utils.isLoginAndBind(mContext)) {
                    metroChat();
                }
            }

        }
    }

    private void metroChat() {
        if (mTaoDetailBean != null) {
            TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
            String hos_userid = chatData.getHos_userid();
            String is_rongyun = chatData.getIs_rongyun();
            TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();
            String hospital_id = hos_doc.getHospital_id();
            String doc_user_id = hos_doc.getDoc_user_id();
            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(chatData.getObj_id())
                        .setObjType(chatData.getObj_type() + "")
                        .setTitle(mTaoDetailBean.getBasedata().getTitle())
                        .setPrice(mTaoDetailBean.getBasedata().getPrice_discount())
                        .setImg(mTaoDetailBean.getPic().get(0).getImg())
                        .setMemberPrice(mTaoDetailBean.getMember_data().getMember_price())
                        .setYmClass(chatData.getYmaq_class())
                        .setYmId(chatData.getYmaq_id())
                        .setSkuId(mSkuId)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                tongjiClick("1");
                //test
                HashMap<String, String> event_params = chatData.getEvent_params();

                YmStatistics.getInstance().tongjiApp(new EventParamData(chatData.getEvent_name(), "0"), event_params);
            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }


    private void addTag(String tagName) {
        TextView textView = new TextView(mContext);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color._99));
        textView.setText(tagName);
        textView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_tagview_unselect));
        textView.setPadding(Utils.dip2px(6), Utils.dip2px(3), Utils.dip2px(6), Utils.dip2px(3));
        mSkuSpecContainer.addView(textView);
    }

    /**
     * 设置评论数据
     *
     * @param appraisal
     */
    @SuppressLint("ClickableViewAccessibility")
    private void SetCommentStyle(AppRaisalBean appraisal) {
        if (null != appraisal) {
            String title = appraisal.getTitle();
            final String jumpUrl = appraisal.getJumpUrl();
            String rate = appraisal.getRate();
            String num = appraisal.getNum();
            final List<AppraisalListBean> appraisalList = appraisal.getAppraisalList();
            if (null != appraisalList && appraisalList.size() > 0) {
                mSkuCommentVisorgone.setVisibility(View.VISIBLE);
                mSkuCommentKoubeiTitle.setText(title);
//                mSkuCommentKoubeiCheck.setText(rate);
                ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                scrollLinearLayoutManager.setScrollEnable(false);
                mSkuCommentRecycler.setLayoutManager(scrollLinearLayoutManager);
                SkuCommentListAdapter skuCommentListAdapter = new SkuCommentListAdapter(R.layout.sku_comment_list, appraisalList);
                mSkuCommentRecycler.setAdapter(skuCommentListAdapter);
                mSkuCommentRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color._f5)));
                skuCommentListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                        HashMap<String, String> event_params = appraisalList.get(position).getEvent_params();
//                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.APPRAISAL_TO_DIARY,(position+1)+""),event_params,new ActivityTypeData("2"));
//                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appraisalList.get(position).getAppmurl());
                        AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();
                        HashMap<String, String> event_params = appraisal.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "text", "1"), event_params, new ActivityTypeData("2"));
                        CommentFragment commentFragment = changeFragment(appraisal.getJumpUrl());
                        mOnEventCallBack.onCommentClick(true, "评论详情", commentFragment);
                    }
                });
                skuCommentListAdapter.setmItemEventClick(new SkuCommentListAdapter.ItemEventClick() {
                    @Override
                    public void onItemHeadClick(View v, int index) {
//                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appraisalList.get(index).getUser_center_url());
                        AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();
                        HashMap<String, String> event_params = appraisal.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "text", "1"), event_params, new ActivityTypeData("2"));
                        CommentFragment commentFragment = changeFragment(appraisal.getJumpUrl());
                        mOnEventCallBack.onCommentClick(true, "评论详情", commentFragment);
                    }

                    @Override
                    public void onItemButtonClik(View v, int index) {
                        AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();
                        HashMap<String, String> event_params = appraisal.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "text", "1"), event_params, new ActivityTypeData("2"));
                        CommentFragment commentFragment = changeFragment(appraisal.getJumpUrl());
                        mOnEventCallBack.onCommentClick(true, "评论详情", commentFragment);
                    }
                });
                mSkuCommentRecycler.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
            } else {
                mSkuCommentVisorgone.setVisibility(View.GONE);
            }
        } else {
            mSkuCommentVisorgone.setVisibility(View.GONE);
        }

    }


    private void setDiaryData(final DiaryListBean diaryListBean) {
        if (null != diaryListBean) {
            String title = diaryListBean.getTitle();
            final List<BBsListData550> diaryList = diaryListBean.getDiaryList();
            if (null != diaryList && diaryList.size() > 0) {
                mSkuDiaryVisorgone.setVisibility(View.VISIBLE);
                mSkuDiaryTitle.setText(title);
                ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                scrollLinearLayoutManager.setScrollEnable(false);
                mSkuDiaryList.setLayoutManager(scrollLinearLayoutManager);
                SkuDiaryListAdapter skuDiaryListAdapter = new SkuDiaryListAdapter(R.layout.sku_diary_list, diaryList);
                mSkuDiaryList.setAdapter(skuDiaryListAdapter);
                mSkuDiaryList.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color._f5)));
                skuDiaryListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                        HashMap<String, String> event_params = diaryList.get(position).getEvent_params();
//                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARY_NEW,(position+1)+""),event_params,new ActivityTypeData("2"));
//                        WebUrlTypeUtil.getInstance(mContext).urlToApp(diaryList.get(position).getAppmurl());
                        if (Utils.isFastDoubleClick()) return;
                        DiaryListBean diaryList = mTaoDetailBean.getDiaryList();
                        HashMap<String, String> diaryListEventParams = diaryList.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "img", "0"), diaryListEventParams, new ActivityTypeData("2"));
                        CommentFragment commentFragment1 = changeFragment(diaryList.getJumpUrl());
                        mOnEventCallBack.onCommentClick(true, "日记详情", commentFragment1);
                    }
                });
                skuDiaryListAdapter.setmItemEventClick(new SkuDiaryListAdapter.ItemEventClick() {
                    @Override
                    public void onItemHeadClick(View v, int index) {
//                        WebUrlTypeUtil.getInstance(mContext).urlToApp(diaryList.get(index).getUser_center_url());
                        if (Utils.isFastDoubleClick()) return;
                        DiaryListBean diaryList = mTaoDetailBean.getDiaryList();
                        HashMap<String, String> diaryListEventParams = diaryList.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "img", "0"), diaryListEventParams, new ActivityTypeData("2"));
                        CommentFragment commentFragment1 = changeFragment(diaryList.getJumpUrl());
                        mOnEventCallBack.onCommentClick(true, "日记详情", commentFragment1);
                    }

                    @Override
                    public void onItemFansClick(View v, int index) {

                    }

                    @Override
                    public void onItemButtonClik(View v, int index) {
                        if (Utils.isFastDoubleClick()) return;
                        String jumpUrl = diaryListBean.getJumpUrl();
                        HashMap<String, String> event_params = diaryListBean.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "img", "1"), event_params, new ActivityTypeData("2"));
                        CommentFragment commentFragment = changeFragment(jumpUrl);
                        mOnEventCallBack.onCommentClick(true, "日记详情", commentFragment);
                    }
                });

            } else {
                mSkuDiaryVisorgone.setVisibility(View.GONE);
            }
        } else {
            mSkuDiaryVisorgone.setVisibility(View.GONE);
        }


    }

    /**
     * 有问题问大家
     *
     * @param taoAskBean
     */
    private void SetAskallStyle(TaoDetailBean.TaoAskBean taoAskBean) {
        List<TaoDetailBean.TaoAskBean.DataBeanX> data = taoAskBean.getData();
        if (null != data && data.size() > 0) {
            mSkuCommentAskallVisorgone.setVisibility(View.VISIBLE);
            mSkuGoaskVisorgone.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
            mSkuCommentAskTitle.setText(taoAskBean.getTitle());
            mSkuCommentAskCheck.setText(taoAskBean.getLinkdata().getUrltitle());
            for (int i = 0; i < data.size(); i++) {
                AskallItemView askallItemView = new AskallItemView(mContext);
                askallItemView.setConent(data.get(i).getQuestion(), data.get(i).getAnswer_num());
                mSkuAskallContainer.addView(askallItemView);
            }
        } else {
            mSkuCommentAskallVisorgone.setVisibility(View.GONE);
            mSkuGoaskVisorgone.setVisibility(View.VISIBLE);
        }

    }


    //tablayout对应标签的切换
    private void setScrollPos(int newPos) {
        Log.e(TAG, "lastPos === " + lastPos);
        Log.e(TAG, "newPos === " + newPos);
        if (lastPos != newPos) {
            //该方法不会触发tablayout 的onTabSelected 监听
            mTabLayout.setScrollPosition(newPos, 0, true);
            if (isDacuBackground) {
                if (mTabLayout == null) {
                    return;
                }
//                mTabLayout.getTabAt(newPos).select();
            }
        }
        lastPos = newPos;
    }

    public void refreshData() {
        if (null != mSkuTagLlt) {
            mSkuTagLlt.removeAllViews();
        }
        mSkuRePayContainer.removeAllViews();
        mSkuCuxiaoContainer.removeAllViews();
        mSkuCouponsContainer.removeAllViews();
        mSkuHoscouponContainer.removeAllViews();
        initSkuData(mSkuId, false);
    }


    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initDetailWebview() {
        initWebview(DetailWebview);
        initWebview(mEnvironmentWebwiew);
        initWebview(LiuchengWebview);
        initWebview(kePuWebview);
        urlTaoMap.put("id", mSkuId);
        LodUrl(DetailWebview, FinalConstant.TAO_DETAIL, urlTaoMap);

        LodUrl(LiuchengWebview, FinalConstant.YUYUE_LIUCHENG, urlTaoMap);

        LodUrl(mEnvironmentWebwiew, FinalConstant.YUYUE_ENVIRONMENT, urlTaoMap);

    }


    private void initWebview(WebView webView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.getSettings().setUseWideViewPort(true);    //设置webview推荐使用的窗口，使html界面自适应屏幕
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setLoadWithOverviewMode(true);  //设置webview加载的页面的模式
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true); // 允许访问文件
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(baseWebViewClientMessage);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    }

    @OnClick({R.id.sku_plus_vibility, R.id.sku_click_plus_kaitong, R.id.sku_dacuceo_click,
            R.id.sku_click_coupons, R.id.sku_click_hoscoupons, R.id.sku_select_project_click,
            R.id.sku_project_explain_click, R.id.sku_comment_koubei_click,
            R.id.sku_comment_askall_visorgone, R.id.sku_commetn_goask, R.id.hos_detaile_togao_ly,
            R.id.sku_hos_chat, R.id.sku_hos_phone, R.id.doc_zhuye_rly, R.id.sku_diary_click,
            R.id.sku_repay_click, R.id.sku_ask_click, R.id.sku_baike_click, R.id.coupons_price_after_click,
            R.id.coupons_price_after_tip, R.id.sku_top_vibility, R.id.sku_doc_chat, R.id.sku_bargain_visorgone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sku_plus_vibility:
                if (Utils.isLoginAndBind(mContext)) {
                    Intent intent = new Intent(mContext, PlusVipActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.sku_click_plus_kaitong:
                if (Utils.isLoginAndBind(mContext)) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_MEMBER, "0"), map, new ActivityTypeData("2"));
                    Intent intent = new Intent(mContext, OpeningMemberActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.sku_dacuceo_click:
                String bigPromotionUrl = proInfo.getBigPromotionUrl();
                if (!TextUtils.isEmpty(bigPromotionUrl)) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_ZT, "0"), map, new ActivityTypeData("2"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(bigPromotionUrl, "0", "0");
                }
                break;
            case R.id.sku_click_coupons:
                HashMap<String, String> map = new HashMap<>();
                map.put("id", mSkuId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_NEWCOUPONS, "0"), map, new ActivityTypeData("2"));
                for (int i = 0; i < mTaoDetailBean.getCoupons().size(); i++) {
                    String title = mTaoDetailBean.getCoupons().get(i).getTitle();
                    if ("订金满减：".equals(title) || "新手礼包：".equals(title)) {
                        baseWebViewClientMessage.showWebDetail(mTaoDetailBean.getCoupons().get(i).getUrl());
                    }
                }

                break;
            case R.id.sku_click_hoscoupons:
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        HashMap<String, String> map1 = new HashMap<>();
                        map1.put("id", mSkuId);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_HOSCOUPONS, "0"), map1, new ActivityTypeData("2"));
                        for (int i = 0; i < mTaoDetailBean.getCoupons().size(); i++) {
                            if ("医院红包：".equals(mTaoDetailBean.getCoupons().get(i).getTitle())) {
                                baseWebViewClientMessage.showWebDetail(mTaoDetailBean.getCoupons().get(i).getUrl());
                            }
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    Intent it5 = new Intent();
                    it5.setClass(mContext, LoginActivity605.class);
                    startActivity(it5);
                }
                break;
            case R.id.sku_select_project_click: //规格
                if (Utils.isFastDoubleClick()) return;
                TaoDetailBean.RelTaoBeanX rel_tao = mTaoDetailBean.getRel_tao();
                if (rel_tao != null) {
                    HashMap<String, String> map1 = new HashMap<>();
                    map1.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_REL, "0"), map1, new ActivityTypeData("2"));
                    WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                    lp.alpha = 0.4f;
                    getActivity().getWindow().setAttributes(lp);
                    SkuSpecPopwindow chatJumpPopwindow = new SkuSpecPopwindow(getActivity(), rel_tao);
                    chatJumpPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                            lp.alpha = 1f;
                            getActivity().getWindow().setAttributes(lp);
                        }
                    });
                    chatJumpPopwindow.setOnTagClickListener(new SkuSpecPopwindow.OnTagClickListener() {
                        @Override
                        public void onTagClickListener(String tao_id) {
                            mSkuTagLlt.removeAllViews();
                            mSkuRePayContainer.removeAllViews();
                            mSkuCuxiaoContainer.removeAllViews();
                            mSkuCouponsContainer.removeAllViews();
                            mSkuHoscouponContainer.removeAllViews();
                            initSkuData(tao_id, false);
                        }
                    });
                    chatJumpPopwindow.showAtLocation(skuContainer, Gravity.BOTTOM, 0, 0);
                }


                break;
            case R.id.sku_project_explain_click://项目说明
                if (Utils.isFastDoubleClick()) return;
                HashMap<String, String> map1 = new HashMap<>();
                map1.put("id", mSkuId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_EXPLAIN, "0"), map1, new ActivityTypeData("2"));
                WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                lp.alpha = 0.4f;
                getActivity().getWindow().setAttributes(lp);
                SkuProjectDetailPop skuProjectDetailPop = new SkuProjectDetailPop(mContext, mTaoDetailBean);
                skuProjectDetailPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                        lp.alpha = 1f;
                        getActivity().getWindow().setAttributes(lp);
                    }
                });
                skuProjectDetailPop.showAtLocation(skuContainer, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.sku_baike_click: //百科点击
                if (null != mTabLayout) {
                    HashMap<String, String> mapbaike = new HashMap<>();
                    mapbaike.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_CLICK_BAIKE, "0"), mapbaike, new ActivityTypeData("2"));
                    mTabLayout.getTabAt(4).select();
                }
                break;
            case R.id.sku_comment_koubei_click://口碑评论点击事件
                if (Utils.isFastDoubleClick()) return;
                AppRaisalBean appraisal = mTaoDetailBean.getAppraisal();
                HashMap<String, String> appraisalEventParams = appraisal.getEvent_params();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "text", "0"), appraisalEventParams, new ActivityTypeData("2"));
                CommentFragment commentFragment = changeFragment(appraisal.getJumpUrl());
                mOnEventCallBack.onCommentClick(true, "评论详情", commentFragment);

                break;
            case R.id.sku_diary_click://相关日记
                if (Utils.isFastDoubleClick()) return;
                DiaryListBean diaryList = mTaoDetailBean.getDiaryList();
                HashMap<String, String> diaryListEventParams = diaryList.getEvent_params();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_TO_DIARYLIST, "img", "0"), diaryListEventParams, new ActivityTypeData("2"));
                CommentFragment commentFragment1 = changeFragment(diaryList.getJumpUrl());
                mOnEventCallBack.onCommentClick(true, "日记详情", commentFragment1);
                break;
            case R.id.sku_comment_askall_visorgone://有问题问大家
                if (Utils.isLogin()) {
                    HashMap<String, String> mapbaike = new HashMap<>();
                    mapbaike.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_ASK, "0"), mapbaike, new ActivityTypeData("2"));
                    pageJumpManager.jumpToLookProblemsActivity(mSkuId, "问大家");
                } else {
                    jumpLogin();
                }
                break;
            case R.id.sku_commetn_goask:
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        if (mTaoDetailBean != null) {
                            pageJumpManager.jumpToQuestionsActivity(mSkuId, mTaoDetailBean.getPic().get(0).getImg(), mTaoDetailBean.getBasedata().getTitle(), mTaoDetailBean.getBasedata().getPrice_discount(), mTaoDetailBean.getBasedata().getPrice());
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    jumpLogin();
                }
                break;
            case R.id.hos_detaile_togao_ly: //医院卡片点击
                ClickData hosClickData = mHos_doc.getHospitalClickData();
                if (hosClickData != null) {
                    HashMap<String, String> hosEventParams = hosClickData.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData("0", "top"), hosEventParams);
                    String hospital_id = mTaoDetailBean.getHos_doc().getHospital_id();
                    Intent it = new Intent(mContext, HosDetailActivity.class);
                    it.putExtra("tao_id", mSkuId);
                    it.putExtra("hosid", hospital_id);
                    startActivity(it);
                    if (Utils.isLogin()) {
                        //1:用户从SKU详情页跳转到医院主页
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("hos_id", hospital_id);
                        hashMap.put("pos", "1");
                        new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData s) {
                                if ("1".equals(s.code)) {
                                    Log.e(TAG, "AutoSendApi ==" + s.message);
                                }
                            }
                        });
                        HashMap<String, Object> hashMap2 = new HashMap<>();
                        hashMap2.put("obj_type", "1");
                        hashMap2.put("obj_id", hospital_id);
                        hashMap2.put("hos_id", hospital_id);
                        new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData s) {
                                if ("1".equals(s.code)) {
                                    Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                }
                            }
                        });
                    }
                }
                break;
            case R.id.sku_hos_phone://电话咨询
                ClickData telClickData = mHos_doc.getTelClickData();
                if (null != telClickData) {
                    HashMap<String, String> telEventParams = telClickData.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData("0", "top"), telEventParams);
                    String phone = mHos_doc.getPhone();
                    if (!TextUtils.isEmpty(phone)) {//打电话
                        //版本判断
                        if (Build.VERSION.SDK_INT >= 23) {

                            Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
                                @Override
                                public void onGranted() {        //判断权限是否开启
                                    phoneCall();
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    mFunctionManager.showShort("没有电话权限");
                                }
                            });
                        } else {
                            phoneCall();
                        }
                    } else {
                        mFunctionManager.showShort("该机构没有开通电话");
                    }
                }

                break;
            case R.id.sku_hos_chat://在线咨询
                if (Utils.noLoginChat()) {
                    onLineChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        onLineChat();
                    }
                }
                break;
            case R.id.doc_zhuye_rly:
                String doc_user_id = mHos_doc.getDoc_user_id();
                String doc_name = mHos_doc.getDoc_name();
                if (!"0".equals(doc_user_id)) {
                    ClickData chatClickData = mHos_doc.getDoctorClickData();
                    HashMap<String, String> event_params = chatClickData.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(event_params);
                    Intent it1 = new Intent();
                    Log.e(TAG, "hhhh");
                    it1.setClass(mContext, DoctorDetailsActivity592.class);
                    it1.putExtra("docId", doc_user_id);
                    it1.putExtra("docName", doc_name);
                    it1.putExtra("partId", "");
                    it1.putExtra("isAutoSend", "2");
                    startActivity(it1);

                }
                break;
            case R.id.sku_repay_click://分期点击
                HashMap<String, String> mapfen = new HashMap<>();
                mapfen.put("id", mSkuId);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_LOOK_REPAYMENT, "0"), mapfen, new ActivityTypeData("2"));
                mOnEventCallBack.fenqiClick();
                break;
            case R.id.sku_ask_click://低价提示点击
                if (Utils.isFastDoubleClick()) return;
                WindowManager.LayoutParams lp1 = getActivity().getWindow().getAttributes();
                lp1.alpha = 0.3f;
                getActivity().getWindow().setAttributes(lp1);
                YanzhibiPopwindow yanzhibiPopwindow = new YanzhibiPopwindow(mContext);
                yanzhibiPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                        lp.alpha = 1f;
                        getActivity().getWindow().setAttributes(lp);
                    }
                });
                yanzhibiPopwindow.showAtLocation(skuContainer, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.coupons_price_after_click:
                Coupons coupons = mTaoDetailBean.getCouponPriceInfo().getCoupons();
                if (null != coupons) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(coupons.getUrl());
                    HashMap<String, String> eventParams = coupons.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(eventParams);
                }
                break;
            case R.id.coupons_price_after_tip:
                if (Utils.isFastDoubleClick()) return;
                String coupon_explain = mTaoDetailBean.getCouponPriceInfo().getCoupon_explain();
                WindowManager.LayoutParams layoutParams = getActivity().getWindow().getAttributes();
                layoutParams.alpha = 0.4f;
                getActivity().getWindow().setAttributes(layoutParams);
                CouponsPriceAfterPop couponsPriceAfterPop = new CouponsPriceAfterPop(mContext, coupon_explain);
                couponsPriceAfterPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        WindowManager.LayoutParams lp = getActivity().getWindow().getAttributes();
                        lp.alpha = 1f;
                        getActivity().getWindow().setAttributes(lp);
                    }
                });
                couponsPriceAfterPop.showAtLocation(skuContainer, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.sku_top_vibility://top榜单
                if (Utils.isFastDoubleClick()) return;
                HospitalTop hospitalTop = mTaoDetailBean.getHospital_top();
                if (hospitalTop != null) {
                    HashMap<String, String> eventParams = hospitalTop.getEvent_params();
                    eventParams.put("id", mSkuId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_HOSPITAL_TOP_CLICK), eventParams, new ActivityTypeData("2"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(hospitalTop.getLink());
                }

                break;
            case R.id.sku_doc_chat:
                if (Utils.noLoginChat()) {
                    onLineChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        onLineChat();
                    }
                }
                break;
            case R.id.sku_bargain_visorgone://砍价
                String bargainUrl = mTaoDetailBean.getBargain_url();
                WebViewUrlLoading.getInstance().showWebDetail(mContext, bargainUrl);

                break;

        }
    }

    private void onLineChat() {
        if (mTaoDetailBean != null) {
            ChatClickData chatClickData = mHos_doc.getChatClickData();
            String is_rongyun = chatClickData.getIs_rongyun();
            String hos_userid = chatClickData.getHos_userid();
            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(chatClickData.getObj_id())
                        .setObjType(chatClickData.getObj_type() + "")
                        .setTitle(chatClickData.getGuiji().getTitle())
                        .setPrice(chatClickData.getGuiji().getPrice())
                        .setImg(chatClickData.getGuiji().getImage())
                        .setMemberPrice(chatClickData.getGuiji().getMember_price())
                        .setYmClass(chatClickData.getYmaq_class())
                        .setYmId(chatClickData.getYmaq_id())
                        .setSkuId(mSkuId)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                tongjiClick("1");
                HashMap<String, String> event_params = chatClickData.getEvent_params();
                YmStatistics.getInstance().tongjiApp(event_params);
            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }


    public interface OnEventCallBack {
        void onLoadDataCallBck(TaoDetailBean detailBean);                               //加载数据完成后接口回调

        void onCountDwonViewCallBack(CountDownView countDownView);                      //收藏接口回调

        void onDacuCountDownCallBack(DacuCountDownView dacuCountDownView);

        void onScrollChanged(int t, int height);            //滚动回调

        void onTabClick(int pos);

        void onCommentClick(boolean b, String title, CommentFragment commentFragment);

        void fenqiClick();


    }

    private OnEventCallBack mOnEventCallBack;

    public void setOnEventCallBack(OnEventCallBack onEventCallBack) {
        mOnEventCallBack = onEventCallBack;
    }

    private final int LOGIN_REFRESH = 999;

    /**
     * 跳转到登录页面
     */
    public void jumpLogin() {
        Intent intent = new Intent(mContext, LoginActivity605.class);
        intent.putExtra("skujump", "1");
        startActivityForResult(intent, LOGIN_REFRESH);
    }


    /**
     * 打电话
     */
    @SuppressLint("MissingPermission")
    private void phoneCall() {

        if (TextUtils.isEmpty(phone400)) {
            Toast.makeText(mContext, "数据获取错误", Toast.LENGTH_SHORT).show();
            return;
        }
        tongjiClick("2");
        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        int minute = t.minute;
        Log.e(TAG, "hour === " + hour);

        if (hour > 9 && hour < 22) {

            Log.e(TAG, "phone400 === " + phone400);
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else if (hour == 9 && minute >= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else if (hour == 21 && minute <= 30) {
            ViewInject.toast("正在拨打中·····");
            if ("".equals(mPhoneFen)) {
                Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone400));
                startActivity(it);
            } else {
                Intent it1 = new Intent(Intent.ACTION_CALL);
                it1.setData(Uri.fromParts("tel", phone400 + ",," + mPhoneFen, null));//拼一个电话的Uri，拨打分机号 关键代码
                it1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it1);
            }
        } else {
            showDialogExitEdit();
        }
    }

    void tongjiClick(String type) {

        String uid = Utils.getUid();
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mSkuId);
        maps.put("uid", uid);
        maps.put("type", type);
        new TongjiClickApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

            }
        });

    }

    Button cancelBt;
    Button trueBt11;
    TextView titleTv1;

    void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        titleTv1 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv1.setText("电话咨询服务时间为9:30至21:30。此时请留言，小悦会尽快帮您解决的哦！");

        cancelBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
            }
        });

        trueBt11 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt11.setText("去留言");
        trueBt11.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
                if (Utils.noLoginChat()) {
                    dialogChat();
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        dialogChat();
                    }
                }
            }
        });
    }

    private void dialogChat() {
        if (mTaoDetailBean != null) {
            TaoDetailBean.ChatDataBean chatData = mTaoDetailBean.getChatData();
            String hos_userid = chatData.getHos_userid();
            String is_rongyun = chatData.getIs_rongyun();
            TaoDetailBean.HosDocBean hos_doc = mTaoDetailBean.getHos_doc();

            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(mSkuId)
                        .setObjType("2")
                        .setTitle(mTaoDetailBean.getBasedata().getTitle())
                        .setPrice(mTaoDetailBean.getBasedata().getPrice_discount())
                        .setImg(mTaoDetailBean.getPic().get(0).getImg())
                        .setMemberPrice(mTaoDetailBean.getMember_data().getMember_price())
                        .setYmClass("2")
                        .setYmId(mSkuId)
                        .setSkuId(mSkuId)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                        .setEvent_name("chat_hospital")
                        .setEvent_pos("tao")
                        .setHos_id(hos_doc.getHospital_id())
                        .setDoc_id(hos_doc.getDoc_user_id())
                        .setTao_id(mSkuId)
                        .setEvent_others(hos_doc.getHospital_id())
                        .setId(mSkuId)
                        .setReferrer("17")
                        .setType("2")
                        .build();
                Utils.chatTongJi(mContext, tongJiParams);


            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }


    /**
     * 切换fragment
     *
     * @param url
     */
    private CommentFragment changeFragment(String url) {
        CommentFragment commentFragment = new CommentFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        commentFragment.setArguments(bundle);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.sku_container, commentFragment);
        transaction.addToBackStack("CommentFragment");
        transaction.commit();
        mTabLayout.setVisibility(View.GONE);
        return commentFragment;
    }

    /**
     * 弹出动画
     *
     * @param view
     * @param startWidth
     * @param endWidth
     */
    public void changeViewWidthAnimatorStart(final View view, final int startWidth, final int endWidth, final int scrollState) {

        if (view != null && startWidth >= 0 && endWidth >= 0) {

            ValueAnimator animator = ValueAnimator.ofInt(startWidth, endWidth);
            animator.setInterpolator(new OvershootInterpolator(5f));
            animator.setDuration(300);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override

                public void onAnimationUpdate(ValueAnimator animation) {
                    ViewGroup.LayoutParams params = view.getLayoutParams();
                    params.width = (int) animation.getAnimatedValue();
                    view.setLayoutParams(params);
                    if (scrollState == SCROLL_STATE_IDLE) { //展开
                        mBottomExpandTxt.setVisibility(View.VISIBLE);
                        view.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_expand));
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                        marginLayoutParams.rightMargin = Utils.dip2px(15);
                        view.setLayoutParams(marginLayoutParams);
                    } else { //收缩
                        mBottomExpandTxt.setVisibility(View.GONE);
                        view.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_bottom_btn_common));
                        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                        marginLayoutParams.rightMargin = Utils.dip2px(0);
                        view.setLayoutParams(marginLayoutParams);
                    }

                }

            });

            animator.start();

        }

    }

}
