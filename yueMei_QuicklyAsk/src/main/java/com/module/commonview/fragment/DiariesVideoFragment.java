package com.module.commonview.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.commonview.module.bean.PhotoBrowsListPic;
import com.module.community.controller.activity.VideoListActivity;
import com.quicklyask.activity.R;
import com.quicklyask.view.YueMeiVideoView;

import butterknife.BindView;

public class DiariesVideoFragment extends YMBaseFragment {
    @BindView(R.id.yuemeivideoview)
    YueMeiVideoView mYuemeivideoview;
    private PhotoBrowsListPic mData;
    private boolean flag = true;
    private TextView mTextContent;
    private String mDiaryid;
    private int currentPosition;
    private final int VIDEO_CONTROL = 123;

    public static DiariesVideoFragment newInstance(PhotoBrowsListPic imageUrls, String diaryId) {
        DiariesVideoFragment fragment = new DiariesVideoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", imageUrls);
        bundle.putString("diaryid", diaryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.diaries_video_fragment;
    }

    @Override
    protected void initView(View view) {
        mData = getArguments().getParcelable("data");
        mDiaryid = getArguments().getString("diaryid");
    }

    @Override
    protected void initData(View view) {
        mTextContent = getActivity().findViewById(R.id.photo_browsing_after_content);
        mYuemeivideoview.setVideoParameter(mData.getImg(), mData.getVideo_url());
        mYuemeivideoview.videoStartPlayer();
        mYuemeivideoview.setOnMaxVideoClickListener(new YueMeiVideoView.OnMaxVideoClickListener() {
            @Override
            public void onMaxVideoClick(View v) {
                Intent intent = new Intent(mContext, VideoListActivity.class);
                intent.putExtra("progress", mYuemeivideoview.getCurrentPosition());
                intent.putExtra("id", mDiaryid);
                intent.putExtra("flag", "1");
                startActivityForResult(intent, VIDEO_CONTROL);
            }
        });

        mYuemeivideoview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    mTextContent.setEllipsize(null);
                    mTextContent.setSingleLine(false);
                    mTextContent.setMaxLines(5);
                    flag = false;
                } else {
                    mTextContent.setEllipsize(null);
                    mTextContent.setSingleLine(true);
                    mTextContent.setMaxLines(1);
                    flag = true;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case VIDEO_CONTROL:    //视频请求回调
                if (resultCode == VideoListActivity.RETURN_VIDEO_PROGRESS) {
                    currentPosition = data.getIntExtra("current_position", 0);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //是可见，是视频fragment,播放器部位空
        if (mYuemeivideoview != null) {
            if (currentPosition != 0) {
                mYuemeivideoview.videoRestart(currentPosition);
                currentPosition = 0;
            } else {
                mYuemeivideoview.videoRestart();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mYuemeivideoview != null) {
            mYuemeivideoview.videoSuspend();
        }
    }


    @Override
    public void onDestroyView() {
        if (mYuemeivideoview != null) {
            mYuemeivideoview.videoStopPlayback();
        }
        super.onDestroyView();
    }
}
