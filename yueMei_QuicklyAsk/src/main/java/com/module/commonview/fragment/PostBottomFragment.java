package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.BaomingData;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.view.AnswerDoctorDialog;
import com.module.commonview.view.DiaryProductDetailsDialog;
import com.module.commonview.view.TipGuidePowindow;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.PostMsgEvent;
import com.module.my.controller.activity.EnrolPageActivity647;
import com.module.my.controller.activity.TypeProblemActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.module.community.statistical.statistical.FinalEventName.POSTINFO_TO_TAO;

/**
 * 帖子列表吸底
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostBottomFragment extends YMBaseFragment {

    //吸底
    @BindView(R.id.adiary_list_bootton_focus)
    LinearLayout mBoottonFocus;                     //吸底点赞
    @BindView(R.id.adiary_list_bootton_focus_image)
    ImageView mBoottonFocusImage;                     //吸底点赞图
    @BindView(R.id.adiary_list_bootton_focus_title)
    TextView mBoottonFocusTitle;                     //吸底点赞文案

    LinearLayout mBoottonMessage;                       //吸底评论
    TextView mBoottonMessageTitle;                       //吸底评论

    LinearLayout mBoottonCollection;                    //吸底收藏
    ImageView mBoottonCollectionImg;                     //吸底收藏图
    TextView mBoottonCollectionTitle;                     //吸底收藏文案

//    LinearLayout mBottonComment;//底部评论
//    ImageView mBoottonCommentImg;//评论图
//    TextView mBoottonCommentTitle;//评论数

    private String mDiaryId;
    private List<DiaryTaoData> taoData = new ArrayList<>();
    private String mIsCollect;
    public int mAgreeNum;
    private int mCollectNum;
    public int mAnswerNum;
    private PostListData postListData;
    private DiaryListData diaryListData;

    private DiaryProductDetailsDialog diaryButtomDialog;        //底部弹出的按钮
    private AnswerDoctorDialog answerDoctorDialog;              //医生弹窗
    private String moban;
    private String vote;
    private final int ENROL_CODE = 100;
    private String mUserId;
    private String TAG = "PostBottomFragment";
    private String p_id;
    private String id;
    private BaomingData baoming;            //报名数据
    private String voteIsEnd;               //投票结束
    private final String WRITE_PAGE_TIP = "write_page_tip";     //再写一页提示key
    private TipGuidePowindow tipGuidePowindow;
    private HashMap<String, String> baomingClickData;
    private String selfPageType;
    public String isAgree;                 //是否点赞
    private ChatDataBean mChatData;
    private String hospitalId;
    private String mAskorshare;
    private String loadZtUrl;               //问答帖跳专题链接

    private class myHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (tipGuidePowindow != null) {
                        tipGuidePowindow.dismiss();
                    }
                    break;
            }
        }
    }


    public static PostBottomFragment newInstance(DiaryListData data) {
        PostBottomFragment fragment = new PostBottomFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);

        return fragment;
    }

    public static PostBottomFragment newInstance(PostListData data) {
        PostBottomFragment fragment = new PostBottomFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(PostMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 0:
                //评论减1
                mAnswerNum = mAnswerNum - (int)msgEvent.getData();
                if (mAnswerNum > 0) {
                    mBoottonMessageTitle.setText(mAnswerNum + "");
                } else {
                    mBoottonMessageTitle.setText("评论");
                }
                break;
            case 1:
                //评论加1
                mAnswerNum = mAnswerNum + 1;
                if (mAnswerNum > 0) {
                    mBoottonMessageTitle.setText(mAnswerNum + "");
                } else {
                    mBoottonMessageTitle.setText("评论");
                }
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (isDiaries()) {
            diaryListData = getArguments().getParcelable("data");
            mDiaryId = diaryListData.getId();
            taoData = diaryListData.getTaoData();
            mIsCollect = diaryListData.getIs_collect();
            mCollectNum = Integer.parseInt(diaryListData.getCollect_num());
            Log.e(TAG, "mData.getAnswer_num() === " + diaryListData.getAnswer_num());
            Log.e(TAG, "===========" + diaryListData.getAnswer_num());
            mAnswerNum = Integer.parseInt(diaryListData.getAnswer_num());

            mUserId = diaryListData.getUserdata().getId();

            p_id = diaryListData.getP_id();

            selfPageType = diaryListData.getSelfPageType();

            isAgree = diaryListData.getIs_agree();

            mChatData = diaryListData.getBottomChatData();

            mAskorshare = diaryListData.getAskorshare();

            id = diaryListData.getId();

            DiaryHosDocBean hosDoc = diaryListData.getHosDoc();
            if (hosDoc != null) {
                hospitalId = hosDoc.getHospital_id();
            }
        } else {
            postListData = getArguments().getParcelable("data");
            mDiaryId = postListData.getId();
            mIsCollect = postListData.getIs_collect();
            mAgreeNum = Integer.parseInt(postListData.getAgree_num());
            mCollectNum = Integer.parseInt(postListData.getCollect_num());
            mAnswerNum = Integer.parseInt(postListData.getAnswer_num());

            mUserId = postListData.getUserdata().getId();

            p_id = postListData.getP_id();

            id = postListData.getId();

            moban = postListData.getMoban();
            vote = postListData.getVote();
            baoming = postListData.getBaoming();
            voteIsEnd = postListData.getVote_is_end();

            baomingClickData = postListData.getBaomingClickData();
            selfPageType = postListData.getSelfPageType();
            isAgree = postListData.getIs_agree();

            mAskorshare = postListData.getAskorshare();
            taoData = postListData.getTaoDataList();
            if (taoData.size() == 0) {
                taoData = postListData.getContentSkuList();
            }

            loadZtUrl = postListData.getLoadZtUrl();

            DiaryHosDocBean hosDoc = postListData.getHosDoc();
            if (hosDoc != null) {
                hospitalId = hosDoc.getHospital_id();
            }
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        if ("0".equals(mAskorshare)) {
            //问答类型
            return R.layout.activity_adiary_list_ask_bottom;
        } else {
            return R.layout.activity_adiary_list_bottom;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView(View view) {
        //设置弹层
        if (taoData.size() > 1) {
            diaryButtomDialog = new DiaryProductDetailsDialog(mContext, taoData, mDiaryId, "3", selfPageType, id);       //初始化底部弹出框
            //底部弹出框点击回调
            diaryButtomDialog.setOnItemCallBackListener(new DiaryProductDetailsDialog.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    //帖子底部文中提及列表sku点击
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", id);
                    hashMap.put("to_page_type", "2");
                    hashMap.put("to_page_id", taoData.get(pos).getId());
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TEXT_RELATED_TO_TAO, pos + 1 + ""), hashMap, new ActivityTypeData(selfPageType));

                    GoodsPurchased(pos);
                }
            });
        }


        //设置点赞数
        setAgree();

        if ("0".equals(mAskorshare)) {
            //问答类型
            mBoottonCollection = view.findViewById(R.id.adiary_list_bootton_collection);
            mBoottonCollectionImg = view.findViewById(R.id.adiary_list_bootton_collection_img);
            mBoottonCollectionTitle = view.findViewById(R.id.adiary_list_bootton_collection_title);
            mBoottonMessage = view.findViewById(R.id.adiary_list_bootton_message);

//            Log.e(TAG, "loadZtUrl == " + loadZtUrl);
//            if (!TextUtils.isEmpty(loadZtUrl)) {
            if (postListData.getRecommend_doctor_list() != null && postListData.getRecommend_doctor_list().size() > 0) {
                answerDoctorDialog = new AnswerDoctorDialog(mContext, taoData, mDiaryId, selfPageType, postListData);

                mBoottonMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        if (postListData.getRecommend_doctor_list().size() > 0) {
                            if (answerDoctorDialog != null) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", id);
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POST_BOTTOM_THE_TEXT), hashMap, new ActivityTypeData(selfPageType));
                                answerDoctorDialog.show();
                            }
                        } else {
                            if (Utils.isLoginAndBind(mContext)) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "bbs"));
                                mFunctionManager.goToActivity(TypeProblemActivity.class);
                            }
                        }
//                        Log.e(TAG, "loadZtUrl == " + loadZtUrl);
//                        if (!TextUtils.isEmpty(loadZtUrl)) {
//                            WebUrlTypeUtil.getInstance(mContext).urlToApp(loadZtUrl);
//                        }

                    }
                });
            } else {
                mBoottonMessage.setVisibility(View.GONE);
            }
            //收藏数
            setBottonUI();
        } else {
            FrameLayout mBoottonOtherContainer = view.findViewById(R.id.adiary_list_bootton_other_container);

            mBoottonMessage = view.findViewById(R.id.adiary_list_bootton_message);
            mBoottonMessageTitle = view.findViewById(R.id.adiary_list_bootton_message_title);

            if (isDiaries()) {

                //查看他人的日记
                View otherView = View.inflate(mContext, R.layout.activity_adiary_list_bottom_other2, mBoottonOtherContainer);
                LinearLayout mBoottonSkuClick = otherView.findViewById(R.id.adiary_list_bootton_sku);
                TextView mBoottonSkuTitle = otherView.findViewById(R.id.adiary_list_bootton_sku_title);

                RelativeLayout mBoottonChatClick = otherView.findViewById(R.id.adiary_list_bootton_chat_click);
                TextView mBoottonChatTitle = otherView.findViewById(R.id.adiary_list_bootton_chat);

                if (isSeeOneself()) {
                    //查看自己的日记，收藏占位隐藏
                    mBoottonSkuClick.setVisibility(View.INVISIBLE);
                    mBoottonChatTitle.setText("再写一页");

                    if (mFunctionManager.loadInt(WRITE_PAGE_TIP, 1) == 0) {
                        tipGuidePowindow = new TipGuidePowindow(mContext);
                        tipGuidePowindow.setContent("日记更新挪到这里啦，快来更新吧", 14, Utils.getLocalColor(mContext, R.color.white), Utils.dip2px(150)).setImageBackground(350, TipGuidePowindow.HOME_BUBBLE_CONTENT).showPopupWindow(mBoottonChatClick, -200, -320);

                        mFunctionManager.saveInt(WRITE_PAGE_TIP, 1);

                        new myHandler().sendEmptyMessageDelayed(1, 3000);
                    }

                    //再写一页按钮点击
                    mBoottonChatClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (isDiaryOrDetails()) {       //看的是日记本还是日记
                                if (onEventClickListener != null) {
                                    onEventClickListener.onWritePageClick(v, mDiaryId);
                                }
                            } else {
                                if (onEventClickListener != null) {
                                    onEventClickListener.onWritePageClick(v, p_id);
                                }
                            }
                        }
                    });

                } else {
                    //设置图片
                    Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.post_compar_doc);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    mBoottonChatTitle.setCompoundDrawables(drawable, null, null, null);
                    mBoottonChatTitle.setCompoundDrawablePadding(Utils.dip2px(5));
                    mBoottonChatTitle.setBackgroundResource(R.drawable.shape_diary_list_top_background);

                    //设置商品详情点击
                    if (taoData.size() > 0) {
                        setSkuClick(mBoottonSkuClick, mBoottonSkuTitle, "商品");
                    } else {
                        mBoottonSkuClick.setVisibility(View.INVISIBLE);
                    }

                    //私信是否存在
                    if (!TextUtils.isEmpty(hospitalId)) {
                        mBoottonChatClick.setVisibility(View.VISIBLE);
                    } else {
                        mBoottonChatClick.setVisibility(View.GONE);
                    }
                    //点击咨询
                    mBoottonChatClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mChatData != null) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL, "diary_chat_bottom"), mChatData.getEvent_params());
                                directMessages();
                            }
                        }
                    });
                }
            } else {

                View otherView = View.inflate(mContext, R.layout.activity_adiary_list_bottom_other1, mBoottonOtherContainer);

                mBoottonCollection = otherView.findViewById(R.id.adiary_list_bootton_collection);
                mBoottonCollectionImg = otherView.findViewById(R.id.adiary_list_bootton_collection_img);
                mBoottonCollectionTitle = otherView.findViewById(R.id.adiary_list_bootton_collection_title);


//                 mBottonComment = otherView.findViewById(R.id.adiary_list_bootton_message);
//                 mBoottonCommentImg = otherView.findViewById(R.id.adiary_list_bottom_comment_img);
//                 mBoottonCommentTitle = otherView.findViewById(R.id.adiary_list_bootton_message_title);


                LinearLayout mBoottondetailsClick = otherView.findViewById(R.id.adiary_list_bootton_details_click);
                TextView mBoottondetails = view.findViewById(R.id.adiary_list_bootton_details);

                if (moban != null && !"0".equals(moban)) {           //报名贴存在
                    Log.e(TAG, "baoming.getShow_titlel() == " + baoming.getShow_title());
                    mBoottondetails.setText(baoming.getShow_title());
                    if ("1".equals(baoming.getBaoming_is_end())) {
                        mBoottondetails.setTextColor(Utils.getLocalColor(mContext, R.color._99));
                    } else {
                        mBoottondetails.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));

                        //报名帖点击报名
                        mBoottondetailsClick.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isLoginAndBind(mContext)) {
                                    //报名贴统计
                                    if (baomingClickData != null) {
                                        YmStatistics.getInstance().tongjiApp(baomingClickData);
                                    }
                                    Intent it3 = new Intent(mContext, EnrolPageActivity647.class);
                                    it3.putExtra("cateid", mDiaryId);
                                    it3.putExtra("moban", moban);        //模版这里要换成动态的
                                    startActivityForResult(it3, ENROL_CODE);
                                }
                            }
                        });
                    }
                } else if (!TextUtils.isEmpty(vote) && "1".equals(vote)) {           //投票贴存在
                    if ("1".equals(voteIsEnd)) {
                        mBoottondetails.setTextColor(Utils.getLocalColor(mContext, R.color._99));
                        mBoottondetails.setText("投票已结束");
                    } else {
                        mBoottondetails.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                        mBoottondetails.setText("去投票");

                        //投票贴点击
                        mBoottondetailsClick.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(FinalConstant.FORUM_VOTE + "id/" + mDiaryId + "/");
                            }
                        });
                    }
                } else if (taoData.size() > 0) {
                    setSkuClick(mBoottondetailsClick, mBoottondetails, "文中提及");
                } else {
                    setWeight(mBoottonOtherContainer, mBoottondetailsClick);
                }

                //收藏数
                setBottonUI();
            }
            //评论数
            setComments();

        }

    }

    /**
     * 商品详情点击事件
     *
     * @param mSkuClick ：点击控件
     * @param mSkuTitle ：文案控件
     * @param text      ：文案内容
     */
    @SuppressLint("SetTextI18n")
    private void setSkuClick(LinearLayout mSkuClick, TextView mSkuTitle, String text) {
        mSkuClick.setVisibility(View.VISIBLE);
        if (!isDiaries()) {
            mSkuTitle.setPadding(DensityUtil.dip2px(19), DensityUtil.dip2px(7), DensityUtil.dip2px(19), DensityUtil.dip2px(7));
            mSkuTitle.setBackgroundResource(R.drawable.shape_diary_list_top_background);
            mSkuTitle.setTextColor(Color.parseColor("#FFFFFF"));
        }
        if (taoData.size() > 1) {
            mSkuTitle.setText(text + taoData.size());
            mSkuTitle.setText(text);
        } else {
            mSkuTitle.setText(text);
        }

        mSkuClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (taoData.size() > 1) {
                    if (diaryButtomDialog != null) {
                        diaryButtomDialog.show();
                    }
                } else if (taoData.size() > 0) {
                    GoodsPurchased(0);
                }
                //帖子底部文中提及点击统计
                if (!isDiaries()) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", id);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TEXT_RELATED), hashMap, new ActivityTypeData(selfPageType));
                }
            }
        });
    }

    /**
     * 右边显示隐藏
     *
     * @param mBoottonOtherContainer
     * @param mBoottondetailsClick
     */
    private void setWeight(FrameLayout mBoottonOtherContainer, LinearLayout mBoottondetailsClick) {
        mBoottondetailsClick.setVisibility(View.GONE);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mBoottonOtherContainer.getLayoutParams();
        layoutParams.weight = 16;
        mBoottonOtherContainer.setLayoutParams(layoutParams);
    }

    @Override
    protected void initData(View view) {
        initClickEvent();
    }

    /**
     * 初始换点击事件
     */
    private void initClickEvent() {


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ENROL_CODE:
                if (data != null && resultCode == EnrolPageActivity647.ENROL_CODE) {
                    String textMessage = data.getStringExtra("textMessage");
                    showDialogExitEdit(textMessage, "我知道了");
                }
                break;
        }
    }

    /**
     * 私信跳转
     */
    private void directMessages() {
        if (Utils.noLoginChat()) {
            toChatActivity();
        } else {
            if (Utils.isLoginAndBind(mContext)) {
                toChatActivity();
            }
        }

    }

    private void toChatActivity() {
        String is_rongyun = mChatData.getIs_rongyun();
        String hos_userid = mChatData.getHos_userid();
        String ymaq_class = mChatData.getYmaq_class();
        String ymaq_id = mChatData.getYmaq_id();
        String event_name = mChatData.getEvent_name();
        String event_pos = mChatData.getEvent_pos();
        String obj_type = mChatData.getObj_type();
        String obj_id = mChatData.getObj_id();
        ChatDataGuiJi guiji = mChatData.getGuiji();
        String price = guiji.getPrice();
        String image = guiji.getImage();
        String member_price = guiji.getMember_price();
        String url = guiji.getUrl();
        String title = guiji.getTitle();
        HashMap<String, String> event_params = mChatData.getEvent_params();
        if ("3".equals(is_rongyun)) {
            YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                    .setDirectId(hos_userid)
                    .setObjId(obj_id)
                    .setObjType(obj_type)
                    .setYmClass(ymaq_class)
                    .setYmId(ymaq_id)
                    .setPrice(price)
                    .setMemberPrice(member_price)
                    .setImg(image)
                    .setUrl(url)
                    .setTitle(title)
                    .build();
            new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        } else {
            mFunctionManager.showShort("该服务暂未开通在线客服功能");
        }
    }

    /**
     * 设置点赞数
     */
    @SuppressLint("SetTextI18n")
    public void setAgree() {
        if (mAgreeNum > 0) {
            mBoottonFocusTitle.setText(mAgreeNum + "");
        } else {
            mBoottonFocusTitle.setText("赞");
        }

        if ("1".equals(isAgree)) {
            Glide.with(mContext).load(R.drawable.diry_like_bottom).into(mBoottonFocusImage);
        } else {
            Glide.with(mContext).load(R.drawable.diry_like_not_bottom).into(mBoottonFocusImage);
        }

        //吸底点赞的点击
        mBoottonFocus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (Utils.isLoginAndBind(mContext)) {
                    DiariesReportLikeData data = new DiariesReportLikeData();
                    data.setId(mDiaryId);
                    data.setIs_reply("2");
                    data.setFlag("1");
                    onEventClickListener.onLikeClick(data);
                }
            }
        });
    }

    /**
     * 设置评论数
     */
    @SuppressLint("SetTextI18n")
    public void setComments() {
        //吸底评论数
        if (mAnswerNum > 0) {
            mBoottonMessageTitle.setText(mAnswerNum + "");
        } else {
            mBoottonMessageTitle.setText("评论");
        }

        //吸底评论的点击
        mBoottonMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (Utils.isLoginAndBind(mContext)) {
                    onEventClickListener.onMessageClick();
                }
            }
        });

    }


    /**
     * 设置收藏
     */
    private void setBottonUI() {
        //判断是否收藏
        if ("1".equals(mIsCollect)) {
            mFunctionManager.setImgSrc(mBoottonCollectionImg, R.drawable.diary_list_suction_bottom_selected);
        } else {
            mFunctionManager.setImgSrc(mBoottonCollectionImg, R.drawable.diary_list_suction_bottom_collection);
        }

        //收藏数
        if (mCollectNum > 0) {
            mBoottonCollectionTitle.setText(mCollectNum + "");
        } else {
            mBoottonCollectionTitle.setText("收藏");
        }

        //吸底收藏的点击
        mBoottonCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                if (Utils.isLoginAndBind(mContext)) {
                    if ("1".equals(mIsCollect)) {
                        mFunctionManager.setImgSrc(mBoottonCollectionImg, R.drawable.diary_list_suction_bottom_collection);

                        mIsCollect = "0";
                        mCollectNum--;
                        if (mCollectNum <= 0) {
                            mCollectNum = 0;
                            mBoottonCollectionTitle.setText("收藏");
                        } else {
                            mBoottonCollectionTitle.setText(mCollectNum + "");
                        }

                        onEventClickListener.onCollectionClick(false);
                    } else {

                        mFunctionManager.setImgSrc(mBoottonCollectionImg, R.drawable.diary_list_suction_bottom_selected);
                        mIsCollect = "1";
                        mCollectNum++;
                        mBoottonCollectionTitle.setText(mCollectNum + "");

                        onEventClickListener.onCollectionClick(true);
                    }
                }
            }
        });
    }

    /**
     * 判断是日记还是帖子
     *
     * @return true 日记
     */
    private boolean isDiaries() {
        return getArguments().getParcelable("data") instanceof DiaryListData;
    }

    /**
     * 判断是查看的自己日记还是别人的日记
     *
     * @return ：ture:查看自己的，false:查看别人的
     */
    private boolean isSeeOneself() {
        return mUserId.equals(Utils.getUid());
    }

    /**
     * 判断是日记还是日记本
     *
     * @return ：true 日记本
     */
    private boolean isDiaryOrDetails() {
        if (!TextUtils.isEmpty(p_id)) {
            return "0".equals(p_id);
        } else {
            return false;
        }
    }

    /**
     * 商品去购买按钮点击
     */
    private void GoodsPurchased(int pos) {

        String tao_id = taoData.get(pos).getId();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", mDiaryId);
        hashMap.put("to_page_type", "2");
        hashMap.put("to_page_id", tao_id);
        if (isDiaries()) {
            if (isDiaryOrDetails()) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_TO_TAO, "bottom"), hashMap, new ActivityTypeData(selfPageType));
            } else {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHAREINFO_TO_TAO, "bottom"), hashMap, new ActivityTypeData(selfPageType));
            }
        } else {
            YmStatistics.getInstance().tongjiApp(new EventParamData(POSTINFO_TO_TAO, "bottom"), hashMap, new ActivityTypeData(selfPageType));
        }

        Intent intent = new Intent(mContext, TaoDetailActivity.class);
        intent.putExtra("id", tao_id);
        if (mContext instanceof DiariesAndPostsActivity) {
            intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
        } else {
            intent.putExtra("source", "0");
        }
        intent.putExtra("objid", mDiaryId);
        startActivity(intent);
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     */
    private void showDialogExitEdit(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onCollectionClick(boolean b);                      //收藏接口回调

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onMessageClick();                                  //消息点击回调

        void onWritePageClick(View v, String id);              //再写一页接口回调
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
