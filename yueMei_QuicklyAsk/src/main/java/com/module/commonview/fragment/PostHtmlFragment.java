package com.module.commonview.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.refresh.refresh.RefreshListener;
import com.module.base.view.ScrollWebView;
import com.module.base.view.YMBaseWebViewFragment;
import com.module.commonview.activity.CommentsListActivity;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.commonview.view.CommentDialog;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.event.PostMsgEvent;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/4/24
 */
public class PostHtmlFragment extends YMBaseWebViewFragment {

    private PostListData mData;
    private String mDiaryId;

    @BindView(R.id.fragment_diary_html_posts)
    MyPullRefresh webViewContainer;                  //webView容器
    private SumbitPhotoData sumbitPhotoData;
    private DiaryCommentDialogView mDiaryCommentDialogView;
    private String TAG = "PostHtmlFragment";
    private BaseWebViewClientMessage clientManager;
    private Bundle commentsBundle;
    private ParserPagramsForWebUrl parserWebUrl;
    private CommentDialog commentDialog;
    public int mAnswerNum;

    public static PostHtmlFragment newInstance(PostListData data, String diaryId) {
        PostHtmlFragment fragment = new PostHtmlFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", diaryId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        //判断是否有输入框 有的话弹出键盘(选择图片跳页再回来)
        if(mDiaryCommentDialogView != null && mDiaryCommentDialogView.isShowing()){
            mDiaryCommentDialogView.showKeyboard();
        }
        if(commentDialog != null
                && commentDialog.isShowing()
                && commentDialog.getDiaryCommentDialogView() != null
                && commentDialog.getDiaryCommentDialogView().isShowing()){
            commentDialog.getDiaryCommentDialogView().showKeyboard();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(PostMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 0:
                //评论减1
                mAnswerNum = mAnswerNum - (int)msgEvent.getData();
                if(mAnswerNum < 0){
                    mAnswerNum = 0;
                }
                break;
            case 1:
                //评论加1
                mAnswerNum = mAnswerNum + 1;
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_html_posts;
    }

    @Override
    protected void initView(View view) {
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");
        //评论数
        mAnswerNum = Integer.parseInt(mData.getAnswer_num());
        webViewContainer.addView(mWebView);
        webViewContainer.setRefreshListener(new RefreshListener() {
            @Override
            public void onRefresh() {
                notifyWebView();
            }
        });

        String loadHtmlUrl = FinalConstant1.BASE_VER_URL_S + mData.getLoadHtmlUrl();
        Log.e(TAG, "loadHtmlUrl == " + loadHtmlUrl);
        Log.e(TAG, "diaryId == " + mDiaryId);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", mDiaryId);
        loadUrl(loadHtmlUrl, hashMap);

    }

    @Override
    protected void initData(View view) {
        sumbitPhotoData = new SumbitPhotoData();
        sumbitPhotoData.setUserid(mData.getUserdata().getId());
        sumbitPhotoData.setQid(mDiaryId);
        sumbitPhotoData.setAskorshare(mData.getAskorshare());

        commentsBundle = new Bundle();

        mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);

        clientManager = new BaseWebViewClientMessage(mContext);
        parserWebUrl = new ParserPagramsForWebUrl();

        /**
         * 公共跳转外的跳转
         */
        clientManager.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String url) {
                try {
                    showWebDetail(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //webView滚动回调
        mWebView.setOnScrollChangeListener(new ScrollWebView.OnScrollChangeListener() {
            @Override
            public void onPageEnd(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onPageTop(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                if (onEventClickListener != null) {
                    onEventClickListener.onScrollChanged(t);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mContext != null && !mContext.isFinishing()) {
            if (mDiaryCommentDialogView != null) {
                mDiaryCommentDialogView.dismiss();
            }
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("私密", "1");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("私密", "1");
                        }
                    } else {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("公开", "0");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("公开", "0");
                        }
                    }
                }
                break;
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                Log.e(TAG, "mContext.RESULT_OK ==" + Activity.RESULT_OK);
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            commentDialog.getDiaryCommentDialogView().gridviewInit();
                        } else {
                            mDiaryCommentDialogView.setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            mDiaryCommentDialogView.gridviewInit();
                        }
                    }
                }
                break;
        }
    }

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
            if (webViewContainer != null) {
                webViewContainer.finishRefresh();
            }
        }
        super.onYmProgressChanged(view, newProgress);
    }

    /**
     * 调起评论窗口
     */
    public void startComments() {
        if (Utils.isLoginAndBind(mContext)) {
            if (!TextUtils.isEmpty(mAnswerNum + "") && mAnswerNum > 0) {
                commentDialog = new CommentDialog(mContext, mDiaryId, mData.getUserdata().getId(), mData.getAskorshare(), mData.getP_id(), mAnswerNum);
                if (commentDialog != null) {
                    commentDialog.show();
                }
            } else {
                mDiaryCommentDialogView.showDialog();
                mDiaryCommentDialogView.setPicturesChooseGone(false);

                //评论提交完成回调
                mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                    @Override
                    public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                        notifyWebView();
                        if (onEventClickListener != null) {
                            onEventClickListener.onCommentsClick();
                        }
                    }
                });
            }
        }
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        Log.e(TAG, "url === " + url);
        if (url.startsWith("type")) {
            clientManager.showWebDetail(url);
        } else {
            WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
        }
        return true;
    }

    private void showWebDetail(String urlStr) {
        try {
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;

            if ("6703".equals(obj.getString("type"))) {            //查看全部评论
//                commentsBundle.putString("id", mData.getUserdata().getId());
//                commentsBundle.putString("diary_id", mDiaryId);
//                commentsBundle.putString("askorshare", mData.getAskorshare());
//                commentsBundle.putString("p_id", mData.getP_id());
//                commentsBundle.putString("user_id", mData.getUserdata().getId());
//                mFunctionManager.goToActivity(CommentsListActivity.class, commentsBundle);
                commentDialog = new CommentDialog(mContext, mDiaryId, mData.getUserdata().getId(), mData.getAskorshare(), mData.getP_id(), mAnswerNum);
                if (commentDialog != null) {
                    commentDialog.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***
     * 刷新页面
     */
    public void notifyWebView() {
        mWebView.reload();
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onCommentsClick();         //评论发布后接口回调

        void onScrollChanged(int t);                //滚动回调
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
