package com.module.commonview.fragment;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.ListSuckTopRecyclerAdapter;
import com.module.commonview.module.api.DiaryListApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.PostOtherpic;
import com.module.commonview.other.DiaryTopSkuItemView;
import com.module.commonview.view.DiariesAndPostPicView1;
import com.module.commonview.view.DiariesAndPostPicView2;
import com.module.commonview.view.ViewPagerPageIndicator;
import com.module.community.controller.activity.VideoListActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.controller.activity.WriteNoteActivity;
import com.qmuiteam.qmui.widget.QMUIObservableScrollView;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 日记本和日记详情页面
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiariesAndDetailsFragment extends YMBaseFragment {

    @BindView(R.id.diary_list_scroll_refresh)
    public SmartRefreshLayout mScrollRefresh;                    //刷新加载更多
    @BindView(R.id.diary_list_scroll_refresh_more)
    public YMLoadMore mScrollMore;                              //加载更多
    @BindView(R.id.diary_list_scroll)
    public QMUIObservableScrollView mScroll;                           //滚动布局

    @BindView(R.id.diary_details_viodeo_player)
    DiariesAndPostPicView2 mPicView;                      //吸顶轮播图

    @BindView(R.id.diary_list_suck_top)
    public ViewPagerPageIndicator mListSuckTop;                      //吸顶商品详情

    @BindView(R.id.diary_posts_tip)
    public LinearLayout mDiaryPostsTip;                      //提示框
    @BindView(R.id.diary_posts_tip_title)
    public TextView mDiaryPostsTipTitle;                      //提示框文案

    private String TAG = "DiariesAndDetailsFragment";
    private String mDiaryId;                           //当前查看帖子的id

    private DiaryListData mData;                        //页面总数据
    public DiaryUserdataBean mUserdata;                //楼主信息
    private List<DiaryTaoData> taoData;

    private DiaryListFragment diaryListFragment;                            //日记日记本模块
    private CommentsAndRecommendFragment commentsAndRecommendFragment;      //推荐评论模块
    private PostBottomFragment postBottomFragment;
    private PostHeadFragment postHeadFragment;
    private DiaryListApi mDiaryListApi;
    private String mIsNew = "0";
    private DisplayMetrics metric;
    private final int VIDEO_CONTROL = 123;
    private int currentPosition;

    public static DiariesAndDetailsFragment newInstance(String diaryId, String isNew) {
        DiariesAndDetailsFragment fragment = new DiariesAndDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("diaryId", diaryId);
        bundle.putString("isNew", isNew);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        mDiaryId = getArguments().getString("diaryId");
        mIsNew = getArguments().getString("isNew");

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_posts_list;
    }

    @Override
    protected void initView(View view) {

        //下拉刷新
        mScrollRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadDiaryData();
            }
        });

        //上拉加载更多
        mScrollRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

                if (commentsAndRecommendFragment != null) {
                    commentsAndRecommendFragment.loadCommentsList();
                }
            }
        });
    }

    @Override
    protected void initData(View view) {
        mDiaryListApi = new DiaryListApi();
        metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);

        loadDiaryData();
    }

    @Override
    public void onResume() {
        super.onResume();
        //是可见，是视频fragment,播放器部位空
        if (mPicView.getYueMeiVideoView() != null) {
            if (currentPosition != 0) {
                mPicView.getYueMeiVideoView().videoRestart(currentPosition);
                currentPosition = 0;
            } else {
                mPicView.getYueMeiVideoView().videoRestart();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPicView.getYueMeiVideoView() != null) {
            mPicView.getYueMeiVideoView().videoSuspend();
        }
    }

    @Override
    public void onDestroyView() {
        if (mPicView.getYueMeiVideoView() != null) {
            mPicView.getYueMeiVideoView().videoStopPlayback();
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (isDiaryOrDetails()) {
                    //关注回调
                    if (mUserdata != null && postHeadFragment != null && data != null) {
                        mUserdata.setIs_following(data.getStringExtra("is_following"));
                        postHeadFragment.setFocusButton();
                    }
                }
                break;
            case VIDEO_CONTROL:    //视频请求回调
                if (resultCode == VideoListActivity.RETURN_VIDEO_PROGRESS) {
                    currentPosition = data.getIntExtra("current_position", 0);
                }
                break;
        }
    }

    /**
     * 加载日记的数据
     */
    private void loadDiaryData() {
        if (getActivity() != null) {
            mDialog.startLoading();
            mDiaryListApi.addData("id", mDiaryId);
            mDiaryListApi.addData("is_new", mIsNew);
            mDiaryListApi.getCallBack(mContext, mDiaryListApi.getDiaryListHashMap(), new BaseCallBackListener<DiaryListData>() {
                @Override
                public void onSuccess(DiaryListData data) {
                    mDialog.stopLoading();
                    //刷新隐藏
                    if (mScrollRefresh != null) {
                        mScrollRefresh.finishRefresh();
                    }

                    mData = data;
                    mUserdata = mData.getUserdata();
                    taoData = mData.getTaoData();

                    if (onEventClickListener != null) {
                        onEventClickListener.onLoadedClick(mData);
                    }

                    initDiaryView();                                                        //初始化日记ui
                    initDiaryData();                                                        //初始化日记本数据
                    if (!isSeeOneself() && (mData.getTaoData().size() > 0 || !TextUtils.isEmpty(mData.getHosDoc().getHospital_id()))) {
                        setTipData();
                    }
                }
            });

            //404事件处理
            mDiaryListApi.setOnEvent404Listener(new DiaryListApi.OnEvent404Listener() {
                @Override
                public void onEvent404Click(String message) {
                    mDialog.stopLoading();
                    if (onEventClickListener != null) {
                        onEventClickListener.onEvent404Click(message);
                    }
                }
            });
        }
    }

    //提示文案数据
    private String[] tipTitles = new String[]{"服务医生", "效果维持", "操作时长", "恢复周期", "机构优惠活动"};
    private static final int DIARY_POSTS_TIP11 = 11;
    private static final int DIARY_POSTS_TIP12 = 12;
    private Handler mHandler = new MyHandler(DiariesAndDetailsFragment.this);

    private static class MyHandler extends Handler {
        private final WeakReference<DiariesAndDetailsFragment> mFragment;
        private int index = 0;

        private MyHandler(DiariesAndDetailsFragment fragment) {
            mFragment = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            DiariesAndDetailsFragment theFragment = mFragment.get();
            if (theFragment != null) {
                switch (msg.what) {
                    case DIARY_POSTS_TIP11:
                        if (theFragment.mDiaryPostsTip != null) {
                            if (index == theFragment.tipTitles.length) {
                                theFragment.mDiaryPostsTip.setVisibility(View.GONE);
                            } else {
                                theFragment.mDiaryPostsTipTitle.setText(theFragment.tipTitles[index]);
                                theFragment.mDiaryPostsTip.setVisibility(View.VISIBLE);

                                //渐变
                                ObjectAnimator alphaYAnimator = ObjectAnimator.ofFloat(theFragment.mDiaryPostsTip, "alpha", 0, 1);
                                alphaYAnimator.setDuration(1000);
                                alphaYAnimator.start();

                                index++;
                                sendEmptyMessageDelayed(DIARY_POSTS_TIP12, 2000);
                            }
                        }
                        break;
                    case DIARY_POSTS_TIP12:
                        //渐变
                        ObjectAnimator alphaYAnimator = ObjectAnimator.ofFloat(theFragment.mDiaryPostsTip, "alpha", 1, 0);
                        alphaYAnimator.setDuration(1000);
                        alphaYAnimator.start();

                        sendEmptyMessageDelayed(DIARY_POSTS_TIP11, 2000);
                        break;
                }
            }
        }
    }

    /**
     * 设置右下角私信展示
     */
    private void setTipData() {
        String diariesidList = mFunctionManager.loadStr(FinalConstant.DIARIES_TIP_TAG, "0");
        if ("0".equals(diariesidList)) {
            mFunctionManager.saveStr(FinalConstant.DIARIES_TIP_TAG, "1");
            mHandler.sendEmptyMessage(DIARY_POSTS_TIP11);
        }
    }

    /**
     * 初始化日记ui
     */
    private void initDiaryView() {
        if (isDiaryOrDetails()) {
            //设置标题
            postHeadFragment = PostHeadFragment.newInstance(mData, mDiaryId);
            setActivityFragment(R.id.diary_data_diary_head, postHeadFragment);

//        删除按钮的回调
            postHeadFragment.setOnEventClickListener(new PostHeadFragment.OnEventClickListener() {
                @Override
                public void onDeleteClick(DiariesDeleteData deleteData) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemDeleteClick(deleteData);
                    }
                }
            });
        }

        //设置日记日记本数据模块
        if (isDiaryOrDetails()) {
            diaryListFragment = DiaryListFragment.newInstance(mData, mDiaryId);
            setActivityFragment(R.id.diary_data_diary_or_posts, diaryListFragment);
        } else {
            setActivityFragment(R.id.diary_data_diary_or_posts, DiaryDetailsFragment.newInstance(mData, mDiaryId));
        }

        //设置推荐评论模块
        commentsAndRecommendFragment = CommentsAndRecommendFragment.newInstance(mData, mDiaryId);
        setActivityFragment(R.id.diary_data_recom_comm, commentsAndRecommendFragment);

        //吸底模块
        postBottomFragment = PostBottomFragment.newInstance(mData);
        setActivityFragment(R.id.diary_data_list_bottom, postBottomFragment);

        //评论推荐模块回调
        commentsAndRecommendFragment.setOnEventClickListener(new CommentsAndRecommendFragment.OnEventClickListener() {
            /**
             * 发表评论完成后
             */
            @Override
            public void onCommentsClick(boolean isZhuTie) {
//                if (isZhuTie) {
                    commentsAndRecommendFragment.mAnswerNum++;
                    commentsAndRecommendFragment.setComments();

                    postBottomFragment.mAnswerNum++;
                    postBottomFragment.setComments();
//                }
            }

            @Override
            public void onLikeClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onLikeClick(data);
                }
            }

            @Override
            public void onItemDeleteClick(DiariesDeleteData deleteData) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemDeleteClick(deleteData);
                }
            }

            @Override
            public void onItemReportClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemReportClick(data);
                }
            }

            @Override
            public void onLoadMoreClick() {
                mScrollRefresh.finishLoadMore();
            }
        });

        //吸底回调
        postBottomFragment.setOnEventClickListener(new PostBottomFragment.OnEventClickListener() {
            @Override
            public void onCollectionClick(boolean b) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCollectionClick(b);
                }
            }

            @Override
            public void onLikeClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onLikeClick(data);
                }
            }

            @Override
            public void onMessageClick() {
                if (commentsAndRecommendFragment != null) {
                    commentsAndRecommendFragment.setCommentsCallback();
                }
            }

            @Override
            public void onWritePageClick(View v, String id) {
                if (diaryListFragment != null) {
                    diaryListFragment.writeAPage();
                } else {
                    Intent intent = new Intent(mContext, WriteNoteActivity.class);
                    intent.putExtra("cateid", "");
                    intent.putExtra("userid", "");
                    intent.putExtra("hosid", "");
                    intent.putExtra("hosname", "");
                    intent.putExtra("docname", "");
                    intent.putExtra("fee", "");
                    intent.putExtra("taoid", "");
                    intent.putExtra("server_id", "");
                    intent.putExtra("sharetime", mData.getSharetime());                   //手术时间
                    intent.putExtra("type", "3");
                    intent.putExtra("noteid", id);
                    intent.putExtra("notetitle", "");
                    intent.putExtra("addtype", "2");
                    intent.putExtra("beforemaxday", mData.getBefore_max_day());
                    intent.putExtra("consumer_certificate", "0");
                    intent.putExtra("write_page", true);
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * 初始化日记本日记
     */
    @SuppressLint("SetTextI18n")
    private void initDiaryData() {
        if (getActivity() != null) {
            if (!isDiaryOrDetails()) {
                //设置顶部轮播图
                List<PostOtherpic> pic = mData.getPic();
                if (pic.size() > 0) {
                    mPicView.setVisibility(View.VISIBLE);
                    mPicView.setData(pic, metric, null, mData);

                    mPicView.setOnMaxVideoClickListener(new DiariesAndPostPicView2.OnMaxVideoClickListener() {
                        @Override
                        public void onMaxVideoClick(String videoUrl, int currentPosition) {
                            Intent intent = new Intent(mContext, VideoListActivity.class);
                            intent.putExtra("id", mDiaryId);
                            intent.putExtra("flag", "1");
                            intent.putExtra("progress", currentPosition);
                            startActivityForResult(intent, VIDEO_CONTROL);
                        }
                    });
                } else {
                    mPicView.setVisibility(View.GONE);
                }
            } else {

                diaryListFragment.setOnEventClickListener(new DiaryListFragment.OnEventClickListener() {
                    @Override
                    public void onItemClick(View v, int pos) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(diaryListFragment.mDiaryRecyclerAdapter.getmDatas().get(pos).getUrl(), "0", "0");
                    }

                    @Override
                    public void onLikeClick(DiariesReportLikeData data) {
                        onEventClickListener.onLikeClick(data);
                    }

                    @Override
                    public void onItemDeleteClick(DiariesDeleteData deleteData) {
                        deleteData.setCommentsOrReply(-1);
                        deleteData.setPosPos(-1);
                        onEventClickListener.onItemDeleteClick(deleteData);
                    }
                });
            }

            List<DiaryTaoData> taoData = mData.getTaoData();
            DiaryTopSkuItemView diaryTopSkuItemView = new DiaryTopSkuItemView(mContext, mDiaryId);
            List<View> views = diaryTopSkuItemView.initTaoData(taoData);
            mListSuckTop.initView(views, taoData, Utils.dip2px(70));
            mListSuckTop.setOnItemCallBackListener(new ViewPagerPageIndicator.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos, DiaryTaoData data) {
                    if (isDiaryOrDetails()) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_TO_TAO, "top"), new ActivityTypeData(mData.getSelfPageType()));
                    } else {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHAREINFO_TO_TAO, "top"), new ActivityTypeData(mData.getSelfPageType()));

                    }
                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", data.getId());
                    if (mContext instanceof DiariesAndPostsActivity) {
                        intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                    } else {
                        intent.putExtra("source", "0");
                    }
                    intent.putExtra("objid", mDiaryId);
                    startActivity(intent);
                }
            });

            diaryTopSkuItemView.setOnEventClickListener(new DiaryTopSkuItemView.OnEventClickListener() {
                @Override
                public void onItemClick(View view, DiaryTaoData data) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("hos_id", data.getHospital_id());
                    hashMap.put("doc_id", data.getDoc_id());
                    hashMap.put("tao_id", data.getId());
                    hashMap.put("id", mDiaryId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL, "diary_top", data.getHospital_id()), hashMap, new ActivityTypeData("45"));
                }
            });

            //滚动监听
            mScroll.addOnScrollChangedListener(new QMUIObservableScrollView.OnScrollChangedListener() {
                @Override
                public void onScrollChanged(QMUIObservableScrollView scrollView, int l, int t, int oldl, int oldt) {

                    if (DiariesAndDetailsFragment.this.taoData.size() > 0) {                   //有淘数据

                        if (isDiaryOrDetails()) {                               //日记本

                            if (isSeeOneself()) {
                                if (t > 750) {
                                    mListSuckTop.setVisibility(View.VISIBLE);
                                } else {
                                    mListSuckTop.setVisibility(View.GONE);
                                }

                            } else {
                                if (t > 1000) {
                                    mListSuckTop.setVisibility(View.VISIBLE);
                                } else {
                                    mListSuckTop.setVisibility(View.GONE);
                                }
                            }
                        } else {                                                //日记

                            int mTopHeight = mPicView.getHeight() - (Utils.dip2px(50) + statusbarHeight);
                            mTopHeight = mTopHeight <= 0 ? 750 : mTopHeight;
                            if (t > mTopHeight) {
                                mListSuckTop.setVisibility(View.VISIBLE);
                            } else {
                                mListSuckTop.setVisibility(View.GONE);
                            }
                        }
                    }

                    if (onEventClickListener != null) {
                        onEventClickListener.onScrollChanged(t, mPicView.getHeight(), mData);
                    }
                }
            });

        }

    }

    /**
     * 判断是查看的自己日记还是别人的日记
     *
     * @return ：ture:查看自己的，false:查看别人的
     */
    private boolean isSeeOneself() {
        return mUserdata.getId().equals(Utils.getUid());
    }

    /**
     * 判断是日记还是日记本
     *
     * @return ：true 日记本
     */
    private boolean isDiaryOrDetails() {
        if (mData != null) {
            return "0".equals(mData.getP_id());
        } else {
            return false;
        }
    }

    /**
     * 获取日记日记本模块
     *
     * @return
     */
    public DiaryListFragment getDiaryListFragment() {
        return diaryListFragment;
    }

    /**
     * 获取评论推荐模块
     *
     * @return
     */
    public CommentsAndRecommendFragment getCommentsAndRecommendFragment() {
        return commentsAndRecommendFragment;
    }

    /**
     * 获取吸底模块
     *
     * @return
     */
    public PostBottomFragment getPostBottomFragment() {
        return postBottomFragment;
    }

    /**
     * 获取日记日记本数据
     *
     * @return
     */
    public DiaryListData getmData() {
        return mData;
    }

    public interface OnEventClickListener {
        void onLoadedClick(DiaryListData data);                 //加载数据完成后接口回调

        void onCollectionClick(boolean b);                      //收藏接口回调

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调

        void onEvent404Click(String message);                   //日记或者日记本不存在

        void onScrollChanged(int t, int height, DiaryListData data);            //滚动回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}