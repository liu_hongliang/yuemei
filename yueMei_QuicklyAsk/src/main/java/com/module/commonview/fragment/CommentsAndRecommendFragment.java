package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.CommentsListActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.CommentsRecyclerAdapter;
import com.module.commonview.adapter.GoodsGroupViewpagerAdapter;
import com.module.commonview.adapter.RecommGridViewAdapter;
import com.module.commonview.module.api.DiaryRecommListApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryOtherPostBean;
import com.module.commonview.module.bean.DiaryReplyLisListTao;
import com.module.commonview.module.bean.DiaryReplyList;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.module.bean.DiaryReplyListPic;
import com.module.commonview.module.bean.DiaryReplyListTao;
import com.module.commonview.module.bean.DiaryReplyListUserdata;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.commonview.other.PostSkuItemView;
import com.module.commonview.view.CommentDialog;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.UnderlinePageIndicator;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.PostMsgEvent;
import com.module.event.VoteMsgEvent;
import com.module.home.controller.adapter.HomeHosDocAdapter;
import com.module.home.controller.adapter.ProjectAnswerAdapter;
import com.module.home.model.bean.HomeAskEntry;
import com.module.home.model.bean.QuestionListData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 评论和推荐模块
 * Created by 裴成浩 on 2018/8/15.
 */
public class CommentsAndRecommendFragment extends YMBaseFragment {

    @BindView(R.id.diary_list_diary_recommended1)
    FrameLayout diaryRecommended1;                           //推荐列表容器（在评论之上）
    @BindView(R.id.diary_list_diary_recommended2)
    FrameLayout diaryRecommended2;                           //推荐列表容器（在评论之下）

    @BindView(R.id.ll_reply)//评论框布局 问答详情页隐藏
            LinearLayout ll_reply;

    @BindView(R.id.diary_list_comments_num)
    TextView mCommentsNum;                          //评论数
    @BindView(R.id.diary_list_look_more)
    TextView mCommentLookMore;                          //查看更多评论
    @BindView(R.id.diary_list_comments_img)
    ImageView mCommentsImg;                   //当前用户头像
    @BindView(R.id.diary_list_comments_list_release)
    TextView mCommentsRelease;                           //好日记求鼓励
    @BindView(R.id.diary_list_comments_list_container)
    public LinearLayout mCommentsRecyclerContainer;        //评论列表容器
    @BindView(R.id.diary_list_comments_list)
    RecyclerView mCommentsRecycler;                 //评论列表
    @BindView(R.id.diary_list_comments_list_more)
    LinearLayout mCommentsRecyclerMore;             //评论列表查看更多

    @BindView(R.id.diary_list_container)
    LinearLayout questionContainer;
    @BindView(R.id.diary_list_related_title)
    TextView mRelatedTitle;                             //问答提示
    @BindView(R.id.diary_list_related)
    RecyclerView mRelatedList;                         //问答列表模块

    @BindView(R.id.aiary_list_related_title)
    TextView mRecommTitle;                         //相关日记推荐列表
    @BindView(R.id.aiary_list_related_recommended)
    RecyclerView mRecommGridView;                       //相关日记推荐列表

    private String TAG = "CommentsAndRecommendFragment";

    private String mDiaryId;                           //当前查看帖子的id
    public DiaryUserdataBean mUserdata;                //楼主信息
    private List<DiaryReplyList> mReplyList;

//    private SumbitPhotoData sumbitPhotoData;

    private Bundle commentsBundle;                              //评论列表传值

    private DiaryCommentDialogView mDiaryCommentDialogView;
    public CommentsRecyclerAdapter mCommentsRecyclerAdapter;   //评论列表适配器
    private String mAskorshare;
    private String mPid;
    public int mAnswerNum;
    private HomeAskEntry mAskEntry;
    private DiaryRecommListApi mDiaryRecommListApi;
    private RecommGridViewAdapter recommGridViewAdapter;
    private int mPage = 1;
    private List<HomeTaoData> mTaolist = new ArrayList<>();
    private String selfPageType;
    private ProjectAnswerAdapter mProjectAnswerAdapter;
    private List<QuestionListData> other_question;
    private String mTagTitle;
    private CommentDialog commentDialog;

    public static CommentsAndRecommendFragment newInstance(DiaryListData data, String diaryId) {
        CommentsAndRecommendFragment fragment = new CommentsAndRecommendFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);

        return fragment;
    }

    public static CommentsAndRecommendFragment newInstance(PostListData data, String diaryId) {
        CommentsAndRecommendFragment fragment = new CommentsAndRecommendFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("diaryId", diaryId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        //判断是否有输入框 有的话弹出键盘(选择图片跳页再回来)
        if (mDiaryCommentDialogView != null && mDiaryCommentDialogView.isShowing()) {
            mDiaryCommentDialogView.showKeyboard();
        }
        if (commentDialog != null
                && commentDialog.isShowing()
                && commentDialog.getDiaryCommentDialogView() != null
                && commentDialog.getDiaryCommentDialogView().isShowing()) {
            commentDialog.getDiaryCommentDialogView().showKeyboard();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(PostMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 0:
                //评论减1
                mAnswerNum = mAnswerNum - (int) msgEvent.getData();
                if (mAnswerNum < 0) {
                    mAnswerNum = 0;
                }
                //查看更多评论
                if (mAnswerNum > 3) {
                    mCommentsRecyclerMore.setVisibility(View.VISIBLE);
                } else {
                    mCommentsRecyclerMore.setVisibility(View.GONE);
                }
                setComments();
                break;
            case 1:
                //评论加1
                mAnswerNum = mAnswerNum + 1;
                //查看更多评论
                if (mAnswerNum > 3) {
                    mCommentsRecyclerMore.setVisibility(View.VISIBLE);
                } else {
                    mCommentsRecyclerMore.setVisibility(View.GONE);
                }
                setComments();
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mDiaryRecommListApi = new DiaryRecommListApi();
        mDiaryId = getArguments().getString("diaryId");
        if (isDiaries()) {
            DiaryListData mData = getArguments().getParcelable("data");
            mUserdata = mData.getUserdata();
            mReplyList = mData.getReplyList();
            mAskorshare = mData.getAskorshare();
            mPid = mData.getP_id();
            selfPageType = mData.getSelfPageType();
            try {
                mAnswerNum = Integer.parseInt(mData.getAnswer_num());
            } catch (Exception e) {
                mAnswerNum = 0;
            }
            mTaolist = mData.getTaolist();
            List<DiaryTagList> tag = mData.getTag();
            if (tag != null && tag.size() > 0) {
                mTagTitle = tag.get(0).getName();
            }

        } else {
            PostListData mData = getArguments().getParcelable("data");
            mUserdata = mData.getUserdata();
            mReplyList = mData.getReplyList();
            mAskorshare = mData.getAskorshare();
            mPid = mData.getP_id();
            mAnswerNum = Integer.parseInt(mData.getAnswer_num());
            mTaolist = mData.getTaolist();
            mAskEntry = mData.getAsk_entry();
            selfPageType = mData.getSelfPageType();
            other_question = mData.getOther_question();
            List<DiaryTagList> tag = mData.getTag();
            if (tag != null && tag.size() > 0) {
                mTagTitle = tag.get(0).getName();
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_adiary_list_other;
    }

    @Override
    protected void initView(View view) {
        Log.e(TAG, "mTaolist.size() == " + mTaolist.size());
        if (mTaolist.size() != 0) {
            if ("0".equals(mAskorshare)) {
                //如果是问题贴
                mTaolist.size();
                diaryRecommended2.setVisibility(View.VISIBLE);

                ll_reply.setVisibility(View.GONE);

                //推荐sku显示
                View skuView = View.inflate(mContext, R.layout.activity_adiary_list_other_sku2, diaryRecommended2);
                ViewPager skuPager = skuView.findViewById(R.id.adiary_list_other_sku2_pager);
                UnderlinePageIndicator skuIndicator = skuView.findViewById(R.id.adiary_list_other_sku2_indicator);

                //初始化淘数据
                GoodsGroupViewpagerAdapter adapter2 = new GoodsGroupViewpagerAdapter(new PostSkuItemView(mContext, mDiaryId, mTagTitle).initTaoData(mTaolist));
                skuPager.setAdapter(adapter2);

                for (int i = 0; i < mTaolist.size(); i++) {
                    if (TextUtils.isEmpty(mTaolist.get(i).getFanxian())) {
                        ViewGroup.LayoutParams layoutParams = skuPager.getLayoutParams();
                        layoutParams.height = DensityUtil.dip2px(100);
                        skuPager.setLayoutParams(layoutParams);
                    } else {
                        ViewGroup.LayoutParams layoutParams = skuPager.getLayoutParams();
                        layoutParams.height = DensityUtil.dip2px(135);
                        skuPager.setLayoutParams(layoutParams);
                    }
                }

                //item点击事件
                adapter2.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
                    @Override
                    public void onItemClick(View v, int pos) {
                        goSkuActivity(pos);
                    }
                });

                if (mTaolist.size() > 1) {
                    skuIndicator.setVisibility(View.VISIBLE);
                    skuIndicator.setViewPager(skuPager);
                } else {
                    skuPager.setVisibility(View.VISIBLE);
                    skuIndicator.setVisibility(View.GONE);
                }

            } else {
                diaryRecommended1.setVisibility(View.VISIBLE);

                //推荐sku设置
                View skuView = View.inflate(mContext, R.layout.activity_adiary_list_other_sku1, diaryRecommended1);
                RecyclerView diaryHosdocRl = skuView.findViewById(R.id.diary_list_diary_hosdoc_recycler);

                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
                diaryHosdocRl.setLayoutManager(layoutManager);
                HomeHosDocAdapter homeHosDocAdapter = new HomeHosDocAdapter(mContext, mTaolist, false);
                diaryHosdocRl.setAdapter(homeHosDocAdapter);

                homeHosDocAdapter.setOnItemClickListener(new HomeHosDocAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int pos) {
                        goSkuActivity(pos);
                    }
                });
            }
        }
    }

    @Override
    protected void initData(View view) {
//        sumbitPhotoData = new SumbitPhotoData();
        commentsBundle = new Bundle();

        loadCommentsList();
        initPostData();
        if ("0".equals(mAskorshare) && other_question != null && !other_question.isEmpty()) {
            questionContainer.setVisibility(View.VISIBLE);
            setRecyclerData((ArrayList<QuestionListData>) other_question);
        } else {
            questionContainer.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDestroyView() {

        if (mContext != null && !mContext.isFinishing()) {
            if (mDiaryCommentDialogView != null) {
                mDiaryCommentDialogView.dismiss();
            }
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("私密", "1");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("私密", "1");
                        }
                    } else {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("公开", "0");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("公开", "0");
                        }
                    }
                }
                break;
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                Log.e(TAG, "mContext.RESULT_OK ==" + Activity.RESULT_OK);
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            commentDialog.getDiaryCommentDialogView().gridviewInit();
                        } else {
                            mDiaryCommentDialogView.setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            mDiaryCommentDialogView.gridviewInit();
                        }
                    }
                }
                break;
        }
    }

    private void initPostData() {
        if (Utils.isLogin()) {
            Glide.with(mContext).load(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(mCommentsImg);
        } else {
            //当前用户头像
            Glide.with(mContext).load(mFunctionManager.loadStr(FinalConstant.UHEADIMG, "")).transform(new GlideCircleTransform(mContext)).into(mCommentsImg);
        }

        setComments();

        //初始化评论列表
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        mCommentsRecycler.setLayoutManager(scrollLinearLayoutManager);
        mCommentsRecyclerAdapter = new CommentsRecyclerAdapter(mContext, mReplyList, Utils.dip2px(20), CommentsRecyclerAdapter.MORE_COMMENTS, mAskEntry, mAskorshare);
        mCommentsRecycler.setAdapter(mCommentsRecyclerAdapter);

        //初始化相关日记推荐列表
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 2);
        gridLayoutManager.setScrollEnable(false);
        mRecommGridView.setLayoutManager(gridLayoutManager);
        //好日记求鼓励
        mCommentsRelease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"0".equals(mAskorshare)) {
                    startComments();
                } else {
                    if (mUserdata.getId().equals(Utils.getUid())) {
                        startComments();
                    } else {
                        mFunctionManager.showShort("问题贴仅限医生和提问者评论");
                    }
                }
            }
        });

        mCommentsRecyclerAdapter.setOnEventClickListener(new CommentsRecyclerAdapter.OnEventClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }

            @Override
            public void onItemLikeClick(DiariesReportLikeData data) {
                if (onEventClickListener != null) {
                    onEventClickListener.onLikeClick(data);
                }
            }

            @Override
            public void onItemReplyClick(View v, final int pos, String name) {
                SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
                sumbitPhotoData.setUserid(mUserdata.getId());
                sumbitPhotoData.set_id(mReplyList.get(pos).getUserdata().getId());
                sumbitPhotoData.setQid(mDiaryId);
                sumbitPhotoData.setCid(mReplyList.get(pos).getId());
                sumbitPhotoData.setAskorshare(mAskorshare);
                sumbitPhotoData.setNickname(name);
                mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
                mDiaryCommentDialogView.showDialog();
                mDiaryCommentDialogView.setPicturesChooseGone(true);

//                sumbitPhotoData.setUserid("");
//                sumbitPhotoData.set_id("");
//                sumbitPhotoData.setQid("");
//                sumbitPhotoData.setCid("");
//                sumbitPhotoData.setAskorshare("");
//                sumbitPhotoData.setNickname("");

                //回复楼中楼提交完成回调
                mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                    @Override
                    public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                        if (Utils.isLoginAndBind(mContext)) {
                            DiaryReplyListList diaryReplyListList = new DiaryReplyListList();
                            diaryReplyListList.setId(id);
                            diaryReplyListList.setContent(content);
                            diaryReplyListList.setTao(new DiaryReplyLisListTao());

                            //设置当前用户信息
                            DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                            diaryReplyListUserdata.setTalent("0");
                            diaryReplyListUserdata.setLable("");
                            diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                            diaryReplyListUserdata.setId(Utils.getUid());
                            diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                            diaryReplyListList.setUserdata(diaryReplyListUserdata);


                            if (mCommentsRecyclerAdapter.getmDatas().get(pos).getList() == null
                                    || mCommentsRecyclerAdapter.getmDatas().get(pos).getList().size() == 0) {          //如果当前没有评论
                                mCommentsRecyclerAdapter.setCommentsPosReply(pos, diaryReplyListList);
//                                mCommentsRecyclerAdapter.notifyDataSetChanged();
                            } else {
                                mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).addItem(diaryReplyListList);
//                                mCommentsRecyclerAdapter.getHashMapAdapter().get(pos).notifyDataSetChanged();
                            }
                            if (onEventClickListener != null) {
                                onEventClickListener.onCommentsClick(false);
                            }
                        }
                    }
                });
            }

            @Override
            public void onItemDeleteClick(DiariesDeleteData deleteData) {
                onEventClickListener.onItemDeleteClick(deleteData);
            }

            @Override
            public void onItemReportClick(DiariesReportLikeData data) {
                onEventClickListener.onItemReportClick(data);
            }

            @Override
            public void onTaoAndDiaryClick(DiaryReplyListTao tao) {
                if ("1".equals(tao.getIs_tao())) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", mDiaryId);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.REPLY_TO_TAO, "reply"), hashMap, new ActivityTypeData(selfPageType));
                }

                String source;
                if (mContext instanceof DiariesAndPostsActivity) {
                    source = ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier();
                } else {
                    source = "0";
                }
                WebUrlTypeUtil.getInstance(mContext).urlToApp(tao.getUrl(), source, "0");
            }
        });

        //查看更多回复
        mCommentsRecyclerAdapter.setItemJumpCallBackListener(new CommentsRecyclerAdapter.ItemJumpCallBackListener() {
            @Override
            public void onItemJumpClick(View v) {
//                commentsBundle.putString("id", mUserdata.getId());
//                commentsBundle.putString("diary_id", mDiaryId);
//                commentsBundle.putString("askorshare", mAskorshare);
//                commentsBundle.putString("p_id", mPid);
//                commentsBundle.putString("user_id", mUserdata.getId());
//                mFunctionManager.goToActivity(CommentsListActivity.class, commentsBundle);
                commentDialog = new CommentDialog(mContext, mDiaryId, mUserdata.getId(), mAskorshare, mPid, mAnswerNum);
                if (commentDialog != null) {
                    commentDialog.show();
                }
            }
        });

        //查看更多评论
        if (mAnswerNum > 3) {
            mCommentsRecyclerMore.setVisibility(View.VISIBLE);
        } else {
            mCommentsRecyclerMore.setVisibility(View.GONE);
        }

        //其他点击事件
        setMultiOnClickListener(mCommentsRecyclerMore, mCommentsImg);
    }


    /**
     * 评论回调
     */
    public void setCommentsCallback() {
        if (!"0".equals(mAskorshare)) {
            if (mAnswerNum > 0) {
                commentDialog = new CommentDialog(mContext, mDiaryId, mUserdata.getId(), mAskorshare, mPid, mAnswerNum);
                if (commentDialog != null) {
                    commentDialog.show();
                }
            } else {
                startComments();
            }
        } else {
            if (mUserdata.getId().equals(Utils.getUid())) {
                if (mAnswerNum > 0) {
                    commentDialog = new CommentDialog(mContext, mDiaryId, mUserdata.getId(), mAskorshare, mPid, mAnswerNum);
                    if (commentDialog != null) {
                        commentDialog.show();
                    }
                } else {
                    startComments();
                }
            } else {
                mFunctionManager.showShort("问题贴仅限医生和提问者评论");
            }
        }
    }

    private void startComments() {
        if (Utils.isLoginAndBind(mContext)) {
            SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
            sumbitPhotoData.setUserid(mUserdata.getId());
            sumbitPhotoData.setQid(mDiaryId);
            sumbitPhotoData.setAskorshare(mAskorshare);
            mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
            mDiaryCommentDialogView.showDialog();
            mDiaryCommentDialogView.setPicturesChooseGone(false);

//            sumbitPhotoData.setUserid("");
//            sumbitPhotoData.setQid("");
//            sumbitPhotoData.setAskorshare("");

            //评论提交完成回调
            mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                @Override
                public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                    //评论后的刷新
                    DiaryReplyList diaryReplyList = new DiaryReplyList();
                    diaryReplyList.setId(id);
                    diaryReplyList.setAgree_num("0");
                    diaryReplyList.setContent(content);
                    diaryReplyList.setList(new ArrayList<DiaryReplyListList>());
                    diaryReplyList.setReply_num("0");
                    diaryReplyList.setTao(new DiaryReplyListTao());
                    //设置当前用户信息
                    DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                    diaryReplyListUserdata.setTalent("0");
                    diaryReplyListUserdata.setLable("刚刚");
//                    diaryReplyListUserdata.setName(mFunctionManager.loadStr(FinalConstant.UNAME, "悦美用户"));
                    diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                    diaryReplyListUserdata.setId(Utils.getUid());
//                    diaryReplyListUserdata.setAvatar(mFunctionManager.loadStr(FinalConstant.UHEADIMG, ""));
                    diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                    diaryReplyList.setUserdata(diaryReplyListUserdata);

                    //设置评论图片
                    List<DiaryReplyListPic> pics = new ArrayList<>();
                    for (String s : imgLists) {
                        pics.add(new DiaryReplyListPic(s));
                    }
                    diaryReplyList.setPic(pics);
                    mCommentsRecyclerAdapter.addItem(diaryReplyList);
                    mCommentsRecyclerAdapter.notifyDataSetChanged();
                    if (mCommentsRecyclerContainer.getVisibility() == View.GONE || mCommentsRecyclerContainer.getVisibility() == View.INVISIBLE) {
                        mCommentsRecyclerContainer.setVisibility(View.VISIBLE);
                    }

                    if (onEventClickListener != null) {
                        onEventClickListener.onCommentsClick(true);
                    }

                }
            });
        }
    }

    /**
     * 设置评论数
     */
    @SuppressLint("SetTextI18n")
    public void setComments() {
        //吸底评论数
        if (mAnswerNum > 0) {
            mCommentsRecyclerContainer.setVisibility(View.VISIBLE);
            if (mAskorshare != null && "0".equals(mAskorshare)) {
                mCommentsNum.setText("当前共有" + mAnswerNum + "条答案");
                mCommentLookMore.setText("查看更多" + mAnswerNum + "条解答");
            } else {
                mCommentsNum.setText("热评(" + mAnswerNum + ")");
                mCommentLookMore.setText("查看更多" + mAnswerNum + "条评论");
            }
        } else {
            mCommentsRecyclerContainer.setVisibility(View.GONE);
            mCommentsNum.setText("暂无评价");
        }
    }

    /**
     * 跳转SKU页面
     *
     * @param pos
     */
    private void goSkuActivity(int pos) {
        String tao_id = mTaolist.get(pos).getId();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", mDiaryId);
        hashMap.put("to_page_type", "2");
        hashMap.put("to_page_id", tao_id);
        if (isDiaries()) {
            if (isDiaryOrDetails()) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_TO_TAO, "recomend"), hashMap, new ActivityTypeData(selfPageType));
            } else {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHAREINFO_TO_TAO, "recomend"), hashMap, new ActivityTypeData(selfPageType));
            }
        } else {
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TO_TAO, "recomend"), hashMap, new ActivityTypeData(selfPageType));
        }

        Intent intent = new Intent(mContext, TaoDetailActivity.class);
        intent.putExtra("id", tao_id);
        if (mContext instanceof DiariesAndPostsActivity) {
            intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
        } else {
            intent.putExtra("source", "0");
        }
        intent.putExtra("objid", mDiaryId);
        startActivity(intent);
    }

    /**
     * 判断是日记还是帖子
     *
     * @return true 日记
     */
    private boolean isDiaries() {
        return getArguments().getParcelable("data") instanceof DiaryListData;
    }

    /**
     * 判断是日记还是日记本
     *
     * @return ：true 日记本
     */
    private boolean isDiaryOrDetails() {
        if (!TextUtils.isEmpty(mPid)) {
            return "0".equals(mPid);
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.diary_list_comments_list_more:    //评论列表查看更多

//                commentsBundle.putString("id", mUserdata.getId());
//                commentsBundle.putString("diary_id", mDiaryId);
//                commentsBundle.putString("askorshare", mAskorshare);
//                commentsBundle.putString("p_id", mPid);
//                commentsBundle.putString("user_id", mUserdata.getId());
//                mFunctionManager.goToActivity(CommentsListActivity.class, commentsBundle);
                commentDialog = new CommentDialog(mContext, mDiaryId, mUserdata.getId(), mAskorshare, mPid, mAnswerNum);
                if (commentDialog != null) {
                    commentDialog.show();
                }
                break;
            case R.id.diary_list_comments_img:       //用户自己的头像点击
                Intent intent = new Intent(mContext, PersonCenterActivity641.class);
                intent.putExtra("id", Utils.getUid());
                startActivity(intent);

                break;
        }
    }

    /**
     * 加载评论列表
     */
    public void loadCommentsList() {
        mDiaryRecommListApi.addData("id", mDiaryId);                     //日记本id
        mDiaryRecommListApi.addData("page", mPage + "");
        mDiaryRecommListApi.getCallBack(mContext, mDiaryRecommListApi.getHashMap(), new BaseCallBackListener<List<DiaryOtherPostBean>>() {
            @Override
            public void onSuccess(List<DiaryOtherPostBean> diaryOtherPostBeans) {
                mPage++;
                initCommentsList(diaryOtherPostBeans);                                                     //初始化推荐列表
            }
        });
    }

    /**
     * 设置提问列表数据
     *
     * @param questionList
     */
    @SuppressLint("SetTextI18n")
    private void setRecyclerData(ArrayList<QuestionListData> questionList) {
        if (!TextUtils.isEmpty(mTagTitle)) {
            mRelatedTitle.setText(mTagTitle + "问答热榜");
        } else {
            mRelatedTitle.setText("问答热榜");
        }

        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        mRelatedList.setLayoutManager(scrollLinearLayoutManager);
        mProjectAnswerAdapter = new ProjectAnswerAdapter(mContext, questionList, "PostsDetails");
        mRelatedList.setAdapter(mProjectAnswerAdapter);

        //item点击事件
        mProjectAnswerAdapter.setOnItemClickListener(new ProjectAnswerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int pos, QuestionListData data) {
                HashMap<String, String> eventParams = data.getEvent_params();
                YmStatistics.getInstance().tongjiApp(eventParams);

                String jumpUrl = data.getJumpUrl();
                HashMap<String, String> event_params = mProjectAnswerAdapter.getDatas().get(pos).getEvent_params();
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_ASKLIST_CLICK, "channel|sharelist_" + Utils.getCity() + "_" + getTagId() + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE + ""), event_params, new ActivityTypeData("121"));
                if (!TextUtils.isEmpty(jumpUrl)) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpUrl);
                }
            }
        });
    }

    /**
     * 初始化评论列表
     *
     * @param diaryOtherPostBeans
     */
    @SuppressLint("SetTextI18n")
    private void initCommentsList(final List<DiaryOtherPostBean> diaryOtherPostBeans) {
        if (diaryOtherPostBeans.size() < 20) {
            onEventClickListener.onLoadMoreClick();
        } else {
            onEventClickListener.onLoadMoreClick();
        }

        if (!TextUtils.isEmpty(mTagTitle)) {
            mRecommTitle.setText("更多" + mTagTitle + "内容");
        } else {
            mRecommTitle.setText("更多内容");
        }

        if (recommGridViewAdapter == null) {
            recommGridViewAdapter = new RecommGridViewAdapter(mContext, diaryOtherPostBeans, Utils.dip2px(20), Utils.dip2px(20), Utils.dip2px(5));
            mRecommGridView.setAdapter(recommGridViewAdapter);

            //相关日记推荐点击
            recommGridViewAdapter.setOnItemCallBackListener(new RecommGridViewAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, DiaryOtherPostBean data, int pos) {
                    if (!TextUtils.isEmpty(data.getUrl())) {
                        if (isDiaries()) {
                            HashMap<String, String> event_params = data.getEvent_params();
                            event_params.put("id", mDiaryId);
                            event_params.put("to_page_id", mDiaryId);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_RECOMMEND_POST_CLICK, (pos + 1) + ""), event_params, new ActivityTypeData("45"));
                        }
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUrl());
                    }
                }

            });
        } else {
            recommGridViewAdapter.setDataAll(diaryOtherPostBeans);
        }
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {

        void onCommentsClick(boolean isZhuTie);                 //评论发布后接口回调  true：主贴评论，false：回复楼中楼

        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调

        void onLoadMoreClick();     //加载更多
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
