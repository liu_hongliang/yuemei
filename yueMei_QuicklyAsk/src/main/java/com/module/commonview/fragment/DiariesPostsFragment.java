package com.module.commonview.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.view.YMBaseWebViewFragment;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.module.api.WebBottomApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.commonview.module.bean.VideoShareData;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.model.bean.BBsButtonData;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.module.my.controller.activity.EnrolPageActivity647;
import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIPullRefreshLayout;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONObject;
import org.kymjs.aframe.ui.ViewInject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 帖子详情
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiariesPostsFragment extends YMBaseWebViewFragment {

//    @BindView(R.id.doary_posts_progress_bar)
//    ProgressBar mProgressBar;                       //webView加载进度
    @BindView(R.id.diary_posts_web_view)
    MyPullRefresh webViewContainer;                  //webView容器
    @BindView(R.id.fragment_diary_posts_bottom_container)
    FrameLayout bottomContainer;                  //吸底容器

    private String TAG = "DiariesPostsFragment";
    private String mUrl;
    private String mDiaryId;
    private String askorshare;
    private String user_id;
    private VideoShareData videoShareData;

    private LinearLayout likeClick;
    public ImageView likeImg;
    public TextView likeTitle;
    private LinearLayout commentsClick;
    private LinearLayout signClick;
    private HashMap<String, Object> headMap = new HashMap<>();
    private HashMap<String, Object> paramMap = new HashMap<>();

    private final int ENROL_CODE = 100;
    private DiaryCommentDialogView mDiaryCommentDialogView;
    private BaseWebViewClientMessage clientManager;
    private WebBottomApi mWebBottomApi;

    public static DiariesPostsFragment newInstance(DiaryListData data, String id, String url) {
        DiariesPostsFragment fragment = new DiariesPostsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", id);
        bundle.putString("url", url);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DiaryListData mData = getArguments().getParcelable("data");

        mDiaryId = getArguments().getString("id");
        mUrl = getArguments().getString("url");

        if(mData != null){
            askorshare = mData.getAskorshare();
            user_id = mData.getUserdata().getId();
        }

        clientManager = new BaseWebViewClientMessage(mContext);
        mWebBottomApi = new WebBottomApi();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_posts;
    }

    @Override
    protected void initView(View view) {
        Log.e(TAG, "mUrl === " + mUrl);
        webViewContainer.addView(mWebView);

        initWebVeiw();

        //下拉刷新
        webViewContainer.setOnPullListener(new QMUIPullRefreshLayout.OnPullListener() {
            @Override
            public void onMoveTarget(int offset) {
            }

            @Override
            public void onMoveRefreshView(int offset) {

            }

            @Override
            public void onRefresh() {
                headMap.clear();
                paramMap.clear();

                initWebVeiw();
            }
        });

        /**
         * 公共跳转外的跳转
         */
        clientManager.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String url) {
                try {
                    showWebDetail(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void initData(View view) {
        initUrlData();
    }


    /**
     * 初始化
     */
    private void initWebVeiw() {
        headMap.put("id", mDiaryId);
        if (!TextUtils.isEmpty(askorshare)) {
            headMap.put("askorshare", askorshare);
            paramMap.put("askorshare", askorshare);
        }

        loadUrl(mUrl, paramMap, headMap);
    }

    /**
     * 初始化下边的吸底
     */
    private void initUrlData() {

        switch (askorshare) {
            case "0":          //问题贴
                View view1 = mInflater.inflate(R.layout.fragment_diary_posts_answer, bottomContainer);
                LinearLayout pageClick = view1.findViewById(R.id.detail_posts_bottom_page_click);
                commentsClick = view1.findViewById(R.id.detail_posts_bottom_comments_click);

                if (isSeeOneself()) {
                    pageClick.setVisibility(View.GONE);
                    commentsClick.setVisibility(View.VISIBLE);
                } else {
                    pageClick.setVisibility(View.VISIBLE);
                    commentsClick.setVisibility(View.GONE);
                }

                break;
            case "3":          //达人、活动帖

                View view2 = mInflater.inflate(R.layout.fragment_diary_posts_bottom, bottomContainer);
                likeClick = view2.findViewById(R.id.detail_posts_bottom_like_click);
                likeImg = view2.findViewById(R.id.detail_posts_bottom_like_img);
                likeTitle = view2.findViewById(R.id.detail_posts_bottom_like_title);
                commentsClick = view2.findViewById(R.id.detail_posts_bottom_comments_click);
                signClick = view2.findViewById(R.id.detail_posts_bottom_sign_click);

                setPostsData();

                //点赞
                likeClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDiaryId);
                        data.setFlag("1");
                        data.setIs_reply("0");
                        onEventClickListener.onLikeClick(data);
                    }
                });


                break;
            case "4":           //随聊帖
            default:            //其他
                View view3 = mInflater.inflate(R.layout.fragment_diary_posts_bottom1, bottomContainer);
                likeClick = view3.findViewById(R.id.detail_posts_bottom_like_click);
                likeImg = view3.findViewById(R.id.detail_posts_bottom_like_img);
                likeTitle = view3.findViewById(R.id.detail_posts_bottom_like_title);
                commentsClick = view3.findViewById(R.id.detail_posts_bottom_comments_click);

                //点赞
                likeClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDiaryId);
                        data.setFlag("1");
                        data.setIs_reply("0");
                        onEventClickListener.onLikeClick(data);
                    }
                });

                break;
        }

        //评论
        commentsClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {

                    SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
                    sumbitPhotoData.setUserid(user_id);
                    sumbitPhotoData.setQid(mDiaryId);
                    sumbitPhotoData.setAskorshare(askorshare);
                    mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
                    mDiaryCommentDialogView.showDialog();

                    //评论完成后刷新
                    mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                        @Override
                        public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                            mWebView.reload();
                        }
                    });

                }
            }
        });
    }

    private void setPostsData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mDiaryId);
        mWebBottomApi.getCallBack(mContext, maps, new BaseCallBackListener<BBsButtonData>() {

            @Override
            public void onSuccess(BBsButtonData btData) {
                final String moban = btData.getMoban();

                if ("0".equals(moban)) {
                    signClick.setVisibility(View.GONE);
                } else {
                    signClick.setVisibility(View.VISIBLE);

                    //报名
                    signClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isLoginAndBind(mContext)) {
                                Intent it3 = new Intent();
                                it3.putExtra("cateid", mDiaryId);
                                it3.putExtra("moban", moban);        //模版这里要换成动态的
                                it3.setClass(mContext, EnrolPageActivity647.class);
                                startActivityForResult(it3, ENROL_CODE);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onYmLoadResource(WebView view, String url) {
        if (url.contains("m.yuemei.com")) {
            WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "10", mDiaryId, videoShareData);
        }
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        WebView.HitTestResult hitTestResult = view.getHitTestResult();
        int hitType = hitTestResult.getType();
        Log.e(TAG, "hitTestResult == " + hitTestResult);
        Log.e(TAG, "hitType == " + hitType);
        Log.e(TAG, "url == " + url);

        if (hitType != WebView.HitTestResult.UNKNOWN_TYPE) {
            if (url.startsWith("type")) {
                clientManager.showWebDetail(url);
            } else {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "10", mDiaryId, videoShareData);
            }
            return true;
        }
        return super.ymShouldOverrideUrlLoading(view, url);
    }

    @Override
    protected void onYmPageFinished(WebView view, String url) {
        view.loadUrl("javascript:window.getShareData.OnGetShareData(" + "document.querySelector('meta[name=\"reply_info\"]').getAttribute('content')" + ");");
        super.onYmPageFinished(view, url);
    }

    @Override
    protected void onYmReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
    }

    @Override
    protected void onYmReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        view.loadData("<html><body><h1>  </h1></body></html>", "text/html", "UTF-8");
        ViewInject.toast("请检查您的网络");
    }

    @Override
    protected boolean onYmJsAlert(WebView view, String url, String message, JsResult result) {
        String[] strs = message.split("\n");
        showDialogJsAlert(message, result, strs.length);
        return true;
    }

    @Override
    protected boolean onYmJsConfirm(WebView view, String url, String message, JsResult result) {
        String[] strs = message.split("\n");
        showDialogConfirm(message, result, strs.length);
        return true;
    }

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
//        if (mProgressBar != null) {
//            if (newProgress == 100) {
//                webViewContainer.finishRefresh();
//                mProgressBar.setVisibility(View.GONE);
//            } else {
//                if (View.GONE == mProgressBar.getVisibility()) {
//                    mProgressBar.setVisibility(View.VISIBLE);
//                }
//                mProgressBar.setProgress(newProgress);
//            }
//        }

        super.onYmProgressChanged(view, newProgress);
    }

    /**
     * dialog提示
     *
     * @param content
     * @param num
     */
    private void showDialogJsAlert(String content, final JsResult result, int num) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);

        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                result.confirm();       //关闭js的弹窗
                dialog.dismiss();
            }
        });
    }

    private void showDialogConfirm(final String content, final JsResult result, final int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dialog_edit_exit2);

        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mFunctionManager.showShort("取关成功");
                result.confirm();
                dialog.dismiss();
            }
        });

        Button cancelBt99 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "bbbbb");
                result.cancel();
                dialog.dismiss();
            }
        });

    }

    /**
     * 设置视频播放数据
     *
     * @param videoShareData
     */
    public void setVideoShareData(VideoShareData videoShareData) {
        this.videoShareData = videoShareData;
    }


    /**
     * 判断是查看的自己日记还是别人的日记
     *
     * @return ：ture:查看自己的，false:查看别人的
     */
    private boolean isSeeOneself() {
        return user_id.equals(Utils.getUid());
    }

    /**
     * 获取到webView
     *
     * @return
     */
    public WebView getWebView() {
        return mWebView;
    }

    @Override
    public void onDestroy() {
        if (mContext != null && !mContext.isFinishing() && mDiaryCommentDialogView != null) {
            mDiaryCommentDialogView.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ENROL_CODE:
                if (null != data && resultCode == EnrolPageActivity647.ENROL_CODE) {
                    String textMessage = data.getStringExtra("textMessage");
                    showDialogExitEdit(textMessage, "我知道了");
                }
                break;
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        mDiaryCommentDialogView.setIsPublic("私密", "1");
                    } else {
                        mDiaryCommentDialogView.setIsPublic("公开", "0");
                    }
                }
                break;
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                if (resultCode == Activity.RESULT_OK) {
                    if (null != data) {
                        mDiaryCommentDialogView.setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                        mDiaryCommentDialogView.gridviewInit();
                    }
                }
                break;
        }
    }

    /**
     * 公共跳转外的跳转
     *
     * @param urlStr
     * @throws Exception
     */
    private void showWebDetail(String urlStr) throws Exception {
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页
                String id1 = obj.getString("id");
                String docname1 = URLDecoder.decode(obj.getString("docname"), "utf-8");

                Intent intent = new Intent();
                intent.setClass(mContext, DoctorDetailsActivity592.class);
                intent.putExtra("docId", id1);
                intent.putExtra("docName", docname1);
                intent.putExtra("partId", "");
                startActivity(intent);

                break;
            case "2":   //回复问题
                if (Utils.isLoginAndBind(mContext)) {
                    String cid = obj.getString("cid");
                    String qid = obj.getString("qid");
                    String askorshare = obj.getString("askorshare");

                    SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
                    sumbitPhotoData.setUserid(user_id);
                    sumbitPhotoData.set_id(qid);
                    sumbitPhotoData.setQid(mDiaryId);
                    sumbitPhotoData.setCid(cid);
                    sumbitPhotoData.setAskorshare(askorshare);
                    mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
                    mDiaryCommentDialogView.showDialog();
                    mDiaryCommentDialogView.setPicturesChooseGone(true);

                    //回复完成后刷新
                    mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                        @Override
                        public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                            mWebView.reload();
                        }
                    });
                }

                break;

            case "3":   // 补充几句 需要上传照片
                mFunctionManager.showShort("补充几句 需要上传照片");
                break;
            case "4":   // 删除主帖 接口
                String id4 = obj.getString("id");
                String reply4 = obj.getString("reply");

                DiariesDeleteData data4 = new DiariesDeleteData();
                data4.setId(id4);
                data4.setReplystr(reply4);
                data4.setCommentsOrReply(0);
                data4.setZhuTie(true);
                onEventClickListener.onItemDeleteClick(data4);

                break;

            case "5":   //只看楼主
                String userid = obj.getString("userid");
                headMap.put("userid", userid);
                paramMap.put("userid", userid);
                loadUrl(mUrl, paramMap, headMap);
                break;

            case "6":   // 查看更多问题

                Intent intent6 = new Intent();
                String link6 = obj.getString("link");
                String louc6 = obj.getString("louc");
                String userid6 = obj.getString("userid");
                intent6.setClass(mContext, BBsFinalWebActivity.class);
                intent6.putExtra("url", link6);
                intent6.putExtra("louc", louc6);
                intent6.putExtra("qid", "0");
                intent6.putExtra("userid", userid6);
                intent6.putExtra("typeroot", "0");
                startActivity(intent6);

                break;

            case "412": //点赞
                String replyid412 = obj.getString("replyid");

                DiariesReportLikeData data412 = new DiariesReportLikeData();
                data412.setIs_reply("1");
                data412.setId(replyid412);
                data412.setFlag("1");
                onEventClickListener.onLikeClick(data412);
                break;

            case "414": // 删除楼中帖 接口
                String id414 = obj.getString("id");
                String reply414 = obj.getString("reply");

                DiariesDeleteData data414 = new DiariesDeleteData();
                data414.setId(id414);
                data414.setReplystr(reply414);
                data414.setCommentsOrReply(1);
                onEventClickListener.onItemDeleteClick(data414);

                break;

            case "415": //显示全部
                headMap.clear();
                paramMap.clear();
                initWebVeiw();
                break;
            case "515":// 举报
                String replyid515 = obj.getString("replyid");
                String flag515 = obj.getString("flag");
                DiariesReportLikeData data515 = new DiariesReportLikeData();
                data515.setFlag("2");
                data515.setId(replyid515);

                if ("2".equals(flag515)) {
                    data515.setIs_reply("0");
                } else {
                    data515.setIs_reply("1");
                }

                onEventClickListener.onItemReportClick(data515);

                break;

            case "516": //单条日记
                Intent intent516 = new Intent();
                String id516 = obj.getString("id");
                String link516 = obj.getString("link");
                link516 = FinalConstant.baseUrl + FinalConstant.VER + link516;
                intent516.setClass(mContext, DiariesAndPostsActivity.class);
                intent516.putExtra("qid", id516);
                intent516.putExtra("url", link516);
                startActivity(intent516);
                break;
            case "5411":    //回复问题
                mFunctionManager.showShort("回复问题");
                break;
            case "5442":    // 删除单条帖子内容
                String id5442 = obj.getString("id");
                String reply5442 = obj.getString("reply");

                DiariesDeleteData data5442 = new DiariesDeleteData();
                data5442.setId(id5442);
                data5442.setReplystr(reply5442);
                data5442.setCommentsOrReply(0);
                onEventClickListener.onItemDeleteClick(data5442);
                break;

            case "6013":    //补充更新
                mFunctionManager.showShort("补充更新");
                break;

            case "6021":    //指示
                mFunctionManager.showShort("指示");
                break;


        }
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     */
    private void showDialogExitEdit(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    public interface OnEventClickListener {
        void onLikeClick(DiariesReportLikeData data);           //点赞接口回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除点击回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
