package com.module.commonview.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.GoodsGroupViewpagerAdapter;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.other.DiaryHeadSkuItemView;
import com.module.commonview.other.DiarySkuItemView;
import com.module.commonview.view.Expression;
import com.module.commonview.view.PostFlowLayoutGroup;
import com.module.commonview.view.UnderlinePageIndicator;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.view.MzRatingBar;
import com.module.home.controller.activity.SearchAllActivity668;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 日记详情
 * Created by 裴成浩 on 2018/6/14.
 */
public class DiaryDetailsFragment extends YMBaseFragment {

    @BindView(R.id.diary_list_other_goods_group1)
    ViewPager otherGoodsGroup1;
    @BindView(R.id.diary_list_other_goods_indicator1)
    UnderlinePageIndicator otherGoodsIndicator1;

    @BindView(R.id.tv_title_guanlian)
    TextView tv_title_guanlian;

    @BindView(R.id.diary_title_text)
    TextView mTitle;                              //日记标题
    @BindView(R.id.diary_details_centent)
    TextView mCentent;                              //日记文案
    @BindView(R.id.diary_details_centent_img)
    RecyclerView mRecylerList;                      //日记图片列表

    @BindView(R.id.diary_details_recommended_more_click)
    LinearLayout mTitleNumClick;                    //查看其余日记点击
    @BindView(R.id.diary_details_recommended_more_title)
    TextView mTitleNum;                             //日记图片列表

    @BindView(R.id.diary_list_adiary_tag_container)
    FlowLayout mDiaryFlowLayout;                         //标签

    @BindView(R.id.diary_list_hos_container)
    LinearLayout mDiaryHosContainer;                     //医院模块
    @BindView(R.id.diary_list_goods_hos_img)
    ImageView mDiaryHosImg;                              //医院logo
    @BindView(R.id.diary_list_goods_hos_name)
    TextView mDiaryHosName;                              //医院名称
    @BindView(R.id.diary_list_goods_hos_bar)
    MzRatingBar mDiaryHosBar;                            //医院评分
    @BindView(R.id.diary_list_goods_hos_case)
    TextView mDiaryHosCase;                              //医院案例
    @BindView(R.id.diary_list_goods_hos_address)
    TextView mDiaryHosAddress;                           //医院地址

    private final String TAG = "DiaryDetailsFragment";

    private DiaryListData mData;                        //日记数据
    private String mDiaryId;                            //日记id

    public static DiaryDetailsFragment newInstance(DiaryListData data, String id) {
        DiaryDetailsFragment fragment = new DiaryDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("id", id);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mData = getArguments().getParcelable("data");
        mDiaryId = getArguments().getString("id");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_diary_details;
    }

    @Override
    protected void initView(View view) {
        final List<DiaryTaoData> taoData = mData.getTaoData();    //下单商品数据
        DiaryHosDocBean hosDoc = mData.getHosDoc();         //服务的医生医院
//        DiarySkuItemView diarySkuItemView = new DiarySkuItemView(mContext, mDiaryId, "2");
        DiaryHeadSkuItemView diarySkuItemView = new DiaryHeadSkuItemView(mContext, mDiaryId, "2");
        mTitle.setText(mData.getTitle());
        //加载日记标签
        setTagData(hosDoc);

        //初始化服务医院
        if (hosDoc != null && !"0".equals(hosDoc.getHospital_id())) {
            initHosDocData(hosDoc);
        }

        GoodsGroupViewpagerAdapter adapter2 = new GoodsGroupViewpagerAdapter(diarySkuItemView.initTaoData(taoData, null));
        otherGoodsGroup1.setAdapter(adapter2);

        ViewGroup.LayoutParams layoutParams = otherGoodsGroup1.getLayoutParams();
        layoutParams.height = Utils.dip2px(110);
        otherGoodsGroup1.setLayoutParams(layoutParams);

        adapter2.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {

                String tao_id = mData.getTaoData().get(pos).getId();
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", mDiaryId);
                hashMap.put("to_page_type", "2");
                hashMap.put("to_page_id", tao_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHAREINFO_TO_TAO, "in_the_text"), hashMap, new ActivityTypeData(mData.getSelfPageType()));

                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                intent.putExtra("id", tao_id);
                if (mContext instanceof DiariesAndPostsActivity) {
                    intent.putExtra("source", ((DiariesAndPostsActivity) mContext).toTaoPageIdentifier());
                } else {
                    intent.putExtra("source", "0");
                }
                intent.putExtra("objid", mDiaryId);
                startActivity(intent);
            }
        });

//        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTitle.getLayoutParams();

        if (taoData.size() > 1) {
            otherGoodsIndicator1.setVisibility(View.VISIBLE);
            otherGoodsIndicator1.setViewPager(otherGoodsGroup1);
            tv_title_guanlian.setVisibility(View.VISIBLE);
//            layoutParams.topMargin = Utils.dip2px(20);
        } else if (taoData.size() > 0) {
            otherGoodsGroup1.setVisibility(View.VISIBLE);
            otherGoodsIndicator1.setVisibility(View.GONE);
            tv_title_guanlian.setVisibility(View.VISIBLE);
//            layoutParams.topMargin = Utils.dip2px(20);
        } else {
            otherGoodsGroup1.setVisibility(View.GONE);
            otherGoodsIndicator1.setVisibility(View.GONE);
            tv_title_guanlian.setVisibility(View.GONE);
//            layoutParams.topMargin = Utils.dip2px(5);
        }

//        mTitle.setLayoutParams(layoutParams);
    }

    /***
     * 初始化服务医院
     * @param mHosDocData
     */
    @SuppressLint("SetTextI18n")
    private void initHosDocData(DiaryHosDocBean mHosDocData) {
        final String hospitalId = mHosDocData.getHospital_id();
        String hospitalName = mHosDocData.getHospital_name();

        Log.e(TAG, "hospitalId=== " + hospitalId);
        Log.e(TAG, "hospitalName=== " + hospitalName);
        if (!TextUtils.isEmpty(hospitalId)) {
            mDiaryHosContainer.setVisibility(View.VISIBLE);
            mFunctionManager.setCircleImageSrc(mDiaryHosImg, mHosDocData.getHosimg());
            mDiaryHosName.setText(hospitalName);
            String people = mHosDocData.getPeople();
            if (!TextUtils.isEmpty(people)) {
                mDiaryHosCase.setText(people + "案例");
            }
            mDiaryHosAddress.setText(mHosDocData.getAddress());

            //评分
            mDiaryHosBar.setMax(100);
            int score;
            try {
                score = Integer.parseInt(mHosDocData.getComment_bili());
            } catch (Exception e) {
                score = 0;
                e.printStackTrace();
            }
            mDiaryHosBar.setProgress(score);    //评分

            //医院点击
            mDiaryHosContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(mContext, HosDetailActivity.class);
                    it.putExtra("hosid", hospitalId);
                    startActivity(it);
                }
            });
        } else {
            mDiaryHosContainer.setVisibility(View.GONE);
        }
    }

    /**
     * 加载日记标签
     *
     * @param hosDoc
     */
    private void setTagData(DiaryHosDocBean hosDoc) {
        List<DiaryTagList> tags = mData.getTag();
        //添加医生标签
        if (hosDoc != null) {
            String doctor_id = hosDoc.getDoctor_id();
            String doctor_name = hosDoc.getDoctor_name();
            String title = hosDoc.getTitle();
            Log.e(TAG, "doctor_id == " + doctor_id);
            Log.e(TAG, "doctor_name == " + doctor_name);
            Log.e(TAG, "title == " + title);

            if (!TextUtils.isEmpty(doctor_name) && !TextUtils.isEmpty(doctor_id)) {
                String docName;
                if (!TextUtils.isEmpty(title)) {
                    docName = doctor_name + " " + title;
                } else {
                    docName = doctor_name;
                }
                DiaryTagList diaryTagList = new DiaryTagList(doctor_id, hosDoc.getDoctor_url(), docName);
                tags.add(diaryTagList);
            }
        }

        if (tags.size() > 0) {
            mDiaryFlowLayout.setVisibility(View.VISIBLE);
            PostFlowLayoutGroup mPostFlowLayoutGroup = new PostFlowLayoutGroup(mContext, mDiaryFlowLayout, tags);
            mPostFlowLayoutGroup.setClickCallBack(new PostFlowLayoutGroup.ClickCallBack() {
                @Override
                public void onClick(View v, int pos, DiaryTagList data) {
                    if (!TextUtils.isEmpty(data.getUrl())) {
                        HashMap<String, String> event_params = data.getEvent_params();
                        if (event_params != null) {
                            event_params.put("id", mDiaryId);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.POSTINFO_TAG_CLICK, (pos + 1) + ""), event_params);
                            Intent it = new Intent(mContext, SearchAllActivity668.class);
                            it.putExtra("keys", data.getName());
                            it.putExtra("results", true);
                            it.putExtra("position", 1);
                            startActivity(it);
                        } else {
                            Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                            intent.putExtra("docId", data.getId());
                            intent.putExtra("docName", data.getName());
                            intent.putExtra("partId", "");
                            startActivity(intent);
                        }

                    }
                }
            });
        } else {
            mDiaryFlowLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initData(View view) {
        initDiaryData();                                                        //初始化日记本数据
    }

    /**
     * 初始化数据
     */
    private void initDiaryData() {

        //楼主剩余日记
        mTitleNum.setText(mData.getLocation().getTitle());

        //设置内容标题
//        mTitle.setText(mData.getTitle());

        try {
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(mData.getContent(), mContext, Utils.dip2px(12));
            mCentent.setText(stringBuilder);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //楼主剩余日记点击
        mTitleNumClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(mData.getLocation().getAppmurl(), "0", "0");
            }
        });
    }

}
