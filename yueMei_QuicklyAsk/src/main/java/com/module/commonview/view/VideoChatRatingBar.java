package com.module.commonview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

public class VideoChatRatingBar extends FrameLayout {

    private final Context mContext;
    private ProgressBar progressBar;
    public VideoChatRatingBar(Context context) {
        this(context, null);
    }

    public VideoChatRatingBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoChatRatingBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initView() {

        //进度条
        progressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setProgressDrawable(Utils.getLocalDrawable(mContext, R.drawable.video_chat_ratingbar));
        progressBar.setMax(100);

        //星级评分图
        ImageView imageView = new ImageView(mContext);
        imageView.setBackgroundResource(R.drawable.video_chat_ratingbar_bg);

        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(progressBar,lp);
        addView(imageView,lp);

        Log.e("","x == ");
    }

    /**
     * 设置进度
     */
    public void setProgressBar(int progress) {
        if (progressBar != null) {
            progressBar.setProgress(progress);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        if (x<0) x=0;
        if (x>getMeasuredWidth()) x = getMeasuredWidth();
        int progress = ((x*100/getMeasuredWidth())/10)*10;
        if (progress < 10){
            progress = 10;
        }
        if (progress >90){
            progress = 100;
        }
        Log.e("VideoChatRatingBar","progress =="+progress);
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                setProgressBar(progress);
                break;
            case MotionEvent.ACTION_MOVE:
                setProgressBar(progress);
                break;
            case MotionEvent.ACTION_UP:
                setProgressBar(progress);
                break;
        }
        return true;
    }
}
