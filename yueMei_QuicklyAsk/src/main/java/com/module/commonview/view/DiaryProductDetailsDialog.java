package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.module.commonview.PageJumpManager;
import com.module.commonview.adapter.ButtomDiaryDialogAdapter;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;

import org.xutils.common.util.DensityUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/11.
 */
public class DiaryProductDetailsDialog extends Dialog {

    private Context mContext;
    private List<DiaryTaoData> mDiaryTaoDatas;
    private String mDiaryId;
    private String mType;
    private String mSelfPageType;
    private String mId;

    public DiaryProductDetailsDialog(Context context, List<DiaryTaoData> diaryTaoDatas,String diaryId,String type,String selfPageType,String id) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mDiaryTaoDatas = diaryTaoDatas;
        this.mDiaryId = diaryId;
        this.mType = type;
        this.mSelfPageType = selfPageType;
        this.mId = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.pop_diary_button_dialog1, null, false);

        onClickListener(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);           //点击外部是否可以dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = DensityUtil.dip2px(428);
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        LinearLayout mClose = view.findViewById(R.id.pop_diary_button_close);
        ListView buttomList = view.findViewById(R.id.pop_diary_button_list);

        ButtomDiaryDialogAdapter buttomDialogAdapter = new ButtomDiaryDialogAdapter(mContext, mDiaryTaoDatas);
        buttomList.setAdapter(buttomDialogAdapter);

        //跳转设置
        buttomList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemCallBackListener.onItemClick(view, position);
                downDialog();
            }
        });

        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

        buttomDialogAdapter.setOnItemChatCallListener(new ButtomDiaryDialogAdapter.ItemChatCallListener() {
            @Override
            public void onItemClick(View v, int pos) {
                downDialog();
                String eventPos = "";
                final String hos_userid = mDiaryTaoDatas.get(pos).getHos_userid();
                final String taoid = mDiaryTaoDatas.get(pos).getId();
                final String title = mDiaryTaoDatas.get(pos).getTitle();
                final String price_discount = mDiaryTaoDatas.get(pos).getPrice_discount();
                final String member_price = mDiaryTaoDatas.get(pos).getMember_price();
                final String list_cover_image = mDiaryTaoDatas.get(pos).getList_cover_image();

//                HashMap<String,String> hashMap = new HashMap<>();
//                hashMap.put("id",mId);
//                hashMap.put("hos_id",mDiaryTaoDatas.get(pos).getHospital_id());
//                hashMap.put("doc_id",mDiaryTaoDatas.get(pos).getDoc_id());
//                hashMap.put("tao_id",mDiaryTaoDatas.get(pos).get)
//                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL,"text_related_chat",mDiaryTaoDatas.get(pos).getHospital_id()), new ActivityTypeData(mSelfPageType));
                if (Utils.noLoginChat()) {
                    toChatActivity(pos, eventPos, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        toChatActivity(pos, eventPos, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
                    }
                }
            }
        });

    }

    private void toChatActivity(int pos, String eventPos, String hos_userid, String taoid, String title, String price_discount, String member_price, String list_cover_image) {
        switch (mType){
            case "1":
                eventPos = "diary_tao";
                break;
            case "2":
                eventPos = "share_tao";
                break;
            case "3":
                eventPos = "text_related_chat";
                break;
        }
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("215")
                .setYmId(taoid)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                .setEvent_name("chat_hospital")
                .setEvent_pos(eventPos)
                .setHos_id(mDiaryTaoDatas.get(pos).getHospital_id())
                .setDoc_id(mDiaryTaoDatas.get(pos).getDoc_id())
                .setTao_id(mDiaryTaoDatas.get(pos).getId())
                .setEvent_others(mDiaryTaoDatas.get(pos).getHospital_id())
                .setId(mDiaryId)
                .setReferrer("17")
                .setType("2")
                .build();
        Utils.chatTongJi(mContext, tongJiParams);
    }


    /**
     * 关闭
     */
    private void downDialog() {
        dismiss();
    }

    //item点击回调
    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }

}
