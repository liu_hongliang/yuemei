package com.module.commonview.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;

import com.quicklyack.emoji.Expressions;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * 不可以上传图片的评论弹层
 * Created by 裴成浩 on 2018/9/12.
 */
public class CommentDialogView extends Dialog {

    private Activity mContext;
    private RelativeLayout queDetail;
    private RelativeLayout queCancel1;
    private Button queSumbit1;
    private EditText queContent1;
    private LinearLayout insertExpression;

    private LinearLayout biaoqingContentLy;
    private ImageButton closeImBt;
    private ViewPager viewPager;
    private ImageView page0;
    private ImageView page1;

    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;

    private ArrayList<GridView> grids;
    private GridView gView1;
    private GridView gView2;
    private String TAG = "CommentDialogView";

    public CommentDialogView(@NonNull Activity context) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.comment_pop_buttom_dialog, null, false);
        ButterKnife.bind(this, view);

        findView(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);    //点击外部不可dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
    }

    private void findView(View view) {
        queDetail = view.findViewById(R.id.que_detail_input1_rly);
        queCancel1 = view.findViewById(R.id.que_web_cancel1_rly);
        queSumbit1 = view.findViewById(R.id.que_web_sumbit1_bt);
        queContent1 = view.findViewById(R.id.que_web_input_content1_et);
        insertExpression = view.findViewById(R.id.ll_insert_expression);

        //表情键盘
        biaoqingContentLy = view.findViewById(R.id.biaoqing_shuru_content_ly1);
        closeImBt = view.findViewById(R.id.colse_biaoqingjian_bt);
        viewPager = view.findViewById(R.id.viewpager);
        page0 = view.findViewById(R.id.page0_select);
        page1 = view.findViewById(R.id.page1_select);

        initExpression();
        initBottHttp();

        queContent1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = queContent1.getText().toString().length();
                if(length > 100){
                    ViewInject.toast("内容不能大于100字");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = queContent1.getText().toString().length();
                if(length>=5 && length <= 100){
                    queSumbit1.setEnabled(true);
                    queSumbit1.setBackgroundResource(R.drawable.btn_submit_orange12x);
                }else {
                    queSumbit1.setEnabled(false);
                    queSumbit1.setBackgroundResource(R.drawable.btn_submit_gray12x);
                }
            }
        });

        /**
         * 表情键盘显示
         */
        insertExpression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(),
                            0);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        //关闭表情键盘
        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    /**
     * 初始化表情键盘
     */
    private void initExpression() {

        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        grids = new ArrayList<GridView>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems,
                R.layout.singleexpression, new String[]{"image"},
                new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    queContent1.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(mContext.getResources(),
                            expressionImages[arg2 % expressionImages.length]);

                    bitmap = Utils.zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(
                            expressionImageNames[arg2].substring(1,
                                    expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0,
                            expressionImageNames[arg2].length() - 2,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    queContent1.append(spannableString);

                }

            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);
        // viewPager.setAdapter();

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }


    /**
     * 问题解答
     */
    public void initBottHttp() {
        queDetail.setVisibility(View.VISIBLE);
        queContent1.requestFocus();

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

        /**
         * 回答问题框提交按钮
         */
        queSumbit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSubmitClickListener != null) {
                    onSubmitClickListener.onSubmitClick(queContent1.getText().toString());
                    coleAnswerBox();
                }
            }
        });

        /**
         * 回答问题框关闭按钮
         */
        queCancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coleAnswerBox();
            }
        });
    }

    /**
     * 关闭解答框
     */
    private void coleAnswerBox() {
        queDetail.setVisibility(View.GONE);
        queContent1.setText("");
        View view = getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputmanger = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanger.hideSoftInputFromWindow(
                    view.getWindowToken(), 0);
        }
    }


    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(mContext.getResources().getDrawable(
                            R.drawable.page_focused));
                    page1.setImageDrawable(mContext.getResources().getDrawable(
                            R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(mContext.getResources().getDrawable(
                            R.drawable.page_focused));
                    page0.setImageDrawable(mContext.getResources().getDrawable(
                            R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext,
                            listItems, R.layout.singleexpression,
                            new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1,
                                                int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                queContent1.onKeyDown(KeyEvent.KEYCODE_DEL,
                                        event); // 抛给系统处理了
                            } else {
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(
                                        mContext.getResources(), expressionImages1[pos
                                                % expressionImages1.length]);
                                bitmap = Utils.zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext,
                                        bitmap);
                                SpannableString spannableString = new SpannableString(
                                        expressionImageNames1[pos]
                                                .substring(1,
                                                        expressionImageNames1[pos]
                                                                .length() - 1));

                                spannableString.setSpan(imageSpan, 0,
                                        expressionImageNames1[pos].length() - 2,
                                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                queContent1.append(spannableString);
                            }
                        }
                    });
                    break;
            }
        }
    }

    /**
     * 显示
     */
    public void showDialog() {
        Log.e(TAG, "showDialog......");
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        show();
    }

    //提交接口回调
    public interface OnSubmitClickListener {
        void onSubmitClick(String content);
    }

    private OnSubmitClickListener onSubmitClickListener;

    public void setOnSubmitClickListener(OnSubmitClickListener onSubmitClickListener) {
        this.onSubmitClickListener = onSubmitClickListener;
    }
}
