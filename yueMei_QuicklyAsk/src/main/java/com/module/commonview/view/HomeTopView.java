package com.module.commonview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 首页置顶View
 * Created by 裴成浩 on 2019/3/19
 */
public class HomeTopView extends FrameLayout {
    private final String TAG = "HomeTopView";
    private final Context mContext;
    private final int mChildWidth = Utils.dip2px(40);
    private final int mChildHeight = Utils.dip2px(40);

    private ImageView mHomeImage;
    private ImageView mTopImage;
    private int viewWidth;
    private int viewHeight;
    private boolean mIsTabTop = false;          //true:Tab置顶，false:Tab未置顶

    public HomeTopView(Context context) {
        this(context, null);
    }

    public HomeTopView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeTopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView(context);
    }

    /**
     * 自定义初始化布局
     */
    private void initView(Context context) {
        mHomeImage = new ImageView(mContext);
        mTopImage = new ImageView(mContext);
        LayoutParams lp = new LayoutParams(mChildWidth, mChildHeight);
        lp.gravity = Gravity.CENTER;
        addView(mHomeImage, lp);
        addView(mTopImage, lp);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = getMeasuredWidth();
        viewHeight = getMeasuredHeight();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mTopImage.layout((viewWidth - mChildWidth) / 2, viewHeight + (viewHeight - mChildHeight) / 2, viewWidth - (viewWidth - mChildWidth) / 2, viewHeight + mChildHeight + (viewHeight - mChildHeight) / 2);
    }

    /**
     * 判断是否置顶
     *
     * @param isTabTop true:Tab置顶，false:Tab未置顶
     */
    public void isTopHome(boolean isTabTop) {
        Log.e(TAG, "viewHeight == " + viewHeight);
        mIsTabTop = isTabTop;
        if (mIsTabTop) {
            setScrollY(viewHeight);
        } else {
            setScrollY(0);
        }
    }

    /**
     * 获取是否是完全显示折叠状态
     *
     * @return true:Tab置顶，false:Tab未置顶
     */
    public boolean isTabTop() {
        return mIsTabTop;
    }

    /**
     * 设置图片
     */
    public void setImgResource() {
        mHomeImage.setBackgroundResource(R.drawable.tab_bt_top_home);
        mTopImage.setBackgroundResource(R.drawable.home_topper);
    }
}
