package com.module.commonview.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.Cfg;

public class SkuProjectDetailPop extends PopupWindow {

    public SkuProjectDetailPop(Context context, TaoDetailBean mTaoDetailBean) {
        final View view = View.inflate(context, R.layout.sku_project_detail_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        setHeight((int) (windowH * 0.55));
        setFocusable(true);
        setContentView(view);
        update();
        LinearLayout mSkuProjectVisorgone = view.findViewById(R.id.sku_project_visorgone);
        TextView mSkuProjectContent = view.findViewById(R.id.sku_project_content);
        LinearLayout mSkuCostVisorgone = view.findViewById(R.id.sku_cost_visorgone);
        TextView mSkuCostContent = view.findViewById(R.id.sku_cost_content);
        LinearLayout mSkuTimeVisorgone = view.findViewById(R.id.sku_time_visorgone);
        TextView mSkuTimeContent = view.findViewById(R.id.sku_time_content);
        LinearLayout mSkuTipVisorgone = view.findViewById(R.id.sku_tip_visorgone);
        TextView mSkuTipContent = view.findViewById(R.id.sku_tip_content);
        LinearLayout mSkuprojectClose = view.findViewById(R.id.sku_project_close);
        Button mSkuprojectBtn = view.findViewById(R.id.sku_project_btn);

        if (mTaoDetailBean != null){
            TaoDetailBean.IteminfoBean iteminfo = mTaoDetailBean.getIteminfo();//项目
            if (iteminfo != null) {
                mSkuProjectVisorgone.setVisibility(View.VISIBLE);
                mSkuProjectContent.setText(iteminfo.getTitle());
            } else {
                mSkuProjectVisorgone.setVisibility(View.GONE);
            }
            String fee_explain = mTaoDetailBean.getFee_explain();//费用
            if (!TextUtils.isEmpty(fee_explain.trim())) {
                mSkuCostVisorgone.setVisibility(View.VISIBLE);
                mSkuCostContent.setText(fee_explain);
            } else {
                mSkuCostVisorgone.setVisibility(View.GONE);
            }

            String use_time = mTaoDetailBean.getUse_time();//时间
            if (!TextUtils.isEmpty(use_time.trim())) {
                mSkuTimeVisorgone.setVisibility(View.VISIBLE);
                mSkuTimeContent.setText(use_time);
            } else {
                mSkuTimeVisorgone.setVisibility(View.GONE);
            }
            String warm_tips = mTaoDetailBean.getWarm_tips();//提示
            if (!TextUtils.isEmpty(warm_tips.trim())) {
                mSkuTipVisorgone.setVisibility(View.VISIBLE);
                mSkuTipContent.setText(warm_tips);
            } else {
                mSkuTipVisorgone.setVisibility(View.GONE);
            }
        }

        mSkuprojectClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mSkuprojectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
