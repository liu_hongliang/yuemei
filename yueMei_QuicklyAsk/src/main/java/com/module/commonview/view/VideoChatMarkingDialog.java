package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import com.module.doctor.view.StaScoreBar;
import com.module.my.model.bean.ProjcetList;
import com.quicklyask.activity.R;

import java.util.ArrayList;

/**
 * 评分弹窗
 */
public class VideoChatMarkingDialog extends Dialog {


    public VideoChatMarkingDialog(@NonNull Context context) {
        super(context,  R.style.CustomDialog);

        setContentView(R.layout.video_chat_marking);

        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.AnimBottom);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);

        setCanceledOnTouchOutside(false);

        LinearLayout markingClose = findViewById(R.id.marking_close);
        VideoChatRatingBar scoreBar = findViewById(R.id.video_chat_ratingbar);
        RecyclerView markingList = findViewById(R.id.video_chat_marking_recycler);
        Button markingBtn = findViewById(R.id.marking_btn);


        ArrayList<ProjcetList> projcetLists = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ProjcetList projcetList = new ProjcetList();
            projcetList.setPostName("鼻部"+i);
            projcetList.setPostVal("¥"+(600+i));
            projcetLists.add(projcetList);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        markingList.setLayoutManager(linearLayoutManager);
        VideoChatMarkingAdapter videoChatMarkingAdapter = new VideoChatMarkingAdapter(R.layout.video_chat_marking_list, projcetLists);
        markingList.setAdapter(videoChatMarkingAdapter);

        markingClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
