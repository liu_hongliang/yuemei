package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.commonview.PageJumpManager;
import com.module.commonview.adapter.PostVideoImagePagerAdapter1;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryListData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.module.bean.PostOtherpic;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiVideoView;

import org.xutils.common.util.DensityUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 帖子页面上边视频图片轮播自定义控件2
 * Created by 裴成浩 on 2018/8/31.
 * <p>
 * <p>
 * <p>
 * leftView = LayoutInflater.from(this).inflate(R.layout.item_pager_left,null);
 * rightView = LayoutInflater.from(this).inflate(R.layout.item_pager_right,null);
 * pager.setRefreshView(leftView,rightView);
 * adapter = new FragAdapter(4,getSupportFragmentManager());
 * pager.setAdapter(adapter);
 * pager.setOnStretchListener(this);
 */
public class DiariesAndPostPicView1 extends RelativeLayout {

    private Context mContext;
    private TextView mTextView;
    private StretchPager mViewPager;
    private List<PostOtherpic> mDatas;
    private PostListData mPostListData;
    private DiaryListData mDiaryListData;
    private PostVideoImagePagerAdapter1 postVideoImagePagerAdapter;
    private String TAG = "DiariesAndPostPicView";
    private View rightView;

    private float startY;
    private float startX;
    // 记录viewPager是否拖拽的标记
    private boolean mIsVpDragger;
    private int mTouchSlop;

    String is_rongyun;
    String hos_userid;
    String ymaq_class;
    String ymaq_id;
    String event_name;
    String event_pos;
    String obj_type;
    String obj_id;
    ChatDataGuiJi guiji;
    String price;
    String image;
    String member_price;
    String url;
    String title;
    HashMap<String, String> event_params;

    public DiariesAndPostPicView1(Context context) {
        this(context, null);
    }

    public DiariesAndPostPicView1(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DiariesAndPostPicView1(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        initView();
    }

    private void initView() {
        View view = View.inflate(mContext, R.layout.diaries_post_pic_view1, this);
        mViewPager = view.findViewById(R.id.post_details_viodeo_image);
        mTextView = view.findViewById(R.id.post_details_viodeo_image_index);
        rightView = LayoutInflater.from(mContext).inflate(R.layout.item_pager_right, null);
        mViewPager.setRefreshView(null, rightView);
    }


    /**
     * 设置数据
     *
     * @param data
     */
    @SuppressLint("SetTextI18n")
    public void setData(List<PostOtherpic> data, final DisplayMetrics metric, PostListData postListData, DiaryListData diaryListData) {
        if (data.size() > 0) {
            this.mPostListData = postListData;
            this.mDiaryListData = diaryListData;
            this.mDatas = data;

            //设置下标
            if (mDatas.size() > 1) {
                mTextView.setText("1/" + mDatas.size());
                mTextView.setVisibility(VISIBLE);
            } else {
                mTextView.setVisibility(GONE);
            }

//            if(postListData.getTaoDataList() != null && !postListData.getTaoDataList().isEmpty()){
//
//            }

            //设置适配器
            PostOtherpic postOtherpic = mDatas.get(0);
            int width = Integer.parseInt(postOtherpic.getWidth());
            int height = Integer.parseInt(postOtherpic.getHeight());
            Log.e(TAG, "width === " + width);
            Log.e(TAG, "height === " + height);
            if (width != 0 && height != 0) {
                setAdapter((metric.widthPixels * height) / width, metric);
            } else {
                Glide.with(mContext).load(postOtherpic.getImg()).into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        int intrinsicWidth = resource.getIntrinsicWidth();
                        int intrinsicHeight = resource.getIntrinsicHeight();

                        setAdapter((metric.widthPixels * intrinsicHeight) / intrinsicWidth, metric);
                    }
                });
            }
        }
    }

    private void setAdapter(final int defaultheight, DisplayMetrics metric) {
        if (postVideoImagePagerAdapter == null) {

            //第一个item的高度
            int maxHeight = (metric.heightPixels / 3) * 2;
            ViewGroup.LayoutParams params = getLayoutParams();
            params.height = defaultheight > maxHeight ? maxHeight : defaultheight;
            setLayoutParams(params);

            Log.e(TAG, "defaultheight === " + defaultheight);

            postVideoImagePagerAdapter = new PostVideoImagePagerAdapter1(mContext, mDatas, metric);
            mViewPager.setAdapter(postVideoImagePagerAdapter);
            mViewPager.setOnStretchListener(new StretchPager.OnStretchListener() {
                @Override
                public void onScrolled(int direction, int distance) {

                }

                @Override
                public void onRefresh(int direction, int distance) {
                    //右滑 大于等于百分之30出发
                    if (StretchPager.STRETCH_RIGHT == direction && Math.abs(distance) >= DensityUtil.getScreenWidth() * 0.3) {
                        if (Utils.noLoginChat()){
                                if (mPostListData != null && mPostListData.getChatData() != null) {
                                    is_rongyun = mPostListData.getChatData().getIs_rongyun();
                                    hos_userid = mPostListData.getChatData().getHos_userid();
                                    ymaq_class = mPostListData.getChatData().getYmaq_class();
                                    ymaq_id = mPostListData.getChatData().getYmaq_id();
                                    event_name = mPostListData.getChatData().getEvent_name();
                                    event_pos = mPostListData.getChatData().getEvent_pos();
                                    obj_type = mPostListData.getChatData().getObj_type();
                                    obj_id = mPostListData.getChatData().getObj_id();
                                    guiji = mPostListData.getChatData().getGuiji();
                                    price = guiji.getPrice();
                                    image = guiji.getImage();
                                    member_price = guiji.getMember_price();
                                    url = guiji.getUrl();
                                    title = guiji.getTitle();
                                    event_params = mPostListData.getChatData().getEvent_params();
                                } else {
                                    is_rongyun = mDiaryListData.getChatData().getIs_rongyun();
                                    hos_userid = mDiaryListData.getChatData().getHos_userid();
                                    ymaq_class = mDiaryListData.getChatData().getYmaq_class();
                                    ymaq_id = mDiaryListData.getChatData().getYmaq_id();
                                    event_name = mDiaryListData.getChatData().getEvent_name();
                                    event_pos = mDiaryListData.getChatData().getEvent_pos();
                                    obj_type = mDiaryListData.getChatData().getObj_type();
                                    obj_id = mDiaryListData.getChatData().getObj_id();
                                    guiji = mDiaryListData.getChatData().getGuiji();
                                    price = guiji.getPrice();
                                    image = guiji.getImage();
                                    member_price = guiji.getMember_price();
                                    url = guiji.getUrl();
                                    title = guiji.getTitle();
                                    event_params = mDiaryListData.getChatData().getEvent_params();
                                }
                                if ("3".equals(is_rongyun)) {
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
                                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                            .setDirectId(hos_userid)
                                            .setObjId(obj_id)
                                            .setObjType(obj_type)
                                            .setYmClass(ymaq_class)
                                            .setYmId(ymaq_id)
                                            .setPrice(price)
                                            .setMemberPrice(member_price)
                                            .setImg(image)
                                            .setUrl(url)
                                            .setTitle(title)
                                            .build();
                                    new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                                } else {
                                    Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
                                }
                        }else {
                            if (Utils.isLoginAndBind(mContext)) {
                                if (mPostListData != null && mPostListData.getChatData() != null) {
                                    is_rongyun = mPostListData.getChatData().getIs_rongyun();
                                    hos_userid = mPostListData.getChatData().getHos_userid();
                                    ymaq_class = mPostListData.getChatData().getYmaq_class();
                                    ymaq_id = mPostListData.getChatData().getYmaq_id();
                                    event_name = mPostListData.getChatData().getEvent_name();
                                    event_pos = mPostListData.getChatData().getEvent_pos();
                                    obj_type = mPostListData.getChatData().getObj_type();
                                    obj_id = mPostListData.getChatData().getObj_id();
                                    guiji = mPostListData.getChatData().getGuiji();
                                    price = guiji.getPrice();
                                    image = guiji.getImage();
                                    member_price = guiji.getMember_price();
                                    url = guiji.getUrl();
                                    title = guiji.getTitle();
                                    event_params = mPostListData.getChatData().getEvent_params();
                                } else {
                                    is_rongyun = mDiaryListData.getChatData().getIs_rongyun();
                                    hos_userid = mDiaryListData.getChatData().getHos_userid();
                                    ymaq_class = mDiaryListData.getChatData().getYmaq_class();
                                    ymaq_id = mDiaryListData.getChatData().getYmaq_id();
                                    event_name = mDiaryListData.getChatData().getEvent_name();
                                    event_pos = mDiaryListData.getChatData().getEvent_pos();
                                    obj_type = mDiaryListData.getChatData().getObj_type();
                                    obj_id = mDiaryListData.getChatData().getObj_id();
                                    guiji = mDiaryListData.getChatData().getGuiji();
                                    price = guiji.getPrice();
                                    image = guiji.getImage();
                                    member_price = guiji.getMember_price();
                                    url = guiji.getUrl();
                                    title = guiji.getTitle();
                                    event_params = mDiaryListData.getChatData().getEvent_params();
                                }
                                if ("3".equals(is_rongyun)) {
                                    YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
                                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                            .setDirectId(hos_userid)
                                            .setObjId(obj_id)
                                            .setObjType(obj_type)
                                            .setYmClass(ymaq_class)
                                            .setYmId(ymaq_id)
                                            .setPrice(price)
                                            .setMemberPrice(member_price)
                                            .setImg(image)
                                            .setUrl(url)
                                            .setTitle(title)
                                            .build();
                                    new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                                } else {
                                    Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }
                }

                @Override
                public void onRelease(int direction) {
                }
            });

            final int[] imgheights = postVideoImagePagerAdapter.getImgheights();

            //设置滑动监听
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                /**
                 * 滑动过程
                 * @param position
                 * @param positionOffset
                 * @param positionOffsetPixels
                 */
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    if (position == imgheights.length - 1) {
                        return;
                    }

                    //计算ViewPager现在应该的高度,heights[]表示页面高度的数组。
                    int height = (int) ((imgheights[position] == 0 ? defaultheight : imgheights[position]) * (1 - positionOffset) + (imgheights[position + 1] == 0 ? defaultheight : imgheights[position + 1]) * positionOffset);

                    Log.e(TAG, "height === " + height);
                    //为ViewPager设置高度
                    ViewGroup.LayoutParams params = getLayoutParams();
                    params.height = height;
                    setLayoutParams(params);
                }

                /**
                 * 选中时，选中的是第几个
                 * @param position
                 */
                @SuppressLint("SetTextI18n")
                @Override
                public void onPageSelected(int position) {
                    List<PostOtherpic> data = postVideoImagePagerAdapter.getData();
                    mTextView.setText((position + 1) + "/" + data.size());

                    if (mTextView.getVisibility() == View.GONE) {
                        mTextView.setVisibility(VISIBLE);
                    }

                    //如果数据大于0，且第一个是视频
                    if (data.size() > 0 && "1".equals(data.get(0).getIs_video())) {

                        YueMeiVideoView mVideoView = getYueMeiVideoView();

                        Log.e(TAG, "mVideoView === " + mVideoView);
                        if (mVideoView != null) {
                            //如果当前选中的不是视频
                            if (position != 0) {
                                mVideoView.videoPausePlayer();
                            } else {
                                mVideoView.videoStartPlayer();
                            }
                        }
                    }
                }

                /**
                 * 滚动状态变化，开始滚动是1，停止滚动是0
                 * @param state
                 */
                @Override
                public void onPageScrollStateChanged(int state) {
                    Log.e(TAG, "onPageScrollStateChanged ---> state === " + state);
                }
            });

            //如果有视频，是视频放大
            postVideoImagePagerAdapter.setOnAdapterClickListener(new PostVideoImagePagerAdapter1.OnAdapterClickListener() {
                @Override
                public void onMaxVideoClick(String videoUrl, int currentPosition) {
                    if (onMaxVideoClickListener != null) {
                        onMaxVideoClickListener.onMaxVideoClick(videoUrl, currentPosition);
                    }
                }

                @Override
                public void onProgressBarStateClick(int visibility) {
                    if (visibility == View.VISIBLE) {
                        if (mViewPager.getCurrentItem() == 0 && postVideoImagePagerAdapter.getData().size() > 0 && "1".equals(postVideoImagePagerAdapter.getData().get(0).getIs_video())) {
                            mTextView.setVisibility(GONE);
                        } else {
                            mTextView.setVisibility(VISIBLE);
                        }
                    } else {
                        mTextView.setVisibility(VISIBLE);
                    }
                }
            });
        }
    }

    /**
     * 获取视频播放器
     *
     * @return
     */
    @Nullable
    public YueMeiVideoView getYueMeiVideoView() {
        if (postVideoImagePagerAdapter != null) {
            return postVideoImagePagerAdapter.getVideoView();
        } else {
            return null;
        }
    }

//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        int action = ev.getAction();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                // 记录手指按下的位置
//                startY = ev.getY();
//                startX = ev.getX();
//                // 初始化标记
//                mIsVpDragger = false;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                // 如果viewpager正在拖拽中，那么不拦截它的事件，直接return false；
//                if (mIsVpDragger) {
//                    return false;
//                }
//
//                // 获取当前手指位置
//                float endY = ev.getY();
//                float endX = ev.getX();
//                float distanceX = Math.abs(endX - startX);
//                float distanceY = Math.abs(endY - startY);
//                // 如果X轴位移大于Y轴位移，那么将事件交给viewPager处理。
//                if (distanceX > mTouchSlop && distanceX > distanceY) {
//                    mIsVpDragger = true;
//                    return false;
//                }
//            case MotionEvent.ACTION_UP:
//            case MotionEvent.ACTION_CANCEL:
//                // 初始化标记
//                mIsVpDragger = false;
//                break;
//        }
//        // 如果是Y轴位移大于X轴，事件交给swipeRefreshLayout处理。
//        return super.onInterceptTouchEvent(ev);
//    }


    //放大视频的监听
    private OnMaxVideoClickListener onMaxVideoClickListener;

    public interface OnMaxVideoClickListener {
        void onMaxVideoClick(String videoUrl, int currentPosition);
    }

    public void setOnMaxVideoClickListener(OnMaxVideoClickListener onMaxVideoClickListener) {
        this.onMaxVideoClickListener = onMaxVideoClickListener;
    }
}
