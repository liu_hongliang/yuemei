package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Button;

import com.quicklyask.activity.R;

public class VideoChatComplainDialog extends Dialog {


    public final Button mCancelBtn;
    public final Button mSurelBtn;

    public VideoChatComplainDialog(@NonNull Context context) {
        super(context, R.style.CustomDialog);
        setContentView(R.layout.video_chat_complain_dialog);

        setCanceledOnTouchOutside(false);

        mCancelBtn = findViewById(R.id.complain_cancel_btn);
        mSurelBtn = findViewById(R.id.complain_sure_btn);
    }
}
