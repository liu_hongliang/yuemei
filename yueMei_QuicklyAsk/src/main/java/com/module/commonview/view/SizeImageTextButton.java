package com.module.commonview.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.module.community.view.CustomUtil;
import com.quicklyask.activity.R;

/**
 * 字体压在图片上的
 *
 * @author 裴成浩
 * @data 2019/10/22
 */
public class SizeImageTextButton extends View {

    private final String TAG = "SizeImageRadioButton";
    private final Bitmap mBitmap;
    private Paint mTextPaint;
    public static final String text = "颜究院";

    public SizeImageTextButton(Context context) {
        this(context, null);
    }

    public SizeImageTextButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SizeImageTextButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setStyle(Paint.Style.STROKE);
        mTextPaint.setColor(Color.RED);
        mTextPaint.setTextSize(9);


        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.tab_bt_city);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmap, 0, 0, mTextPaint);
        float[] textSize = CustomUtil.getTextSize(mTextPaint, text);
        canvas.drawText(text, getHeight() - textSize[0], (getWidth() - textSize[1]) / 2, mTextPaint);
    }
}