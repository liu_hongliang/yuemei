package com.module.commonview.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.commonview.module.api.SendEMSApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.doctor.model.bean.GroupRongIMData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取手机号 并验证
 * Created by 裴成浩 on 2017/12/18.
 */

public class PhonePopupWindows extends PopupWindow {

    private String kefu_nickName;
    private String skuType;
    private String hosUserid;
    private String taoId;
    private EditText phoneEt;
    private EditText codeEt;
    private RelativeLayout getCodeRly;
    private TextView getCodeTv;
    private Context mContext;
    private String uid;
    private String phoneStr;
    private PageJumpManager pageJumpManager;
    private GroupRongIMData groupRIMdata;
//    PhonePopupWindows(mContex,sku_type,hos_userid,kefu_nickName);
    @SuppressWarnings("deprecation")
    public PhonePopupWindows(Context mContext,String taoid,String sku_type, String hos_userid, String kefu_nickName) {
        this.mContext = mContext;
        this.kefu_nickName = kefu_nickName;
        pageJumpManager = new PageJumpManager(mContext);

        uid = Utils.getUid();
        skuType = sku_type;
        hosUserid = hos_userid;
        taoId = taoid;

        final View view = View.inflate(mContext, R.layout.pop_zixun_getphone, null);

        view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        // showAtLocation(parent, Gravity.BOTTOM, 0, 0);
        update();

        Button cancelBt = view.findViewById(R.id.cancel_bt);
        Button zixunBt = view.findViewById(R.id.zixun_bt);
        phoneEt = view.findViewById(R.id.no_pass_login_phone_et);
        codeEt = view.findViewById(R.id.no_pass_login_code_et);
        getCodeRly = view
                .findViewById(R.id.no_pass_yanzheng_code_rly);
        getCodeTv = view.findViewById(R.id.yanzheng_code_tv);

        zixunBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String phone = phoneEt.getText().toString();
                String codestr = codeEt.getText().toString();
                if (phone.length() > 0) {
                    if (ifPhoneNumber()) {
                        if (codestr.length() > 0) {
                            initCode1();
                        } else {
                            ViewInject.toast("请输入验证码");
                        }
                    } else {
                        ViewInject.toast("请输入正确的手机号");
                    }
                } else {
                    ViewInject.toast("请输入手机号");
                }
            }
        });

        getCodeRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String phone = phoneEt.getText().toString();
                if (phone.length() > 0) {
                    if (ifPhoneNumber()) {
                        sendEMS();
                    } else {
                        ViewInject.toast("请输入正确的手机号");
                    }
                } else {
                    ViewInject.toast("请输入手机号");
                }

            }
        });

        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private boolean ifPhoneNumber() {
        String textPhn = phoneEt.getText().toString();
        return Utils.isMobile(textPhn);
    }


    void initCode1() {
        String codeStr = codeEt.getText().toString();
        String phone = phoneEt.getText().toString();
        SecurityCodeApi securityCodeApi = new SecurityCodeApi();
        Map<String, Object> map = new HashMap<>();
        phoneStr = phone;
        map.put("uid", uid);
        map.put("phone", phone);
        map.put("code", codeStr);
        map.put("flag", "1");
        securityCodeApi.getCallBack(mContext, map, new BaseCallBackListener<String>() {

            @Override
            public void onSuccess(String s) {
                if (s != null) {
                    Cfg.saveStr(mContext, FinalConstant.UPHONE, phoneStr);
                    dismiss();

                    if ("1".equals(skuType)) {
                        if (null != groupRIMdata) {
                            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                    .setDirectId(hosUserid)
                                    .setObjId(taoId)
                                    .setObjType("2")
                                    .setYmClass("0")
                                    .setYmId("0")
                                    .build();
                            pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                        } else {
                            ViewInject.toast("未获取到聊天对象请稍后重试");
                        }


                    } else {
                        if (kefu_nickName.length() > 1) {
                            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                    .setDirectId(hosUserid)
                                    .setObjId(taoId)
                                    .setObjType("2")
                                    .setYmClass("0")
                                    .setYmId("0")
                                    .build();
                            pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                        }
                    }
                } else {
                    ViewInject.toast("请求错误");
                }
            }
        });
    }

    void sendEMS() {
        String phone = phoneEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("phone", phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    ViewInject.toast("请求成功");

                    getCodeRly.setClickable(false);
                    codeEt.requestFocus();

                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // getCodeRly
                            // .setBackgroundResource(R.drawable.biankuang_hui);
                            getCodeTv.setTextColor(mContext.getResources()
                                    .getColor(R.color.button_zi));
                            getCodeTv.setText("(" + millisUntilFinished
                                    / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            // getCodeRly
                            // .setBackgroundResource(R.drawable.shape_bian_ff5c77);
                            getCodeTv
                                    .setTextColor(mContext.getResources()
                                            .getColor(
                                                    R.color.button_bian_hong1));
                            getCodeRly.setClickable(true);
                            getCodeTv.setText("重发验证码");
                        }
                    }.start();
                } else {
                    ViewInject.toast(s.message);
                }

            }
        });
    }

}