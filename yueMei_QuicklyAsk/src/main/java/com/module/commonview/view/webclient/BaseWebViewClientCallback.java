package com.module.commonview.view.webclient;

/**
 * Created by 裴成浩 on 2018/1/9.
 */

public interface BaseWebViewClientCallback {
    void otherJump(String urlStr) throws Exception;
}
