package com.module.commonview.view;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

/**
 * Created by 裴成浩 on 2019/2/22
 */
public class CustomTabLayout extends TabLayout {
    private Context mContext;

    public CustomTabLayout(Context context) {
        this(context, null);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }


//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int dp10 = Utils.dip2px(mContext, 5);
//        LinearLayout mTabStrip = (LinearLayout) this.getChildAt(0);
//        try {
//            Field mTabs = TabLayout.class.getDeclaredField("mTabs");
//            mTabs.setAccessible(true);
//            ArrayList<Tab> tabs = (ArrayList<Tab>) mTabs.get(this);
//            for (int i = 0; i < mTabStrip.getChildCount(); i++) {
//                Tab tab = tabs.get(i);
//                Field mView = tab.getClass().getDeclaredField("mView");
//                mView.setAccessible(true);
//                Object tabView = mView.get(tab);
//                Field mTextView = mContext.getClassLoader().loadClass("android.support.design.widget.TabLayout$TabView").getDeclaredField("mTextView");
//                mTextView.setAccessible(true);
//                TextView textView = (TextView) mTextView.get(tabView);
//                float textWidth = textView.getPaint().measureText(textView.getText().toString());
//                View child = mTabStrip.getChildAt(i);
//                child.setPadding(0, 0, 0, 0);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) textWidth, LinearLayout.LayoutParams.MATCH_PARENT);
//                params.leftMargin = dp10;
//                params.rightMargin = dp10;
//                child.setLayoutParams(params);
//                child.invalidate();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
