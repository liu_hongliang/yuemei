package com.module.commonview.view;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.module.doctor.model.bean.PartAskData;
import com.module.doctor.model.bean.PartAskDataList;
import com.module.other.adapter.MakeLeftAdapter2;
import com.module.other.adapter.ProjectRightListAdapter2;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/2/5.
 */

public class BaseProjectPopupwindows2 extends PopupWindow {

    private String TAG = "BaseProject2";
    private final View mView;
    private Activity mActivity;
    private RecyclerView mLeftRecy;
    private RecyclerView mRightRecy;
    private RelativeLayout otherRly;
    private List<PartAskData> mData;
    private MakeLeftAdapter2 leftListAdapter2;
    private ProjectRightListAdapter2 rightListAdapter;
    private final int windowsHeight;
    private int mOnePos = 0;
    private String mTwoId;

    public BaseProjectPopupwindows2(Activity context, View v, List<PartAskData> data2) {
        this.mActivity = context;
        this.mView = v;
        this.mData = data2;

        DisplayMetrics metric = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsHeight = metric.heightPixels;

        initView();

        if (mData != null && mData.size() > 0) {
            setLeftView();
            setRightView(mOnePos);
        }
    }

    private void initView() {
        final View view = View.inflate(mActivity, R.layout.pop_show_project2, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_ins));

        mLeftRecy = view.findViewById(R.id.left_project_listview);
        mRightRecy = view.findViewById(R.id.right_project_listview);
        otherRly = view.findViewById(R.id.ly_content_ly2);

        //设置左侧列表布局管理器
        mLeftRecy.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        ((DefaultItemAnimator) mLeftRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果

        //设置右侧列表布局管理器
        mRightRecy.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        ((DefaultItemAnimator) mRightRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
        // 设置右侧列表自定义分割线
        mRightRecy.addItemDecoration(new android.support.v7.widget.DividerItemDecoration(mActivity, LinearLayoutManager.VERTICAL));


        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        otherRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }


    /**
     * 设置左边listView的数据
     */
    public void setLeftView() {
        //设置适配器
        Log.e(TAG, "mPos == " + mOnePos);
        leftListAdapter2 = new MakeLeftAdapter2(mActivity, mData, mOnePos, Utils.dip2px(mActivity, 41));
        mLeftRecy.setAdapter(leftListAdapter2);

        // 设置点击某条的监听
        leftListAdapter2.setOnItemClickListener(new MakeLeftAdapter2.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (mOnePos != pos) {
                    int typePos = mOnePos;         //之前选中的

                    mOnePos = pos;

                    leftListAdapter2.setmPos(mOnePos);
                    leftListAdapter2.notifyItemChanged(mOnePos);

                    leftListAdapter2.notifyItemChanged(typePos);

                    mLeftRecy.smoothScrollToPosition(mOnePos);
                    setRightView(mOnePos);
                }
            }
        });
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(windowsHeight - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }

    /**
     * 设置右边listView的数据
     *
     * @param onePos
     */
    public void setRightView(int onePos) {

        //设置适配器
        rightListAdapter = new ProjectRightListAdapter2(mActivity, mData, onePos, mTwoId);
        mRightRecy.setAdapter(rightListAdapter);
        rightListAdapter.setOnItemClickListener(new ProjectRightListAdapter2.OnItemClickListener() {
            @Override
            public void onItemClick(View view, PartAskData oneData, PartAskDataList twoData) {
                mTwoId = twoData.get_id();
                rightListAdapter.setmTwoPos(mTwoId);
                if (!TextUtils.isEmpty(twoData.get_id()) && !"0".equals(oneData.get_id()) && !twoData.get_id().equals(oneData.get_id())) {
                    Log.e(TAG, "twoData.get_id() == " + twoData.get_id());
                    Log.e(TAG, "twoData.getName() == " + twoData.getName());
                    onItemSelectedClickListener.onItemSelectedClick(twoData.get_id(), twoData.getName());
                } else {
                    Log.e(TAG, "oneData.get_id() == " + oneData.get_id());
                    Log.e(TAG, "oneData.getName() == " + oneData.getName());
                    onItemSelectedClickListener.onItemSelectedClick(oneData.get_id(), oneData.getName());
                }
                dismiss();
            }
        });
    }

    public int getmOnePos() {
        return mOnePos;
    }

    public List<PartAskData> getmData() {
        return mData;
    }

    public void setmData(List<PartAskData> mData) {
        this.mData = mData;
    }

    private OnItemSelectedClickListener onItemSelectedClickListener;

    public interface OnItemSelectedClickListener {
        void onItemSelectedClick(String id, String name);
    }

    public void setOnItemSelectedClickListener(OnItemSelectedClickListener onItemSelectedClickListener) {
        Log.e("null", "onItemSelectedClickListener222 == " + onItemSelectedClickListener);
        this.onItemSelectedClickListener = onItemSelectedClickListener;
    }

}
