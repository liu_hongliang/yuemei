package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.my.model.bean.ProjcetList;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

public class VideoChatProjectSelectAdapter extends BaseQuickAdapter<ProjcetList, BaseViewHolder> {

    private List<ProjcetList> data;
    private final int windowsWight;
    public VideoChatProjectSelectAdapter(int layoutResId, @Nullable List<ProjcetList> data) {
        super(layoutResId, data);
        this.data = data;
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProjcetList item) {
        TextView textView= helper.getView(R.id.item_sort_screen_item_text);
        textView.setText(item.getPostName());
        ViewGroup.LayoutParams layoutParams=textView.getLayoutParams();
        layoutParams.width = (windowsWight - Utils.dip2px(60)) / 3;
        textView.setLayoutParams(layoutParams);
        if (item.isIs_selected()) {
            textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff5c77));
            textView.setBackgroundResource(R.drawable.home_diary_tab);
        } else {
            textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            textView.setBackgroundResource(R.drawable.home_diary_tab2);
        }
    }


    @NonNull
    @Override
    public List<ProjcetList> getData() {
        return data;
    }

    /**
     *  * 单选
     *  * @param postion
     *  
     */
    public void singleSelect(int postion) {
        for (int i = 0; i < data.size(); i++) {
            ProjcetList projcetList = data.get(i);
            if (postion == i){
                projcetList.setIs_selected(true);
            }else {
                projcetList.setIs_selected(false);
            }
        }
        notifyDataSetChanged();
    }
}
