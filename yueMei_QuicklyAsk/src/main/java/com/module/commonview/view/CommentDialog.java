package com.module.commonview.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.commonview.adapter.CommentsRecyclerAdapter2;
import com.module.commonview.module.api.CommentsListApi;
import com.module.commonview.module.api.DeleteBBsApi;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryReplyInfo;
import com.module.commonview.module.bean.DiaryReplyLisListTao;
import com.module.commonview.module.bean.DiaryReplyList2;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.module.bean.DiaryReplyListPic;
import com.module.commonview.module.bean.DiaryReplyListTao;
import com.module.commonview.module.bean.DiaryReplyListUserdata;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.event.PostMsgEvent;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import org.greenrobot.eventbus.EventBus;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;

//评论列表弹窗
public class CommentDialog extends Dialog {
    RecyclerView buttomList;
    SmartRefreshLayout diary_comments_list_refresh;
    TextView tv_title;
    private Context mContext;
    private String mDiaryId;
    private String id;
    private String mAskorshare;
    private String pId;
    private int mNumber;                                //评论数
    private CommentsListApi mCommentsListApi;           //日记评论列表请求类
    private DeleteBBsApi mDeleteBBsApi;                 //删除评论
    private ZanOrJuBaoApi mPointLikeApi;
    private CommentsRecyclerAdapter2 mCommentsRecyclerAdapter2;
    private DiaryCommentDialogView mDiaryCommentDialogView;
//    private SumbitPhotoData sumbitPhotoData;
    private EditExitDialog editDialog;
    private int mPage = 1;
    private DiaryReplyInfo diaryReplyInfo;
    private List<DiaryReplyList2> diaryReplyLists;
    private List<DiaryReplyList2> NotShowHotLists;
    private int delecount = 0;

    //返回出去当前输入框实例
    public DiaryCommentDialogView getDiaryCommentDialogView() {
        return mDiaryCommentDialogView;
    }

    public CommentDialog(Context context, String diaryId, String id, String askorshare, String pid, int number) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mDiaryId = diaryId;
        this.id = id;
        this.mAskorshare = askorshare;
        this.pId = pid;
        this.mNumber = number;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.pop_comment_button_dialog, null, false);
        onClickListener(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);           //点击外部是否可以dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = DensityUtil.getScreenHeight() / 4 * 3;
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        mCommentsListApi = new CommentsListApi();
        mPointLikeApi = new ZanOrJuBaoApi();
        mDeleteBBsApi = new DeleteBBsApi();
//        sumbitPhotoData = new SumbitPhotoData();

        diaryReplyLists = new ArrayList<DiaryReplyList2>();
        NotShowHotLists = new ArrayList<DiaryReplyList2>();

        LinearLayout mClose = view.findViewById(R.id.pop_diary_button_close);
        buttomList = view.findViewById(R.id.pop_diary_button_list);
        diary_comments_list_refresh = view.findViewById(R.id.diary_comments_list_refresh);
        tv_title = view.findViewById(R.id.tv_title);
        YMLoadMore diary_comments_list_refresh_more = view.findViewById(R.id.diary_comments_list_refresh_more);
        FrameLayout activity_diaries_and_posts_bottom_click = view.findViewById(R.id.activity_diaries_and_posts_bottom_click);

        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        buttomList.setLayoutManager(scrollLinearLayoutManager);
        diary_comments_list_refresh.setEnableRefresh(false);
        tv_title.setText("全部" + mNumber + "条评论");
        //回复主贴
        activity_diaries_and_posts_bottom_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"0".equals(mAskorshare)) {
                    startComments();
                } else {
                    if (id.equals(Utils.getUid())) {
                        startComments();
                    } else {
                        MyToast.makeTextToast2(mContext, "问题贴仅限医生和提问者评论", MyToast.SHOW_TIME).show();
                    }

                }
            }
        });

        //上拉加载更多
        diary_comments_list_refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadCommentsList();
            }
        });

        loadCommentsList();

        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

    }

    /**
     * 加载评论列表
     */
    private void loadCommentsList() {
        mCommentsListApi.addData("postuserid", id);                     //帖子用户id
        mCommentsListApi.addData("q_id", mDiaryId);                     //帖子id
        mCommentsListApi.addData("askorshare", mAskorshare);            //帖子类型
        mCommentsListApi.addData("p_id", pId);
        mCommentsListApi.addData("page", mPage + "");
        //最后一条id
        if (diaryReplyInfo == null || TextUtils.isEmpty(diaryReplyInfo.getNew_reply_last_id())) {
            mCommentsListApi.addData("last_id", "0");
        } else {
            mCommentsListApi.addData("last_id", diaryReplyInfo.getNew_reply_last_id());
        }
        mCommentsListApi.getCallBack(mContext, mCommentsListApi.getHashMap(), new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {
                diary_comments_list_refresh.finishRefresh();
                diaryReplyInfo = (DiaryReplyInfo) o;
                if (mPage == 1) {
                    //取我的评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getMy_reply() != null && diaryReplyInfo.getMy_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getMy_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getMy_reply().get(i).setType("0");
                            diaryReplyLists.add(diaryReplyInfo.getMy_reply().get(i));
                        }
                    }
                    //取最热评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getHot_reply() != null && diaryReplyInfo.getHot_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getHot_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getHot_reply().get(i).setType("1");
                            diaryReplyLists.add(diaryReplyInfo.getHot_reply().get(i));
                        }
                        //为了取出热门未展示部分数据
                        if (diaryReplyInfo.getNew_reply().size() > 3) {
                            for (int i = 3; i < diaryReplyInfo.getHot_reply().size(); i++) {
                                NotShowHotLists.add(diaryReplyInfo.getHot_reply().get(i));
                            }
                        }
                    }
                    //取最新评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getNew_reply() != null && diaryReplyInfo.getNew_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getNew_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getNew_reply().get(i).setType("2");
                            diaryReplyLists.add(diaryReplyInfo.getNew_reply().get(i));
                        }
                    }
                    if (diaryReplyLists.size() < Integer.parseInt(diaryReplyInfo.getReply_page_offect())) {
                        diary_comments_list_refresh.finishLoadMoreWithNoMoreData();
                    } else {
                        diary_comments_list_refresh.finishLoadMore();
                    }
                    initCommentsList(diaryReplyLists);
                } else {
                    List<DiaryReplyList2> diaryReplyListsNew = new ArrayList<>();
                    //取最新评论数据
                    if (diaryReplyInfo != null && diaryReplyInfo.getNew_reply() != null && diaryReplyInfo.getNew_reply().size() > 0) {
                        for (int i = 0; i < diaryReplyInfo.getNew_reply().size(); i++) {
                            //往里插入一个类型 为了适配器里多布局分类
                            diaryReplyInfo.getNew_reply().get(i).setType("2");
                            diaryReplyListsNew.add(diaryReplyInfo.getNew_reply().get(i));
                        }
                    }
                    if (diaryReplyListsNew.size() < Integer.parseInt(diaryReplyInfo.getReply_page_offect())) {
                        diary_comments_list_refresh.finishLoadMoreWithNoMoreData();
                    } else {
                        diary_comments_list_refresh.finishLoadMore();
                    }
                    initCommentsList(diaryReplyListsNew);
                }
                mPage++;
            }
        });
    }

    private void initCommentsList(final List<DiaryReplyList2> diaryReplyLists) {
        if (mCommentsRecyclerAdapter2 == null) {
            mCommentsRecyclerAdapter2 = new CommentsRecyclerAdapter2((Activity) mContext, diaryReplyLists, Utils.dip2px(13), mAskorshare, id);
            buttomList.setAdapter(mCommentsRecyclerAdapter2);
            if (mCommentsRecyclerAdapter2.getHotItemCount().size() > 3) {
                int hideCount = mCommentsRecyclerAdapter2.getHotItemCount().size();
                //需要折叠评论
                for (int i = 0; i < hideCount - 3; i++) {
                    mCommentsRecyclerAdapter2.getmDatas().remove(NotShowHotLists.get(i));
                }
                mCommentsRecyclerAdapter2.notifyDataSetChanged();
            }
            mCommentsRecyclerAdapter2.setOnEventClickListener(new CommentsRecyclerAdapter2.OnEventClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                }

                @Override
                public void onItemLikeClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        pointAndReportLike(data);
                    }
                }

                @Override
                public void onItemReplyClick(View v, final int pos, String name) {
                    if (Utils.isLoginAndBind(mContext)) {
                        SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
                        sumbitPhotoData.setUserid(id);
                        sumbitPhotoData.set_id(diaryReplyLists.get(pos).getUserdata().getId());
                        sumbitPhotoData.setQid(mDiaryId);
                        sumbitPhotoData.setCid(diaryReplyLists.get(pos).getId());
                        sumbitPhotoData.setAskorshare(mAskorshare);
                        sumbitPhotoData.setNickname(name);
                        mDiaryCommentDialogView = new DiaryCommentDialogView((Activity) mContext, sumbitPhotoData);
                        mDiaryCommentDialogView.showDialog();
                        mDiaryCommentDialogView.setPicturesChooseGone(true);
                        //置空
//                        sumbitPhotoData.setNickname("");

                        //回复楼中楼提交完成回调
                        mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                            @Override
                            public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {

                                DiaryReplyListList diaryReplyListList = new DiaryReplyListList();
                                diaryReplyListList.setId(id);
                                diaryReplyListList.setContent(content);
                                diaryReplyListList.setTao(new DiaryReplyLisListTao());

                                //设置当前用户信息
                                DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                                diaryReplyListUserdata.setTalent("0");
                                diaryReplyListUserdata.setLable("");
                                diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                                diaryReplyListUserdata.setId(Utils.getUid());
                                diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                                diaryReplyListList.setUserdata(diaryReplyListUserdata);

                                diaryReplyListList.setUserdata(diaryReplyListUserdata);

                                if (mCommentsRecyclerAdapter2.getmDatas().get(pos).getList() == null
                                        || mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().size() == 0) {
                                    //如果当前没有评论
                                    mCommentsRecyclerAdapter2.setCommentsPosReply(pos, diaryReplyListList);
//                                    mCommentsRecyclerAdapter2.notifyDataSetChanged();
                                } else {
                                    mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).addItem(diaryReplyListList);
//                                    mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).notifyDataSetChanged();
                                }
                                mNumber = mNumber + 1;
                                tv_title.setText("全部" + mNumber + "条评论");
                                EventBus.getDefault().post(new PostMsgEvent(1));
                            }
                        });
                    }
                }

                @Override
                public void onItemDeleteClick(DiariesDeleteData deleteData) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit("确定要删除此评论吗?", deleteData);
                    }
                }

                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    if (Utils.isLoginAndBind(mContext)) {
                        showDialogExitEdit1("确定要举报此内容?", data);
                    }
                }

                @Override
                public void onTaoAndDiaryClick(DiaryReplyListTao tao) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(tao.getUrl(), "0", "0");
                }

                @Override
                public void onCommentMoreClick(View v, int pos) {
                    //展开热门评论 循环添加
                    for (int i = 0; i < NotShowHotLists.size(); i++) {
                        mCommentsRecyclerAdapter2.addItem(NotShowHotLists.get(i), pos + 1 + i);
                    }
                    mCommentsRecyclerAdapter2.upDataOpenHotList("1");
                }
            });
        } else {
            mCommentsRecyclerAdapter2.setAddData(diaryReplyLists);
        }
    }


    /**
     * 评论
     */
    private void startComments() {
        SumbitPhotoData sumbitPhotoData = new SumbitPhotoData();
        sumbitPhotoData.setUserid(id);
        sumbitPhotoData.setQid(mDiaryId);
        sumbitPhotoData.setAskorshare(mAskorshare);
        mDiaryCommentDialogView = new DiaryCommentDialogView((Activity) mContext, sumbitPhotoData);
        mDiaryCommentDialogView.showDialog();
        mDiaryCommentDialogView.setPicturesChooseGone(false);

        //评论提交完成回调
        mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
            @Override
            public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                mNumber = mNumber + 1;
                tv_title.setText("全部" + mNumber + "条评论");
                //添加评论
                EventBus.getDefault().post(new PostMsgEvent(1));
                //评论后的刷新
                DiaryReplyList2 diaryReplyList = new DiaryReplyList2();
                diaryReplyList.setId(id);
                diaryReplyList.setAgree_num("0");
                diaryReplyList.setContent(content);
                diaryReplyList.setList(new ArrayList<DiaryReplyListList>());
                diaryReplyList.setReply_num("0");
                diaryReplyList.setType("0");


//                diaryReplyList.setTao(new DiaryReplyListTao());


                //设置当前用户信息
                DiaryReplyListUserdata diaryReplyListUserdata = new DiaryReplyListUserdata();
                diaryReplyListUserdata.setTalent("0");
                diaryReplyListUserdata.setLable("刚刚");
//                diaryReplyListUserdata.setName(mFunctionManager.loadStr(FinalConstant.UNAME, "悦美用户"));
                diaryReplyListUserdata.setName(Cfg.loadStr(mContext, FinalConstant.UNAME, ""));
                diaryReplyListUserdata.setId(Utils.getUid());
//                diaryReplyListUserdata.setAvatar(mFunctionManager.loadStr(FinalConstant.UHEADIMG, ""));
                diaryReplyListUserdata.setAvatar(Cfg.loadStr(mContext, FinalConstant.UHEADIMG, ""));
                diaryReplyList.setUserdata(diaryReplyListUserdata);

                //设置评论图片
                List<DiaryReplyListPic> pics = new ArrayList<>();
                for (String s : imgLists) {
                    pics.add(new DiaryReplyListPic(s));
                }
                diaryReplyList.setPic(pics);
                mCommentsRecyclerAdapter2.addItem(diaryReplyList);
                mCommentsRecyclerAdapter2.notifyDataSetChanged();
                buttomList.scrollToPosition(0);
            }
        });
    }

    /**
     * 删除帖子 或 回复
     *
     * @param deleteData : 要删除的id（评论和回复）
     */
    private void deleteDiaries(DiariesDeleteData deleteData) {

        mDeleteBBsApi.getHashMap().put("id", deleteData.getId());
        mDeleteBBsApi.getHashMap().put("reply", deleteData.getReplystr());
        mDeleteBBsApi.getCallBack(mContext, mDeleteBBsApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String message) {
                MyToast.makeTextToast2(mContext, message, MyToast.SHOW_TIME).show();
            }
        });
    }

    /**
     * 日记本点赞和举报请求接口
     *
     * @param data
     */
    private void pointAndReportLike(DiariesReportLikeData data) {
        final String flag = data.getFlag();
        final String isReply = data.getIs_reply();
        final int pos = data.getPos();
        mPointLikeApi.addData("flag", flag);
        mPointLikeApi.addData("id", data.getId());
        mPointLikeApi.addData("is_reply", isReply);
        mPointLikeApi.addData("puid", pId);
        mPointLikeApi.getCallBack(mContext, mPointLikeApi.getHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData data) {
                if (!data.isOtherCode) {
                    MyToast.makeTextToast2(mContext, data.message, MyToast.SHOW_TIME).show();
                }
                if ("1".equals(flag)) {
                    if ("1".equals(data.code)) {

                        String is_agree = JSONUtil.resolveJson(data.data, "is_agree");

                        List<DiaryReplyList2> mDatas = mCommentsRecyclerAdapter2.getmDatas();
                        int agreeNum = Integer.parseInt(mDatas.get(pos).getAgree_num());
                        if ("1".equals(is_agree)) {
                            agreeNum++;
                        } else {
                            agreeNum--;
                        }
                        mDatas.get(pos).setIs_agree(is_agree);
                        mDatas.get(pos).setAgree_num(agreeNum + "");
                        mCommentsRecyclerAdapter2.notifyItemChanged(pos, "like");
                    }
                }
            }
        });
    }

    /**
     * 删除提示框
     *
     * @param content    ：提示框文案
     * @param deleteData ：要删除的id
     */
    private void showDialogExitEdit(String content, final DiariesDeleteData deleteData) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //删除
                deleteDiaries(deleteData);
                int commentsOrReply = deleteData.getCommentsOrReply();
                int pos = deleteData.getPos();
                int posPos = deleteData.getPosPos();
                if (commentsOrReply == 0) {
                    //我的 最新 可能重复 删除的时候一起删
                    //帖子id
                    String postId = mCommentsRecyclerAdapter2.getmDatas().get(pos).getId();
                    if (mCommentsRecyclerAdapter2.getmDatas().get(pos).getList() != null && mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().size() > 0) {
                        delecount = mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().size();
                    } else {
                        delecount = 0;
                    }
                    for (int i = mCommentsRecyclerAdapter2.getmDatas().size() - 1; i >= 0; i--) {
                        if (mCommentsRecyclerAdapter2.getmDatas().get(i).getId().equals(postId)) {
                            mCommentsRecyclerAdapter2.deleteItem(i);
                        }
                    }
                    mNumber = mNumber - delecount - 1;
                    if (mNumber < 0) {
                        mNumber = 0;
                    }
                    tv_title.setText("全部" + mNumber + "条评论");
                    EventBus.getDefault().post(new PostMsgEvent(0, delecount + 1));

                } else {
//                    String postId = mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().get(posPos).getId();
                    int delecount = 1;
//                    for (int i = mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().size() - 1; i >= 0; i--) {
//                        if (mCommentsRecyclerAdapter2.getmDatas().get(pos).getList().get(i).getId().equals(postId)) {
                    mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).deleteItem(posPos);
                    if (mCommentsRecyclerAdapter2.getHashMapAdapter().get(pos).getmDatas().size() == 0) {
                        mCommentsRecyclerAdapter2.notifyItem(pos);
//                                mCommentsRecyclerAdapter2.notifyDataSetChanged();
                    }
//                        }
//                    }
                    mNumber = mNumber - delecount;
                    if (mNumber < 0) {
                        mNumber = 0;
                    }
                    tv_title.setText("全部" + mNumber + "条评论");
                    EventBus.getDefault().post(new PostMsgEvent(0, delecount));
                }
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

    }

    /**
     * 举报提示框
     *
     * @param content ：提示框文案
     * @param data    ：要举报的参数
     */
    private void showDialogExitEdit1(String content, final DiariesReportLikeData data) {

        editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //举报
                pointAndReportLike(data);

                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

    }


    /**
     * 关闭
     */
    private void downDialog() {
        if (mDiaryCommentDialogView != null) {
            mDiaryCommentDialogView.dismiss();
        }

        if (editDialog != null) {
            editDialog.dismiss();
        }
        dismiss();
    }

}
