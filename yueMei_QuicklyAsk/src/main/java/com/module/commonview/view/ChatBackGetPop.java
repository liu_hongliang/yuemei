package com.module.commonview.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.commonview.module.bean.ChatBackSuccessBean;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.yy.mobile.rollingtextview.CharOrder;
import com.yy.mobile.rollingtextview.RollingTextView;
import com.yy.mobile.rollingtextview.strategy.Strategy;

import java.util.HashMap;

public class ChatBackGetPop extends PopupWindow {

    public ChatBackGetPop(final Context context, ChatBackSuccessBean successBean, final String hos_id){
        final View view = View.inflate(context, R.layout.chat_backget_pop, null);
        setWidth(Utils.dip2px(278));
        setHeight(Utils.dip2px(243));
        setFocusable(true);
        setContentView(view);
        update();

        ImageView background = view.findViewById(R.id.chat_back_bg);
        TextView title = view.findViewById(R.id.chat_back_title);
        TextView unit = view.findViewById(R.id.chat_back_unit);
        TextView desc = view.findViewById(R.id.chat_back_desc);


        RollingTextView rollingTextView = view.findViewById(R.id.chat_back_coin);



        Glide.with(context)
                .load(successBean.getRewardImg())
                .placeholder(R.drawable.chat_back_popbg)
                .error(R.drawable.chat_back_popbg)
                .into(background);
        title.setText(successBean.getSuccessNoticeTitle());
        unit.setText(successBean.getRewardType());
        desc.setText(successBean.getSuccessNoticeDesc());

        Button getBtn = view.findViewById(R.id.backget_btn);
        rollingTextView.setAnimationDuration(2000L);
        rollingTextView.addCharOrder(CharOrder.Number);
        rollingTextView.setCharStrategy(Strategy.CarryBitAnimation());
        rollingTextView.setText("0");
        rollingTextView.setText(successBean.getRewardMoney());
        getBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                new FunctionManager(context).showShort("奖励已经存入您的账户~");
                HashMap<String,String> parms = new HashMap<>();

                parms.put("id",hos_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_ALERT,"yes"),parms,new ActivityTypeData("151"));
            }
        });
    }


}
