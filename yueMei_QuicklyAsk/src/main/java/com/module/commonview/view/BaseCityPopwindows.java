package com.module.commonview.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.cn.demo.pinyin.AssortView;
import com.cn.demo.pinyin.AssortView2;
import com.cn.demo.pinyin.PinyinAdapter591;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.InitLoadCityApi;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.adapter.HotCityAdapter;
import com.module.doctor.controller.adapter.LocationCityAdapter;
import com.module.doctor.controller.adapter.SurroundingCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.api.SurroundingCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.MainCityData;
import com.module.doctor.model.bean.RecentVisitCityData;
import com.module.home.controller.activity.MainCitySelectActivity560;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Location;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyGridView;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.ViewInject;
import org.xutils.common.util.DensityUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * 城市选择Popwindows
 * Created by 裴成浩 on 2017/12/13.
 */

public class BaseCityPopwindows extends PopupWindow {

    private final View mView;
    private boolean isShowNear = false;
    private String TAG = "BaseCityPopwindows";
    private final Activity mActivity;
    private final ExpandableListView eListView;

    private final AssortView2 assortView;
    private final RelativeLayout othserRly;
    private final KJDB kjdbs;
    private View headView;
    private MyGridView hotGridlist;

    private List<MainCityData> citylistCache;
    private PinyinAdapter591 pinAdapter;
    private TextView cityLocTv;
    private List<CityDocDataitem> hotcityList;
    private HotCityAdapter hotcityAdapter;
    private List<MainCityData> citylist;
    private String autoCity;

    //周边城市整体布局
    private LinearLayout ll_surrounding_city;
    //周边城市列表
    private MyGridView gv_periphery_city;
    //定位和最近访问
    private MyGridView gv_location;
    private TextView tv_city_location;
    private LinearLayout ll_location_button;
    private LinearLayout ll_sucess;
    private LinearLayout ll_fail;
    private TextView tv_looking;

    private LocationClient locationClient;
    private String provinceLocation;
    private String isShowWholeCountry;
    private List<RecentVisitCityData> recentVisitCity;
    private LocationCityAdapter locationCityAdapter;

    public BaseCityPopwindows(Activity mActivity, View v) {
        this(mActivity, v, false, "1");
    }

    public BaseCityPopwindows(Activity mActivity, View v, boolean isShowNear, String ShowWholeCountry) {
        this.mActivity = mActivity;
        this.mView = v;
        this.isShowNear = isShowNear;
        this.isShowWholeCountry = ShowWholeCountry;

        kjdbs = KJDB.create(mActivity, "yuemeicity");

        final View view = View.inflate(mActivity, R.layout.pop_city_diqu, null);

        view.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.fade_ins));

        eListView = view.findViewById(R.id.elist1);
        assortView = view.findViewById(R.id.assort1);
        othserRly = view.findViewById(R.id.ly_content_ly1);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//多加这一句，问题就解决了！这句的官方文档解释是：让窗口背景后面的任何东西变暗
        setFocusable(true);
        setTouchable(true);
        setOutsideTouchable(true);
        setBackgroundDrawable(new BitmapDrawable(mActivity.getResources(), (Bitmap) null));
        setContentView(view);
        update();

        autoCity = Cfg.loadStr(mActivity, FinalConstant.LOCATING_CITY, "失败");
        initViewData();
        cityLocTv.setText(autoCity);
        othserRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }

    public void initViewData() {
        eListView.removeHeaderView(headView);
        LayoutInflater mInflater = mActivity.getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select_head_714, null);
        eListView.addHeaderView(headView);
        ll_surrounding_city = headView.findViewById(R.id.ll_surrounding_city);
        gv_periphery_city = headView.findViewById(R.id.gv_periphery_city);
        gv_location = headView.findViewById(R.id.gv_location);
        tv_city_location = headView.findViewById(R.id.tv_city_location);
        ll_location_button = headView.findViewById(R.id.ll_location_button);
        ll_sucess = headView.findViewById(R.id.ll_sucess);
        ll_fail = headView.findViewById(R.id.ll_fail);
        tv_looking = headView.findViewById(R.id.tv_looking);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) ll_sucess.getLayoutParams();
        linearParams.width = (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3;
        ll_sucess.setLayoutParams(linearParams);

        tv_looking.setText(Utils.getCity());
//        setLocationCity();
        initLocation();
        ll_location_button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_RECENT),hashMap,new ActivityTypeData("1"));
                if (ll_fail.getVisibility() == View.VISIBLE) {
                    if (lacksPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        //未开启
                        final YueMeiDialog yueMeiDialog = new YueMeiDialog(mActivity, "请允许悦美获取您当前位置，以帮您查询附近优惠项目", "取消", "确定");
                        yueMeiDialog.setCanceledOnTouchOutside(false);
                        yueMeiDialog.show();
                        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                            @Override
                            public void leftBtnClick() {
                                yueMeiDialog.dismiss();
                            }

                            @Override
                            public void rightBtnClick() {
                                yueMeiDialog.dismiss();
                                JumpSetting();
                            }
                        });
                    } else {
                        //已开启
                        initLocation();
                    }
//                    //版本判断
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        Acp.getInstance(mActivity).request(new AcpOptions.Builder()
//                                        .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)
//                                        .build(),
//                                new AcpListener() {
//                                    @Override
//                                    public void onGranted() {
//                                        if (!"失败".equals(autoCity)) {
//                                            initLocation();
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onDenied(List<String> permissions) {
//                                    }
//                                });
//                    } else {
//                        if (null != autoCity && autoCity.length() > 0) {
//                            initLocation();
//                        } else {
//                            ViewInject.toast("正在获取...");
//                        }
//
//                    }
                } else {
                    if (onAllClickListener != null) {
                        String cityName = autoCity;
                        onAllClickListener.onAllClick(cityName);
                    }
                }
            }
        });
        //附近是否显示隐藏
        LinearLayout nearCity = headView.findViewById(R.id.city_all_near_rly);
//        if (isShowNear) {
//            nearCity.setVisibility(View.VISIBLE);
//            nearCity.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (onAllClickListener != null) {
//                        onAllClickListener.onAllClick("附近");
//                    }
//                }
//            });
//        } else {
        nearCity.setVisibility(View.GONE);
//        }


        hotGridlist = headView.findViewById(R.id.group_grid_list1);

        citylistCache = kjdbs.findAll(MainCityData.class);
        Log.e(TAG, "citylistCache === " + citylistCache.toString());
        Log.e(TAG, "citylistCache === " + citylistCache.size());
        if (null != citylistCache && citylistCache.size() > 0) {
            pinAdapter = new PinyinAdapter591(mActivity, citylistCache);
            eListView.setAdapter(pinAdapter);

            // 展开所有
            for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
                eListView.expandGroup(i);
            }

            // 字母按键回调
            assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

                View layoutView = LayoutInflater.from(mActivity).inflate(
                        R.layout.alert_dialog_menu_layout, null);

                TextView text = (TextView) layoutView
                        .findViewById(R.id.content);
                RelativeLayout alRly = (RelativeLayout) layoutView
                        .findViewById(R.id.pop_city_rly);

                PopupWindow popupWindow;

                public void onTouchAssortListener(String str) {
                    if (str.equals("定位") || str.equals("热门") || str.equals("周边")) {
//                            eListView.smoothScrollToPosition(0);//滚动
                        eListView.setSelection(0);
                    }
                    int index = pinAdapter.getAssort().getHashList()
                            .indexOfKey(str);
                    if (index != -1) {
                        eListView.setSelectedGroup(index);
                    }
                    if (popupWindow == null) {

                        popupWindow = new PopupWindow(layoutView, 80, 80, false);

                        popupWindow.showAtLocation(alRly, Gravity.CENTER, 0, 0);
                    }
                }

                public void onTouchAssortUP() {
                    if (popupWindow != null)
                        popupWindow.dismiss();
                    popupWindow = null;
                }
            });

            eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView arg0, View arg1, int groupPosition, int childPosition, long arg4) {
                    String id = citylistCache.get(childPosition).get_id();
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("id",id);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_LIST_CLICK),hashMap,new ActivityTypeData("1"));
                    if (onAllClickListener != null) {
                        onAllClickListener.onAllClick(PinyinAdapter591.assortAA.getHashList().getValueIndex(groupPosition, childPosition));
                    }
                    return true;
                }
            });

        } else {
            initloadCity();
        }

        loadHotCity();
        loadSurroundingCity();

        LinearLayout cityAll = headView
                .findViewById(R.id.city_all_doc_rly);
        if (TextUtils.isEmpty(isShowWholeCountry) || isShowWholeCountry.equals("1")) {
            cityAll.setVisibility(View.VISIBLE);
        } else {
            cityAll.setVisibility(View.GONE);
        }
        cityAll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_ENTIRE_COUNTRY),hashMap,new ActivityTypeData("1"));
                if (onAllClickListener != null) {
                    onAllClickListener.onAllClick("全国");
                }
            }
        });

        RelativeLayout cityAntuo = headView.findViewById(R.id.city_auto_loaction_rly);
        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);


        cityAntuo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (onAllClickListener != null) {
                    String city = autoCity;
                    String cityName;
                    if ("失败".equals(city)) {
                        cityName = "全国";
                    } else {
                        cityName = city;
                    }
                    Log.e(TAG, "cityName2222 == " + cityName);
                    onAllClickListener.onAllClick(cityName);
                }
            }
        });

    }

    private void locatingCity() {
        if (onAllClickListener != null) {
            String city = autoCity;
            String cityName;
            if ("失败".equals(city)) {
                cityName = "全国";
            } else {
                cityName = city;
            }
            Log.e(TAG, "cityName2222 == " + cityName);
            onAllClickListener.onAllClick(cityName);
        }
    }

    private void setLocationCity() {
        Cfg.saveStr(mActivity, "location_city", provinceLocation);
        recentVisitCity = JSONUtil.jsonToArrayList(Cfg.loadStr(mActivity, "recent_visit_city", ""), RecentVisitCityData.class);
        if (recentVisitCity.size() < 3) {
            recentVisitCity = recentVisitCity.subList(0, recentVisitCity.size());
        } else {
            recentVisitCity = recentVisitCity.subList(recentVisitCity.size() - 3, recentVisitCity.size());
        }
        Cfg.saveStr(mActivity, "recent_visit_city", new Gson().toJson(recentVisitCity));
        //倒序
        Collections.reverse(recentVisitCity);
        if (ll_fail.getVisibility() == View.VISIBLE) {
            gv_location.setNumColumns(1);
            locationCityAdapter = new LocationCityAdapter(mActivity, recentVisitCity, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3, "1", provinceLocation);
        } else {
            gv_location.setNumColumns(2);
            locationCityAdapter = new LocationCityAdapter(mActivity, recentVisitCity, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3, "0", provinceLocation);
        }
        gv_location.setAdapter(locationCityAdapter);

        gv_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_RECENT),hashMap,new ActivityTypeData("1"));
                if (onAllClickListener != null) {
                    String cityName = recentVisitCity.get(position).getName();
                    onAllClickListener.onAllClick(cityName);
                }
            }
        });
    }

    private void loadSurroundingCity() {
        SurroundingCityApi surroundingCityApi = new SurroundingCityApi();
        surroundingCityApi.getHashMap().clear();
        surroundingCityApi.addData("city", Cfg.loadStr(mActivity, FinalConstant.DWCITY, ""));
        surroundingCityApi.getCallBack(mActivity, surroundingCityApi.getHashMap(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(final List<CityDocDataitem> cityDocDataitem) {
                if (cityDocDataitem != null && cityDocDataitem.size() > 0) {
                    ll_surrounding_city.setVisibility(View.VISIBLE);
                    SurroundingCityAdapter surroundingCityAdapter = new SurroundingCityAdapter(mActivity,
                            cityDocDataitem);
                    gv_periphery_city.setAdapter(surroundingCityAdapter);
                } else {
                    ll_surrounding_city.setVisibility(View.GONE);
                }
                //周边城市点击
                gv_periphery_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        if (onAllClickListener != null) {
                            YmStatistics.getInstance().tongjiApp(cityDocDataitem.get(pos).getEvent_params());
                            String cityName = cityDocDataitem.get(pos).getName();
                            onAllClickListener.onAllClick(cityName);
                        }
                    }
                });
            }

        });
    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;
                hotcityAdapter = new HotCityAdapter(mActivity, hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        if (onAllClickListener != null) {
                            YmStatistics.getInstance().tongjiApp(hotcityList.get(pos).getEvent_params());
                            String cityName = hotcityList.get(pos).getName();
                            onAllClickListener.onAllClick(cityName);
                        }
                    }
                });
            }

        });

    }

    void initloadCity() {
        new InitLoadCityApi().getCallBack(mActivity, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "serverData.data === " + serverData.data);
                    citylist = JSONUtil.jsonToArrayList(serverData.data, MainCityData.class);

                    if (null != citylist && citylist.size() > 0) {
                        pinAdapter = new PinyinAdapter591(mActivity,
                                citylist);
                        eListView.setAdapter(pinAdapter);
                    }

                    // 展开所有
                    for (int i = 0, length = pinAdapter
                            .getGroupCount(); i < length; i++) {
                        eListView.expandGroup(i);
                    }

                    // 字母按键回调
                    assortView.setOnTouchAssortListener(new AssortView.OnTouchAssortListener() {

                        View layoutView = LayoutInflater
                                .from(mActivity)
                                .inflate(
                                        R.layout.alert_dialog_menu_layout,
                                        null);

                        TextView text = (TextView) layoutView
                                .findViewById(R.id.content);
                        RelativeLayout alRly = (RelativeLayout) layoutView
                                .findViewById(R.id.pop_city_rly);

                        PopupWindow popupWindow;

                        public void onTouchAssortListener(
                                String str) {
                            if (str.equals("定位") || str.equals("热门") || str.equals("周边")) {
//                            eListView.smoothScrollToPosition(0);//滚动
                                eListView.setSelection(0);
                            }
                            int index = pinAdapter
                                    .getAssort()
                                    .getHashList()
                                    .indexOfKey(str);
                            if (index != -1) {
                                eListView
                                        .setSelectedGroup(index);
                            }
                            if (popupWindow != null) {
                                // text.setText(str);
                            } else {
                                popupWindow = new PopupWindow(
                                        layoutView, 80, 80,
                                        false);
                                popupWindow.showAtLocation(
                                        alRly,
                                        Gravity.CENTER, 0,
                                        0);
                            }
                        }

                        public void onTouchAssortUP() {
                            if (popupWindow != null)
                                popupWindow.dismiss();
                            popupWindow = null;
                        }
                    });

                    eListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                        @Override
                        public boolean onChildClick(ExpandableListView arg0, View arg1, int groupPosition, int childPosition, long arg4) {
                            if (onAllClickListener != null) {
                                onAllClickListener.onAllClick(PinyinAdapter591.assortAA.getHashList().getValueIndex(groupPosition, childPosition));
                            }
                            return true;
                        }
                    });
                }
            }
        });
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }

    public void showPop(int rawY) {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
//        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mActivity)[1] - rawY - mView.getHeight());
            }
//            showAsDropDown(mView);
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        }

    }

    //城市选择回调
    private OnAllClickListener onAllClickListener;

    public interface OnAllClickListener {
        void onAllClick(String city);
    }

    public void setOnAllClickListener(OnAllClickListener onAllClickListener) {
        this.onAllClickListener = onAllClickListener;
    }

    /**
     * 初始话百度的api定位
     */
    private void initLocation() {
        try {
            locationClient = new LocationClient(mActivity);
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(new MyBDLocationListener());
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定位结果的响应函数
     *
     * @author
     */
    public class MyBDLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            // 设置editTextLocation为返回的百度地图获取到的街道信息
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                Location location = new Location();
                location.setAddress(bdLocation.getAddrStr());

                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);

                String latitude = bdLocation.getLatitude() + "";
                String longitude = bdLocation.getLongitude() + "";
                //判断是否定位失败
                if (!"4.9E-324".equals(latitude) && !"4.9E-324".equals(longitude)) {
                    Cfg.saveStr(mActivity, FinalConstant.DW_LATITUDE, latitude);
                    Cfg.saveStr(mActivity, FinalConstant.DW_LONGITUDE, longitude);
                } else {
                    Cfg.saveStr(mActivity, FinalConstant.DW_LATITUDE, "0");
                    Cfg.saveStr(mActivity, FinalConstant.DW_LONGITUDE, "0");
//                    MyToast.makeTextToast2(mActivity, "定位失败，请查看手机是否开启了定位权限", MyToast.SHOW_TIME).show();
                }

                String ss = bdLocation.getCity();

                if (ss != null && ss.length() > 1) {

                    if (ss.contains("省")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("市")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("自治区")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }
//                    Cfg.saveStr(mActivity, "city_dingwei", provinceLocation);
//
//                    Cfg.saveStr(mActivity, FinalConstant.TAOCITY, provinceLocation);
//
//                    Cfg.saveStr(mActivity, FinalConstant.DWCITY, provinceLocation);
                    ll_fail.setVisibility(View.GONE);
                    ll_sucess.setVisibility(View.VISIBLE);
                    tv_city_location.setText(provinceLocation);
//                    if (Utils.getCity().equals(provinceLocation)) {
//                        ll_sucess.setBackgroundResource(R.drawable.shape_location_ff527f);
//                        tv_city_location.setTextColor(Color.parseColor("#FF527F"));
//                    } else {
//                        ll_sucess.setBackgroundResource(R.drawable.shape_location_f6f6f6);
//                        tv_city_location.setTextColor(Color.parseColor("#333333"));
//                    }
                    locationClient.unRegisterLocationListener(new MyBDLocationListener());
                } else {
//                    Cfg.saveStr(mActivity, "city_dingwei", "全国");
//
//                    Cfg.saveStr(mActivity, FinalConstant.TAOCITY, "全国");
//
//                    Cfg.saveStr(mActivity, FinalConstant.DWCITY, "全国");
                    provinceLocation = "失败";
                    ll_fail.setVisibility(View.VISIBLE);
                    ll_sucess.setVisibility(View.GONE);
                    locationClient.unRegisterLocationListener(new MyBDLocationListener());
                }
                setLocationCity();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 判断是否缺少权限
     */

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static boolean lacksPermission(Context mContexts, String permission) {
        return ContextCompat.checkSelfPermission(mContexts, permission) == PackageManager.PERMISSION_DENIED;
    }

    private void JumpSetting() {
        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            i.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            i.setData(Uri.fromParts("package", mActivity.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            i.setAction(Intent.ACTION_VIEW);
            i.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            i.putExtra("com.android.settings.ApplicationPkgName", mActivity.getPackageName());
        }
        mActivity.startActivity(i);
    }
}
