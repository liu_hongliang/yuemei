package com.module.commonview.view;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.PopupWindow;

import com.module.commonview.adapter.SortScreenAdapter;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/5/17
 */
public class SortScreenPopwin extends PopupWindow {
    private String TAG = "SortScreenPopwin";

    private Activity mContext;
    private View mView;
    private ArrayList<ProjcetData> mDatas;
    private SortScreenAdapter sortScreenKindAdapter;

    /**
     * 构造方法
     *
     * @param context
     * @param view    :要显示在下方的控件
     * @param datas   ：要使用的数据
     */
    public SortScreenPopwin(Activity context, View view, ArrayList<ProjcetData> datas) {

        this.mContext = context;
        this.mView = view;
        this.mDatas = datas;

        findView();
    }

    private void findView() {
        final View view = View.inflate(mContext, R.layout.sort_screen_popwin_view, null);

        //设置宽
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        //设置动画
        setAnimationStyle(R.anim.fade_ins);
        //设置背景
        setBackgroundDrawable(new ColorDrawable(Utils.getLocalColor(mContext, R.color.clear_1)));

        //设置焦点
        setFocusable(true);
        //点击外面窗口消失
        setOutsideTouchable(true);
        //设置自定义布局
        setContentView(view);

        final RecyclerView mRecycler = view.findViewById(R.id.sort_screen_popwin_recycler);
        Button mReset = view.findViewById(R.id.sort_screen_popwin_reset);
        Button mSure = view.findViewById(R.id.sort_screen_popwin_sure);
        View mOther = view.findViewById(R.id.sort_screen_popwin_other);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);

        sortScreenKindAdapter = new SortScreenAdapter(mContext, mDatas);
        mRecycler.setAdapter(sortScreenKindAdapter);

        //点击外部关闭
        mOther.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                dismiss();
            }
        });

        //重置
        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButtonClickListener != null) {
                    onButtonClickListener.onResetListener(v);
                    clearEdt(mRecycler);
                }
            }
        });

        //确定
        mSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButtonClickListener != null) {
                    onButtonClickListener.onSureListener(v, getSelectedData());
                }
            }
        });
    }

    /**
     * 恢复edittext默认值
     * @param mRecycler
     */
    private void clearEdt(RecyclerView mRecycler) {
        RecyclerView.ViewHolder holder = mRecycler.findViewHolderForAdapterPosition(1);
        if (holder != null & holder instanceof SortScreenAdapter.ViewHolder2){
            SortScreenAdapter.ViewHolder2 holder2 = (SortScreenAdapter.ViewHolder2) holder;
            holder2.mEditText1.setText("自定义最低价");
            holder2.mEditText1.clearFocus();
            holder2.mEditText2.setText("自定义最高价");
            holder2.mEditText2.clearFocus();
        }
    }

    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];

        mView.getLocationOnScreen(location);

        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                Log.e(TAG,"height ===="+(Utils.getScreenSize(mContext)[1] - rawY - mView.getHeight()));
                Log.e(TAG,"getScreenSize ===="+Utils.getScreenSize(mContext)[1]);
                Log.e(TAG,"mView ===="+mView.getHeight());
                setHeight(Utils.getScreenSize(mContext)[1]  - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }
    }


    public void showPop(int rawY) {
        int[] location = new int[2];

        mView.getLocationOnScreen(location);

//        int rawY = location[1];                                     //当前组件到屏幕顶端的距离
        Log.e(TAG, "rawY === " + rawY);

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                Log.e(TAG,"height ===="+(Utils.getScreenSize(mContext)[1] - rawY - mView.getHeight()));
                Log.e(TAG,"getScreenSize ===="+Utils.getScreenSize(mContext)[1]);
                Log.e(TAG,"mView ===="+mView.getHeight());
                setHeight(Utils.getScreenSize(mContext)[1]  - rawY - mView.getHeight());
            }
//            showAsDropDown(mView);
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        }
    }

    /**
     * 获取选中数据
     *
     * @return : 选中的数据集合
     */
    public ArrayList<ProjcetList> getSelectedData() {
        if (sortScreenKindAdapter != null) {
            return sortScreenKindAdapter.getSelectedData();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 重置数据
     */
    public void resetData() {
        if (sortScreenKindAdapter != null) {
            sortScreenKindAdapter.resetData();
        }
    }

    //确定重置点击回调
    private OnButtonClickListener onButtonClickListener;


    public interface OnButtonClickListener {
        void onResetListener(View view);

        void onSureListener(View view, ArrayList data);
    }

    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }


}
