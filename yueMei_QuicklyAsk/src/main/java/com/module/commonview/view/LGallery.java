package com.module.commonview.view;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.module.commonview.Listener.OnLGalleryPageSelectedListener;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.view.YueMeiVideoView;
import java.util.List;

public class LGallery extends FrameLayout implements ViewPager.OnPageChangeListener, OnLGalleryPageSelectedListener {
    private static final String TAG = "LGallery";
    private Context mContext;
    private ViewPager viewPager;
    private TextView tvNumIndicator;
    private TextView tvNumIndicatorVideo;
    private TextView tvNumIndicatorImage;
    private ImageView ivIndicatorImgPlay;
    private LinearLayout indicator;
    private LinearLayout indicatorVideo;
    private LinearLayout indicatorImage;
    private LinearLayout videoOrPhoto;
    private ImageView mSelloutImg;
    private LGalleryPagerAdapter mLGalleryPagerAdapter;
    private List<TaoDetailBean.PicBean> mPicBeans;
    private List<TaoDetailBean.VideoBean> mVideoBeans;
    private Point point;


    public LGallery(@NonNull Context context) {
        this(context, null);
    }

    public LGallery(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LGallery(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView(context);
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.lgallery, this, true);
        this.indicator = view.findViewById(R.id.indicator);
        this.tvNumIndicator = view.findViewById(R.id.tvNumIndicator);
        this.viewPager = view.findViewById(R.id.viewPager);
        this.indicatorVideo = view.findViewById(R.id.indicator_video);
        this.indicatorImage = view.findViewById(R.id.indicator_image);
        this.tvNumIndicatorVideo = view.findViewById(R.id.tvNumIndicator_video);
        this.tvNumIndicatorImage = view.findViewById(R.id.tvNumIndicator_image);
        this.ivIndicatorImgPlay = view.findViewById(R.id.indicator_video_play);

        this.videoOrPhoto = view.findViewById(R.id.lgallery_video_or_photo);
        this.mSelloutImg = view.findViewById(R.id.sell_out_img);
    }


    public void setData(List<TaoDetailBean.PicBean> imgDatas, List<TaoDetailBean.VideoBean> videoDatas,String skuId) {
        this.mPicBeans = imgDatas;
        this.mVideoBeans = videoDatas;
        if (mVideoBeans.size() == 0) {
            if (imgDatas.size() >= 1) {
                indicator.setVisibility(VISIBLE);
                tvNumIndicator.setText("1/" + imgDatas.size());
                videoOrPhoto.setVisibility(GONE);
            }
        } else {
            indicator.setVisibility(GONE);
            videoOrPhoto.setVisibility(VISIBLE);
        }
        viewPager.setOffscreenPageLimit(imgDatas.size() + videoDatas.size());

        initAdapter(mContext,skuId);
        indicatorVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });
        indicatorImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });
    }

    private void initAdapter(Context context,String skuId) {
        mLGalleryPagerAdapter = new LGalleryPagerAdapter(context, mPicBeans, mVideoBeans,skuId);
        viewPager.setAdapter(mLGalleryPagerAdapter);
        viewPager.setFocusable(true);
        viewPager.addOnPageChangeListener(this);

        mLGalleryPagerAdapter.setOnAdapterClickListener(new LGalleryPagerAdapter.OnAdapterClickListener() {
            @Override
            public void onMaxVideoClick(YueMeiVideoView videoView, String videoUrl, int currentPosition) {
                if (onAdapterClickListener != null) {
                    onAdapterClickListener.onMaxVideoClick(videoView, videoUrl, currentPosition);
                }
            }
        });
    }

    public void setSellOut(String status) {
        if ("1".equals(status)) {
            mSelloutImg.setVisibility(GONE);

        } else {
            mSelloutImg.setVisibility(VISIBLE);
        }
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        if (mVideoBeans.size() != 0) {
            if (i == 0) {
                indicator.setVisibility(GONE);
                indicatorVideo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.lgallery_video_select));
                indicatorImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.lgralley_video_unselect));
                tvNumIndicatorVideo.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                tvNumIndicatorImage.setTextColor(ContextCompat.getColor(mContext, R.color._33));
                ivIndicatorImgPlay.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_video_write));
            } else {
                indicator.setVisibility(VISIBLE);
                indicatorImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.lgallery_video_select));
                indicatorVideo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.lgralley_video_unselect));
                tvNumIndicatorImage.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                tvNumIndicatorVideo.setTextColor(ContextCompat.getColor(mContext, R.color._33));
                ivIndicatorImgPlay.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sku_video_black));
                tvNumIndicator.setText(i + "/" + mPicBeans.size());
            }
            tvNumIndicator.setText((i) + "/" + mPicBeans.size());
        } else {
            tvNumIndicator.setText((i + 1) + "/" + mPicBeans.size());
        }

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onLGalleryPageSelected(int postion) {

    }

    //放大视频的监听
    private OnAdapterClickListener onAdapterClickListener;

    public interface OnAdapterClickListener {
        void onMaxVideoClick(YueMeiVideoView videoView, String videoUrl, int currentPosition);
    }

    public void setOnAdapterClickListener(OnAdapterClickListener onAdapterClickListener) {
        this.onAdapterClickListener = onAdapterClickListener;
    }


}
