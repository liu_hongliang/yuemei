package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;

import org.xutils.common.util.DensityUtil;


public class PayOrderDetailsDialog extends Dialog {

    private Context mContext;
    private float mPrice1, mPrice2, mPrice3, mPrice4, mPrice5, mPrice6;
    private String mOrderMoney;
    private LinearLayout mClose, ll1, ll2, ll3, ll4, ll5, ll6;
    private TextView tv_money_ll1, tv_money_ll2, tv_money_ll3, tv_money_ll4, tv_money_ll5, tv_money_ll6;
    private TextView mTotalMoney;
    private Button mPayButton;
    private LinearLayout ll_order_details;
    public PayListener payListener;


    public PayOrderDetailsDialog(Context context, float price1, float price2, float price3, float price4, float price5, float price6, String orderMoney, PayListener callback) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mPrice1 = price1;
        this.mPrice2 = price2;
        this.mPrice3 = price3;
        this.mPrice4 = price4;
        this.mPrice5 = price5;
        this.mPrice6 = price6;
        this.mOrderMoney = orderMoney;
        this.payListener = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.pop_pay_order_detail_dialog, null, false);
        onClickListener(view);
        setContentView(view);

        setCancelable(false);
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);


        WindowManager.LayoutParams params = window.getAttributes();
//        params.y = 50;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = DensityUtil.dip2px(375);
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        mClose = view.findViewById(R.id.pop_diary_button_close);
        ll1 = view.findViewById(R.id.ll1);
        ll2 = view.findViewById(R.id.ll2);
        ll3 = view.findViewById(R.id.ll3);
        ll4 = view.findViewById(R.id.ll4);
        ll5 = view.findViewById(R.id.ll5);
        ll6 = view.findViewById(R.id.ll6);
        tv_money_ll1 = view.findViewById(R.id.tv_money_ll1);
        tv_money_ll2 = view.findViewById(R.id.tv_money_ll2);
        tv_money_ll3 = view.findViewById(R.id.tv_money_ll3);
        tv_money_ll4 = view.findViewById(R.id.tv_money_ll4);
        tv_money_ll5 = view.findViewById(R.id.tv_money_ll5);
        tv_money_ll6 = view.findViewById(R.id.tv_money_ll6);
        mTotalMoney = view.findViewById(R.id.make_sure_order_order_price);
        mPayButton = view.findViewById(R.id.make_sure_order_pay_order);
        ll_order_details = view.findViewById(R.id.ll_order_details);
        mTotalMoney.setText(mOrderMoney);
        if (mPrice1 == 0) {
            ll1.setVisibility(View.GONE);
        } else {
            tv_money_ll1.setText("" + mPrice1);
            ll1.setVisibility(View.VISIBLE);
        }
        if (mPrice2 == 0) {
            ll2.setVisibility(View.GONE);
        } else {
            tv_money_ll2.setText("- ¥" + mPrice2);
            ll2.setVisibility(View.VISIBLE);
        }
        if (mPrice3 == 0) {
            ll3.setVisibility(View.GONE);
        } else {
            tv_money_ll3.setText("- ¥" + mPrice3);
            ll3.setVisibility(View.VISIBLE);
        }
        if (mPrice4 == 0) {
            ll4.setVisibility(View.GONE);
        } else {
            tv_money_ll4.setText("- ¥" + mPrice4);
            ll4.setVisibility(View.VISIBLE);
        }
        if (mPrice5 == 0) {
            ll5.setVisibility(View.GONE);
        } else {
            tv_money_ll5.setText("+ ¥" + mPrice5);
            ll5.setVisibility(View.VISIBLE);
        }
        if (mPrice6 == 0) {
            ll6.setVisibility(View.GONE);
        } else {
            tv_money_ll6.setText("- ¥" + mPrice6);
            ll6.setVisibility(View.VISIBLE);
        }
        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

        mPayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
                if (payListener != null) {
                    payListener.onPay(v);
                }
            }
        });

        ll_order_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

    }


    /**
     * 关闭
     */
    private void downDialog() {
        dismiss();
    }

    public interface PayListener {
        void onPay(View v);
    }

    public void setOnItemCallBackListener(PayListener listener) {
        this.payListener = listener;
    }

}
