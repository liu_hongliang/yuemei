package com.module.commonview.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * Vip价格样式View
 * Created by 裴成浩 on 2019/8/12
 */
public class YmVipPriceView extends RelativeLayout {

    private final Context mContext;
    private TextView textView;

    public YmVipPriceView(Context context) {
        this(context, null);
    }

    public YmVipPriceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public YmVipPriceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec,  Utils.dip2px(14));
    }

    private void initView() {
        textView = new TextView(mContext);
        textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        textView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.shape_vip_price_background));
        textView.setPadding(Utils.dip2px(13), 0, Utils.dip2px(5), 0);
        textView.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(mContext);
        imageView.setBackgroundResource(R.drawable.plus_image_new);

        //TextView设置
        RelativeLayout.MarginLayoutParams textViewlp = new RelativeLayout.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        textViewlp.leftMargin = Utils.dip2px(10);

        RelativeLayout.LayoutParams imageViewlp = new RelativeLayout.LayoutParams(Utils.dip2px(21), Utils.dip2px(14));

        addView(textView, textViewlp);
        addView(imageView, imageViewlp);
    }


    /**
     * 设置vip价格
     *
     * @param price
     */
    public void setVipPrice(String price) {
        if (textView != null) {
            textView.setText(price);
        }
    }
}
