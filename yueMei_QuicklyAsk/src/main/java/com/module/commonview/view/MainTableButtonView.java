package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.MainTableActivity;
import com.module.commonview.utils.StatisticalManage;
import com.module.community.controller.activity.CommunityActivity;
import com.module.home.controller.activity.HomeActivity;
import com.module.home.controller.activity.HomeActivity623;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.my.controller.activity.HomePersonActivity;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.other.AutoLoginControler;
import com.module.taodetail.controller.activity.TaoActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.StartTabbar;
import com.quicklyask.entity.StartTabbarBgimage;
import com.quicklyask.entity.StartTabbarTab;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import java.util.List;

import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.PreLoginListener;


/**
 * 首页底部按钮
 * Created by 裴成浩 on 2018/9/26.
 */
public class MainTableButtonView extends LinearLayout {

    private Animation mScaleAnimation;
    private Activity mContext;
    private SizeImageRadioButton bnBottom[] = new SizeImageRadioButton[5];
    private TabHost tabHost;
    public static final int NEWS_CODE = 100;
    private String TAG = "MainTableButtonView";
    public HomeTopView tabHomeTop;
    private TextView tabMessageNum;
    private View tabMessagePrompt;
    private View tabPersonalPrompt;
    public int tabPos = 0;
    private View tabNewuser;

    public MainTableButtonView(Context context) {
        this(context, null);
    }

    public MainTableButtonView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTableButtonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);
        this.mContext = (Activity) context;

        mScaleAnimation = AnimationUtils.loadAnimation(mContext, R.anim.ta2_bianda1);
    }

    /**
     * 默认布局
     */
    public void initDefaultView(TabHost th, String city, boolean isCallback) {
        this.tabHost = th;
        removeAllViews();
        Log.e(TAG, "mContext == " + mContext);
        Log.e(TAG, "tabHost == " + tabHost);

        //添加分割线
        View view = new View(mContext);
        view.setBackgroundColor(Utils.getLocalColor(mContext, R.color._e5));
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(0.5f)));
        addView(view);

        //初始化默认布局
        View defaultView = View.inflate(mContext, R.layout.main_table_button_view1, this);

        bnBottom[0] = defaultView.findViewById(R.id.tab_home);
        bnBottom[1] = defaultView.findViewById(R.id.tab_tao);
        tabNewuser = defaultView.findViewById(R.id.tab_newuser);
        String tao_newuser = Cfg.loadStr(mContext, "tao_newuser", "");
        if (TextUtils.isEmpty(tao_newuser)) {
            tabNewuser.setVisibility(VISIBLE);
        } else {
            tabNewuser.setVisibility(GONE);
        }
        if (isCallback) {
            if ("值得买".equals(city)) {
                bnBottom[1].setDrawable(ContextCompat.getDrawable(mContext, R.drawable.tab_bt_tao));
                bnBottom[1].setDrawableWidth(Utils.dip2px(22));
                bnBottom[1].setDrawableHeight(Utils.dip2px(22));
                bnBottom[1].setText(city);
                bnBottom[1].initView();
                ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) tabNewuser.getLayoutParams();
                layoutParams.topMargin = Utils.dip2px(3);
                layoutParams.rightMargin = Utils.dip2px(20);
                tabNewuser.setLayoutParams(layoutParams);

            } else {
                bnBottom[1].setDrawable(ContextCompat.getDrawable(mContext, R.drawable.tab_bt_city));
                bnBottom[1].setDrawableWidth(Utils.dip2px(70));
                bnBottom[1].setDrawableHeight(Utils.dip2px(25));
                bnBottom[1].setText(city);
                bnBottom[1].initView();
                ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) tabNewuser.getLayoutParams();
                layoutParams.topMargin = Utils.dip2px(3);
                layoutParams.rightMargin = Utils.dip2px(12);
                tabNewuser.setLayoutParams(layoutParams);
            }
        }

        bnBottom[2] = defaultView.findViewById(R.id.tab_community);
        bnBottom[3] = defaultView.findViewById(R.id.tab_message);
        bnBottom[4] = defaultView.findViewById(R.id.tab_personal);
        tabHomeTop = defaultView.findViewById(R.id.tab_home_top);
        tabHomeTop.setImgResource();

        tabMessageNum = defaultView.findViewById(R.id.tab_message_num);
        tabMessagePrompt = defaultView.findViewById(R.id.tab_message_prompt);
        tabPersonalPrompt = defaultView.findViewById(R.id.tab_personal_prompt);

        initView();
    }

    /**
     * 网络加载布局
     *
     * @param th
     */
    public void initNetworkView(TabHost th, StartTabbar tabbar) {
        this.tabHost = th;
        Log.e(TAG, "mContext == " + mContext);
        Log.e(TAG, "tabHost == " + tabHost);

        List<StartTabbarTab> mTabs = tabbar.getTab();
        StartTabbarBgimage bgimage = tabbar.getBgimage();

        //网络加载布局
        View networkView = View.inflate(mContext, R.layout.main_table_button_view2, this);
        ImageView tabBackground = networkView.findViewById(R.id.tab_button_background);
        bnBottom[0] = networkView.findViewById(R.id.tab_network_home);
        bnBottom[1] = networkView.findViewById(R.id.tab_network_tao);
        bnBottom[2] = networkView.findViewById(R.id.tab_network_community);
        bnBottom[3] = networkView.findViewById(R.id.tab_network_message);
        bnBottom[4] = networkView.findViewById(R.id.tab_network_personal);
        tabHomeTop = networkView.findViewById(R.id.tab_network_home_top);
        tabMessageNum = networkView.findViewById(R.id.tab_network_message_num);
        tabMessagePrompt = networkView.findViewById(R.id.tab_network_message_prompt);
        tabPersonalPrompt = networkView.findViewById(R.id.tab_network_personal_prompt);

        //设置颜色
        int[][] states = new int[][]{new int[]{-android.R.attr.state_checked}, new int[]{android.R.attr.state_checked}};

        String unselectedColor = TextUtils.isEmpty(tabbar.getUnselected_color()) ? "#333333" : tabbar.getUnselected_color();
        String selectedColor = TextUtils.isEmpty(tabbar.getSelected_color()) ? "#ff5c77" : tabbar.getSelected_color();

        int[] colors = new int[]{Utils.setCustomColor(unselectedColor), Utils.setCustomColor(selectedColor)};
        ColorStateList colorStateList = new ColorStateList(states, colors);

        //加载背景
        if (!TextUtils.isEmpty(bgimage.getImg())) {
            Glide.with(mContext).load(bgimage.getImg()).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(tabBackground);
        } else {
            tabBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color.transparent_f5ffffff));
        }

        for (int i = 0; i < mTabs.size(); i++) {
            final StartTabbarTab tabbarTab = mTabs.get(i);

            //设置文案
            bnBottom[i].setText(tabbarTab.getTitle());

            //设置字体颜色
            bnBottom[i].setTextColor(colorStateList);

            //加载选中项
            final int finalI = i;

            String selectedImg;
            final String unselectedImg;
            if (Utils.isOldSmallPhone(mContext)) {
                selectedImg = tabbarTab.getSelected_img_2x();
                unselectedImg = tabbarTab.getUnselected_img_2x();
            } else {
                selectedImg = tabbarTab.getSelected_img_3x();
                unselectedImg = tabbarTab.getUnselected_img_3x();
            }


            Glide.with(mContext).load(selectedImg).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(final GlideDrawable resource1, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    if (resource1.isAnimated()){
                        resource1.setLoopCount(-1);
                        resource1.start();
                    }
                    //加载未选中项
                    Glide.with(mContext).load(unselectedImg).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(new SimpleTarget<GlideDrawable>() {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                            if (resource.isAnimated()){
                                resource.setLoopCount(-1);
                                resource.start();
                            }
                            StateListDrawable drawableStateList = new StateListDrawable();
                            drawableStateList.addState(new int[]{android.R.attr.state_checked}, resource1);
                            drawableStateList.addState(new int[]{-android.R.attr.state_checked}, resource);

                            bnBottom[finalI].setDrawable(drawableStateList);
                            bnBottom[finalI].initView();
                        }
                    });
                }
            });
        }
        initView();
    }

    /**
     * 公有部分初始化
     */
    private void initView() {
        //初始化跳转
        TabHost.TabSpec tab1 = tabHost.newTabSpec("首页");
        TabHost.TabSpec tab2 = tabHost.newTabSpec("淘整型");
        TabHost.TabSpec tab3 = tabHost.newTabSpec("社区");
        TabHost.TabSpec tab4 = tabHost.newTabSpec("消息");
        TabHost.TabSpec tab5 = tabHost.newTabSpec("我的");

        tab1.setIndicator("1").setContent(new Intent(mContext, HomeActivity.class));
        tab2.setIndicator("2").setContent(new Intent(mContext, TaoActivity.class));
        //社区页面有默认选中
        Intent intent = new Intent(mContext, CommunityActivity.class);
        if (mContext instanceof MainTableActivity) {
            String mTabid = ((MainTableActivity) mContext).mTabCa;
            intent.putExtra("tab_ca", mTabid);
        } else {
            intent.putExtra("tab_ca", "0");
        }
        tab3.setIndicator("3").setContent(intent);
        tab4.setIndicator("4").setContent(new Intent(mContext, MessageFragmentActivity1.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        tab5.setIndicator("5").setContent(new Intent(mContext, HomePersonActivity.class));
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
        tabHost.addTab(tab4);
        tabHost.addTab(tab5);
        //首页点击事件
        bnBottom[0].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bnBottomClickListener(0);
            }
        });

        //淘整型点击事件
        bnBottom[1].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bnBottomClickListener(1);
                if (null != tabNewuser) {
                    Cfg.saveStr(mContext, "tao_newuser", "1");
                    tabNewuser.setVisibility(GONE);
                }
            }
        });

        //社区点击事件
        bnBottom[2].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bnBottomClickListener(2);
            }
        });

        //消息点击事件
        bnBottom[3].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.noLoginChat()) {
                    bnBottomClickListener(3);

                } else {
                    if (Utils.isLogin()){
                        if (Utils.isBind()) {
                            bnBottomClickListener(3);
                        } else {
                            setCheckedPos(3);
                            Utils.jumpBindingPhone(MainTableButtonView.this.mContext);
                        }
                    } else {
                        setCheckedPos(3);
                        Cfg.saveStr(mContext,"is_jump_login","1");
                        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(mContext);
                        if (verifyEnable) {
                            mContext.startActivity(new Intent(mContext, AutoLoginControler.class));
                        } else {
                            Intent intent = new Intent(mContext, LoginActivity605.class);
                            intent.putExtra("news", "1");
                            mContext.startActivityForResult(intent, NEWS_CODE);
                        }

                    }
                }

            }
        });

        //我的点击事件
        bnBottom[4].setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bnBottomClickListener(4);
                //预取号
                JVerificationInterface.preLogin(mContext, 5000, new PreLoginListener() {
                    @Override
                    public void onResult(int i, String s) {
                        Log.e(TAG, "code ==" + i + ",  content===" + s);
                    }
                });
            }
        });

        //首页置顶点击
        tabHomeTop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onHomeTopClickListener != null) {
                    bnBottom[0].startAnimation(mScaleAnimation);
                    bnBottomClickListener(0);
                    onHomeTopClickListener.onHomeTopClick(tabHomeTop.isTabTop(), tabHomeTop);
                }
            }
        });
    }

    /**
     * 点击公共处理
     *
     * @param pos :选中按钮
     */
    private void bnBottomClickListener(int pos) {
        Log.e(TAG, "pos == " + pos);
        Log.e(TAG, "bnBottom[pos].isChecked() == " + bnBottom[pos].isChecked());
        setCheckedPos(pos);
//        onTabSelectedCallBack.onTabSelected(pos);

        Utils.tongjiApp(mContext, "home_menu", (pos + 1) + "", "home", "1");        //开启统计

        //GrowingIO统计
        switch (pos) {
            case 0:
                StatisticalManage.getInstance().growingIO("table_01");
                break;
            case 1:
                StatisticalManage.getInstance().growingIO("table_02");
                break;
            case 2:
                StatisticalManage.getInstance().growingIO("table_03");
                break;
            case 3:
                StatisticalManage.getInstance().growingIO("table_04");
                break;
            case 4:
                StatisticalManage.getInstance().growingIO("table_05");
                break;
        }
    }

    /**
     * 选中哪一个
     *
     * @param pos
     */
    public void setCheckedPos(int pos) {
        for (int i = 0; i < bnBottom.length; i++) {
            if (i == pos) {
                tabHost.setCurrentTab(i);
                bnBottom[i].setChecked(true);
                if (pos != tabPos) {
                    bnBottom[i].startAnimation(mScaleAnimation);
                    tabPos = pos;
                }
            } else {
                bnBottom[i].setChecked(false);
            }
        }

        //判断当前是否选中的首页，置顶按钮的显示与隐藏
        if (tabPos == 0) {
            tabHomeTop.setVisibility(VISIBLE);
        } else {
            tabHomeTop.setVisibility(INVISIBLE);
        }
    }

    /**
     * 消息页面下标数量
     *
     * @param num：下边数量
     */
    @SuppressLint("SetTextI18n")
    public void setMessageNum(int num) {
        if (num == 0) {
            tabMessageNum.setVisibility(GONE);
        } else {
            tabMessageNum.setVisibility(VISIBLE);
            if (num > 99) {
                tabMessageNum.setText("99+");
            } else {
                tabMessageNum.setText(num + "");
            }
        }
    }

    /**
     * 设置消息页面提示点是否显示
     *
     * @param visibility
     */
    public void setMessagePrompt(int visibility) {
        tabMessagePrompt.setVisibility(visibility);
    }

    /**
     * 设置我的页面提示点是否显示
     *
     * @param visibility
     */
    public void setPersonalPrompt(int visibility) {
        tabPersonalPrompt.setVisibility(visibility);
    }


    public interface OnHomeTopClickListener {
        void onHomeTopClick(boolean isTabTop, HomeTopView tabHomeTop);  //置顶点击回调
    }

    private OnHomeTopClickListener onHomeTopClickListener;

    public void setOnHomeTopClickListener(OnHomeTopClickListener onHomeTopClickListener) {
        this.onHomeTopClickListener = onHomeTopClickListener;
    }

//    private onTabSelectedCallBack onTabSelectedCallBack;
//
//    public void setOnTabSelectedCallBack(MainTableButtonView.onTabSelectedCallBack onTabSelectedCallBack) {
//        this.onTabSelectedCallBack = onTabSelectedCallBack;
//    }
//
//    public interface onTabSelectedCallBack{
//        void onTabSelected(int position);
//    }
}
