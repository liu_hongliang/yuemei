package com.module.commonview.view;

import android.content.Context;
import android.text.TextUtils;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;

import com.baidu.mapapi.model.inner.GeoPoint;
import com.module.commonview.utils.StatisticalManage;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.entity.Location;
import com.quicklyask.util.Cfg;

import java.util.HashMap;

/**
 * 百度定位
 * Created by 裴成浩 on 2017/12/20.
 */

public class MyBDLocationListener implements BDLocationListener {

    private Context mContext;

    public MyBDLocationListener(Context context) {
        this.mContext = context;
    }

    @Override
    public void onReceiveLocation(BDLocation bdLocation) {
        /**
         * 此处需联网
         */
        // 设置editTextLocation为返回的百度地图获取到的街道信息
        // editTextLocation.setText(bdLocation.getAddrStr());

        // 自定义的位置类保存街道信息和经纬度（地图中使用）
        Location location = new Location();
        location.setAddress(bdLocation.getAddrStr());
        GeoPoint geoPoint = new GeoPoint(
                (int) (bdLocation.getLatitude() * 1E6),
                (int) (bdLocation.getLongitude() * 1E6));
        location.setGeoPoint(geoPoint);

        String autoCity;
        if (!TextUtils.isEmpty(bdLocation.getCity())) {
            autoCity = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
        } else {
            autoCity = "失败";
        }

        Cfg.saveStr(mContext, FinalConstant.LOCATING_CITY, autoCity);

        //添加统计
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("area01","失败".equals(autoCity) ? "全国" : autoCity);
        StatisticalManage.getInstance().growingIO("location_area",hashMap);

    }

}