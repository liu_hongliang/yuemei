package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.my.model.bean.ProjcetList;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 匹配面诊
 */
public class VideoChatSelectProjectDialog extends Dialog {
    public static final String TAG = VideoChatSelectProjectDialog.class.getSimpleName();
    private final VideoChatProjectSelectAdapter mProjectSelectAdapter;
    private int pos = -1;
    private int age = 0;
    private boolean isKonwAboutMessage = true;
    private TextView alreadyKonwTxt1;
    private TextView alreadyKonwTxt2;



    public VideoChatSelectProjectDialog(@NonNull final Context context) {
        super(context, R.style.CustomDialog);
        setContentView(R.layout.video_chat_select_project_dialog);

        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.AnimBottom);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        setCanceledOnTouchOutside(false);

        LinearLayout projectClose =  findViewById(R.id.project_close);
        RecyclerView projectList =  findViewById(R.id.project_list);
        StringScrollPicker ageSelect =  findViewById(R.id.project_select);
        alreadyKonwTxt1 =  findViewById(R.id.project_already_konw_txt1);
        alreadyKonwTxt2 =  findViewById(R.id.project_already_konw_txt2);
        Button projectBtn =  findViewById(R.id.project_btn);

        ArrayList<ProjcetList> projcetLists = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ProjcetList projcetList = new ProjcetList();
            projcetList.setPostName("鼻部"+i);
            projcetList.setIs_selected(false);
            projcetLists.add(projcetList);
        }

        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(context, 3);
        gridLayoutManager.setScrollEnable(false);
        mProjectSelectAdapter = new VideoChatProjectSelectAdapter(R.layout.item_sort_screen_item_view, projcetLists);
        projectList.setLayoutManager(gridLayoutManager);
        projectList.setAdapter(mProjectSelectAdapter);
        mProjectSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                pos = position;
                VideoChatProjectSelectAdapter chatProjectSelectAdapter = (VideoChatProjectSelectAdapter)adapter;
                chatProjectSelectAdapter.singleSelect(position);
            }
        });

        List<String> ageList = new ArrayList<>();
        for (int i = 1; i <100 ; i++) {
            ageList.add(String.valueOf(i));
        }
        int endClolr = ContextCompat.getColor(context, R.color._33);
        int startClolr = ContextCompat.getColor(context, R.color.red_ff527f);
        ageSelect.setColor(startClolr,endClolr);
        ageSelect.setData(ageList);
        ageSelect.setSelectedPosition(23);
        ageSelect.setOnSelectedListener(new ScrollPickerView.OnSelectedListener() {
            @Override
            public void onSelected(ScrollPickerView scrollPickerView, int position) {
                age = position+1;
            }
        });

        projectClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        alreadyKonwTxt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alreadyKonwTxt1.setTextColor(Utils.getLocalColor(context, R.color.red_ff5c77));
                alreadyKonwTxt1.setBackgroundResource(R.drawable.home_diary_tab);
                alreadyKonwTxt2.setTextColor(Utils.getLocalColor(context, R.color._33));
                alreadyKonwTxt2.setBackgroundResource(R.drawable.home_diary_tab2);
            }
        });
        alreadyKonwTxt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isKonwAboutMessage = false;
                alreadyKonwTxt2.setTextColor(Utils.getLocalColor(context, R.color.red_ff5c77));
                alreadyKonwTxt2.setBackgroundResource(R.drawable.home_diary_tab);
                alreadyKonwTxt1.setTextColor(Utils.getLocalColor(context, R.color._33));
                alreadyKonwTxt1.setBackgroundResource(R.drawable.home_diary_tab2);
            }
        });

        projectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == -1){
                    MyToast.makeTextToast2(context, "请选择您要面诊的项目", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (age == 0){
                    MyToast.makeTextToast2(context, "请选择您的年龄", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }
}
