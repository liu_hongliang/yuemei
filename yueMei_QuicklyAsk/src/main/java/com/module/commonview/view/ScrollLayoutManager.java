package com.module.commonview.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * 设置自定义的布局管理器，可以设置是否可以垂直滚动
 * Created by 裴成浩 on 2018/6/6.
 */
public class ScrollLayoutManager extends LinearLayoutManager {

    private boolean isScrollEnable = true;
    private String TAG = "ScrollLinearLayoutManager";

    public ScrollLayoutManager(Context context) {
        super(context);
    }

    public ScrollLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public ScrollLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnable && super.canScrollVertically();
    }

    /**
     * 设置是否可以垂直滑动
     *
     * @param isEnable :false 不可滑动
     */
    public void setScrollEnable(boolean isEnable) {
        this.isScrollEnable = isEnable;
    }

    //为了解决插入删除 崩溃问题
    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}