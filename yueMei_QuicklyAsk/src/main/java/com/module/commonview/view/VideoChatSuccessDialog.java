package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;

import com.quicklyask.activity.R;

public class VideoChatSuccessDialog extends Dialog {
    public VideoChatSuccessDialog(@NonNull Context context) {
        super(context, R.style.CustomDialog);

        setContentView(R.layout.video_chat_success);

        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.AnimBottom);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
