package com.module.commonview.view;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

public class VideoChatComplainPop extends PopupWindow {

    private Activity mActivity;
    public View view;
    public VideoChatComplainPop(Activity activity) {
        this.mActivity=activity;
        initView();
    }

    private void initView() {
        view = View.inflate(mActivity, R.layout.video_chat_pop, null);
        setWidth(Utils.dip2px(101));
        setHeight(Utils.dip2px(47));

        setContentView(view);
        setFocusable(true);//获取焦点
        setOutsideTouchable(true);//获取外部触摸事件
        setTouchable(true);
        update();
    }
}
