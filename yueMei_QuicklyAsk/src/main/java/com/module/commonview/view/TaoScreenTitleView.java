package com.module.commonview.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.module.commonview.module.bean.TaoScreenTitleData;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.other.module.bean.TaoPopItemData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 淘整型排序
 * Created by 裴成浩 on 2019/9/4
 */
public class TaoScreenTitleView extends LinearLayout {

    private final Context mContext;
    LinearLayout mTitleCityClick;
    TextView mTitleCity;
    ImageView mImageCity;
    LinearLayout mTitleSortClick;
    TextView mTitleSort;
    ImageView mImageSort;
    LinearLayout mTilteSalesClick;
    TextView mTitleSales;
    LinearLayout mTilteCaseClick;
    TextView mTitleCase;
    View mViewLine4;
    LinearLayout mTitleKindClick;
    TextView mTitleKind;
    ImageView mImageKind;
    ImageView mKindSelected;
    ImageView mChangeIv;
    LinearLayout mChangeClick;
    private int rawHeight;

    private BaseCityPopwindows cityPop; //城市选择
    private BaseSortPopupwindows sortPop;//智能排序
    private SortScreenPopwin scerPop;   //筛选

    private final String NEAR_TITLE = "附近";
    private final String ALL_COUTRY = "全国";
    private String nearCity = NEAR_TITLE;
    private TaoScreenTitleData taoScreenTitleData;
    private boolean mIsChange = false;
    private String isShowWholeCountry = "";

    public TaoScreenTitleView(Context context) {
        this(context, null);
    }

    public TaoScreenTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaoScreenTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        taoScreenTitleData = new TaoScreenTitleData();
        taoScreenTitleData.setSort("1");                                    //综合默认选中1
        taoScreenTitleData.setKindStr(new ArrayList<ProjcetList>());        //筛选
    }

    public void setRawHeight(int rawHeight) {
        this.rawHeight = rawHeight;
    }

    /**
     * 初始化数据
     */
    public void initView(boolean isChange, String isShowWholeCountry) {
        setOrientation(VERTICAL);

        View view = View.inflate(mContext, R.layout.tao_screen_title_view, this);

        mTitleCityClick = view.findViewById(R.id.screen_title_city_click);
        mTitleCity = view.findViewById(R.id.screen_title_city);
        mImageCity = view.findViewById(R.id.screen_image_city);

        mTitleSortClick = view.findViewById(R.id.screen_title_sort_click);
        mTitleSort = view.findViewById(R.id.screen_title_sort);
        mImageSort = view.findViewById(R.id.screen_image_sort);

        mTilteSalesClick = view.findViewById(R.id.screen_title_sales_click);
        mTitleSales = view.findViewById(R.id.screen_title_sales);
        mTilteCaseClick = view.findViewById(R.id.screen_title_case_click);
        mTitleCase = view.findViewById(R.id.screen_title_case);

        mViewLine4 = view.findViewById(R.id.screen_view_line4);                   //排序和筛选的分割线
        mTitleKindClick = view.findViewById(R.id.screen_title_kind_click);
        mTitleKind = view.findViewById(R.id.screen_title_kind);
        mImageKind = view.findViewById(R.id.screen_image_kind);
        mKindSelected = view.findViewById(R.id.screen_image_kind_selected);
        mChangeClick = view.findViewById(R.id.screen_change_click);
        mChangeIv = view.findViewById(R.id.tao_type_change_iv);

        if (isChange) {
            mChangeClick.setVisibility(VISIBLE);
        } else {
            mChangeClick.setVisibility(GONE);
        }

//        View line = new View(mContext);
//        line.setBackgroundColor(Utils.getLocalColor(mContext, R.color._e5));
//        line.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(0.5f)));
//        addView(line);

        //城市设置
        setCityData(isShowWholeCountry);

        //城市点击
        mTitleCityClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClickListener();
                }
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        if (rawHeight != 0) {
                            cityPop.showPop(rawHeight);
                        } else {
                            cityPop.showPop();
                        }
                    }
                    initCityView(cityPop.isShowing());
                    cityPop.initViewData();
                }
            }
        });

        //综合点击
        mTitleSortClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClickListener();
                }
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        if (rawHeight != 0) {
                            sortPop.showPop(rawHeight);
                        } else {
                            sortPop.showPop();
                        }

                    }
                    initSortView(sortPop.isShowing());
                }
            }
        });

        //销量
        mTilteSalesClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClickListener();
                }
                if (onEventClickListener != null) {
                    if (sortPop != null) {
                        TaoPopItemData data = sortPop.setSelected(0);
                        if (data != null) {
                            setSort("4", data.getName());
                        }
                    }
                    onEventClickListener.onScreenClick();
                }
            }
        });

        //案例
        mTilteCaseClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClickListener();
                }

                if (onEventClickListener != null) {
                    if (sortPop != null) {
                        TaoPopItemData data = sortPop.setSelected(0);
                        if (data != null) {
                            setSort("5", data.getName());
                        }
                    }
                    onEventClickListener.onScreenClick();
                }
            }
        });

        //筛选点击
        mTitleKindClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClickListener();
                }
                if (scerPop != null) {
                    if (scerPop.isShowing()) {
                        scerPop.dismiss();
                    } else {
                        if (rawHeight != 0) {
                            scerPop.showPop(rawHeight);
                        } else {
                            scerPop.showPop();
                        }

                    }
                    initKindView(scerPop.isShowing(), isScerPopSelected());
                }
            }
        });
        //切换点击
        mChangeClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (mIsChange) {
                    initChangeView(!mIsChange);
                    onEventClickListener.onChangeClick(!mIsChange);
                } else {
                    initChangeView(!mIsChange);
                    onEventClickListener.onChangeClick(!mIsChange);
                }
            }
        });
    }

    /**
     * 城市回调
     */
    private void setCityData(String isShowWholeCountry) {

        String city = Utils.getCity();
        if ("全国".equals(city)) {
            setCityTitle(ALL_COUTRY);
        } else {
//            setCityTitle(nearCity);
            setCityTitle(city);
        }
        nearCity = city;

        if (mContext instanceof Activity) {
            cityPop = new BaseCityPopwindows((Activity) mContext, this, true, isShowWholeCountry);

            //城市回调
            cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
                @Override
                public void onAllClick(String city) {
                    if (!NEAR_TITLE.equals(city)) {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, nearCity);
                    }

                    if (NEAR_TITLE.equals(city)) {
                        taoScreenTitleData.setHomepagecity("1");
                    } else {
                        taoScreenTitleData.setHomepagecity("0");
                    }

                    setCityTitle(city);
                    if (cityPop != null) {
                        cityPop.dismiss();
                        initCityView(cityPop.isShowing());
                    }

                    if (onEventClickListener != null) {
                        onEventClickListener.onScreenClick();
                    }
                }
            });

            cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    initCityView(false);
                }
            });
        }
    }


    /**
     * 综合数据
     */
    public void setSortData(List<TaoPopItemData> lvSortData) {
        if (mContext instanceof Activity) {
            sortPop = new BaseSortPopupwindows((Activity) mContext, this, lvSortData);

            sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
                @Override
                public void onSequencingClick(int pos, String sortId, String sortName) {
                    if (sortPop != null) {
                        sortPop.dismiss();
                        initSortView(sortPop.isShowing());
                        setSort(sortId, sortName);
                    }

                    if (onEventClickListener != null) {
                        onEventClickListener.onScreenClick();
                    }
                }
            });

            sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    initSortView(false);
                }
            });
        }
    }

    /**
     * 筛选数据
     *
     * @param data
     */
    public void setScreeData(ArrayList<ProjcetData> data) {
        if (mContext instanceof Activity) {
            scerPop = new SortScreenPopwin((Activity) mContext, this, data);

            scerPop.setOnButtonClickListener(new SortScreenPopwin.OnButtonClickListener() {
                @Override
                public void onResetListener(View view) {
                    if (scerPop != null) {
                        taoScreenTitleData.getKindStr().clear();
                        scerPop.resetData();
                    }
                }

                @Override
                public void onSureListener(View view, ArrayList data) {
                    if (scerPop != null) {
                        taoScreenTitleData.setKindStr(scerPop.getSelectedData());
                        scerPop.dismiss();
                    }

                    if (onEventClickListener != null) {
                        onEventClickListener.onScreenClick();
                    }
                }
            });

            //筛选关闭
            scerPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    initKindView(false, isScerPopSelected());
                }
            });
        }
    }

    /**
     * 城市的展开和收缩
     *
     * @param isShow
     */
    private void initCityView(boolean isShow) {
        if (isShow) {
            mTitleCity.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageCity.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleCity.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageCity.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    /**
     * 排序的展开和收缩
     *
     * @param isShow
     */
    private void initSortView(boolean isShow) {
        if (isShow) {
            mTitleSort.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageSort.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleSort.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageSort.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }


    /**
     * 筛选的展开和收缩
     *
     * @param isShow     :
     * @param isSelected ：是否有选中项
     */
    public void initKindView(boolean isShow, boolean isSelected) {
        if (isShow) {
            mTitleKind.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mImageKind.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            mTitleKind.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mImageKind.setBackgroundResource(R.drawable.gra_tao_tab);
        }

        if (isSelected) {
            mKindSelected.setVisibility(VISIBLE);
        } else {
            mKindSelected.setVisibility(GONE);
        }

    }

    /**
     * 切换按钮样式
     */
    public void initChangeView(boolean isChange) {
        mIsChange = isChange;
        if (isChange) {
            mChangeIv.setBackgroundResource(R.drawable.tao_type_change2);
        } else {
            mChangeIv.setBackgroundResource(R.drawable.tao_type_change);
        }
    }


    /**
     * 设置城市名称
     *
     * @param title
     */
    public void setCityTitle(String title) {
        mTitleCity.setText(title);
    }

    /**
     * 设置排序的名称
     *
     * @param title
     */
    private void setSortTitle(String title) {
        mTitleSort.setText(title);
    }

    /**
     * 筛选中的数据是否有选中项
     *
     * @return ：true:有 false:无
     */
    private boolean isScerPopSelected() {
        return taoScreenTitleData.getKindStr().size() != 0;
    }

    public TaoScreenTitleData getTaoScreenTitleData() {
        return taoScreenTitleData;
    }

    /**
     * 设置 综合、销量、案例
     *
     * @param sort_id
     */
    private void setSort(String sort_id, String sortName) {

        if ("4".equals(sort_id)) {
            mTitleSales.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
            mTitleCase.setTextColor(Utils.getLocalColor(mContext, R.color._41));
        } else if ("5".equals(sort_id)) {
            mTitleSales.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mTitleCase.setTextColor(Utils.getLocalColor(mContext, R.color.tabfontcickbackgroud));
        } else {
            mTitleSales.setTextColor(Utils.getLocalColor(mContext, R.color._41));
            mTitleCase.setTextColor(Utils.getLocalColor(mContext, R.color._41));
        }

        setSortTitle(sortName);

        if (taoScreenTitleData != null) {
            taoScreenTitleData.setSort(sort_id);
        }
    }

    //刷新回调
    public interface OnEventClickListener {

        void onScreenClick();                 //刷新回调

        void onChangeClick(boolean isChange);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }


    public void setClickListener(ClickListener clickListener) {
        mClickListener = clickListener;
    }

    private ClickListener mClickListener;

    public interface ClickListener {

        void onClickListener();
    }

}
