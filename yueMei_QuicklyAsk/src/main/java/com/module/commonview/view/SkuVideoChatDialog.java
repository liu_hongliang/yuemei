package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.net.http.SslError;
import android.nfc.Tag;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.ChatActivity;
import com.module.commonview.module.bean.MessageBean;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.model.api.SaveVideoScoreApi;
import com.module.community.model.bean.FaceVdieoBean;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.controller.other.SkuVideoChatJSCallBack;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SkuVideoChatDialog extends Dialog {
    public static final String TAG =SkuVideoChatDialog.class.getSimpleName();
    private final WebView mWebView;
    private Context context;
    public static final int MATCH_SUCCESS = 1;//匹配成功三秒跳转
    public static final int SAVEVIDEOSCORE = 2;//更新面诊评价评分
    private Handler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<SkuVideoChatDialog> mDialogWeakReference;
        private MyHandler(SkuVideoChatDialog skuVideoChatDialog) {
            mDialogWeakReference = new WeakReference<>(skuVideoChatDialog);
        }
        @Override
        public void handleMessage(Message msg) {
            SkuVideoChatDialog theDialog = mDialogWeakReference.get();
            if (theDialog != null) {
                switch (msg.what) {
                    case MATCH_SUCCESS:
                        FaceVdieoBean faceVdieoBean = (FaceVdieoBean) msg.obj;
                        String faceVideoUrl = faceVdieoBean.getFaceVideoUrl();
                        String hospitalAccountId = faceVdieoBean.getHitHospitalAccountId();
                        WebViewUrlLoading.getInstance().showWebDetail(theDialog.getContext(), null, faceVideoUrl, hospitalAccountId);
                        theDialog.dismiss();
                        break;
                    case SAVEVIDEOSCORE:
                        FaceVdieoBean faceVdieoBean2 = (FaceVdieoBean) msg.obj;
                        String channelId = faceVdieoBean2.getChannel_id();
                        if (!TextUtils.isEmpty(channelId)) {
                            theDialog.saveVideoScore(channelId);
                        }
                        theDialog.dismiss();
                        break;
                }
            }
        }
    }


    /**
     *
     * @param context
     * @param url
     * @param height 根据弹窗的高度区分三种不同类型的弹窗 0.45 匹配成功弹窗 0.75 筛选弹窗  0.6 结束评分弹窗
     * @param faceVdieoBean  当匹配成功弹窗的时候有值
     */
    @SuppressLint("JavascriptInterface")
    public SkuVideoChatDialog(@NonNull final Context context, String url, float height, FaceVdieoBean faceVdieoBean) {
        super(context, R.style.DialogTheme);
        this.context = context;
        setContentView(R.layout.sku_video_chat_dialog);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.AnimBottom);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, (int) (windowH * height));
        setCanceledOnTouchOutside(false);
        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Activity activity = (Activity) context;
                activity.finish();
            }
        });
        LinearLayout webContainer = findViewById(R.id.web_container);
        mWebView = new WebView(context);
        mWebView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        webContainer.addView(mWebView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        WebSettings settings = mWebView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);//支持通过JS打开新窗口
        mWebView.getSettings().setDatabaseEnabled(true);//开启数据库
        mWebView.setFocusable(true);//获取焦点
        mWebView.requestFocus();
        String dir = context.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();//设置数据库路径
        mWebView.getSettings().setCacheMode(mWebView.getSettings().LOAD_CACHE_ELSE_NETWORK);//本地缓存
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mWebView.getSettings().setBlockNetworkImage(false);//显示网络图像
        mWebView.getSettings().setBlockNetworkLoads(false);
        mWebView.getSettings().setLoadsImagesAutomatically(true);//显示网络图像
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);//插件支持
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.getSettings().setLoadWithOverviewMode(true);// 缩放至屏幕的大小
        mWebView.getSettings().setUseWideViewPort(false);// 将图片调整到适合webview大小
        mWebView.getSettings().setBuiltInZoomControls(true);// 设置支持缩放
        mWebView.getSettings().setSupportZoom(false);//设置是否支持变焦
        mWebView.getSettings().setGeolocationEnabled(true);//定位
        mWebView.addJavascriptInterface(new SkuVideoChatJSCallBack(this),"android");
        mWebView.getSettings().setGeolocationDatabasePath(dir);//数据库
        mWebView.getSettings().setAllowFileAccess(true);
//        mWebView.setWebViewClient(mWebViewClientMessage);
        mWebView.setWebViewClient(new WebViewClient(){

        });
        mWebView.getSettings().setDomStorageEnabled(true);//缓存 （ 远程web数据的本地化存储）
        WebViewClient myWebViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("type")) {
                    try {
                        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                        parserWebUrl.parserPagrms(url);
                        JSONObject obj = parserWebUrl.jsonObject;
                        String mType = obj.getString("type");
                        if ("7166".equals(mType)){
                            String isClose = obj.getString("is_close");//1关闭当前弹窗 2不关
                            if ("1".equals(isClose)){
                                dismiss();
                            }
                        }else {
                            dismiss();
                        }
                        WebViewUrlLoading.getInstance().showWebDetail(context, url);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    dismiss();
                    WebUrlTypeUtil.getInstance(context).urlToApp(url, "0", "0");
                }

                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        };

        //建立对象
        mWebView.setWebViewClient(myWebViewClient);//调用
        mWebView.setWebChromeClient(new WebChromeClient() {

            //重写WebChromeClient的onGeolocationPermissionsShowPrompt
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }

        });


        if (height == 0.68f){
            Log.e(TAG,"0.68 -------");
            if (null == faceVdieoBean){
                return;
            }
            Map<String, Object> params = faceVdieoBean.getParams();
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url,params);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }else if (height == 0.5f){
            if (null == faceVdieoBean){
                return;
            }
            Map<String, Object> params = faceVdieoBean.getParams();
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url,params);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }else if (height == 0.45f){
            if (null == faceVdieoBean){
                return;
            }
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("hit_hospital_account_id",faceVdieoBean.getHitHospitalAccountId());
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url,hashMap);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        } else if (height == 0.6f){
            if (null == faceVdieoBean){
                return;
            }
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("channel_id",faceVdieoBean.getChannel_id());
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url,hashMap);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }else {
            Log.e(TAG," -------");
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }



        if (height == 0.45f && null != faceVdieoBean){
            Message message = mHandler.obtainMessage();
            message.what = MATCH_SUCCESS;
            message.obj = faceVdieoBean;
            mHandler.sendMessageDelayed(message,3000);
        }
        if (height == 0.6f && null != faceVdieoBean){
            Message message = mHandler.obtainMessage();
            message.what = SAVEVIDEOSCORE;
            message.obj = faceVdieoBean;
            mHandler.sendMessageDelayed(message,10000);
        }
    }


    private void saveVideoScore(String channelId){
        HashMap<String, Object> params = new HashMap<>();
        params.put("channel_id",channelId);
        params.put("score_type","2");
        new SaveVideoScoreApi().getCallBack(context, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){

                }
            }
        });
    }
}
