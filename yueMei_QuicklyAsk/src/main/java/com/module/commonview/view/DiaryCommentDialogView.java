package com.module.commonview.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.SumbitPhotoApi;
import com.module.commonview.module.bean.SumbitPhotoBean;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.community.controller.activity.SwitchPhotoPublicIfActivity;
import com.module.my.model.bean.ForumTextData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.emoji.Expressions;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.MyUploadImage;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.xinlan.imageeditlibrary.editimage.EditImageActivity;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.util.DensityUtil;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 评论列表评论弹层（可以上传图片）
 * Created by 裴成浩 on 2018/6/13.
 */
public class DiaryCommentDialogView extends AlertDialog {

    private SumbitPhotoData mDatas;
    private ArrayList<String> mResults = new ArrayList<>();
    //    @BindView(R.id.bbs_web_cancel1_iv)
//    ImageView cancelIv1;
//    @BindView(R.id.bbs_web_cancel1_rly)
//    RelativeLayout cancelRly1;
    @BindView(R.id.bbs_web_sumbit1_iv)
    ImageView sumbitIv1;
    @BindView(R.id.bbs_web_sumbit1_rly)
    RelativeLayout sumbitRly1;
    @BindView(R.id.bbs_web_sumbit1_bt)
    Button sumbitBt1;
    @BindView(R.id.bbs_web_input_content1_et)
    EditText inputContentEt;
    @BindView(R.id.bbs_web_input_content1_ly2)
    LinearLayout iputly2;
    @BindView(R.id.up_photo_iv)
    ImageView upPhotoIv;
    @BindView(R.id.write_que_bbs_add_photo_rly_container)
    LinearLayout sCPhotoRlyContainer;
    @BindView(R.id.write_que_bbs_add_photo_rly)
    RelativeLayout sCPhotoRly;
    //    @BindView(R.id.input_wenzi_iv)
//    ImageView inputWenziIv;
//    @BindView(R.id.write_que_bbs_input_rly)
//    RelativeLayout sRFontRly;
    @BindView(R.id.setting_item_iv1)
    ImageView settingItemIv1;
    @BindView(R.id.set_photo_public_tv1)
    TextView setPhotoPublicTv1;
    @BindView(R.id.huifu_new_iv2)
    ImageView newIv2;
    @BindView(R.id.suibian_biaoqing_input_rly1)
    RelativeLayout biaoqingBt1;
    //    @BindView(R.id.colse_imbt_photo_bt)
//    ImageButton colsePhto;
//    @BindView(R.id.write_question_tips_photo_rly)
//    RelativeLayout tipsPhoto;
//    @BindView(R.id.noScrollgridview_bbs)
//    GridView gridview;
//    @BindView(R.id.selectimg_horizontalScrollView4)
//    HorizontalScrollView selectimg_horizontalScrollView;
    @BindView(R.id.bbs_input_photo_ly)
    LinearLayout photoLy;
    @BindView(R.id.set_photo_public_tv)
    TextView ifPhotoTv;
    @BindView(R.id.setting_item_iv)
    ImageView settingItemIv;
    @BindView(R.id.set_switch_photo_rly)
    RelativeLayout photoIfPublic;
    @BindView(R.id.biaoqingshuru_up_line1)
    View biaoqingshuruUpLine1;
    @BindView(R.id.colse_biaoqingjian_bt1)
    ImageButton closeImBt;
    @BindView(R.id.chacha_lr1)
    RelativeLayout chachaLr1;
    @BindView(R.id.viewpager11)
    ViewPager viewPager1;
    @BindView(R.id.page0_select1)
    ImageView page01;
    @BindView(R.id.page1_select1)
    ImageView page11;
    @BindView(R.id.page_select1)
    LinearLayout pageSelect1;
    @BindView(R.id.biaoqing_shuru_content_ly11)
    LinearLayout biaoqingContentLy;
    @BindView(R.id.set_switch_photo_rly1)
    RelativeLayout content1;
    @BindView(R.id.set_switch_photo_rly2)
    RelativeLayout content2;

    @BindView(R.id.rv_pic)
    RecyclerView rv_pic;
    @BindView(R.id.tv_photo)
    TextView tv_photo;
    private Activity mContext;
    public Pattern emoji;
    private float dp;
    private String sumbitContent2;
    //    private GridAdapter adapter;
    private PicAdapter adapter;
    public static final int REQUEST_CODE = 732;
    public static final int PHOTO_IF_PUBLIC = 4;
    private ImageView page0;
    private ImageView page1;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private int[] expressionImages1s;
    private String[] expressionImageNames1s;
    private ArrayList<GridView> grids;
    private GridView gView1;
    private ArrayList<GridView> grids1;
    private GridView gView2;
    private boolean isPhoto = false;
    private static final String TAG = "DiaryCommentDialogView";
    ArrayList<JSONObject> typeData = new ArrayList<>();           //图片上传完成后返回的图片地址集合。key是本地存储路径，vle是服务器连接
    ArrayList<String> mResultsFinal = new ArrayList<>();           //图片压缩完成后图片本地地址集合。key是本地存储路径，vle是压缩后的本地路径
    public static final int IMAG_PROGRESS = 10;     //照片进度
    public static final int IMAG_SUCCESS = 11;     //照片上传成功
    public static final int IMAG_FAILURE = 12;     //照片上传失败
    private Handler mHandler = new MyHandler(this);
    private String mKey;
    private Gson mGson;

    private static class MyHandler extends Handler {
        private final WeakReference<DiaryCommentDialogView> mActivity;

        public MyHandler(DiaryCommentDialogView activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            DiaryCommentDialogView theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case IMAG_PROGRESS:
                        break;
                    case IMAG_SUCCESS:
                        String imgUrl = (String) msg.obj;
                        int imageWidth = msg.arg1;
                        int imageHeight = msg.arg2;
                        Log.e(TAG, "imgUrl == " + imgUrl);
                        Log.e(TAG, "imageWidth == " + imageWidth);
                        Log.e(TAG, "imageHeight == " + imageHeight);
                        JSONObject jsonObject = theActivity.setJson(imgUrl, imageWidth, imageHeight);
                        theActivity.typeData.add(jsonObject);
                        break;
                    case IMAG_FAILURE:
                        break;
                }
            }
        }
    }

    private JSONObject setJson(String imgUrl, int imageWidth, int imageHeight) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("width", imageWidth + "");
            jsonObject.put("height", imageHeight + "");
            jsonObject.put("img", imgUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public DiaryCommentDialogView(@NonNull Activity context, SumbitPhotoData sumbitPhotoDatas) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mDatas = sumbitPhotoDatas;
        emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.diary_comment_pop_buttom_dialog_new, null, false);
        ButterKnife.bind(this, view);

        mGson = new Gson();
        inputContentEt.setFocusable(true);
        inputContentEt.setFocusableInTouchMode(true);
        inputContentEt.requestFocus();

        findView(view);
        initBottHttp();
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);    //点击外部不可dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        //解决dilaog中EditText无法弹出输入的问题
        window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                showKeyboard();
            }
        });
    }

    void findView(View view) {
        // 回复的
        page0 = view.findViewById(R.id.page0_select);
        page1 = view.findViewById(R.id.page1_select);
        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;

        initViewPager1();

        String ifiv1 = Cfg.loadStr(mContext, "new1", "");
        if (ifiv1.length() > 0) {
            newIv2.setVisibility(View.GONE);
        }

        String ifiv2 = Cfg.loadStr(mContext, "new2", "");
        if (ifiv2.length() > 0) {
            newIv2.setVisibility(View.GONE);
        }

        iputly2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
//                photoLy.setVisibility(View.GONE);


                sCPhotoRly.setVisibility(View.VISIBLE);
//                sRFontRly.setVisibility(View.GONE);
                photoIfPublic.setVisibility(View.GONE);
                content1.setVisibility(View.GONE);
                content2.setVisibility(View.GONE);

                // 表情隐藏
                biaoqingContentLy.setVisibility(View.GONE);
                Log.e(TAG, "打开1111");
                showKeyboard();
            }
        });

        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                Log.e(TAG, "打开2222");
                showKeyboard();
            }
        });

        inputContentEt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
            }
        });

        // 回复的
        page01 = view.findViewById(R.id.page0_select1);
        page11 = view.findViewById(R.id.page1_select1);
        // 引入表情
        expressionImages1s = Expressions.expressionImgs;
        expressionImageNames1s = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;
        initViewPager2();

        biaoqingBt1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    Log.e(TAG, "关闭1111");
                    closeKeyboard(view);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
//                photoLy.setVisibility(View.GONE);
                content1.setVisibility(View.GONE);
                content2.setVisibility(View.GONE);
                Cfg.saveStr(mContext, "new2", "1");
                newIv2.setVisibility(View.GONE);
            }
        });

        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                biaoqingContentLy.setVisibility(View.GONE);
                Log.e(TAG, "打开3333");
                showKeyboard();
            }
        });

//        colsePhto.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                photoLy.setVisibility(View.GONE);
//                content1.setVisibility(View.GONE);
//                content2.setVisibility(View.GONE);
//                Log.e(TAG, "打开4444");
//                showKeyboard();
//            }
//        });

        photoIfPublic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent addIntent = new Intent();
                addIntent.putExtra("type", mDatas.getVisibility());
                addIntent.setClass(mContext, SwitchPhotoPublicIfActivity.class);
                mContext.startActivityForResult(addIntent, PHOTO_IF_PUBLIC);
            }
        });

        inputContentEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    sumbitBt1.setTextColor(Color.parseColor("#CCCCCC"));
                    sumbitBt1.setEnabled(false);
                } else {
                    sumbitBt1.setTextColor(Color.parseColor("#FF527F"));
                    sumbitBt1.setEnabled(true);
                }
            }
        });
    }

    private void initViewPager1() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        grids = new ArrayList<>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(mContext.getResources(), expressionImages[arg2 % expressionImages.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames[arg2].substring(1, expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    inputContentEt.append(spannableString);

                }

            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager1.setAdapter(mPagerAdapter);

        viewPager1.setOnPageChangeListener(new GuidePageChangeListener());
    }


    /**
     * 图片选择按钮是隐藏
     *
     * @param isGone:true:隐藏，false:显示
     */
    public void setPicturesChooseGone(boolean isGone) {
        if (isGone) {
            sCPhotoRlyContainer.setVisibility(View.GONE);
            photoLy.setVisibility(View.GONE);
        } else {
            sCPhotoRlyContainer.setVisibility(View.VISIBLE);
        }
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_focused));
                    page1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_focused));
                    page0.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<String, Object>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(mContext.getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                inputContentEt.append(spannableString);

                                // System.out.println("edit的内容 = " +
                                // spannableString);
                            }

                        }
                    });
                    break;
            }
        }
    }


    private void initViewPager2() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        grids1 = new ArrayList<>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages1s[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(mContext.getResources(), expressionImages1s[arg2 % expressionImages1s.length]);

                    bitmap = zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mContext, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames1s[arg2].substring(1, expressionImageNames1s[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames1s[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    inputContentEt.append(spannableString);
                    // System.out.println("edit的内容 = " + spannableString);
                }

            }
        });
        grids1.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids1.add(gView2);

        // grids.add(gView3);
        // System.out.println("GridView的长度 = " + grids.size());

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids1.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids1.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids1.get(position));
                return grids1.get(position);
            }
        };

        viewPager1.setAdapter(mPagerAdapter);
        // viewPager.setAdapter();

        viewPager1.setOnPageChangeListener(new GuidePageChangeListener1());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener1 implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page01.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_focused));
                    page11.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page11.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_focused));
                    page01.setImageDrawable(mContext.getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mContext, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                inputContentEt.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(mContext.getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mContext, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                inputContentEt.append(spannableString);

                            }
                            // System.out.println("edit的内容 = " + spannableString);
                        }
                    });
                    break;
            }
        }
    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth, double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
        return bitmap;
    }

    private void initBottHttp() {
        try {
//            inputContentEt.requestFocus();

            initRly();

            // 滑动图片
            dp = mContext.getResources().getDimension(R.dimen.dp);
//            gridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
            gridviewInit();
            // 取消
//            cancelRly1.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View arg0) {
//                    photoIfPublic.setVisibility(View.GONE);
//                    View view = getWindow().peekDecorView();
//                    if (view != null) {
//                        Log.e(TAG, "关闭2222");
//                        closeKeyboard(view);
//                    }
//
//                    photoLy.setVisibility(View.GONE);
//                    sRFontRly.setVisibility(View.GONE);
//                    content1.setVisibility(View.GONE);
//                    content2.setVisibility(View.GONE);
//
//                    // 表情隐藏
//                    biaoqingContentLy.setVisibility(View.GONE);
//                    dismiss();
////                    dismissDialog();
//                }
//            });
            // 提交
            sumbitBt1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    View view = getWindow().peekDecorView();
                    if (view != null) {
                        Log.e(TAG, "关闭3333");
                        closeKeyboard(view);
                    }
                    sumbitContent2 = inputContentEt.getText().toString().trim();

                    Matcher matcher = emoji.matcher(sumbitContent2);

                    if (matcher.find()) {
                        Toast.makeText(mContext, "暂不支持表情输入", Toast.LENGTH_SHORT).show();
                    } else {
                        if (sumbitContent2.length() > 0 && !"".equals(sumbitContent2)) {
                            if (sumbitContent2.length() > 1) {
                                if (Utils.isLoginAndBind(mContext)) {
                                    if (!isPhoto) {
                                        inputContentEt.setText("");
                                        postFileQue();
                                        dismiss();
                                    } else {
                                        if (typeData.size() == mResultsFinal.size()) {
                                            inputContentEt.setText("");
                                            postFileQue();
                                            dismiss();
                                        } else {
                                            Toast.makeText(mContext, "图片上传中请稍后...", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(mContext, "亲，内容至少要大于1个字哟！", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mContext, "内容不能为空", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 初始化图片列表形式
     */
    public void gridviewInit() {
//        adapter = new GridAdapter(mContext);
//        adapter.setSelectedPosition(0);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rv_pic.setLayoutManager(linearLayoutManager);
        adapter = new PicAdapter(mContext, mResults);
        rv_pic.setAdapter(adapter);

//        int size = 0;
//        if (mResults.size() < 9) {
//            size = mResults.size() + 1;
//        } else {
//            size = mResults.size();
//        }

        if (mResults.size() == 0) {
            photoLy.setVisibility(View.GONE);
        } else {
            if (mResults.size() >= 9) {
                upPhotoIv.setBackgroundResource(R.drawable.pic_gray_dialog);
                tv_photo.setTextColor(Color.parseColor("#CCCCCC"));
            } else {
                upPhotoIv.setBackgroundResource(R.drawable.pic_dialog);
                tv_photo.setTextColor(Color.parseColor("#666666"));
            }
            photoLy.setVisibility(View.VISIBLE);
        }
//
//        ViewGroup.LayoutParams params = gridview.getLayoutParams();
//        final int width = size * (int) (dp * 9.4f);
//        params.width = width;
//        gridview.setLayoutParams(params);
//        gridview.setColumnWidth((int) (dp * 9.4f));
//        gridview.setStretchMode(GridView.NO_STRETCH);
//        gridview.setNumColumns(size);
//        gridview.setAdapter(adapter);
//
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View v, final int pos, long arg3) {
//
//                //版本判断
//                if (Build.VERSION.SDK_INT >= 23) {
//
//                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
//                        @Override
//                        public void onGranted() {
//                            toXIangce(pos);
//                        }
//
//                        @Override
//                        public void onDenied(List<String> permissions) {
//
//                        }
//                    });
//                } else {
//                    toXIangce(pos);
//                }
//
//            }
//        });
//
//        Log.e(TAG, "selectimg_horizontalScrollView == " + selectimg_horizontalScrollView);
//        selectimg_horizontalScrollView.getViewTreeObserver().addOnPreDrawListener(// 绘制完毕
//                new ViewTreeObserver.OnPreDrawListener() {
//                    public boolean onPreDraw() {
//                        selectimg_horizontalScrollView.scrollTo(width, 0);
//                        selectimg_horizontalScrollView.getViewTreeObserver().removeOnPreDrawListener(this);
//                        return false;
//                    }
//                });
    }

    void toXIangce(int pos) {
        if (pos == mResults.size()) {
            String sdcardState = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {

                // start multiple photos selector
                Intent intent = new Intent(mContext, ImagesSelectorActivity.class);
                // max number of images to be selected
                intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 4);
                // min size of image which will be shown; to filter tiny images (mainly icons)
                intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
                // show camera or not
                intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                // pass current selected images as the initial value
                intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
                // start the selector
                mContext.startActivityForResult(intent, REQUEST_CODE);

            } else {
                Toast.makeText(mContext, "sdcard已拔出，不能选择照片", Toast.LENGTH_SHORT).show();
            }

        } else {
            String paths = mResults.get(pos);
            Intent it = new Intent(mContext, EditImageActivity.class);
            it.putExtra(EditImageActivity.FILE_PATH, paths);
            File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
            it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
            it.putExtra("pos", pos + "");
            mContext.startActivityForResult(it, 9);

        }
    }


    /**
     * 图片列表
     *
     * @author lenovo17
     */
//    public class GridAdapter extends BaseAdapter {
//        private LayoutInflater listContainer;
//        private int selectedPosition = -1;
//        private boolean shape;
//
//        public boolean isShape() {
//            return shape;
//        }
//
//        public void setShape(boolean shape) {
//            this.shape = shape;
//        }
//
//        public class ViewHolder {
//            public ImageView image;
//            public Button bt;
//        }
//
//        public GridAdapter(Context context) {
//            listContainer = LayoutInflater.from(context);
//        }
//
//        public int getCount() {
//            if (mResults.size() < 9) {
//                return mResults.size() + 1;
//            } else {
//                return mResults.size();
//            }
//        }
//
//        public Object getItem(int arg0) {
//
//            return null;
//        }
//
//        public long getItemId(int arg0) {
//
//            return 0;
//        }
//
//        public void setSelectedPosition(int position) {
//            selectedPosition = position;
//        }
//
//
//        /**
//         * ListView Item设置
//         */
//        public View getView(int position, View convertView, ViewGroup parent) {
//            final int sign = position;
//            // 自定义视图
//            GridAdapter.ViewHolder holder = null;
//            if (convertView == null) {
//                holder = new GridAdapter.ViewHolder();
//                // 获取list_item布局文件的视图
//
//                convertView = listContainer.inflate(R.layout.item_published_grida1, null);
//
//                // 获取控件对象
//                holder.image = convertView.findViewById(R.id.item_grida_image);
//                holder.bt = convertView.findViewById(R.id.item_grida_bt);
//                // 设置控件集到convertView
//                convertView.setTag(holder);
//            } else {
//                holder = (GridAdapter.ViewHolder) convertView.getTag();
//            }
//
//            if (position == mResults.size()) {
//
//                holder.image.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.camera_gray3x));
//
//                holder.bt.setVisibility(View.GONE);
//                if (position == 9) {
//                    holder.image.setVisibility(View.GONE);
//                }
//            } else {
//                Glide.with(mContext).load(mResults.get(position)).into(holder.image);
//            }
//            return convertView;
//        }
//    }

    public class PicAdapter extends RecyclerView.Adapter<PicAdapter.ViewHolder> {

        private Context context;
        private List<String> data;

        public PicAdapter(Context context, List<String> data) {
            this.context = context;
            this.data = data;

        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_published_grida1, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
            Glide.with(mContext)
                    .load(mResults.get(position))
                    .transform(new CenterCrop(mContext), new GlideRoundTransform(mContext, Utils.dip2px(4)))
                    .into(holder.item_grida_image);
            holder.tv_index.setText(position + 1 + "/" + data.size());
            holder.item_grida_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mResults.remove(position);
                    if (mResults.size() >= 9) {
                        upPhotoIv.setBackgroundResource(R.drawable.pic_gray_dialog);
                        tv_photo.setTextColor(Color.parseColor("#CCCCCC"));
                    } else {
                        upPhotoIv.setBackgroundResource(R.drawable.pic_dialog);
                        tv_photo.setTextColor(Color.parseColor("#666666"));
                    }
                    notifyDataSetChanged();
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String paths = mResults.get(position);
                    Intent it = new Intent(mContext, EditImageActivity.class);
                    it.putExtra(EditImageActivity.FILE_PATH, paths);
                    File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
                    it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
                    it.putExtra("pos", position + "");
                    mContext.startActivityForResult(it, 9);

                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView item_grida_image;
            private Button item_grida_bt;
            private TextView tv_index;

            public ViewHolder(View itemView) {
                super(itemView);
                item_grida_image = itemView.findViewById(R.id.item_grida_image);
                item_grida_bt = itemView.findViewById(R.id.item_grida_bt);
                tv_index = itemView.findViewById(R.id.tv_index);
            }
        }
    }


    public void initRly() {
        if (TextUtils.isEmpty(mDatas.getNickname())) {
            inputContentEt.setHint("来都来了，不评论一句吗");
        } else {
            inputContentEt.setHint("回复:" + mDatas.getNickname());
        }

        sCPhotoRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                photoLy.setVisibility(View.VISIBLE);
//                sCPhotoRly.setVisibility(View.GONE);
//                sRFontRly.setVisibility(View.VISIBLE);

                biaoqingContentLy.setVisibility(View.GONE);
                if ("0".equals(mDatas.getAskorshare())) {
                    photoIfPublic.setVisibility(View.VISIBLE);
                    content1.setVisibility(View.VISIBLE);
                } else {
                    //不知道为啥
//                    content1.setVisibility(View.VISIBLE);
//                    content2.setVisibility(View.VISIBLE);
                }
                //如果软键盘已经出现 关闭软键盘
//                View view = getWindow().peekDecorView();
//                if (view != null) {
//                    Log.e(TAG, "关闭4444");
//                    closeKeyboard(view);
//                }
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            selectImg();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {

                        }
                    });
                } else {
                    selectImg();
                }
            }
        });

//        sRFontRly.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                inputContentEt.setEnabled(true);
//                photoLy.setVisibility(View.GONE);
//                sCPhotoRly.setVisibility(View.VISIBLE);
//                sRFontRly.setVisibility(View.GONE);
//                photoIfPublic.setVisibility(View.GONE);
//                content1.setVisibility(View.GONE);
//                content2.setVisibility(View.GONE);
//
//                biaoqingContentLy.setVisibility(View.GONE);
//
//                Log.e(TAG, "打开5555");
//                showKeyboard();
//            }
//        });

    }

    private void selectImg() {
        String sdcardState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {
            // start multiple photos selector
            Intent intent = new Intent(mContext, ImagesSelectorActivity.class);
            // max number of images to be selected
            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 9);
            // min size of image which will be shown; to filter tiny images (mainly icons)
            intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 100000);
            // show camera or not
            intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
            // pass current selected images as the initial value
            intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults);
            // start the selector
            mContext.startActivityForResult(intent, REQUEST_CODE);
        } else {
            Toast.makeText(mContext, "sdcard已拔出，不能选择照片", Toast.LENGTH_SHORT).show();
        }
    }

    void upLoadFile() {
        if (mResults.size() > 0) {
            for (int i = 0; i < mResults.size(); i++) {
                // 压缩图片
                String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
                File path1 = new File(pathS);// 建立这个路径的文件或文件夹
                if (!path1.exists()) {// 如果不存在，就建立文件夹
                    path1.mkdirs();
                }
                File file = new File(path1, "yuemei_" + i + System.currentTimeMillis() + ".JPEG");
                String desPath = file.getPath();
                FileUtils.compressPicture(mResults.get(i), desPath);
                int[] mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
                mKey = QiNuConfig.getKey();
                if (i == mResults.size() - 1) {
                    MyUploadImage.getMyUploadImage(mContext, mHandler, desPath, true).upCommentsList(mKey, mImageWidthHeight);
                } else {
                    MyUploadImage.getMyUploadImage(mContext, mHandler, desPath, false).upCommentsList(mKey, mImageWidthHeight);
                }
                mResultsFinal.add(desPath);
            }
        }
    }


    /**
     * 上传图片文件
     */
    public void postFileQue() {

        Log.e(TAG, "userid == " + mDatas.getUserid());
        Log.e(TAG, "docid == " + mDatas.get_id());
        Log.e(TAG, "qid == " + mDatas.getQid());
        Log.e(TAG, "cid == " + mDatas.getCid());
        Log.e(TAG, "askorshare ==" + mDatas.getAskorshare());
        Log.e(TAG, "sumbitContent2 == " + sumbitContent2);
        Log.e(TAG, "visibility == " + mDatas.getVisibility());

        HashMap<String, Object> maps = new HashMap<>();
        maps.put("userid", mDatas.getUserid());
        maps.put("docid", mDatas.get_id());
        maps.put("qid", mDatas.getQid());
        maps.put("cid", mDatas.getCid());
        maps.put("type", "1");
        maps.put("askorshare", mDatas.getAskorshare());
        maps.put("content", mGson.toJson(new ForumTextData(sumbitContent2)));
        maps.put("visibility", mDatas.getVisibility());
        if (typeData.size() > 0) {
            maps.put("image", typeData.toString());
        }
        Log.e(TAG, "image == " + typeData.toString());

        new SumbitPhotoApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (!serverData.isOtherCode) {
                    MyToast.makeTextToast2(mContext, serverData.message, MyToast.SHOW_TIME).show();
                }
                if ("1".equals(serverData.code)) {
                    try {
                    Log.e(TAG, "serverData == " + serverData.data);
                    SumbitPhotoBean sumbitPhotoBean = JSONUtil.TransformSingleBean(serverData.data, SumbitPhotoBean.class);
                    if (onSubmitClickListener != null) {
                        onSubmitClickListener.onSubmitClick(sumbitPhotoBean.get_id(), mResultsFinal, sumbitContent2);
                    }
                    Utils.sumitHttpCode(mContext, "7");
                    inputContentEt.setText("");
                    // 清理图片缓存
                    mResults.clear();
                    typeData.clear();
                    gridviewInit();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    //提交接口回调
    public interface OnSubmitClickListener {
        void onSubmitClick(String id, ArrayList<String> imgLists, String content);
    }

    private OnSubmitClickListener onSubmitClickListener;

    public void setOnSubmitClickListener(OnSubmitClickListener onSubmitClickListener) {
        this.onSubmitClickListener = onSubmitClickListener;
    }

    /**
     * 显示
     */
    public void showDialog() {
        show();
        Log.e(TAG, "showDialog......");
    }

    public void setmResults(ArrayList<String> mResults) {
        this.mResults = mResults;
        upLoadFile();
        isPhoto = true;

    }

    /**
     * 设置是否是私密
     *
     * @param photoTvTitle
     * @param isPublic
     */
    public void setIsPublic(String photoTvTitle, String isPublic) {
        ifPhotoTv.setText(photoTvTitle);
        mDatas.setVisibility(isPublic);
    }

    /**
     * 打开键盘
     */
    public void showKeyboard() {
        inputContentEt.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("302","showKeyboard");
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }, 200);
    }

    /**
     * 关闭键盘
     *
     * @param view
     */
    private void closeKeyboard(View view) {
        InputMethodManager inputmanger = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
