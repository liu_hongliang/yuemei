package com.module.commonview.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.module.commonview.adapter.GoodsGroupViewpagerAdapter;
import com.module.commonview.module.bean.DiaryTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/8/6
 */
public class ViewPagerPageIndicator extends FrameLayout {

    private final Context mContext;

    public ViewPagerPageIndicator(Context context) {
        this(context, null);
    }

    public ViewPagerPageIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ViewPagerPageIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    public void initView(List<View> views, final List<DiaryTaoData> datas, int pagerHigh) {
        removeAllViews();
        View view = View.inflate(mContext, R.layout.view_pager_page_indicator_view, this);
        LinearLayout pageIndicator = view.findViewById(R.id.view_pager_page_indicator_view);
        ViewPager pager = view.findViewById(R.id.view_pager_page_indicator_pager);
        UnderlinePageIndicator indicator = view.findViewById(R.id.view_pager_page_indicator_indicator);

        //初始化淘数据
        GoodsGroupViewpagerAdapter adapter2 = new GoodsGroupViewpagerAdapter(views);
        pager.setAdapter(adapter2);

        //设置高度
        ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) pageIndicator.getLayoutParams();
        if (datas.size() > 1) {
            indicator.setVisibility(View.VISIBLE);
            indicator.setViewPager(pager);
            layoutParams.height = pagerHigh + Utils.dip2px(6);
        } else if (datas.size() > 0) {
            pager.setVisibility(View.VISIBLE);
            indicator.setVisibility(View.GONE);
            layoutParams.height = pagerHigh;
        } else {
            pager.setVisibility(View.GONE);
            indicator.setVisibility(View.GONE);
            layoutParams.height = 0;
        }
        pageIndicator.setLayoutParams(layoutParams);

        //item点击
        adapter2.setOnItemCallBackListener(new GoodsGroupViewpagerAdapter.ItemCallBackListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (itemCallBackListener != null) {
                    itemCallBackListener.onItemClick(v, pos, datas.get(pos));
                }
            }
        });
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos, DiaryTaoData data);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}
