package com.module.commonview.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.net.http.SslError;
import android.nfc.Tag;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.web.WebUtil;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChatJumpPopwindow extends PopupWindow {

    private final TextView mTitle;
    private final WebView mWebView;
    private BaseWebViewClientMessage mWebViewClientMessage;
    private MapButtomDialogView mapButtomDialogView;

    @SuppressLint("SetJavaScriptEnabled")
    public ChatJumpPopwindow(final Activity context, String url){
        final View view = View.inflate(context, R.layout.chat_jump_pop, null);
        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_H, 0);
        setHeight((int) (windowH * 0.88));
        setFocusable(true);
        setContentView(view);
        update();


        mTitle = view.findViewById(R.id.chat_jump_title);
        LinearLayout close=view.findViewById(R.id.chat_jump_close);
        LinearLayout webContainer=view.findViewById(R.id.web_container);
        mWebViewClientMessage = new BaseWebViewClientMessage(context);
        mapButtomDialogView = new MapButtomDialogView(context);
        mWebView = new WebView(context);
        mWebView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        webContainer.addView(mWebView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        WebSettings settings = mWebView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);//支持通过JS打开新窗口
        mWebView.getSettings().setDatabaseEnabled(true);//开启数据库
        mWebView.setFocusable(true);//获取焦点
        mWebView.requestFocus();
        String dir = context.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();//设置数据库路径
        mWebView.getSettings().setCacheMode(mWebView.getSettings().LOAD_CACHE_ELSE_NETWORK);//本地缓存
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mWebView.getSettings().setBlockNetworkImage(false);//显示网络图像
        mWebView.getSettings().setBlockNetworkLoads(false);
        mWebView.getSettings().setLoadsImagesAutomatically(true);//显示网络图像
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);//插件支持
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.getSettings().setLoadWithOverviewMode(true);// 缩放至屏幕的大小
        mWebView.getSettings().setUseWideViewPort(false);// 将图片调整到适合webview大小
        mWebView.getSettings().setBuiltInZoomControls(true);// 设置支持缩放
        mWebView.getSettings().setSupportZoom(false);//设置是否支持变焦
        mWebView.getSettings().setGeolocationEnabled(true);//定位

        mWebView.getSettings().setGeolocationDatabasePath(dir);//数据库
        mWebView.getSettings().setAllowFileAccess(true);

        mWebView.getSettings().setDomStorageEnabled(true);//缓存 （ 远程web数据的本地化存储）

        WebViewClient myWebViewClient = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    if (url.startsWith("type")) {
                        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                        parserWebUrl.parserPagrms(url);
                        JSONObject obj = parserWebUrl.jsonObject;
                        String mType = obj.getString("type");
                        String name = obj.getString("name");
                        String lon = obj.getString("lon");
                        String lat = obj.getString("lat");
                        if ("6752".equals(mType)) {
                            mapButtomDialogView.showView(name, lon, lat);
                        } else {
                            mWebViewClientMessage.showWebDetail(url);
                        }
                    } else {
                        WebUrlTypeUtil.getInstance(context).urlToApp(url, "0", "0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        };

        //建立对象
        mWebView.setWebViewClient(myWebViewClient);//调用
        mWebView.setWebChromeClient(new WebChromeClient() {

            //重写WebChromeClient的onGeolocationPermissionsShowPrompt
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                    mTitle.setText(title);

            }

        });
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        Uri mUri = Uri.parse(url);
        String[] params = mUri.getPath().split("/");
        for (int i = 4; i <params.length ; i++) {
            if (i % 2 == 0) {
                keys.add(params[i]);
            } else {
                values.add(params[i]);
            }
        }

        if (keys.size() == values.size()) {
            HashMap<String, Object> hashMap = new HashMap<>();
            for (int j = 0; j < keys.size(); j++) {
                hashMap.put(keys.get(j), values.get(j));
            }
            WebSignData addressAndHead = SignUtils.getAddressAndHead(url+"/", hashMap);
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


}
