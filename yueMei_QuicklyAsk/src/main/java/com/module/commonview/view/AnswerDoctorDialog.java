package com.module.commonview.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.module.commonview.PageJumpManager;
import com.module.commonview.adapter.ButtomDiaryDialogAdapter;
import com.module.commonview.adapter.ButtomDoctorDialogAdapter;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;

import org.xutils.common.util.DensityUtil;

import java.util.List;

//回答问题医生弹窗
public class AnswerDoctorDialog extends Dialog {

    private Context mContext;
    private List<DiaryTaoData> mDiaryTaoDatas;
    private PostListData mPostListData;
    private String mDiaryId;
    private String mType;

    public AnswerDoctorDialog(Context context, List<DiaryTaoData> diaryTaoDatas, String diaryId, String type, PostListData postListData) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mDiaryTaoDatas = diaryTaoDatas;
        this.mDiaryId = diaryId;
        this.mType = type;
        this.mPostListData = postListData;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.pop_doctor_button_dialog, null, false);
        onClickListener(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);           //点击外部是否可以dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = DensityUtil.dip2px(428);
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        LinearLayout mClose = view.findViewById(R.id.pop_diary_button_close);
        ListView buttomList = view.findViewById(R.id.pop_diary_button_list);

        ButtomDoctorDialogAdapter buttomDoctorDialogAdapter = new ButtomDoctorDialogAdapter(mContext, mPostListData);
        buttomList.setAdapter(buttomDoctorDialogAdapter);

        //跳转设置
        buttomList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (itemCallBackListener != null){
                    itemCallBackListener.onItemClick(view, position);
                }
                downDialog();
            }
        });

        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

        buttomDoctorDialogAdapter.setOnItemChatCallListener(new ButtomDoctorDialogAdapter.ItemChatCallListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if (Utils.noLoginChat()){
                    toChatActivity(pos);
                }else {
                    if (Utils.isLoginAndBind(mContext)) {
                        toChatActivity(pos);
                    }
                }
            }
        });

    }

    private void toChatActivity(int pos) {
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(mPostListData.getRecommend_doctor_list().get(pos).getHos_userid())
                .setObjId(mPostListData.getRecommend_doctor_list().get(pos).getDoctor_id())
                .setObjType("3")
                .setTitle(mPostListData.getRecommend_doctor_list().get(pos).getName())
                .setImg(mPostListData.getRecommend_doctor_list().get(pos).getAvatar())
                .setYmClass("108")
                .setYmId(mPostListData.getRecommend_doctor_list().get(pos).getId())
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                .setEvent_name("chat_hospital")
                .setEvent_pos("related_doctors")
                .setHos_id(mPostListData.getRecommend_doctor_list().get(pos).getHospital_id())
                .setDoc_id(mPostListData.getRecommend_doctor_list().get(pos).getDoctor_id())
                .setEvent_others(mPostListData.getRecommend_doctor_list().get(pos).getHospital_id())
                .setId(mPostListData.getRecommend_doctor_list().get(pos).getId())
                .setType(mType)
                .build();
        Utils.chatTongJi(mContext, tongJiParams);
    }


    /**
     * 关闭
     */
    private void downDialog() {
        dismiss();
    }

    //item点击回调
    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }

}
