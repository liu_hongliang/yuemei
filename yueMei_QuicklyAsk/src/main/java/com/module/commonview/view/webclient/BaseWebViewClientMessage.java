package com.module.commonview.view.webclient;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FanXianApi;
import com.module.commonview.module.bean.CashBackD;
import com.module.commonview.module.bean.Type5983Bean;
import com.module.commonview.view.FanxianPopupWindows;
import com.module.commonview.view.SkuVideoChatDialog;
import com.module.community.model.bean.FaceVdieoBean;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.view.LoadingProgress;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.BaoxianPopWindow;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * 公共的WebViewClient
 * Created by 裴成浩 on 2017/12/28.
 */
public class BaseWebViewClientMessage extends WebViewClient {

    private String mObjid;
    private String mSource;
    private Activity mActivity;
    private String TAG = "BaseWebViewClient";
    private View contentLy;
    private BaseWebViewClientCallback baseWebViewClientCallback;
    private BaseWebViewTel baseWebViewTel;
    private WebViewTypeOutside webViewTypeOutside;
    private boolean isTypeOutside = false;
    private String ztid5983 = "0";
    private String objType5983 = "0";
    private String title5983 = "";
    private String img5983 = "";
    private String url = "";
    private String mYmClass = "";
    private String mYmId = "";
    private LoadingProgress progress2;
    private WebView mWebView;
    private JSONObject mJsonObject;


    public BaseWebViewClientMessage(Activity activity) {
        this(activity, "0");
    }

    public BaseWebViewClientMessage(Activity activity, String source) {
        this(activity, source, "0");
    }

    public BaseWebViewClientMessage(Activity activity, String source, String objid) {
        this.mActivity = activity;
        this.mSource = source;
        this.mObjid = objid;
        progress2 = new LoadingProgress(mActivity);
    }

    public BaseWebViewClientMessage(Activity activity, String source, String objid, WebView webView) {
        this.mActivity = activity;
        this.mSource = source;
        this.mObjid = objid;
        this.mWebView = webView;
        progress2 = new LoadingProgress(mActivity);
    }

    /**
     * 加载资源
     *
     * @param view
     * @param url
     */
    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
        Log.e(TAG, "onLoadResource......");
    }

    /**
     * 应该重写Url加载
     *
     * @param view
     * @param url
     * @return
     */
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.e(TAG, "shouldOverrideUrlLoading......");
        WebView.HitTestResult hitTestResult = view.getHitTestResult();

        int hitType = hitTestResult.getType();
        Log.e(TAG, "hitTestResult == " + hitTestResult);
        Log.e(TAG, "hitType == " + hitType);
        Log.e(TAG, "url == " + url);

        if (isTypeOutside) {
            if (webViewTypeOutside != null) {
                webViewTypeOutside.typeOutside(view, url);
            }
        } else {
            if (url.startsWith("type")) {
                showWebDetail(url);
            } else if (url.startsWith("tel")) {
                if (baseWebViewTel != null) {
                    baseWebViewTel.tel(view, url);
                }
            } else {
                WebUrlTypeUtil.getInstance(mActivity).urlToApp(url, mSource, mObjid);
            }
        }
        return true;
    }


    /**
     * 加载开始
     *
     * @param view
     * @param url
     * @param favicon
     */
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        Log.e(TAG, "onPageStarted......");
    }

    /**
     * 加载完成
     *
     * @param view
     * @param url
     */
    @Override
    public void onPageFinished(WebView view, String url) {
        Log.e(TAG, "onPageFinished......");
        stopLoading();
        view.loadUrl("javascript:window.getShareData.OnGetShareData(" + "document.querySelector('meta[name=\"reply_info\"]').getAttribute('content')" + ");");
        super.onPageFinished(view, url);
    }

    /**
     * 在收到Ssl错误
     *
     * @param view
     * @param handler
     * @param error
     */
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        Log.e(TAG, "onReceivedSslError......");
        handler.proceed();
    }

    /**
     * 在收到错误
     *
     * @param view
     * @param request
     * @param error
     */
    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        Log.e(TAG, "onReceivedError111......" + request.toString());
        Log.e(TAG, "onReceivedError222......" + error.toString());
        super.onReceivedError(view, request, error);
    }


    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
        String mType;
        JSONObject obj = null;
        try {
            if (urlStr.contains("type:eq")) {
                ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
                parserWebUrl.parserPagrms(urlStr);
                obj = parserWebUrl.jsonObject;
                mType = obj.getString("type");
            } else {
                mJsonObject = new JSONObject(urlStr);
                mType = mJsonObject.optString("type");
            }

            switch (mType) {
                case "10":// 跳转讨论组首页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "11":// 跳转讨论组首页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "12":// 点击跳转至问题编辑页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "13":// 跳转到淘整形首页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "14":// 跳转到讨论组首页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "15":// 经验值有什么用
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "16":// 我的经验明细
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "411"://查看图片
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "418"://陶整形详情页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "431"://个人信息
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "432":// 写日记
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "441":// 订单详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "444"://去支付
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "445"://申请退款
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "446": //查看详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "447": //去逛逛
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "448":// 在线留言
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "511":// 医院详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "516"://帖子最终页：点击查看主贴
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "531":// 医院介绍
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,mWebView, urlStr);
                    break;
                case "522": //消息列表通知
                    Log.e(TAG,"mSource ==="+mSource);
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,null, urlStr,mSource);
                    break;
                case "532": // 医院服务 全部淘整形/医院相册
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "534":////医院评论
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,null, urlStr,mObjid);
                    break;
                case "535":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity, urlStr);
                    break;
                case "536": // 案例详情/查看图片
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "541":         // 医院地址
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "543": // 吐槽
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "544"://分享
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "999":// 悦美
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "1000"://专题
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5411":   // 预约流程
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5413":    // 费用说明
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5415": //改退规则
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5421":    // 写日记
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5426"://问答详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5445":    //银行卡限额
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5451":// 领取
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5461":    // 面部整形术前照片标准
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5462":    // 身体整形术前照片标准
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5463":    // 皮肤整形术前照片标准
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5464":    // 毛发整形术前照片标准
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5465":    // 牙齿整形术前照片标准
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5466":    //写日记关联
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5443":    //查看图片
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5801"://医生服务
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5902"://跳百科二级
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5903"://跳百科四级
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5921":    //代金劵使用说明
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5961"://百科搜索
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "5983"://自己的私信
                    Type5983Bean type5983Bean = new Type5983Bean();
                    type5983Bean.setTitle5983(title5983);
                    type5983Bean.setObjType5983(objType5983);
                    type5983Bean.setObjId(ztid5983);
                    type5983Bean.setImg5983(img5983);
                    type5983Bean.setUrl(url);
                    type5983Bean.setmYmClass(mYmClass);
                    type5983Bean.setmYmId(mYmId);
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,null,urlStr,type5983Bean);
                    break;
                case "5992":    //更多跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6001":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6002":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6003":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6008":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6009":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6011":  //返现完成度弹层
                    String id6011 = obj.getString("id");
                    getFanxianData6011(id6011);
                    break;
                case "6014":    //写日记
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6015":    // 查看图片
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6020":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6022":  // 领优惠券
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,mWebView,urlStr);
                    break;
                case "6061":// 观看直播
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6004"://整形百科
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6006": //医院跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6080"://保险弹窗
                    String taoid6080 = obj.getString("tao_id");
                    String urlss6080 = FinalConstant.TAO_BAOXIAN;
                    HashMap<String, Object> urlMap6080 = new HashMap<>();
                    urlMap6080.put("tao_id", taoid6080);
                    BaoxianPopWindow baoxianPop6080 = new BaoxianPopWindow(mActivity, urlss6080, urlMap6080);
                    baoxianPop6080.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                    break;
                case "6091"://悦美快速专题
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6092"://特价专场
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6101":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6103"://选择发帖类型
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6120":    // 修改返现信息
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6135"://完善资料
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6152"://社区问题客服之类的
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6180":// 我的订单列表
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6230": //精选日记列表页跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6231": //医生跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6232":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6310"://返回按钮
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6311"://图片发送
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6342"://跳转到问题详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6344":// 跳转到问题详情
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6351"://悦美
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6352":    //跳转到分享微信好友【小程序】
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6354":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6355":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6312":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,null,urlStr,mSource);
                    break;
                case "6361":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6460":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6482":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,mWebView,urlStr);
                    break;
                case "6491":            // 	跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6492":            // 新增/修改/查看 收货地址
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6493":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6494":            //	跳转到我的优惠券
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6495":            //	跳转到钱包明细
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6496":            // 会员续费
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6497":                                // 跳转到项目淘列表选中会员筛选
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6498":            // 领券中心
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6499":            //	跳转到颜值币商城
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "64991":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,mWebView,urlStr);
                    break;
                case "64995":            //	拉新分享
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6521":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6522": //我的
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6523":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6571": //重新加入购物车
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6651":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6671":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6701"://我的收藏
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6702"://我的订单-代写日记列表
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6721"://到签到页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6722"://我的资料页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6723"://日记列表
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6724":// 颜究院
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6741":// 项目页 type:eq:6741:and:medthod:eq:0:and:one_id:eq:292:and:two_id:eq:292:and:three_id:eq:292
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6751":        //外链接跳转
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6753": //关注
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,mWebView,urlStr,mObjid);
                    break;
                case "6821":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "6882":
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "7161"://显示筛选弹窗
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "7162"://匹配面诊医生
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "7163"://淘整形是否有视频面诊
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "7164"://视频面诊聊天页
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                case "7165"://匹配成功展示弹层
                    WebViewUrlLoading.getInstance().showWebDetail(mActivity,urlStr);
                    break;
                default:
                    if (baseWebViewClientCallback != null) {
                        baseWebViewClientCallback.otherJump(urlStr);
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void loadUrl(WebView webView, String mUrl, HashMap<String, Object> mSingStr) {
        startLoading();
        Log.e(TAG, "url111====" + mUrl);
        Log.e(TAG, "mSingStr====" + mSingStr);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(mUrl, mSingStr);
        Log.e(TAG, "url====" + addressAndHead.getUrl());
        Log.e(TAG, "httpHeaders====" + addressAndHead.getHttpHeaders());
        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    void getFanxianData6011(String id) {
        FanXianApi mFanXianApi = new FanXianApi();
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", id);
        mFanXianApi.getCallBack(mActivity, maps, new BaseCallBackListener<CashBackD>() {
            @Override
            public void onSuccess(CashBackD cashBack) {
                new FanxianPopupWindows(mActivity, contentLy, cashBack);
            }
        });
    }


    /**
     * 设置5983的参数
     *
     * @param ztid5983
     */
    public void setParameter5983(String ztid5983){
        setParameter5983(ztid5983, "0", "", "","", "", "");
    }
    public void setParameter5983(String ztid5983, String objType5983, String ymClass, String ymId) {
        setParameter5983(ztid5983, objType5983, "", "", ymClass, ymId);
    }

    public void setParameter5983(String ztid5983, String objType5983, String title, String img, String ymClass, String ymId) {
        setParameter5983(ztid5983, objType5983, title, img, "", ymClass, ymId);
    }

    //专题
    public void setParameter5983(String ztid5983, String objType5983, String title, String img, String url, String ymClass, String ymId) {
        this.ztid5983 = ztid5983;
        this.objType5983 = objType5983;
        this.title5983 = title;
        this.img5983 = img;
        this.url = url;
        this.mYmClass = ymClass;
        this.mYmId = ymId;

    }

    /**
     * 加载效果
     */
    public void startLoading() {
        progress2.startLoading();
    }

    /**
     * 对话框消失
     */
    public void stopLoading() {
        progress2.stopLoading();
    }

    /**
     * 这里432、5466、6080、6011、441
     */
    public void setView(View view) {
        this.contentLy = view;
    }

    /**
     * 其他跳转
     *
     * @param baseWebViewClientCallback
     */
    public void setBaseWebViewClientCallback(BaseWebViewClientCallback baseWebViewClientCallback) {
        this.baseWebViewClientCallback = baseWebViewClientCallback;
    }

    /**
     * 页面刷新回调
     *
     * @param baseWebViewReload
     */
    public void setBaseWebViewReload(BaseWebViewReload baseWebViewReload) {
    }

    /**
     * 第二种跳转回调
     *
     * @param baseWebViewTel
     */
    public void setBaseWebViewTel(BaseWebViewTel baseWebViewTel) {
        this.baseWebViewTel = baseWebViewTel;
    }


    /**
     * 设置回调
     */
    public void setTypeOutside(boolean typeOutside) {
        isTypeOutside = typeOutside;
    }

    /**
     * 全部shouldOverrideUrlLoading处理的回调
     *
     * @param webViewTypeOutside
     */
    public void setWebViewTypeOutside(WebViewTypeOutside webViewTypeOutside) {
        this.webViewTypeOutside = webViewTypeOutside;
    }

}
