package com.module.commonview.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

/**
 * Created by 裴成浩 on 2018/1/4.
 */

public class MessagePromptPopup extends PopupWindow{

    public MessagePromptPopup(Context context,String imgUrl,String str) {
        super(context);
        final View view = View.inflate(context, R.layout.pop_jump_windows, null);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(true);
        setOutsideTouchable(true);
        setContentView(view);
        update();

        ImageView head = view.findViewById(R.id.iv_jump_head);
        TextView content = view.findViewById(R.id.tv_jump_tip);
        Glide.with(context)
                .load(imgUrl)
                .transform(new GlideCircleTransform(context))
                .placeholder(R.drawable.ic_headpic_img)
                .error(R.drawable.ic_headpic_img)
                .into(head);

        content.setText(str);
        // 点击 之外 消失
        view.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = view.findViewById(R.id.message_prompt).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y > height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }

}
