package com.module.commonview.view;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.quicklyask.activity.R;

public class SkuCouposPopwindow extends PopupWindow {

    private Context mContext;

    public SkuCouposPopwindow (Context mContext,String money,String limitMoney ){
        final View view = View.inflate(mContext, R.layout.sku_coupos_pop, null);
//        setAnimationStyle(R.style.AnimBottom);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(true);
        setContentView(view);
        update();

        TextView couposContent=view.findViewById(R.id.sku_coupos_content);
        Button couposBtn=view.findViewById(R.id.sku_coupos_btn);
        String content="恭喜获得"+money+"元"+"订金优惠券，满"+limitMoney+"可用，领取后1天内使用有效";
        SpannableString spannableString = new SpannableString(content);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#F1595A")),4,(4+money.length()+1),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        couposContent.setText(spannableString);
        couposBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }
}
