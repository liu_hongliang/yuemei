package com.module.commonview.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.util.Log;

import com.quicklyask.activity.R;

/**
 * 可以控制RadioButton图片大小的控件
 * Created by 裴成浩 on 2018/9/28.
 */
public class SizeImageRadioButton extends AppCompatRadioButton {

    private final String TAG = "SizeImageRadioButton";
    private int drawableWidth;
    private int drawableHeight;
    private int drawablePadding;
    private int drawableLocation;
    private Drawable drawable;

    public SizeImageRadioButton(Context context) {
        this(context, null);
    }

    public SizeImageRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SizeImageRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SizeImageRadioButton);//获取定义的属性

        drawable = typedArray.getDrawable(R.styleable.SizeImageRadioButton_drawable);
        drawableWidth = (int) typedArray.getDimension(R.styleable.SizeImageRadioButton_drawable_width, 0);
        drawableHeight = (int) typedArray.getDimension(R.styleable.SizeImageRadioButton_drawable_height, 0);
        drawablePadding = (int) typedArray.getDimension(R.styleable.SizeImageRadioButton_drawable_padding, 0);
        drawableLocation = typedArray.getInt(R.styleable.SizeImageRadioButton_drawable_location, 0);
        Log.e(TAG, "drawableWidth == " + drawableWidth);
        Log.e(TAG, "drawableHeight == " + drawableHeight);
        Log.e(TAG, "drawablePadding == " + drawablePadding);
        Log.e(TAG, "drawableLocation == " + drawableLocation);

        initView();

    }

    public void initView() {
        if (drawable != null) {

            if (drawableWidth == 0 ) {
                drawableWidth = drawable.getIntrinsicWidth();
            }
            if ( drawableHeight == 0) {
                drawableHeight = drawable.getIntrinsicHeight();
            }

            switch (drawableLocation) {
                case 1:                        //左
                    drawable.setBounds(drawablePadding, 0, drawableWidth, drawableHeight);
                    setCompoundDrawables(drawable, null, null, null);
                    break;
                case 2:                        //上
                    drawable.setBounds(0, drawablePadding, drawableWidth, drawableHeight);
                    setCompoundDrawables(null, drawable, null, null);
                    break;
                case 3:                        //右
                    drawable.setBounds(drawablePadding, 0, drawableWidth, drawableHeight);
                    setCompoundDrawables(null, null, drawable, null);
                    break;
                case 4:                        //下
                    drawable.setBounds(0, drawablePadding, drawableWidth, drawableHeight);
                    setCompoundDrawables(null, null, null, drawable);
                    break;
            }
        }
    }

    public void setDrawableWidth(int drawableWidth) {
        this.drawableWidth = drawableWidth;
    }

    public void setDrawableHeight(int drawableHeight) {
        this.drawableHeight = drawableHeight;
    }

    public void setDrawablePadding(int drawablePadding) {
        this.drawablePadding = drawablePadding;
    }

    public void setDrawableLocation(int drawableLocation) {
        this.drawableLocation = drawableLocation;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
