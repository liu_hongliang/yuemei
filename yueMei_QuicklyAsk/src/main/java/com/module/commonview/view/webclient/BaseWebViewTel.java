package com.module.commonview.view.webclient;

import android.webkit.WebView;

/**
 * Created by 裴成浩 on 2018/1/9.
 */

public interface BaseWebViewTel {
    void tel(WebView view, String url);
}
