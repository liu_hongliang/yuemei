package com.module.commonview.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 自定义指示器
 * Created by 裴成浩 on 2018/5/22.
 */
public class UnderlinePageIndicator extends RelativeLayout implements ViewTreeObserver.OnGlobalLayoutListener {

    private String TAG = "UnderlinePageIndicator";
    private Context mContext;
    private int selectedColor;
    private int uncheckColor;
    private float mLineMarg;                 //指示器之间的间距
    private View mSelectedView;             //选中的线
    private LinearLayout mLinearLayout;     //未选中的指示器
    private int lineLeftMarg;               //指示器距离右边的间距
    private int viewWidth;                //指示器宽
    private int viewHeight;               //指示器高
    private RelativeLayout.LayoutParams mSelectedParams;      //选中view的参数
    private LinearLayout.LayoutParams mUncheckParams;       //未选中的view参数

    public UnderlinePageIndicator(Context context) {
        this(context, null);
    }

    public UnderlinePageIndicator(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UnderlinePageIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        selectedColor = R.color.button_bian_hong1;
        uncheckColor = R.color.line_background;
        mLineMarg = Utils.dip2px(6);
        viewWidth = Utils.dip2px(6);
        viewHeight = Utils.dip2px(6);

        initAttr(attrs);
        initView();
    }

    /**
     * 设置自定义属性
     *
     * @param attrs
     */
    private void initAttr(AttributeSet attrs) {
        TypedArray ta = mContext.obtainStyledAttributes(attrs, R.styleable.UnderlinePageIndicator);
        selectedColor = ta.getResourceId(R.styleable.UnderlinePageIndicator_selectedColor, selectedColor);
        uncheckColor = ta.getResourceId(R.styleable.UnderlinePageIndicator_uncheckColor, uncheckColor);
        mLineMarg = ta.getDimension(R.styleable.UnderlinePageIndicator_lineSpacing, mLineMarg);


        viewWidth = (int) ta.getDimension(R.styleable.UnderlinePageIndicator_viewWidth, viewWidth);
        viewHeight = (int) ta.getDimension(R.styleable.UnderlinePageIndicator_viewHeight, viewHeight);
    }


    /**
     * 初始化布局
     */
    private void initView() {
        mLinearLayout = new LinearLayout(mContext);
        mLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

        //设置选中指示器大小
        mSelectedParams = new RelativeLayout.LayoutParams(viewWidth, viewHeight);
        mSelectedParams.leftMargin = (int) mLineMarg / 2;

        //设置选中的指示器
        mSelectedView = new View(mContext);
        mSelectedView.getViewTreeObserver().addOnGlobalLayoutListener(this);

        //设置选中指示器颜色
        setSelectedColor(selectedColor);
        //设置未选中指示器颜色
        setUncheckColor(uncheckColor);
        //设置指示器间距
        setSpacingColor(mLineMarg);

        addView(mLinearLayout);
        addView(mSelectedView);
    }

    /**
     * 设置viewPage
     *
     * @param viewPager
     */
    public void setViewPager(ViewPager viewPager) {
        //设置未选中指示器大小
        mUncheckParams = new LinearLayout.LayoutParams(viewWidth, viewHeight);

        PagerAdapter adapter = viewPager.getAdapter();
        int count = 0;
        if (adapter != null) {
            count = adapter.getCount();
        }

        int marginNumber = (int) mLineMarg / 2;
        Log.e(TAG, "lineMarg === " + mLineMarg);
        Log.e(TAG, "marginNumber === " + marginNumber);
        for (int i = 0; i < count; i++) {
            View uncheckView = new View(mContext);     // 圆点
            uncheckView.setBackgroundResource(uncheckColor);
            if (i == 0) {
                mUncheckParams.rightMargin = marginNumber;
            } else if (i == count - 1) {
                mUncheckParams.leftMargin = marginNumber;
            } else {
                mUncheckParams.leftMargin = marginNumber;
                mUncheckParams.rightMargin = marginNumber;
            }
            mLinearLayout.addView(uncheckView, mUncheckParams);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (lineLeftMarg == 0 && mLinearLayout.getChildCount() > 1) {
                    lineLeftMarg = mLinearLayout.getChildAt(1).getLeft() - mLinearLayout.getChildAt(0).getLeft();
                }

                float lineLeftMargin = ((position + positionOffset) * lineLeftMarg);
                //距离左边的距离
                mSelectedParams.leftMargin = (int) (mLineMarg / 2 + lineLeftMargin);
                mSelectedView.setLayoutParams(mSelectedParams);
            }

            @Override
            public void onPageSelected(int position) {
                Log.e(TAG, "position2 == " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e(TAG, "state == " + state);
            }
        });
    }

    /**
     * 设置指示器选中的颜色
     *
     * @param selectedColor
     */
    public void setSelectedColor(int selectedColor) {
        mSelectedView.setBackgroundResource(selectedColor);
    }

    /**
     * 设置指示器未选中的颜色
     *
     * @param uncheckColor
     */
    public void setUncheckColor(int uncheckColor) {
        for (int i = 0; i < mLinearLayout.getChildCount(); i++) {
            View uncheckView = mLinearLayout.getChildAt(i);
            uncheckView.setBackgroundResource(uncheckColor);
        }
    }

    /**
     * 设置指示器行间距
     *
     * @param lineMarg
     */
    public void setSpacingColor(float lineMarg) {
        this.mLineMarg = lineMarg;

        //选中的设置
        mSelectedView.setLayoutParams(mSelectedParams);

        //未选中的设置
        for (int i = 0; i < mLinearLayout.getChildCount(); i++) {
            View uncheckView = mLinearLayout.getChildAt(i);
            if (i == 0) {
                mUncheckParams.rightMargin = (int) mLineMarg / 2;
            } else if (i == mLinearLayout.getChildCount() - 1) {
                mUncheckParams.leftMargin = (int) mLineMarg / 2;
            } else {
                mUncheckParams.leftMargin = (int) mLineMarg / 2;
                mUncheckParams.rightMargin = (int) mLineMarg / 2;
            }
            uncheckView.setLayoutParams(mUncheckParams);
        }

    }

    @Override
    public void onGlobalLayout() {
        mSelectedView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

        //间距 = 第1个点距离左边距离 - 第0个点距离左边距离
        lineLeftMarg = 0;
        if (mLinearLayout.getChildCount() > 1) {
            lineLeftMarg = mLinearLayout.getChildAt(1).getLeft() - mLinearLayout.getChildAt(0).getLeft();
        }

    }
}
