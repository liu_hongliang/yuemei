package com.module.commonview.view;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.my.model.bean.ProjcetList;
import com.quicklyask.activity.R;

import java.util.List;

public class VideoChatMarkingAdapter extends BaseQuickAdapter<ProjcetList, BaseViewHolder> {
    public VideoChatMarkingAdapter(int layoutResId, @Nullable List<ProjcetList> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProjcetList item) {
        Glide.with(mContext).load("").placeholder(R.color.red_ff5c77).into((ImageView) helper.getView(R.id.marking_img));
        helper.setText(R.id.marking_title,item.getPostName())
                .setText(R.id.marking_price,item.getPostVal());

    }
}
