package com.module.commonview.view;

public interface ProgressListener {

    void progress(long bytesRead, long contentLength, boolean done);

}
