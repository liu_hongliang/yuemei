package com.module.commonview.view;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by zhaoyong
 * on 2016/8/2.
 */
public class DropDownMultiPagerAdapter extends PagerAdapter {

    private List<View> list;

    DropDownMultiPagerAdapter(List<View> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = list.get(position);
        Log.e("DropDownMultiAdapter","position =="+position);
        view.setTag(position);
        container.addView(view, 0);
        return list.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
