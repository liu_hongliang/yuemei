package com.module.commonview.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.view.CenterAlignImageSpan;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取日记本中SKU item的view
 * Created by 裴成浩 on 2019/3/28
 */
public class DiarySkuItemView {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final String mDiaryId;
    private final FunctionManager mFunctionManager;
    private String TAG = "DiarySkuItemView";
    private String mIsDiaryOrDetail;// 1 日记本 2 日记详情  3 帖子

    public DiarySkuItemView(Context context, String diaryId, String isDiaryOrDetail) {
        this.mContext = context;
        this.mDiaryId = diaryId;
        mIsDiaryOrDetail = isDiaryOrDetail;
        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    /**
     * 初始化下单商品
     */
    @SuppressLint("SetTextI18n")
    public List<View> initTaoData(List<DiaryTaoData> diaryTaoDatas, ViewPager otherGoodsGroup) {
        List<View> views = new ArrayList<>();

        boolean haveDacu = false;

        for (int i = 0; i < diaryTaoDatas.size(); i++) {
            final DiaryTaoData taoData = diaryTaoDatas.get(i);

            View view = mInflater.inflate(R.layout.item_diary_list_goods_group, null);
            TextView goodPrompt = view.findViewById(R.id.diary_list_goods_prompt);                      //提示
            RelativeLayout dacuContainer = view.findViewById(R.id.diary_list_goods_dacu);             //大促图容器
            ImageView dacuImg = view.findViewById(R.id.diary_list_goods_dacu_img);                    //大促图
            TextView dacuText = view.findViewById(R.id.diary_list_goods_dacu_text);                    //查看大促详情按钮
            ImageView skuImg = view.findViewById(R.id.diary_list_goods_sku_img);                        //图片
            LinearLayout plusPriceVibiable = view.findViewById(R.id.diary_list_plus_vibiable);                    //图片
            TextView plusPrice = view.findViewById(R.id.plus_price);                    //图片
            TextView viewContent = view.findViewById(R.id.diary_list_goods_title);
            TextView diary_list_goods_instructions = view.findViewById(R.id.diary_list_goods_instructions);//说明
            final TextView btn = view.findViewById(R.id.diary_list_goods_btn);

            final String is_rongyun = taoData.getIs_rongyun();
            if ("3".equals(is_rongyun)) {
                btn.setVisibility(View.VISIBLE);
            } else {
                btn.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(taoData.getFanxian())) {
                diary_list_goods_instructions.setVisibility(View.VISIBLE);
            } else {
                diary_list_goods_instructions.setVisibility(View.GONE);
            }
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClick(taoData);
                }
            });

            Log.e(TAG, "taoData.getList_cover_image() == " + taoData.getList_cover_image());
            mFunctionManager.setRoundImageSrc(skuImg, taoData.getList_cover_image(), Utils.dip2px(5));
            if ("3".equals(mIsDiaryOrDetail)) {
                goodPrompt.setText("文中相关商品");
            } else {
                goodPrompt.setText("下单商品");
            }

            //大促
            if ("1".equals(taoData.getDacu66_id())) {
                SpannableString spanString = new SpannableString("大促" + taoData.getTitle());

                CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.cu_tips_2x), Utils.dip2px(15), Utils.dip2px(15)));

                spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                viewContent.setText(spanString);                   //文案

                if (!TextUtils.isEmpty(taoData.getDacu66_img())) {
                    haveDacu = true;
                    dacuContainer.setVisibility(View.VISIBLE);
                    Glide.with(mContext).load(taoData.getDacu66_img()).bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(6), GlidePartRoundTransform.CornerType.TOP)).into(dacuImg);
                    dacuContainer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!TextUtils.isEmpty(taoData.getDacu66_url())) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(taoData.getDacu66_url(), "0", "0");
                            }
                        }
                    });
                } else {
                    dacuContainer.setVisibility(View.GONE);
                }

            } else {
                viewContent.setText(taoData.getTitle());
                dacuContainer.setVisibility(View.GONE);
            }

            String member_price = taoData.getMember_price();

            mFunctionManager.setTextValue(view, R.id.diary_list_goods_price, taoData.getPrice_discount());           //价格
            int memberPrice = Integer.parseInt(member_price);
            if (memberPrice >= 0) {
                plusPriceVibiable.setVisibility(View.VISIBLE);
                plusPrice.setText("¥" + member_price);
            } else {
                plusPriceVibiable.setVisibility(View.GONE);
            }

            mFunctionManager.setTextValue(view, R.id.diary_list_goods_unit, taoData.getFee_scale());                 //单位
            mFunctionManager.setTextValue(view, R.id.diary_list_goods_instructions, taoData.getFanxian());           //说明

            views.add(view);
        }

        ViewGroup.LayoutParams layoutParams = otherGoodsGroup.getLayoutParams();
        if (haveDacu) {
            layoutParams.height = Utils.dip2px(190);
        } else {
            layoutParams.height = Utils.dip2px(160);
        }
        otherGoodsGroup.setLayoutParams(layoutParams);

        return views;
    }

    private void ItemClick(DiaryTaoData taoData) {
        String eventPos = "";
        final String hos_userid = taoData.getHos_userid();
        final String taoid = taoData.getId();
        final String title = taoData.getTitle();
        final String price_discount = taoData.getPrice_discount();
        final String member_price = taoData.getMember_price();
        final String list_cover_image = taoData.getList_cover_image();
        if (Utils.noLoginChat()) {
            toChatActivity(taoData, eventPos, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
        } else {
            if (Utils.isLoginAndBind(mContext)) {
                toChatActivity(taoData, eventPos, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
            }
        }

    }

    private void toChatActivity(DiaryTaoData taoData, String eventPos, String hos_userid, String taoid, String title, String price_discount, String member_price, String list_cover_image) {
        if ("1".equals(mIsDiaryOrDetail)) {
            eventPos = "diary_tao";
        } else if ("2".equals(mIsDiaryOrDetail)) {
            eventPos = "share_tao";
        }
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("102")
                .setYmId(mDiaryId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                .setEvent_name("chat_hospital")
                .setEvent_pos(eventPos)
                .setHos_id(taoData.getHospital_id())
                .setDoc_id(taoData.getDoc_id())
                .setTao_id(taoData.getId())
                .setEvent_others(taoData.getHospital_id())
                .setId(mDiaryId)
                .setReferrer("17")
                .setType("2")
                .build();
        Utils.chatTongJi(mContext, tongJiParams);
    }
}
