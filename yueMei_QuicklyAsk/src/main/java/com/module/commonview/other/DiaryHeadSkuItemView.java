package com.module.commonview.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryHosDocBean;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.view.CenterAlignImageSpan;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 首页头部SKU滑动
 * Created by 裴成浩 on 2019/8/6
 */
public class DiaryHeadSkuItemView {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final String mDiaryId;
    private String mType;//1日记本 2日记 3详情
    private final FunctionManager mFunctionManager;
    private String TAG = "DiaryHeadSkuItemView";

    public DiaryHeadSkuItemView(Context context, String diaryId,String type) {
        this.mContext = context;
        this.mDiaryId = diaryId;
        this.mType = type;
        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    /**
     * 初始化下单商品
     */
    @SuppressLint("SetTextI18n")
    public List<View> initTaoData(List<DiaryTaoData> diaryTaoDatas, final DiaryHosDocBean hosDoc) {
        List<View> views = new ArrayList<>();

        for (int i = 0; i < diaryTaoDatas.size(); i++) {
            final DiaryTaoData taoData = diaryTaoDatas.get(i);

            View view = mInflater.inflate(R.layout.item_diary_head_list_goods_group, null);
            ImageView skuImg = view.findViewById(R.id.diary_list_goods_sku_img);
            TextView viewContent = view.findViewById(R.id.diary_list_goods_title);
            TextView price = view.findViewById(R.id.diary_list_goods_price);
            TextView originalPrice = view.findViewById(R.id.diary_list_goods_original_price);
            RelativeLayout plusPriceVibiable = view.findViewById(R.id.diary_list_plus_vibiable);
            TextView plusPrice = view.findViewById(R.id.plus_vip_price);
            final TextView btn = view.findViewById(R.id.diary_list_goods_btn);
            LinearLayout hosClick = view.findViewById(R.id.diary_list_goods_hos_click);
            TextView hosName = view.findViewById(R.id.diary_list_goods_hos_name);
            TextView hosDistance = view.findViewById(R.id.diary_list_goods_hos_distance);
            LinearLayout docClick = view.findViewById(R.id.diary_list_goods_doc_click);
            TextView docName = view.findViewById(R.id.diary_list_goods_doc_name);

            LinearLayout ll_posts = view.findViewById(R.id.post_list_goods_doc_click);
            TextView post_title = view.findViewById(R.id.post_title);

            if(mType.equals("3")){
                //详情
                hosClick.setVisibility(View.GONE);
                docClick.setVisibility(View.GONE);
                if(TextUtils.isEmpty(taoData.getFanxian())){
                    ll_posts.setVisibility(View.GONE);
                }else{
                    ll_posts.setVisibility(View.VISIBLE);
                    mFunctionManager.setTextValue(view, R.id.post_title, taoData.getFanxian());           //说明
                }
            }else if(mType.equals("1")){
                //日记本
                hosClick.setVisibility(View.VISIBLE);
                docClick.setVisibility(View.VISIBLE);
                ll_posts.setVisibility(View.GONE);
                //医生医院设置
                final String hospital_id = hosDoc.getHospital_id();
                final String doctor_id = hosDoc.getDoctor_id();
                final String doctor_name = hosDoc.getDoctor_name();
                if (!TextUtils.isEmpty(hospital_id) && !"0".equals(hospital_id)) {
                    hosClick.setVisibility(View.VISIBLE);
                    hosName.setText(hosDoc.getHospital_name());
                    hosDistance.setText(hosDoc.getBusiness_district() + hosDoc.getDistance());
                } else {
                    hosClick.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(doctor_id) && !"0".equals(doctor_id)) {
                    docClick.setVisibility(View.VISIBLE);
                    String title = hosDoc.getTitle();
                    String doc_name;
                    if (!TextUtils.isEmpty(title)) {
                        doc_name = doctor_name + "·" + hosDoc.getTitle();
                    } else {
                        doc_name = doctor_name;
                    }
                    docName.setText(doc_name);
                } else {
                    docClick.setVisibility(View.GONE);
                }
            }else{
                //日记
                hosClick.setVisibility(View.GONE);
                docClick.setVisibility(View.GONE);
                if(TextUtils.isEmpty(taoData.getFanxian())){
                    ll_posts.setVisibility(View.GONE);
                }else{
                    ll_posts.setVisibility(View.VISIBLE);
                    mFunctionManager.setTextValue(view, R.id.post_title, taoData.getFanxian());           //说明
                }
            }

            //SKU图片优化
            mFunctionManager.setRoundImageSrc(skuImg, taoData.getList_cover_image(), Utils.dip2px(5));

            //SKU文案
            if ("1".equals(taoData.getDacu66_id())) {
                SpannableString spanString = new SpannableString("大促" + taoData.getTitle());
                CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.cu_tips_2x), Utils.dip2px(15), Utils.dip2px(15)));
                spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                viewContent.setText(spanString);                   //文案
            } else {
                viewContent.setText(taoData.getTitle());
            }

            //咨询按钮显示与隐藏
            final String is_rongyun = taoData.getIs_rongyun();
            if ("3".equals(is_rongyun)) {
                btn.setVisibility(View.VISIBLE);
            } else {
                btn.setVisibility(View.GONE);
            }

            //咨询按钮点击
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClick(taoData);
                    if(onEventClickListener != null){
                        onEventClickListener.onItemClick(v,taoData);
                    }
                }
            });

            //医院
            hosClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String hospital_url = hosDoc.getHospital_url();
                    if (!TextUtils.isEmpty(hospital_url)) {
                        HashMap<String, String> event_params = hosDoc.getEvent_params();
                        if (event_params == null) {
                            event_params = new HashMap<>();
                        }
                        event_params.put("id", mDiaryId);
                        event_params.put("to_page_type", "4");
                        event_params.put("to_page_id", hosDoc.getHospital_id());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_HOSPITAL_CLICK), event_params, new ActivityTypeData("45"));

                        WebUrlTypeUtil.getInstance(mContext).urlToApp(hospital_url);
                    }
                }
            });

            //医生
            docClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String doctor_url = hosDoc.getDoctor_url();
                    if (!TextUtils.isEmpty(doctor_url)) {
                        HashMap<String, String> event_params = hosDoc.getEvent_params();
                        if (event_params == null) {
                            event_params = new HashMap<>();
                        }
                        event_params.put("id", mDiaryId);
                        event_params.put("to_page_id", hosDoc.getDoctor_id());
                        event_params.put("to_page_type", "3");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.DIARY_DOCTORS_CLICK), event_params, new ActivityTypeData("45"));
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(doctor_url);
                    }
                }
            });

            String member_price = taoData.getMember_price();
            String oPrice = taoData.getPrice();
            String priceDiscount = taoData.getPrice_discount();

            int memberPrice = Integer.parseInt(member_price);
            int o_price = Integer.parseInt(oPrice);
            int price_discount = Integer.parseInt(priceDiscount);
            price.setText(priceDiscount);
            if (memberPrice >= 0) {
                plusPriceVibiable.setVisibility(View.VISIBLE);
                plusPrice.setText("¥" + member_price);
            } else {
                plusPriceVibiable.setVisibility(View.GONE);
                if (o_price >= 0 && o_price != price_discount) {
                    originalPrice.setVisibility(View.VISIBLE);
                    originalPrice.setText("¥" + o_price);
                    originalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
                } else {
                    originalPrice.setVisibility(View.GONE);
                }
            }

            views.add(view);
        }

        return views;
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onItemClick(View view,DiaryTaoData data);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    private void ItemClick(DiaryTaoData taoData) {
        final String hos_userid = taoData.getHos_userid();
        final String taoid = taoData.getId();
        final String title = taoData.getTitle();
        final String price_discount = taoData.getPrice_discount();
        final String member_price = taoData.getMember_price();
        final String list_cover_image = taoData.getList_cover_image();
        if (Utils.noLoginChat()){
            toChatActivity(taoData, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
        }else {
            if (Utils.isLoginAndBind(mContext)) {
                toChatActivity(taoData, hos_userid, taoid, title, price_discount, member_price, list_cover_image);
            }
        }
    }

    private void toChatActivity(DiaryTaoData taoData, String hos_userid, String taoid, String title, String price_discount, String member_price, String list_cover_image) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id",taoid);
        hashMap.put("hos_id",taoData.getHospital_id());
        hashMap.put("doc_id",hos_userid);
        hashMap.put("tao_id",hos_userid);
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL,"share_tao",hos_userid),hashMap);
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("102")
                .setYmId(mDiaryId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
    }
}
