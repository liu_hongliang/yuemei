package com.module.commonview.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.PageJumpManager;
import com.module.commonview.fragment.PostBottomFragment;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.view.ViewPagerPageIndicator;
import com.module.commonview.view.YmVipPriceView;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/8/6
 */
public class DiaryTopSkuItemView {
    private final Context mContext;
    private final String mDiaryId;
    private String TAG = "DiarySkuItemView";

    public DiaryTopSkuItemView(Context context, String diaryId) {
        this.mContext = context;
        this.mDiaryId = diaryId;
    }

    /**
     * 初始化下单商品
     */
    @SuppressLint("SetTextI18n")
    public List<View> initTaoData(List<DiaryTaoData> diaryTaoDatas) {
        List<View> views = new ArrayList<>();

        for (int i = 0; i < diaryTaoDatas.size(); i++) {
            final DiaryTaoData taoData = diaryTaoDatas.get(i);

            View view = View.inflate(mContext, R.layout.item_diary_list_suck_top, null);
            ImageView mImg = view.findViewById(R.id.diary_list_suck_top_img);
            TextView mTitle = view.findViewById(R.id.diary_list_suck_top_title);
            TextView mPrice = view.findViewById(R.id.diary_list_suck_top_price);
            TextView mUnit = view.findViewById(R.id.diary_list_suck_top_unit);
            FrameLayout mPriceContainer = view.findViewById(R.id.diary_list_sku_price_container);
            TextView mOriginalPrice = view.findViewById(R.id.diary_list_suck_original_price);
            RelativeLayout mPlusVisibity = view.findViewById(R.id.tao_plus_vibility);
            TextView mPlusPrice = view.findViewById(R.id.tao_plus_vip_price);
            Button mToChat = view.findViewById(R.id.diary_list_suck_top_btn);

            Glide.with(mContext)
                    .load(taoData.getList_cover_image())
                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(7)))
                    .into(mImg);
            mTitle.setText(taoData.getTitle());               //文案
            String price_discount = taoData.getPrice_discount();
            mPrice.setText("¥" + price_discount);      //价格
            mUnit.setText(taoData.getFee_scale());              //单位

            String member_price = taoData.getMember_price();
            String price = taoData.getPrice();
            if (Integer.parseInt(member_price) >= 0) {
                mPriceContainer.setVisibility(View.VISIBLE);
                mPlusVisibity.setVisibility(View.VISIBLE);
                mOriginalPrice.setVisibility(View.GONE);
                mPlusPrice.setText("¥" + member_price);
            } else if (Integer.parseInt(price) >= 0) {
                mPriceContainer.setVisibility(View.VISIBLE);
                mPlusVisibity.setVisibility(View.GONE);

                if (Integer.parseInt(price) != Integer.parseInt(price_discount)) {
                    mOriginalPrice.setVisibility(View.VISIBLE);
                    mOriginalPrice.setText("¥" + price);
                    mOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
                } else {
                    mOriginalPrice.setVisibility(View.GONE);
                }
            } else {
                mPriceContainer.setVisibility(View.INVISIBLE);
            }

            //私信
            mToChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClick(taoData);
                    if(onEventClickListener != null){
                        onEventClickListener.onItemClick(v,taoData);
                    }
                }
            });

            views.add(view);
        }

        return views;
    }

    private void ItemClick(DiaryTaoData taoData) {

        final String hos_userid = taoData.getHos_userid();
        final String taoid = taoData.getId();
        final String title = taoData.getTitle();
        final String price_discount = taoData.getPrice_discount();
        final String member_price = taoData.getMember_price();
        final String list_cover_image = taoData.getList_cover_image();
        if (Utils.noLoginChat()) {
            toChatActivity(hos_userid, taoid, title, price_discount, member_price, list_cover_image);

        } else {
            if (Utils.isLoginAndBind(mContext)) {
                toChatActivity(hos_userid, taoid, title, price_discount, member_price, list_cover_image);
            }
        }

    }

    private void toChatActivity(String hos_userid, String taoid, String title, String price_discount, String member_price, String list_cover_image) {
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("102")
                .setYmId(mDiaryId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
    }

    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onItemClick(View view,DiaryTaoData data);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
