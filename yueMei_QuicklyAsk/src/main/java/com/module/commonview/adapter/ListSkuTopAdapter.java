package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.PostoperativeRecoverBean;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;

import java.util.List;

public class ListSkuTopAdapter extends RecyclerView.Adapter<ListSkuTopAdapter.ViewHolder> {
    private final int windowsWight;         //屏幕宽度
    private Activity mContext;
    private List<PostoperativeRecoverBean.TaoDataBean> mDatas;
    private final LayoutInflater mInflater;
    private String TAG = "ListSkuTopAdapter";
    private String mDiaryId;

    public ListSkuTopAdapter(Activity context, List<PostoperativeRecoverBean.TaoDataBean> datas, String diaryId) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
        this.mDiaryId = diaryId;
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ListSkuTopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_diary_list_suck_top, parent, false);
        return new ListSkuTopAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ListSkuTopAdapter.ViewHolder holder, int position) {
        final PostoperativeRecoverBean.TaoDataBean taoData = mDatas.get(position);

        Glide.with(mContext).load(taoData.getList_cover_image())
                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).into(holder.mImg);
        holder.mTitle.setText(taoData.getTitle());               //文案
        holder.mPrice.setText("¥" + taoData.getPrice_discount());      //价格
        String member_price = taoData.getMember_price();
        int i = Integer.parseInt(member_price);
        if (i >= 0) {
            holder.mPlusVisibity.setVisibility(View.VISIBLE);
            holder.mPlusPrice.setText("¥" + member_price);
        } else {
            holder.mPlusVisibity.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams layoutParams = holder.suckTop.getLayoutParams();
        if (mDatas.size() > 1) {
            layoutParams.width = windowsWight / 3 * 2;
        } else {
            layoutParams.width = windowsWight;
        }
        holder.suckTop.setLayoutParams(layoutParams);
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String is_rongyun = taoData.getIs_rongyun();
                String hos_userid = taoData.getHos_userid();
                String taoid = taoData.getId();
                String title = taoData.getTitle();
                String price_discount = taoData.getPrice_discount();
                String member_price = taoData.getMember_price();
                String list_cover_image = taoData.getList_cover_image();
                if (Utils.noLoginChat()) {
                    if ("3".equals(is_rongyun)) {
                        toChatActivity(hos_userid, taoid, title, price_discount, member_price, list_cover_image, taoData);
                    } else {
                        Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        if ("3".equals(is_rongyun)) {
                            toChatActivity(hos_userid, taoid, title, price_discount, member_price, list_cover_image, taoData);
                        } else {
                            Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void toChatActivity(String hos_userid, String taoid, String title, String price_discount, String member_price, String list_cover_image, PostoperativeRecoverBean.TaoDataBean taoData) {
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(taoid)
                .setObjType("2")
                .setTitle(title)
                .setPrice(price_discount)
                .setImg(list_cover_image)
                .setMemberPrice(member_price)
                .setYmClass("105")
                .setYmId(mDiaryId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                .setEvent_name("chat_hospital")
                .setEvent_pos("postimglist")
                .setHos_id(taoData.getHospital_id())
                .setDoc_id(taoData.getDoc_id())
                .setTao_id(taoid)
                .setEvent_others(taoData.getHospital_id())
                .setId(mDiaryId)
                .setReferrer("17")
                .setType("2")
                .build();
        Utils.chatTongJi(mContext, tongJiParams);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout suckTop;
        ImageView mImg;
        TextView mTitle;
        TextView mPrice;
        RelativeLayout mPlusVisibity;
        TextView mPlusPrice;
        Button btn;

        public ViewHolder(View itemView) {
            super(itemView);
            suckTop = itemView.findViewById(R.id.item_diary_list_suck_top);
            mImg = itemView.findViewById(R.id.diary_list_suck_top_img);
            mTitle = itemView.findViewById(R.id.diary_list_suck_top_title);
            mPrice = itemView.findViewById(R.id.diary_list_suck_top_price);
            mPlusVisibity = itemView.findViewById(R.id.tao_plus_vibility);
            mPlusPrice = itemView.findViewById(R.id.tao_plus_vip_price);
            btn = itemView.findViewById(R.id.diary_list_suck_top_btn);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemCallBackListener != null) {
                        itemCallBackListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}
