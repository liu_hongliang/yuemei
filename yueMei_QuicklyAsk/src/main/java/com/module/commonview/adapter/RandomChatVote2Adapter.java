package com.module.commonview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.quicklyask.activity.R;
import java.util.List;

/**
 * 文 件 名: RandomChatVoteAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-27 11:04
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class RandomChatVote2Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<String> mTxtList;

    public RandomChatVote2Adapter(Context context, List<String> txt_list) {
        this.mContext = context;
        this.mTxtList = txt_list;
        inflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new viewHolder(inflater.inflate(R.layout.item_vote_random_chat2, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof viewHolder) {
            setLayout(((viewHolder) holder), position);
        }
    }

    private void setLayout(viewHolder holder, final int position) {
        if (TextUtils.isEmpty(mTxtList.get(position))) {
            holder.tv_sku_title.setText("");
        } else {
            holder.tv_sku_title.setText(mTxtList.get(position));
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTxtList.size();
    }

    public List<String> getDate(){
        return mTxtList;
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView tv_sku_title;

        public viewHolder(View itemView) {
            super(itemView);
            tv_sku_title = itemView.findViewById(R.id.tv_sku_title);
        }
    }


    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}

