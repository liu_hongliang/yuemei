package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.SkuAddcarApi;
import com.module.commonview.module.bean.LocusData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.List;

public class LookMoreAdapter extends BaseQuickAdapter<LocusData,BaseViewHolder> {
    private String mHosId;
    public LookMoreAdapter(int layoutResId, @Nullable List<LocusData> data,String hosId) {
        super(layoutResId, data);
        this.mHosId=hosId;
    }


    @Override
    protected void convert(BaseViewHolder helper, final LocusData item) {
        Glide.with(mContext).load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into((ImageView) helper.getView(R.id.more_sku_img));
        helper.setText(R.id.more_sku_title,item.getTitle());
        String price = item.getPrice();

        if (!TextUtils.isEmpty(price)){
            int lookPrice = Integer.parseInt(price);
            if (lookPrice >= 0){
                helper.setGone(R.id.looking_price,true);
                helper.setText(R.id.looking_price,"¥"+item.getPrice());
            }else {
                helper.setGone(R.id.looking_price,false);
            }
        }else {
            helper.setVisible(R.id.looking_price,false);
        }

        String sku_order_num = item.getSku_order_num();
        if (!TextUtils.isEmpty(sku_order_num) && !"0".equals(sku_order_num)){
            helper.setGone(R.id.sku_list_ording,true);
            helper.setText(R.id.sku_list_ording,sku_order_num);
        }else {
            helper.setGone(R.id.sku_list_ording,false);
        }


    }
}
