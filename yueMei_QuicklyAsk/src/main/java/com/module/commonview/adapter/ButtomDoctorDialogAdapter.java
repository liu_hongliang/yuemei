package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.DiaryTagList;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.module.bean.PostListData;
import com.module.commonview.view.DoctorFlowLayoutGroup;
import com.module.commonview.view.PostFlowLayoutGroup;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;

import org.apache.commons.collections.CollectionUtils;
import org.xutils.common.util.DensityUtil;

import java.util.HashMap;
import java.util.List;

public class ButtomDoctorDialogAdapter extends BaseAdapter {

    private Context mContext;
    private PostListData mPostListData;

    public ButtomDoctorDialogAdapter(Context context, PostListData postListData) {
        this.mContext = context;
        this.mPostListData = postListData;
    }

    @Override
    public int getCount() {
        return mPostListData.getRecommend_doctor_list().size();
    }

    @Override
    public Object getItem(int position) {
        return mPostListData.getRecommend_doctor_list().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_buttom_doctor_dialog, null);
            holder = new ViewHolder();
            holder.iv_icon = convertView.findViewById(R.id.iv_icon);
            holder.tv_name = convertView.findViewById(R.id.tv_name);
            holder.tv_level = convertView.findViewById(R.id.tv_level);
            holder.tv_begood = convertView.findViewById(R.id.tv_begood);
            holder.ll_tag = convertView.findViewById(R.id.ll_tag);
//            holder.tv_type = convertView.findViewById(R.id.tv_type);
            holder.tv_consultation = convertView.findViewById(R.id.tv_consultation);
            holder.ll = convertView.findViewById(R.id.ll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Glide.with(mContext).load(mPostListData.getRecommend_doctor_list().get(position).getAvatar()).transform(new GlideCircleTransform(mContext)).into(holder.iv_icon);
        holder.tv_name.setText(mPostListData.getRecommend_doctor_list().get(position).getName());
        holder.tv_level.setText(mPostListData.getRecommend_doctor_list().get(position).getTitle());
//        holder.tv_type.setMaxLine(1);
        if ("3".equals(mPostListData.getRecommend_doctor_list().get(position).getIs_rongyun())) {
            holder.tv_consultation.setVisibility(View.VISIBLE);
        } else {
            holder.tv_consultation.setVisibility(View.INVISIBLE);
        }
        holder.ll_tag.removeAllViews();
        List<String> speciality_board = mPostListData.getRecommend_doctor_list().get(position).getSpeciality_board();
        if (CollectionUtils.isNotEmpty(speciality_board)) {
            if (speciality_board.size() > 2) {
                //显示2个
                for (int i = 0; i < 2; i++) {
                    TextView textView = new TextView(mContext);
                    textView.setGravity(Gravity.CENTER);
                    textView.setLines(1);
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    textView.setTextSize(12);
                    textView.setTextColor(Color.parseColor("#333333"));
                    textView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bian_yuanjiao_f6f6f6));
                    textView.setText(speciality_board.get(i));
                    textView.setPadding(DensityUtil.dip2px(10), DensityUtil.dip2px(3), DensityUtil.dip2px(10), DensityUtil.dip2px(3));
                    LinearLayout.LayoutParams textLayPams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    textLayPams.setMargins(0, 0, DensityUtil.dip2px(4), 0);//4个参数按顺序分别是左上右下
                    textView.setLayoutParams(textLayPams);
                    holder.ll_tag.addView(textView);
                }
            } else {
                for (int i = 0; i < speciality_board.size(); i++) {
                    TextView textView = new TextView(mContext);
                    textView.setGravity(Gravity.CENTER);
                    textView.setLines(1);
                    textView.setEllipsize(TextUtils.TruncateAt.END);
                    textView.setTextSize(12);
                    textView.setTextColor(Color.parseColor("#333333"));
                    textView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bian_yuanjiao_f6f6f6));
                    textView.setText(speciality_board.get(i));
                    textView.setPadding(DensityUtil.dip2px(10), DensityUtil.dip2px(3), DensityUtil.dip2px(10), DensityUtil.dip2px(3));
                    LinearLayout.LayoutParams textLayPams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    textLayPams.setMargins(0, 0, DensityUtil.dip2px(4), 0);//4个参数按顺序分别是左上右下
                    textView.setLayoutParams(textLayPams);
                    holder.ll_tag.addView(textView);
                }
            }
        }

//        if(mPostListData.getRecommend_doctor_list().get(position).getSpeciality_board() != null && !mPostListData.getRecommend_doctor_list().get(position).getSpeciality_board().isEmpty()){
//            //适配数据
//            DoctorFlowLayoutGroup mDocFlowLayout = new DoctorFlowLayoutGroup(mContext, holder.tv_type, mPostListData.getRecommend_doctor_list().get(position).getSpeciality_board());
////        mDocFlowLayout.setClickCallBack(new DoctorFlowLayoutGroup.ClickCallBack() {
////            @Override
////            public void onClick(View v, int pos, String a) {
////
////            }
////        });
//        }

        holder.tv_consultation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemChatCallListener != null) {
                    itemChatCallListener.onItemClick(v, position);
                }
            }
        });

        holder.iv_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPostListData.getRecommend_doctor_list().get(position).getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(mPostListData.getRecommend_doctor_list().get(position).getUrl(), "0", "0");
                }
            }
        });

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPostListData.getRecommend_doctor_list().get(position).getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(mPostListData.getRecommend_doctor_list().get(position).getUrl(), "0", "0");
                }
            }
        });
        return convertView;
    }


    class ViewHolder {
        ImageView iv_icon;
        TextView tv_name;
        TextView tv_level;
        TextView tv_begood;
        FlowLayout tv_type;
        TextView tv_consultation;

        LinearLayout ll_tag;
        LinearLayout ll;
    }


    //item点击回调
    private ItemChatCallListener itemChatCallListener;

    public interface ItemChatCallListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemChatCallListener(ItemChatCallListener itemChatCallListener) {
        this.itemChatCallListener = itemChatCallListener;
    }
}
