package com.module.commonview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.module.commonview.module.bean.VoteListBean;
import com.quicklyask.activity.R;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 * 文 件 名: VotePostContentAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-03 16:32
 * 邮   箱: 188897876@qq.com
 * 修改备注：帖子页里多个投票适配器   文字投票
 */

public class VotePostContentOrdinaryAdapter extends EasyAdapter2<VotePostContentOrdinaryAdapter.viewHolder1> {
    private Context mContext;
    private LayoutInflater inflater;
    private String isVote = "0"; //未投0 已投1
    private String mIsSingle;//0单选  1多选
    private VoteListBean voteListBean;
    private ArrayList<String> selectList;

    public VotePostContentOrdinaryAdapter(Context context, VoteListBean voteListBean, String isVote, String isSingle) {
        this.mContext = context;
        this.voteListBean = voteListBean;
        this.isVote = isVote;
        this.mIsSingle = isSingle;
        inflater = LayoutInflater.from(mContext);
        selectList = new ArrayList<>();
    }


    private void setView1(viewHolder1 holder, final int i) {
        if (mIsSingle.equals("0")) {
            //单选
            holder.iv_select_single.setVisibility(View.VISIBLE);
            holder.iv_select_multiple.setVisibility(View.GONE);
        } else {
            //多选
            holder.iv_select_single.setVisibility(View.GONE);
            holder.iv_select_multiple.setVisibility(View.VISIBLE);
        }
        if (isVote.equals("0")) {
            holder.ll_type1.setVisibility(View.VISIBLE);
            holder.ll_type2.setVisibility(View.GONE);
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTitle())) {
                holder.tv_sku_title1.setText("");
            } else {
                holder.tv_sku_title1.setText(voteListBean.getOption().get(i).getTitle());
            }
        } else {
            holder.ll_type1.setVisibility(View.GONE);
            holder.ll_type2.setVisibility(View.VISIBLE);

            if (voteListBean.getOption().get(i).getIs_vote_option().equals("0")) {
                holder.iv_check_mark_vote.setVisibility(View.GONE);
                holder.tv_sku_title2.setTextColor(Color.parseColor("#333333"));
                //这里直接设置有问题
                Drawable progressDrawable = mContext.getResources().getDrawable(R.drawable.vote_progress_bar2_ordinary);
                progressDrawable.setBounds(holder.progress.getProgressDrawable().getBounds());
                holder.progress.setProgressDrawable(progressDrawable);
                holder.progress.setProgress(0);
            } else {
                //选没选中都变成黑色的
//                holder.tv_sku_title2.setTextColor(Color.parseColor("#FFFFFF"));
                holder.tv_sku_title2.setTextColor(Color.parseColor("#333333"));
                holder.iv_check_mark_vote.setVisibility(View.VISIBLE);
                Drawable progressDrawable = mContext.getResources().getDrawable(R.drawable.vote_progress_bar1_ordinary);
                progressDrawable.setBounds(holder.progress.getProgressDrawable().getBounds());
                holder.progress.setProgressDrawable(progressDrawable);
                holder.progress.setProgress(0);
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getTitle())) {
                holder.tv_sku_title2.setText("");
            } else {
                holder.tv_sku_title2.setText(voteListBean.getOption().get(i).getTitle());
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getVote_option_rate())) {
                holder.tv_progress.setText("");
                holder.progress.setProgress(0);
            } else {
                holder.tv_progress.setText(new DecimalFormat("###################.###########").format(Double.parseDouble(voteListBean.getOption().get(i).getVote_option_rate().trim()) * 100));
                holder.progress.setProgress((int) (Double.parseDouble(voteListBean.getOption().get(i).getVote_option_rate().trim()) * 100));
            }
            if (TextUtils.isEmpty(voteListBean.getOption().get(i).getVote_num())) {
                holder.tv_num.setText("");
            } else {
                holder.tv_num.setText(voteListBean.getOption().get(i).getVote_num() + "票");
            }
        }
    }


    @NonNull
    @Override
    public viewHolder1 onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new viewHolder1(inflater.inflate(R.layout.item1_ordinary_vote_post, parent, false));
    }

    public String getSelectId() {
        selectList.clear();
        if (VotePostContentOrdinaryAdapter.this.getSelectMode() == SelectMode.SINGLE_SELECT) {
            if (VotePostContentOrdinaryAdapter.this.getSingleSelectedPosition() != -1) {
                if (!selectList.contains(voteListBean.getOption().get(VotePostContentOrdinaryAdapter.this.getSingleSelectedPosition()).getId())) {
                    selectList.add(voteListBean.getOption().get(VotePostContentOrdinaryAdapter.this.getSingleSelectedPosition()).getId());
                }
            }
        } else {
            for (int i = 0; i < voteListBean.getOption().size(); i++) {
                if (isSelected(i)) {
                    if (!selectList.contains(voteListBean.getOption().get(i).getId())) {
                        selectList.add(voteListBean.getOption().get(i).getId());
                    }
                }
            }
        }
        return (StringUtils.strip(selectList.toString(), "[]").trim()).replace(" ", "");
    }

    @Override
    public int getItemCount() {
        return voteListBean.getOption().size();
    }

    @Override
    public void whenBindViewHolder(viewHolder1 holder, int i) {
        setView1((viewHolder1) holder, i);
    }


    class viewHolder1 extends RecyclerView.ViewHolder {
        LinearLayout ll_type1;
        TextView tv_sku_title1;
        ImageView iv_select_single;
        ImageView iv_select_multiple;
        LinearLayout ll_content_type1;

        LinearLayout ll_type2;
        TextView tv_sku_title2;
        ImageView iv_check_mark_vote;
        TextView tv_progress;
        TextView tv_num;
        ProgressBar progress;

        public viewHolder1(View itemView) {
            super(itemView);
            ll_type1 = itemView.findViewById(R.id.ll_type1);
            tv_sku_title1 = itemView.findViewById(R.id.tv_sku_title1);
            ll_content_type1 = itemView.findViewById(R.id.ll_content_type1);
            iv_select_single = itemView.findViewById(R.id.iv_select_single);
            iv_select_multiple = itemView.findViewById(R.id.iv_select_multiple);

            ll_type2 = itemView.findViewById(R.id.ll_type2);
            tv_sku_title2 = itemView.findViewById(R.id.tv_sku_title2);
            iv_check_mark_vote = itemView.findViewById(R.id.iv_check_mark_vote);
            tv_progress = itemView.findViewById(R.id.tv_progress);
            tv_num = itemView.findViewById(R.id.tv_num);
            progress = itemView.findViewById(R.id.progress);
        }
    }

}
