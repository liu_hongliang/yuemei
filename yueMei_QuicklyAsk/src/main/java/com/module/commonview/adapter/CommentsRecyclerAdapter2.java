package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.HosImagShowActivity;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryReplyList2;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.module.bean.DiaryReplyListPic;
import com.module.commonview.module.bean.DiaryReplyListTao;
import com.module.commonview.module.bean.DiaryReplyListUserdata;
import com.module.commonview.view.CenterImageSpan;
import com.module.commonview.view.Expression;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.model.bean.CaseFinalPic;
import com.module.home.model.bean.HomeAskEntry;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 评论列表适配器
 * 新版评论列表 我的 最热 最新
 */
public class CommentsRecyclerAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private String TAG = "CommentsRecyclerAdapter2";
    public static final int MORE_COMMENTS = 1;                      //楼中楼评论只显示一条，多余的用查看更多显示
    private static final int SHOW_ALL_COMMENTS = 2;                 //楼中楼评论展示所有
    private final int mType;                                        //是否只显示一条楼中楼
    private final HomeAskEntry mAskEntry;                           //提问入口
    private final int windowsWight;                                 //屏幕宽度
    private final int mManager;                                     //外编剧需要动态设置
    private Activity mContext;
    private List<DiaryReplyList2> mDatas;
    private LayoutInflater mInflater;
    private String mAskorshare;
    private final BaseWebViewClientMessage webViewClientManager;
    private FunctionManager mFunctionManager;
    private HashMap<Integer, MoreReplyListAdapter2> hashMapAdapter = new LinkedHashMap<>();
    private final int ITEM_COMMENTS_MY = 2;     //评论 我的
    private final int ITEM_COMMENTS_HOT = 3;    //评论 热门
    private final int ITEM_COMMENTS_NEW = 4;    //评论 最新
    private String isOpenHot = "0";               //热门评论是否展开了
    //    private final int ITEM_COMMENTS_MY_HEADER = 5;     //评论 我的 头部
//    private final int ITEM_COMMENTS_HOT_HEADER = 6;    //评论 热门 头部
//    private final int ITEM_COMMENTS_HOT_FOOTER = 7;    //评论 热门 尾部
//    private final int ITEM_COMMENTS_NEW_HEADER = 8;    //评论 最新 头部
//    private int mMySize = 0;//我的评论数量
//    private int mHotSize = 0;//热门评论数量
    private String mPostUesrId;//帖子用户id


    //默认展示所有
    public CommentsRecyclerAdapter2(Activity context, List<DiaryReplyList2> datas, int manager, String mAskorshare, String id) {
        this(context, datas, manager, SHOW_ALL_COMMENTS, null, mAskorshare, id);
    }

    public CommentsRecyclerAdapter2(Activity context, List<DiaryReplyList2> datas, int manager, int type, HomeAskEntry askEntry, String askorshare, String id) {
        this.mContext = context;
        this.mDatas = datas;
        this.mManager = manager;
        this.mType = type;
        this.mAskEntry = askEntry;
        this.mAskorshare = askorshare;
        this.mPostUesrId = id;//发帖人id
        mInflater = LayoutInflater.from(context);
        webViewClientManager = new BaseWebViewClientMessage(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
        mFunctionManager = new FunctionManager(mContext);
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas != null && mDatas.size() > 0 && mDatas.get(position).getType().equals("0")) {
            return ITEM_COMMENTS_MY;
        } else if (mDatas != null && mDatas.size() > 0 && mDatas.get(position).getType().equals("1")) {
            return ITEM_COMMENTS_HOT;
        } else {
            return ITEM_COMMENTS_NEW;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_COMMENTS_MY:
                return new MyViewHolder(mInflater.inflate(R.layout.item_diary_list_recycview_comments2, parent, false));
            case ITEM_COMMENTS_HOT:
                return new HotViewHolder(mInflater.inflate(R.layout.item_diary_list_recycview_comments_hot, parent, false));
            case ITEM_COMMENTS_NEW:
                return new NewViewHolder(mInflater.inflate(R.layout.item_diary_list_recycview_comments2, parent, false));
            default:
                return new NewViewHolder(mInflater.inflate(R.layout.item_diary_list_recycview_comments2, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            if (holder instanceof MyViewHolder) {
                MyViewHolder commentsViewHolder = (MyViewHolder) holder;
                int typePos = position;
                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case "like":
                            //点赞数设置
                            if (Integer.parseInt(mDatas.get(typePos).getAgree_num()) > 0) {
                                commentsViewHolder.mPraiseNum.setVisibility(View.VISIBLE);
                                commentsViewHolder.mPraiseNum.setText(mDatas.get(typePos).getAgree_num());
                            } else {
                                commentsViewHolder.mPraiseNum.setVisibility(View.GONE);
                            }
                            //点赞图设置
                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(commentsViewHolder.mPraiseFlag);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(commentsViewHolder.mPraiseFlag);
                            }
                            break;
                    }
                }
            } else if (holder instanceof HotViewHolder) {
                HotViewHolder commentsViewHolder = (HotViewHolder) holder;
                int typePos = position;
                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case "like":
                            //点赞数设置
                            if (Integer.parseInt(mDatas.get(typePos).getAgree_num()) > 0) {
                                commentsViewHolder.mPraiseNum.setVisibility(View.VISIBLE);
                                commentsViewHolder.mPraiseNum.setText(mDatas.get(typePos).getAgree_num());
                            } else {
                                commentsViewHolder.mPraiseNum.setVisibility(View.GONE);
                            }
                            //点赞图设置
                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(commentsViewHolder.mPraiseFlag);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(commentsViewHolder.mPraiseFlag);
                            }
                            break;
                    }
                }
            } else {
                NewViewHolder commentsViewHolder = (NewViewHolder) holder;
                int typePos = position;
                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case "like":
                            //点赞数设置
                            if (Integer.parseInt(mDatas.get(typePos).getAgree_num()) > 0) {
                                commentsViewHolder.mPraiseNum.setVisibility(View.VISIBLE);
                                commentsViewHolder.mPraiseNum.setText(mDatas.get(typePos).getAgree_num());
                            } else {
                                commentsViewHolder.mPraiseNum.setVisibility(View.GONE);
                            }
                            //点赞图设置
                            if ("1".equals(mDatas.get(position).getIs_agree())) {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(commentsViewHolder.mPraiseFlag);
                            } else {
                                Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(commentsViewHolder.mPraiseFlag);
                            }
                            break;
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyViewHolder) {
            setMyView((MyViewHolder) holder, position);
        } else if (holder instanceof HotViewHolder) {
            setHotView((HotViewHolder) holder, position);
        } else {
            setNewView((NewViewHolder) holder, position);
        }
    }

    //设置我的数据
    private void setMyView(MyViewHolder holder, final int position1) {

        //设置外边距
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mItemCommentsView.getLayoutParams();
        layoutParams.leftMargin = mManager;
        layoutParams.rightMargin = mManager;
        holder.mItemCommentsView.setLayoutParams(layoutParams);

        final DiaryReplyList2 data = mDatas.get(position1);
        final DiaryReplyListUserdata userdata = data.getUserdata();       //评论用户信息
//        final DiaryReplyListTao tao = data.getTao();
        final List<DiaryReplyListPic> pic = data.getPic();

        List<DiaryReplyListList> list = data.getList();

        if (0 == position1 && data.getType().equals("0")) {
            holder.mForm.setText("我的评论");
            holder.mForm.setVisibility(View.VISIBLE);
        } else {
            holder.mForm.setVisibility(View.GONE);
        }

        if ("0".equals(mAskorshare)) {
            holder.mComments.setVisibility(View.INVISIBLE);
            holder.mName.setTextColor(Color.parseColor("#333333"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.tv_dots.setVisibility(View.INVISIBLE);
        } else {
            holder.mComments.setVisibility(View.VISIBLE);
            holder.mName.setTextColor(Color.parseColor("#999999"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.tv_dots.setVisibility(View.VISIBLE);
        }

        //用户头像
        Glide.with(mContext)
                .load(userdata.getAvatar())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ten_ball_placeholder)
                .error(R.drawable.ten_ball_placeholder)
                .into(holder.mImg);

        holder.mName.setText(userdata.getName());

        //医生职称
        if (!TextUtils.isEmpty(userdata.getTitle())) {
            holder.mTitle.setVisibility(View.VISIBLE);
            holder.mIcon.setVisibility(View.VISIBLE);
            holder.mTitle.setText(userdata.getTitle());
        } else {
            holder.mIcon.setVisibility(View.GONE);
            holder.mTitle.setVisibility(View.GONE);
        }

        //医院所在城市
        if (!TextUtils.isEmpty(userdata.getCityName())) {
            holder.mCity.setVisibility(View.VISIBLE);
            holder.mCity.setText(userdata.getCityName());
        } else {
            holder.mCity.setVisibility(View.GONE);
        }

        //医院名称
        if (!TextUtils.isEmpty(userdata.getHospitalName())) {
            holder.mHosName.setVisibility(View.VISIBLE);
            holder.mHosName.setText(userdata.getHospitalName());
        } else {
            holder.mHosName.setVisibility(View.GONE);
        }

        //用户描述
        holder.mDate.setText(userdata.getLable());

        //点赞数设置
        if (Integer.parseInt(mDatas.get(position1).getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(mDatas.get(position1).getAgree_num());     //点赞数
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图设置
        if ("1".equals(mDatas.get(position1).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseFlag);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseFlag);
        }

        if ("1".equals(data.getSet_tid())) {

            //转化出表情
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));

            //设置开始的图片
            SpannableString spanString = new SpannableString("占位 " + stringBuilder);
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.comments_selection_of);
            drawable.setBounds(0, 0, Utils.dip2px(40), Utils.dip2px(17));
            CenterImageSpan span = new CenterImageSpan(drawable);
            spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            holder.mContent.setText(spanString);

        } else {
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        }


        if (pic.size() != 0) {
            //图片设置
            holder.mMoreImgList.setVisibility(View.VISIBLE);
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            holder.mMoreImgList.setLayoutManager(scrollLinearLayoutManager);
            MoreReplyImgListAdapter moreReplyImgListAdapter = new MoreReplyImgListAdapter(mContext, pic, windowsWight);
            holder.mMoreImgList.setAdapter(moreReplyImgListAdapter);

            moreReplyImgListAdapter.setItemJumpCallBackListener(new MoreReplyImgListAdapter.ItemJumpCallBackListener() {
                @Override
                public void onItemClick(View v, int position) {

                    List<CaseFinalPic> list = new ArrayList<>();
                    for (DiaryReplyListPic item : pic) {
                        list.add(new CaseFinalPic(item.getYuan()));
                    }
                    Intent intent = new Intent(mContext, HosImagShowActivity.class);
                    intent.putExtra("casefinaljson", (Serializable) list);
                    intent.putExtra("pos", position);
                    mContext.startActivity(intent);
                }

                @Override
                public void onItemLongClick(View v) {
                    itemLong(position1);
                }
            });
        } else {
            holder.mMoreImgList.setVisibility(View.GONE);
        }
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        holder.mMoreList.setLayoutManager(scrollLinearLayoutManager);
        MoreReplyListAdapter2 moreReplyListAdapter = new MoreReplyListAdapter2(mContext, list);
        holder.mMoreList.setAdapter(moreReplyListAdapter);

        hashMapAdapter.put(position1, moreReplyListAdapter);

        if (moreReplyListAdapter.getmDatas() != null && moreReplyListAdapter.getmDatas().size() != 0) {
            //评论回复设置
//            holder.mMoreListClick.setVisibility(View.VISIBLE);
//
//            //只展示一条楼中楼
//            if (mType == 1) {
//                if (Integer.parseInt(data.getListNum()) > 1) {
//                    holder.mMoreListMoreClick.setVisibility(View.VISIBLE);
//                    holder.mMoreListMoreTitle.setText("共" + data.getListNum() + "条评论");
//                    holder.mMoreListMoreClick.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            itemJumpCallBackListener.onItemJumpClick(v);
//                        }
//                    });
//                } else {
//                    holder.mMoreListMoreClick.setVisibility(View.GONE);
//                }
//            } else {
//                holder.mMoreListMoreClick.setVisibility(View.GONE);
//            }

            //删除楼中楼
            moreReplyListAdapter.setItemDeleteCallBackListener(new MoreReplyListAdapter2.ItemDeleteCallBackListener() {
                @Override
                public void onItemDeleteClick(DiariesDeleteData diariesDeleteData) {
                    diariesDeleteData.setCommentsOrReply(1);
                    diariesDeleteData.setPos(position1);

                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });

            //举报楼中楼
            moreReplyListAdapter.setItemReportCallBackListener(new MoreReplyListAdapter2.ItemReportCallBackListener() {
                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    data.setPos(position1);
                    onEventClickListener.onItemReportClick(data);
                }
            });

        } else {
            holder.mMoreListClick.setVisibility(View.GONE);
        }

        //头像点击事件
        holder.mImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });

        holder.mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });
        holder.mContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemLong(position1);
            }
        });

        final String is_rongyun = userdata.getIs_rongyun();
        final String hos_userid = userdata.getHos_userid();
        final String docId = userdata.getId();
        final String title = userdata.getName();
        final String list_cover_image = userdata.getAvatar();
        final String id = userdata.getId();
        if ("3".equals(is_rongyun)) {
            holder.mMbtn.setVisibility(View.VISIBLE);
        } else {
            holder.mMbtn.setVisibility(View.GONE);
        }
        holder.mMbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                .setDirectId(hos_userid)
                                .setObjId(docId)
                                .setObjType("3")
                                .setTitle(title)
                                .setImg(list_cover_image)
                                .setYmClass("108")
                                .setYmId(id)
                                .build();
                        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                                .setEvent_name("chat_hospital")
                                .setEvent_pos("askinfo")
                                .setHos_id(userdata.getHospital_id())
                                .setDoc_id(docId)
                                .setTao_id("0")
                                .setEvent_others(userdata.getHospital_id())
                                .setId(docId)
                                .setReferrer("17")
                                .setType("2")
                                .build();
                        Utils.chatTongJi(mContext, tongJiParams);
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
            }
        });
    }

    //设置热门数据
    private void setHotView(final HotViewHolder holder, final int position1) {
        //设置外边距
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mItemCommentsView.getLayoutParams();
        layoutParams.leftMargin = mManager;
        layoutParams.rightMargin = mManager;
        holder.mItemCommentsView.setLayoutParams(layoutParams);

        final DiaryReplyList2 data = mDatas.get(position1);
        final DiaryReplyListUserdata userdata = data.getUserdata();       //评论用户信息
//        final DiaryReplyListTao tao = data.getTao();
        final List<DiaryReplyListPic> pic = data.getPic();
        List<DiaryReplyListList> list = data.getList();

        if (getMyItemCount().size() > 0) {
            if (getMyItemCount().size() == position1 && data.getType().equals("1")) {
                holder.mForm.setText("热门评论");
                holder.mForm.setVisibility(View.VISIBLE);
            } else {
                holder.mForm.setVisibility(View.GONE);
            }
        } else {
            if (0 == position1 && data.getType().equals("1")) {
                holder.mForm.setText("热门评论");
                holder.mForm.setVisibility(View.VISIBLE);
            } else {
                holder.mForm.setVisibility(View.GONE);
            }
        }

        if (isOpenHot.equals("1")) {
            holder.mCommentOpen.setText("暂无更多热评");
            holder.mCommentOpen.setTextColor(Color.parseColor("#999999"));
        } else {
            holder.mCommentOpen.setText("展开全部热门评论");
            holder.mCommentOpen.setTextColor(Color.parseColor("#FF527F"));
        }

        if (isOpenHot.equals("1") && getHotItemCount().size() == 3) {
            holder.mCommentOpen.setVisibility(View.GONE);
        } else {
            if (isOpenHot.equals("0")) {
                if (getMyItemCount().size() + 3 - 1 == position1) {
                    holder.mCommentOpen.setVisibility(View.VISIBLE);
                } else {
                    holder.mCommentOpen.setVisibility(View.GONE);
                }
            } else {
                if (getMyItemCount().size() + getHotItemCount().size() - 1 == position1) {
                    holder.mCommentOpen.setVisibility(View.VISIBLE);
                } else {
                    holder.mCommentOpen.setVisibility(View.GONE);
                }
            }
        }

        holder.mCommentOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpenHot.equals("0")) {
                    //展开
                    holder.mCommentOpen.setText("暂无更多热评");
                    holder.mCommentOpen.setTextColor(Color.parseColor("#999999"));
                    if (onEventClickListener != null) {
                        onEventClickListener.onCommentMoreClick(v, position1);
                    }
                }
            }
        });

        if ("0".equals(mAskorshare)) {
            holder.mComments.setVisibility(View.INVISIBLE);
            holder.mName.setTextColor(Color.parseColor("#333333"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.tv_dots.setVisibility(View.INVISIBLE);
        } else {
            holder.mComments.setVisibility(View.VISIBLE);
            holder.mName.setTextColor(Color.parseColor("#999999"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.tv_dots.setVisibility(View.VISIBLE);
        }

        //用户头像
        Glide.with(mContext)
                .load(userdata.getAvatar())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ten_ball_placeholder)
                .error(R.drawable.ten_ball_placeholder)
                .into(holder.mImg);

        holder.mName.setText(userdata.getName());

        //医生职称
        if (!TextUtils.isEmpty(userdata.getTitle())) {
            holder.mTitle.setVisibility(View.VISIBLE);
            holder.mIcon.setVisibility(View.VISIBLE);
            holder.mTitle.setText(userdata.getTitle());
        } else {
            holder.mIcon.setVisibility(View.GONE);
            holder.mTitle.setVisibility(View.GONE);
        }

        //医院所在城市
        if (!TextUtils.isEmpty(userdata.getCityName())) {
            holder.mCity.setVisibility(View.VISIBLE);
            holder.mCity.setText(userdata.getCityName());
        } else {
            holder.mCity.setVisibility(View.GONE);
        }

        //医院名称
        if (!TextUtils.isEmpty(userdata.getHospitalName())) {
            holder.mHosName.setVisibility(View.VISIBLE);
            holder.mHosName.setText(userdata.getHospitalName());
        } else {
            holder.mHosName.setVisibility(View.GONE);
        }

        //用户描述
        holder.mDate.setText(userdata.getLable());

        //点赞数设置
        if (Integer.parseInt(mDatas.get(position1).getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(mDatas.get(position1).getAgree_num());     //点赞数
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图设置
        if ("1".equals(mDatas.get(position1).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseFlag);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseFlag);
        }

        if ("1".equals(data.getSet_tid())) {

            //转化出表情
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));

            //设置开始的图片
            SpannableString spanString = new SpannableString("占位 " + stringBuilder);
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.comments_selection_of);
            drawable.setBounds(0, 0, Utils.dip2px(40), Utils.dip2px(17));
            CenterImageSpan span = new CenterImageSpan(drawable);
            spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            holder.mContent.setText(spanString);

        } else {
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        }


        if (pic.size() != 0) {
            //图片设置
            holder.mMoreImgList.setVisibility(View.VISIBLE);
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            holder.mMoreImgList.setLayoutManager(scrollLinearLayoutManager);
            MoreReplyImgListAdapter moreReplyImgListAdapter = new MoreReplyImgListAdapter(mContext, pic, windowsWight);
            holder.mMoreImgList.setAdapter(moreReplyImgListAdapter);

            moreReplyImgListAdapter.setItemJumpCallBackListener(new MoreReplyImgListAdapter.ItemJumpCallBackListener() {
                @Override
                public void onItemClick(View v, int position) {

                    List<CaseFinalPic> list = new ArrayList<>();
                    for (DiaryReplyListPic item : pic) {
                        list.add(new CaseFinalPic(item.getYuan()));
                    }
                    Intent intent = new Intent(mContext, HosImagShowActivity.class);
                    intent.putExtra("casefinaljson", (Serializable) list);
                    intent.putExtra("pos", position);
                    mContext.startActivity(intent);
                }

                @Override
                public void onItemLongClick(View v) {
                    itemLong(position1);
                }
            });
        } else {
            holder.mMoreImgList.setVisibility(View.GONE);
        }
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        holder.mMoreList.setLayoutManager(scrollLinearLayoutManager);
        MoreReplyListAdapter2 moreReplyListAdapter = new MoreReplyListAdapter2(mContext, list);
        holder.mMoreList.setAdapter(moreReplyListAdapter);

        hashMapAdapter.put(position1, moreReplyListAdapter);

        if (moreReplyListAdapter.getmDatas() != null && moreReplyListAdapter.getmDatas().size() != 0) {
//            //评论回复设置
//            holder.mMoreListClick.setVisibility(View.VISIBLE);
//
//            //只展示一条楼中楼
//            if (mType == 1) {
//                if (Integer.parseInt(data.getListNum()) > 1) {
//                    holder.mMoreListMoreClick.setVisibility(View.VISIBLE);
//                    holder.mMoreListMoreTitle.setText("共" + data.getListNum() + "条评论");
//                    holder.mMoreListMoreClick.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            itemJumpCallBackListener.onItemJumpClick(v);
//                        }
//                    });
//                } else {
//                    holder.mMoreListMoreClick.setVisibility(View.GONE);
//                }
//            } else {
//                holder.mMoreListMoreClick.setVisibility(View.GONE);
//            }

            //删除楼中楼
            moreReplyListAdapter.setItemDeleteCallBackListener(new MoreReplyListAdapter2.ItemDeleteCallBackListener() {
                @Override
                public void onItemDeleteClick(DiariesDeleteData diariesDeleteData) {
                    diariesDeleteData.setCommentsOrReply(1);
                    diariesDeleteData.setPos(position1);

                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });

            //举报楼中楼
            moreReplyListAdapter.setItemReportCallBackListener(new MoreReplyListAdapter2.ItemReportCallBackListener() {
                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    data.setPos(position1);
                    onEventClickListener.onItemReportClick(data);
                }
            });

        } else {
            holder.mMoreListClick.setVisibility(View.GONE);
        }

        //头像点击事件
        holder.mImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });

        holder.mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });

        holder.mContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemLong(position1);
            }
        });

        final String is_rongyun = userdata.getIs_rongyun();
        final String hos_userid = userdata.getHos_userid();
        final String docId = userdata.getId();
        final String title = userdata.getName();
        final String list_cover_image = userdata.getAvatar();
        final String id = userdata.getId();
        if ("3".equals(is_rongyun)) {
            holder.mMbtn.setVisibility(View.VISIBLE);
        } else {
            holder.mMbtn.setVisibility(View.GONE);
        }
        holder.mMbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                .setDirectId(hos_userid)
                                .setObjId(docId)
                                .setObjType("3")
                                .setTitle(title)
                                .setImg(list_cover_image)
                                .setYmClass("108")
                                .setYmId(id)
                                .build();
                        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                                .setEvent_name("chat_hospital")
                                .setEvent_pos("askinfo")
                                .setHos_id(userdata.getHospital_id())
                                .setDoc_id(docId)
                                .setTao_id("0")
                                .setEvent_others(userdata.getHospital_id())
                                .setId(docId)
                                .setReferrer("17")
                                .setType("2")
                                .build();
                        Utils.chatTongJi(mContext, tongJiParams);
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
            }
        });
    }

    //设置最新数据
    private void setNewView(NewViewHolder holder, final int position1) {
        //设置外边距
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mItemCommentsView.getLayoutParams();
        layoutParams.leftMargin = mManager;
        layoutParams.rightMargin = mManager;
        holder.mItemCommentsView.setLayoutParams(layoutParams);

        final DiaryReplyList2 data = mDatas.get(position1);
        final DiaryReplyListUserdata userdata = data.getUserdata();       //评论用户信息
//        final DiaryReplyListTao tao = data.getTao();
        final List<DiaryReplyListPic> pic = data.getPic();

        List<DiaryReplyListList> list = data.getList();
        if (isOpenHot.equals("0") && getHotItemCount().size() > 0) {
            if (getMyItemCount().size() + 3 == position1 && data.getType().equals("2")) {
                holder.mForm.setText("最新评论");
                holder.mForm.setVisibility(View.VISIBLE);
            } else {
                holder.mForm.setVisibility(View.GONE);
            }
        } else {
            //展开
            if (getMyItemCount().size() + getHotItemCount().size() == position1 && data.getType().equals("2")) {
                holder.mForm.setText("最新评论");
                holder.mForm.setVisibility(View.VISIBLE);
            } else {
                holder.mForm.setVisibility(View.GONE);
            }
        }

        if ("0".equals(mAskorshare)) {
            holder.mComments.setVisibility(View.INVISIBLE);
            holder.mName.setTextColor(Color.parseColor("#333333"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            holder.tv_dots.setVisibility(View.INVISIBLE);
        } else {
            holder.mComments.setVisibility(View.VISIBLE);
            holder.mName.setTextColor(Color.parseColor("#999999"));
            holder.mName.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            holder.tv_dots.setVisibility(View.VISIBLE);
        }

        //用户头像
        Glide.with(mContext)
                .load(userdata.getAvatar())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ten_ball_placeholder)
                .error(R.drawable.ten_ball_placeholder)
                .into(holder.mImg);

        holder.mName.setText(userdata.getName());

        //医生职称
        if (!TextUtils.isEmpty(userdata.getTitle())) {
            holder.mTitle.setVisibility(View.VISIBLE);
            holder.mIcon.setVisibility(View.VISIBLE);
            holder.mTitle.setText(userdata.getTitle());
        } else {
            holder.mIcon.setVisibility(View.GONE);
            holder.mTitle.setVisibility(View.GONE);
        }

        //医院所在城市
        if (!TextUtils.isEmpty(userdata.getCityName())) {
            holder.mCity.setVisibility(View.VISIBLE);
            holder.mCity.setText(userdata.getCityName());
        } else {
            holder.mCity.setVisibility(View.GONE);
        }

        //医院名称
        if (!TextUtils.isEmpty(userdata.getHospitalName())) {
            holder.mHosName.setVisibility(View.VISIBLE);
            holder.mHosName.setText(userdata.getHospitalName());
        } else {
            holder.mHosName.setVisibility(View.GONE);
        }

        //用户描述
        holder.mDate.setText(userdata.getLable());

        //点赞数设置
        if (Integer.parseInt(mDatas.get(position1).getAgree_num()) > 0) {
            holder.mPraiseNum.setVisibility(View.VISIBLE);
            holder.mPraiseNum.setText(mDatas.get(position1).getAgree_num());     //点赞数
        } else {
            holder.mPraiseNum.setVisibility(View.GONE);
        }

        //点赞图设置
        if ("1".equals(mDatas.get(position1).getIs_agree())) {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_selected).into(holder.mPraiseFlag);
        } else {
            Glide.with(mContext).load(R.drawable.diary_list_recycler_praise).into(holder.mPraiseFlag);
        }

        if ("1".equals(data.getSet_tid())) {

            //转化出表情
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));

            //设置开始的图片
            SpannableString spanString = new SpannableString("占位 " + stringBuilder);
            Drawable drawable = Utils.getLocalDrawable(mContext, R.drawable.comments_selection_of);
            drawable.setBounds(0, 0, Utils.dip2px(40), Utils.dip2px(17));
            CenterImageSpan span = new CenterImageSpan(drawable);
            spanString.setSpan(span, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            holder.mContent.setText(spanString);

        } else {
            SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));
            holder.mContent.setText(stringBuilder);
        }


        if (pic.size() != 0) {
            //图片设置
            holder.mMoreImgList.setVisibility(View.VISIBLE);
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            holder.mMoreImgList.setLayoutManager(scrollLinearLayoutManager);
            MoreReplyImgListAdapter moreReplyImgListAdapter = new MoreReplyImgListAdapter(mContext, pic, windowsWight);
            holder.mMoreImgList.setAdapter(moreReplyImgListAdapter);

            moreReplyImgListAdapter.setItemJumpCallBackListener(new MoreReplyImgListAdapter.ItemJumpCallBackListener() {
                @Override
                public void onItemClick(View v, int position) {

                    List<CaseFinalPic> list = new ArrayList<>();
                    for (DiaryReplyListPic item : pic) {
                        list.add(new CaseFinalPic(item.getYuan()));
                    }
                    Intent intent = new Intent(mContext, HosImagShowActivity.class);
                    intent.putExtra("casefinaljson", (Serializable) list);
                    intent.putExtra("pos", position);
                    mContext.startActivity(intent);
                }

                @Override
                public void onItemLongClick(View v) {
                    itemLong(position1);
                }
            });
        } else {
            holder.mMoreImgList.setVisibility(View.GONE);
        }
        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        holder.mMoreList.setLayoutManager(scrollLinearLayoutManager);
        MoreReplyListAdapter2 moreReplyListAdapter = new MoreReplyListAdapter2(mContext, list);
        holder.mMoreList.setAdapter(moreReplyListAdapter);

        hashMapAdapter.put(position1, moreReplyListAdapter);

        if (moreReplyListAdapter.getmDatas() != null && moreReplyListAdapter.getmDatas().size() != 0) {
            //评论回复设置
//            holder.mMoreListClick.setVisibility(View.VISIBLE);
//
//            //只展示一条楼中楼
//            if (mType == 1) {
//                if (Integer.parseInt(data.getListNum()) > 1) {
//                    holder.mMoreListMoreClick.setVisibility(View.VISIBLE);
//                    holder.mMoreListMoreTitle.setText("共" + data.getListNum() + "条评论");
//                    holder.mMoreListMoreClick.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            itemJumpCallBackListener.onItemJumpClick(v);
//                        }
//                    });
//                } else {
//                    holder.mMoreListMoreClick.setVisibility(View.GONE);
//                }
//            } else {
//                holder.mMoreListMoreClick.setVisibility(View.GONE);
//            }

            //删除楼中楼
            moreReplyListAdapter.setItemDeleteCallBackListener(new MoreReplyListAdapter2.ItemDeleteCallBackListener() {
                @Override
                public void onItemDeleteClick(DiariesDeleteData diariesDeleteData) {
                    diariesDeleteData.setCommentsOrReply(1);
                    diariesDeleteData.setPos(position1);

                    onEventClickListener.onItemDeleteClick(diariesDeleteData);
                }
            });

            //举报楼中楼
            moreReplyListAdapter.setItemReportCallBackListener(new MoreReplyListAdapter2.ItemReportCallBackListener() {
                @Override
                public void onItemReportClick(DiariesReportLikeData data) {
                    data.setPos(position1);
                    onEventClickListener.onItemReportClick(data);
                }
            });

        } else {
            holder.mMoreListClick.setVisibility(View.GONE);
        }

        //头像点击事件
        holder.mImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });
        holder.mName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userdata.getUrl() != null) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(userdata.getUrl(), "0", "0");
                }
            }
        });
        holder.mContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemLong(position1);
            }
        });
        final String is_rongyun = userdata.getIs_rongyun();
        final String hos_userid = userdata.getHos_userid();
        final String docId = userdata.getId();
        final String title = userdata.getName();
        final String list_cover_image = userdata.getAvatar();
        final String id = userdata.getId();
        if ("3".equals(is_rongyun)) {
            holder.mMbtn.setVisibility(View.VISIBLE);
        } else {
            holder.mMbtn.setVisibility(View.GONE);
        }
        holder.mMbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                .setDirectId(hos_userid)
                                .setObjId(docId)
                                .setObjType("3")
                                .setTitle(title)
                                .setImg(list_cover_image)
                                .setYmClass("108")
                                .setYmId(id)
                                .build();
                        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                                .setEvent_name("chat_hospital")
                                .setEvent_pos("askinfo")
                                .setHos_id(userdata.getHospital_id())
                                .setDoc_id(docId)
                                .setTao_id("0")
                                .setEvent_others(userdata.getHospital_id())
                                .setId(docId)
                                .setReferrer("17")
                                .setType("2")
                                .build();
                        Utils.chatTongJi(mContext, tongJiParams);
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    //返回我的评论数据
    public List<DiaryReplyList2> getMyItemCount() {
        List<DiaryReplyList2> MyList = new ArrayList<DiaryReplyList2>();
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getType().equals("0")) {
                MyList.add(mDatas.get(i));
            }
        }
        return MyList;
    }

    //返回热门评论数据
    public List<DiaryReplyList2> getHotItemCount() {
        List<DiaryReplyList2> HotList = new ArrayList<DiaryReplyList2>();
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getType().equals("1")) {
                HotList.add(mDatas.get(i));
            }
        }
        return HotList;
    }

    //返回最新评论数据
    public List<DiaryReplyList2> getNewItemCount() {
        List<DiaryReplyList2> NewList = new ArrayList<DiaryReplyList2>();
        for (int i = 0; i < mDatas.size(); i++) {
            if (mDatas.get(i).getType().equals("2")) {
                NewList.add(mDatas.get(i));
            }
        }
        return NewList;
    }


    /**
     * 我的
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mForm;
        RelativeLayout mItemCommentsView;
        ImageView mImg;
        TextView mName;
        TextView mTitle;
        ImageView mIcon;
        TextView mCity;
        TextView mHosName;
        TextView mComments;
        LinearLayout mPraiseClickFlag;
        ImageView mPraiseFlag;
        TextView mPraiseNum;
        TextView mDate;
        TextView mContent;
        LinearLayout taoAndDiaryContiainer;
        ImageView taoAndDiaryPicture;
        TextView taoAndDiaryTitle;
        TextView taoAndDiaryPrice;
        RecyclerView mMoreImgList;
        LinearLayout mMoreListClick;
        RecyclerView mMoreList;
        LinearLayout mMoreListMoreClick;
        TextView mMoreListMoreTitle;
        TextView mMbtn;
        TextView tv_dots;

        CardView cardview;
        ImageView skuImg;
        TextView viewContent;
        TextView price;
        TextView originalPrice;
        RelativeLayout plusPriceVibiable;
        TextView plusPrice;
        TextView btn;
        LinearLayout ll_posts;
        TextView post_title;

        public MyViewHolder(View itemView) {
            super(itemView);
            mForm = itemView.findViewById(R.id.tv_type);
            mItemCommentsView = itemView.findViewById(R.id.item_diary_list_recycview_comments);
            mImg = itemView.findViewById(R.id.diary_list_comments_img);
            mName = itemView.findViewById(R.id.diary_list_comments_name);
            mTitle = itemView.findViewById(R.id.diary_list_comments_title);
            mIcon = itemView.findViewById(R.id.diary_list_comments_icon);
            mCity = itemView.findViewById(R.id.diary_list_comments_city);
            mHosName = itemView.findViewById(R.id.diary_list_comments_hos);
            mComments = itemView.findViewById(R.id.diary_list_comments_comments);
            mPraiseClickFlag = itemView.findViewById(R.id.diary_list_comments_praise_click);
            mPraiseFlag = itemView.findViewById(R.id.diary_list_comments_praise_flag);
            mPraiseNum = itemView.findViewById(R.id.diary_list_comments_praise_num);
            mDate = itemView.findViewById(R.id.diary_list_comments_date);
            mContent = itemView.findViewById(R.id.diary_list_comments_content);
            taoAndDiaryContiainer = itemView.findViewById(R.id.comments_list_tao_diary_container);
            taoAndDiaryPicture = itemView.findViewById(R.id.comments_list_tao_diary_picture);
            taoAndDiaryTitle = itemView.findViewById(R.id.comments_list_tao_diary_title);
            taoAndDiaryPrice = itemView.findViewById(R.id.comments_list_tao_diary_price);
            mMoreImgList = itemView.findViewById(R.id.diary_list_comments_img_list);
            mMoreListClick = itemView.findViewById(R.id.diary_list_comments_more_list_click);
            mMoreList = itemView.findViewById(R.id.diary_list_comments_more_list);
            mMoreListMoreClick = itemView.findViewById(R.id.diary_list_comments_more_list_more_click);
            mMoreListMoreTitle = itemView.findViewById(R.id.diary_list_comments_more_list_more_title);
            mMbtn = itemView.findViewById(R.id.diary_list_comments_btn);
            tv_dots = itemView.findViewById(R.id.tv_dots);


            cardview = itemView.findViewById(R.id.cardview);

            //图片
            skuImg = itemView.findViewById(R.id.diary_list_goods_sku_img);
            //标题
            viewContent = itemView.findViewById(R.id.diary_list_goods_title);
            //价格布局
            price = itemView.findViewById(R.id.diary_list_goods_price);
            //价格
            originalPrice = itemView.findViewById(R.id.diary_list_goods_original_price);
//             //
//             diary_list_goods_original_price = itemView.findViewById(R.id.diary_list_goods_original_price);
            //plus布局
            plusPriceVibiable = itemView.findViewById(R.id.diary_list_plus_vibiable);
            //plus价格
            plusPrice = itemView.findViewById(R.id.plus_vip_price);
            //咨询按钮
            btn = itemView.findViewById(R.id.diary_list_goods_btn);
            //底部提示布局
            ll_posts = itemView.findViewById(R.id.post_list_goods_doc_click);
            //提示
            post_title = itemView.findViewById(R.id.post_title);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //item长按事件
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    itemLong(getLayoutPosition());
                    return false;
                }
            });

            //点赞
            mPraiseClickFlag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        data.setIs_reply("1");
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            //回复点击
            mComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });

            //文字点击
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });
        }

    }

    /**
     * 热门
     */
    public class HotViewHolder extends RecyclerView.ViewHolder {
        TextView mForm;
        RelativeLayout mItemCommentsView;
        ImageView mImg;
        TextView mName;
        TextView mTitle;
        ImageView mIcon;
        TextView mCity;
        TextView mHosName;
        TextView mComments;
        LinearLayout mPraiseClickFlag;
        ImageView mPraiseFlag;
        TextView mPraiseNum;
        TextView mDate;
        TextView mContent;
        LinearLayout taoAndDiaryContiainer;
        ImageView taoAndDiaryPicture;
        TextView taoAndDiaryTitle;
        TextView taoAndDiaryPrice;
        RecyclerView mMoreImgList;
        LinearLayout mMoreListClick;
        RecyclerView mMoreList;
        LinearLayout mMoreListMoreClick;
        TextView mMoreListMoreTitle;
        TextView mMbtn;
        TextView tv_dots;

        CardView cardview;
        ImageView skuImg;
        TextView viewContent;
        TextView price;
        TextView originalPrice;
        RelativeLayout plusPriceVibiable;
        TextView plusPrice;
        TextView btn;
        LinearLayout ll_posts;
        TextView post_title;

        TextView mCommentOpen;//评论展开

        public HotViewHolder(View itemView) {
            super(itemView);
            mForm = itemView.findViewById(R.id.tv_type);
            mItemCommentsView = itemView.findViewById(R.id.item_diary_list_recycview_comments);
            mImg = itemView.findViewById(R.id.diary_list_comments_img);
            mName = itemView.findViewById(R.id.diary_list_comments_name);
            mTitle = itemView.findViewById(R.id.diary_list_comments_title);
            mIcon = itemView.findViewById(R.id.diary_list_comments_icon);
            mCity = itemView.findViewById(R.id.diary_list_comments_city);
            mHosName = itemView.findViewById(R.id.diary_list_comments_hos);
            mComments = itemView.findViewById(R.id.diary_list_comments_comments);
            mPraiseClickFlag = itemView.findViewById(R.id.diary_list_comments_praise_click);
            mPraiseFlag = itemView.findViewById(R.id.diary_list_comments_praise_flag);
            mPraiseNum = itemView.findViewById(R.id.diary_list_comments_praise_num);
            mDate = itemView.findViewById(R.id.diary_list_comments_date);
            mContent = itemView.findViewById(R.id.diary_list_comments_content);
            taoAndDiaryContiainer = itemView.findViewById(R.id.comments_list_tao_diary_container);
            taoAndDiaryPicture = itemView.findViewById(R.id.comments_list_tao_diary_picture);
            taoAndDiaryTitle = itemView.findViewById(R.id.comments_list_tao_diary_title);
            taoAndDiaryPrice = itemView.findViewById(R.id.comments_list_tao_diary_price);
            mMoreImgList = itemView.findViewById(R.id.diary_list_comments_img_list);
            mMoreListClick = itemView.findViewById(R.id.diary_list_comments_more_list_click);
            mMoreList = itemView.findViewById(R.id.diary_list_comments_more_list);
            mMoreListMoreClick = itemView.findViewById(R.id.diary_list_comments_more_list_more_click);
            mMoreListMoreTitle = itemView.findViewById(R.id.diary_list_comments_more_list_more_title);
            mMbtn = itemView.findViewById(R.id.diary_list_comments_btn);
            tv_dots = itemView.findViewById(R.id.tv_dots);


            cardview = itemView.findViewById(R.id.cardview);

            //图片
            skuImg = itemView.findViewById(R.id.diary_list_goods_sku_img);
            //标题
            viewContent = itemView.findViewById(R.id.diary_list_goods_title);
            //价格布局
            price = itemView.findViewById(R.id.diary_list_goods_price);
            //价格
            originalPrice = itemView.findViewById(R.id.diary_list_goods_original_price);
//             //
//             diary_list_goods_original_price = itemView.findViewById(R.id.diary_list_goods_original_price);
            //plus布局
            plusPriceVibiable = itemView.findViewById(R.id.diary_list_plus_vibiable);
            //plus价格
            plusPrice = itemView.findViewById(R.id.plus_vip_price);
            //咨询按钮
            btn = itemView.findViewById(R.id.diary_list_goods_btn);
            //底部提示布局
            ll_posts = itemView.findViewById(R.id.post_list_goods_doc_click);
            //提示
            post_title = itemView.findViewById(R.id.post_title);

            mCommentOpen = itemView.findViewById(R.id.tv_comment_open);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //item长按事件
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    itemLong(getLayoutPosition());
                    return false;
                }
            });

            //点赞
            mPraiseClickFlag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        data.setIs_reply("1");
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            //回复点击
            mComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });

            //文字点击
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });
        }

    }


    /**
     * 热门
     */
    public class NewViewHolder extends RecyclerView.ViewHolder {
        TextView mForm;
        RelativeLayout mItemCommentsView;
        ImageView mImg;
        TextView mName;
        TextView mTitle;
        ImageView mIcon;
        TextView mCity;
        TextView mHosName;
        TextView mComments;
        LinearLayout mPraiseClickFlag;
        ImageView mPraiseFlag;
        TextView mPraiseNum;
        TextView mDate;
        TextView mContent;
        LinearLayout taoAndDiaryContiainer;
        ImageView taoAndDiaryPicture;
        TextView taoAndDiaryTitle;
        TextView taoAndDiaryPrice;
        RecyclerView mMoreImgList;
        LinearLayout mMoreListClick;
        RecyclerView mMoreList;
        LinearLayout mMoreListMoreClick;
        TextView mMoreListMoreTitle;
        TextView mMbtn;
        TextView tv_dots;

        CardView cardview;
        ImageView skuImg;
        TextView viewContent;
        TextView price;
        TextView originalPrice;
        RelativeLayout plusPriceVibiable;
        TextView plusPrice;
        TextView btn;
        LinearLayout ll_posts;
        TextView post_title;

        public NewViewHolder(View itemView) {
            super(itemView);
            mForm = itemView.findViewById(R.id.tv_type);
            mItemCommentsView = itemView.findViewById(R.id.item_diary_list_recycview_comments);
            mImg = itemView.findViewById(R.id.diary_list_comments_img);
            mName = itemView.findViewById(R.id.diary_list_comments_name);
            mTitle = itemView.findViewById(R.id.diary_list_comments_title);
            mIcon = itemView.findViewById(R.id.diary_list_comments_icon);
            mCity = itemView.findViewById(R.id.diary_list_comments_city);
            mHosName = itemView.findViewById(R.id.diary_list_comments_hos);
            mComments = itemView.findViewById(R.id.diary_list_comments_comments);
            mPraiseClickFlag = itemView.findViewById(R.id.diary_list_comments_praise_click);
            mPraiseFlag = itemView.findViewById(R.id.diary_list_comments_praise_flag);
            mPraiseNum = itemView.findViewById(R.id.diary_list_comments_praise_num);
            mDate = itemView.findViewById(R.id.diary_list_comments_date);
            mContent = itemView.findViewById(R.id.diary_list_comments_content);
            taoAndDiaryContiainer = itemView.findViewById(R.id.comments_list_tao_diary_container);
            taoAndDiaryPicture = itemView.findViewById(R.id.comments_list_tao_diary_picture);
            taoAndDiaryTitle = itemView.findViewById(R.id.comments_list_tao_diary_title);
            taoAndDiaryPrice = itemView.findViewById(R.id.comments_list_tao_diary_price);
            mMoreImgList = itemView.findViewById(R.id.diary_list_comments_img_list);
            mMoreListClick = itemView.findViewById(R.id.diary_list_comments_more_list_click);
            mMoreList = itemView.findViewById(R.id.diary_list_comments_more_list);
            mMoreListMoreClick = itemView.findViewById(R.id.diary_list_comments_more_list_more_click);
            mMoreListMoreTitle = itemView.findViewById(R.id.diary_list_comments_more_list_more_title);
            mMbtn = itemView.findViewById(R.id.diary_list_comments_btn);
            tv_dots = itemView.findViewById(R.id.tv_dots);


            cardview = itemView.findViewById(R.id.cardview);

            //图片
            skuImg = itemView.findViewById(R.id.diary_list_goods_sku_img);
            //标题
            viewContent = itemView.findViewById(R.id.diary_list_goods_title);
            //价格布局
            price = itemView.findViewById(R.id.diary_list_goods_price);
            //价格
            originalPrice = itemView.findViewById(R.id.diary_list_goods_original_price);
//             //
//             diary_list_goods_original_price = itemView.findViewById(R.id.diary_list_goods_original_price);
            //plus布局
            plusPriceVibiable = itemView.findViewById(R.id.diary_list_plus_vibiable);
            //plus价格
            plusPrice = itemView.findViewById(R.id.plus_vip_price);
            //咨询按钮
            btn = itemView.findViewById(R.id.diary_list_goods_btn);
            //底部提示布局
            ll_posts = itemView.findViewById(R.id.post_list_goods_doc_click);
            //提示
            post_title = itemView.findViewById(R.id.post_title);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

            //item长按事件
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    itemLong(getLayoutPosition());
                    return false;
                }
            });

            //点赞
            mPraiseClickFlag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        data.setFlag("1");
                        data.setPos(getLayoutPosition());
                        data.setIs_reply("1");
                        onEventClickListener.onItemLikeClick(data);
                    }
                }
            });

            //回复点击
            mComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });

            //文字点击
            mContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (onEventClickListener != null) {
                            onEventClickListener.onItemReplyClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getUserdata().getName());
                        }
                    }
                }
            });
        }

    }

    /**
     * 长按事件
     *
     * @param pos
     */
    private void itemLong(int pos) {
        if (isOneselfDiary(pos)) {                                  //自己的长按删除
            DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
            diariesDeleteData.setId(mDatas.get(pos).getId());
            diariesDeleteData.setCommentsOrReply(0);
            diariesDeleteData.setReplystr("1");
            diariesDeleteData.setPos(pos);
            diariesDeleteData.setPosPos(-1);
            onEventClickListener.onItemDeleteClick(diariesDeleteData);
        } else {                                                  //他人的长按举报
            DiariesReportLikeData diariesReportLikeData = new DiariesReportLikeData();
            diariesReportLikeData.setId(mDatas.get(pos).getId());
            diariesReportLikeData.setFlag("2");
            diariesReportLikeData.setPos(pos);
            diariesReportLikeData.setIs_reply("1");
            onEventClickListener.onItemReportClick(diariesReportLikeData);
        }
    }

    public List<DiaryReplyList2> getmDatas() {
        return mDatas;
    }

    public void upDataOpenHotList(String open) {
        isOpenHot = open;
        notifyDataSetChanged();
    }

    public void addItem(DiaryReplyList2 diaryReplyList, int pos) {
        mDatas.add(pos, diaryReplyList);
        notifyItemInserted(pos);
    }

//    /**
//     * 删除数据
//     *
//     * @param startPosition 起始位置 itemcount 个数
//     * @return
//     */
//    public void delete(int startPosition, int itemcount) {
//        notifyItemRangeRemoved(startPosition, itemcount);
//    }

    /**
     * 判断是否是自己的日记
     *
     * @param position
     * @return
     */
    private boolean isOneselfDiary(int position) {
        return Utils.getUid().equals(mDatas.get(position).getUserdata().getId());
    }


    /**
     * 删除某一条数据
     *
     * @param pos
     */
    public void deleteItem(int pos) {
        mDatas.remove(pos);             //删除数据源
        notifyItemRemoved(pos);         //刷新被删除的地方
        notifyItemRangeChanged(pos, getItemCount()); //刷新被删除数据，以及其后面的数据
    }

    /**
     * 在第一条添加一条数据
     *
     * @param diaryReplyList
     */
    public void addItem(DiaryReplyList2 diaryReplyList) {
        mDatas.add(0, diaryReplyList);
        notifyItemInserted(0);
    }

    /**
     * 添加评论中的第一条回复
     *
     * @param pos
     * @param diaryReplyListUserdata
     */
    public void setCommentsPosReply(int pos, DiaryReplyListList diaryReplyListUserdata) {
        if(mDatas.get(pos).getList() != null){
            mDatas.get(pos).getList().add(diaryReplyListUserdata);
        }else{
            List<DiaryReplyListList> list = new ArrayList<>();
            list.add(diaryReplyListUserdata);
            mDatas.get(pos).setList(list);
        }
        notifyItemChanged(pos);
    }

    /**
     * 刷新某一条数据
     *
     * @param pos
     */
    public void notifyItem(int pos) {
        notifyItemChanged(pos);
    }

    /**
     * 添加一页数据
     */
    public void setAddData(List<DiaryReplyList2> newDatas) {
        mDatas.addAll(newDatas);
        notifyItemRangeChanged(mDatas.size() - newDatas.size(), newDatas.size());
    }

    public HashMap<Integer, MoreReplyListAdapter2> getHashMapAdapter() {
        return hashMapAdapter;
    }

    public interface OnEventClickListener {
        void onItemClick(View v, int pos);                      //item点击回调

        void onItemLikeClick(DiariesReportLikeData data);       //点赞回调

        void onItemReplyClick(View v, int pos, String name);                 //回复点击回调

        void onItemDeleteClick(DiariesDeleteData deleteData);   //删除长按回调

        void onItemReportClick(DiariesReportLikeData data);     //举报回调

        void onTaoAndDiaryClick(DiaryReplyListTao tao);

        void onCommentMoreClick(View v, int pos);               //点击评论加载更多
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    //查看更多评论点击回调
    private ItemJumpCallBackListener itemJumpCallBackListener;

    public interface ItemJumpCallBackListener {
        void onItemJumpClick(View v);
    }

    public void setItemJumpCallBackListener(ItemJumpCallBackListener itemJumpCallBackListener) {
        this.itemJumpCallBackListener = itemJumpCallBackListener;
    }
}
