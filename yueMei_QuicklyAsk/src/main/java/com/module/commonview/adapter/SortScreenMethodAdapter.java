package com.module.commonview.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.module.commonview.module.bean.ProjcetSXitem;

import java.util.List;

/**
 * 筛选实施方法
 * Created by 裴成浩 on 2019/5/17
 */
public class SortScreenMethodAdapter  extends RecyclerView.Adapter<SortScreenMethodAdapter.ViewHolder>{

    private Activity mContent;
    private List<ProjcetSXitem> mKindData;
    private final LayoutInflater mInflater;

    public SortScreenMethodAdapter(Activity content, List<ProjcetSXitem> kindData) {
        this.mContent = content;
        this.mKindData = kindData;
        mInflater = LayoutInflater.from(mContent);
    }

    @NonNull
    @Override
    public SortScreenMethodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        View itemView = mInflater.inflate(R.layout.item_diary_list_gridview, viewGroup, false);
//        return new ViewHolder(itemView);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull SortScreenMethodAdapter.ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
