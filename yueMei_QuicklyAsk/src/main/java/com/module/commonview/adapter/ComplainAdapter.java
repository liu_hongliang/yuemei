package com.module.commonview.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.ComplaintOption;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ComplainAdapter extends BaseQuickAdapter<ComplaintOption, BaseViewHolder> {

    private final int windowsWight;
    private List<ComplaintOption> data;
    private List<Integer> multiSelected = new ArrayList<>();

    public ComplainAdapter(int layoutResId, @Nullable List<ComplaintOption> data) {
        super(layoutResId, data);
        this.data = data;
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
    }

    @Override
    protected void convert(final BaseViewHolder helper, ComplaintOption item) {
        LinearLayout container = helper.getView(R.id.complain_item_container);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        layoutParams.width = (windowsWight- Utils.dip2px(59))/2;
        container.setLayoutParams(layoutParams);
        helper.setText(R.id.complain_text, item.getName());
        CheckBox checkBox = helper.getView(R.id.complain_text_check);
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiSelect(helper.getAdapterPosition());
            }
        });

        if (multiSelected.contains(helper.getAdapterPosition())) {
            checkBox.setBackground(ContextCompat.getDrawable(mContext, R.drawable.complain_select));
            helper.setChecked(R.id.complain_text_check, true);
        } else {
            if (multiSelected.size() == 2){
                if (!item.isIs_select()){
                    checkBox.setBackground(ContextCompat.getDrawable(mContext, R.drawable.complain_unselect_gray));
                }
            }else {
                if (!item.isIs_select()){
                    checkBox.setBackground(ContextCompat.getDrawable(mContext, R.drawable.complain_unselect));
                }
            }
            helper.setChecked(R.id.complain_text_check, false);
        }

    }


    @NonNull
    @Override
    public List<ComplaintOption> getData() {
        return data;
    }


    /**
     * 获取多选项位置，元素顺序按照选择顺序排列
     */
    public List<Integer> getMultiSelectedPosition() {
        return multiSelected;
    }

    public void multiSelect(int pos) {
        if (multiSelected.size() < 2){
            if (multiSelected.contains(pos)){
                multiSelected.remove((Integer)pos);
                data.get(pos).setIs_select(false);
            }else {
                multiSelected.add(pos);
                data.get(pos).setIs_select(true);
            }
        }else if (multiSelected.size() == 2 && multiSelected.contains(pos)){
            multiSelected.remove((Integer)pos);
            data.get(pos).setIs_select(false);
        }
        notifyDataSetChanged();
    }
}
