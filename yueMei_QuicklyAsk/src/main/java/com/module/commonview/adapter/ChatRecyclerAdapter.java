package com.module.commonview.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.Util;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.commonview.activity.ChatActivity;
import com.module.commonview.activity.HosImagShowActivity;
import com.module.commonview.activity.MediaManager;
import com.module.commonview.module.bean.LocusData;
import com.module.commonview.module.bean.MenuDoctorsData;
import com.module.commonview.module.bean.MessageBean;
import com.module.commonview.module.bean.MoreCouponsBean;
import com.module.commonview.module.bean.VoiceMessage;
import com.module.commonview.view.ChatJumpPopwindow;
import com.module.commonview.view.Expression;
import com.module.commonview.view.HttpTextView;
import com.module.commonview.view.ProcessImageView;
import com.module.commonview.view.RedPackPopwindows;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.bean.CaseFinalPic;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import org.apache.commons.collections.CollectionUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * 私信适配器
 * Created by Mao Jiqing on 2016/9/29.
 */
public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = "ChatRecyclerAdapter";
    private Activity context;
    private String mReferer;
    private List<MessageBean.DataBean> userList = new ArrayList<>();
    private ArrayList<String> imageList = new ArrayList<>();
    private HashMap<Integer, Integer> imagePosition = new HashMap<>();
    public static final int FROM_USER_MSG = 0;//接收消息类型
    public static final int TO_USER_MSG = 1;//发送消息类型
    public static final int FROM_USER_IMG = 2;//接收图片类型
    public static final int TO_USER_IMG = 3;//发送图片类型
    public static final int LOOKING_RIGHT = 4;//我正在看右
    public static final int LOOKING_LEFT = 5;//我正在看左
    public static final int COUPONS_LEFT = 6;//红包左
    public static final int COUPONS_RIGHT = 9;//红包左
    public static final int TIP = 7;// 安全提示
    public static final int LOOKMORE_LEFT = 8;// 多个sku
    public static final int LOOKMORE_RIGHT = 10;// 多个sku
    public static final int KOUBEI_LEFT = 11;// 口碑案例左
    public static final int KOUBEI_RIGHT = 12;//口碑案例右
    public static final int DOCTOR_LEFT = 13;//医生左
    public static final int DOCTOR_RIGHT = 14;//医生右
    public static final int VOICE_LEFT = 15;//语音左
    public static final int VOICE_RIGHT = 16;//语音右
    public static final int ADRESS_LEFT = 17;//医院地址左
    public static final int ADRESS_RIGHT = 18;//医院地址右
    public static final int SERVICE_ONLINE = 19;//客服在线


    private int mMinItemWith;// 设置对话框的最大宽度和最小宽度
    private int mMaxItemWith;
    public List<String> unReadPosition = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mResults = new ArrayList<>();
    private HashMap<String, ProcessImageView> imgs = new HashMap();
    private String mHos_id;
    private String mObj_id;
    private String mObj_type;
    private String mUid;
    private String mId;//聊天对象id
    private final HashMap<Integer, Boolean> map = new HashMap<>();
    private ImageView mVoiceView;
    private String isLeftOrRight;


    public ChatRecyclerAdapter(Activity context, List<MessageBean.DataBean> userList,String onLine) {
        this.context = context;
        this.userList = userList;
        mUid=Utils.getUid();
        mLayoutInflater = LayoutInflater.from(context);
        if (userList.size() < 10){
            //添加提示语
            MessageBean.DataBean dataBean = new MessageBean.DataBean();
            dataBean.setType(ChatRecyclerAdapter.TIP);
            userList.add(0, dataBean);
            //插入客服在线状态
            if("1".equals(onLine)){
                MessageBean.DataBean dataBean1 = new MessageBean.DataBean();
                dataBean1.setType(ChatRecyclerAdapter.SERVICE_ONLINE);
                userList.add(1, dataBean1);
            }
        }
        Log.e(TAG,"userList == "+userList.toString());
        // 获取系统宽度
        WindowManager wManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wManager.getDefaultDisplay().getMetrics(outMetrics);
        mMaxItemWith = (int) (outMetrics.widthPixels * 0.5f);
        mMinItemWith = (int) (outMetrics.widthPixels * 0.15f);

    }

    public ChatRecyclerAdapter(Activity context, List<MessageBean.DataBean> userList, String hos_id, String objId, String objType,String mId,String onLine) {
        this.context = context;
        this.userList = userList;
        this.mHos_id = hos_id;
        this.mObj_id = objId;
        this.mObj_type = objType;
        mUid=Utils.getUid();
        this.mId=mId;
        Log.d(TAG, "hosid======" + mHos_id);
        Log.d(TAG, "objid======" + mObj_id);
        Log.d(TAG, "objtype======" + mObj_type);
        if (userList.size() < 10){
            //添加提示语
            MessageBean.DataBean dataBean = new MessageBean.DataBean();
            dataBean.setType(ChatRecyclerAdapter.TIP);
            userList.add(0, dataBean);
            //插入客服在线状态
            if("1".equals(onLine)){
                MessageBean.DataBean dataBean1 = new MessageBean.DataBean();
                dataBean1.setType(ChatRecyclerAdapter.SERVICE_ONLINE);
                userList.add(1, dataBean1);
            }

        }

        Log.e(TAG,"userList == "+userList.toString());
        mLayoutInflater = LayoutInflater.from(context);
        // 获取系统宽度
        WindowManager wManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wManager.getDefaultDisplay().getMetrics(outMetrics);
        mMaxItemWith = (int) (outMetrics.widthPixels * 0.5f);
        mMinItemWith = (int) (outMetrics.widthPixels * 0.15f);
    }

    public void addMessage(List<MessageBean.DataBean> userList){
        this.userList=userList;
    }

    @Override
    public int getItemViewType(int position) {
        return userList.get(position).getType();
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case FROM_USER_MSG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_msgfrom_list_item, null, false);
                holder = new FromUserMsgViewHolder(view);
                break;
            case FROM_USER_IMG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_imagefrom_list_item, null, false);
                holder = new FromUserImageViewHolder(view);
                break;
            case TO_USER_MSG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_msgto_list_item, null, false);
                holder = new ToUserMsgViewHolder(view);
                break;
            case TO_USER_IMG:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_imageto_list_item, null, false);
                holder = new ToUserImgViewHolder(view);
                break;
            case LOOKING_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_looking_list_item_right, null, false);
                holder = new LookingViewHolder(view);
                break;
            case LOOKING_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_looking_list_item_left, null, false);
                holder = new LookingViewHolder(view);
                break;
            case COUPONS_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_redpack_list_item, null, false);
                holder = new CouponsViewHolder(view);
                break;
            case COUPONS_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_redpack_right_item, null, false);
                holder = new CouponsViewHolder(view);
                break;
            case TIP:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_tip_list_item, null, false);
                holder = new TipViewHolder(view);
                break;
            case SERVICE_ONLINE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_service_online_item, parent, false);
                holder = new ServiceOnlineHolder(view);
                break;
            case LOOKMORE_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_more_sku, null, false);
                holder = new LookMoreHolder(view);
                break;
            case LOOKMORE_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_more_sku_right, null, false);
                holder = new LookMoreHolder(view);
                break;
            case ADRESS_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_hosadress_left, null, false);
                holder = new HosAdressLeftHolder(view);
                break;
            case ADRESS_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_hosadress_right, null, false);
                holder = new HosAdressRightHolder(view);
                break;
            case KOUBEI_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_compar_left, null, false);
                holder = new KouBeiViewHolder(view);
                break;
            case KOUBEI_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_compar_right, null, false);
                holder = new KouBeiViewHolder(view);
                break;
            case DOCTOR_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_doctor_left, null, false);
                holder = new DoctorViewHolder(view);
                break;
            case DOCTOR_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_dortor_right, null, false);
                holder = new DoctorViewHolder(view);
                break;
            case VOICE_LEFT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_voice_left, null, false);
                holder = new VoiceLeftViewHolder(view);
                break;
            case VOICE_RIGHT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_voice_right, null, false);
                holder = new VoiceRightViewHolder(view);
                break;

        }
        return holder;
    }

    class FromUserMsgViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private TextView chat_time;
        private RelativeLayout messageContainer;
        private HttpTextView content;
        private ImageView sendFailImg;

        public FromUserMsgViewHolder(View view) {
            super(view);
            messageContainer = view.findViewById(R.id.message_container);
            headicon = view.findViewById(R.id.tb_other_user_icon);
            chat_time = view.findViewById(R.id.chat_time1);
            content = view.findViewById(R.id.content);
            sendFailImg = view.findViewById(R.id.mysend_fail_img);

        }
    }

    class FromUserImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private TextView chat_time;
        private ImageView image_Msg;
        private ImageView sendFailImg;

        private LinearLayout image_group;

        public FromUserImageViewHolder(View view) {
            super(view);
            headicon = view
                    .findViewById(R.id.tb_other_user_icon);
            chat_time = view.findViewById(R.id.chat_time);
            image_Msg = view.findViewById(R.id.image_message);
            sendFailImg = view.findViewById(R.id.mysend_fail_img);
            image_group = view.findViewById(R.id.image_group);
        }
    }

    class ToUserMsgViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private HttpTextView content;
        private TextView chatTime;
        private RelativeLayout messageContainer;
        private ImageView sendFailImg;

        public ToUserMsgViewHolder(View view) {
            super(view);
            chatTime = view.findViewById(R.id.mychat_time);
            headicon = view.findViewById(R.id.tb_my_user_icon);
            content = view.findViewById(R.id.mycontent);
            messageContainer=view.findViewById(R.id.message_container_right);
            sendFailImg = view.findViewById(R.id.mysend_fail_img);
        }
    }

    class ToUserImgViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private TextView chat_time;
        private LinearLayout image_group;
        private ImageView image_Msg;
        private ImageView sendFailImg, sendFailProgress;
        private ProcessImageView image_Progress;

        public ToUserImgViewHolder(View view) {
            super(view);
            headicon = view
                    .findViewById(R.id.tb_my_user_icon);
            chat_time = view
                    .findViewById(R.id.mychat_time);
            sendFailImg = view
                    .findViewById(R.id.mysend_fail_img);
            image_group = view
                    .findViewById(R.id.image_group);
            image_Msg = view
                    .findViewById(R.id.image_message);
            image_Progress = view.findViewById(R.id.image_progress);
            sendFailProgress = view.findViewById(R.id.mysend_fail_progress);

        }
    }

    class LookingViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private TextView chat_time;
        private TextView content, desc, price;
        private ImageView lookingIcon;
        private LinearLayout mLinearLayout, mLooking;
        private LinearLayout mPlusVisibity;
        private TextView mPlusPrice;

        public LookingViewHolder(View view) {
            super(view);
            headicon = view.findViewById(R.id.tb_my_user_icon);
            chat_time = view.findViewById(R.id.mychat_time);
            content = view.findViewById(R.id.mycontent);
            lookingIcon = view.findViewById(R.id.looking_icon);
            desc = view.findViewById(R.id.looking_desc);
            price = view.findViewById(R.id.looking_price);
            mLinearLayout = view.findViewById(R.id.look_conprice);
            mLooking = view.findViewById(R.id.looking);
            mPlusVisibity=view.findViewById(R.id.tao_plus_vibility);
            mPlusPrice=view.findViewById(R.id.tao_plus_price);
        }
    }


    class CouponsViewHolder extends RecyclerView.ViewHolder{
        private RelativeLayout mLinearContent;
        private ImageView headicon;
        private LinearLayout mClick;
        private RelativeLayout mContent;
        private ImageView mRedPack;
        private TextView mTitle,mMoney,mLowestConsumption,mDesc;
        public CouponsViewHolder(View itemView) {
            super(itemView);
            mLinearContent=itemView.findViewById(R.id.lin_content);
            headicon = itemView.findViewById(R.id.layout_redpack_user_icon);
            mClick=itemView.findViewById(R.id.layout_redpack_click);
            mContent=itemView.findViewById(R.id.layout_redpack_layout);
            mRedPack=itemView.findViewById(R.id.redpack);
            mTitle=itemView.findViewById(R.id.layout_redpack_title);
            mMoney=itemView.findViewById(R.id.layout_redpack_money);
            mLowestConsumption=itemView.findViewById(R.id.layout_redpack_lowest_consumption);
            mDesc=itemView.findViewById(R.id.layout_redpack_desc);

        }
    }

    class TipViewHolder extends RecyclerView.ViewHolder{

        public TipViewHolder(@NonNull View itemView) {
            super(itemView);
            TextView textView = itemView.findViewById(R.id.item_tip);
        }
    }

    class ServiceOnlineHolder extends RecyclerView.ViewHolder{

        public ServiceOnlineHolder(@NonNull View itemView) {
            super(itemView);
        }
    }



    class LookMoreHolder extends RecyclerView.ViewHolder{
        private TextView chatTime;
        private RelativeLayout chatContainer;
        private ImageView userIcon;
        private TextView chatTitle;
        private RecyclerView chatList;
        public LookMoreHolder(@NonNull View itemView) {
            super(itemView);
            chatTime = itemView.findViewById(R.id.mychat_time);
            chatContainer = itemView.findViewById(R.id.mychat_container);
            userIcon = itemView.findViewById(R.id.tb_my_user_icon);
            chatTitle = itemView.findViewById(R.id.mycontent);
            chatList = itemView.findViewById(R.id.more_sku_list);
        }
    }


    class HosAdressLeftHolder extends RecyclerView.ViewHolder{
        private LinearLayout container;
        private ImageView userIcon;
        private ImageView hosIcon;
        private TextView hosName;
        private TextView hosAdress;
        public HosAdressLeftHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.looking);
            userIcon = itemView.findViewById(R.id.tb_my_user_icon);
            hosIcon = itemView.findViewById(R.id.hosadress_img);
            hosName = itemView.findViewById(R.id.hosadress_name);
            hosAdress = itemView.findViewById(R.id.hospital_adress);
        }
    }

    class HosAdressRightHolder extends RecyclerView.ViewHolder{
        private LinearLayout container;
        private ImageView userIcon;
        private ImageView hosIcon;
        private TextView hosName;
        private TextView hosAdress;
        public HosAdressRightHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.looking);
            userIcon = itemView.findViewById(R.id.tb_my_user_icon);
            hosIcon = itemView.findViewById(R.id.hosadress_img);
            hosName = itemView.findViewById(R.id.hosadress_name);
            hosAdress = itemView.findViewById(R.id.hospital_adress);
        }
    }

    class KouBeiViewHolder extends RecyclerView.ViewHolder{
        private TextView chatTime;
        private RelativeLayout chatContainer;
        private ImageView userIcon;
        private TextView chatTitle,chatContent;
        private RecyclerView chatList;

        public KouBeiViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.findViewById(R.id.mychat_time);
            chatContainer = itemView.findViewById(R.id.mychat_container);
            userIcon = itemView.findViewById(R.id.tb_my_user_icon);
            chatTitle = itemView.findViewById(R.id.compar_title);
            chatContent = itemView.findViewById(R.id.compar_content);
            chatList = itemView.findViewById(R.id.compar_list);
        }
    }

    class DoctorViewHolder extends RecyclerView.ViewHolder{
        private TextView chatTime;
        private RelativeLayout chatContainer;
        private ImageView userIcon;
        private RecyclerView chatList;
        public DoctorViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.findViewById(R.id.mychat_time);
            chatContainer = itemView.findViewById(R.id.mychat_container);
            userIcon = itemView.findViewById(R.id.tb_my_user_icon);
            chatList = itemView.findViewById(R.id.doctor_list);
        }
    }

    class VoiceLeftViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private HttpTextView content;
        private TextView chatTime;
        private LinearLayout voiceClick;
        private ImageView voiceImg;
        private RelativeLayout messageContainer;
        private ImageView sendFailImg;

        public VoiceLeftViewHolder(View view) {
            super(view);
            chatTime = view.findViewById(R.id.chat_time1);
            headicon = view.findViewById(R.id.tb_other_user_icon);
            content = view.findViewById(R.id.content);
            voiceClick = view.findViewById(R.id.voice_click);
            voiceImg = view.findViewById(R.id.voice_img);
            messageContainer=view.findViewById(R.id.message_container);
            sendFailImg = view.findViewById(R.id.mysend_fail_img);
        }
    }

    class VoiceRightViewHolder extends RecyclerView.ViewHolder {
        private ImageView headicon;
        private HttpTextView content;
        private TextView chatTime;
        private LinearLayout voiceClick;
        private RelativeLayout messageContainer;
        private ImageView voiceImg;
        private ImageView sendFailImg;

        public VoiceRightViewHolder(View view) {
            super(view);
            chatTime = view.findViewById(R.id.mychat_time);
            headicon = view.findViewById(R.id.tb_my_user_icon);
            content = view.findViewById(R.id.mycontent);
            voiceClick = view.findViewById(R.id.voice_click);
            messageContainer=view.findViewById(R.id.message_container_right);
            voiceImg = view.findViewById(R.id.voice_img);
            sendFailImg = view.findViewById(R.id.mysend_fail_img);
        }
    }





    /**
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessageBean.DataBean tbub = userList.get(position);
        int itemViewType = getItemViewType(position);
        Log.d(TAG, "onBindViewHolder-------------------");
        switch (itemViewType) {
            case FROM_USER_MSG:
                fromMsgUserLayout((FromUserMsgViewHolder) holder, tbub, position);
                break;
            case FROM_USER_IMG:
                fromImgUserLayout((FromUserImageViewHolder) holder, tbub, position);
                break;
            case TO_USER_MSG:
                toMsgUserLayout((ToUserMsgViewHolder) holder, tbub, position);
                break;
            case TO_USER_IMG:
                toImgUserLayout((ToUserImgViewHolder) holder, tbub, position);
                break;
            case LOOKING_RIGHT:
                toLookingLayout((LookingViewHolder) holder, tbub, position);
                break;
            case LOOKING_LEFT:
                toLookingLayout((LookingViewHolder) holder, tbub, position);
                break;
            case COUPONS_LEFT:
                toCouponsLayout((CouponsViewHolder) holder, tbub, position);
                break;
            case COUPONS_RIGHT:
                toCouponsLayout((CouponsViewHolder) holder, tbub, position);
                break;
            case LOOKMORE_LEFT:
                toLookMoreLayout((LookMoreHolder) holder,tbub,position);
                break;
            case ADRESS_LEFT:
                hosAdressLeftLayout((HosAdressLeftHolder)holder,tbub);
                break;
            case ADRESS_RIGHT:
                hosAdressRightLayout((HosAdressRightHolder)holder,tbub);
                break;
            case LOOKMORE_RIGHT:
                toLookMoreLayout((LookMoreHolder) holder,tbub,position);
                break;
            case KOUBEI_LEFT:
                kouBeiLayout((KouBeiViewHolder) holder,tbub,position);
                break;
            case KOUBEI_RIGHT:
                kouBeiLayout((KouBeiViewHolder) holder,tbub,position);
                break;
            case DOCTOR_LEFT:
                doctorLayout((DoctorViewHolder)holder,tbub,position);
                break;
            case DOCTOR_RIGHT:
                doctorLayout((DoctorViewHolder)holder,tbub,position);
                break;
            case VOICE_LEFT:
                voiceLeftLayout((VoiceLeftViewHolder)holder,tbub,position);
                break;
            case VOICE_RIGHT:
                voiceRightLayout((VoiceRightViewHolder)holder,tbub,position);
                break;
        }
    }


    @Override
    public int getItemCount() {
        Log.d(TAG, "Listsize==========>" + userList.size());
        return userList.size();
    }

    private void fromMsgUserLayout(final FromUserMsgViewHolder holder, final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {
            Log.e(TAG, "tbub === " + tbub);
            if (tbub != null) {
                Glide.with(context).load(tbub.getUser_avatar())
                        .transform(new GlideCircleTransform(context))
                        .into(holder.headicon);
                setTimePoint(holder.chat_time, tbub, position);

                holder.content.setVisibility(View.VISIBLE);

                try {
                    String content=tbub.getContent().replace("\\n","\n");
                    Expression.handlerEmojiText(holder.content, content, context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e(TAG, "tbub.getMessageStatus() === " + tbub.getMessageStatus());
                if (tbub.getMessageStatus() == 0) {
                    holder.sendFailImg.setVisibility(View.GONE);
                } else {
                    holder.sendFailImg.setVisibility(View.VISIBLE);
                }


            }
        }

    }

    private void fromImgUserLayout(final FromUserImageViewHolder holder, final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {
            Glide.with(context).load(tbub.getUser_avatar())
                    .transform(new GlideCircleTransform(context))
                    .into(holder.headicon);
            setTimePoint(holder.chat_time, tbub, position);
            int res;
            res = R.drawable.message_left;
            Glide.with(context)
                    .load(tbub.getImgdata_android().get(0).getImg())
                    .transform(new GlideRoundTransform(context, Utils.dip2px(5)))
                    .into(holder.image_Msg);
            if (tbub.getMessageStatus() == 0) {
                holder.sendFailImg.setVisibility(View.GONE);
            } else {
                holder.sendFailImg.setVisibility(View.VISIBLE);
            }
            holder.image_Msg.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    List<CaseFinalPic> list = new ArrayList<CaseFinalPic>();
                    Log.e(TAG, "333 === " + tbub.getImgdata_android().get(0).getImg_y());
                    list.add(new CaseFinalPic(tbub.getImgdata_android().get(0).getImg_y()));
                    Intent it = new Intent();
                    it.setClass(context, HosImagShowActivity.class);
                    it.putExtra("casefinaljson", (Serializable) list);
                    context.startActivity(it);
                }

            });
        }

    }


    private void toMsgUserLayout(final ToUserMsgViewHolder holder, final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {

            Glide.with(context).load(tbub.getUser_avatar())
                    .transform(new GlideCircleTransform(context))
                    .into(holder.headicon);

        /* time */

            setTimePoint(holder.chatTime, tbub, position);

            try {
                String content=tbub.getContent().replace("\\n","\n");
                Expression.handlerEmojiText(holder.content,content, context);
            } catch (Exception e) {

                e.printStackTrace();
            }
            if (tbub.getMessageStatus() == 0) {
                holder.sendFailImg.setVisibility(View.GONE);
            } else {
                holder.sendFailImg.setVisibility(View.VISIBLE);
            }

        }
    }


    private void toImgUserLayout(final ToUserImgViewHolder holder, final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {
            Log.d(TAG, "position---------------->" + position);
            Glide.with(context).load(tbub.getUser_avatar())
                    .transform(new GlideCircleTransform(context))
                    .into(holder.headicon);
            holder.image_group.setVisibility(View.VISIBLE);
            setTimePoint(holder.chat_time, tbub, position);
            int res;
            res = R.drawable.message_right;

            Glide.with(context)
                    .load(tbub.getImgdata_android().get(0).getImg())
                    .transform(new GlideRoundTransform(context, Utils.dip2px(5)))
                    .into(holder.image_Msg);

            if (tbub.getMessageStatus() == 0) {
                holder.sendFailImg.setVisibility(View.GONE);
                holder.sendFailProgress.setVisibility(View.GONE);
            } else {
                holder.sendFailImg.setVisibility(View.VISIBLE);
            }
            holder.sendFailImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            // holder.image_Msg.setProgress(tbub.getProgress());
            ChatActivity.setImageProgress(new ChatActivity.ImageProgress() {
                @Override
                public void setProgress(int progress) {
                    holder.image_Progress.setVisibility(View.VISIBLE);
                    holder.image_Progress.setProgress(progress);
                    if (progress == 100) {
                        holder.image_Progress.setVisibility(View.GONE);

                    }
                }
            });
            if (holder.image_Progress.getVisibility() == View.GONE) {
                holder.image_Msg.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Auto-generated method stub
                        List<CaseFinalPic> list = new ArrayList<>();
                        Log.e(TAG, "uri == " + tbub.getImgdata_android().get(0).getImg_y());
                        list.add(new CaseFinalPic(tbub.getImgdata_android().get(0).getImg_y()));
                        Intent it = new Intent();
                        it.setClass(context, HosImagShowActivity.class);
                        it.putExtra("casefinaljson", (Serializable) list);
                        context.startActivity(it);
                    }

                });
            }

            holder.sendFailImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String path = tbub.getImgdata_android().get(0).getImg();
                    String path1 = tbub.getImgdata_android().get(0).getImg_y();
                    Log.e(TAG, "path === " + path);
                    Log.e(TAG, "path1 === " + path1);
                    ((ChatActivity) context).sendImage(new File(path), path1);
                    holder.sendFailImg.setVisibility(View.GONE);
                    holder.sendFailProgress.setVisibility(View.VISIBLE);
                    Animation operatingAnim = AnimationUtils.loadAnimation(context, R.anim.rotateanm);
                    holder.sendFailProgress.startAnimation(operatingAnim);
                }
            });
        }
    }


    private void toLookingLayout(final LookingViewHolder holder, final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null) {
                if (!TextUtils.isEmpty(tbub.getClassidStr())){
                    switch (tbub.getClassidStr()){
                        case "3":
                            String msg_title = tbub.getGuijiata_android().get(0).getMsg_title();
                            if (!TextUtils.isEmpty(msg_title)){
                                holder.content.setText(msg_title);
                            }else {
                                holder.content.setText("我正在看");
                            }
                            holder.desc.setText(tbub.getGuijiata_android().get(0).getTitle());
                            break;
                        case "6":
                            holder.content.setText(tbub.getGuijiata_android().get(0).getMsg_title());
                            holder.desc.setText(tbub.getGuijiata_android().get(0).getTitle());
                            break;
                        case "10"://plus会员
                            String msg_title2 = tbub.getGuijiata_android().get(0).getMsg_title();
                            if (!TextUtils.isEmpty(msg_title2)){
                                holder.content.setText(msg_title2);
                            }else {
                                holder.content.setText("我正在看");
                            }
                            holder.desc.setSingleLine(false);
                            holder.desc.setText(tbub.getGuijiata_android().get(0).getTitle());
                            break;
                        default:

                    }
                }else {
                    String msg_title = tbub.getGuijiata_android().get(0).getMsg_title();
                    if (!TextUtils.isEmpty(msg_title)){
                        holder.content.setText(msg_title);
                    }else {
                        holder.content.setText("我正在看");
                    }

                    holder.desc.setText(tbub.getGuijiata_android().get(0).getTitle());
                }



                setTimePoint(holder.chat_time, tbub, position);
                if (Util.isOnMainThread() && !context.isFinishing()) {
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.headicon);
                    Glide.with(context).load(tbub.getGuijiata_android().get(0).getImg()).into(holder.lookingIcon);
                }
                if (!TextUtils.isEmpty(tbub.getGuijiata_android().get(0).getPrice()) && !"0".equals(tbub.getGuijiata_android().get(0).getPrice())) {
                    holder.mLinearLayout.setVisibility(View.VISIBLE);
                    holder.price.setText(tbub.getGuijiata_android().get(0).getPrice());
                } else {
                    holder.mLinearLayout.setVisibility(View.GONE);
                }
                if (tbub.getGuijiata_android().get(0).getMember_price() != null){
                    String member_price = tbub.getGuijiata_android().get(0).getMember_price();
                    int i = Integer.parseInt(member_price);
                    if (i >= 0){
                        holder.mPlusVisibity.setVisibility(View.VISIBLE);
                        holder.mPlusPrice.setText("¥"+member_price);
                    }else {
                        holder.mPlusVisibity.setVisibility(View.GONE);
                    }
                }


                holder.mLooking.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e(TAG, "mObj_type == " + mObj_type);
                        Log.e(TAG, "mObj_id == " + mObj_id);
                        String groupId = Cfg.loadStr(context, FinalConstant.GROUP_ID, "");
                        String userMore = Cfg.loadStr(context, FinalConstant.USER_MORE, "");
                        Log.e(TAG, "grouid == " + groupId);
                        Log.e(TAG, "userMore == " + userMore);
                        if (!"1".equals(groupId) || "1".equals(userMore)){
                            return;
                        }
                        String Appurl = tbub.getGuijiata_android().get(0).getApp_url();
                        if (!TextUtils.isEmpty(Appurl)){
                            if (Appurl.contains("type:eq")) {
                                BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(context);
                                baseWebViewClientMessage.showWebDetail(Appurl);
                            }else {
                                WebUrlTypeUtil.getInstance(context).urlToApp(tbub.getGuijiata_android().get(0).getApp_url(), "0", "0");
                            }
                        }
                    }
                });
            }
        }
    }

    private void toCouponsLayout(final CouponsViewHolder holder, final MessageBean.DataBean tbub, final int position){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null) {
                final MessageBean.CouponsBean couponsBean = tbub.getCoupons();
                final String coupons_id = couponsBean.getCoupons_id();
                if (!TextUtils.isEmpty(coupons_id) && !"0".equals(coupons_id)){
                    holder.mLinearContent.setVisibility(View.VISIBLE);
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.headicon);
                    holder.mTitle.setText(couponsBean.getTitle());
                    holder.mMoney.setText(couponsBean.getMoney());
                    String couponsDesc = couponsBean.getCouponsDesc();
                    if (!TextUtils.isEmpty(couponsDesc)){
                        holder.mLowestConsumption.setText("（"+couponsDesc+"）");
                    }else {
                        holder.mLowestConsumption.setText("（满"+couponsBean.getLowest_consumption()+"可用）");
                    }
                    holder.mDesc.setText(couponsBean.getHos_name());
                    if ("1".equals(couponsBean.getIs_get())){
                        holder.mRedPack.setBackgroundResource(R.drawable.red_pack_ok);
                        holder.mContent.setBackgroundResource(R.drawable.red_pack_top_ok);
                        holder.mDesc.setText("已存入-我的优惠券");
                    }
                    holder.mClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()){
                                return;
                            }
                            if ("1".equals(couponsBean.getIs_get()))return;
                            BaseNetWorkCallBackApi api = new BaseNetWorkCallBackApi(FinalConstant1.CHAT, "receivecoupons");
                            api.addData("coupons_ids",coupons_id);
                            api.addData("msg_id",tbub.getMsg_id());
                            api.addData("id",mId);
                            api.startCallBack(new BaseCallBackListener<ServerData>() {
                                @Override
                                public void onSuccess(ServerData serverData) {
                                    if ("1".equals(serverData.code)){
                                        HashMap<String, String> event_params = couponsBean.getEvent_params();
                                        if (event_params != null){
                                            event_params.put("id",mHos_id);
                                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MENU_CHAT_COUPONS,"0",mHos_id),event_params,new ActivityTypeData("151"));
                                        }
                                        MoreCouponsBean moreCouponsBean = JSONUtil.TransformSingleBean(serverData.data, MoreCouponsBean.class);
                                        List<MessageBean.CouponsBean> receiveSuccess = moreCouponsBean.getReceiveSuccess();
                                        RedPackPopwindows redPackPopwindows = new RedPackPopwindows(context, holder.mLinearContent, receiveSuccess);
                                        if (!redPackPopwindows.isShowing()){
                                            redPackPopwindows.showAtLocation(holder.mLinearContent, Gravity.BOTTOM, 0, 0);
                                            tbub.getCoupons().setIs_get("1");
                                            notifyDataSetChanged();
                                        }
                                    }else {
                                        if (Utils.isLogin()){
                                            Toast.makeText(context,serverData.message,Toast.LENGTH_SHORT).show();
                                        }else {
                                            Toast.makeText(context,"登录成功后才能领取优惠券",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });


                        }
                    });
                }else {
                    holder.mLinearContent.setVisibility(View.GONE);
                }

            }
        }
    }

    private List<LocusData> dataList = null;
    private void toLookMoreLayout(final LookMoreHolder holder, final MessageBean.DataBean tbub, final int position){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null){

                List<LocusData> menuTaoData = tbub.getMenuTaoData();//淘整形
                if (menuTaoData != null && menuTaoData.size() >0){
                    dataList=menuTaoData;
                }else {
                    List<LocusData> locusData = tbub.getLocusData();
                    if (locusData != null && locusData.size() > 0){
                        dataList=locusData;
                    }
                }

                if (dataList != null && dataList.size() >0) {
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.userIcon);
//                    String msg_title = dataList.get(0).getMsg_title();
//                    if (!TextUtils.isEmpty(msg_title)) {
//                        holder.chatTitle.setText(msg_title);
//                    }
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
                    holder.chatList.setLayoutManager(layoutManager);
                    LookMoreAdapter lookMoreAdapter = new LookMoreAdapter(R.layout.more_sku_list_item, dataList,mHos_id);
                    holder.chatList.setAdapter(lookMoreAdapter);
                    lookMoreAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            if (Utils.isFastDoubleClick()) return;
                            List<LocusData> data = adapter.getData();
                            String app_url = data.get(position).getApp_url();
                            if (app_url.contains("hospitalmap") || app_url.contains("chatdiary")) {
                                showJumpPop(holder.chatContainer, app_url);
                            } else WebUrlTypeUtil.getInstance(context).urlToApp(app_url);

                        }
                    });
                }
            }
        }
    }


    private void hosAdressLeftLayout(final HosAdressLeftHolder holder, final MessageBean.DataBean tbub){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null){
                final List<LocusData> locusData = tbub.getLocusData();
                if (CollectionUtils.isNotEmpty(locusData)){
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.userIcon);
                    Glide.with(context).load(locusData.get(0).getImg()).into(holder.hosIcon);
                    holder.hosName.setText(locusData.get(0).getTitle());
                    holder.hosAdress.setText(locusData.get(0).getAddress());
                    holder.container.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) return;
                            String appUrl = locusData.get(0).getApp_url();
                            if (appUrl.contains("hospitalmap") || appUrl.contains("chatdiary")) {
                                showJumpPop(holder.container, appUrl);
                            } else WebUrlTypeUtil.getInstance(context).urlToApp(appUrl);
                        }
                    });
                }
            }
        }
    }

    private void hosAdressRightLayout(final HosAdressRightHolder holder, final MessageBean.DataBean tbub){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null){

                final List<LocusData> locusData = tbub.getLocusData();
                if (CollectionUtils.isNotEmpty(locusData)){
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.userIcon);
                    Glide.with(context).load(locusData.get(0).getImg()).into(holder.hosIcon);
                    holder.hosName.setText(locusData.get(0).getTitle());
                    holder.hosAdress.setText(locusData.get(0).getAddress());
                    holder.container.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) return;
                            String appUrl = locusData.get(0).getApp_url();
                            if (appUrl.contains("hospitalmap") || appUrl.contains("chatdiary")) {
                                showJumpPop(holder.container, appUrl);
                            } else WebUrlTypeUtil.getInstance(context).urlToApp(appUrl);
                        }
                    });
                }
            }
        }
    }

    /**
     * 加载口碑日记的数据
     * @param holder
     * @param tbub
     * @param position
     */
    private void kouBeiLayout(final KouBeiViewHolder holder, final MessageBean.DataBean tbub, final int position){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null){
                final List<LocusData> menuDiaryData = tbub.getMenuDiaryData();
                if (menuDiaryData != null && menuDiaryData.size() > 0){
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.userIcon);
                    holder.chatTitle.setText(menuDiaryData.get(0).getTitle());
                    holder.chatContent.setText(menuDiaryData.get(0).getMsg_title());
                    ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    scrollLinearLayoutManager.setScrollEnable(false);
                    holder.chatList.setLayoutManager(scrollLinearLayoutManager);
                    ChatListItemkoubeiAdapter chatListItemkoubeiAdapter = new ChatListItemkoubeiAdapter(R.layout.chat_compar_list_item, menuDiaryData);
                    holder.chatList.setAdapter(chatListItemkoubeiAdapter);
                    chatListItemkoubeiAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            String app_url = menuDiaryData.get(position).getApp_url();
                            if (app_url.contains("hospitalmap") || app_url.contains("chatdiary")) {
                                showJumpPop(holder.chatContainer, app_url);
                            } else WebUrlTypeUtil.getInstance(context).urlToApp(app_url);
                        }
                    });
                }

            }
        }
    }


    /**
     * 加载医生的数据
     * @param holder
     * @param tbub
     * @param position
     */
    private void doctorLayout(final DoctorViewHolder holder, final MessageBean.DataBean tbub, final int position){
        if (Util.isOnMainThread() && !context.isFinishing()) {
            if (tbub != null){
                final List<MenuDoctorsData> menuDoctorsData = tbub.getMenuDoctorsData();
                if (menuDoctorsData != null && menuDoctorsData.size() > 0){
                    Glide.with(context).load(tbub.getUser_avatar())
                            .transform(new GlideCircleTransform(context))
                            .into(holder.userIcon);
                    ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    scrollLinearLayoutManager.setScrollEnable(false);
                    holder.chatList.setLayoutManager(scrollLinearLayoutManager);
                    ChatItemDoctorAdapter chatItemDoctorAdapter = new ChatItemDoctorAdapter(R.layout.chat_list_item_doctor, menuDoctorsData);
                    holder.chatList.setAdapter(chatItemDoctorAdapter);
                    chatItemDoctorAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                            WebUrlTypeUtil.getInstance(context).urlToApp(menuDoctorsData.get(position).getApp_url());
                        }
                    });
                }


            }
        }
    }

    /**
     *  加载左边声音的数据
     * @param holder
     * @param tbub
     * @param position
     */
    private void voiceLeftLayout(VoiceLeftViewHolder holder,final MessageBean.DataBean tbub,final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {

            Glide.with(context).load(tbub.getUser_avatar())
                    .transform(new GlideCircleTransform(context))
                    .into(holder.headicon);

            /* time */
            setTimePoint(holder.chatTime, tbub, position);
            holder.content.setVisibility(View.VISIBLE);
            VoiceMessage voiceMessages = tbub.getVoiceMessages();
            final String playerUrl = voiceMessages.getPlayerUrl();
            String voiceTime = voiceMessages.getVoiceTime();
            holder.content.setText(voiceTime+"\"");
            if (!voiceMessages.isPlay()){
                Glide.with(context).load(R.drawable.play_voice_left).into(holder.voiceImg);
            }else {
                Glide.with(context).load(R.drawable.playing_voice_left).asGif().into(mVoiceView);
            }
            if (tbub.getMessageStatus() == 0) {
                holder.sendFailImg.setVisibility(View.GONE);
            } else {
                holder.sendFailImg.setVisibility(View.VISIBLE);
            }
            holder.voiceClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mVoiceView != null) {
                        Log.e(TAG, "isLeftOrRight ==" + isLeftOrRight);
                        if ("Left".equals(isLeftOrRight)) {
                            Glide.with(context).load(R.drawable.play_voice_left).into(mVoiceView);
                        } else {
                            isLeftOrRight = "Left";
                            Glide.with(context).load(R.drawable.play_voice_right).into(mVoiceView);
                        }
                        mVoiceView = null;
                    }else {
                        isLeftOrRight = "Left";
                    }
                    mVoiceView = v.findViewById(R.id.voice_img);
                    for (int i = 0; i < userList.size(); i++) {
                        if (i == position) {
                            Log.e(TAG, "isPlay == " + userList.get(position).getVoiceMessages().isPlay());
                            if (!userList.get(position).getVoiceMessages().isPlay()) {
                                userList.get(position).getVoiceMessages().setPlay(true);
                                Glide.with(context).load(R.drawable.playing_voice_left).asGif().into(mVoiceView);
                                MediaManager.playSound(playerUrl, new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        if (null != userList.get(position).getVoiceMessages()) {
                                            userList.get(position).getVoiceMessages().setPlay(false);
                                            Glide.with(context).load(R.drawable.play_voice_left).into(mVoiceView);
                                        }
                                    }
                                });
                            } else {
                                userList.get(position).getVoiceMessages().setPlay(false);
                                Glide.with(context).load(R.drawable.play_voice_left).into(mVoiceView);
                                MediaManager.stop();
                            }
                        } else {
                            if (null != userList.get(i).getVoiceMessages()) {
                                userList.get(i).getVoiceMessages().setPlay(false);
                            }
                        }
                    }
                }
            });
        }
    }


    /**
     * 加载右边声音的数据
     * @param holder
     * @param tbub
     * @param position
     */

    private void voiceRightLayout(final VoiceRightViewHolder holder,final MessageBean.DataBean tbub, final int position) {
        if (Util.isOnMainThread() && !context.isFinishing()) {

            Glide.with(context).load(tbub.getUser_avatar())
                    .transform(new GlideCircleTransform(context))
                    .into(holder.headicon);
            /* time */
            setTimePoint(holder.chatTime, tbub, position);
            holder.content.setVisibility(View.VISIBLE);
            final VoiceMessage voiceMessages = tbub.getVoiceMessages();
            final String playerUrl = voiceMessages.getPlayerUrl();
            String voiceTime = voiceMessages.getVoiceTime();
            holder.content.setText(voiceTime+"\"");
            if (!voiceMessages.isPlay()){
                Glide.with(context).load(R.drawable.play_voice_right).into(holder.voiceImg);
            }else {
                Glide.with(context).load(R.drawable.playing_voice_right).asGif().into(mVoiceView);
            }
            if (tbub.getMessageStatus() == 0) {
                holder.sendFailImg.setVisibility(View.GONE);
            } else {
                Log.e(TAG,"sendFailImg");
                holder.sendFailImg.setVisibility(View.VISIBLE);
            }

            holder.voiceClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mVoiceView != null) {
                        Log.e(TAG, "isLeftOrRight ==" + isLeftOrRight);
                        if ("Right".equals(isLeftOrRight)) {
                            Glide.with(context).load(R.drawable.play_voice_right).into(mVoiceView);
                        } else {
                            isLeftOrRight = "Right";
                            Glide.with(context).load(R.drawable.play_voice_left).into(mVoiceView);
                        }
                        mVoiceView = null;
                    }else {
                        isLeftOrRight = "Right";
                    }
                    mVoiceView = v.findViewById(R.id.voice_img);
                    for (int i = 0; i < userList.size(); i++) {
                        if (i == position) {
                            Log.e(TAG, "isPlay == " + userList.get(position).getVoiceMessages().isPlay());
                            if (!userList.get(position).getVoiceMessages().isPlay()) {
                                userList.get(position).getVoiceMessages().setPlay(true);
                                Glide.with(context).load(R.drawable.playing_voice_right).asGif().into(mVoiceView);
                                MediaManager.playSound(playerUrl, new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        if (null != userList.get(position).getVoiceMessages()) {
                                            userList.get(position).getVoiceMessages().setPlay(false);
                                            Glide.with(context).load(R.drawable.play_voice_right).into(mVoiceView);
                                        }
                                    }
                                });
                            } else {
                                userList.get(position).getVoiceMessages().setPlay(false);
                                Glide.with(context).load(R.drawable.play_voice_right).into(mVoiceView);
                                MediaManager.stop();
                            }
                        } else {
                            if (null != userList.get(i).getVoiceMessages()) {
                                userList.get(i).getVoiceMessages().setPlay(false);
                            }
                        }
                    }
                }
            });
        }
    }



    /***
     * 设置时间点
     *
     * @param chatTime
     * @param tbub
     * @param position
     */
    private void setTimePoint(TextView chatTime, MessageBean.DataBean tbub, int position) {
        String timeSet = tbub.getTimeSet();
        if (TextUtils.isEmpty(timeSet)) {
            Calendar cal = Calendar.getInstance();
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);
            chatTime.setText(hour + ":" + minute);
        } else {
            chatTime.setText(timeSet);
        }
        if (position == 0) {                //如果是当前页面的第一条消息，直接显示
            chatTime.setVisibility(View.VISIBLE);
        } else {
            String dateTime1 = userList.get(position - 1).getDateTime();    //上一条数据的时间
            String dateTime2 = tbub.getDateTime();                         //自己当前的时间
            if (!TextUtils.isEmpty(dateTime1) && !TextUtils.isEmpty(dateTime2)){
                String startTime = Utils.stampToJavaAndPhpDate(dateTime1);
                String endTime = Utils.stampToJavaAndPhpDate(dateTime2);

                long[] timeSub = Utils.getTimeSub(startTime, endTime);
                long days = timeSub[0];
                long hours = timeSub[1];
                long minutes = timeSub[2];
                long second = timeSub[3];

                if (days == 0 && hours == 0 && minutes < 5) {
                    chatTime.setVisibility(View.GONE);
                } else {
                    chatTime.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    public static Bitmap getLoacalBitmap(String url) {
        try {
            ByteArrayOutputStream out;
            FileInputStream fis = new FileInputStream(url);
            BufferedInputStream bis = new BufferedInputStream(fis);
            out = new ByteArrayOutputStream();
            @SuppressWarnings("unused")
            int hasRead = 0;
            byte[] buffer = new byte[1024 * 2];
            while ((hasRead = bis.read(buffer)) > 0) {
                // 读出多少数据，向输出流中写入多少
                out.write(buffer);
                out.flush();
            }
            out.close();
            fis.close();
            bis.close();
            byte[] data = out.toByteArray();
            // 长宽减半
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inSampleSize = 3;
            return BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取最后一条数据
     *
     * @return
     */
    public String getLastData() {
        if (userList != null && userList.size() > 0) {
            Log.e(TAG, "userList.size() === " + userList.size());
            Log.e(TAG, "userList === " + userList.get(userList.size() - 1).toString());
            MessageBean.DataBean dataBean = userList.get(userList.size() - 1);
            String content = dataBean.getContent();
            Log.e(TAG, "content === " + content);
            return content;
        }
        return "";
    }

    /**
     * 获取最后一条数据时间
     *
     * @return
     */
    public String getLastTime() {
        if (userList != null && userList.size() > 0) {
            Log.e(TAG, "userList.size() === " + userList.size());
            Log.e(TAG, "userList === " + userList.get(userList.size() - 1).toString());
            MessageBean.DataBean dataBean = userList.get(userList.size() - 1);
            String timeSet = dataBean.getTimeSet();
            Log.e(TAG, "timeSet === " + timeSet);
            return timeSet;
        }
        return "";
    }

    /**
     * 显示弹窗
     * @param panent 父view
     */
    public void showJumpPop(final View panent,String url){
        WindowManager.LayoutParams lp = context.getWindow().getAttributes();
        lp.alpha = 0.3f;
        context.getWindow().setAttributes(lp);
        ChatJumpPopwindow chatJumpPopwindow = new ChatJumpPopwindow(context,url);
        chatJumpPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                panent.startAnimation(AnimationUtils.loadAnimation(context, R.anim.root_out));
                WindowManager.LayoutParams lp = context.getWindow().getAttributes();
                lp.alpha = 1f;
                context.getWindow().setAttributes(lp);
            }
        });
        chatJumpPopwindow.showAtLocation(panent, Gravity.BOTTOM, 0, 0);
    }



    EventListener mEventListener;

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    public interface EventListener{
        void onVoiceCallBack(String filePath);
    }

}
