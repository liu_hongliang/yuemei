package com.module.commonview.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.HotPic;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import java.util.HashMap;
import java.util.List;

public class SkuDiaryListAdapter extends BaseQuickAdapter<BBsListData550,BaseViewHolder> {
    public SkuDiaryListAdapter(int layoutResId, @Nullable List<BBsListData550> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final BBsListData550 item) {
        Glide.with(mContext)
                .load(item.getUser_img())
                .transform(new GlideCircleTransform(mContext))
                .into((ImageView) helper.getView(R.id.sku_diary_head_img));
        helper.setText(R.id.sku_diary_head_name,item.getUser_name())
                .setText(R.id.sku_diary_title,item.getTitle())
                .setText(R.id.sku_diary_eye,item.getView_num())
                .setText(R.id.sku_diary_updata,item.getLast_update_time());
        String shareNum = item.getShareNum();
        if (!TextUtils.isEmpty(shareNum) || !"0".equals(shareNum)){
            helper.setText(R.id.sku_diary_head_time,item.getTime()+"   共更新"+shareNum+"篇");
        }else {
            helper.setText(R.id.sku_diary_head_time,item.getTime());
        }


        String answer_num = item.getAnswer_num();
        if (!TextUtils.isEmpty(answer_num) && !"0".equals(answer_num)){
            helper.setGone(R.id.sku_diary_talk_visorgone,true);
            helper.setText(R.id.sku_diary_talk,item.getAnswer_num());
        }else {
            helper.setGone(R.id.sku_diary_talk_visorgone,false);
        }
        List<HotPic> pic = item.getPic();
        if (null != pic && pic.size() == 2){
            helper.setGone(R.id.sku_diary_towimg_container,true);
            Glide.with(mContext).load(pic.get(0).getImg())
                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .into((ImageView) helper.getView(R.id.sku_diary_left_img));
            Glide.with(mContext).load(pic.get(1).getImg())
                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                    .placeholder(R.drawable.home_other_placeholder)
                    .error(R.drawable.home_other_placeholder)
                    .into((ImageView) helper.getView(R.id.sku_diary_right_img));
            helper.setText(R.id.sku_diary_right_time,"After" + item.getAfter_day() + "天");
        }else {
            helper.setGone(R.id.sku_diary_towimg_container,false);
        }

//        if (helper.getLayoutPosition() == getData().size() -1){
//            helper.setGone(R.id.sku_diary_btn,true);
//        }else {
//            helper.setGone(R.id.sku_diary_btn,false);
//        }

        final String is_follow_user = item.getIs_follow_user();
        switch (is_follow_user) {
            case "0":          //未关注
                setFocusView(helper,R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                break;
            case "1":          //已关注
                setFocusView(helper,R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                break;
            case "2":          //互相关注
                setFocusView(helper,R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                break;
        }
        LinearLayout headClick = helper.getView(R.id.sku_diary_head_click);
        headClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemButtonClick.onItemHeadClick(v,helper.getLayoutPosition());
            }
        });

//        LinearLayout fansClick = helper.getView(R.id.sku_diary_fans);
//        fansClick.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Utils.isLogin()) {
//                    if (Utils.isBind()) {
////                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_SHARELIST_USERFOLLOW,"home|share_" + mPartId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE,"0"),new ActivityTypeData("1"));
//                        switch (is_follow_user) {
//                            case "0":          //未关注
//                                FocusAndCancel(helper,item.getUser_id(), is_follow_user, helper.getLayoutPosition());
//                                break;
//                            case "1":          //已关注
//                            case "2":          //互相关注
//                                showDialogExitEdit(helper,item.getUser_id(), is_follow_user, helper.getLayoutPosition());
//                                break;
//                        }
//                    } else {
//                        Utils.jumpBindingPhone(mContext);
//                    }
//                } else {
//                    Utils.jumpLogin(mContext);
//                }
//                mItemButtonClick.onItemFansClick(v,helper.getLayoutPosition());
//            }
//        });
        Button bottomBtn=helper.getView(R.id.sku_diary_btn);
        bottomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemButtonClick.onItemButtonClik(v,helper.getLayoutPosition());
            }
        });

    }

    private void showDialogExitEdit(final BaseViewHolder helper,final String id, final String mFolowing, final int position) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(helper,id, mFolowing, position);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 关注和取消关注
     */
    private void FocusAndCancel(final BaseViewHolder helper,String id, final String mFolowing, final int position) {
        Log.e(TAG, "id === " + id);
        Log.e(TAG, "mFolowing === " + mFolowing);
        Log.e(TAG, "position === " + position);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                getData().get(position).setIs_follow_user("0");
                                setFocusView(helper,R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                                break;
                            case "1":          //已关注
                                getData().get(position).setIs_follow_user("1");
                                setFocusView(helper,R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                                break;
                            case "2":          //互相关注
                                getData().get(position).setIs_follow_user("2");
                                setFocusView(helper,R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                                break;
                        }

                        notifyDataSetChanged();
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    /**
     * 修改关注样式
     *
     * @param drawable1
     * @param drawable
     * @param color
     * @param text
     */
    private void setFocusView(final BaseViewHolder helper,int drawable1, int drawable, String color, String text) {
        LinearLayout fansLiin=helper.getView(R.id.sku_diary_fans);
        fansLiin.setBackground(ContextCompat.getDrawable(mContext, drawable));
        ImageView imageViewFans=helper.getView(R.id.sku_diary_imgage_fans);
        imageViewFans.setBackground(ContextCompat.getDrawable(mContext, drawable1));
        TextView textViewFans=helper.getView(R.id.sku_diary_center_fans);
        textViewFans.setTextColor(Color.parseColor(color));
        textViewFans.setText(text);
    }


    public void setmItemEventClick(ItemEventClick mItemButtonClick) {
        this.mItemButtonClick = mItemButtonClick;
    }

    ItemEventClick mItemButtonClick;

    public interface ItemEventClick{
        void onItemHeadClick(View v,int index);
        void onItemFansClick(View v,int index);
        void onItemButtonClik(View v,int index);
    }
}
