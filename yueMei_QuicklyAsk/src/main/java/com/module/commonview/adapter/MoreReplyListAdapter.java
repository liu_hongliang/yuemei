package com.module.commonview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.view.Expression;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/5/28.
 */
public class MoreReplyListAdapter extends RecyclerView.Adapter<MoreReplyListAdapter.ViewHolder> {

    private String TAG = "MoreReplyListAdapter";
    private Context mContext;
    private List<DiaryReplyListList> mDatas;
    private LayoutInflater mInflater;

    public MoreReplyListAdapter(Context context, List<DiaryReplyListList> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_diary_list_recycview_comments_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mDatas != null && mDatas.size() > 0) {
            DiaryReplyListList data = mDatas.get(position);
            Log.e(TAG, "data.getUserdata().getName() == " + data.getUserdata().getName());
            Log.e(TAG, "data.getContent() == " + data.getContent());
            SpannableStringBuilder textSpanned = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));
            Log.e(TAG, textSpanned.toString());
            holder.mTitle.setText(textSpanned.toString());
            Glide.with(mContext).load(data.getUserdata().getAvatar())
                    .transform(new GlideCircleTransform(mContext))
                    .placeholder(R.drawable.ten_ball_placeholder)
                    .error(R.drawable.ten_ball_placeholder)
                    .into(holder.item_diary_list_recycview_comments_list_img);
            holder.item_diary_list_recycview_comments_list_name.setText(data.getUserdata().getName());
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitle;
        ImageView item_diary_list_recycview_comments_list_img;
        TextView item_diary_list_recycview_comments_list_name;


        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_title);
            item_diary_list_recycview_comments_list_name = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_name);
            item_diary_list_recycview_comments_list_img = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_img);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (isOneselfDiary(getLayoutPosition())) {                //自己的长按删除
                        DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                        diariesDeleteData.setId(mDatas.get(getLayoutPosition()).getId());
                        diariesDeleteData.setReplystr("1");
                        diariesDeleteData.setPosPos(getLayoutPosition());
                        itemDeleteCallBackListener.onItemDeleteClick(diariesDeleteData);
                    } else {                                                  //他人的长按举报
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setIs_reply("1");
                        data.setPosPos(getLayoutPosition());
                        data.setFlag("2");
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        itemReportCallBackListener.onItemReportClick(data);
                    }
                    return false;
                }
            });
        }
    }

    /**
     * 判断是否是自己的日记
     *
     * @param position
     * @return
     */
    private boolean isOneselfDiary(int position) {
        return Utils.getUid().equals(mDatas.get(position).getUserdata().getId());
    }

    /**
     * 在第一条添加一条数据
     *
     * @param diaryReplyListList
     */
    public void addItem(DiaryReplyListList diaryReplyListList) {
        mDatas.add(0, diaryReplyListList);
        notifyItemInserted(0);
    }

    /**
     * 删除某一条数据
     *
     * @param pos
     */
    public void deleteItem(int pos) {
        mDatas.remove(pos);             //删除数据源
        notifyItemRemoved(pos);         //刷新被删除的地方
        notifyItemRangeChanged(pos, getItemCount()); //刷新被删除数据，以及其后面的数据
    }

    public List<DiaryReplyListList> getmDatas() {
        return mDatas;
    }

    //删除长按回调
    private ItemDeleteCallBackListener itemDeleteCallBackListener;

    public interface ItemDeleteCallBackListener {
        void onItemDeleteClick(DiariesDeleteData diariesDeleteData);
    }

    public void setItemDeleteCallBackListener(ItemDeleteCallBackListener itemDeleteCallBackListener) {
        this.itemDeleteCallBackListener = itemDeleteCallBackListener;
    }

    //举报回调
    private ItemReportCallBackListener itemReportCallBackListener;

    public interface ItemReportCallBackListener {
        void onItemReportClick(DiariesReportLikeData data);
    }

    public void setItemReportCallBackListener(ItemReportCallBackListener itemReportCallBackListener) {
        this.itemReportCallBackListener = itemReportCallBackListener;
    }
}