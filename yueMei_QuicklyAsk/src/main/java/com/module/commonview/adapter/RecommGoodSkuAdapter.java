package com.module.commonview.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.commonview.view.CenterAlignImageSpan;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.xinlan.imageeditlibrary.editimage.view.imagezoom.easing.Linear;

import java.util.HashMap;
import java.util.List;

/**
 * 日记本页面SKU列表
 * Created by 裴成浩 on 2018/11/12.
 */
public class RecommGoodSkuAdapter extends RecyclerView.Adapter<RecommGoodSkuAdapter.ViewHolder> {

    private final String TAG = "RecommGoodSkuAdapter";
    private Context mContext;
    private List<DiaryTaoData> mTaoDatas;
    private final LayoutInflater mInflater;
    private FunctionManager mFunctionManager;
    private String mSelfPageType;

    public RecommGoodSkuAdapter(Context context, List<DiaryTaoData> mTaoDatas,String selfPageType) {
        this.mContext = context;
        this.mTaoDatas = mTaoDatas;
        mInflater = LayoutInflater.from(context);
        this.mSelfPageType = selfPageType;
        mFunctionManager = new FunctionManager(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.item_detail_sku, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

//        if (i != 0) {
//            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.itemGroup.getLayoutParams();
//            layoutParams.topMargin = Utils.dip2px(8);
//            viewHolder.itemGroup.setLayoutParams(layoutParams);
//        }
//
//        final DiaryTaoData mTaoDatas.get(i) = mTaoDatas.get(i);
//
//        Glide.with(mContext).load(mTaoDatas.get(i).getList_cover_image()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.skuImg);
//
//        //大促
//        if ("1".equals(mTaoDatas.get(i).getDacu66_id())) {
//            SpannableString spanString = new SpannableString("大促" + mTaoDatas.get(i).getTitle());
//
//            CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.cu_tips_2x), Utils.dip2px(15), Utils.dip2px(15)));
//
//            spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//
//            viewHolder.viewContent.setText(spanString);                   //文案
//
//            if (!TextUtils.isEmpty(mTaoDatas.get(i).getDacu66_img())) {
//                viewHolder.dacuContainer.setVisibility(View.VISIBLE);
//                Glide.with(mContext).load(mTaoDatas.get(i).getDacu66_img()).bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(6), GlidePartRoundTransform.CornerType.TOP)).into(viewHolder.dacuImg);
//                viewHolder.dacuContainer.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (!TextUtils.isEmpty(mTaoDatas.get(i).getDacu66_url())) {
//                            WebUrlTypeUtil.getInstance(mContext).urlToApp(mTaoDatas.get(i).getDacu66_url(), "0", "0");
//                        }
//                    }
//                });
//            } else {
//                viewHolder.dacuContainer.setVisibility(View.GONE);
//            }
//
//        } else {
//            viewHolder.viewContent.setText(mTaoDatas.get(i).getTitle());
//            viewHolder.dacuContainer.setVisibility(View.GONE);
//        }
//
//        //价格
//        viewHolder.goodsPrice.setText(mTaoDatas.get(i).getPrice_discount());
//        final String member_price = mTaoDatas.get(i).getMember_price();
//        int memberPrice = Integer.parseInt(member_price);
//        if (memberPrice >= 0) {
//            viewHolder.plusPriceVibiable.setVisibility(View.VISIBLE);
//            viewHolder.plusPrice.setText("¥" + member_price);
//        } else {
//            viewHolder.plusPriceVibiable.setVisibility(View.GONE);
//        }
//
//        //单位
//        viewHolder.goodsUnit.setText(mTaoDatas.get(i).getFee_scale());
//        viewHolder.instructions.setText(mTaoDatas.get(i).getFanxian());
//        final String is_rongyun = mTaoDatas.get(i).getIs_rongyun();
//        final String hos_userid = mTaoDatas.get(i).getHos_userid();
//        final String taoid = mTaoDatas.get(i).getId();
//        final String title = mTaoDatas.get(i).getTitle();
//        final String price_discount = mTaoDatas.get(i).getPrice_discount();
//        final String list_cover_image = mTaoDatas.get(i).getList_cover_image();
//        if ("3".equals(is_rongyun)) {
//            viewHolder.zixun.setVisibility(View.VISIBLE);
//        } else {
//            viewHolder.zixun.setVisibility(View.GONE);
//        }
//        viewHolder.zixun.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Utils.isLogin()) {
//                    if (Utils.isBind()) {
//                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
//                                .setDirectId(hos_userid)
//                                .setObjId(taoid)
//                                .setObjType("2")
//                                .setTitle(title)
//                                .setPrice(price_discount)
//                                .setImg(list_cover_image)
//                                .setMemberPrice(member_price)
//                                .build();
//                        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
//                    } else {
//                        Utils.jumpBindingPhone(mContext);
//                    }
//
//                } else {
//                    Utils.jumpLogin(mContext);
//                }
//            }
//        });


        //SKU图片优化
        mFunctionManager.setRoundImageSrc(viewHolder.skuImg, mTaoDatas.get(i).getList_cover_image(), Utils.dip2px(5));

        //SKU文案
        if ("1".equals(mTaoDatas.get(i).getDacu66_id())) {
            SpannableString spanString = new SpannableString("大促" + mTaoDatas.get(i).getTitle());
            CenterAlignImageSpan imageSpan = new CenterAlignImageSpan(mContext, Utils.zoomImage(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.cu_tips_2x), Utils.dip2px(15), Utils.dip2px(15)));
            spanString.setSpan(imageSpan, 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            viewHolder.viewContent.setText(spanString);                   //文案
        } else {
            viewHolder.viewContent.setText(mTaoDatas.get(i).getTitle());
        }

        //咨询按钮显示与隐藏
        final String is_rongyun = mTaoDatas.get(i).getIs_rongyun();
        if ("3".equals(is_rongyun)) {
            viewHolder.btn.setVisibility(View.VISIBLE);
        } else {
            viewHolder.btn.setVisibility(View.GONE);
        }

        //底部说明
        if(TextUtils.isEmpty(mTaoDatas.get(i).getFanxian())){
            viewHolder.ll_posts.setVisibility(View.GONE);
        }else{
            viewHolder.ll_posts.setVisibility(View.VISIBLE);
            viewHolder.post_title.setText(mTaoDatas.get(i).getFanxian());
        }

        //咨询按钮点击
        viewHolder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.noLoginChat()) {
                    toChatActivity(i);
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        toChatActivity(i);
                    }
                }
            }
        });

        String member_price = mTaoDatas.get(i).getMember_price();
        String oPrice = mTaoDatas.get(i).getPrice();
        String priceDiscount = mTaoDatas.get(i).getPrice_discount();

        int memberPrice = Integer.parseInt(member_price);
        int o_price = Integer.parseInt(oPrice);
        int price_discount = Integer.parseInt(priceDiscount);
        viewHolder.price.setText(priceDiscount);
        if (memberPrice >= 0) {
            viewHolder.plusPriceVibiable.setVisibility(View.VISIBLE);
            viewHolder.plusPrice.setText("¥" + member_price);
        } else {
            viewHolder.plusPriceVibiable.setVisibility(View.GONE);
            if (o_price >= 0 && o_price != price_discount) {
                viewHolder.originalPrice.setVisibility(View.VISIBLE);
                viewHolder.originalPrice.setText("¥" + o_price);
                viewHolder.originalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
            } else {
                viewHolder.originalPrice.setVisibility(View.GONE);
            }
        }
        
    }

    private void toChatActivity(int i) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id",mTaoDatas.get(i).getId());
        hashMap.put("hos_id",mTaoDatas.get(i).getHospital_id());
        hashMap.put("doc_id",mTaoDatas.get(i).getHos_userid());
        hashMap.put("tao_id",mTaoDatas.get(i).getId());
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL,"text_related_chat",mTaoDatas.get(i).getHospital_id()),hashMap,new ActivityTypeData(mSelfPageType));
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(mTaoDatas.get(i).getHos_userid())
                .setObjId(mTaoDatas.get(i).getId())
                .setObjType("2")
                .setTitle(mTaoDatas.get(i).getTitle())
                .setPrice(mTaoDatas.get(i).getPrice_discount())
                .setImg(mTaoDatas.get(i).getList_cover_image())
                .setMemberPrice(mTaoDatas.get(i).getMember_price())
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
    }

    @Override
    public int getItemCount() {
        return mTaoDatas.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView skuImg;
        TextView viewContent;
        TextView price;
        TextView originalPrice;
        RelativeLayout plusPriceVibiable;
        TextView plusPrice;
        final TextView btn;
        LinearLayout ll_posts;
        TextView post_title;
        

        public ViewHolder(View itemView) {
            super(itemView);
            //图片
            skuImg = itemView.findViewById(R.id.diary_list_goods_sku_img);
             //标题
            viewContent = itemView.findViewById(R.id.diary_list_goods_title);
             //价格布局
            price = itemView.findViewById(R.id.diary_list_goods_price);
             //价格
            originalPrice = itemView.findViewById(R.id.diary_list_goods_original_price);
//             //
//             diary_list_goods_original_price = itemView.findViewById(R.id.diary_list_goods_original_price);
             //plus布局
            plusPriceVibiable = itemView.findViewById(R.id.diary_list_plus_vibiable);
             //plus价格
            plusPrice = itemView.findViewById(R.id.plus_vip_price);
             //咨询按钮
            btn = itemView.findViewById(R.id.diary_list_goods_btn);
             //底部提示布局
            ll_posts = itemView.findViewById(R.id.post_list_goods_doc_click);
             //提示
             post_title = itemView.findViewById(R.id.post_title);
             
            //点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemnClickListener != null) {
                        itemnClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    public List<DiaryTaoData> getTaoDatas() {
        return mTaoDatas;
    }

    //点击事件
    private ItemClickListener itemnClickListener;

    public interface ItemClickListener {
        void onItemClick(View v, int pos);
    }

    public void setItemnClickListener(ItemClickListener itemnClickListener) {
        this.itemnClickListener = itemnClickListener;
    }
}
