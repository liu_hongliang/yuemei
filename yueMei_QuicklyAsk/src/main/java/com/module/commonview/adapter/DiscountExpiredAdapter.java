package com.module.commonview.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.my.model.bean.DiscountExpiredInfo;
import com.quicklyask.activity.R;

/**
 * 文 件 名: DiscountExpiredAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-17 16:51
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class DiscountExpiredAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private SparseArray<CountDownTimer> countDownCounters;
    private DiscountExpiredInfo mDiscountExpiredInfo;

    public DiscountExpiredAdapter(Context context, DiscountExpiredInfo discountExpiredInfo) {
        this.mContext = context;
        this.mDiscountExpiredInfo = discountExpiredInfo;
        inflater = LayoutInflater.from(mContext);
        this.countDownCounters = new SparseArray<>();

    }

    @Override
    public int getCount() {
        return mDiscountExpiredInfo.getCoupons() == null ? 0 : mDiscountExpiredInfo.getCoupons().size();
    }

    @Override
    public Object getItem(int position) {
        return mDiscountExpiredInfo.getCoupons().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_discount_expired, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_price = convertView.findViewById(R.id.tv_price);
            viewHolder.tv_full_reduction = convertView.findViewById(R.id.tv_full_reduction);
            viewHolder.tv_hours = convertView.findViewById(R.id.tv_hours);
            viewHolder.tv_minute = convertView.findViewById(R.id.tv_minute);
            viewHolder.tv_second = convertView.findViewById(R.id.tv_second);
            viewHolder.ll_countDown = convertView.findViewById(R.id.ll_countDown);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CountDownTimer countDownTimer = countDownCounters.get(viewHolder.ll_countDown.hashCode());
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        long timer = Long.parseLong(mDiscountExpiredInfo.getCoupons().get(position).getEnd_time()) * 1000 - System.currentTimeMillis();
        if (timer > 0) {
            countDownTimer = new CountDownTimer(timer, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    viewHolder.tv_hours.setText(getSecond(getCountTimeByLong(millisUntilFinished), "1"));
                    viewHolder.tv_minute.setText(getSecond(getCountTimeByLong(millisUntilFinished), "2"));
                    viewHolder.tv_second.setText(getSecond(getCountTimeByLong(millisUntilFinished), "3"));
                }

                @Override
                public void onFinish() {
                    viewHolder.tv_hours.setText("00");
                    viewHolder.tv_minute.setText("00");
                    viewHolder.tv_second.setText("00");

                }
            }.start();
            countDownCounters.put(viewHolder.ll_countDown.hashCode(), countDownTimer);
        } else {
            viewHolder.tv_hours.setText("00");
            viewHolder.tv_minute.setText("00");
            viewHolder.tv_second.setText("00");
        }
        viewHolder.tv_price.setText(mDiscountExpiredInfo.getCoupons().get(position).getMoney());
        viewHolder.tv_full_reduction.setText(mDiscountExpiredInfo.getCoupons().get(position).getDesc());
        return convertView;
    }


    public class ViewHolder {
        TextView tv_price;
        TextView tv_full_reduction;
        TextView tv_hours;
        TextView tv_minute;
        TextView tv_second;
        LinearLayout ll_countDown;
    }

    /**
     * 清空当前 CountTimeDown 资源
     */
    public void cancelAllTimers() {
        if (countDownCounters == null) {
            return;
        }
        for (int i = 0, length = countDownCounters.size(); i < length; i++) {
            CountDownTimer cdt = countDownCounters.get(countDownCounters.keyAt(i));
            if (cdt != null) {
                cdt.cancel();
            }
        }
    }

    //毫秒换成00:00:00
    public static String getCountTimeByLong(long finishTime) {
        int totalTime = (int) (finishTime / 1000);//秒
        int hour = 0, minute = 0, second = 0;

        if (3600 <= totalTime) {
            hour = totalTime / 3600;
            totalTime = totalTime - 3600 * hour;
        }
        if (60 <= totalTime) {
            minute = totalTime / 60;
            totalTime = totalTime - 60 * minute;
        }
        if (0 <= totalTime) {
            second = totalTime;
        }
        StringBuilder sb = new StringBuilder();

        if (hour < 10) {
            sb.append("0").append(hour).append(":");
        } else {
            sb.append(hour).append(":");
        }
        if (minute < 10) {
            sb.append("0").append(minute).append(":");
        } else {
            sb.append(minute).append(":");
        }
        if (second < 10) {
            sb.append("0").append(second);
        } else {
            sb.append(second);
        }
        return sb.toString();

    }

    /**
     * 时分秒格式00:00:00转换秒数
     *
     * @param time //时分秒格式00:00:00
     * @return 秒数
     * @type type 0天 1时 2分 3秒
     */
    public static String getSecond(String time, String type) {
        String s1 = "00";
        String s2 = "00";
        String s3 = "00";
        String s4 = "00";
        if (time.length() == 8) { //时分秒格式00:00:00
            int index1 = time.indexOf(":");
            int index2 = time.indexOf(":", index1 + 1);
            s1 = time.substring(0, index1);//小时
            s1 =  String.format("%02d",Integer.parseInt(time.substring(0, index1)) - (Integer.parseInt(s1) / 24) * 24);
            s2 = time.substring(index1 + 1, index2);//分钟
            s3 = time.substring(index2 + 1);//秒
        }
        if (time.length() == 5) {//分秒格式00:00
            s3 = time.substring(time.length() - 2); //秒  后两位肯定是秒
            s2 = time.substring(0, 2);    //分钟
            s1 = "00";
        }
        if (s1.equals("00")) {
            s4 = "00";
        } else {
            s4 = String.format("%02d", Integer.parseInt(time.substring(0, time.indexOf(":"))) / 24);
        }
        if (type.equals("1")) {
            return s1;
        } else if (type.equals("2")) {
            return s2;
        } else if (type.equals("0")) {
            return s4;
        } else {
            return s3;
        }
    }

}
