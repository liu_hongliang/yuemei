package com.module.commonview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.commonview.module.bean.ButtomDialogBean;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/5/16.
 */
public class ButtomDialogAdapter extends BaseAdapter {

    private final Context context;
    private final List<ButtomDialogBean> buttomDialogBeans;
    private final LayoutInflater inflater;
    private ViewHolder viewHolder;

    public ButtomDialogAdapter(Context context, List<ButtomDialogBean> buttomDialogBeans) {
        this.context = context;
        this.buttomDialogBeans = buttomDialogBeans;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return buttomDialogBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return buttomDialogBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_buttom_dialog, null);
            viewHolder = new ViewHolder();

            viewHolder.buttonDialogIV = convertView.findViewById(R.id.iv_button_dialog);
            viewHolder.buttonDialogTV = convertView.findViewById(R.id.tv_button_dialog);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ButtomDialogBean buttomDialogBean = buttomDialogBeans.get(position);

        viewHolder.buttonDialogIV.setBackgroundResource(buttomDialogBean.getImage());
        viewHolder.buttonDialogTV.setText(buttomDialogBean.getTitle());

        return convertView;
    }

    static class ViewHolder {
        public ImageView buttonDialogIV;
        public TextView buttonDialogTV;
    }
}
