package com.module.commonview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.commonview.module.bean.DiaryReplyListList;
import com.module.commonview.view.Expression;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

//回复超过1条折叠  第一次展开5条 第二次展开全部
public class MoreReplyListAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "MoreReplyListAdapter2";
    private Context mContext;
    private List<DiaryReplyListList> mDatas;
    private LayoutInflater mInflater;
    private final static int TYPE_NORMAL = 0;//正常条目
    private final static int TYPE_SEE_MORE = 1;//展开全部xx条回复
    private final static int TYPE_SEE_MODE2 = 2; //展开全部
    private final static int TYPE_HIDE = 3;//已经到底了
    private boolean mOpen1 = false;//是否是展开状态 超过两个
    private boolean mOpen2 = false;//是否是展开状态 超过六个

    public MoreReplyListAdapter2(Context context, List<DiaryReplyListList> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_NORMAL:
                return new ViewHolder1(mInflater.inflate(R.layout.item_diary_list_recycview_comments_list2, parent, false));
            case TYPE_SEE_MORE:
                return new ViewHolder2(mInflater.inflate(R.layout.item_comments_more, parent, false));
            case TYPE_HIDE:
                return new ViewHolder3(mInflater.inflate(R.layout.item_comments_more, parent, false));
            case TYPE_SEE_MODE2:
                return new ViewHolder4(mInflater.inflate(R.layout.item_comments_more, parent, false));
            default:
                return new ViewHolder1(mInflater.inflate(R.layout.item_diary_list_recycview_comments_list2, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder1) {
            setView1((ViewHolder1) viewHolder, i);
        } else if (viewHolder instanceof ViewHolder2) {
            setView2((ViewHolder2) viewHolder, i);
        } else if (viewHolder instanceof ViewHolder3) {
            setView3((ViewHolder3) viewHolder, i);
        } else {
            setView4((ViewHolder4) viewHolder, i);
        }
    }


    private void setView1(ViewHolder1 holder, int position) {
        if (mDatas != null && mDatas.size() > 0) {
            final DiaryReplyListList data = mDatas.get(position);
            SpannableStringBuilder textSpanned = Expression.handlerEmojiText1(data.getContent(), mContext, Utils.dip2px(12));
            holder.mTitle.setText(textSpanned);
            Glide.with(mContext).load(data.getUserdata().getAvatar())
                    .transform(new GlideCircleTransform(mContext))
                    .placeholder(R.drawable.ten_ball_placeholder)
                    .error(R.drawable.ten_ball_placeholder)
                    .into(holder.item_diary_list_recycview_comments_list_img);
            holder.item_diary_list_recycview_comments_list_name.setText(data.getUserdata().getName());

            holder.item_diary_list_recycview_comments_list_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(data.getUserdata().getUrl())) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUserdata().getUrl(), "0", "0");
                    }
                }
            });

            holder.item_diary_list_recycview_comments_list_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(data.getUserdata().getUrl())) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUserdata().getUrl(), "0", "0");
                    }
                }
            });
        }
    }

    private void setView2(ViewHolder2 viewHolder, int i) {
        viewHolder.iv_down.setVisibility(View.VISIBLE);
        viewHolder.tv_more.setText("展开全部" + mDatas.size() + "条回复");
        viewHolder.tv_more.setTextColor(Color.parseColor("#999999"));
        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpen1 = true;
                notifyDataSetChanged();
            }
        });
    }

    private void setView3(ViewHolder3 viewHolder, int i) {
        viewHolder.iv_down.setVisibility(View.GONE);
        viewHolder.tv_more.setText("已经到底了");
        viewHolder.tv_more.setTextColor(Color.parseColor("#CCCCCC"));
    }

    private void setView4(ViewHolder4 viewHolder, int i) {
        viewHolder.iv_down.setVisibility(View.VISIBLE);
        viewHolder.tv_more.setText("展开全部");
        viewHolder.tv_more.setTextColor(Color.parseColor("#999999"));
        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpen2 = true;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas.size() <= 1) {
            return TYPE_NORMAL;
        } else if (mDatas.size() > 1 && mDatas.size() <= 6) {
            if (mOpen1) {
                if (position == mDatas.size()) {
                    return TYPE_HIDE;
                } else {
                    return TYPE_NORMAL;
                }
            } else {
                if (position == 1) {
                    return TYPE_SEE_MORE;
                } else {
                    return TYPE_NORMAL;
                }
            }
        } else {
            if (mOpen1) {
                if (mOpen2) {
                    if (position == mDatas.size()) {
                        return TYPE_HIDE;
                    } else {
                        return TYPE_NORMAL;
                    }
                } else {
                    if (position == 6) {
                        return TYPE_SEE_MODE2;
                    } else {
                        return TYPE_NORMAL;
                    }
                }
            } else {
                if (position == 1) {
                    return TYPE_SEE_MORE;
                } else {
                    return TYPE_NORMAL;
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mDatas == null || mDatas.size() == 0) {
            return 0;
        }
        if (mDatas.size() > 1) {
            if (mDatas.size() <= 6) {
                if (mOpen1) {
                    return mDatas.size() + 1;
                } else {
                    return 2;
                }
            } else {
                if (mOpen1) {
                    if (mOpen2) {
                        return mDatas.size() + 1;
                    } else {
                        return 7;
                    }
                } else {
                    return 2;
                }
            }
        } else {
            return mDatas.size();
        }
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {

        TextView mTitle;
        ImageView item_diary_list_recycview_comments_list_img;
        TextView item_diary_list_recycview_comments_list_name;


        public ViewHolder1(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_title);
            item_diary_list_recycview_comments_list_name = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_name);
            item_diary_list_recycview_comments_list_img = itemView.findViewById(R.id.item_diary_list_recycview_comments_list_img);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (isOneselfDiary(getLayoutPosition())) {                //自己的长按删除
                        DiariesDeleteData diariesDeleteData = new DiariesDeleteData();
                        diariesDeleteData.setId(mDatas.get(getLayoutPosition()).getId());
                        diariesDeleteData.setReplystr("1");
                        diariesDeleteData.setPosPos(getLayoutPosition());
                        itemDeleteCallBackListener.onItemDeleteClick(diariesDeleteData);
                    } else {                                                  //他人的长按举报
                        DiariesReportLikeData data = new DiariesReportLikeData();
                        data.setIs_reply("1");
                        data.setPosPos(getLayoutPosition());
                        data.setFlag("2");
                        data.setId(mDatas.get(getLayoutPosition()).getId());
                        itemReportCallBackListener.onItemReportClick(data);
                    }
                    return false;
                }
            });
        }
    }

    public class ViewHolder2 extends RecyclerView.ViewHolder {

        TextView tv_more;
        ImageView iv_down;

        public ViewHolder2(View itemView) {
            super(itemView);
            tv_more = itemView.findViewById(R.id.tv_more);
            iv_down = itemView.findViewById(R.id.iv_down);
        }
    }

    public class ViewHolder3 extends RecyclerView.ViewHolder {

        TextView tv_more;
        ImageView iv_down;

        public ViewHolder3(View itemView) {
            super(itemView);
            tv_more = itemView.findViewById(R.id.tv_more);
            iv_down = itemView.findViewById(R.id.iv_down);
        }
    }

    public class ViewHolder4 extends RecyclerView.ViewHolder {

        TextView tv_more;
        ImageView iv_down;

        public ViewHolder4(View itemView) {
            super(itemView);
            tv_more = itemView.findViewById(R.id.tv_more);
            iv_down = itemView.findViewById(R.id.iv_down);
        }
    }

    /**
     * 判断是否是自己的日记
     *
     * @param position
     * @return
     */
    private boolean isOneselfDiary(int position) {
        return Utils.getUid().equals(mDatas.get(position).getUserdata().getId());
    }

    /**
     * 在第一条添加一条数据
     *
     * @param diaryReplyListList
     */
    public void addItem(DiaryReplyListList diaryReplyListList) {
        mDatas.add(0, diaryReplyListList);
        notifyItemInserted(0);
    }

    /**
     * 删除某一条数据
     *
     * @param pos
     */
    public void deleteItem(int pos) {
        mDatas.remove(pos);             //删除数据源
        notifyItemRemoved(pos);         //刷新被删除的地方
        notifyItemRangeChanged(pos, getItemCount()); //刷新被删除数据，以及其后面的数据
    }

    public List<DiaryReplyListList> getmDatas() {
        return mDatas;
    }

    //删除长按回调
    private ItemDeleteCallBackListener itemDeleteCallBackListener;

    public interface ItemDeleteCallBackListener {
        void onItemDeleteClick(DiariesDeleteData diariesDeleteData);
    }

    public void setItemDeleteCallBackListener(ItemDeleteCallBackListener itemDeleteCallBackListener) {
        this.itemDeleteCallBackListener = itemDeleteCallBackListener;
    }

    //举报回调
    private ItemReportCallBackListener itemReportCallBackListener;

    public interface ItemReportCallBackListener {
        void onItemReportClick(DiariesReportLikeData data);
    }

    public void setItemReportCallBackListener(ItemReportCallBackListener itemReportCallBackListener) {
        this.itemReportCallBackListener = itemReportCallBackListener;
    }
}