package com.module.commonview.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.ComparedImg;
import com.module.commonview.module.bean.LocusData;
import com.module.commonview.module.bean.PicBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class ChatListItemkoubeiAdapter extends BaseQuickAdapter<LocusData, BaseViewHolder> {
    public ChatListItemkoubeiAdapter(int layoutResId, @Nullable List<LocusData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, LocusData item) {
        ComparedImg compared_img = item.getCompared_img();
        if (compared_img != null){
            List<PicBean> pic = compared_img.getPic();
            if (pic != null && pic.size() > 0){
                Glide.with(mContext)
                        .load(pic.get(0).getImg())
                        .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into((ImageView) helper.getView(R.id.bbs_list_duozhang_iv1));
                if (pic.size()==2){
                    Glide.with(mContext)
                            .load(pic.get(1).getImg())
                            .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                            .placeholder(R.drawable.home_other_placeholder)
                            .error(R.drawable.home_other_placeholder)
                            .into((ImageView) helper.getView(R.id.bbs_list_duozhang_iv2));
                }

                helper.setText(R.id.bbs_list_picrule2,"After"+compared_img.getAfter_day()+"天")
                        .setText(R.id.chat_compar_list_item_title,item.getTitle());

                if (helper.getLayoutPosition() == getData().size()-1){
                    helper.setGone(R.id.chat_compar_list_item_line,false);
                }else {
                    helper.setGone(R.id.chat_compar_list_item_line,true);
                }
            }
        }


    }
}
