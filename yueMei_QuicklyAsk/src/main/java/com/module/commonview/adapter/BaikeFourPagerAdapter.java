package com.module.commonview.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/4/29
 */
public class BaikeFourPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private List<YMBaseFragment> fragmentList;

    public BaikeFourPagerAdapter(Context mContext, FragmentManager fm, List<YMBaseFragment> mFragmentList) {
        super(fm);
        this.mContext = mContext;
        this.fragmentList = mFragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    private String titles[] = {"项目介绍", "相关日记", "常见问题", "相关商品", "相关医生", "相关医院", "科普问答"};
    private int[] imageViews = {R.drawable.baike_three_tab_choose1, R.drawable.baike_three_tab_choose2, R.drawable.baike_three_tab_choose3,
            R.drawable.baike_three_tab_choose4, R.drawable.baike_three_tab_choose5, R.drawable.baike_three_tab_choose6, R.drawable.baike_three_tab_choose7};

    public View getTabView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.baike_four_tab_item, null);
        ImageView mImageView = view.findViewById(R.id.baike_three_tab_image);
        TextView mTextView = view.findViewById(R.id.baike_three_tab_text);

        mTextView.setText(titles[position]);
        mImageView.setBackgroundResource(imageViews[position]);

        return view;
    }
}
