package com.module.commonview.adapter;

import android.app.Activity;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.my.model.bean.ProjcetList;
import com.module.my.model.bean.ProjcetListImage;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/23
 */
public class SortScreenItemAdapter extends RecyclerView.Adapter<SortScreenItemAdapter.ViewHolder> {

    private final Activity mContext;
    private List<ProjcetList> mData;
    private final LayoutInflater mLayoutInflater;
    private final int windowsWight;
    private boolean mIsRadio = false;
    private String mDefaultSelectId;

    public SortScreenItemAdapter(Activity context, List<ProjcetList> data) {
        this(context, data, false, "");
    }

    public SortScreenItemAdapter(Activity context, List<ProjcetList> data, boolean isRadio, String default_select_id) {
        this.mContext = context;
        this.mData = data;
        this.mIsRadio = isRadio;
        this.mDefaultSelectId = default_select_id;
        mLayoutInflater = LayoutInflater.from(mContext);
        if (!TextUtils.isEmpty(default_select_id)) {
            for (ProjcetList list : mData) {
                if (default_select_id.equals(list.getId())) {
                    list.setIs_selected(true);
                }
            }
        }
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.item_sort_screen_item_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProjcetList data = mData.get(position);
        ProjcetListImage image = data.getImage();
        String before = image.getBefore();
        String after = image.getAfter();
        if (!TextUtils.isEmpty(before) && !TextUtils.isEmpty(after)) {
            holder.mImageView.setVisibility(View.VISIBLE);
            holder.text.setVisibility(View.GONE);
            ViewGroup.LayoutParams layoutParams = holder.mImageView.getLayoutParams();
            layoutParams.width = (windowsWight - Utils.dip2px(52)) / 3;
            holder.mImageView.setLayoutParams(layoutParams);
            if (data.isIs_selected()){
                Glide.with(mContext).load(after).into(holder.mImageView);
            }else {
                Glide.with(mContext).load(before).into(holder.mImageView);
            }
        }else {
            holder.mImageView.setVisibility(View.GONE);
            holder.text.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams layoutParams = holder.text.getLayoutParams();
            layoutParams.width = (windowsWight - Utils.dip2px(52)) / 3;
            holder.text.setLayoutParams(layoutParams);
            holder.text.setText(data.getTitle());
            if (data.isIs_selected()) {
                holder.text.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff5c77));
                holder.text.setBackgroundResource(R.drawable.home_diary_tab);
            } else {
                holder.text.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                holder.text.setBackgroundResource(R.drawable.home_diary_tab2);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView text;
        private ImageView mImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.item_sort_screen_item_text);
            mImageView = itemView.findViewById(R.id.item_sort_screen_item_bg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIsRadio) {
                        int selectedPos = -1;
                        for (int i = 0; i < mData.size(); i++) {
                            ProjcetList data = mData.get(i);
                            if (data.isIs_selected()) {
                                data.setIs_selected(!data.isIs_selected());
                                selectedPos = i;
                                break;
                            }
                        }

                        if (!mData.get(getLayoutPosition()).isIs_selected()) {
                            mData.get(getLayoutPosition()).setIs_selected(!mData.get(getLayoutPosition()).isIs_selected());
                        }

                        if (selectedPos >= 0) {
                            notifyItemChanged(selectedPos);
                        }
                        notifyItemChanged(getLayoutPosition());
                    } else {
                        mData.get(getLayoutPosition()).setIs_selected(!mData.get(getLayoutPosition()).isIs_selected());
                        notifyItemChanged(getLayoutPosition());
                    }
                }
            });
        }
    }

    /**
     * 获取选中数据
     *
     * @return : 选中的数据集合
     */
    public ArrayList<ProjcetList> getSelectedData() {
        ArrayList<ProjcetList> selectedData = new ArrayList<>();
        for (ProjcetList data : mData) {
            if (data.isIs_selected()) {
                selectedData.add(data);
            }
        }
        return selectedData;
    }

    /**
     * 重置数据
     */
    public void resetData() {
        for (int i = 0; i < mData.size(); i++) {
            ProjcetList data = mData.get(i);
            if (mDefaultSelectId.equals(data.getId())) {
                data.setIs_selected(true);
            }else {
                if (data.isIs_selected()) {
                    data.setIs_selected(false);
                }
            }
            notifyItemChanged(i);
        }
    }
}
