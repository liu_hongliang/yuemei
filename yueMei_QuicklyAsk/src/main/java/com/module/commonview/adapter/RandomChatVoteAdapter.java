package com.module.commonview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.other.module.bean.SearchTaoDate;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 文 件 名: RandomChatVoteAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-27 11:04
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class RandomChatVoteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<SearchTaoDate.TaoListBean> mTaoList;
    private FunctionManager mFunctionManager;

    public RandomChatVoteAdapter(Context context, List<SearchTaoDate.TaoListBean> tao_list) {
        this.mContext = context;
        this.mTaoList = tao_list;
        inflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new viewHolder(inflater.inflate(R.layout.item_vote_random_chat, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof viewHolder) {
            setLayout(((viewHolder) holder), position);
        }
    }

    private void setLayout(viewHolder holder, final int position) {
        if (TextUtils.isEmpty(mTaoList.get(position).getImg())) {
            mFunctionManager.setRoundImageSrc(holder.iv_pic, R.drawable.sall_null_2x, Utils.dip2px(5));
        } else {
            mFunctionManager.setRoundImageSrc(holder.iv_pic, mTaoList.get(position).getImg(), Utils.dip2px(5));
        }
        if (TextUtils.isEmpty(mTaoList.get(position).getSubtitle())) {
            holder.tv_sku_title.setText("");
        } else {
            holder.tv_sku_title.setText(mTaoList.get(position).getSubtitle());
        }
        if (TextUtils.isEmpty(mTaoList.get(position).getHos_name())) {
            holder.tv_sku_hos.setText("");
        } else {
            holder.tv_sku_hos.setText(mTaoList.get(position).getHos_name());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTaoList.size();
    }

    public List<SearchTaoDate.TaoListBean> getDate(){
        return mTaoList;
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ImageView iv_pic;
        TextView tv_sku_title;
        TextView tv_sku_hos;

        public viewHolder(View itemView) {
            super(itemView);
            iv_pic = itemView.findViewById(R.id.iv_pic);
            tv_sku_title = itemView.findViewById(R.id.tv_sku_title);
            tv_sku_hos = itemView.findViewById(R.id.tv_sku_hos);
        }
    }


    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}

