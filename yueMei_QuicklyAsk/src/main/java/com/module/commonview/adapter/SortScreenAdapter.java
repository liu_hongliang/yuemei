package com.module.commonview.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.commonview.view.DoubleSlideSeekBar;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.my.controller.other.EditInputFilter;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 筛选活动
 * Created by 裴成浩 on 2019/5/17
 */
public class SortScreenAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG= "SortScreenAdapter";
    private Activity mContext;
    private List<ProjcetData> mDatas;

    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TWO = 2;
    private final LayoutInflater mLayoutInflater;
    private HashMap<Integer, SortScreenItemAdapter> mHashMap = new HashMap<>();
    private final int mScreenWidth;
    private SortScreenItemAdapter mSortScreenItemAdapter;
    private String editLowStr="0";//最低价
    private String editHighStr="0";//最高价

    public SortScreenAdapter(Activity content, ArrayList<ProjcetData> data) {
        this.mContext = content;
        this.mDatas = data;
        mLayoutInflater = LayoutInflater.from(mContext);
        mScreenWidth = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
    }

    @Override
    public int getItemViewType(int position) {
        switch (mDatas.get(position).getId()) {
            case 3:
            case 4:
                return ITEM_TYPE_TWO;
            default:
                return ITEM_TYPE_ONE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_TWO:
                return new ViewHolder2(mLayoutInflater.inflate(R.layout.item_sort_screen_view2, viewGroup, false));
            default:
                return new ViewHolder1(mLayoutInflater.inflate(R.layout.item_sort_screen_view1, viewGroup, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder1) {
            setView1((ViewHolder1) holder, position);
        } else if (holder instanceof ViewHolder2) {
            setView2((ViewHolder2) holder, position);
        }
    }

    //设置样式1
    private void setView1(ViewHolder1 holder, int position) {
        ProjcetData data = mDatas.get(position);
        //设置标题
        holder.mName1.setText(data.getName());

        //设置适配器
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
        gridLayoutManager.setScrollEnable(false);
        for (ProjcetList dataasdas : data.getList()) {
            Log.e("adasdasdasas", "dataasdas11111 == " + dataasdas.getTitle());
        }
        SortScreenItemAdapter sortScreenItemAdapter = new SortScreenItemAdapter(mContext, data.getList());
        holder.mlist1.setLayoutManager(gridLayoutManager);
        holder.mlist1.setAdapter(sortScreenItemAdapter);

        mHashMap.put(data.getId(), sortScreenItemAdapter);
    }


    //设置样式2
    @SuppressLint("ClickableViewAccessibility")
    private void setView2(final ViewHolder2 holder, int position) {
        ProjcetData data = mDatas.get(position);
        int id = data.getId();
        List<ProjcetList> projcetLists = data.getList();
        switch (id){
            case 3://价格区间
                holder.mEditLayout.setVisibility(View.VISIBLE);
                InputFilter[] filters = {new EditInputFilter()};
                holder.mEditText1.setFilters(filters);
                holder.mEditText2.setFilters(filters);
                ViewGroup.LayoutParams editParams1 = holder.mEditText1.getLayoutParams();
                int  editWidth = (mScreenWidth/2)- Utils.dip2px(22);
                editParams1.width = editWidth;
                holder.mEditText1.setLayoutParams(editParams1);
                ViewGroup.LayoutParams editParams2 = holder.mEditText2.getLayoutParams();
                editParams2.width = editWidth;
                holder.mEditText1.setLayoutParams(editParams2);
                holder.mEditText1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        editLowStr = s.toString();

                    }
                });
                holder.mEditText2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        editHighStr = s.toString();

                    }
                });
                holder.mEditText1.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (null != mSortScreenItemAdapter){
                            mSortScreenItemAdapter.resetData();
                        }
                        if (!TextUtils.isEmpty(holder.mEditText1.getText())){
                            holder.mEditText1.setText("自定义最低价");
                        }
                        return false;
                    }
                });
                holder.mEditText2.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (null != mSortScreenItemAdapter){
                            mSortScreenItemAdapter.resetData();
                        }
                        if (!TextUtils.isEmpty(holder.mEditText2.getText())){
                            holder.mEditText2.setText("自定义最高价");
                        }
                        return false;
                    }
                });
                break;
            case 4: //执行医师
                holder.mEditLayout.setVisibility(View.GONE);
                break;
        }
        //设置标题
        holder.mName2.setText(data.getName());
        //设置价格进度条
//        holder.mSeek2.setProgressLow(0);                 //滑动前的数据
//        holder.mSeek2.setProgressHigh(20000);            //滑动后的数据

        //设置适配器
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
        gridLayoutManager.setScrollEnable(false);
        for (ProjcetList dataasdas : data.getList()) {
            Log.e("adasdasdasas", "dataasdas222222 == " + dataasdas.getTitle());
        }
        mSortScreenItemAdapter = new SortScreenItemAdapter(mContext,projcetLists,true,data.getDefault_select_id());
        holder.mlist2.setLayoutManager(gridLayoutManager);
        holder.mlist2.setAdapter(mSortScreenItemAdapter);
        mHashMap.put(data.getId(), mSortScreenItemAdapter);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    //样式一
    public class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView mName1;
        RecyclerView mlist1;

        public ViewHolder1(@NonNull View itemView) {
            super(itemView);
            mName1 = itemView.findViewById(R.id.sort_screen_popwin_name1);
            mlist1 = itemView.findViewById(R.id.sort_screen_popwin_list1);
        }
    }

    //样式二
    public class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView mName2;
        DoubleSlideSeekBar mSeek2;
        RecyclerView mlist2;
        LinearLayout mEditLayout;
        public  EditText mEditText1;
        public  EditText mEditText2;

        public ViewHolder2(@NonNull View itemView) {
            super(itemView);
            mName2 = itemView.findViewById(R.id.sort_screen_popwin_name2);
            mSeek2 = itemView.findViewById(R.id.sort_screen_popwin_seek2);
            mlist2 = itemView.findViewById(R.id.sort_screen_popwin_list2);
            mEditLayout = itemView.findViewById(R.id.edit_layout);
            mEditText1 = itemView.findViewById(R.id.sort_screen_popwin_edt1);
            mEditText2 = itemView.findViewById(R.id.sort_screen_popwin_edt2);


        }
    }

    /**
     * 获取选中数据
     *
     * @return : 选中的数据集合
     */
    public ArrayList<ProjcetList> getSelectedData() {
        ArrayList<ProjcetList> selectedData = new ArrayList<>();
        for (ProjcetData data : mDatas) {
            if (data.getId() == 3){
                ArrayList<ProjcetList> selectedData1 = mHashMap.get(data.getId()).getSelectedData();
                if (CollectionUtils.isEmpty(selectedData1)){
                    if ("0".equals(editLowStr) && "0".equals(editHighStr)){
                        continue;
                    }
                    ProjcetList projcetList = new ProjcetList();
                    projcetList.setPostVal(editLowStr+"_"+editHighStr);
                    projcetList.setPostName("real_price_discount");
                    selectedData.add(projcetList);
                }else {
                    selectedData.addAll(selectedData1);
                }
            }else {
                selectedData.addAll(mHashMap.get(data.getId()).getSelectedData());
            }

        }
        return selectedData;
    }

    /**
     * 重置数据
     */
    public void resetData() {
        for (int i = 0; i < mDatas.size(); i++) {
            ProjcetData data = mDatas.get(i);
            SortScreenItemAdapter sortScreenItemAdapter = mHashMap.get(data.getId());
            sortScreenItemAdapter.resetData();

        }

    }

}
