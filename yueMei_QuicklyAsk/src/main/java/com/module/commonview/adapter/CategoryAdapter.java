package com.module.commonview.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.home.model.bean.CategoryBean;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;


import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * 文 件 名: CategoryAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-17 16:51
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class CategoryAdapter extends BaseAdapter {
    private Context mContext;
    private List<CategoryBean.TaoListBean> mList;
    private int mCount = 0;
    private LayoutInflater inflater;

    public CategoryAdapter(Context context, List<CategoryBean.TaoListBean> list, int count) {
        this.mContext = context;
        this.mList = list;
        this.mCount = count;
        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_category, null);
            viewHolder = new ViewHolder();
            viewHolder.tao_list_image = convertView.findViewById(R.id.tao_list_image);
            viewHolder.tao_list_title = convertView.findViewById(R.id.tao_list_title);
            viewHolder.tv_hos = convertView.findViewById(R.id.tv_hos);
            viewHolder.tao_list_price_num = convertView.findViewById(R.id.tao_list_price_num);
            viewHolder.tao_list_flowLayout = convertView.findViewById(R.id.tao_list_flowLayout);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (!TextUtils.isEmpty(mList.get(position).getTao_img())) {
            Glide.with(mContext)
                    .load(mList.get(position).getTao_img())
                    .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                    .placeholder(R.drawable.home_other_placeholder)
                    .into(viewHolder.tao_list_image);
        }
        if (!TextUtils.isEmpty(mList.get(position).getTitle())) {
            viewHolder.tao_list_title.setText(mList.get(position).getTitle());
        }
        if (!TextUtils.isEmpty(mList.get(position).getHospital_name())) {
            viewHolder.tv_hos.setText(mList.get(position).getHospital_name());
        }
        if (!TextUtils.isEmpty(mList.get(position).getPrice())) {
            viewHolder.tao_list_price_num.setText(mList.get(position).getPrice());
        }
        //设置标签
        List<String> promotion = mList.get(position).getIcon_label();
        if (CollectionUtils.isNotEmpty(promotion)) {
            viewHolder.tao_list_flowLayout.setVisibility(View.VISIBLE);
            setTagView(viewHolder.tao_list_flowLayout, promotion);
        } else {
            viewHolder.tao_list_flowLayout.setVisibility(View.GONE);
        }
        return convertView;
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<String> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++) {
                TextView textView = new TextView(mContext);
                textView.setText(lists.get(i));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                textView.setBackgroundResource(R.drawable.shape_category_manjian_ff527f);
                textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                mFlowLayout.addView(textView);
            }
        }
    }


    public class ViewHolder {
        ImageView tao_list_image;
        TextView tao_list_title;
        TextView tv_hos;
        TextView tao_list_price_num;
        FlowLayout tao_list_flowLayout;
    }

    public void updata(List<CategoryBean.TaoListBean> list, int count) {
        mList = list;
        mCount = count;
        notifyDataSetChanged();
    }

    public int getmCount() {
        return mCount;
    }

}
