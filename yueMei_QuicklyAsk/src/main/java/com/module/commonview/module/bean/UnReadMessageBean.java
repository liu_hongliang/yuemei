package com.module.commonview.module.bean;

/**
 * Created by Administrator on 2017/12/12.
 */

public class UnReadMessageBean {

    /**
     * code : 1
     * message : 成功
     * data : {"noread":"0"}
     */

    private String code;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * noread : 0
         */

        private String noread;

        public String getNoread() {
            return noread;
        }

        public void setNoread(String noread) {
            this.noread = noread;
        }
    }
}
