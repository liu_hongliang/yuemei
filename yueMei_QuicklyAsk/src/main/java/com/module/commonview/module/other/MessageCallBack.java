package com.module.commonview.module.other;

import com.module.commonview.module.bean.MessageBean;

/**
 * Created by 裴成浩 on 2018/1/11.
 */

public interface MessageCallBack {
    void receiveMessage(MessageBean.DataBean dataBean,String group_id);

    void onFocusCallBack(String txt);
}
