/**
 * 
 */
package com.module.commonview.module.bean;

/**
 * @author pch
 * 
 */
public class YuDingData {

	private String hos_name;
	private String doc_name;
	private String is_order;
	private String feeScale;
	private String dingjin;			//普通订金价

	private String is_group;		//是否是拼团
	private String group_number;	//起拼人数
	private String group_price;		//拼团全款
	private String group_dingjin;	//拼团订金
	private String group_is_order;	//拼团支持 全款或订金

	private String price;			//悦美价
	private String hos_price;		//到院剩余价格
	private String lijian;			//立减价格（可能为0）
	private String refund;			//是否可以退款
	private String number;			// 限购数量
	private String start_number;	//最低限购数

	private String is_bao;			//是否有保险
	private String baoxian;			//保险的报销本内容显示
	private String bao_price;		//保险金额

	public String getHos_name() {
		return hos_name;
	}

	public void setHos_name(String hos_name) {
		this.hos_name = hos_name;
	}

	public String getDoc_name() {
		return doc_name;
	}

	public void setDoc_name(String doc_name) {
		this.doc_name = doc_name;
	}

	public String getIs_order() {
		return is_order;
	}

	public void setIs_order(String is_order) {
		this.is_order = is_order;
	}

	public String getDingjin() {
		return dingjin;
	}

	public void setDingjin(String dingjin) {
		this.dingjin = dingjin;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getHos_price() {
		return hos_price;
	}

	public void setHos_price(String hos_price) {
		this.hos_price = hos_price;
	}

	public String getLijian() {
		return lijian;
	}

	public void setLijian(String lijian) {
		this.lijian = lijian;
	}

	public String getFeeScale() {
		return feeScale;
	}

	public void setFeeScale(String feeScale) {
		this.feeScale = feeScale;
	}

	public String getRefund() {
		return refund;
	}

	public void setRefund(String refund) {
		this.refund = refund;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStart_number() {
		return start_number;
	}

	public void setStart_number(String start_number) {
		this.start_number = start_number;
	}

	public String getIs_bao() {
		return is_bao;
	}

	public void setIs_bao(String is_bao) {
		this.is_bao = is_bao;
	}

	public String getBaoxian() {
		return baoxian;
	}

	public void setBaoxian(String baoxian) {
		this.baoxian = baoxian;
	}

	public String getBao_price() {
		return bao_price;
	}

	public void setBao_price(String bao_price) {
		this.bao_price = bao_price;
	}

	public String getIs_group() {
		return is_group;
	}

	public void setIs_group(String is_group) {
		this.is_group = is_group;
	}

	public String getGroup_number() {
		return group_number;
	}

	public void setGroup_number(String group_number) {
		this.group_number = group_number;
	}

	public String getGroup_dingjin() {
		return group_dingjin;
	}

	public void setGroup_dingjin(String group_dingjin) {
		this.group_dingjin = group_dingjin;
	}

	public String getGroup_price() {
		return group_price;
	}

	public void setGroup_price(String group_price) {
		this.group_price = group_price;
	}

	public String getGroup_is_order() {
		return group_is_order;
	}

	public void setGroup_is_order(String group_is_order) {
		this.group_is_order = group_is_order;
	}

	@Override
	public String toString() {
		return "YuDingData{" +
				"hos_name='" + hos_name + '\'' +
				", doc_name='" + doc_name + '\'' +
				", is_order='" + is_order + '\'' +
				", dingjin='" + dingjin + '\'' +
				", price='" + price + '\'' +
				", hos_price='" + hos_price + '\'' +
				", lijian='" + lijian + '\'' +
				", feeScale='" + feeScale + '\'' +
				", refund='" + refund + '\'' +
				", number='" + number + '\'' +
				", start_number='" + start_number + '\'' +
				", is_bao='" + is_bao + '\'' +
				", baoxian='" + baoxian + '\'' +
				", bao_price='" + bao_price + '\'' +
				'}';
	}
}
