package com.module.commonview.module.bean;

import java.util.HashMap;

public class QuickReplyTop {

    private String show_content;
    private String send_content;
    private String quick_reply;
    private String event_name;
    private String event_pos;
    private String menu_type;
    private String menu_img;
    private String jump_url;
    private HashMap<String,String> params;

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public String getShow_content() {
        return show_content;
    }

    public void setShow_content(String show_content) {
        this.show_content = show_content;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }

    public String getQuick_reply() {
        return quick_reply;
    }

    public void setQuick_reply(String quick_reply) {
        this.quick_reply = quick_reply;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }

    public String getMenu_img() {
        return menu_img;
    }

    public void setMenu_img(String menu_img) {
        this.menu_img = menu_img;
    }
}
