package com.module.commonview.module.bean;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostContentImage{
    private String img;
    private String url;
    private String width;
    private String height;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
