package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/22.
 */
public class DiaryListListPic implements Parcelable {
    private String img;
    private String width;
    private String height;
    private String video_url;
    private String is_video;

    public DiaryListListPic() {
    }

    protected DiaryListListPic(Parcel in) {
        img = in.readString();
        width = in.readString();
        height = in.readString();
        video_url = in.readString();
        is_video = in.readString();
    }

    public static final Creator<DiaryListListPic> CREATOR = new Creator<DiaryListListPic>() {
        @Override
        public DiaryListListPic createFromParcel(Parcel in) {
            return new DiaryListListPic(in);
        }

        @Override
        public DiaryListListPic[] newArray(int size) {
            return new DiaryListListPic[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(width);
        dest.writeString(height);
        dest.writeString(video_url);
        dest.writeString(is_video);
    }
}
