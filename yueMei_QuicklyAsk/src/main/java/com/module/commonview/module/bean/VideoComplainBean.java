package com.module.commonview.module.bean;

import java.util.List;

public class VideoComplainBean {
    private DoctorInfo doctorInfo;
    private List<ComplaintOption> complaintOption;

    public DoctorInfo getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(DoctorInfo doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public List<ComplaintOption> getComplaintOption() {
        return complaintOption;
    }

    public void setComplaintOption(List<ComplaintOption> complaintOption) {
        this.complaintOption = complaintOption;
    }
}
