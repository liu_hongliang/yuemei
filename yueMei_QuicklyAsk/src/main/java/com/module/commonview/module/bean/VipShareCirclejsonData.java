package com.module.commonview.module.bean;

/**
 * Created by 裴成浩 on 2018/9/14.
 */
public class VipShareCirclejsonData {
    private String title;
    private String content;
    private String callback_url;
    private String img_weix;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getImg_weix() {
        return img_weix;
    }

    public void setImg_weix(String img_weix) {
        this.img_weix = img_weix;
    }
}
