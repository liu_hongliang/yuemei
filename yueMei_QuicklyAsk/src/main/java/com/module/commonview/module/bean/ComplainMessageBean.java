package com.module.commonview.module.bean;

public class ComplainMessageBean {
    private String text;

    public ComplainMessageBean(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
