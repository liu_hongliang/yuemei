package com.module.commonview.module.bean;

public class ChatShowLogin {
    private String hos_id;

    public ChatShowLogin(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }
}
