package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 判断是否收藏
 * Created by Administrator on 2017/10/16.
 */

public class IsCollectApi implements BaseCallBackApi {
    private String TAG = "IsCollectApi";
    private HashMap<String, Object> hashMap;  //传值容器

    public IsCollectApi() {
        hashMap = new HashMap<>();
    }
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "isjoin", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " +mData.toString());
                if ("1".equals(mData.code)) {
                    listener.onSuccess(mData.data);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }
}
