package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.module.MyApplication;
import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import org.kymjs.aframe.ui.ViewInject;

import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by Administrator on 2017/10/13.
 */

public class BBsDetailUserInfoApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "getuserinfo", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
               listener.onSuccess(mData);
            }
        });
    }
}
