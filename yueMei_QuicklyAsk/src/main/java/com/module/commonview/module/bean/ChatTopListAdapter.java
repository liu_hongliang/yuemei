package com.module.commonview.module.bean;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

public class ChatTopListAdapter extends BaseQuickAdapter<QuickReplyTop, BaseViewHolder> {
    public ChatTopListAdapter(int layoutResId, @Nullable List<QuickReplyTop> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, QuickReplyTop item) {

        int windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        RelativeLayout itemView = helper.getView(R.id.chat_top_list_item_container);
        ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.width = windowsWight / 3;
        itemView.setLayoutParams(layoutParams);

        Glide.with(mContext).load(item.getMenu_img()).into((ImageView) helper.getView(R.id.chat_top_list_item_background));
//        RelativeLayout container=helper.getView(R.id.chat_top_list_item_container);
//        ViewGroup.LayoutParams layoutParams =  container.getLayoutParams();
//        layoutParams.width=weight /size;
//        container.setLayoutParams(layoutParams);
        helper.setText(R.id.chat_top_list_item_text,item.getShow_content());

    }
}
