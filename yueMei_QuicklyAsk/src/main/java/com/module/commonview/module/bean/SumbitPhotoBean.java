package com.module.commonview.module.bean;

/**
 * Created by 裴成浩 on 2018/6/20.
 */
public class SumbitPhotoBean {

    private String onelogin;
    private String _id;
    private String url;

    public String getOnelogin() {
        return onelogin;
    }

    public void setOnelogin(String onelogin) {
        this.onelogin = onelogin;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
