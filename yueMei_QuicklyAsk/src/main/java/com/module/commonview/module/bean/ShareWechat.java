package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/1/4.
 */

public class ShareWechat implements Parcelable{
    private String title;
    private String description;
    private String webpageUrl;
    private String path;
    private String thumbImage;
    private String userName;

    public ShareWechat() {
    }

    protected ShareWechat(Parcel in) {
        title = in.readString();
        description = in.readString();
        webpageUrl = in.readString();
        path = in.readString();
        thumbImage = in.readString();
        userName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(webpageUrl);
        dest.writeString(path);
        dest.writeString(thumbImage);
        dest.writeString(userName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ShareWechat> CREATOR = new Creator<ShareWechat>() {
        @Override
        public ShareWechat createFromParcel(Parcel in) {
            return new ShareWechat(in);
        }

        @Override
        public ShareWechat[] newArray(int size) {
            return new ShareWechat[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebpageUrl() {
        return webpageUrl;
    }

    public void setWebpageUrl(String webpageUrl) {
        this.webpageUrl = webpageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getThumbImage() {
        return thumbImage;
    }

    public void setThumbImage(String thumbImage) {
        this.thumbImage = thumbImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
