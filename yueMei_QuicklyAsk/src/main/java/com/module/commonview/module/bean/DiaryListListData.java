package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;

/**
 * 日记本列表
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryListListData implements Parcelable {

    private String id;
    private String url;
    private String day;
    private String day_title;
    private String user_id;
    private String content;
    private String agree_num;
    private String have_pic;
    private String is_agree;
    private List<DiaryListListPic> pic;
    private String cycle;
    private String diary_number;
    private HashMap<String,String> event_params;

    protected DiaryListListData(Parcel in) {
        id = in.readString();
        url = in.readString();
        day = in.readString();
        day_title = in.readString();
        user_id = in.readString();
        content = in.readString();
        agree_num = in.readString();
        have_pic = in.readString();
        is_agree = in.readString();
        pic = in.createTypedArrayList(DiaryListListPic.CREATOR);
        cycle = in.readString();
        diary_number = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(url);
        dest.writeString(day);
        dest.writeString(day_title);
        dest.writeString(user_id);
        dest.writeString(content);
        dest.writeString(agree_num);
        dest.writeString(have_pic);
        dest.writeString(is_agree);
        dest.writeTypedList(pic);
        dest.writeString(cycle);
        dest.writeString(diary_number);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryListListData> CREATOR = new Creator<DiaryListListData>() {
        @Override
        public DiaryListListData createFromParcel(Parcel in) {
            return new DiaryListListData(in);
        }

        @Override
        public DiaryListListData[] newArray(int size) {
            return new DiaryListListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDay_title() {
        return day_title;
    }

    public void setDay_title(String day_title) {
        this.day_title = day_title;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public String getHave_pic() {
        return have_pic;
    }

    public void setHave_pic(String have_pic) {
        this.have_pic = have_pic;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    public List<DiaryListListPic> getPic() {
        return pic;
    }

    public void setPic(List<DiaryListListPic> pic) {
        this.pic = pic;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getDiary_number() {
        return diary_number;
    }

    public void setDiary_number(String diary_number) {
        this.diary_number = diary_number;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
