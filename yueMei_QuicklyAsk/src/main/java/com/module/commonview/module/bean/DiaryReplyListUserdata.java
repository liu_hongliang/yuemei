package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyListUserdata implements Parcelable {

    private String talent;
    private String lable;
    private String name;
    private String id;
    private String avatar;
    private String url;
    private String group_id;
    private String is_rongyun;
    private String hos_userid;
    private String doctor_id;
    private String hospital_id;
    private String obj_id;
    private String obj_type;
    private String title;
    private String cityName;
    private String hospitalName;
    private String is_following;

    public DiaryReplyListUserdata() {
    }

    protected DiaryReplyListUserdata(Parcel in) {
        talent = in.readString();
        lable = in.readString();
        name = in.readString();
        id = in.readString();
        avatar = in.readString();
        url = in.readString();
        group_id = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        doctor_id = in.readString();
        hospital_id = in.readString();
        obj_id = in.readString();
        obj_type = in.readString();
        title = in.readString();
        cityName = in.readString();
        hospitalName = in.readString();
        is_following = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(talent);
        dest.writeString(lable);
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(avatar);
        dest.writeString(url);
        dest.writeString(group_id);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
        dest.writeString(doctor_id);
        dest.writeString(hospital_id);
        dest.writeString(obj_id);
        dest.writeString(obj_type);
        dest.writeString(title);
        dest.writeString(cityName);
        dest.writeString(hospitalName);
        dest.writeString(is_following);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryReplyListUserdata> CREATOR = new Creator<DiaryReplyListUserdata>() {
        @Override
        public DiaryReplyListUserdata createFromParcel(Parcel in) {
            return new DiaryReplyListUserdata(in);
        }

        @Override
        public DiaryReplyListUserdata[] newArray(int size) {
            return new DiaryReplyListUserdata[size];
        }
    };

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getObj_id() {
        return obj_id;
    }

    public void setObj_id(String obj_id) {
        this.obj_id = obj_id;
    }

    public String getObj_type() {
        return obj_type;
    }

    public void setObj_type(String obj_type) {
        this.obj_type = obj_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getIs_following() {
        return is_following;
    }

    public void setIs_following(String is_following) {
        this.is_following = is_following;
    }
}
