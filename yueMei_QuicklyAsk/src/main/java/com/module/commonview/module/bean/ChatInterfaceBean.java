package com.module.commonview.module.bean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/12/11.
 */

public class ChatInterfaceBean {

    /**
     * pushWelcome : 0
     * welcome :
     * title : 西安西京医院整形外科
     * hos_name : 西安西京医院整形外科
     * hos_id : 5255
     * group_id : 4696
     * id : 71150
     * gohospital : 1
     */

    private String pushWelcome;
    private String welcome;
    private String title;
    private String hos_name;
    private String hos_id;
    private String group_id;
    private String id;
    private String gohospital;
    private List<QuickReply> quick_reply;
    private String icon;
    private List<QuickReplyTop> quick_reply_top;
    private List<QuickQuestion> quick_question;
    private HashMap<String, String> quick_question_event_params;
    private String online;


    public String getPushWelcome() {
        return pushWelcome;
    }

    public void setPushWelcome(String pushWelcome) {
        this.pushWelcome = pushWelcome;
    }

    public String getWelcome() {
        return welcome;
    }

    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGohospital() {
        return gohospital;
    }

    public void setGohospital(String gohospital) {
        this.gohospital = gohospital;
    }

    public List<QuickReply> getQuick_reply() {
        return quick_reply;
    }

    public void setQuick_reply(List<QuickReply> quick_reply) {
        this.quick_reply = quick_reply;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<QuickReplyTop> getQuick_reply_top() {
        return quick_reply_top;
    }

    public void setQuick_reply_top(List<QuickReplyTop> quick_reply_top) {
        this.quick_reply_top = quick_reply_top;
    }

    public List<QuickQuestion> getQuick_question() {
        return quick_question;
    }

    public void setQuick_question(List<QuickQuestion> quick_question) {
        this.quick_question = quick_question;
    }

    public HashMap<String, String> getQuick_question_event_params() {
        return quick_question_event_params;
    }

    public void setQuick_question_event_params(HashMap<String, String> quick_question_event_params) {
        this.quick_question_event_params = quick_question_event_params;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }
}
