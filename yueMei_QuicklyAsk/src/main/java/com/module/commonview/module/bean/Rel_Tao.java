package com.module.commonview.module.bean;

/**
 * 组合功能数据 规格
 * 
 * @author dwb
 * 
 */
public class Rel_Tao {

	private String tao_id;
	private String spe_name;
	private String status;

	public String getTao_id() {
		return tao_id;
	}

	public void setTao_id(String tao_id) {
		this.tao_id = tao_id;
	}

	public String getSpe_name() {
		return spe_name;
	}

	public void setSpe_name(String spe_name) {
		this.spe_name = spe_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
