package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.google.gson.stream.JsonReader;
import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.bean.DiaryListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.XinJsonReader;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryListApi implements BaseCallBackApi {
    private String TAG = "DiaryListApi";
    private HashMap<String, Object> mDiaryListHashMap;  //传值容器

    public DiaryListApi() {
        mDiaryListHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "shareinfo", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                if ("1".equals(mData.code)) {

                    JsonReader jsonReader = new XinJsonReader(new StringReader(mData.data));
                    jsonReader.setLenient(true);

                    DiaryListData diaryListData = JSONUtil.TransformSingleBean(jsonReader, DiaryListData.class);
                    listener.onSuccess(diaryListData);
                } else if ("404".equals(mData.code)) {
                    onEvent404Listener.onEvent404Click(mData.message);
                }

            }
        });
    }

    public HashMap<String, Object> getDiaryListHashMap() {
        return mDiaryListHashMap;
    }

    public void addData(String key, String value) {
        mDiaryListHashMap.put(key, value);
    }

    public interface OnEvent404Listener {
        void onEvent404Click(String message);
    }

    private OnEvent404Listener onEvent404Listener;

    public void setOnEvent404Listener(OnEvent404Listener onEvent404Listener) {
        this.onEvent404Listener = onEvent404Listener;
    }
}
