package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class VideoChatGetTokenBean implements Parcelable {

    /**
     * app_id : zfcm0mja
     * user_id : c209aae1cc8146d5
     * gslb : ["https://rgslb.rtc.aliyuncs.com"]
     * token : a13dd525bac3596410ed0757aeab04bf06a2ed0090ba517d57ae071590752779
     * nonce : AK-5e8bf0e988e95
     * timestamp : 1586272681
     * channel_id : 10000
     */

    private String app_id;
    private String user_id;
    private String token;
    private String nonce;
    private int timestamp;
    private String channel_id;
    private List<String> gslb;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public List<String> getGslb() {
        return gslb;
    }

    public void setGslb(List<String> gslb) {
        this.gslb = gslb;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.app_id);
        dest.writeString(this.user_id);
        dest.writeString(this.token);
        dest.writeString(this.nonce);
        dest.writeInt(this.timestamp);
        dest.writeString(this.channel_id);
        dest.writeStringList(this.gslb);
    }

    public VideoChatGetTokenBean() {
    }

    protected VideoChatGetTokenBean(Parcel in) {
        this.app_id = in.readString();
        this.user_id = in.readString();
        this.token = in.readString();
        this.nonce = in.readString();
        this.timestamp = in.readInt();
        this.channel_id = in.readString();
        this.gslb = in.createStringArrayList();
    }

    public static final Parcelable.Creator<VideoChatGetTokenBean> CREATOR = new Parcelable.Creator<VideoChatGetTokenBean>() {
        @Override
        public VideoChatGetTokenBean createFromParcel(Parcel source) {
            return new VideoChatGetTokenBean(source);
        }

        @Override
        public VideoChatGetTokenBean[] newArray(int size) {
            return new VideoChatGetTokenBean[size];
        }
    };
}
