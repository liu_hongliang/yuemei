package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.module.bean.SearchTaoDate;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */

public class LoadSearchTaoDataApi implements BaseCallBackApi {
    private String TAG = "LoadSearchTaoDataApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public LoadSearchTaoDataApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.TAOPK, "searchtao", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                try {
                    if ("1".equals(mData.code)) {
                        SearchTaoDate searchTaoDate = JSONUtil.TransformSingleBean(mData.data, SearchTaoDate.class);
                        listener.onSuccess(searchTaoDate);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
