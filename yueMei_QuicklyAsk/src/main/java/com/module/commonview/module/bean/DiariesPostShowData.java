package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/8/17
 */
public class DiariesPostShowData implements Parcelable {
    private String data;
    private int number;

    public DiariesPostShowData(String data, int number) {
        this.data = data;
        this.number = number;
    }

    public String getData() {
        return data;
    }

    public int getNumber() {
        return number;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.data);
        dest.writeInt(this.number);
    }

    protected DiariesPostShowData(Parcel in) {
        this.data = in.readString();
        this.number = in.readInt();
    }

    public static final Parcelable.Creator<DiariesPostShowData> CREATOR = new Parcelable.Creator<DiariesPostShowData>() {
        @Override
        public DiariesPostShowData createFromParcel(Parcel source) {
            return new DiariesPostShowData(source);
        }

        @Override
        public DiariesPostShowData[] newArray(int size) {
            return new DiariesPostShowData[size];
        }
    };
}
