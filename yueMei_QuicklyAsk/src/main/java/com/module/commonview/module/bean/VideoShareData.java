package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/2/24.
 */

public class VideoShareData implements Parcelable {

    private String url = "";
    private String title = "";
    private String content = "";
    private String img_weibo = "";
    private String img_weix = "";
    private String askorshare = "";
    private ShareWechat Wechat;

    public VideoShareData() {
    }

    protected VideoShareData(Parcel in) {
        url = in.readString();
        title = in.readString();
        content = in.readString();
        img_weibo = in.readString();
        img_weix = in.readString();
        askorshare = in.readString();
        Wechat = in.readParcelable(ShareWechat.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(img_weibo);
        dest.writeString(img_weix);
        dest.writeString(askorshare);
        dest.writeParcelable(Wechat, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VideoShareData> CREATOR = new Creator<VideoShareData>() {
        @Override
        public VideoShareData createFromParcel(Parcel in) {
            return new VideoShareData(in);
        }

        @Override
        public VideoShareData[] newArray(int size) {
            return new VideoShareData[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public VideoShareData setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public VideoShareData setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public VideoShareData setContent(String content) {
        this.content = content;
        return this;
    }

    public String getImg_weibo() {
        return img_weibo;
    }

    public VideoShareData setImg_weibo(String img_weibo) {
        this.img_weibo = img_weibo;
        return this;
    }

    public String getImg_weix() {
        return img_weix;
    }

    public VideoShareData setImg_weix(String img_weix) {
        this.img_weix = img_weix;
        return this;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public VideoShareData setAskorshare(String askorshare) {
        this.askorshare = askorshare;
        return this;
    }

    public ShareWechat getWechat() {
        return Wechat;
    }

    public VideoShareData setWechat(ShareWechat wechat) {
        Wechat = wechat;
        return this;
    }
}
