package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryCompareBean implements Parcelable {
    private BeforeBean before;
    private AfterBean after;

    public BeforeBean getBefore() {
        return before;
    }

    public void setBefore(BeforeBean before) {
        this.before = before;
    }

    public AfterBean getAfter() {
        return after;
    }

    public void setAfter(AfterBean after) {
        this.after = after;
    }

    public static class BeforeBean implements Parcelable {

        private String img;
        private String width;
        private String height;
        private String pic_num;
        private HashMap<String,String> event_params;


        protected BeforeBean(Parcel in) {
            img = in.readString();
            width = in.readString();
            height = in.readString();
            pic_num = in.readString();
        }

        public static final Creator<BeforeBean> CREATOR = new Creator<BeforeBean>() {
            @Override
            public BeforeBean createFromParcel(Parcel in) {
                return new BeforeBean(in);
            }

            @Override
            public BeforeBean[] newArray(int size) {
                return new BeforeBean[size];
            }
        };

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getPic_num() {
            return pic_num;
        }

        public void setPic_num(String pic_num) {
            this.pic_num = pic_num;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(img);
            dest.writeString(width);
            dest.writeString(height);
            dest.writeString(pic_num);
        }
    }

    public static class AfterBean implements Parcelable {

        private String img;
        private String width;
        private String height;
        private String pic_num;
        private HashMap<String,String> event_params;

        protected AfterBean(Parcel in) {
            img = in.readString();
            width = in.readString();
            height = in.readString();
            pic_num = in.readString();
        }

        public static final Creator<AfterBean> CREATOR = new Creator<AfterBean>() {
            @Override
            public AfterBean createFromParcel(Parcel in) {
                return new AfterBean(in);
            }

            @Override
            public AfterBean[] newArray(int size) {
                return new AfterBean[size];
            }
        };

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getPic_num() {
            return pic_num;
        }

        public void setPic_num(String pic_num) {
            this.pic_num = pic_num;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(img);
            dest.writeString(width);
            dest.writeString(height);
            dest.writeString(pic_num);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.before, flags);
        dest.writeParcelable(this.after, flags);
    }

    public DiaryCompareBean() {
    }

    protected DiaryCompareBean(Parcel in) {
        this.before = in.readParcelable(BeforeBean.class.getClassLoader());
        this.after = in.readParcelable(AfterBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<DiaryCompareBean> CREATOR = new Parcelable.Creator<DiaryCompareBean>() {
        @Override
        public DiaryCompareBean createFromParcel(Parcel source) {
            return new DiaryCompareBean(source);
        }

        @Override
        public DiaryCompareBean[] newArray(int size) {
            return new DiaryCompareBean[size];
        }
    };
}
