package com.module.commonview.module.bean;

public class QuickReply {

    /**
     * show_content : 关于价格
     * send_content : 请问这个项目的价格，都包含什么费用？
     * event_name : quick_reply
     * event_pos : 1
     */

    private String show_content;
    private String send_content;
    private String event_name;
    private String event_pos;
    private String menu_type;
    private String jump_url;

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public String getShow_content() {
        return show_content;
    }

    public void setShow_content(String show_content) {
        this.show_content = show_content;
    }

    public String getSend_content() {
        return send_content;
    }

    public void setSend_content(String send_content) {
        this.send_content = send_content;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }
}
