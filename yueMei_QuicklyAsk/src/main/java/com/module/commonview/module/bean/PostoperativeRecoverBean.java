package com.module.commonview.module.bean;

import java.util.List;

public class PostoperativeRecoverBean {

    private List<DataBean> data;
    private List<TaoDataBean> taoData;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<TaoDataBean> getTaoData() {
        return taoData;
    }

    public void setTaoData(List<TaoDataBean> taoData) {
        this.taoData = taoData;
    }

    public static class DataBean {
        /**
         * title : 术前照片
         * user_id : 85329239
         * id : 2423524
         * url : https://m.yuemei.com/c/2423524.html
         * pic : [{"img":"https://p31.yuemei.com/postimg/20180301/150_150/15198991936680.jpg","pic_id":"4740439","images":"upload/forum/image/20180301/15198991936680.jpg","width":"150","height":"150","weight":"1","is_video":"0","video_url":""},{"img":"https://p31.yuemei.com/postimg/20180301/150_150/1519899196342.jpg","pic_id":"4740442","images":"upload/forum/image/20180301/1519899196342.jpg","width":"150","height":"150","weight":"2","is_video":"0","video_url":""},{"img":"https://p31.yuemei.com/postimg/20180605/150_150/15281791936562.jpg","pic_id":"6676162","images":"upload/forum/image/20180605/15281791936562.jpg","width":"150","height":"150","weight":"3","is_video":"0","video_url":""}]
         * surgeryafterdays : 112
         */

        private String title;
        private String user_id;
        private String id;
        private String url;
        private String surgeryafterdays;
        private List<PicBean> pic;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getSurgeryafterdays() {
            return surgeryafterdays;
        }

        public void setSurgeryafterdays(String surgeryafterdays) {
            this.surgeryafterdays = surgeryafterdays;
        }

        public List<PicBean> getPic() {
            return pic;
        }

        public void setPic(List<PicBean> pic) {
            this.pic = pic;
        }

        public static class PicBean {
            /**
             * img : https://p31.yuemei.com/postimg/20180301/150_150/15198991936680.jpg
             * pic_id : 4740439
             * images : upload/forum/image/20180301/15198991936680.jpg
             * width : 150
             * height : 150
             * weight : 1
             * is_video : 0
             * video_url :
             */

            private String img;
            private String pic_id;
            private String images;
            private String width;
            private String height;
            private String weight;
            private String is_video;
            private String video_url;
            private boolean is_cover;
            public boolean isIs_cover() {
                return is_cover;
            }

            public void setIs_cover(boolean is_cover) {
                this.is_cover = is_cover;
            }


            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getPic_id() {
                return pic_id;
            }

            public void setPic_id(String pic_id) {
                this.pic_id = pic_id;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getWidth() {
                return width;
            }

            public void setWidth(String width) {
                this.width = width;
            }

            public String getHeight() {
                return height;
            }

            public void setHeight(String height) {
                this.height = height;
            }

            public String getWeight() {
                return weight;
            }

            public void setWeight(String weight) {
                this.weight = weight;
            }

            public String getIs_video() {
                return is_video;
            }

            public void setIs_video(String is_video) {
                this.is_video = is_video;
            }

            public String getVideo_url() {
                return video_url;
            }

            public void setVideo_url(String video_url) {
                this.video_url = video_url;
            }
        }
    }

    public static class TaoDataBean {
        /**
         * id : 34175
         * title : 【网红脸套餐】广州面部私人订制：眼综合+鼻综合  瞬变女神  美到爆  院长亲诊 效果保障
         * price_discount : 11680
         * price : 21800
         * showPrice : 1
         * fee_scale : /次
         * list_cover_image : https://p14.yuemei.com/tao/2018/0621/200_200/jt180621185518_6a0e3a.jpg
         * fanxian : 体验项目写日记返现400元
         */

        private String id;
        private String title;
        private String price_discount;
        private String price;
        private String showPrice;
        private String fee_scale;
        private String list_cover_image;
        private String fanxian;
        private String member_price;
        private String is_rongyun;
        private String hos_userid;
        private String hospital_id;
        private String doctor_id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getShowPrice() {
            return showPrice;
        }

        public void setShowPrice(String showPrice) {
            this.showPrice = showPrice;
        }

        public String getFee_scale() {
            return fee_scale;
        }

        public void setFee_scale(String fee_scale) {
            this.fee_scale = fee_scale;
        }

        public String getList_cover_image() {
            return list_cover_image;
        }

        public void setList_cover_image(String list_cover_image) {
            this.list_cover_image = list_cover_image;
        }

        public String getFanxian() {
            return fanxian;
        }

        public void setFanxian(String fanxian) {
            this.fanxian = fanxian;
        }

        public String getMember_price() {
            return member_price;
        }

        public void setMember_price(String member_price) {
            this.member_price = member_price;
        }

        public String getIs_rongyun() {
            return is_rongyun;
        }

        public void setIs_rongyun(String is_rongyun) {
            this.is_rongyun = is_rongyun;
        }

        public String getHos_userid() {
            return hos_userid;
        }

        public void setHos_userid(String hos_userid) {
            this.hos_userid = hos_userid;
        }

        public String getHospital_id() {
            return hospital_id;
        }

        public void setHospital_id(String hospital_id) {
            this.hospital_id = hospital_id;
        }

        public String getDoc_id() {
            return doctor_id;
        }

        public void setDoc_id(String doc_id) {
            this.doctor_id = doc_id;
        }
    }

}
