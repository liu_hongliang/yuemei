package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/4/26
 */
public class BaomingData implements Parcelable {
    private String show_title;
    private String alert_title;
    private String baoming_is_end;

    protected BaomingData(Parcel in) {
        show_title = in.readString();
        alert_title = in.readString();
        baoming_is_end = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(show_title);
        dest.writeString(alert_title);
        dest.writeString(baoming_is_end);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaomingData> CREATOR = new Creator<BaomingData>() {
        @Override
        public BaomingData createFromParcel(Parcel in) {
            return new BaomingData(in);
        }

        @Override
        public BaomingData[] newArray(int size) {
            return new BaomingData[size];
        }
    };

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getAlert_title() {
        return alert_title;
    }

    public void setAlert_title(String alert_title) {
        this.alert_title = alert_title;
    }

    public String getBaoming_is_end() {
        return baoming_is_end;
    }

    public void setBaoming_is_end(String baoming_is_end) {
        this.baoming_is_end = baoming_is_end;
    }
}
