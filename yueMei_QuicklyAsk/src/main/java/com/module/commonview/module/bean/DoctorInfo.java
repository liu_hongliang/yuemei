package com.module.commonview.module.bean;

import java.util.List;

public class DoctorInfo {
    private String show_doctors_name;
    private String show_doctors_job;
    private String show_doctors_job_key;
    private String show_doctors_avatar;
    private String hospital_name;
    private String video_backdrop;
    private List<String> doctors_good_part;

    public List<String> getDoctors_good_part() {
        return doctors_good_part;
    }

    public void setDoctors_good_part(List<String> doctors_good_part) {
        this.doctors_good_part = doctors_good_part;
    }

    public String getShow_doctors_name() {
        return show_doctors_name;
    }

    public void setShow_doctors_name(String show_doctors_name) {
        this.show_doctors_name = show_doctors_name;
    }

    public String getShow_doctors_job() {
        return show_doctors_job;
    }

    public void setShow_doctors_job(String show_doctors_job) {
        this.show_doctors_job = show_doctors_job;
    }

    public String getShow_doctors_job_key() {
        return show_doctors_job_key;
    }

    public void setShow_doctors_job_key(String show_doctors_job_key) {
        this.show_doctors_job_key = show_doctors_job_key;
    }

    public String getShow_doctors_avatar() {
        return show_doctors_avatar;
    }

    public void setShow_doctors_avatar(String show_doctors_avatar) {
        this.show_doctors_avatar = show_doctors_avatar;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getVideo_backdrop() {
        return video_backdrop;
    }

    public void setVideo_backdrop(String video_backdrop) {
        this.video_backdrop = video_backdrop;
    }
}
