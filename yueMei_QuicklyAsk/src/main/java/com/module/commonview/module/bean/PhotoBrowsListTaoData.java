package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/6/12.
 */
public class PhotoBrowsListTaoData implements Parcelable{

    /**
     * is_show_member : 1
     * pay_dingjin : 1360
     * number : 10
     * start_number : 1
     * pay_price_discount : 6800
     * member_price : 6460
     * m_list_logo :
     * bmsid : 0
     * seckilling : 0
     * img : https://p24.yuemei.com/tao/2018/1220/200_200/jt181220170002_2b8fde.jpg
     * title : 鼻综合
     * subtitle : 鼻综合Ⅰ假体+耳软骨 Ⅱ立体综合隆鼻 Ⅲ无惧揉捏 生态自然 翘/挺/美鼻
     * hos_name : 北京华悦府医疗美容诊所
     * doc_name : 王宁博
     * price : 18000
     * price_discount : 6800
     * price_range_max : 0
     * id : 90623
     * _id : 90623
     * showprice : 1
     * specialPrice : 0
     * show_hospital : 1
     * invitation : 0
     * lijian : 0
     * baoxian :
     * insure : {"is_insure":"0","insure_pay_money":"0","title":""}
     * img66 :
     * repayment : 最高可享12期分期付款：花呗分期
     * hos_red_packet :
     * mingyi : 0
     * hot : 0
     * newp : 0
     * shixiao : 0
     * extension_user :
     * postStr :
     * depreciate :
     * rate : 1250人预订
     * feeScale : /次
     * is_fanxian : 1
     * promotion : []
     * is_rongyun : 3
     * hos_userid : 85007051
     */

    private String is_show_member;
    private String pay_dingjin;
    private String number;
    private String start_number;
    private String pay_price_discount;
    private String member_price;
    private String m_list_logo;
    private String bmsid;
    private String seckilling;
    private String img;
    private String title;
    private String subtitle;
    private String hos_name;
    private String doc_name;
    private String price;
    private String price_discount;
    private String price_range_max;
    private String id;
    private String _id;
    private String showprice;
    private String specialPrice;
    private String show_hospital;
    private String invitation;
    private String lijian;
    private String baoxian;
    private InsureBean insure;
    private String img66;
    private String repayment;
    private String hos_red_packet;
    private String mingyi;
    private String hot;
    private String newp;
    private String shixiao;
    private String extension_user;
    private String postStr;
    private String depreciate;
    private String rate;
    private String feeScale;
    private String is_fanxian;
    private String is_rongyun;
    private String hos_userid;
    private String url;
    private String hospital_id;
    private String doctor_id;


    public String getIs_show_member() {
        return is_show_member;
    }

    public void setIs_show_member(String is_show_member) {
        this.is_show_member = is_show_member;
    }

    public String getPay_dingjin() {
        return pay_dingjin;
    }

    public void setPay_dingjin(String pay_dingjin) {
        this.pay_dingjin = pay_dingjin;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStart_number() {
        return start_number;
    }

    public void setStart_number(String start_number) {
        this.start_number = start_number;
    }

    public String getPay_price_discount() {
        return pay_price_discount;
    }

    public void setPay_price_discount(String pay_price_discount) {
        this.pay_price_discount = pay_price_discount;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public String getM_list_logo() {
        return m_list_logo;
    }

    public void setM_list_logo(String m_list_logo) {
        this.m_list_logo = m_list_logo;
    }

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getSeckilling() {
        return seckilling;
    }

    public void setSeckilling(String seckilling) {
        this.seckilling = seckilling;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getHos_name() {
        return hos_name;
    }

    public void setHos_name(String hos_name) {
        this.hos_name = hos_name;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getPrice_range_max() {
        return price_range_max;
    }

    public void setPrice_range_max(String price_range_max) {
        this.price_range_max = price_range_max;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getShowprice() {
        return showprice;
    }

    public void setShowprice(String showprice) {
        this.showprice = showprice;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public String getShow_hospital() {
        return show_hospital;
    }

    public void setShow_hospital(String show_hospital) {
        this.show_hospital = show_hospital;
    }

    public String getInvitation() {
        return invitation;
    }

    public void setInvitation(String invitation) {
        this.invitation = invitation;
    }

    public String getLijian() {
        return lijian;
    }

    public void setLijian(String lijian) {
        this.lijian = lijian;
    }

    public String getBaoxian() {
        return baoxian;
    }

    public void setBaoxian(String baoxian) {
        this.baoxian = baoxian;
    }

    public InsureBean getInsure() {
        return insure;
    }

    public void setInsure(InsureBean insure) {
        this.insure = insure;
    }

    public String getImg66() {
        return img66;
    }

    public void setImg66(String img66) {
        this.img66 = img66;
    }

    public String getRepayment() {
        return repayment;
    }

    public void setRepayment(String repayment) {
        this.repayment = repayment;
    }

    public String getHos_red_packet() {
        return hos_red_packet;
    }

    public void setHos_red_packet(String hos_red_packet) {
        this.hos_red_packet = hos_red_packet;
    }

    public String getMingyi() {
        return mingyi;
    }

    public void setMingyi(String mingyi) {
        this.mingyi = mingyi;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getNewp() {
        return newp;
    }

    public void setNewp(String newp) {
        this.newp = newp;
    }

    public String getShixiao() {
        return shixiao;
    }

    public void setShixiao(String shixiao) {
        this.shixiao = shixiao;
    }

    public String getExtension_user() {
        return extension_user;
    }

    public void setExtension_user(String extension_user) {
        this.extension_user = extension_user;
    }

    public String getPostStr() {
        return postStr;
    }

    public void setPostStr(String postStr) {
        this.postStr = postStr;
    }

    public String getDepreciate() {
        return depreciate;
    }

    public void setDepreciate(String depreciate) {
        this.depreciate = depreciate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getFeeScale() {
        return feeScale;
    }

    public void setFeeScale(String feeScale) {
        this.feeScale = feeScale;
    }

    public String getIs_fanxian() {
        return is_fanxian;
    }

    public void setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getDoc_id() {
        return doctor_id;
    }

    public void setDoc_id(String doc_id) {
        this.doctor_id = doc_id;
    }

    public static class InsureBean {
        /**
         * is_insure : 0
         * insure_pay_money : 0
         * title :
         */

        private String is_insure;
        private String insure_pay_money;
        private String title;

        public String getIs_insure() {
            return is_insure;
        }

        public void setIs_insure(String is_insure) {
            this.is_insure = is_insure;
        }

        public String getInsure_pay_money() {
            return insure_pay_money;
        }

        public void setInsure_pay_money(String insure_pay_money) {
            this.insure_pay_money = insure_pay_money;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.is_show_member);
        dest.writeString(this.pay_dingjin);
        dest.writeString(this.number);
        dest.writeString(this.start_number);
        dest.writeString(this.pay_price_discount);
        dest.writeString(this.member_price);
        dest.writeString(this.m_list_logo);
        dest.writeString(this.bmsid);
        dest.writeString(this.seckilling);
        dest.writeString(this.img);
        dest.writeString(this.title);
        dest.writeString(this.subtitle);
        dest.writeString(this.hos_name);
        dest.writeString(this.doc_name);
        dest.writeString(this.price);
        dest.writeString(this.price_discount);
        dest.writeString(this.price_range_max);
        dest.writeString(this.id);
        dest.writeString(this._id);
        dest.writeString(this.showprice);
        dest.writeString(this.specialPrice);
        dest.writeString(this.show_hospital);
        dest.writeString(this.invitation);
        dest.writeString(this.lijian);
        dest.writeString(this.baoxian);
        dest.writeParcelable((Parcelable) this.insure, flags);
        dest.writeString(this.img66);
        dest.writeString(this.repayment);
        dest.writeString(this.hos_red_packet);
        dest.writeString(this.mingyi);
        dest.writeString(this.hot);
        dest.writeString(this.newp);
        dest.writeString(this.shixiao);
        dest.writeString(this.extension_user);
        dest.writeString(this.postStr);
        dest.writeString(this.depreciate);
        dest.writeString(this.rate);
        dest.writeString(this.feeScale);
        dest.writeString(this.is_fanxian);
        dest.writeString(this.is_rongyun);
        dest.writeString(this.hos_userid);
        dest.writeString(this.url);
        dest.writeString(this.hospital_id);
        dest.writeString(this.doctor_id);
    }

    public PhotoBrowsListTaoData() {
    }

    protected PhotoBrowsListTaoData(Parcel in) {
        this.is_show_member = in.readString();
        this.pay_dingjin = in.readString();
        this.number = in.readString();
        this.start_number = in.readString();
        this.pay_price_discount = in.readString();
        this.member_price = in.readString();
        this.m_list_logo = in.readString();
        this.bmsid = in.readString();
        this.seckilling = in.readString();
        this.img = in.readString();
        this.title = in.readString();
        this.subtitle = in.readString();
        this.hos_name = in.readString();
        this.doc_name = in.readString();
        this.price = in.readString();
        this.price_discount = in.readString();
        this.price_range_max = in.readString();
        this.id = in.readString();
        this._id = in.readString();
        this.showprice = in.readString();
        this.specialPrice = in.readString();
        this.show_hospital = in.readString();
        this.invitation = in.readString();
        this.lijian = in.readString();
        this.baoxian = in.readString();
        this.insure = in.readParcelable(InsureBean.class.getClassLoader());
        this.img66 = in.readString();
        this.repayment = in.readString();
        this.hos_red_packet = in.readString();
        this.mingyi = in.readString();
        this.hot = in.readString();
        this.newp = in.readString();
        this.shixiao = in.readString();
        this.extension_user = in.readString();
        this.postStr = in.readString();
        this.depreciate = in.readString();
        this.rate = in.readString();
        this.feeScale = in.readString();
        this.is_fanxian = in.readString();
        this.is_rongyun = in.readString();
        this.hos_userid = in.readString();
        this.url = in.readString();
        this.hospital_id = in.readString();
        this.doctor_id = in.readString();
    }

    public static final Creator<PhotoBrowsListTaoData> CREATOR = new Creator<PhotoBrowsListTaoData>() {
        @Override
        public PhotoBrowsListTaoData createFromParcel(Parcel source) {
            return new PhotoBrowsListTaoData(source);
        }

        @Override
        public PhotoBrowsListTaoData[] newArray(int size) {
            return new PhotoBrowsListTaoData[size];
        }
    };
}
