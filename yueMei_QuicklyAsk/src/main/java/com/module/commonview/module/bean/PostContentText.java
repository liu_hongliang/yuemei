package com.module.commonview.module.bean;

/**
 * Created by 裴成浩 on 2018/8/15.
 */
public class PostContentText{

    private String content;
    private String url;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
