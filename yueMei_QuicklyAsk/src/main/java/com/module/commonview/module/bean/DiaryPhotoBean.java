package com.module.commonview.module.bean;

public class DiaryPhotoBean {

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrev_id() {
        return prev_id;
    }

    public void setPrev_id(String prev_id) {
        this.prev_id = prev_id;
    }

    public String getNext_id() {
        return next_id;
    }

    public void setNext_id(String next_id) {
        this.next_id = next_id;
    }

    public String getTitleSku() {
        return titleSku;
    }

    public void setTitleSku(String titleSku) {
        this.titleSku = titleSku;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getUrlSku() {
        return urlSku;
    }

    public void setUrlSku(String urlSku) {
        this.urlSku = urlSku;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPic_id() {
        return pic_id;
    }

    public void setPic_id(String pic_id) {
        this.pic_id = pic_id;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    private String url;
    private String title;
    private String content;
    private String prev_id;
    private String next_id;
    private String titleSku;
    private String price_discount;
    private String urlSku;
    private String img;
    private String pic_id;
    private String images;
    private String width;
    private String height;
    private String weight;
    private String is_video;
    private String video_url;

    @Override
    public String toString() {
        return "DiaryPhotoBean{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", prev_id='" + prev_id + '\'' +
                ", next_id='" + next_id + '\'' +
                ", titleSku='" + titleSku + '\'' +
                ", price_discount='" + price_discount + '\'' +
                ", urlSku='" + urlSku + '\'' +
                ", img='" + img + '\'' +
                ", pic_id='" + pic_id + '\'' +
                ", images='" + images + '\'' +
                ", width='" + width + '\'' +
                ", height='" + height + '\'' +
                ", weight='" + weight + '\'' +
                ", is_video='" + is_video + '\'' +
                ", video_url='" + video_url + '\'' +
                '}';
    }
}
