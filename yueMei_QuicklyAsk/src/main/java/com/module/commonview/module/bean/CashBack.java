package com.module.commonview.module.bean;

/**
 * Created by dwb on 16/3/29.
 */
public class CashBack {

    private String code;
    private String message;
    private CashBackD data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CashBackD getData() {
        return data;
    }

    public void setData(CashBackD data) {
        this.data = data;
    }
}
