package com.module.commonview.module.bean;

import com.module.my.model.bean.ProjcetList;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/4
 */
public class TaoScreenTitleData implements Serializable {
    private String homepagecity;                     //
    private String sort;                            //综合
    private List<ProjcetList> kindStr;         //排序

    public String getHomepagecity() {
        return homepagecity;
    }

    public void setHomepagecity(String homepagecity) {
        this.homepagecity = homepagecity;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }


    public List<ProjcetList> getKindStr() {
        return kindStr;
    }

    public void setKindStr(List<ProjcetList> kindStr) {
        this.kindStr = kindStr;
    }
}
