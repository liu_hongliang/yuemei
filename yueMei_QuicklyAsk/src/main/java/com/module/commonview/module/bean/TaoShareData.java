package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class TaoShareData implements Parcelable {

	private String url;
	private String title;
	private String content;
	private String img_weibo;
	private String img_weix;
	private ShareWechat Wechat;
	private SharePictorial Pictorial;

	public TaoShareData(Parcel in){
		Pictorial = in.readParcelable(TaoShareData.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(Pictorial,flags);
	}

	public static final Parcelable.Creator<TaoShareData> CREATOR = new Parcelable.Creator<TaoShareData>() {
		public TaoShareData createFromParcel(Parcel in) {
			return new TaoShareData(in);
		}

		public TaoShareData[] newArray(int size) {
			return new TaoShareData[size];
		}
	};

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImg_weibo() {
		return img_weibo;
	}

	public void setImg_weibo(String img_weibo) {
		this.img_weibo = img_weibo;
	}

	public String getImg_weix() {
		return img_weix;
	}

	public void setImg_weix(String img_weix) {
		this.img_weix = img_weix;
	}

	public ShareWechat getWechat() {
		return Wechat;
	}

	public void setWechat(ShareWechat wechat) {
		Wechat = wechat;
	}

	public SharePictorial getPictorial() {
		return Pictorial;
	}

	public void setPictorial(SharePictorial pictorial) {
		Pictorial = pictorial;
	}

}
