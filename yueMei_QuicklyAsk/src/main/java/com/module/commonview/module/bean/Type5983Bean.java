package com.module.commonview.module.bean;

public class Type5983Bean {
    private String ztid5983 = "0";
    private String objType5983 = "0";
    private String objId="0";
    private String title5983 = "";
    private String img5983 = "";
    private String url = "";
    private String mYmClass = "";
    private String mYmId = "";



    public String getZtid5983() {
        return ztid5983;
    }

    public void setZtid5983(String ztid5983) {
        this.ztid5983 = ztid5983;
    }

    public String getObjType5983() {
        return objType5983;
    }

    public void setObjType5983(String objType5983) {
        this.objType5983 = objType5983;
    }

    public String getTitle5983() {
        return title5983;
    }

    public void setTitle5983(String title5983) {
        this.title5983 = title5983;
    }

    public String getImg5983() {
        return img5983;
    }

    public void setImg5983(String img5983) {
        this.img5983 = img5983;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getmYmClass() {
        return mYmClass;
    }

    public void setmYmClass(String mYmClass) {
        this.mYmClass = mYmClass;
    }

    public String getmYmId() {
        return mYmId;
    }

    public void setmYmId(String mYmId) {
        this.mYmId = mYmId;
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }
}
