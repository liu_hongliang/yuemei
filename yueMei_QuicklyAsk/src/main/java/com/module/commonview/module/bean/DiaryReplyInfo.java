package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyInfo implements Parcelable {

    /**
     * my_reply : []
     * hot_reply : [{"id":"593209","userdata":{"name":"奥嗨呦","obj_id":"83670","id":"83670","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/83670/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/08/36/70_avatar_120_120.jpg","talent":"0","lable":"2015-06-29"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"8","reply_num":"30","is_agree":"0","content":"有活动呀~哈哈","pic":[],"list":[{"id":"594893","userdata":{"name":"用户605896149782","obj_id":"156289","id":"156289","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/156289/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar5_120_120.jpg","talent":"0","lable":"2015-06-30"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"你团购过整形项目吗？","pic":[]},{"id":"596801","userdata":{"name":"奥嗨呦","obj_id":"83670","id":"83670","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/83670/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/08/36/70_avatar_120_120.jpg","talent":"0","lable":"2015-07-02"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"很早做过洗牙，还不错~","pic":[]},{"id":"741413","userdata":{"name":"闪烁","obj_id":"296305","id":"296305","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/296305/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar1_120_120.jpg","talent":"0","lable":"2015-10-02"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"回复\t\t用户605896149782：\t怎么团购啊","pic":[]},{"id":"861655","userdata":{"name":"O(∩_∩)O哈哈许多大地震动手术*,~","obj_id":"89798","id":"89798","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/89798/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar4_120_120.jpg","talent":"0","lable":"2016-01-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"[龇牙][大笑][鼓掌][亲亲]","pic":[]},{"id":"861659","userdata":{"name":"O(∩_∩)O哈哈许多大地震动手术*,~","obj_id":"89798","id":"89798","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/89798/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar4_120_120.jpg","talent":"0","lable":"2016-01-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"[得意][得意][黑脸][黑脸][黑脸][笑][笑][笑][惊呆][惊呆][羞涩]","pic":[]},{"id":"861665","userdata":{"name":"O(∩_∩)O哈哈许多大地震动手术*,~","obj_id":"89798","id":"89798","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/89798/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar4_120_120.jpg","talent":"0","lable":"2016-01-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"[可爱][可爱]","pic":[]},{"id":"946233","userdata":{"name":"用户352277206116","obj_id":"500163","id":"500163","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/500163/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/50/01/63_avatar_120_120.jpg","talent":"0","lable":"2016-04-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"还好不错","pic":[]},{"id":"946235","userdata":{"name":"用户352277206116","obj_id":"500163","id":"500163","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/500163/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/50/01/63_avatar_120_120.jpg","talent":"0","lable":"2016-04-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"O(∩_∩)O哈哈~:[棒棒哒]","pic":[]}]},{"id":"601733","userdata":{"name":"七夜哥哥","obj_id":"181267","id":"181267","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/181267/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar3_120_120.jpg","talent":"0","lable":"2015-07-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"7","reply_num":"3","is_agree":"0","content":"ky","pic":[],"list":[{"id":"662309","userdata":{"name":"小悦悦","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"2015-08-11"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"可以","pic":[]}]},{"id":"608755","userdata":{"name":"对不起我是个好人__","obj_id":"152059","id":"152059","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/152059/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/15/20/59_avatar_120_120.jpg","talent":"0","lable":"2015-07-09"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"6","reply_num":"5","is_agree":"0","content":"[大爱][大爱][大爱]好活动","pic":[],"list":[{"id":"620715","userdata":{"name":"女儿国","obj_id":"83618","id":"83618","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/83618/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/000/08/36/18_avatar_120_120.jpg","talent":"0","lable":"2015-07-16"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"有那么多骗子吗？有病吧","pic":[]},{"id":"784461","userdata":{"name":"╰☆莣メ誋こ","obj_id":"344301","id":"344301","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/344301/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar2_120_120.jpg","talent":"0","lable":"2015-11-01"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"不靠谱吗","pic":[]},{"id":"1066001","userdata":{"name":"鱼儿飞飞","obj_id":"672683","id":"672683","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/672683/","group_id":"1","avatar":"https://www.yuemei.com/images/weibo/noavatar4_120_120.jpg","talent":"0","lable":"2016-07-12"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"回复\t\t女儿国：\t怎么，亲做过项目吗？","pic":[]}]},{"id":"23066032","userdata":{"name":"","obj_id":"86359549","id":"86359549","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/86359549/","group_id":"","avatar":"https://p21.yuemei.com/avatar/086/35/95/49_avatar_120_120.jpg","talent":"0","lable":"2018-07-03"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"6","reply_num":"1","is_agree":"0","content":"[爱心][爱心][爱心]","pic":[],"list":[]}]
     * new_reply : [{"id":"49454433","userdata":{"name":"悦Mer_0250158570","obj_id":"87179236","id":"87179236","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/87179236/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/087/17/92/36_avatar_120_120.jpg","talent":"0","lable":"03-24"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"[俏皮]","pic":[]},{"id":"49447005","userdata":{"name":"百合莉莉","obj_id":"88174513","id":"88174513","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/88174513/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/088/17/45/13_avatar_120_120.jpg","talent":"0","lable":"03-05"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"好","pic":[]},{"id":"49443969","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"02-17"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"1","is_agree":"0","content":"由于疫情原因，暂未复工，2月份返现到账延迟，各位小可爱悉知~","pic":[],"list":[{"id":"49443973","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"02-17"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"2月份审核成功的小可爱已经具体私信通知了，需要修改的小可爱可以接着修改，及时私信小悦悦~","pic":[]}]},{"id":"49411744","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"01-19"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"1","reply_num":"0","is_agree":"0","content":"公布2019年12月返现名单：ID：悦Mer_8633107489，日记链接：/c/7848665.html，项目：韩式双眼皮，返现金额：100元。","pic":[]},{"id":"49410816","userdata":{"name":"周奉天","obj_id":"88632642","id":"88632642","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/88632642/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/088/63/26/42_avatar_120_120.jpg","talent":"0","lable":"01-19"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"厉害","pic":[]},{"id":"49327229","userdata":{"name":"AOdelia","obj_id":"88705643","id":"88705643","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/88705643/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/088/70/56/43_avatar_120_120.jpg","talent":"0","lable":"01-17"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"2","is_agree":"0","content":"为什么我2篇日志一篇评分没有4分，另一篇说达到返现标准却还没有返现？什么情况？请相关人员回复。","pic":[],"list":[{"id":"49411741","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"01-19"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"亲爱的~你的名下有两个日记本。其中一个日记本的一个评分是3分，另外一个是全5分。但是这个5分日记本写日记时间是12月份，目前为止更新了6篇。请仔细对照本帖的上面的要求哦~不合格的原因：1.非私密部位不能遮挡；2.其中3篇单帖日记的图片数没有达到要求；3.你的植发项目还没完全恢复哦~恢复期最快起码要一个多月哦~你补充了这些信息，点击我的头像再私信我哦~（づ￣3￣）づ╭❤～","pic":[]},{"id":"49411746","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"01-19"},"prev_user_info":{"name":"@小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":""},"prev_id":"49411741","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"小悦悦：可以在电脑端登陆悦美账号，进行修改日记哦~","pic":[]}]},{"id":"49254867","userdata":{"name":"AOdelia","obj_id":"88705643","id":"88705643","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/88705643/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/088/70/56/43_avatar_120_120.jpg","talent":"0","lable":"01-10"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"1","reply_num":"1","is_agree":"0","content":"评分4分以上是哪里看啊？誰评呢？我写的为啥没有4分？","pic":[],"list":[{"id":"49411730","userdata":{"name":"小悦悦（作者）","obj_id":"70278","id":"70278","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/70278/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/000/07/02/78_avatar_120_120.jpg","talent":"13","lable":"01-19"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"0","reply_num":"0","is_agree":"0","content":"1.日记评分：打开悦美APP\u2014\u2014底部\u201c我的\u201d\u2014\u2014头像下面\u201c日记\u201d\u2014\u2014点击你需查看的日记\u2014\u2014点击对比图下方的笔\u2014\u2014分级即星级。2.评分当然是下单用户自己评价的咯~3.亲爱的，看一下自己日记的星级","pic":[]}]},{"id":"49248761","userdata":{"name":"合肥白领安琪儿医院","obj_id":"12142","id":"85524004","obj_type":"3","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/hospital/12142/","group_id":"3","avatar":"https://p21.yuemei.com/avatar/085/52/40/04_avatar_120_120.jpg","talent":"13","lable":"01-07"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"2","reply_num":"0","is_agree":"0","content":"相信悦美的品牌，相信安琪儿的技术，携手悦美，一起美丽","pic":[]},{"id":"49134669","userdata":{"name":"周奉天","obj_id":"88632642","id":"88632642","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/88632642/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/088/63/26/42_avatar_120_120.jpg","talent":"0","lable":"2019-12-23"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"2","reply_num":"0","is_agree":"0","content":"很漂亮","pic":[]},{"id":"49082526","userdata":{"name":"悦Mer_0668541717","obj_id":"86557588","id":"86557588","obj_type":"6","doctor_id":"0","hospital_id":"0","hospital_name":"","city_name":"","title":"","url":"https://m.yuemei.com/u/86557588/","group_id":"1","avatar":"https://p21.yuemei.com/avatar/086/55/75/88_avatar_120_120.jpg","talent":"0","lable":"2019-12-09"},"prev_id":"0","tao_id":"0","set_tid":"0","reply_tid":"0","appendix":[],"agree_num":"1","reply_num":"0","is_agree":"0","content":"好看","pic":[]}]
     * new_reply_last_id : 49082526
     * reply_page_offect : 10
     */

    private String new_reply_last_id;
    private String reply_page_offect;
    private List<DiaryReplyList2> my_reply;
    private List<DiaryReplyList2> hot_reply;
    private List<DiaryReplyList2> new_reply;

    public String getNew_reply_last_id() {
        return new_reply_last_id;
    }

    public void setNew_reply_last_id(String new_reply_last_id) {
        this.new_reply_last_id = new_reply_last_id;
    }

    public String getReply_page_offect() {
        return reply_page_offect;
    }

    public void setReply_page_offect(String reply_page_offect) {
        this.reply_page_offect = reply_page_offect;
    }

    public List<DiaryReplyList2> getMy_reply() {
        return my_reply;
    }

    public void setMy_reply(List<DiaryReplyList2> my_reply) {
        this.my_reply = my_reply;
    }

    public List<DiaryReplyList2> getHot_reply() {
        return hot_reply;
    }

    public void setHot_reply(List<DiaryReplyList2> hot_reply) {
        this.hot_reply = hot_reply;
    }

    public List<DiaryReplyList2> getNew_reply() {
        return new_reply;
    }

    public void setNew_reply(List<DiaryReplyList2> new_reply) {
        this.new_reply = new_reply;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.new_reply_last_id);
        dest.writeString(this.reply_page_offect);
        dest.writeTypedList(this.my_reply);
        dest.writeTypedList(this.hot_reply);
        dest.writeTypedList(this.new_reply);
    }

    public DiaryReplyInfo() {
    }

    protected DiaryReplyInfo(Parcel in) {
        this.new_reply_last_id = in.readString();
        this.reply_page_offect = in.readString();
        this.my_reply = in.createTypedArrayList(DiaryReplyList2.CREATOR);
        this.hot_reply = in.createTypedArrayList(DiaryReplyList2.CREATOR);
        this.new_reply = in.createTypedArrayList(DiaryReplyList2.CREATOR);
    }

    public static final Parcelable.Creator<DiaryReplyInfo> CREATOR = new Parcelable.Creator<DiaryReplyInfo>() {
        @Override
        public DiaryReplyInfo createFromParcel(Parcel source) {
            return new DiaryReplyInfo(source);
        }

        @Override
        public DiaryReplyInfo[] newArray(int size) {
            return new DiaryReplyInfo[size];
        }
    };
}
