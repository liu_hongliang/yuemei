package com.module.commonview.module.bean;

public class ContinueToSend {
    private String classid;
    private String content;
    private String quick_reply;

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getQuick_reply() {
        return quick_reply;
    }

    public void setQuick_reply(String quick_reply) {
        this.quick_reply = quick_reply;
    }
}
