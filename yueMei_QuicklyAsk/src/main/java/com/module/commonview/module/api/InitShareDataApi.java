package com.module.commonview.module.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.BBsShareData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 日记分享数据
 * Created by Administrator on 2018/2/22.
 */

public class InitShareDataApi implements BaseCallBackApi {
    private String TAG = "InitShareDataApi";
    private HashMap<String, Object> hashMap;  //传值容器

    public InitShareDataApi() {
        hashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.SHARE, "post", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {

                listener.onSuccess(mData);

            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }

    public void addData(String key, String value) {
        hashMap.put(key, value);
    }
}
