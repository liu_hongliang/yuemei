package com.module.commonview.module.bean;

import android.text.TextUtils;

/**
 * Created by 裴成浩 on 2018/6/19.
 */
public class SumbitPhotoData {

    private String userid;
    private String _id;
    private String qid;
    private String cid;
    private String askorshare;
    private String visibility;
    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String get_id() {
        if(TextUtils.isEmpty(cid)){
            return "0";
        }else{
            return _id;
        }
    }

    public void set_id(String docid) {
        this._id = docid;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getCid() {
        if(TextUtils.isEmpty(cid)){
            return "0";
        }else{
            return cid;
        }
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }


    public String getVisibility() {
        if(TextUtils.isEmpty(visibility)){
            return "0";
        }else{
            return visibility;
        }
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
}
