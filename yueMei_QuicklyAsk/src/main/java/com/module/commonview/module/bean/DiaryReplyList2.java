package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/6/7.
 */
public class DiaryReplyList2 implements Parcelable {
    private String id;
    private DiaryReplyListUserdata userdata;
    private String agree_num;
    private String reply_num;
    private String content;
//    private DiaryReplyListTao tao;
    private List<DiaryReplyListPic> pic;
    private List<DiaryReplyListList> list;
    private String listNum = "0";
    private String set_tid;
    private String is_agree;
    //类型  0 我的  1 热门  2最新
    private String type;


    public DiaryReplyList2() {
    }

    public DiaryReplyList2(String id) {
        this.id = id;
    }

    protected DiaryReplyList2(Parcel in) {
        id = in.readString();
        userdata = in.readParcelable(DiaryReplyListUserdata.class.getClassLoader());
        agree_num = in.readString();
        reply_num = in.readString();
        content = in.readString();
//        tao = in.readParcelable(DiaryReplyListTao.class.getClassLoader());
        pic = in.createTypedArrayList(DiaryReplyListPic.CREATOR);
        list = in.createTypedArrayList(DiaryReplyListList.CREATOR);
        listNum = in.readString();
        set_tid = in.readString();
        is_agree = in.readString();
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeParcelable(userdata, flags);
        dest.writeString(agree_num);
        dest.writeString(reply_num);
        dest.writeString(content);
//        dest.writeParcelable(tao, flags);
        dest.writeTypedList(pic);
        dest.writeTypedList(list);
        dest.writeString(listNum);
        dest.writeString(set_tid);
        dest.writeString(is_agree);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryReplyList2> CREATOR = new Creator<DiaryReplyList2>() {
        @Override
        public DiaryReplyList2 createFromParcel(Parcel in) {
            return new DiaryReplyList2(in);
        }

        @Override
        public DiaryReplyList2[] newArray(int size) {
            return new DiaryReplyList2[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DiaryReplyListUserdata getUserdata() {
        return userdata;
    }

    public void setUserdata(DiaryReplyListUserdata userdata) {
        this.userdata = userdata;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public String getReply_num() {
        return reply_num;
    }

    public void setReply_num(String reply_num) {
        this.reply_num = reply_num;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

//    public DiaryReplyListTao getTao() {
//        return tao;
//    }
//
//    public void setTao(DiaryReplyListTao tao) {
//        this.tao = tao;
//    }

    public List<DiaryReplyListPic> getPic() {
        return pic;
    }

    public void setPic(List<DiaryReplyListPic> pic) {
        this.pic = pic;
    }

    public List<DiaryReplyListList> getList() {
        return list;
    }

    public void setList(List<DiaryReplyListList> list) {
        this.list = list;
    }

    public String getListNum() {
        return listNum;
    }

    public void setListNum(String listNum) {
        this.listNum = listNum;
    }

    public String getSet_tid() {
        return set_tid;
    }

    public void setSet_tid(String set_tid) {
        this.set_tid = set_tid;
    }

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }
}
