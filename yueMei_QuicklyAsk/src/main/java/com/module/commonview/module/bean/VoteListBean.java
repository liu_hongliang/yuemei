package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: VoteListBean
 * 创 建 人: 原成昊
 * 创建日期: 2019-12-31 17:39
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */


public class VoteListBean implements Parcelable {
    /**
     * id : 38
     * post_id : 7973548
     * vote_title : ggbnnnjj
     * vote_type : 2
     * option : [{"id":"278","vote_num":"0","tao_id":"96226"},{"id":"279","vote_num":"0","tao_id":"133351"},{"id":"280","vote_num":"0","tao_id":"173383"}]
     * is_vote_option : 0
     */

    private String id;
    private String post_id;
    private String vote_title;
    private String vote_type;
    private String is_vote_option;
    @SerializedName("class")
    private String classX;
    private List<OptionBean> option;
    private String total_vote_num;

    public String getTotal_vote_num() {
        return total_vote_num;
    }

    public void setTotal_vote_num(String total_vote_num) {
        this.total_vote_num = total_vote_num;
    }

    public String getClassX() {
        return classX;
    }

    public void setClassX(String classX) {
        this.classX = classX;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getVote_title() {
        return vote_title;
    }

    public void setVote_title(String vote_title) {
        this.vote_title = vote_title;
    }

    public String getVote_type() {
        return vote_type;
    }

    public void setVote_type(String vote_type) {
        this.vote_type = vote_type;
    }

    public String getIs_vote_option() {
        return is_vote_option;
    }

    public void setIs_vote_option(String is_vote_option) {
        this.is_vote_option = is_vote_option;
    }

    public List<VoteListBean.OptionBean> getOption() {
        return option;
    }

    public void setOption(List<VoteListBean.OptionBean> option) {
        this.option = option;
    }



    public static class OptionBean implements Parcelable {
        /**
         * id : 278
         * vote_num : 0
         * tao_id : 96226
         */

        private String id;
        private String vote_num;
        private String tao_id;
        private String vote_option_rate;
        private String is_vote_option;
        private String title;
        private VoteListBean.TaoBeanVote tao;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public VoteListBean.TaoBeanVote getTao() {
            return tao;
        }

        public void setTao(VoteListBean.TaoBeanVote tao) {
            this.tao = tao;
        }

        public String getVote_option_rate() {
            return vote_option_rate;
        }

        public void setVote_option_rate(String vote_option_rate) {
            this.vote_option_rate = vote_option_rate;
        }

        public String getIs_vote_option() {
            return is_vote_option;
        }

        public void setIs_vote_option(String is_vote_option) {
            this.is_vote_option = is_vote_option;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVote_num() {
            return vote_num;
        }

        public void setVote_num(String vote_num) {
            this.vote_num = vote_num;
        }

        public String getTao_id() {
            return tao_id;
        }

        public void setTao_id(String tao_id) {
            this.tao_id = tao_id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.vote_num);
            dest.writeString(this.tao_id);
            dest.writeString(this.vote_option_rate);
            dest.writeString(this.is_vote_option);
            dest.writeString(this.title);
            dest.writeParcelable(this.tao, flags);
        }

        public OptionBean() {
        }

        protected OptionBean(Parcel in) {
            this.id = in.readString();
            this.vote_num = in.readString();
            this.tao_id = in.readString();
            this.vote_option_rate = in.readString();
            this.is_vote_option = in.readString();
            this.title = in.readString();
            this.tao = in.readParcelable(TaoBeanVote.class.getClassLoader());
        }

        public static final Creator<OptionBean> CREATOR = new Creator<OptionBean>() {
            @Override
            public OptionBean createFromParcel(Parcel source) {
                return new OptionBean(source);
            }

            @Override
            public OptionBean[] newArray(int size) {
                return new OptionBean[size];
            }
        };
    }

    public static class TaoBeanVote implements Parcelable {
        private String id;
        private String sale_price;
        private String lable;
        private String title;
        private String list_cover_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSale_price() {
            return sale_price;
        }

        public void setSale_price(String sale_price) {
            this.sale_price = sale_price;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getList_cover_image() {
            return list_cover_image;
        }

        public void setList_cover_image(String list_cover_image) {
            this.list_cover_image = list_cover_image;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.sale_price);
            dest.writeString(this.lable);
            dest.writeString(this.title);
            dest.writeString(this.list_cover_image);
        }

        public TaoBeanVote() {
        }

        protected TaoBeanVote(Parcel in) {
            this.id = in.readString();
            this.sale_price = in.readString();
            this.lable = in.readString();
            this.title = in.readString();
            this.list_cover_image = in.readString();
        }

        public static final Creator<TaoBeanVote> CREATOR = new Creator<TaoBeanVote>() {
            @Override
            public TaoBeanVote createFromParcel(Parcel source) {
                return new TaoBeanVote(source);
            }

            @Override
            public TaoBeanVote[] newArray(int size) {
                return new TaoBeanVote[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.post_id);
        dest.writeString(this.vote_title);
        dest.writeString(this.vote_type);
        dest.writeString(this.is_vote_option);
        dest.writeList(this.option);
        dest.writeString(this.total_vote_num);
    }

    public VoteListBean() {
    }

    protected VoteListBean(Parcel in) {
        this.id = in.readString();
        this.post_id = in.readString();
        this.vote_title = in.readString();
        this.vote_type = in.readString();
        this.is_vote_option = in.readString();
        this.option = new ArrayList<OptionBean>();
        in.readList(this.option, OptionBean.class.getClassLoader());
        this.total_vote_num = in.readString();
    }

    public static final Parcelable.Creator<VoteListBean> CREATOR = new Parcelable.Creator<VoteListBean>() {
        @Override
        public VoteListBean createFromParcel(Parcel source) {
            return new VoteListBean(source);
        }

        @Override
        public VoteListBean[] newArray(int size) {
            return new VoteListBean[size];
        }
    };
}
