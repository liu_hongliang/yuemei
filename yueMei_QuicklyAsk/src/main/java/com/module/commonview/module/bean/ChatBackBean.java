package com.module.commonview.module.bean;

import java.util.HashMap;
import java.util.List;

public class ChatBackBean {

    /**
     * comparedTitle : 为你推荐擅长埋线提升的医生快来咨询
     * comparedDoctors : [{"doctorsUserID":"86780641","doctorsImg":"https://p21.yuemei.com/avatar/086/78/06/41_avatar_120_120.jpg","doctorsName":"陈仕文","doctorsTitle":"","doctorsTag":"口碑一级棒","hospitalName":"长沙梵童医疗美容","diary_pf":"5.0","sku_order_num":"95017","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/N0FDc0hvZjZXMjNXdlF5RGh5UDMrdz09/group_id/11710/uid/SllwYUlsTS9ZbURLbFVINkJRUFNFUT09/hos_id/12532/doctorID/86780641/labelID/8205/comparedDoctorID/84951425/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0"},"doctorsCenterUrl":"https://m.yuemei.com/dr/86780641/"},{"doctorsUserID":"84951425","doctorsImg":"https://p21.yuemei.com/avatar/084/95/14/25_avatar_120_120.jpg","doctorsName":"刘翔","doctorsTitle":"","doctorsTag":"手术榜冠军","hospitalName":"长沙美莱医疗美容医院","diary_pf":"5.0","sku_order_num":"2919","jumpUrl":"https://chat.yuemei.com/comparedchat/index/targetId/MEsxd0MrZjFVckhuK3I1TFhUZVovQT09/group_id/6298/uid/SllwYUlsTS9ZbURLbFVINkJRUFNFUT09/hos_id/7341/doctorID/84951425/labelID/8205/comparedDoctorID/86780641/","type":"6751","typeControlParams":{"isHide":"1","isRefresh":"0"},"doctorsCenterUrl":"https://m.yuemei.com/dr/84951425/"}]
     */

    private String comparedTitle;
    private List<ComparedDoctorsBean> comparedDoctors;

    public String getComparedTitle() {
        return comparedTitle;
    }

    public void setComparedTitle(String comparedTitle) {
        this.comparedTitle = comparedTitle;
    }

    public List<ComparedDoctorsBean> getComparedDoctors() {
        return comparedDoctors;
    }

    public void setComparedDoctors(List<ComparedDoctorsBean> comparedDoctors) {
        this.comparedDoctors = comparedDoctors;
    }

    public static class ComparedDoctorsBean {
        /**
         * doctorsUserID : 86780641
         * doctorsImg : https://p21.yuemei.com/avatar/086/78/06/41_avatar_120_120.jpg
         * doctorsName : 陈仕文
         * doctorsTitle :
         * doctorsTag : 口碑一级棒
         * hospitalName : 长沙梵童医疗美容
         * diary_pf : 5.0
         * sku_order_num : 95017
         * jumpUrl : https://chat.yuemei.com/comparedchat/index/targetId/N0FDc0hvZjZXMjNXdlF5RGh5UDMrdz09/group_id/11710/uid/SllwYUlsTS9ZbURLbFVINkJRUFNFUT09/hos_id/12532/doctorID/86780641/labelID/8205/comparedDoctorID/84951425/
         * type : 6751
         * typeControlParams : {"isHide":"1","isRefresh":"0"}
         * doctorsCenterUrl : https://m.yuemei.com/dr/86780641/
         */

        private String doctorsUserID;
        private String doctorsImg;
        private String doctorsName;
        private String doctorsTitle;
        private String doctorsTag;
        private String hospitalName;
        private String diary_pf;
        private String sku_order_num;
        private String jumpUrl;
        private String type;
        private TypeControlParamsBean typeControlParams;
        private String doctorsCenterUrl;
        private String doctorsTagID;
        private HashMap<String,String> event_params;

        public String getDoctorsUserID() {
            return doctorsUserID;
        }

        public void setDoctorsUserID(String doctorsUserID) {
            this.doctorsUserID = doctorsUserID;
        }

        public String getDoctorsImg() {
            return doctorsImg;
        }

        public void setDoctorsImg(String doctorsImg) {
            this.doctorsImg = doctorsImg;
        }

        public String getDoctorsName() {
            return doctorsName;
        }

        public void setDoctorsName(String doctorsName) {
            this.doctorsName = doctorsName;
        }

        public String getDoctorsTitle() {
            return doctorsTitle;
        }

        public void setDoctorsTitle(String doctorsTitle) {
            this.doctorsTitle = doctorsTitle;
        }

        public String getDoctorsTag() {
            return doctorsTag;
        }

        public void setDoctorsTag(String doctorsTag) {
            this.doctorsTag = doctorsTag;
        }

        public String getHospitalName() {
            return hospitalName;
        }

        public void setHospitalName(String hospitalName) {
            this.hospitalName = hospitalName;
        }

        public String getDiary_pf() {
            return diary_pf;
        }

        public void setDiary_pf(String diary_pf) {
            this.diary_pf = diary_pf;
        }

        public String getSku_order_num() {
            return sku_order_num;
        }

        public void setSku_order_num(String sku_order_num) {
            this.sku_order_num = sku_order_num;
        }

        public String getJumpUrl() {
            return jumpUrl;
        }

        public void setJumpUrl(String jumpUrl) {
            this.jumpUrl = jumpUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public TypeControlParamsBean getTypeControlParams() {
            return typeControlParams;
        }

        public void setTypeControlParams(TypeControlParamsBean typeControlParams) {
            this.typeControlParams = typeControlParams;
        }

        public String getDoctorsCenterUrl() {
            return doctorsCenterUrl;
        }

        public void setDoctorsCenterUrl(String doctorsCenterUrl) {
            this.doctorsCenterUrl = doctorsCenterUrl;
        }

        public String getDoctorsTagID() {
            return doctorsTagID;
        }

        public void setDoctorsTagID(String doctorsTagID) {
            this.doctorsTagID = doctorsTagID;
        }

        public HashMap<String, String> getEvent_params() {
            return event_params;
        }

        public void setEvent_params(HashMap<String, String> event_params) {
            this.event_params = event_params;
        }

        public static class TypeControlParamsBean {
            /**
             * isHide : 1
             * isRefresh : 0
             */

            private String isHide;
            private String isRefresh;

            public String getIsHide() {
                return isHide;
            }

            public void setIsHide(String isHide) {
                this.isHide = isHide;
            }

            public String getIsRefresh() {
                return isRefresh;
            }

            public void setIsRefresh(String isRefresh) {
                this.isRefresh = isRefresh;
            }
        }
    }
}
