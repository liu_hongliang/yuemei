package com.module.commonview.module.bean;

public class NotifycationBean {
    private String img;
    private String title;
    private String contnet;
    private String ismessage;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContnet() {
        return contnet;
    }

    public void setContnet(String contnet) {
        this.contnet = contnet;
    }

    public String getIsmessage() {
        return ismessage;
    }

    public void setIsmessage(String ismessage) {
        this.ismessage = ismessage;
    }
}
