package com.module.commonview.module.bean;

/**
 * 控制面板的类型
 * Created by 裴成浩 on 2018/5/16.
 */
public enum ButtomDialogEnum {
    WEIXIN,WEIXIN_CIRCLE,WEIXIN_FAVORITE,SINA,SMS,QQ,QZONE,HUABAO
}
