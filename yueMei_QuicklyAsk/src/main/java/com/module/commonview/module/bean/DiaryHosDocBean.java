package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2018/6/5.
 */
public class DiaryHosDocBean implements Parcelable {
    private String hospital_name;
    private String hosimg;
    private String hospital_id;
    private String address;
    private String is_rongyun;
    private String hos_userid;
    private String hospital_url;
    private String people;
    private String comment_bili;
    private String distance;
    private String title;
    private String doctor_url;
    private String doctor_name;
    private String doctor_id;
    private String business_district;
    private HashMap<String,String> event_params;

    protected DiaryHosDocBean(Parcel in) {
        hospital_name = in.readString();
        hosimg = in.readString();
        hospital_id = in.readString();
        address = in.readString();
        is_rongyun = in.readString();
        hos_userid = in.readString();
        hospital_url = in.readString();
        people = in.readString();
        comment_bili = in.readString();
        distance = in.readString();
        title = in.readString();
        doctor_url = in.readString();
        doctor_name = in.readString();
        doctor_id = in.readString();
        business_district = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(hospital_name);
        dest.writeString(hosimg);
        dest.writeString(hospital_id);
        dest.writeString(address);
        dest.writeString(is_rongyun);
        dest.writeString(hos_userid);
        dest.writeString(hospital_url);
        dest.writeString(people);
        dest.writeString(comment_bili);
        dest.writeString(distance);
        dest.writeString(title);
        dest.writeString(doctor_url);
        dest.writeString(doctor_name);
        dest.writeString(doctor_id);
        dest.writeString(business_district);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiaryHosDocBean> CREATOR = new Creator<DiaryHosDocBean>() {
        @Override
        public DiaryHosDocBean createFromParcel(Parcel in) {
            return new DiaryHosDocBean(in);
        }

        @Override
        public DiaryHosDocBean[] newArray(int size) {
            return new DiaryHosDocBean[size];
        }
    };

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getHosimg() {
        return hosimg;
    }

    public void setHosimg(String hosimg) {
        this.hosimg = hosimg;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIs_rongyun() {
        return is_rongyun;
    }

    public void setIs_rongyun(String is_rongyun) {
        this.is_rongyun = is_rongyun;
    }

    public String getHos_userid() {
        return hos_userid;
    }

    public void setHos_userid(String hos_userid) {
        this.hos_userid = hos_userid;
    }

    public String getHospital_url() {
        return hospital_url;
    }

    public void setHospital_url(String hospital_url) {
        this.hospital_url = hospital_url;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getComment_bili() {
        return comment_bili;
    }

    public void setComment_bili(String comment_bili) {
        this.comment_bili = comment_bili;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDoctor_url() {
        return doctor_url;
    }

    public void setDoctor_url(String doctor_url) {
        this.doctor_url = doctor_url;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getBusiness_district() {
        return business_district;
    }

    public void setBusiness_district(String business_district) {
        this.business_district = business_district;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
