package com.module.commonview.module.bean;

/**
 * 日记本页面要删除的数据参数
 * Created by 裴成浩 on 2018/6/27.
 */
public class DiariesDeleteData {
    private String id;                          //要删除的id
    private int commentsOrReply;                //0:要删除的是主贴回复，1:评论回复
    private String replystr;                    //0删除日记 1删除是评论删除
    private int pos = -1;                       //删除的评论下标,删除日记的下标。
    private int posPos = -1;                    //删除的评论下的回复下标，删除日记不存在是-1
    private boolean isZhuTie = false;           //删除的是否是主贴，默认不是
    private boolean isDiaries = true;           //删除的是否是日记，默认是日记

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCommentsOrReply() {
        return commentsOrReply;
    }

    public void setCommentsOrReply(int commentsOrReply) {
        this.commentsOrReply = commentsOrReply;
    }

    public String getReplystr() {
        return replystr;
    }

    public void setReplystr(String replystr) {
        this.replystr = replystr;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPosPos() {
        return posPos;
    }

    public void setPosPos(int posPos) {
        this.posPos = posPos;
    }

    public boolean isZhuTie() {
        return isZhuTie;
    }

    public void setZhuTie(boolean zhuTie) {
        isZhuTie = zhuTie;
    }

    public boolean isDiaries() {
        return isDiaries;
    }

    public void setDiaries(boolean diaries) {
        isDiaries = diaries;
    }
}
