package com.module.commonview.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/5/15
 */
public class WorkTime implements Parcelable {
    private String start;       //上班时间
    private String end;         //下班时间

    protected WorkTime(Parcel in) {
        start = in.readString();
        end = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(start);
        dest.writeString(end);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WorkTime> CREATOR = new Creator<WorkTime>() {
        @Override
        public WorkTime createFromParcel(Parcel in) {
            return new WorkTime(in);
        }

        @Override
        public WorkTime[] newArray(int size) {
            return new WorkTime[size];
        }
    };

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
