/**
 * 
 */
package com.module.commonview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.EveryoneChatApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 百科相关日记
 * 
 * @author Robin
 * 
 */
public class BaikeDailyListFragment extends ListFragment {

	private final String TAG = "BaikeDailyListFragment";

	private Activity mCotext;

	public LoadingProgress mDalog;

	// List
	private DropDownListView mlist;
	private int mCurPage = 1;
	private Handler mHandler;

	private List<BBsListData550> lvBBslistData = new ArrayList<>();
	private List<BBsListData550> lvBBslistMoreData = new ArrayList<>();
	private BBsListAdapter bbsListAdapter;

	private String partid = " ";

	private LinearLayout nodataTv;
	private EveryoneChatApi everyoneChatApi;
	private HashMap<String, Object> everyoneChatMap = new HashMap<>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_list_baike, container,
				false);
		nodataTv =  v.findViewById(R.id.my_collect_post_tv_nodata);

		if(isAdded()){
			partid = getArguments().getString("id");
		}

		mDalog = new LoadingProgress(getActivity());
		return v;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
		super.onActivityCreated(savedInstanceState);
		mCotext = getActivity();
		everyoneChatApi = new EveryoneChatApi();
		mlist = (DropDownListView) getListView();
		initList();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	void initList() {

		mHandler = getHandler();
		mDalog.startLoading();
		lodBBsListData550(true);

		mlist.setOnBottomListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lodBBsListData550(false);
			}
		});

		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adpter, View v, int pos,
					long arg3) {

				if(null!=lvBBslistData) {

					String url = lvBBslistData.get(pos).getUrl();
					if (url.length() > 0) {
						String qid = lvBBslistData.get(pos).getQ_id();
						String appmurl = lvBBslistData.get(pos).getAppmurl();
						WebUrlTypeUtil.getInstance(mCotext).urlToApp(appmurl, "0", "0");
					}
				}
			}
		});
	}

	void lodBBsListData550(final boolean isDonwn) {

		everyoneChatMap.put("type", "2");
		everyoneChatMap.put("id", partid);
		everyoneChatMap.put("page", mCurPage + "");
		everyoneChatApi.getCallBack(mCotext, everyoneChatMap, new BaseCallBackListener<List<BBsListData550>>() {
				@Override
				public void onSuccess(List<BBsListData550> docListDatas) {
					Message msg = null;
					if (isDonwn) {
						if (mCurPage == 1) {
							lvBBslistData = docListDatas;
							msg = mHandler.obtainMessage(1);
							msg.sendToTarget();
						}
					} else {
						mCurPage++;
						lvBBslistMoreData = docListDatas;
						msg = mHandler.obtainMessage(2);
						msg.sendToTarget();
					}
				}
			});
	}

	@SuppressLint("HandlerLeak")
	private Handler getHandler() {
		return new Handler() {
			@SuppressLint({ "NewApi", "SimpleDateFormat" })
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null != lvBBslistData && lvBBslistData.size() > 0) {
						nodataTv.setVisibility(View.GONE);

						mDalog.stopLoading();

						if (null != getActivity()) {
							bbsListAdapter = new BBsListAdapter(getActivity(),
									lvBBslistData);

							setListAdapter(bbsListAdapter);
						}

						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"MM-dd HH:mm:ss");
						String updateAt = "";
						if(isAdded()){
							updateAt = getString(R.string.update_at);
						}
						mlist.onDropDownComplete(updateAt
								+ dateFormat.format(new Date()));
						mlist.onBottomComplete();
					} else {
						mDalog.stopLoading();
						nodataTv.setVisibility(View.VISIBLE);
						mlist.setVisibility(View.GONE);
					}
					break;
				case 2:
					if (null != lvBBslistMoreData
							&& lvBBslistMoreData.size() > 0) {
						bbsListAdapter.add(lvBBslistMoreData);
						bbsListAdapter.notifyDataSetChanged();
						mlist.onBottomComplete();
					} else {
						mlist.setHasMore(false);
						mlist.setShowFooterWhenNoMore(true);
						mlist.onBottomComplete();
					}
					break;
				}

			}
		};
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onPageStart("MainScreen"); // 统计页面
		StatService.onResume(getActivity());
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPageEnd("MainScreen");
		StatService.onPause(getActivity());
	}
}
