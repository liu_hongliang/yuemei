package com.module;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.YMBaseActivity;
import com.quicklyask.activity.R;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: TestActivity
 * 创 建 人: 原成昊
 * 创建日期: 2020-04-12 16:00
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class TestActivity extends YMBaseActivity {
    private RecyclerView recyclerView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    protected void initView() {
        recyclerView = findViewById(R.id.rv);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add("10000000" + i);
        }
        SeeMoreAdapter seeMoreAdapter = new SeeMoreAdapter(list);
        recyclerView.setLayoutManager(new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(seeMoreAdapter);

    }

    @Override
    protected void initData() {

    }

    class SeeMoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final static int TYPE_NORMAL = 0;//正常条目
        private final static int TYPE_SEE_MORE = 1;//查看更多(展开全部xx条回复)
        private final static int TYPE_SEE_MODE2 = 2; //展开全部
        private final static int TYPE_HIDE = 3;//收起 （已经到底了）
        private List<String> mList;
        private boolean mOpen1 = false;//是否是展开状态 超过两个
        private boolean mOpen2 = false;//是否是展开状态 超过六个


        SeeMoreAdapter(List<String> mList) {
            this.mList = mList;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            switch (i) {
                case TYPE_NORMAL:
                    return new SeeMoreViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_test_view, viewGroup, false));
                case TYPE_SEE_MORE:
                    return new SeeMoreViewHolder2(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comments_more, viewGroup, false));
                case TYPE_HIDE:
                    return new SeeMoreViewHolder3(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comments_more, viewGroup, false));
                case TYPE_SEE_MODE2:
                    return new SeeMoreViewHolder4(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comments_more, viewGroup, false));
                default:
                    return new SeeMoreViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_test_view, viewGroup, false));
            }
        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder instanceof SeeMoreViewHolder) {
                setView1((SeeMoreViewHolder) viewHolder, position);
            } else if (viewHolder instanceof SeeMoreViewHolder2) {
                setView2((SeeMoreViewHolder2) viewHolder, position);
            } else if (viewHolder instanceof SeeMoreViewHolder3) {
                setView3((SeeMoreViewHolder3) viewHolder, position);
            } else {
                setView4((SeeMoreViewHolder4) viewHolder, position);
            }
        }


        @Override
        public int getItemViewType(int position) {
            if (mList.size() <= 1) {
                return TYPE_NORMAL;
            } else if (mList.size() > 1 && mList.size() <= 6) {
                if (mOpen1) {
                    if (position == mList.size()) {
                        return TYPE_HIDE;
                    } else {
                        return TYPE_NORMAL;
                    }
                } else {
                    if (position == 1) {
                        return TYPE_SEE_MORE;
                    } else {
                        return TYPE_NORMAL;
                    }
                }
            } else {
                if (mOpen1) {
                    if (mOpen2) {
                        if (position == mList.size()) {
                            return TYPE_HIDE;
                        } else {
                            return TYPE_NORMAL;
                        }
                    } else {
                        if (position == 6) {
                            return TYPE_SEE_MODE2;
                        } else {
                            return TYPE_NORMAL;
                        }
                    }
                } else {
                    if (position == 1) {
                        return TYPE_SEE_MORE;
                    } else {
                        return TYPE_NORMAL;
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            if (mList == null || mList.size() == 0) {
                return 0;
            }
            if (mList.size() > 1) {
                if (mList.size() <= 6) {
                    if (mOpen1) {
                        return mList.size() + 1;
                    } else {
                        return 2;
                    }
                } else {
                    if (mOpen1) {
                        if (mOpen2) {
                            return mList.size() + 1;
                        } else {
                            return 7;
                        }
                    } else {
                        return 2;
                    }

                }
            } else {
                return mList.size();
            }
        }


        private void setView1(SeeMoreAdapter.SeeMoreViewHolder viewHolder, int position) {
            viewHolder.tv_title.setText(mList.get(position));
            viewHolder.tv_title.setClickable(false);
        }

        private void setView2(SeeMoreAdapter.SeeMoreViewHolder2 viewHolder, int position) {
            viewHolder.iv_down.setVisibility(View.VISIBLE);
            viewHolder.tv_more.setText("展开全部" + mList.size() + "条回复");
            viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOpen1 = true;
                    notifyDataSetChanged();
                }
            });
        }

        private void setView3(SeeMoreViewHolder3 viewHolder, int position) {
            viewHolder.iv_down.setVisibility(View.GONE);
            viewHolder.tv_more.setText("已经到底了");
//            viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mOpen1 = false;
//                    notifyDataSetChanged();
//                }
//            });
        }

        private void setView4(SeeMoreViewHolder4 viewHolder, int position) {
            viewHolder.iv_down.setVisibility(View.GONE);
            viewHolder.tv_more.setText("展开全部");
            viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOpen2 = true;
                    notifyDataSetChanged();
                }
            });
        }

        class SeeMoreViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title;

            public SeeMoreViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_title = itemView.findViewById(R.id.tv_title);
            }
        }

        class SeeMoreViewHolder2 extends RecyclerView.ViewHolder {

            TextView tv_more;
            ImageView iv_down;


            public SeeMoreViewHolder2(@NonNull View itemView) {
                super(itemView);
                tv_more = itemView.findViewById(R.id.tv_more);
                iv_down = itemView.findViewById(R.id.iv_down);
            }
        }

        class SeeMoreViewHolder3 extends RecyclerView.ViewHolder {

            TextView tv_more;
            ImageView iv_down;


            public SeeMoreViewHolder3(@NonNull View itemView) {
                super(itemView);
                tv_more = itemView.findViewById(R.id.tv_more);
                iv_down = itemView.findViewById(R.id.iv_down);
            }
        }

        class SeeMoreViewHolder4 extends RecyclerView.ViewHolder {

            TextView tv_more;
            ImageView iv_down;


            public SeeMoreViewHolder4(@NonNull View itemView) {
                super(itemView);
                tv_more = itemView.findViewById(R.id.tv_more);
                iv_down = itemView.findViewById(R.id.iv_down);
            }
        }

    }


}
