package com.module;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.RequiresApi;
import android.support.multidex.MultiDex;
import android.webkit.WebView;

import com.alibaba.sdk.android.AlibabaSDK;
import com.alibaba.sdk.android.Environment;
import com.alibaba.sdk.android.callback.InitResultCallback;
import com.alibaba.sdk.android.push.CloudPushService;
import com.alibaba.sdk.android.push.CommonCallback;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bun.miitmdid.core.IIdentifierListener;
import com.bun.miitmdid.core.JLibrary;
import com.bun.miitmdid.core.MdidSdkHelper;
import com.bun.miitmdid.supplier.IdSupplier;
import com.danikula.videocache.HttpProxyCacheServer;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.ishumei.smantifraud.SmAntiFraud;
import com.leon.channel.helper.ChannelReaderUtil;

import com.module.community.controller.other.YueMeiQtCallBack;
import com.module.community.other.MyFileNameGenerator;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.adapter.ImageAdapter;
import com.module.other.netWork.netWork.BuildConfig;
import com.module.other.netWork.netWork.FinalConstant1;
import com.q.Qt;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ImeiUtils;
import com.quicklyask.util.MD5Utils;
import com.quicklyask.util.Utils;
import com.taobao.weex.InitConfig;
import com.taobao.weex.WXSDKEngine;
import com.tendcloud.appcpa.TalkingDataAppCpa;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.utils.Log;

import org.xutils.x;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.jpush.android.api.JPushInterface;

import static com.quicklyask.util.DeviceIdUtils.getAndroidId;

//MultiDexApplication
public class MyApplication extends Application {
    private static Context mContext;
    private String TAG = "MyApplication";
    //当前在栈顶的activity地址值
    public static String activityAddress;
    private HttpProxyCacheServer proxy;
    private ExecutorService service;
    // 正常状态
    public static final int STATE_NORMAL = 0;
    // 从后台回到前台
    public static final int STATE_BACK_TO_FRONT = 1;
    // 从前台进入后台
    public static final int STATE_FRONT_TO_BACK = 2;
    // APP状态
    private static int sAppState = STATE_NORMAL;
    // 标记程序是否已进入后台(依据onStop回调)
    private boolean flag;
    // 标记程序是否已进入后台(依据onTrimMemory回调)
    private boolean background;

    /**
     * 获取全局上下文
     */
    public static Context getContext() {
        return mContext;
    }

    private static MyApplication mInstance;
    private Activity mAppActivity = null;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mInstance = this;

        //设置线程的优先级不予主线程抢资源
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        SDKInitializer.initialize(getApplicationContext());
        webviewSetPath(this);
        //自4.3.0起，百度地图SDK所有接口均支持百度坐标和国测局坐标，用此方法设置您使用的坐标类型.
        //包括BD09LL和GCJ02两种坐标，默认是BD09LL坐标。
        SDKInitializer.setCoordType(CoordType.BD09LL);
        startInitThreadPool();
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1) {
            getResources();
        }
        super.onConfigurationChanged(newConfig);
    }

    /**
     * 设置 app 字体不随系统字体设置改变
     */
    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        if (res != null) {
            Configuration config = res.getConfiguration();
            if (config != null && config.fontScale != 1.0f) {
                config.fontScale = 1.0f;
                res.updateConfiguration(config, res.getDisplayMetrics());
            }
        }
        return res;
    }

    private void startInitThreadPool() {
        service = Executors.newCachedThreadPool();
        for (int i = 0; i <= 11; i++) {
            service.execute(new WorksRunable(i));
        }
    }

    private void initWeex() {
        InitConfig.Builder builder = new InitConfig.Builder();
        builder.setImgAdapter(new ImageAdapter());
        InitConfig config = builder.build();
        WXSDKEngine.initialize(MyApplication.this, config);
        Log.e(TAG, "WXSDKEngine.isInitialized: " + WXSDKEngine.isInitialized());
    }

    private class WorksRunable implements Runnable {
        int position;

        public WorksRunable(int position) {
            this.position = position;
        }

        @Override
        public void run() {
            switch (position) {
                case 0:
                    //联网请求框架初始化
                    netWorkConfig();          //优化0.3秒左右
                    //捕捉错误日志
                    Thread.setDefaultUncaughtExceptionHandler(AppExceptionHandler.getInstance());
                    break;
                case 1:
                    //xUtils联网请求框架初始化
                    xUtilsConfig();
                    break;
                case 2:
                    //TCAgent统计初始化
                    TCAgentConfig(mContext);
                    break;
                case 3:
                    //友盟初始化
                    shareConfig();
                    break;
                case 4:
                    //fresco图片加载框架初始化
                    frescoConfig();
                    break;
                case 5:
                    //数美设备唯一标识初始化
                    initShuMei();
                    break;
                case 6:
                    //百度统计初始化
//                    initChannel();
//                    try {
//                        ApplicationInfo appInfo = MyApplication.getContext().getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
//                        Bundle bundle = appInfo.metaData;
//                        String baiduMobAdChannel = bundle.getString("BaiduMobAd_CHANNEL");
//                        String umengChannel = bundle.getString("UMENG_CHANNEL");
//                        String tdChannelId = bundle.getString("TD_CHANNEL_ID");
//                        android.util.Log.e(TAG,"baiduMobAdChannel ==>"+baiduMobAdChannel);
//                        android.util.Log.e(TAG,"umengChannel ==>"+umengChannel);
//                        android.util.Log.e(TAG,"tdChannelId ==>"+tdChannelId);
//                    } catch (PackageManager.NameNotFoundException e) {
//                        e.printStackTrace();
//                    }
                    StatService.autoTrace(mContext, true, false);
                    break;
                case 7:
                    //极光推送初始化
                    JPushInterface.init(MyApplication.this);  //优化0.1秒左右
                    break;
                case 8:
                    initMdidSdk();
                    break;
                default:
                    break;
            }

        }
    }

    private void initMdidSdk() {
        try {
            MdidSdkHelper.InitSdk(this, true, new IIdentifierListener() {
                @Override
                public void OnSupport(boolean isSupport, IdSupplier idSupplier) {
                    String deviceId;
                    if (isSupport) {
                        // 支持获取补充设备标识
                        deviceId = idSupplier.getOAID();
                    } else {
                        // 不支持获取补充设备标识
                        // 可以自己决定设备标识获取方案，这里直接使用了ANDROID_ID
                        deviceId =getAndroidId(MyApplication.this);
                    }
                    // 将设备标识MD5加密后返回，以获取统一格式
                    String digest = MD5Utils.digest(deviceId);
                    Log.e(TAG,"digest =="+digest);
                    Cfg.saveStr(MyApplication.this,"device_id",digest);
                    // 释放连接
                    if (null != idSupplier){
                        idSupplier.shutDown();
                    }
                }
            });
        }catch (Exception e){
            Log.e(TAG,"Exception =="+e.toString());
        }

    }




//Android P 以及之后版本不支持同时从多个进程使用具有相同数据目录的WebView
    //为其它进程webView设置目录

    @RequiresApi(api = 28)
    public void webviewSetPath(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            String processName = getProcessName(context);
            if (!"com.quicklyask.activity".equals(processName)) {//判断不等于默认进程名称
                WebView.setDataDirectorySuffix(processName);
            }
        }
    }

    public String getProcessName(Context context) {
        if (context == null) return null;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            if (processInfo.pid == android.os.Process.myPid()) {
                return processInfo.processName;
            }
        }
        return null;
    }


    private void initChannel() {
        try {
            String channel = ChannelReaderUtil.getChannel(getApplicationContext());
            Log.e(TAG, "channel == " + channel);
            String strChannel = getResources().getString(R.string.marketv);
            Log.e(TAG, "strChannel == " + strChannel);
            ApplicationInfo appInfo = MyApplication.getContext().getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            if (channel == null){
                appInfo.metaData.putString("BaiduMobAd_CHANNEL",strChannel);
                appInfo.metaData.putString("UMENG_CHANNEL",strChannel);
                appInfo.metaData.putString("TD_CHANNEL_ID",strChannel);
            }else {
                appInfo.metaData.putString("BaiduMobAd_CHANNEL",channel);
                appInfo.metaData.putString("UMENG_CHANNEL",channel);
                appInfo.metaData.putString("TD_CHANNEL_ID",channel);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG,"NameNotFoundException");
        }
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public Activity getCurrentActivity() {
        return mAppActivity;
    }


    /**
     * 数美设备唯一标识初始化
     */
    private void initShuMei() {
//        organization   5eXABLtJiiFqTTVJw1aq
//        accessKey      RhY7WBlBeUdJyBKAMEKu

        // 如果 AndroidManifest.xml 中没有指定主进程名字，主进程名默认与 packagename 相同
        if (this.getPackageName().equals(getCurProcessName(this))) {
            SmAntiFraud.SmOption option = new SmAntiFraud.SmOption();
            String DEBUG_ORG = "5eXABLtJiiFqTTVJw1aq";// organization 代码 不要传 AccessKey
            option.setOrganization(DEBUG_ORG);
            String channel = ChannelReaderUtil.getChannel(getApplicationContext());
            String strChannel = getResources().getString(R.string.marketv);
            option.setChannel(channel == null ? strChannel : channel);//渠道代码
            // 可选的方式，deviceId 拉取成功的事件监听，异步方式
            SmAntiFraud.registerServerIdCallback(new SmAntiFraud.IServerSmidCallback() {
                @Override
                public void onSuccess(String s) {
                    Log.e(TAG, "onSuccess s " + s);
                }

                @Override
                public void onError(int i) {
                    Log.e(TAG, "onError i " + i);
                }
            });
            SmAntiFraud.create(this, option);
            // 注意！！获取 deviceId，这个接口在真正的注册或登录事件产生的地方调用。
            // create 后马上调用返回的是本地的 deviceId，
            // 本地 deviceId 和服务器同步需要一点时间。
            String deviceId = SmAntiFraud.getDeviceId();
        }
    }


    /**
     * 联网请求框架初始化
     */
    private void netWorkConfig() {

        //联网请求框架初始化
        BuildConfig.configNetWork(this);

        //联网框架的参数配置
        BuildConfig.configParameter();


    }


    /**
     * 视频缓存
     *
     * @param context
     * @return
     */
    public static HttpProxyCacheServer getProxy(Context context) {
        MyApplication app = (MyApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this)
                .maxCacheSize(1024 * 1024 * 200)       //200M内存自动释放
                .fileNameGenerator(new MyFileNameGenerator())
                .build();
    }

    /**
     * 友盟初始化
     */
    private void shareConfig() {
        Config.DEBUG = true;
        UMShareAPI.get(MyApplication.this);
        PlatformConfig.setWeixin("wx953d482099727deb", "ea236d79fe6e515e688cb1f4f1d36091");
        PlatformConfig.setSinaWeibo("3499099852", "526dae50fd9640037c6b35b8e9efacd1", "http://www.yuemei.com");
        PlatformConfig.setQQZone("1103359963", "yk1cNhXH5tjHJeDC");
//				Config.REDIRECT_URL = "您新浪后台的回调地址";
    }

    /**
     * 阿里推送初始化
     */
    private void aliOneConfig() {
        //initOneSDK(mContext);
    }

    /**
     * TCAgent统计初始化
     *
     * @param mContext
     */
    private void TCAgentConfig(Context mContext) {
        TCAgent.init(MyApplication.this);
        TCAgent.setReportUncaughtExceptions(true);
        String channel = ChannelReaderUtil.getChannel(getApplicationContext());
        String strChannel = mContext.getResources().getString(R.string.marketv);
        TalkingDataAppCpa.init(MyApplication.this, "278A6B3A649F4C06AE8900AAB1780A90", channel == null ? strChannel : channel);
    }

    /**
     * fresco图片加载框架初始化
     */
    private void frescoConfig() {
        Fresco.initialize(MyApplication.this);
    }

    /**
     * xUtils初始化
     */
    private void xUtilsConfig() {
        x.Ext.init(MyApplication.this);
    }


    /**
     * 取得当前进程名
     *
     * @param context
     * @return
     */
    public static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        JLibrary.InitEntry(base);
    }

    /**
     * 初始化AlibabaSDK
     *
     * @param applicationContext
     */
    private void initOneSDK(final Context applicationContext) {
        AlibabaSDK.setEnvironment(Environment.ONLINE);
        AlibabaSDK.asyncInit(applicationContext, new InitResultCallback() {

            @Override
            public void onSuccess() {
                Log.d("AAAAAAA", "init onesdk success");
                //alibabaSDK初始化成功后，初始化云推送通道
                initCloudChannel(applicationContext);
            }

            @Override
            public void onFailure(int code, String message) {
                Log.e("AAAAAAAA", "init onesdk failed : " + message);
            }
        });
    }

    /**
     * 初始化云推送通道
     *
     * @param applicationContext
     */
    private void initCloudChannel(Context applicationContext) {
        CloudPushService pushService = AlibabaSDK.getService(CloudPushService.class);
        pushService.register(applicationContext, new CommonCallback() {
            @Override
            public void onSuccess() {
                Log.e("AAAAAAAA", "init cloudchannel success");

            }

            @Override
            public void onFailed(String errorCode, String errorMessage) {
                Log.e("AAAAAAAAAAA", "init cloudchannel failed === errorMessage:" + errorMessage + ",errorCode:" + errorCode);
            }
        });

    }

    /**
     * Activity 生命周期监听，用于监控app前后台状态切换
     */
    ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            activityAddress = activity.toString();
        }

        @Override
        public void onActivityStarted(Activity activity) {


        }

        @Override
        public void onActivityResumed(Activity activity) {
            mAppActivity = activity;
            Log.e("initGlobeActivity", "onActivityResumed--->" + activity.toString());
            if (background || flag) {
                background = false;
                flag = false;
                sAppState = STATE_BACK_TO_FRONT;
                Log.e(TAG, "onResume: STATE_BACK_TO_FRONT");
                Utils.getInfo(activity);

            } else {
                sAppState = STATE_NORMAL;
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {

            //判断当前activity是否处于前台
            if (!Utils.isCurAppTop(activity)) {
                // 从前台进入后台
                sAppState = STATE_FRONT_TO_BACK;
                flag = true;
                Log.e(TAG, "onStop: " + "STATE_FRONT_TO_BACK");
            } else {
                // 否则是正常状态
                sAppState = STATE_NORMAL;
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }
    };


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        // TRIM_MEMORY_UI_HIDDEN是UI不可见的回调, 通常程序进入后台后都会触发此回调,大部分手机多是回调这个参数
        // TRIM_MEMORY_BACKGROUND也是程序进入后台的回调, 不同厂商不太一样, 魅族手机就是回调这个参数
        if (level == Application.TRIM_MEMORY_UI_HIDDEN || level == TRIM_MEMORY_BACKGROUND) {
            background = true;
        } else if (level == Application.TRIM_MEMORY_COMPLETE) {
            background = !Utils.isCurAppTop(this);
        }

        if (background) {
            sAppState = STATE_FRONT_TO_BACK;
            Log.e(TAG, "onTrimMemory: " + "STATE_FRONT_TO_BACK");
        } else {
            sAppState = STATE_NORMAL;
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(mContext).clearMemory();
    }
}