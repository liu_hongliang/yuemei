package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2018/7/17.
 */
public class CommunityStaggeredNoticeData {
    private String lable;
    private String title;
    private String url;
    private String img;

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
