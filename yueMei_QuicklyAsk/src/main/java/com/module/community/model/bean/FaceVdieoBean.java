package com.module.community.model.bean;

import java.util.Map;

public class FaceVdieoBean {
    private String faceVideoUrl;
    private String hitHospitalAccountId;
    private String channel_id;
    private Map<String, Object> params;

    public FaceVdieoBean() {
    }

    public FaceVdieoBean(String faceVideoUrl, String hitHospitalAccountId) {
        this.faceVideoUrl = faceVideoUrl;
        this.hitHospitalAccountId = hitHospitalAccountId;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getFaceVideoUrl() {
        return faceVideoUrl;
    }

    public void setFaceVideoUrl(String faceVideoUrl) {
        this.faceVideoUrl = faceVideoUrl;
    }

    public String getHitHospitalAccountId() {
        return hitHospitalAccountId;
    }

    public void setHitHospitalAccountId(String hitHospitalAccountId) {
        this.hitHospitalAccountId = hitHospitalAccountId;
    }
}
