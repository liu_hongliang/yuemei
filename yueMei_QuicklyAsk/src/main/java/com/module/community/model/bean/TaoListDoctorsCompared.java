package com.module.community.model.bean;


import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/3
 */
public class TaoListDoctorsCompared {
    private TaoListDoctorsEnum doctorsEnum;
    private String showSkuListPosition;
    private HashMap<String,String> changeOneEventParams;
    private HashMap<String,String> exposureEventParams;
    private List<TaoListDoctorsComparedData> doctorsList;

    public TaoListDoctorsEnum getDoctorsEnum() {
        return doctorsEnum;
    }

    public void setDoctorsEnum(TaoListDoctorsEnum doctorsEnum) {
        this.doctorsEnum = doctorsEnum;
    }

    public String getShowSkuListPosition() {
        return showSkuListPosition;
    }

    public void setShowSkuListPosition(String showSkuListPosition) {
        this.showSkuListPosition = showSkuListPosition;
    }

    public HashMap<String, String> getChangeOneEventParams() {
        return changeOneEventParams;
    }

    public void setChangeOneEventParams(HashMap<String, String> changeOneEventParams) {
        this.changeOneEventParams = changeOneEventParams;
    }

    public HashMap<String, String> getExposureEventParams() {
        return exposureEventParams;
    }

    public void setExposureEventParams(HashMap<String, String> exposureEventParams) {
        this.exposureEventParams = exposureEventParams;
    }

    public List<TaoListDoctorsComparedData> getDoctorsList() {
        return doctorsList;
    }

    public void setDoctorsList(List<TaoListDoctorsComparedData> doctorsList) {
        this.doctorsList = doctorsList;
    }
}
