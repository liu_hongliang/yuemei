package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/7/10.
 */
public class HomeCommunityTagData implements Parcelable {

    private int id;
    private String name;
    private int is_login;
    private int is_web;
    private int moban;
    private int is_hot;
    private String url;
    private String controller;
    private String action;
    private int tongjiid;
    private ArrayList<HomeCommunityTagTag> tag;

    public HomeCommunityTagData() {
    }

    protected HomeCommunityTagData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        is_login = in.readInt();
        is_web = in.readInt();
        moban = in.readInt();
        is_hot = in.readInt();
        url = in.readString();
        controller = in.readString();
        action = in.readString();
        tongjiid = in.readInt();
        tag = in.createTypedArrayList(HomeCommunityTagTag.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(is_login);
        dest.writeInt(is_web);
        dest.writeInt(moban);
        dest.writeInt(is_hot);
        dest.writeString(url);
        dest.writeString(controller);
        dest.writeString(action);
        dest.writeInt(tongjiid);
        dest.writeTypedList(tag);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeCommunityTagData> CREATOR = new Creator<HomeCommunityTagData>() {
        @Override
        public HomeCommunityTagData createFromParcel(Parcel in) {
            return new HomeCommunityTagData(in);
        }

        @Override
        public HomeCommunityTagData[] newArray(int size) {
            return new HomeCommunityTagData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIs_login() {
        return is_login;
    }

    public void setIs_login(int is_login) {
        this.is_login = is_login;
    }

    public int getIs_web() {
        return is_web;
    }

    public void setIs_web(int is_web) {
        this.is_web = is_web;
    }

    public int getMoban() {
        return moban;
    }

    public void setMoban(int moban) {
        this.moban = moban;
    }

    public int getIs_hot() {
        return is_hot;
    }

    public void setIs_hot(int is_hot) {
        this.is_hot = is_hot;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getTongjiid() {
        return tongjiid;
    }

    public void setTongjiid(int tongjiid) {
        this.tongjiid = tongjiid;
    }

    public ArrayList<HomeCommunityTagTag> getTag() {
        return tag;
    }

    public void setTag(ArrayList<HomeCommunityTagTag> tag) {
        this.tag = tag;
    }
}
