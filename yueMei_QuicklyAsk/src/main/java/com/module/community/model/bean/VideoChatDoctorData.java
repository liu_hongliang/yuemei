package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class VideoChatDoctorData implements Parcelable {
    private String show_doctors_name;
    private String show_doctors_job;
    private String show_doctors_avatar;
    private String hospital_name;
    private String video_backdrop;
    private String show_doctors_job_key;

    public String getShow_doctors_name() {
        return show_doctors_name;
    }

    public void setShow_doctors_name(String show_doctors_name) {
        this.show_doctors_name = show_doctors_name;
    }

    public String getShow_doctors_job() {
        return show_doctors_job;
    }

    public void setShow_doctors_job(String show_doctors_job) {
        this.show_doctors_job = show_doctors_job;
    }

    public String getShow_doctors_avatar() {
        return show_doctors_avatar;
    }

    public void setShow_doctors_avatar(String show_doctors_avatar) {
        this.show_doctors_avatar = show_doctors_avatar;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getVideo_backdrop() {
        return video_backdrop;
    }

    public void setVideo_backdrop(String video_backdrop) {
        this.video_backdrop = video_backdrop;
    }

    public String getShow_doctors_job_key() {
        return show_doctors_job_key;
    }

    public void setShow_doctors_job_key(String show_doctors_job_key) {
        this.show_doctors_job_key = show_doctors_job_key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.show_doctors_name);
        dest.writeString(this.show_doctors_job);
        dest.writeString(this.show_doctors_avatar);
        dest.writeString(this.hospital_name);
        dest.writeString(this.video_backdrop);
        dest.writeString(this.show_doctors_job_key);
    }

    public VideoChatDoctorData() {
    }

    protected VideoChatDoctorData(Parcel in) {
        this.show_doctors_name = in.readString();
        this.show_doctors_job = in.readString();
        this.show_doctors_avatar = in.readString();
        this.hospital_name = in.readString();
        this.video_backdrop = in.readString();
        this.show_doctors_job_key = in.readString();
    }

    public static final Parcelable.Creator<VideoChatDoctorData> CREATOR = new Parcelable.Creator<VideoChatDoctorData>() {
        @Override
        public VideoChatDoctorData createFromParcel(Parcel source) {
            return new VideoChatDoctorData(source);
        }

        @Override
        public VideoChatDoctorData[] newArray(int size) {
            return new VideoChatDoctorData[size];
        }
    };
}
