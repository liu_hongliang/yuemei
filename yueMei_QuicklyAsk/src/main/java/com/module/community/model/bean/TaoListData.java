package com.module.community.model.bean;

import com.module.home.model.bean.RecommentTaoList;
import com.module.home.model.bean.SearchResultLike;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/3
 */
public class TaoListData {
    private TaoListType type;
    private TaoListDataType taoList;                    //列表数据
    private List<SearchResultLike> likeList;            //猜你喜欢数据
    private TaoListDoctors doctors;                     //医生对比数据
    private String recomendTips;                        //没有更多
    private RecommentTaoList recommentTaoList;

    public RecommentTaoList getRecommentTaoList() {
        return recommentTaoList;
    }

    public void setRecommentTaoList(RecommentTaoList recommentTaoList) {
        this.recommentTaoList = recommentTaoList;
    }

    public TaoListType getType() {
        return type;
    }

    public void setType(TaoListType type) {
        this.type = type;
    }

    public TaoListDataType getTaoList() {
        return taoList;
    }

    public void setTaoList(TaoListDataType taoList) {
        this.taoList = taoList;
    }

    public List<SearchResultLike> getLikeList() {
        return likeList;
    }

    public void setLikeList(List<SearchResultLike> likeList) {
        this.likeList = likeList;
    }

    public TaoListDoctors getDoctors() {
        return doctors;
    }

    public void setDoctors(TaoListDoctors doctors) {
        this.doctors = doctors;
    }

    public String getRecomendTips() {
        return recomendTips;
    }

    public void setRecomendTips(String recomendTips) {
        this.recomendTips = recomendTips;
    }
}
