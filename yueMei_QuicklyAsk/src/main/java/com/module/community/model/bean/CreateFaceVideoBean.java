package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.VideoChatGetTokenBean;

public class CreateFaceVideoBean implements Parcelable {

    private VideoChatGetTokenBean token_data;
    private VideoChatDoctorData doctors_data;

    public VideoChatGetTokenBean getToken_data() {
        return token_data;
    }

    public void setToken_data(VideoChatGetTokenBean token_data) {
        this.token_data = token_data;
    }

    public VideoChatDoctorData getDoctors_data() {
        return doctors_data;
    }

    public void setDoctors_data(VideoChatDoctorData doctors_data) {
        this.doctors_data = doctors_data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.token_data, flags);
        dest.writeParcelable(this.doctors_data, flags);
    }

    public CreateFaceVideoBean() {
    }

    protected CreateFaceVideoBean(Parcel in) {
        this.token_data = in.readParcelable(VideoChatGetTokenBean.class.getClassLoader());
        this.doctors_data = in.readParcelable(VideoChatDoctorData.class.getClassLoader());
    }

    public static final Parcelable.Creator<CreateFaceVideoBean> CREATOR = new Parcelable.Creator<CreateFaceVideoBean>() {
        @Override
        public CreateFaceVideoBean createFromParcel(Parcel source) {
            return new CreateFaceVideoBean(source);
        }

        @Override
        public CreateFaceVideoBean[] newArray(int size) {
            return new CreateFaceVideoBean[size];
        }
    };
}
