package com.module.community.model.bean;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2018/7/10.
 */
public class HomeCommunityData {

    private String img;
    private String title;
    private ArrayList<HomeCommunityTagData> tag;
    private ArrayList<HomeCommunityPartsData> parts;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HomeCommunityTagData> getTag() {
        return tag;
    }

    public void setTag(ArrayList<HomeCommunityTagData> tag) {
        this.tag = tag;
    }

    public ArrayList<HomeCommunityPartsData> getParts() {
        return parts;
    }

    public void setParts(ArrayList<HomeCommunityPartsData> parts) {
        this.parts = parts;
    }
}
