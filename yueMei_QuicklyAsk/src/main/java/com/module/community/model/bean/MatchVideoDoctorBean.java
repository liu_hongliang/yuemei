package com.module.community.model.bean;

public class MatchVideoDoctorBean {
    /**
     * show_message : 未匹配到可面诊的医生
     * jump_url :
     * is_hit_doctors : 0
     */
    private String is_hit_doctors;//是否匹配到面诊医生
    private String show_message;//展示弹层信息
    private String chat_url;//私信type识别地址
    private String hit_doctors_info_url;//匹配到医生信息展示弹层type识别地址
    private String face_video_url;//匹配到医生信息展示弹层type识别地址


    public String getIs_hit_doctors() {
        return is_hit_doctors;
    }

    public void setIs_hit_doctors(String is_hit_doctors) {
        this.is_hit_doctors = is_hit_doctors;
    }

    public String getShow_message() {
        return show_message;
    }

    public void setShow_message(String show_message) {
        this.show_message = show_message;
    }

    public String getChat_url() {
        return chat_url;
    }

    public void setChat_url(String chat_url) {
        this.chat_url = chat_url;
    }

    public String getHit_doctors_info_url() {
        return hit_doctors_info_url;
    }

    public void setHit_doctors_info_url(String hit_doctors_info_url) {
        this.hit_doctors_info_url = hit_doctors_info_url;
    }

    public String getFace_video_url() {
        return face_video_url;
    }

    public void setFace_video_url(String face_video_url) {
        this.face_video_url = face_video_url;
    }
}
