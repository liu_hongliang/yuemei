package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/8/23
 */
public class ExposureLoginData {
    private String referrer;
    private String referrer_id;

    public ExposureLoginData(String referrer, String referrer_id) {
        this.referrer = referrer;
        this.referrer_id = referrer_id;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getReferrer_id() {
        return referrer_id;
    }

    public void setReferrer_id(String referrer_id) {
        this.referrer_id = referrer_id;
    }
}
