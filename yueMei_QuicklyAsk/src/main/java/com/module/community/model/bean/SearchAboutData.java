/**
 * 
 */
package com.module.community.model.bean;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class SearchAboutData implements Parcelable {
	private String img;						//搜索图片
	private String num;						//	右侧显示文案
	private String keywords;				//	结果词
	private String high_keywords;			//	高亮显示词
	private HashMap<String,String> event_params;			//	统计埋点
	private DoctorData doctors_data;
	private HospitalData hospital_data;



	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getHigh_keywords() {
		return high_keywords;
	}

	public void setHigh_keywords(String high_keywords) {
		this.high_keywords = high_keywords;
	}

	public HashMap<String, String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String, String> event_params) {
		this.event_params = event_params;
	}

	public DoctorData getDoctors_data() {
		return doctors_data;
	}

	public void setDoctors_data(DoctorData doctors_data) {
		this.doctors_data = doctors_data;
	}

	public HospitalData getHospital_data() {
		return hospital_data;
	}

	public void setHospital_data(HospitalData hospital_data) {
		this.hospital_data = hospital_data;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.img);
		dest.writeString(this.num);
		dest.writeString(this.keywords);
		dest.writeString(this.high_keywords);
		dest.writeSerializable(this.event_params);
		dest.writeParcelable(this.doctors_data, flags);
		dest.writeParcelable(this.hospital_data, flags);
	}

	public SearchAboutData() {
	}

	protected SearchAboutData(Parcel in) {
		this.img = in.readString();
		this.num = in.readString();
		this.keywords = in.readString();
		this.high_keywords = in.readString();
		this.event_params = (HashMap<String, String>) in.readSerializable();
		this.doctors_data = in.readParcelable(DoctorData.class.getClassLoader());
		this.hospital_data = in.readParcelable(HospitalData.class.getClassLoader());
	}

	public static final Parcelable.Creator<SearchAboutData> CREATOR = new Parcelable.Creator<SearchAboutData>() {
		@Override
		public SearchAboutData createFromParcel(Parcel source) {
			return new SearchAboutData(source);
		}

		@Override
		public SearchAboutData[] newArray(int size) {
			return new SearchAboutData[size];
		}
	};
}
