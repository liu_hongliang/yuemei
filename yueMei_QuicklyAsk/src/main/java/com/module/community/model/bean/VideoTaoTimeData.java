package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/8/13
 */
public class VideoTaoTimeData {
    private String min;
    private String max;
    private String tao_id;

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getTao_id() {
        return tao_id;
    }

    public void setTao_id(String tao_id) {
        this.tao_id = tao_id;
    }
}
