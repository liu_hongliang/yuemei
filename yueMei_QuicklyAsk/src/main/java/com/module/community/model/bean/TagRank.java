package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class TagRank implements Parcelable {
    private String tag;
    private String ranking;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tag);
        dest.writeString(this.ranking);
    }

    public TagRank() {
    }

    protected TagRank(Parcel in) {
        this.tag = in.readString();
        this.ranking = in.readString();
    }

    public static final Parcelable.Creator<TagRank> CREATOR = new Parcelable.Creator<TagRank>() {
        @Override
        public TagRank createFromParcel(Parcel source) {
            return new TagRank(source);
        }

        @Override
        public TagRank[] newArray(int size) {
            return new TagRank[size];
        }
    };
}
