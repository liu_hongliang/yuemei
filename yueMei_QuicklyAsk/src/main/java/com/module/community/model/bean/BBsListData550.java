/**
 * 
 */
package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.module.home.model.bean.CashBackBean;
import com.module.home.model.bean.TaoBean;
import com.quicklyask.entity.HotPic;

import java.util.HashMap;
import java.util.List;

public class BBsListData550 implements Parcelable {
	private String bmsid;
	private String url;
	private List<HotPic> pic;
	private String _id;
	private String q_id;
	private String title;
	private String desc;
	private String user_id;
	private String time;
	private String user_img;
	private String talent;
	private String user_name;
	private String answer_num;
	private String askorshare;//404
	private List<BBsListTag> tag;
	private String price;
	private String set_tid;
	private String group_id;
	private String view_num;
	private String picRule;
	private String is_fanxian;
	private String is_video;
	private String appmurl;
	private TaoBean tao;
	private List<BBsListTag> pic3;
	private String agree_num;
	private String is_agree;
	private String after_day;
	private CashBackBean cashback;
	private String is_follow_user;
	private String rateSale;
	private HashMap<String,String> event_params;
	private String user_center_url;
	private	UserClickData userClickData;
	private Img310 img310;
	private String shareNum;
	private VideoBean video;
	private String last_update_time;
	private List<String> second_parts;
	private TagRank tag_rank;
	private HospitalDistance hospital_distance;

	public TagRank getTag_rank() {
		return tag_rank;
	}

	public void setTag_rank(TagRank tag_rank) {
		this.tag_rank = tag_rank;
	}

	public HospitalDistance getHospital_distance() {
		return hospital_distance;
	}

	public void setHospital_distance(HospitalDistance hospital_distance) {
		this.hospital_distance = hospital_distance;
	}

	public List<String> getSecond_parts() {
		return second_parts;
	}

	public void setSecond_parts(List<String> second_parts) {
		this.second_parts = second_parts;
	}

	public String getBmsid() {
		return bmsid;
	}

	public void setBmsid(String bmsid) {
		this.bmsid = bmsid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<HotPic> getPic() {
		return pic;
	}

	public void setPic(List<HotPic> pic) {
		this.pic = pic;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getQ_id() {
		return q_id;
	}

	public void setQ_id(String q_id) {
		this.q_id = q_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUser_img() {
		return user_img;
	}

	public void setUser_img(String user_img) {
		this.user_img = user_img;
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getAnswer_num() {
		return answer_num;
	}

	public void setAnswer_num(String answer_num) {
		this.answer_num = answer_num;
	}

	public String getAskorshare() {
		return askorshare;
	}

	public void setAskorshare(String askorshare) {
		this.askorshare = askorshare;
	}

	public List<BBsListTag> getTag() {
		return tag;
	}

	public void setTag(List<BBsListTag> tag) {
		this.tag = tag;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSet_tid() {
		return set_tid;
	}

	public void setSet_tid(String set_tid) {
		this.set_tid = set_tid;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getView_num() {
		return view_num;
	}

	public void setView_num(String view_num) {
		this.view_num = view_num;
	}

	public String getPicRule() {
		return picRule;
	}

	public void setPicRule(String picRule) {
		this.picRule = picRule;
	}

	public String getIs_fanxian() {
		return is_fanxian;
	}

	public void setIs_fanxian(String is_fanxian) {
		this.is_fanxian = is_fanxian;
	}

	public String getIs_video() {
		return is_video;
	}

	public void setIs_video(String is_video) {
		this.is_video = is_video;
	}

	public String getAppmurl() {
		return appmurl;
	}

	public void setAppmurl(String appmurl) {
		this.appmurl = appmurl;
	}

	public TaoBean getTao() {
		return tao;
	}

	public void setTao(TaoBean tao) {
		this.tao = tao;
	}

	public List<BBsListTag> getPic3() {
		return pic3;
	}

	public void setPic3(List<BBsListTag> pic3) {
		this.pic3 = pic3;
	}

	public String getAgree_num() {
		return agree_num;
	}

	public void setAgree_num(String agree_num) {
		this.agree_num = agree_num;
	}

	public String getIs_agree() {
		return is_agree;
	}

	public void setIs_agree(String is_agree) {
		this.is_agree = is_agree;
	}

	public String getAfter_day() {
		return after_day;
	}

	public void setAfter_day(String after_day) {
		this.after_day = after_day;
	}

	public CashBackBean getCashback() {
		return cashback;
	}

	public void setCashback(CashBackBean cashback) {
		this.cashback = cashback;
	}

	public String getIs_follow_user() {
		return is_follow_user;
	}

	public void setIs_follow_user(String is_follow_user) {
		this.is_follow_user = is_follow_user;
	}

	public String getRateSale() {
		return rateSale;
	}

	public void setRateSale(String rateSale) {
		this.rateSale = rateSale;
	}

	public HashMap<String,String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap event_params) {
		this.event_params = event_params;
	}

	public String getUser_center_url() {
		return user_center_url;
	}

	public void setUser_center_url(String user_center_url) {
		this.user_center_url = user_center_url;
	}

	public UserClickData getUserClickData() {
		return userClickData;
	}

	public void setUserClickData(UserClickData userClickData) {
		this.userClickData = userClickData;
	}

	public Img310 getImg310() {
		return img310;
	}

	public void setImg310(Img310 img310) {
		this.img310 = img310;
	}

	public String getShareNum() {
		return shareNum;
	}

	public void setShareNum(String shareNum) {
		this.shareNum = shareNum;
	}

	public VideoBean getVideo() {
		return video;
	}

	public void setVideo(VideoBean video) {
		this.video = video;
	}

	public String getLast_update_time() {
		return last_update_time;
	}

	public void setLast_update_time(String last_update_time) {
		this.last_update_time = last_update_time;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.bmsid);
		dest.writeString(this.url);
		dest.writeTypedList(this.pic);
		dest.writeString(this._id);
		dest.writeString(this.q_id);
		dest.writeString(this.title);
		dest.writeString(this.desc);
		dest.writeString(this.user_id);
		dest.writeString(this.time);
		dest.writeString(this.user_img);
		dest.writeString(this.talent);
		dest.writeString(this.user_name);
		dest.writeString(this.answer_num);
		dest.writeString(this.askorshare);
		dest.writeTypedList(this.tag);
		dest.writeString(this.price);
		dest.writeString(this.set_tid);
		dest.writeString(this.group_id);
		dest.writeString(this.view_num);
		dest.writeString(this.picRule);
		dest.writeString(this.is_fanxian);
		dest.writeString(this.is_video);
		dest.writeString(this.appmurl);
		dest.writeParcelable(this.tao, flags);
		dest.writeTypedList(this.pic3);
		dest.writeString(this.agree_num);
		dest.writeString(this.is_agree);
		dest.writeString(this.after_day);
		dest.writeParcelable(this.cashback, flags);
		dest.writeString(this.is_follow_user);
		dest.writeString(this.rateSale);
		dest.writeSerializable(this.event_params);
		dest.writeString(this.user_center_url);
		dest.writeParcelable(this.userClickData,flags);
		dest.writeStringList(this.second_parts);
		dest.writeParcelable(this.tag_rank,flags);
		dest.writeParcelable(this.hospital_distance,flags);
	}

	public BBsListData550() {
	}

	protected BBsListData550(Parcel in) {
		this.bmsid = in.readString();
		this.url = in.readString();
		this.pic = in.createTypedArrayList(HotPic.CREATOR);
		this._id = in.readString();
		this.q_id = in.readString();
		this.title = in.readString();
		this.desc = in.readString();
		this.user_id = in.readString();
		this.time = in.readString();
		this.user_img = in.readString();
		this.talent = in.readString();
		this.user_name = in.readString();
		this.answer_num = in.readString();
		this.askorshare = in.readString();
		this.tag = in.createTypedArrayList(BBsListTag.CREATOR);
		this.price = in.readString();
		this.set_tid = in.readString();
		this.group_id = in.readString();
		this.view_num = in.readString();
		this.picRule = in.readString();
		this.is_fanxian = in.readString();
		this.is_video = in.readString();
		this.appmurl = in.readString();
		this.tao = in.readParcelable(TaoBean.class.getClassLoader());
		this.pic3 = in.createTypedArrayList(BBsListTag.CREATOR);
		this.agree_num = in.readString();
		this.is_agree = in.readString();
		this.after_day = in.readString();
		this.cashback = in.readParcelable(CashBackBean.class.getClassLoader());
		this.is_follow_user = in.readString();
		this.rateSale = in.readString();
		this.event_params = (HashMap) in.readSerializable();
		this.user_center_url=in.readString();
		this.userClickData=in.readParcelable(UserClickData.class.getClassLoader());
		this.second_parts = in.createStringArrayList();
		this.tag_rank = in.readParcelable(TagRank.class.getClassLoader());
		this.hospital_distance = in.readParcelable(HospitalDistance.class.getClassLoader());
	}

	public static final Creator<BBsListData550> CREATOR = new Creator<BBsListData550>() {
		@Override
		public BBsListData550 createFromParcel(Parcel source) {
			return new BBsListData550(source);
		}

		@Override
		public BBsListData550[] newArray(int size) {
			return new BBsListData550[size];
		}
	};

}
