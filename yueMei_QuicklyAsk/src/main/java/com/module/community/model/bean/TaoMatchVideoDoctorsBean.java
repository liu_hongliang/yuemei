package com.module.community.model.bean;

public class TaoMatchVideoDoctorsBean {

    /**
     * show_message : 未匹配到可面诊的医生
     * jump_url :
     * is_hit_doctors : 0
     */

    private String show_message;//展示弹层信息
    private String jump_url;//私信type识别地址/筛选H5页type识别地址
    private String is_hit_doctors;//是否命中面诊医生

    public String getShow_message() {
        return show_message;
    }

    public void setShow_message(String show_message) {
        this.show_message = show_message;
    }

    public String getJump_url() {
        return jump_url;
    }

    public void setJump_url(String jump_url) {
        this.jump_url = jump_url;
    }

    public String getIs_hit_doctors() {
        return is_hit_doctors;
    }

    public void setIs_hit_doctors(String is_hit_doctors) {
        this.is_hit_doctors = is_hit_doctors;
    }
}
