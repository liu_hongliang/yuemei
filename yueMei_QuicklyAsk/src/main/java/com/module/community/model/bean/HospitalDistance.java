package com.module.community.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class HospitalDistance implements Parcelable {
    private String distance;
    private String business_district;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBusiness_district() {
        return business_district;
    }

    public void setBusiness_district(String business_district) {
        this.business_district = business_district;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.distance);
        dest.writeString(this.business_district);
    }

    public HospitalDistance() {
    }

    protected HospitalDistance(Parcel in) {
        this.distance = in.readString();
        this.business_district = in.readString();
    }

    public static final Parcelable.Creator<HospitalDistance> CREATOR = new Parcelable.Creator<HospitalDistance>() {
        @Override
        public HospitalDistance createFromParcel(Parcel source) {
            return new HospitalDistance(source);
        }

        @Override
        public HospitalDistance[] newArray(int size) {
            return new HospitalDistance[size];
        }
    };
}
