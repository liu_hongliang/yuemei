package com.module.community.model.bean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/12.
 */
public class CommunityStaggeredListData {

    private String user_img;
    private String user_name;
    private String id;
    private String url;
    private String title;
    private String is_video;
    private StaggeredImgData img;
    private String view_num;
    private List<BBsListTag> tag;
    private String share_num;
    private HashMap<String,String> event_params;

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public StaggeredImgData getImg() {
        return img;
    }

    public void setImg(StaggeredImgData img) {
        this.img = img;
    }

    public String getView_num() {
        return view_num;
    }

    public void setView_num(String view_num) {
        this.view_num = view_num;
    }

    public List<BBsListTag> getTag() {
        return tag;
    }

    public void setTag(List<BBsListTag> tag) {
        this.tag = tag;
    }

    public String getShare_num() {
        return share_num;
    }

    public void setShare_num(String share_num) {
        this.share_num = share_num;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String,String> event_params) {
        this.event_params = event_params;
    }
}
