package com.module.community.model.bean;

public class BBsShare {
	private String code;
	private String message;
	private BBsShareData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BBsShareData getData() {
		return data;
	}

	public void setData(BBsShareData data) {
		this.data = data;
	}

}
