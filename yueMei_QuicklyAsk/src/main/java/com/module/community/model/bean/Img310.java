package com.module.community.model.bean;

public class Img310 {

    /**
     * img : https://p31.yuemei.com/postimg/20190111/310_310/190111172141_5471a8.jpg
     * width :
     * height :
     * video_url :
     * is_video : 0
     */

    private String img;
    private String width;
    private String height;
    private String video_url;
    private String is_video;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }
}
