package com.module.community.model.bean;

/**
 * Created by 裴成浩 on 2019/7/16
 */
public class AccessToken {
    private String access_token;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
