package com.module.community.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 日历提醒弹层
 * Created by 裴成浩 on 2019/8/27
 */
public class CalendarReminderDialog extends Dialog {
    Context mContext;
    public CalendarReminderDialog(Context context) {
        super(context, R.style.mystyle);
        this.mContext= context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yueme_dialog);


        TextView mTip = findViewById(R.id.yuemei_tip);
        Button mLeftBtn = findViewById(R.id.yuemei_left);
        Button mRightBtn = findViewById(R.id.yuemei_right);
        mTip.setText(mContext.getResources().getString(R.string.calendar_txt));
        mLeftBtn.setText("不允许");
        mRightBtn.setText("好");

        //左侧点击
        mLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //右侧点击
        mRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick(v);
                }

            }
        });
    }

    private BtnClickListener mBtnClickListener;

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }

    public interface BtnClickListener {
        void rightBtnClick(View v);
    }
}
