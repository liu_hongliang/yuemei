package com.module.community.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.DiaryTaoData;
import com.module.community.controller.adapter.VideoListBottomDialogAdapter;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * 视频流底部弹出SKU
 * Created by 裴成浩 on 2019/8/14
 */
public class VideoListBottomDialog extends Dialog {

    private Context mContext;
    private List<HomeTaoData> mDiaryTaoDatas;
    private final int mWindowsHeight;

    public VideoListBottomDialog(Context context, List<HomeTaoData> diaryTaoDatas, int windowsHeight) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.mDiaryTaoDatas = diaryTaoDatas;
        this.mWindowsHeight = windowsHeight;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.video_list_bottom_sku_view, null, false);

        onClickListener(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);           //点击外部是否可以dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = mWindowsHeight / 5 * 3;
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        ImageView mClose = view.findViewById(R.id.video_list_bottom_close);
        RecyclerView buttomList = view.findViewById(R.id.video_list_bottom_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        VideoListBottomDialogAdapter buttomDialogAdapter = new VideoListBottomDialogAdapter(mContext, mDiaryTaoDatas);

        buttomList.setLayoutManager(layoutManager);
        buttomList.setAdapter(buttomDialogAdapter);

        //跳转设置
        buttomDialogAdapter.setOnItemClickListener(new VideoListBottomDialogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, HomeTaoData data) {
                itemCallBackListener.onItemClick(view, data);
                downDialog();
            }
        });

        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });
    }


    /**
     * 关闭
     */
    private void downDialog() {
        dismiss();
    }

    //item点击回调
    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, HomeTaoData data);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }

}
