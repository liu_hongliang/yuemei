package com.module.community.view;

import android.graphics.Paint;

/**
 * 自定义控件用到的封装方法
 *
 * @author 裴成浩
 * @data 2019/10/22
 */
public class CustomUtil {

    /**
     * 获取横向文本宽高
     *
     * @param paint
     * @param text
     * @return
     */
    public static float[] getTextSize(Paint paint, String text) {
        return getTextSize(paint, text, 1);
    }

    /**
     * 获取横向文本宽高多行
     *
     * @param paint
     * @param text
     * @param textLines
     * @return
     */
    public static float[] getTextSize(Paint paint, String text, int textLines) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        // top绝对值
        float top = Math.abs(fontMetrics.top);
        // bottom，正值
        float bottom = fontMetrics.bottom;
        // 文本高度
        float textHeight = top + bottom;
        // 文本总高度
        float textTotalHeight = textHeight * textLines;

        return new float[]{paint.measureText(text), textTotalHeight};
    }
}
