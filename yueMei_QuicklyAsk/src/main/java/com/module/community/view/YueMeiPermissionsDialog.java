package com.module.community.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 悦美权限说明dialog
 */
public class YueMeiPermissionsDialog extends Dialog {
    Context mContext;
    int mType;//1日历 2相机 3定位 4通话 5读写 6消息通知

    public YueMeiPermissionsDialog(Context context, int type) {
        super(context, R.style.mystyle);
        this.mContext = context;
        this.mType = type;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yueme_dialog);


        TextView mTip = findViewById(R.id.yuemei_tip);
        Button mLeftBtn = findViewById(R.id.yuemei_left);
        Button mRightBtn = findViewById(R.id.yuemei_right);
        switch (mType) {
            case 1:
                mTip.setText(mContext.getResources().getString(R.string.calendar_txt));
                break;
            case 2:
                mTip.setText(mContext.getResources().getString(R.string.camera_txt));
                break;
            case 3:
                mTip.setText(mContext.getResources().getString(R.string.location_txt));
                break;
            case 4:
                mTip.setText(mContext.getResources().getString(R.string.call_txt));
                break;
            case 5:
                mTip.setText(mContext.getResources().getString(R.string.storage_txt));
                break;
            case 6:
                mTip.setText(mContext.getResources().getString(R.string.notice_txt));
                break;
        }
        mLeftBtn.setText("不允许");
        mRightBtn.setText("好");
        //左侧点击
        mLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //右侧点击
        mRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBtnClickListener != null) {
                    mBtnClickListener.rightBtnClick(v);
                }

            }
        });
    }

    private BtnClickListener mBtnClickListener;

    public void setBtnClickListener(BtnClickListener btnClickListener) {
        mBtnClickListener = btnClickListener;
    }

    public interface BtnClickListener {
        void rightBtnClick(View v);
    }
}
