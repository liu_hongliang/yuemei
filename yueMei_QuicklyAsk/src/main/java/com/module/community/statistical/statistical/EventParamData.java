package com.module.community.statistical.statistical;

/**
 * Created by 裴成浩 on 2019/3/21
 */
public class EventParamData {
    private String event_name;                                                      //统计名称
    private String event_pos = "0";                                               //统计位置0
    private String event_others = "0";                                            //默认值0
    private String referrer = "0";                                                //页面来源,默认值0

    public EventParamData() {
    }

    public EventParamData(String event_name) {
        this.event_name = event_name;
    }

    public EventParamData(String event_name, String event_pos) {
        this.event_name = event_name;
        this.event_pos = event_pos;
    }

    public EventParamData(String event_name, String event_pos, String event_others) {
        this.event_name = event_name;
        this.event_pos = event_pos;
        this.event_others = event_others;
    }

    public EventParamData(String event_name, String event_pos, String event_others, String referrer) {
        this.event_name = event_name;
        this.event_pos = event_pos;
        this.event_others = event_others;
        this.referrer = referrer;
    }

    public String getEvent_name() {
        return event_name;
    }

    public String getEvent_pos() {
        return event_pos;
    }

    public void setEvent_pos(String event_pos) {
        this.event_pos = event_pos;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_others() {
        return event_others;
    }

    public void setEvent_others(String event_others) {
        this.event_others = event_others;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }
}
