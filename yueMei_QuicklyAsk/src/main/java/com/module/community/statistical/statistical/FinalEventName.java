package com.module.community.statistical.statistical;

/**
 * 统计名称
 * Created by 裴成浩 on 2019/3/21
 */
public class FinalEventName {
    //社区搜索
    public static final String BBS_SEARCH = "bbs_search";
    //发帖入口
    public static final String ASK_ENTRY = "ask_entry";
    //首页展开/折叠点击
    public static final String HOMELOOKDIARY = "homelookdiary";
    //首页tab签吸顶
    public static final String TAB_CEILING = "tab_ceiling";
    //首页下方日记列表的淘，按部位+位置统计，点击
    public static final String HOMESHARETAO = "homesharetao";
    //首页下方日记列表的医生医院，按部位+位置统计，点击
    public static final String HOMESHAREHOSDOC = "homesharehosdoc";
    //首页下方日记列表关注按钮，按部位+位置统计
    public static final String HOME_SHARELIST_USERFOLLOW = "home_sharelist_userfollow";
    //新安装消息弹窗【允许通知弹窗】显示的时候调用统计
    public static final String NEW_SHOW_ALERT = "new_show_alert";
    //消息列表消息弹窗【允许通知弹窗】显示的时候调用统计
    public static final String MESSAGE_SHOW_ALERT = "message_show_alert";
    //新人弹窗
    public static final String NEWPEOPLEALERT = "newpeoplealert";
    //app图片开屏跳过的点击
    public static final String START_JUMP = "start_jump";
    //我的页
    public static final String MINE_CLICK = "mine_click";
    //我的页 焦点图
    public static final String MY_HUANDENG = "my_huandeng";
    //我的页 淘列表
    public static final String MY_TAOLIST = "my_taolist";
    //搜索
    public static final String SEARCH = "search";
    //搜索 淘列表
    public static final String SEARCH_TAO = "search_tao";
    //搜索 淘列表推荐数据
    public static final String SEARCH_RECOMEND = "search_recomend";
    //热门话题列表点击
    public static final String BBS_HUATICLICK = "bbs_huaticlick";
    //首页右下角浮层
    public static final String NEWPEOPLE = "newpeople";
    //首页第一屏横滑卡片点击
    public static final String HOME_CARD = "home_card";
    //首页第一屏栏目点击2
    public static final String HOME_COLUMN2 = "home_column2";
    //频道页焦点图
    public static final String FOCUS_POSITION = "focus_position";
    //首页第一屏栏目点击3
    public static final String HOME_COLUMN3 = "home_column3";
    //首页新人专区
    public static final String HOME_NEWZT = "home_newzt";
    //频道页 医院专场metro点击
    public static final String CHANNEL_METRO = "channel_metro";
    //频道页 活动专场
    public static final String CHANNEL_3K = "channel_3k";
    //频道页 超值特卖
    public static final String CHANNEL_JINGTUI = "channel_jingtui";
    //频道页 教你避坑点击
    public static final String CHANNEL_BBS = "channel_bbs";
    //频道页 擅长医生推荐点击
    public static final String CHANNEL_DOCTOR = "channel_doctor";
    //频道页 擅长医院推荐点击
    public static final String CHANNEL_HOSPITAL = "channel_hospital";
    //频道页 商品列表点击
    public static final String BMS = "bms";
    //频道页-问题列表点击
    public static final String CHANNEL_ASKLIST_CLICK = "channel_asklist_click";
    //切换城市的点击
    public static final String TAO_CITY = "tao_city";
    //搜索框的点击
    public static final String TAO_SEARCH = "tao_search";
    //淘整形导航球的点击
    public static final String TAO_TAG = "tao_tag";
    //淘整形专题推广的点击
    public static final String TAO_ZT = "tao_zt";
    //淘整形专题推广SKU的点击
    public static final String TAO_ZT_TAO = "tao_zt_tao";
    //精选日记列表
    public static final String FEATURED_DIARY = "featured_diary";
    //社区首页滚动banner
    public static final String BBS = "bbs";
    //视频播放按钮点击
    public static final String VIDEOCLICK = "videoclick";
    //社区首页帖子列表按类型按位置点击
    public static final String BBSHOMECLICK = "bbshomeclick";
    //陶整形吸顶统计
    public static final String TAOTAB_CEILING = "taotab_ceiling";
    //登录发送验证码点击
    public static final String GETCODE = "getcode";
    //首页手工为
    public static final String HOME_METRO_TOP = "home_metro_top";
    //首页社区标题
    public static final String HOME_BBS = "home_bbs";
    //启动页
    public static final String START_CLICK = "start_click";
    //首页日记流的日记头像点击
    public static final String HOME_SHARELIST_USER_CLICK = "home_sharelist_user_click";
    //频道页日记头像点击
    public static final String CHANNEL_SHARELIST_USER_CLICK = "channel_sharelist_user_click";
    //搜索结果页日记头像点击
    public static final String SEARCH_SHARELIST_USER_CLICK = "search_sharelist_user_click";
    //精选日记头像点击
    public static final String FEATURED_SHARELIST_USER_CLICK = "featured_sharelist_user_click";
    //帖子详情页-标签
    public static final String POSTINFO_TAG_CLICK = "postinfo_tag_click";
    //百科相关日记点击
    public static final String BAIKE_SHARELIST_CLICK = "baike_sharelist_click";
    //百科常见问题点击
    public static final String BAIKE_ASKLIST_CLICK = "baike_asklist_click";
    //百科相关商品点击
    public static final String BAIKE_TAOLIST_CLICK = "baike_taolist_click";
    //百科相关医生点击
    public static final String BAIKE_DOCTORLIST_CLICK = "baike_doctorlist_click";
    //百科相关医院点击
    public static final String BAIKE_HOSPITALLIST_CLICK = "baike_hospitallist_click";
    //百科科普问答点击
    public static final String BAIKE_KEPULIST_CLICK = "baike_kepulist_click";
    //百科详情分享按钮点击
    public static final String BAIKE_SHARE_CLICK = "baike_share_click";
    //消息通知显示
    public static final String PUSH = "push";
    //视频播放页-评论点击
    public static final String VIDEO_TO_REPLY = "video_to_reply";
    //视频播放页-上滑加载上一个视频
    public static final String NEXT_VIDEO = "next_video";
    //视频播放页-下滑加载下一个视频
    public static final String BBS_VEDIO_SLIDING = "bbs_vedio_sliding";
    //视频播放页-播放
    public static final String VIDEO_BOFANG = "video_bofang";
    //视频播放页-暂停点击
    public static final String VIDEO_ZANTING = "video_zanting";
    //视频播放页-播放时长
    public static final String BOFANG_TIME = "bofang_time";
    //视频播放页-页面跳出（离开当前页面时记录）
    public static final String BOFANG_OUT = "bofang_out";
    //社区首页 - 小卡位所有位置
    public static final String BBS_ALL_CARD_POSITION = "bbs_all_card_position";
    //社区首页 - 焦点图位置
    public static final String BBS_FOCUS_IMAGE_POSITION = "bbs_focus_image_position";
    //社区首页 - 达人带你整手工位
    public static final String BBS_TALENT_PROMPT = "bbs_talent_prompt";
    //社区首页 - 变美不上当
    public static final String BBS_BECOME_BEAUTIFUL = "bbs_become_beautiful";
    //SKU详情页 - 吸底立即预定
    public static final String TAO_YUDING = "tao_yuding";
    //Push点击
    public static final String PUSH_FIVE_CLICK = "push_five_click";
    //对比咨询入口点击
    public static final String COMPARE_TO_CHAT = "compare_to_chat";
    //首页十个球
    public static final String HOME_NAV = "home_nav";
    //日记本详情-淘整型点击
    public static final String DIARY_TO_TAO = "diary_to_tao";
    //日记详情-淘整型点击
    public static final String SHAREINFO_TO_TAO = "shareinfo_to_tao";
    //相册页-吸顶sku点击
    public static final String ALBUM_TOP_TAO = "album_top_tao";
    //日记大图浏览页-sku点击
    public static final String BIGIMG_TAO = "bigimg_tao";
    //帖子-淘整型点击
    public static final String POSTINFO_TO_TAO = "postinfo_to_tao";
    //评论-淘点击
    public static final String REPLY_TO_TAO = "reply_to_tao";
    //淘详情购物车点击
    public static final String TAO_CART = "tao_cart";
    //淘整形分享点击
    public static final String TAO_SHARE = "tao_share";
    //淘整形收藏点击
    public static final String TAO_COLLECT = "tao_collect";
    //淘整形->首页点击
    public static final String TAO_TO_HOME = "tao_to_home";
    //点击开通PLUS
    public static final String TAO_TO_MEMBER = "tao_to_member";
    //查看花呗分期
    public static final String TAO_LOOK_REPAYMENT = "tao_look_repayment";
    //查看新手礼包
    public static final String TAO_LOOK_NEWCOUPONS = "tao_look_newcoupons";
    //查看医院礼包
    public static final String TAO_LOOK_HOSCOUPONS = "tao_look_hoscoupons";
    //点击大促引导
    public static final String TAO_LOOK_ZT = "tao_look_zt";
    //查看【已选】规格
    public static final String TAO_LOOK_REL = "tao_look_rel";
    //切换规格【event_params】
    public static final String TAOREL_TO_TAO = "taorel_to_tao";
    //查看【项目说明】
    public static final String TAO_LOOK_EXPLAIN = "tao_look_explain";
    //点击【医美百科】
    public static final String TAO_CLICK_BAIKE = "tao_click_baike";
    //查看口碑评价
    public static final String TAO_TO_DIARYLIST = "tao_to_diarylist";
    //查看口碑评价
    public static final String APPRAISAL_TO_DIARY = "appraisal_to_diary";
    //查看口碑评价
    public static final String TAO_TO_DIARY_NEW = "tao_to_diary_new";
    //点击问大家
    public static final String TAO_ASK = "tao_ask";
    //sku点击导航
    public static final String TAONAV_CLICK = "taonav_click";
    //搜索tao曝光
    public static final String SEARCH_BAOGUANG = "search_baoguang";
    //优惠券倒计时曝光
    public static final String HOMECOUPONS = "homecoupons";
    //登录按钮点击
    public static final String LOGINCLICK = "loginclick";
    //日记本详情页 - 时间由新到旧点击
    public static final String DIARY_TIME_SORT_CLICK = "diary_time_sort_click";
    //日记本详情页 - 返回按钮点击
    public static final String DIARY_BACK_CLICK = "diary_back_click";
    //视频播放页 - 用户头像点击
    public static final String VIDEO_USER_AVATAR_CLICK = "video_user_avatar_click";
    //视频播放页 - 点赞点击
    public static final String VIDEO_LIKE_CLICK = "video_like_click";
    //视频播放页-点击sku
    public static final String VIDEO_TO_TAO = "video_to_tao";
    //视频播放页 - 购物车列表SKU点击
    public static final String VIDEO_SHOPPING_CART_TAO_CLICK = "video_shopping_cart_tao_click";
    //视频播放页 - 购物车图标点击
    public static final String VIDEO_SHOPPING_CART_CLICK = "video_shopping_cart_click";
    //日记本详情页 - 用户头像点击
    public static final String DIARY_USER_AVATAR_CLICK = "diary_user_avatar_click";
    //日记本详情页 - 术前术后图片点击
    public static final String DIARY_COMPARED_IMG_CLICK = "diary_compared_img_click";
    //日记本详情页 - 医院主页点击
    public static final String DIARY_HOSPITAL_CLICK = "diary_hospital_click";
    //日记本详情页 - 医生主页点击
    public static final String DIARY_DOCTORS_CLICK = "diary_doctors_click";
    //日记本详情页 - 列表筛选标签点击(视频/精华)
    public static final String DIARY_LIST_TAG_CLICK = "diary_list_tag_click";
    //日记本详情页 - 吸底私信点击
    public static final String CHAT_HOSPITAL = "chat_hospital";
    //日记本详情页 - 内容图点击
    public static final String DIARY_CONTENT_IMG_CLICK = "diary_content_img_click";
    //日记本详情页 - 推荐内容点击
    public static final String DIARY_RECOMMEND_POST_CLICK = "diary_recommend_post_click";
    //对比咨询  -  换一换点击(频道页,淘整形列表,搜索结果页)【附加参数（changeOneEventParams）】
    public static final String COMPARED_CHANGE_ONE_CLICK = "compared_change_one_click";
    //视频播放页 - 点击帖子详情
    public static final String VIDEO_POST_INFO_CLICK = "video_post_info_click";
    //对比咨询  -  曝光
    public static final String COMPARE_BAOGUANG = "compare_baoguang";
    //绑定登录手机页 一键绑定按钮点击
    public static final String ONE_CLICK_BINDLOGINPHONE = "one_click_bindLoginPhone";
    //登录页 一键登录按钮点击
    public static final String ONE_CLICK_LOGIN = "one_click_login";
    //搜索热词推荐
    public static final String SEARCH_HOT = "search_hot";
    //搜索搜索
    public static final String RECENT_SEARCH = "Recent_search";
    //有效私信提升
    public static final String CHAT_QUICK_QUESTION_EXPOSURE = "chat_quick_question_exposure";
    //互动菜单  -  优惠券点击
    public static final String MENU_CHAT_COUPONS = "menu_chat_coupons";
    //互动菜单  -  点击加入购物车
    public static final String MENU_TAO_TO_SHOPCARD = "menu_tao_to_shopcard";
    //新首页-搜索热词点击
    public static final String INDEX_NEW_SEARCH = "index_new_search";
    //首页 - 日记流 - 淘整形
    public static final String HTMLHOME_TO_TAO = "htmlHome_to_tao";
    //首页 - 日记流 -  日记本
    public static final String HTMLHOME_TO_POST = "htmlHome_to_post";
    //首页 - 日记流 -  医生/医院
    public static final String HTMLHOME_TO_DOCTOR = "htmlHome_to_doctor";
    //私信任务弹层
    public static final String CHAT_ALERT = "chat_alert";
    //启动页面视频
    public static final String START_VIDEO_CLICK = "start_video_click";
    //频道页面购物车点击
    public static final String SHOPPING_CART_CLICK = "shopping_cart_click";
    //频道页 - 二级标签页面 - 点击顶部标签下拉箭头
    public static final String SURG_PART_DROP_DOWN = "surg_part_drop_down";
    //频道页 - SKU列表 - 筛选导航点击
    public static final String CHANNEL_TAO_FILTER = "channel_tao_filter";
    //新频道页面点击
    public static final String CHANNEL_SEARCH = "channel_search";
    //搜索面板 - 搜索框热词曝光
    public static final String SEARCH_HOT_WORD_EXPOSURE = "search_hot_word_exposure";
    //搜索面板 - 搜索框热词点击
    public static final String SEARCH_HOT_WORD_TO = "search_hot_word_to";
    //帖子底部文中提及点击
    public static final String TEXT_RELATED = "text_related";
    //帖子底部文中提及列表SKU点击
    public static final String TEXT_RELATED_TO_TAO = "text_related_to_tao";
    //底部问题点击
    public static final String POST_BOTTOM_THE_TEXT = "post_bottom_the_text";
    //视频播放完成后SKU弹层SKU点击
    public static final String VIDEO_END_PLAY_SKU = "video_end_play_sku";
    //SKU详情页视频播放点击
    public static final String SKU_INFO_VIDEO_PLAY = "sku_info_video_play";

    //  我的对比页{tao_list.event_params}
    //    我的对比页-淘整形详情
    public static final String TAO_PK_SELECT_TO_TAO = "tao_pk_select_to_tao";
    //  SKU详情页{tao_pk_event_params}
    //    我的对比页-淘整形详情
    public static final String TAO_TO_TAO_PK = "tao_to_tao_pk";
    // 购物车详情页{移动端自行拼装数据}
    //    我的对比页-淘整形详情
    public static final String CART_TO_TAO_PK = "cart_to_tao_pk";
    //    发起投票--发起按钮点击**（提问入口页面）**
    public static final String LAUNCH_ENTRANCE_VOTE_CLICK = "launch_entrance_vote_click";
    //    发起投票--发起按钮点击**（随聊发帖页面）**
    public static final String LAUNCH_POST_VOTE_CLICK = "launch_post_vote_click";
    //**发起投票--**关闭‘委托小悦悦’
    public static final String VOTE_PUSH_CLOSE = "vote_push_close";
    //    发起投票--SKU添加点击
    public static final String VOTE_TAO_ADD = "vote_tao_add";
    //    投票帖详情---去PK
    public static final String VOTE_TAO_PK = "vote_tao_pk";
    //    投票帖详情--SKU点击
    public static final String VOTE_TAO_CLICK = "vote_tao_click";
    //SKU详情页 - 搜索框点击【附加参数（video.event_params）】
    public static final String TAO_SEARCH_CLICK = "tao_search_click";
    //SKU详情页 - 足迹点击【附加参数（event_params）】
    public static final String TAO_BROWSE_TAO_CLICK = "tao_browse_tao_click";
    //SKU详情页 - 榜单点击【附加参数（hospital_top.event_params）】
    public static final String TAO_HOSPITAL_TOP_CLICK = "tao_hospital_top_click";
    //首页日记流-刷新按钮点击
    public static final String HOME_INDEX_REFRESH = "home_index_refresh";
    //点击支付明细
    public static final String PAYMENT_DETAIL_CLICK = "payment_detail_click";
    //购物车页 - 为你推荐点击
    public static final String CART_RECOMMEND_TAO = "cart_recommend_tao";
    //购物车页 - 推荐优惠券曝光
    public static final String CART_COUPONS_EXPOSURE = "cart_coupons_exposure";
    //城市 全国点击
    public static final String CITY_ENTIRE_COUNTRY = "city_entire_country";
    //最近访问
    public static final String CITY_RECENT = "city_recent";
    //城市列表
    public static final String CITY_LIST_CLICK = "city_list_click";
    //登录页面曝光
    public static final String LOGIN_BAOGUANG = "login_baoguang";
    //一键登录页面曝光
    public static final String PHONELOGIN_BAOGUANG = "phonelogin_baoguang";
    //绑定登录手机页曝光
    public static final String PHONEBIND_BAOGUANG = "phonebind_baoguang";
}

