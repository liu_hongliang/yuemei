package com.module.community.statistical.noburied;

import org.aspectj.lang.annotation.Aspect;

/**
 * Created by 裴成浩 on 2019/7/2
 */
@Aspect
public class NoBuriedPointStatistics {

    private String TAG = "NoBuriedPointStatistics";

//    @After("execution(* android.view.View.OnClickListener.onClick(android.view.View))")
//    public void onClickListener(JoinPoint joinPoint) {
//        Object[] args = joinPoint.getArgs();
//        View view = (View) args[0];
//        String simpleName = view.getClass().getSimpleName();
//        int index = ((ViewGroup) view.getParent()).indexOfChild(view);
//
//        Log.e("onClickListener", "simpleName === " + simpleName);
//        Log.e("onClickListener", "index === " + index);
//        Log.e("onClickListener", "getPath(view) === " + getPath(view));
//    }
//
//    @After("execution(* android.view.ViewGroup.dispatchTouchEvent(android.view.MotionEvent))")
//    public void dispatchTouchEvent(JoinPoint joinPoint) {
//        Object[] args = joinPoint.getArgs();
//        MotionEvent event = (MotionEvent) args[0];
//
//        if (!AutoPointer.isAutoPointEnable()) {
//            return super.dispatchTouchEvent(event);
//        }
//
//        int actionMasked = event.getActionMasked();
//
//        if (actionMasked != MotionEvent.ACTION_UP) {
//            return super.dispatchTouchEvent(event);
//        }
//
//        long t = System.currentTimeMillis();
//        analyzeMotionEvent();
//
//        //非线上版本，打印执行时间
//        if (!AutoPointer.isOnlineEnv()) {
//            long time = System.currentTimeMillis() - t;
//            Log.d(TAG, String.format(Locale.CHINA, "处理时间:%d 毫秒", time));
//        }
//    }
//
//    /**
//     * 分析用户的点击行为
//     */
//    private void analyzeMotionEvent() {
//        if (mViewRef == null || mViewRef.get() == null) {
//            DDLogger.e(TAG, "window is null");
//            return;
//        }
//
//        ViewGroup decorView = (ViewGroup) mViewRef.get();
//        int content_id = android.R.id.content;
//        ViewGroup content = (ViewGroup) decorView.findViewById(content_id);
//        if (content == null) {
//            content = decorView; //对于非Activity DecorView 的情况处理
//        }
//
//        Pair<View, Object> targets = findActionTargets(content);
//        if (targets == null) {
//            DDLogger.e(TAG, "has no action targets!!!");
//            return;
//        }
//
//        //发送任务在单线程池中
//        int hashcode = targets.first.hashCode();
//        if (mIgnoreViews.contains(hashcode)) return;
//
//        PointerExecutor.getHandler().post(PointPostAction.create(targets.first, targets.second));
//    }
//
//
//    private String getPath(View view) {
//        StringBuilder aaa = new StringBuilder();
//        do {
//            //1. 构造ViewPath中于view对应的节点:ViewType[index]
//            String simpleName = view.getClass().getSimpleName();
//            int index = ((ViewGroup) view.getParent()).indexOfChild(view);
//            if (view.getParent() instanceof RecyclerView) {
//                RecyclerView recyclerView = (RecyclerView) view.getParent();
//                index = recyclerView.getChildAdapterPosition(view);
//                Log.e("onClickListener", "index == " + index);
//            }
//
//            aaa.insert(0, simpleName + "[" + index + "]/");
//
//            if (view.getParent() instanceof View) {
//                view = (View) view.getParent();
//            }
//        } while (view.getParent() instanceof View);//2. 将view指向上一级的节点
//
//        aaa.insert(0, view.getContext().getClass() + "/");
//        return aaa.toString();
//    }


//    @Before("execution(* android.view.View.OnClickListener.onClick(android.view.View))")
//    public void onClickListener(JoinPoint joinPoint) {
//        Object aThis = joinPoint.getThis();
//        Object[] args = joinPoint.getArgs();

////        String kind = joinPoint.getKind();
//        Signature signature = joinPoint.getSignature();
//        SourceLocation sourceLocation = joinPoint.getSourceLocation();
//        JoinPoint.StaticPart staticPart = joinPoint.getStaticPart();
//        Object target = joinPoint.getTarget();
//        String s = joinPoint.toShortString();
//        String s1 = joinPoint.toString();
//        String s2 = joinPoint.toLongString();
//
////        Log.e(TAG, "aThis == " + aThis);
////        Log.e(TAG, "args == " + args);
////        Log.e(TAG, "kind == " + kind);
//        Log.e(TAG, "signature == " + signature.toString());
//        Log.e(TAG, "sourceLocation == " + sourceLocation.toString());
//        Log.e(TAG, "staticPart == " + staticPart.toString());
//        Log.e(TAG, "target == " + target);
//        Log.e(TAG, "s == " + s);
//        Log.e(TAG, "s1 == " + s1);
//        Log.e(TAG, "s2 == " + s2);
//
//
//        Class declaringType = signature.getDeclaringType();
//        String declaringTypeName = signature.getDeclaringTypeName();
//        int modifiers = signature.getModifiers();
//        String name = signature.getName();
//        String s3 = signature.toLongString();
//        String s4 = signature.toShortString();
//        String s5 = signature.toString();
//
//        Log.e(TAG, "declaringType == " + declaringType);
//        Log.e(TAG, "declaringTypeName == " + declaringTypeName);
//        Log.e(TAG, "modifiers == " + modifiers);
//        Log.e(TAG, "name == " + name);
//        Log.e(TAG, "s3 == " + s3);
//        Log.e(TAG, "s4 == " + s4);
//        Log.e(TAG, "s5 == " + s5);
//
//
//        String fileName = sourceLocation.getFileName();
//        int line = sourceLocation.getLine();
//        Class withinType = sourceLocation.getWithinType();
//
//        Log.e(TAG, "fileName == " + fileName);
//        Log.e(TAG, "line == " + line);
//        Log.e(TAG, "withinType == " + withinType);
//
//        int id = staticPart.getId();
//        String kind1 = staticPart.getKind();
//        Signature signature1 = staticPart.getSignature();
//        SourceLocation sourceLocation1 = staticPart.getSourceLocation();
//        String s6 = staticPart.toLongString();
//        String s7 = staticPart.toShortString();
//        String s8 = staticPart.toString();
//
//        Log.e(TAG, "id == " + id);
//        Log.e(TAG, "kind1 == " + kind1);
//        Log.e(TAG, "signature1 == " + signature1.toString());
//        Log.e(TAG, "sourceLocation1 == " + sourceLocation1.toString());
//        Log.e(TAG, "s6 == " + s6);
//        Log.e(TAG, "s7 == " + s7);
//        Log.e(TAG, "s8 == " + s8);
//
//
////        String s1 = joinPoint.toString();
//    }

}
