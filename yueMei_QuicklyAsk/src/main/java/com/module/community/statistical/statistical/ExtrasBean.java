package com.module.community.statistical.statistical;

import java.util.HashMap;

public class ExtrasBean {
    /**
     * e_id : 0
     * link : https://m.yuemei.com/shopingcart
     * params : {"u":"9600062"}
     * type : 10000
     * u_id : 0
     */

    private int e_id;
    private String link;
    private HashMap<String,String> params;
    private String type;
    private int u_id;

    public int getE_id() {
        return e_id;
    }

    public void setE_id(int e_id) {
        this.e_id = e_id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public HashMap<String,String> getParams() {
        return params;
    }

    public void setParams(HashMap<String,String> params) {
        this.params = params;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getU_id() {
        return u_id;
    }

    public void setU_id(int u_id) {
        this.u_id = u_id;
    }

}
