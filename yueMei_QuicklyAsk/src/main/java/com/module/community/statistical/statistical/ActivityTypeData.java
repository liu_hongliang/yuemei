package com.module.community.statistical.statistical;

/**
 * Created by 裴成浩 on 2019/3/25
 */
public class ActivityTypeData {
    private String type;                    //当前页面type

    public ActivityTypeData(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
