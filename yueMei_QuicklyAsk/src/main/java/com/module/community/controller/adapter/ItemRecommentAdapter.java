package com.module.community.controller.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.RecommentTaoList;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class ItemRecommentAdapter extends BaseQuickAdapter<RecommentTaoList.TaoListBean, BaseViewHolder> {
    public ItemRecommentAdapter(int layoutResId, @Nullable List<RecommentTaoList.TaoListBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecommentTaoList.TaoListBean item) {
        Glide.with(mContext).load(item.getTao_img())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .into((ImageView) helper.getView(R.id.item_img));
        helper.setText(R.id.item_title,item.getTitle())
                .setText(R.id.item_txt_price,"¥"+item.getPrice());
        //设置标签
        List<String> promotion = item.getIcon_label();
        if (CollectionUtils.isNotEmpty(promotion)) {
            helper.getView(R.id.tao_list_flowLayout).setVisibility(View.VISIBLE);
            setTagView((FlowLayout) helper.getView(R.id.tao_list_flowLayout), promotion);
        } else {
            helper.getView(R.id.tao_list_flowLayout).setVisibility(View.GONE);
        }
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<String> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++) {
                TextView textView = new TextView(mContext);
                textView.setText(lists.get(i));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                textView.setBackgroundResource(R.drawable.shape_category_manjian_ff527f);
                textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                mFlowLayout.addView(textView);
            }
        }
    }
}
