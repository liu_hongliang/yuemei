package com.module.community.controller.adapter;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.List;

public class VideoSkuPopAdapter extends BaseQuickAdapter<HomeTaoData,BaseViewHolder> {
    public static final String TAG = "VideoSkuPopAdapter";
    private String mDiaryId;
    public VideoSkuPopAdapter(int layoutResId, @Nullable List<HomeTaoData> data,String diaryId) {
        super(layoutResId, data);
        this.mDiaryId = diaryId;
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeTaoData item) {
        Glide.with(mContext).load(item.getImg())
                .transform(new GlideRoundTransform(mContext,Utils.dip2px(5)))
                .into((ImageView) helper.getView(R.id.item_video_sku_img));
        helper.setText(R.id.item_video_sku_title,"<"+item.getTitle()+">"+item.getSubtitle())
                .setText(R.id.item_video_sku_goods_price,item.getPrice_discount())
                .setText(R.id.item_video_sku_goods_unit,item.getFeeScale());
        TextView chatTxt = helper.getView(R.id.tv_consultation);
        chatTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"onClick");
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",mDiaryId);
                hashMap.put("hos_id",item.getHospital_id());
                hashMap.put("doc_id",item.getHos_userid());
                hashMap.put("tao_id",item.getId());
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHAT_HOSPITAL,"video_end_play_chat",item.getHospital_id()),hashMap);
                ChatDataBean chatData = item.getChatData();
                YmStatistics.getInstance().tongjiApp(chatData.getEvent_params());
                if (Utils.noLoginChat()) {
                    setConsulting(chatData);
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        setConsulting(chatData);
                    }
                }

            }
        });

    }

    /***
     * 咨询设置
     * @param chatData
     */
    private void setConsulting(ChatDataBean chatData) {
        String is_rongyun = chatData.getIs_rongyun();
        String hos_userid = chatData.getHos_userid();
        String event_name = chatData.getEvent_name();
        String event_pos = chatData.getEvent_pos();
        HashMap<String, String> event_params = chatData.getEvent_params();
        if ("3".equals(is_rongyun)) {
            YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
            ChatDataGuiJi mTaoDetailBean = chatData.getGuiji();
            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                    .setDirectId(hos_userid)
                    .setObjId(chatData.getObj_id())
                    .setObjType(chatData.getObj_type() + "")
                    .setTitle(mTaoDetailBean.getTitle())
                    .setImg(mTaoDetailBean.getImage())
                    .setMemberPrice(mTaoDetailBean.getMember_price())
                    .setYmClass("216")
                    .setYmId(chatData.getYmaq_id())
                    .build();

            new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        } else {
            Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
        }
    }
}
