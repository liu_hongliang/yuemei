package com.module.community.controller.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseFragment;
import com.module.community.controller.adapter.CommunityFragmentLabelAdapter;
import com.module.community.controller.adapter.CommunityStagFragmentAdapter;
import com.module.community.controller.api.CommunityFragmentApi;
import com.module.community.controller.other.CommunityListAdapter;
import com.module.community.model.bean.CommunityBannerData;
import com.module.community.model.bean.CommunityKaFive;
import com.module.community.model.bean.CommunityPost;
import com.module.community.model.bean.CommunityStaggeredData;
import com.module.community.model.bean.CommunityStaggeredListData;
import com.module.community.model.bean.CommunityStaggeredMetroData;
import com.module.community.model.bean.CommunityStaggeredNoticeData;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import org.kymjs.kjframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import simplecache.ACache;

/**
 * 社区双列交错模版
 * Created by 裴成浩 on 2018/7/10.
 */
public class CommunityStaggeredFragment extends YMBaseFragment {

    @BindView(R.id.community_stag_background)
    RelativeLayout mStagBackground;
    @BindView(R.id.community_stag_refresh)
    SmartRefreshLayout mPullRefresh;
    @BindView(R.id.community_stag_label_recycler)
    RecyclerView mLabelRecycler;
    @BindView(R.id.community_list_recycler)
    LoadMoreRecyclerView mCommunityRecycler;
    private HomeCommunityTagData mData;
    private CommunityFragmentApi staggeredApi;
    private String TAG = "CommunityStaggeredFragment";
    private ArrayList<CommunityStaggeredListData> mStaggeredDatas;
    private CommunityStagFragmentAdapter communityStagFragmentAdapter;

    private int mPage = 1;
    private View mHeadView;
    private CommunityStaggeredMetroData metro;
    private ArrayList<ArrayList<CommunityStaggeredNoticeData>> notice;
    private int requestTime;
    private final String STAGGERED_REQUESTTIME = "staggered_requestTime";
    private final String COMMUNITY_TUIJIAN_JSON = "community_tuijian_json";
    private CommunityFragmentLabelAdapter mCommunityFragmentAdapter;
    private ACache mCache;
    private StaggeredGridLayoutManager scrollLinearLayoutManager;
    private ArrayList<CommunityBannerData> banner;
    private ArrayList<CommunityBannerData> katwo;
    private ArrayList<CommunityKaFive> kafive;
    private ArrayList<CommunityKaFive> changebeautiful;
    private LinearLayout commnuityTwoka;
    private ImageView commnuityTwokaOne;
    private ImageView commnuityTwokaTwo;
    private Banner commnuityBanner;
    private TextView commnuityTagTitle;
    private TextView commnuityBgTagTitle;
    private LinearLayout bannerContainer;
    private FrameLayout commnuityListContainer;
    private RecyclerView commnuityFiveList;
    private FrameLayout commnuityChangeBeautify;
    private ImageView commnuityChangeBeautifyImg;
    private TextView commnuityChangeBeautifyTitle;
    private ImageView commnuityChangeBeautifyUserimg;
    private TextView commnuityChangeBeautifyUsername;
    private TextView commnuityChangeBeautifyLiulanTxt;
    private TextView commnuityChangeBeautifyPinluTxt;

    public static CommunityStaggeredFragment newInstance(HomeCommunityTagData data) {
        CommunityStaggeredFragment fragment = new CommunityStaggeredFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mData = getArguments().getParcelable("data");
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_staggered_community;
    }

    @Override
    protected void initView(View view) {
        //标签
        if (mData.getTag().size() > 0) {
            mLabelRecycler.setVisibility(View.VISIBLE);
            mCommunityFragmentAdapter = new CommunityFragmentLabelAdapter(mContext, mData.getTag());
            mLabelRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            ((DefaultItemAnimator) mLabelRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
            mLabelRecycler.setAdapter(mCommunityFragmentAdapter);

            mCommunityFragmentAdapter.setOnItemCallBackListener(new CommunityFragmentLabelAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    mPage = 1;
                    isNetwork(true);
                }
            });
        } else {
            mLabelRecycler.setVisibility(View.GONE);
        }

        if (isTuijian()) {
            mHeadView = mInflater.inflate(R.layout.head_fragment_staggered_community, null);
            commnuityTwoka = mHeadView.findViewById(R.id.community_twoka);
            commnuityTwokaOne = mHeadView.findViewById(R.id.communtiy_twoka_one);
            commnuityTwokaTwo = mHeadView.findViewById(R.id.communtiy_twoka_two);
            bannerContainer = mHeadView.findViewById(R.id.banner_container);
            commnuityBanner = mHeadView.findViewById(R.id.community_banner);
            commnuityBgTagTitle = mHeadView.findViewById(R.id.community_cbtag_titel);
            commnuityTagTitle = mHeadView.findViewById(R.id.community_tag_title);
            commnuityListContainer = mHeadView.findViewById(R.id.community_listcontainer);
            commnuityFiveList = mHeadView.findViewById(R.id.community_fiveka_list);
            commnuityChangeBeautify = mHeadView.findViewById(R.id.community_changebeautify);
            commnuityChangeBeautifyImg = mHeadView.findViewById(R.id.community_changebeautify_img);
            commnuityChangeBeautifyTitle = mHeadView.findViewById(R.id.community_changebeautify_title);
            commnuityChangeBeautifyUserimg = mHeadView.findViewById(R.id.community_changebeautify_user_img);
            commnuityChangeBeautifyUsername = mHeadView.findViewById(R.id.community_changebeautify_user_name);
            commnuityChangeBeautifyLiulanTxt = mHeadView.findViewById(R.id.community_changebeautify_lulan_txt);
            commnuityChangeBeautifyPinluTxt = mHeadView.findViewById(R.id.community_changebeautify_pinlun_txt);
            mCommunityRecycler.addHeaderView(mHeadView);
        } else {
            mStagBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color._f6));
        }

        mPullRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                isNetwork(true);
                mIsLoadMore=false;
            }
        });

        //加载更多
        mCommunityRecycler.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                isNetwork(false);
                mIsLoadMore=true;
            }
        });

    }
    //是否是下拉加载
    private boolean mIsLoadMore = false;
    @Override
    protected void initData(View view) {
        staggeredApi = new CommunityFragmentApi(mData.getController(), mData.getAction());
        mCache = ACache.get(mContext);

        //联网请求
        if (isTuijian()) {
            if (SystemTool.checkNet(mContext)) {
                loadingData(false);
            } else {
                String tuijianjson = mCache.getAsString(COMMUNITY_TUIJIAN_JSON);
                if (tuijianjson != null) {
                    parsingJson(tuijianjson);
                }
            }
        } else {
            loadingData(false);
        }
    }

    /**
     * 判断是否联网
     *
     * @param isRefresh
     */
    private void isNetwork(boolean isRefresh) {
        if (SystemTool.checkNet(mContext)) {
            loadingData(isRefresh);
        } else {
            mPullRefresh.finishRefresh();
            mFunctionManager.showShort("请检测网络");
        }
    }

    /**
     * 加载数据
     */
    private void loadingData(final boolean isRefresh) {

        staggeredApi.addData("page", mPage + "");
        staggeredApi.addData("id", mData.getId() + "");
        if (mCommunityFragmentAdapter != null && mCommunityFragmentAdapter.getmData().size() > 0) {
            Log.e(TAG, "二级标签id ===" + mCommunityFragmentAdapter.getmData().get(mCommunityFragmentAdapter.findSelected()).getPartId());
            staggeredApi.addData("partId", mCommunityFragmentAdapter.getmData().get(mCommunityFragmentAdapter.findSelected()).getPartId() + "");
        }
        if (isTuijian()) {
            staggeredApi.addData("requestTime", mFunctionManager.loadInt(STAGGERED_REQUESTTIME, 0) + "");
        }
        staggeredApi.getCallBack(mContext, staggeredApi.getHashMap(), new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String json) {
                if (mPullRefresh != null){
                    mPullRefresh.finishRefresh();
                }

                mPage++;

                if (isTuijian()) {
                    mCache.put(COMMUNITY_TUIJIAN_JSON, json);
                }

                if (isRefresh && isTuijian()) {
                    String message = staggeredApi.getMessage();
                    if (!TextUtils.isEmpty(message)) {
                        mFunctionManager.showShort(message);
                    } else {
                        mFunctionManager.showShort("没有新内容了，稍后再看看");
                    }
                }

                //刷新
                if (isRefresh && communityStagFragmentAdapter != null) {
                    communityStagFragmentAdapter.clearData();
                }

                parsingJson(json);

            }
        });
    }

    /**
     * json数据解析
     *
     * @param json
     */
    private void parsingJson(String json) {
        try {
            CommunityStaggeredData staggeredData = JSONUtil.TransformSingleBean(json, CommunityStaggeredData.class);
            mStaggeredDatas = staggeredData.getData();
            if (!mIsLoadMore){
                banner = staggeredData.getBanner();
                katwo = staggeredData.getKatwo();
                kafive = staggeredData.getKafive();
                changebeautiful = staggeredData.getChangebeautiful();
            }
            Log.e(TAG, "mStaggeredDatas == " + mStaggeredDatas.get(0).getTitle());
            requestTime = staggeredData.getRequestTime();
            initViewData();
        } catch (Exception e) {
            Log.e(TAG, "e == " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 加载完成后的数据设置
     */
    private void initViewData() {

        if (communityStagFragmentAdapter == null) {

            if (isTuijian()) {

                mFunctionManager.saveInt(STAGGERED_REQUESTTIME, requestTime);

            }
            scrollLinearLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
            scrollLinearLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);

            //判断左右列，方法2（推荐）
            communityStagFragmentAdapter = new CommunityStagFragmentAdapter(mContext, mStaggeredDatas);
            mCommunityRecycler.setLayoutManager(scrollLinearLayoutManager);
            mCommunityRecycler.setAdapter(communityStagFragmentAdapter);
            mCommunityRecycler.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    //判断左右列，方法2（推荐）
                    StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());
                    if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                        //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                        if (params.getSpanIndex() % 2 == 0) {
                            //左列
                            outRect.right = Utils.dip2px((int) 2.5);
                        } else {
                            //右列
                            outRect.left = Utils.dip2px((int)2.5);
                        }
                    }
                }
            });
            ((DefaultItemAnimator) mCommunityRecycler.getItemAnimator()).setSupportsChangeAnimations(false);

            //回到顶部
            mCommunityRecycler.setBackTopListener(new LoadMoreRecyclerView.BackTopListener() {
                @Override
                public void onBackTop() {

                    int[] firstVisibleItem = scrollLinearLayoutManager.findFirstVisibleItemPositions(null);
                    if (isTuijian()) {
                        if (firstVisibleItem[1] == 2) {
                            if (communityStagFragmentAdapter != null) {
                                scrollLinearLayoutManager.invalidateSpanAssignments();
                            }
                        }
                    } else {
                        if (firstVisibleItem[0] == 0) {
                            if (communityStagFragmentAdapter != null) {
                                scrollLinearLayoutManager.invalidateSpanAssignments();
                            }
                        }
                    }

                }
            });
            communityStagFragmentAdapter.setOnItemCallBackListener(new CommunityStagFragmentAdapter.ItemCallBackListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    Log.e(TAG, "pos === " + pos);
                    if (isTuijian()) {
                        if (pos != 0 && pos != communityStagFragmentAdapter.getItemCount()) {
                            CommunityStaggeredListData listData = communityStagFragmentAdapter.getmData().get(pos - 1);
                            //统计
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBSHOMECLICK, "bbs|list_" + mData.getTongjiid() + "|" + pos + "|" + FinalConstant1.MARKET + "|" + FinalConstant1.DEVICE),listData.getEvent_params());
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(listData.getUrl(), "0", "0");

                        }
                    } else {
                        if (pos != communityStagFragmentAdapter.getItemCount()) {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBSHOMECLICK, "bbs|list_" + mData.getTongjiid() + "|" + (pos + 1) + "|" + FinalConstant1.MARKET + "|" + FinalConstant1.DEVICE));
                            CommunityStaggeredListData listData = communityStagFragmentAdapter.getmData().get(pos);
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(listData.getUrl(), "0", "0");
                        }
                    }
                }
            });
        } else {
            communityStagFragmentAdapter.addData(mStaggeredDatas);
        }
        if (isTuijian() && !mIsLoadMore){
            mIsLoadMore=false;
            loadHeadData();
        }

        if (mStaggeredDatas.size() < 20) {
            mCommunityRecycler.setNoMore(true);
        } else {
            mCommunityRecycler.loadMoreComplete();
        }
    }


    public void loadHeadData(){
        //上面两卡
        if (katwo != null && !katwo.isEmpty() ){
            commnuityTwoka.setVisibility(View.VISIBLE);

            mFunctionManager.setRoundImageSrc(commnuityTwokaOne,katwo.get(0).getImg(),Utils.dip2px(6));

            commnuityTwokaOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> event_params = katwo.get(0).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_ALL_CARD_POSITION,"1"),event_params,new ActivityTypeData("25"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(katwo.get(0).getUrl());
                }
            });
            commnuityTwokaTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> event_params = katwo.get(1).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_ALL_CARD_POSITION,"2"),event_params,new ActivityTypeData("25"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(katwo.get(1).getUrl());
                }
            });
            mFunctionManager.setRoundImageSrc(commnuityTwokaTwo,katwo.get(1).getImg(),Utils.dip2px(6));
        }else {
            commnuityTwoka.setVisibility(View.GONE);
        }


        //中间banner
        if (banner != null && !banner.isEmpty()){
            bannerContainer.setVisibility(View.VISIBLE);
            ArrayList<String> imgUrls = new ArrayList<>();
            for (int i = 0; i <banner.size() ; i++) {
                imgUrls.add(banner.get(i).getImg());
            }

            //设置banner样式
            commnuityBanner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
            //设置图片加载器
            commnuityBanner.setImageLoader(new ImageLoader() {
                @Override
                public void displayImage(Context context, Object path, ImageView imageView) {

                    if (Util.isOnMainThread()) {
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        Glide.with(mContext)
                                .load(path)
                                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(7), GlidePartRoundTransform.CornerType.ALL))
                                .into(imageView);
                    }

                }
            });
            //设置banner动画效果
            commnuityBanner.setBannerAnimation(Transformer.Default);
            //设置自动轮播，默认为true
            commnuityBanner.isAutoPlay(true);
            //设置轮播时间
            commnuityBanner.setDelayTime(3000);
            //设置指示器位置（当banner模式中有指示器时）
            commnuityBanner.setIndicatorGravity(BannerConfig.CENTER);
            commnuityBanner.setImages(imgUrls);
            commnuityBanner.start();
            commnuityBanner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    HashMap<String, String> event_params = banner.get(position).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_FOCUS_IMAGE_POSITION,""+(position+1)),event_params,new ActivityTypeData("25"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(banner.get(position).getUrl());
                }
            });

        }else {
            bannerContainer.setVisibility(View.GONE);
        }

        //中间横向列表
        if (kafive != null && !kafive.isEmpty()){
            commnuityListContainer.setVisibility(View.VISIBLE);
            commnuityTagTitle.setText(kafive.get(0).getTitle());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            CommunityListAdapter communityListAdapter = new CommunityListAdapter(R.layout.community_list_item, kafive);
            commnuityFiveList.setLayoutManager(linearLayoutManager);
            commnuityFiveList.setAdapter(communityListAdapter);
            communityListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    HashMap<String, String> event_params = kafive.get(position).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_TALENT_PROMPT,""+(position+1)),event_params,new ActivityTypeData("25"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(kafive.get(position).getUrl());
                }
            });
        }else {
            commnuityListContainer.setVisibility(View.GONE);
        }

        //变美不上当
        if (changebeautiful != null && !changebeautiful.isEmpty()){
            commnuityChangeBeautify.setVisibility(View.VISIBLE);
            commnuityBgTagTitle.setText(changebeautiful.get(0).getTitle());
            CommunityKaFive communityKaFive = changebeautiful.get(0);
            Glide.with(mContext)
                    .load(communityKaFive.getImg())
                    .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.RIGHT_TOP))
                    .into(commnuityChangeBeautifyImg);
            final CommunityPost post = communityKaFive.getPost();
            commnuityChangeBeautifyTitle.setText(post.getTitle());
            Glide.with(mContext)
                    .load(post.getUser_img())
                    .transform(new GlideCircleTransform(mContext))
                    .into(commnuityChangeBeautifyUserimg);
            commnuityChangeBeautifyUsername.setText(post.getUser_name());
            commnuityChangeBeautifyLiulanTxt.setText(post.getView_num());
            commnuityChangeBeautifyPinluTxt.setText(post.getAnswer_num());
            commnuityChangeBeautify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HashMap<String, String> event_params = changebeautiful.get(0).getEvent_params();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_BECOME_BEAUTIFUL,"1"),event_params,new ActivityTypeData("25"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(changebeautiful.get(0).getUrl());
                }
            });
        }else {
            commnuityChangeBeautify.setVisibility(View.GONE);
        }
    }


    /**
     * 是否是推荐页
     *
     * @return
     */
    private boolean isTuijian() {
        return mData.getId() == 0;
    }

}
