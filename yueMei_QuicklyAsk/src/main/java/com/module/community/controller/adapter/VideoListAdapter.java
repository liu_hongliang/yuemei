package com.module.community.controller.adapter;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.DiaryUserdataBean;
import com.module.commonview.module.bean.PostContentVideo;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.WorkTime;
import com.module.commonview.view.ButtomDialogView;
import com.module.commonview.view.MyURLSpan;
import com.module.commonview.view.share.MyShareBoardlistener;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.controller.api.LookVideoApi;
import com.module.community.model.bean.BBsShareData;
import com.module.community.model.bean.PlayingVideoData;
import com.module.community.model.bean.ShareDetailPictorial;
import com.module.community.model.bean.VideoListData;
import com.module.community.model.bean.VideoTaoTimeData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.view.VideoListBottomDialog;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.YueMeiVideoView;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.apache.commons.collections.CollectionUtils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static com.quicklyask.entity.VideoViewState.TO_PLAY;

/**
 * Created by 裴成浩 on 2019/5/6
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {

    private Activity mContext;
    private List<VideoListData> mListDatas;
    private int mProgress;
    private LayoutInflater mInflater;
    private String TAG = "VideoListAdapter";
    private final FunctionManager mFunctionManager;
    private final MyShareBoardlistener myShareBoardlistener;
    private final ButtomDialogView buttomDialogView;
    private PlayingVideoData mPlayingData = new PlayingVideoData();                     //当前播放中视频的数据
    private final BaseNetWorkCallBackApi mPointLikeApi;              //点赞收藏接口
    private final LookVideoApi lookVideoApi;                //视频上传3秒接口
    private final int mWindowsWight;                        //屏幕宽度
    private final int mWindowsHeight;                       //屏幕高度

    private final String LIKE_CLICK = "like";                           //点赞局部刷新
    public final String COMMENTS_NUMBER = "comments_number";          //评论数局部刷新
    private final String SHARE_NUMBER = "share_number";                //分享数的局部刷新

    public VideoListAdapter(Activity context, List<VideoListData> urlList, int progress) {
        this.mContext = context;
        this.mListDatas = urlList;
        this.mProgress = progress;

        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);

        //分享模版
        myShareBoardlistener = new MyShareBoardlistener(mContext);
        buttomDialogView = new ButtomDialogView.Builder(mContext).setShareBoardListener(myShareBoardlistener).setIscancelable(true).setIsBackCancelable(true).build();

        //点赞接口
        mPointLikeApi = new BaseNetWorkCallBackApi(FinalConstant1.BBS, "agreeAndCollect");
        //视频上传3秒接口
        lookVideoApi = new LookVideoApi();

        // 获取屏幕高宽ll_video_end
        DisplayMetrics metric = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metric);
        mWindowsWight = metric.widthPixels;
        mWindowsHeight = metric.heightPixels;
    }

    @NonNull
    @Override
    public VideoListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        Log.e(TAG, "onCreateViewHolder == " + i);
        View itemView = mInflater.inflate(R.layout.item_video_list_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListAdapter.ViewHolder holder, int position, List<Object> payloads) {
        Log.e(TAG, "111position === " + position);
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            for (Object payload : payloads) {
                VideoListData videoListData = mListDatas.get(position);
                switch ((String) payload) {
                    case LIKE_CLICK:            //点赞刷新
                        setLikeData(holder, videoListData);
                        break;
                    case COMMENTS_NUMBER:       //评论数刷新
                        setCommentsData(holder, videoListData);
                        break;
                    case SHARE_NUMBER:          //分享数刷新
                        setShareData(holder, videoListData);
                        break;
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final VideoListAdapter.ViewHolder holder, final int pos) {
        Log.e(TAG, "onBindViewHolder == " + pos);

        //视频设置
        setVideoData(holder, pos);

        //左边数据设置
        setLeftData(holder, pos);

        //右边数据设置
        setRightData(holder, pos);

        //设置播放完成布局
        setVideoEndData(holder,pos);
    }

    private void setVideoEndData(final ViewHolder holder,final int pos) {
        final VideoListData videoListData = mListDatas.get(pos);
        if (videoListData != null){
            final List<HomeTaoData> taolist = videoListData.getTaolist();
            if (CollectionUtils.isNotEmpty(taolist)){
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
                VideoSkuPopAdapter popAdapter = new VideoSkuPopAdapter(R.layout.video_sku_recommend, taolist,videoListData.getId());
                holder.rv_video_end.setLayoutManager(layoutManager);
                holder.rv_video_end.setAdapter(popAdapter);
                popAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        String id = taolist.get(position).getId();
                        if (TextUtils.isEmpty(id)){
                            return;
                        }
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("id",videoListData.getId());
                        hashMap.put("to_page_type","2");
                        hashMap.put("to_page_id",id);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_END_PLAY_SKU,(1+position)+""),hashMap);
                        Intent it1 = new Intent();
                        it1.putExtra("id",id);
                        it1.putExtra("source", "0");
                        it1.putExtra("objid", "0");
                        it1.setClass(mContext, TaoDetailActivity.class);
                        mContext.startActivity(it1);
                    }
                });
            }
        }


        holder.iv_replayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mVideoView.videoStartPlayer();
                holder.ll_video_end.setVisibility(View.GONE);
            }
        });

        holder.mVideoView.setOnCompletionListener(new YueMeiVideoView.OnCompletionListener() {
            @Override
            public void onCompletionListener(int videoWidth, int videoHeight) {
//                holder.mVideoView.hideVideoControl(true);
                VideoListData videoListData = mListDatas.get(pos);
                if (videoListData != null){
                    List<HomeTaoData> taolist = videoListData.getTaolist();
                    if (CollectionUtils.isNotEmpty(taolist)){
                        holder.mVideoView.setVideoViewState(TO_PLAY,View.GONE);
                        holder.ll_video_end.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        RecyclerView.ItemAnimator itemAnimator = holder.rv_video_end.getItemAnimator();
        //取消局部刷新动画效果
        if (null != itemAnimator) {
            ((DefaultItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        holder.rv_video_end.setLayoutManager(linearLayoutManager);
//       VideoEndAdapter mVideoEndAdapter = new VideoEndAdapter(mContext);
//       holder.rv_video_end.setAdapter(mVideoEndAdapter);

    }


    /**
     * 视频设置
     *
     * @param holder
     * @param pos
     */
    private void setVideoData(@NonNull final ViewHolder holder, int pos) {
        final PostContentVideo videoData = mListDatas.get(pos).getVideoData();
        Log.e(TAG, "videoData == " + videoData.toString());
        Log.e(TAG, "img == " + videoData.getImg());
        Log.e(TAG, "videoData.getUrl() == " + videoData.getVideo_url());

//        holder.mVideoView.setVideoParameter(null, videoData.getVideo_url());
        holder.mVideoView.setVideoParameter(videoData.getImg(), videoData.getVideo_url());

        if (pos == 0 && !holder.mVideoView.isPlaying()) {
            //设置播放中的数据
            mPlayingData.setData(mListDatas.get(pos));
            mPlayingData.setViewHolder(holder);
            if (mProgress != 0) {
                holder.mVideoView.setSeekTo(mProgress);
            }
            //开始播放视频
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_BOFANG, "1"), mListDatas.get(pos).getEvent_params());
            holder.mVideoView.videoStartPlayer();
        }
    }

    /**
     * 左边数据设置
     *
     * @param holder
     * @param pos
     */
    @SuppressLint("SetTextI18n")
    private void setLeftData(final ViewHolder holder, int pos) {
        final VideoListData videoListData = mListDatas.get(pos);
        List<HomeTaoData> taolist = videoListData.getTaolist();

        //购物袋数量
        int size = taolist.size();
        if (size > 0) {
            holder.mTaoCar.setVisibility(View.VISIBLE);
            holder.mTaoCar.setText(size + "");
        } else {
            holder.mTaoCar.setVisibility(View.GONE);
        }

        //用户名
        holder.mVideoTitle.setText("@" + videoListData.getUserdata().getName());
        //视频内容
        setContentData(holder, videoListData);
    }

    /**
     * 右边数据设置
     *
     * @param holder
     * @param pos
     */
    private void setRightData(ViewHolder holder, int pos) {
        VideoListData videoList = mListDatas.get(pos);
        //用户头像
        String avatar = videoList.getUserdata().getAvatar();
        if (!TextUtils.isEmpty(avatar)) {
            mFunctionManager.setCircleImageSrc(holder.mListUser, avatar);
        }
        //点赞设置
        setLikeData(holder, videoList);

        //咨询设置
        ChatDataBean chatData = videoList.getChatData();
        if ("3".equals(chatData.getIs_rongyun())) {
            WorkTime workTime = chatData.getWork_time();
            String startTime = workTime.getStart();
            String endTime = workTime.getEnd();
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            if (Utils.hourMinuteBetween(hour + ":" + minute, startTime, endTime)) {
                holder.mConsulting.setVisibility(View.VISIBLE);
            } else {
                holder.mConsulting.setVisibility(View.GONE);
            }
        } else {
            holder.mConsulting.setVisibility(View.GONE);
        }

        //评论设置
        setCommentsData(holder, videoList);
        //分享设置
        setShareData(holder, videoList);
    }


    @Override
    public int getItemCount() {
        return mListDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public YueMeiVideoView mVideoView;              //视频播放器

        //左边
        LinearLayout mLeftViewContainer;            //左边所有view容器
        TextView mTaoCar;
        public LinearLayout mTaoClick;
        ImageView mTaoImage;
        TextView mTaoTitle;
        TextView mTaoPrice;
        TextView mTaoReservation;

        LinearLayout mVideoTitleContent;         //用户名和视频内容容器
        TextView mVideoTitle;                    //用户名
        TextView mVideoContent;                  //视频内容

        //右边
        LinearLayout mRightViewContainer;         //右边所有view容器
        ImageView mListUser;                     //用户头像
        LinearLayout mAgreeClick;                  //点赞点击
        ImageView mAgreeImage;                   //点赞图片
        TextView mAgreeNum;                        //点赞数
        RelativeLayout mConsultingClick;            //咨询点击
        ImageView mConsulting;                    //在线是否显示
        LinearLayout mAnswerClick;                  //评论点击
        ImageView mAnswerImage;                   //评论图片
        TextView mAnswerNum;                        //评论数
        LinearLayout mShareClick;                  //分享点击
        ImageView mShareImage;                    //分享图片
        TextView mShareNum;                        //分享数

        public LinearLayout ll_video_end;                //视频播放完布局
        RecyclerView rv_video_end;                //列表
        ImageView iv_replayer;                    //重播按钮

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mVideoView = itemView.findViewById(R.id.item_video_list_video);

            //左边
            mLeftViewContainer = itemView.findViewById(R.id.item_video_list_left_container);
            mTaoCar = itemView.findViewById(R.id.video_list_show_tao_car);
            mTaoClick = itemView.findViewById(R.id.video_list_show_tao_click);
            mTaoImage = itemView.findViewById(R.id.video_list_show_tao_image);
            mTaoTitle = itemView.findViewById(R.id.video_list_show_tao_title);
            mTaoPrice = itemView.findViewById(R.id.video_list_show_tao_price);
            mTaoReservation = itemView.findViewById(R.id.video_list_show_tao_reservation);

            mVideoTitleContent = itemView.findViewById(R.id.item_video_list_title_content_container);
            mVideoTitle = itemView.findViewById(R.id.item_video_list_title);
            mVideoContent = itemView.findViewById(R.id.item_video_list_content);

            //右边
            mRightViewContainer = itemView.findViewById(R.id.item_video_list_right_container);
            mListUser = itemView.findViewById(R.id.item_video_list_user);
            mAgreeClick = itemView.findViewById(R.id.item_video_list_agree_click);
            mAgreeImage = itemView.findViewById(R.id.item_video_list_agree);
            mAgreeNum = itemView.findViewById(R.id.item_video_list_agree_num);
            mConsultingClick = itemView.findViewById(R.id.item_video_list_consulting_click);
            mConsulting = itemView.findViewById(R.id.item_video_list_consulting);
            mAnswerClick = itemView.findViewById(R.id.item_video_list_answer_click);
            mAnswerImage = itemView.findViewById(R.id.item_video_list_answer);
            mAnswerNum = itemView.findViewById(R.id.item_video_list_answer_num);
            mShareClick = itemView.findViewById(R.id.item_video_list_share_click);
            mShareImage = itemView.findViewById(R.id.item_video_list_share);
            mShareNum = itemView.findViewById(R.id.item_video_list_share_num);

            ll_video_end = itemView.findViewById(R.id.ll_video_end);
            rv_video_end = itemView.findViewById(R.id.rv_video);
            iv_replayer = itemView.findViewById(R.id.iv_replayer);


            //购物袋点击
            mTaoCar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VideoListData videoListData = mListDatas.get(getLayoutPosition());
                    List<HomeTaoData> taolist = videoListData.getTaolist();
                    ChatDataBean chatData = videoListData.getChatData();

                    if (taolist.size() == 1) {
                        HomeTaoData taoData = taolist.get(0);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_SHOPPING_CART_CLICK), videoListData.getShoppingCartEventParams());
                        JumpSku(taoData);
                    } else if (taolist.size() > 1) {
                        VideoListBottomDialog videoListBottomDialog = new VideoListBottomDialog(mContext, taolist, mWindowsHeight);
                        videoListBottomDialog.show();
                        videoListBottomDialog.setOnItemCallBackListener(new VideoListBottomDialog.ItemCallBackListener() {
                            @Override
                            public void onItemClick(View v, HomeTaoData data) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_SHOPPING_CART_TAO_CLICK), data.getEvent_params());
                                JumpSku(data);
                            }
                        });
                    }
                }
            });

            //当前显示的tao
            mTaoClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    VideoListData videoListData = mListDatas.get(getLayoutPosition());
                    HomeTaoData selected_tao = videoListData.getSelected_tao();
                    if (selected_tao != null) {
                        HashMap<String, String> event_params = videoListData.getEvent_params();
                        HashMap<String, String> event_params1 = selected_tao.getEvent_params();
                        event_params.putAll(event_params1);

                        for (String key : event_params.keySet()) {
                            if ("event_name".equals(key)) {
                                event_params.put(key, FinalEventName.VIDEO_TO_TAO);
                            }
                        }

                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_TO_TAO), event_params);
                        JumpSku(selected_tao);
                    }
                }
            });

            //头像点击
            mListUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    VideoListData videoListData = mListDatas.get(getLayoutPosition());
                    DiaryUserdataBean userdata = videoListData.getUserdata();
                    String url = userdata.getUrl();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_USER_AVATAR_CLICK), userdata.getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });

            //点赞
            mAgreeClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    if (Utils.isLoginAndBind(mContext)) {
                        VideoListData videoListData = mListDatas.get(getLayoutPosition());
                        DiaryUserdataBean userdata = videoListData.getUserdata();

                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_LIKE_CLICK), videoListData.getLikeEventParams());
                        setLike(getLayoutPosition(), videoListData.getId(), userdata.getId());
                    }

                }
            });

            //咨询
            mConsultingClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    ChatDataBean chatData = mListDatas.get(getLayoutPosition()).getChatData();
                    YmStatistics.getInstance().tongjiApp(chatData.getEvent_params());
                    if (Utils.noLoginChat()) {
                        setConsulting(chatData);
                    } else {
                        if (Utils.isLoginAndBind(mContext)) {
                            setConsulting(chatData);
                        }
                    }
                }
            });

            //评论
            mAnswerClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    VideoListData videoListData = mListDatas.get(getLayoutPosition());
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_TO_REPLY, "1"), videoListData.getEvent_params());

                    if (Utils.isLoginAndBind(mContext)) {
                        DiaryUserdataBean mUserdata = videoListData.getUserdata();
//                        Bundle commentsBundle = new Bundle();
//                        commentsBundle.putString("id", mUserdata.getId());
//                        commentsBundle.putString("diary_id", videoListData.getId());
//                        commentsBundle.putString("askorshare", videoListData.getAskorshare());
//                        commentsBundle.putString("p_id", videoListData.getP_id());
//                        commentsBundle.putString("user_id", mUserdata.getId());
//
//
//                        Intent intent = new Intent(mContext, CommentsListActivity.class);
//                        intent.putExtra("data", commentsBundle);
//                        mContext.startActivityForResult(intent, VideoListActivity.REQUEST_NUMBER);
                        if (itemCommentClickListener != null) {
                            itemCommentClickListener.onItemCommentClick(v, getLayoutPosition());
                        }
                    }
                }

            });

            //分享
            mShareClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    YmStatistics.getInstance().tongjiApp(mListDatas.get(getLayoutPosition()).getShareClickData());

                    setShare(getLayoutPosition());

                }
            });


            //播放暂停回调
            mVideoView.setOnControllerClick(new YueMeiVideoView.OnControllerClick() {
                @Override
                public void onControllerClickk(boolean isPlaying) {
                    Log.e(TAG, "isPlaying == " + isPlaying);
                    if (isPlaying) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_BOFANG, "1"), mListDatas.get(getLayoutPosition()).getEvent_params());
                    } else {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_ZANTING, "1"), mListDatas.get(getLayoutPosition()).getEvent_params());
                    }
                }
            });

            //视频进度回调
            mVideoView.setOnProgressClick(new YueMeiVideoView.OnProgressClick() {
                @Override
                public void onProgressClickk(int pogress, int duration) {
                    setVideoProgress(pogress, duration);
                    //视频大于3s上传这个视频已经看过
                    if (duration > 3000) {
                        if (!mPlayingData.isUploadLookVideo()) {
                            mPlayingData.setUploadLookVideo(true);
                            lookVideo(mListDatas.get(getLayoutPosition()).getId());
                        }
                    }
                }
            });

            //控制的显示隐藏
            mVideoView.setOnProgressBarStateClick(new YueMeiVideoView.OnProgressBarStateClick() {
                @Override
                public void onProgressBarStateClick(int visibility) {
                    Log.e(TAG, "visibility == " + visibility);
                    ObjectAnimator objectAnimator;
                    if (visibility == View.VISIBLE) {
                        objectAnimator = ObjectAnimator.ofFloat(mVideoTitleContent, "alpha", 1f, 0);
                        objectAnimator.setDuration(100);
                        objectAnimator.start();
                    } else if (visibility == View.GONE) {
                        objectAnimator = ObjectAnimator.ofFloat(mVideoTitleContent, "alpha", 0, 1f);
                        objectAnimator.setDuration(100);
                        objectAnimator.start();
                    }
                }
            });
        }

        /**
         * 视频进度回调设置
         *
         * @param duration
         */
        private void setVideoProgress(int pogress, int duration) {
            if (getLayoutPosition() >= 0 && mListDatas.size() > getLayoutPosition()) {
                VideoListData videoListData = mListDatas.get(getLayoutPosition());
                List<HomeTaoData> homeTaoDatas = videoListData.getTaolist();
                List<VideoTaoTimeData> videoTaoTime = videoListData.getVideoTaoTime();
                HomeTaoData selected_tao = videoListData.getSelected_tao();

                //如果有SKU 且视频大于20秒
                if (homeTaoDatas.size() > 0) {
                    if (duration >= 20 * 1000) {
                        //如果有限定时间 按时间切换SKU
                        if (videoTaoTime.size() > 0) {
                            for (VideoTaoTimeData time : videoTaoTime) {
                                String tao_id = time.getTao_id();
                                int startTime = Integer.parseInt(time.getMin()) * 1000;
                                int entTime = Integer.parseInt(time.getMax()) * 1000;

                                if (pogress >= startTime && pogress <= entTime && (selected_tao == null || !tao_id.equals(selected_tao.getId()))) {
                                    //根据taoID 找到SKU
                                    for (HomeTaoData taoModel : homeTaoDatas) {
                                        if (tao_id.equals(taoModel.get_id())) {
                                            videoListData.setSelected_tao(taoModel);
                                            setTaoData(taoModel);
                                            break;
                                        }
                                    }
                                }
                            }

                        } else {      //没有限定时间 平均分配时间切换SKU

                            long switchingTime = duration / homeTaoDatas.size();

                            long strartTime = 0;
                            long endTime = strartTime + switchingTime;

                            for (HomeTaoData taoData : homeTaoDatas) {
                                if (endTime <= duration) {
                                    VideoTaoTimeData videoTaoTimeData = new VideoTaoTimeData();
                                    videoTaoTimeData.setMin((strartTime / 1000) + "");
                                    videoTaoTimeData.setMax((endTime / 1000) + "");
                                    videoTaoTimeData.setTao_id(taoData.get_id());
                                    videoTaoTime.add(videoTaoTimeData);

                                    strartTime = endTime;
                                    endTime = strartTime + switchingTime;
                                }
                            }

                        }
                    } else {
                        //视频时间小于20秒
                        HomeTaoData taoData = homeTaoDatas.get(0);
                        videoListData.setSelected_tao(taoData);
                        if (selected_tao == null || !taoData.getId().equals(selected_tao.getId())) {
                            setTaoData(taoData);
                        }
                    }
                } else {
                    if (mTaoClick.getVisibility() == View.VISIBLE) {
                        mTaoClick.setVisibility(View.GONE);
                    }
                }
            }
        }

        /**
         * Tao数据设置
         *
         * @param taoData
         */
        @SuppressLint("SetTextI18n")
        private void setTaoData(HomeTaoData taoData) {
            mTaoClick.setVisibility(View.VISIBLE);

            //动画设置
            taoShowAnimation();

            mFunctionManager.setRoundImageSrc(mTaoImage, taoData.getImg(), Utils.dip2px(7));
            mTaoTitle.setText(taoData.getVideoTaoTitle());               //文案
            mTaoPrice.setText("￥" + taoData.getPrice_discount());      //价格
            mTaoReservation.setText(taoData.getRate());            //预定数
        }

        /**
         * 显示动画设置
         */
        private void taoShowAnimation() {
            int duration = 500;
            mTaoClick.setPivotX(0);
            mTaoClick.setPivotY(Utils.dip2px(59) / 2);
            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(mTaoClick, "scaleX", 0, 1f);
            ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(mTaoClick, "scaleY", 0, 1f);
            ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(mTaoClick, "alpha", 0, 1f);

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3);
            animatorSet.setDuration(duration);
            animatorSet.start();
        }
    }

    /**
     * 跳转到SKU页面
     *
     * @param data
     */
    private void JumpSku(HomeTaoData data) {
        Intent intent = new Intent(mContext, TaoDetailActivity.class);
        intent.putExtra("id", data.getId());
        intent.putExtra("source", "0");
        intent.putExtra("objid", "0");
        mContext.startActivity(intent);
    }

    /**
     * 设置视频内容
     *
     * @param holder
     * @param videoListData
     */
    private void setContentData(final ViewHolder holder, final VideoListData videoListData) {
        holder.mVideoContent.post(new Runnable() {
            @Override
            public void run() {
                TextView textView = holder.mVideoContent;
                String origText = videoListData.getTitle();
                String appmurl = videoListData.getAppmurl();

                String textClick = "查看全文>";
                String textOmit = "...";

                int leftViewWidth = holder.mVideoContent.getWidth();

                if (leftViewWidth == 0) {
                    //TextView的宽度在mi4中偶尔拿不到为0，需要手动算一下
                    ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) holder.mLeftViewContainer.getLayoutParams();
                    int leftViewLeftMargin = layoutParams.leftMargin;
                    int leftViewRightMargin = layoutParams.rightMargin;

                    ViewGroup.MarginLayoutParams layoutParams1 = (ViewGroup.MarginLayoutParams) holder.mRightViewContainer.getLayoutParams();
                    int rightViewWidth = layoutParams.width;
                    int rightViewLeftMargin = layoutParams1.leftMargin;
                    int rightViewRightMargin = layoutParams1.rightMargin;

                    leftViewWidth = mWindowsWight - rightViewWidth - leftViewLeftMargin - leftViewRightMargin - rightViewLeftMargin - rightViewRightMargin;
                }

                Log.e(TAG, "leftViewWidth === " + leftViewWidth);

                if (!TextUtils.isEmpty(origText)) {
                    String firstLineText;
                    String secondLineText;
                    String resultText = origText;

                    StaticLayout layout = new StaticLayout(origText, textView.getPaint(),
                            leftViewWidth,
                            Layout.Alignment.ALIGN_NORMAL,
                            1.0f,
                            0.0f,
                            false);

                    firstLineText = origText.substring(0, layout.getLineEnd(0));

                    if (layout.getLineCount() > 2) {

//                        Log.e(TAG, "firstLineText=" + firstLineText);
//                        Log.e(TAG, "origText=" + origText);
//                        Log.e(TAG, "layout.getLineEnd(0)=" + layout.getLineEnd(0));
//                        Log.e(TAG, "layout.getLineEnd(1)=" + layout.getLineEnd(1));

                        int secondLine = layout.getLineEnd(1) - (textOmit.length() / 2) - textClick.length();
                        if (secondLine > layout.getLineEnd(0)) {
                            secondLineText = origText.substring(layout.getLineEnd(0), secondLine);
                        } else {
                            secondLineText = origText.substring(layout.getLineEnd(0), layout.getLineEnd(1));
                        }
//                        Log.e(TAG, "secondLineText=" + secondLineText);
                        resultText = firstLineText + secondLineText + textOmit;

                    }

                    SpannableString spanableInfo = new SpannableString(resultText + textClick);

                    MyURLSpan myURLSpan = new MyURLSpan(appmurl, Utils.getLocalColor(mContext, R.color.white));
                    spanableInfo.setSpan(myURLSpan, resultText.length(), resultText.length() + textClick.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                    //查看全文点击事件
                    myURLSpan.setOnClickListener(new MyURLSpan.OnClickListener() {
                        @Override
                        public void onClick(View arg0, String url) {
                            Log.e(TAG, "url === " + url);
                            if (!TextUtils.isEmpty(url)) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_POST_INFO_CLICK), videoListData.getEvent_params(), new ActivityTypeData("45"));

                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                            }
                        }
                    });

                    textView.setText(spanableInfo);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                }

            }
        });
    }


    /**
     * 看过视频的上传
     */
    private void lookVideo(String id) {
        lookVideoApi.addData("id", id);
        lookVideoApi.getCallBack(mContext, lookVideoApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    Log.e(TAG, "看过视频的上传成功");
                }
            }
        });
    }

    /**
     * 点赞，收藏设置
     *
     * @param pos  : 当前下标
     * @param id   : 帖子id
     * @param puid : 目标用户Id
     */
    private void setLike(final int pos, String id, String puid) {
        //点赞请求
        mPointLikeApi.addData("id", id);                                //目标id
        mPointLikeApi.addData("puid", puid);                            //目标用户Id
        mPointLikeApi.addData("flag", "1");                             //1:点赞 2:举报
        mPointLikeApi.addData("is_reply", "0");                         //0帖子, 1回复

        mPointLikeApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    String is_agree = JSONUtil.resolveJson(data.data, "is_agree");
                    Log.e(TAG, "is_agree  ==== " + is_agree);
                    int agreeNum = mListDatas.get(pos).getAgree_num();
                    if ("1".equals(is_agree)) {
                        agreeNum++;
                    } else {
                        agreeNum--;
                    }
                    mListDatas.get(pos).setIsAgree(is_agree);
                    mListDatas.get(pos).setAgree_num(agreeNum < 0 ? 0 : agreeNum);

                    setLocalRefresh(pos, LIKE_CLICK);

                    mFunctionManager.showShort(data.message);
                }
            }
        });
    }

    /***
     * 咨询设置
     * @param chatData
     */
    private void setConsulting(ChatDataBean chatData) {
        String is_rongyun = chatData.getIs_rongyun();
        String hos_userid = chatData.getHos_userid();
        String event_name = chatData.getEvent_name();
        String event_pos = chatData.getEvent_pos();
        HashMap<String, String> event_params = chatData.getEvent_params();
        if ("3".equals(is_rongyun)) {
            YmStatistics.getInstance().tongjiApp(new EventParamData(event_name, event_pos), event_params);
            ChatDataGuiJi mTaoDetailBean = chatData.getGuiji();
            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                    .setDirectId(hos_userid)
                    .setObjId(chatData.getObj_id())
                    .setObjType(chatData.getObj_type() + "")
                    .setTitle(mTaoDetailBean.getTitle())
                    .setImg(mTaoDetailBean.getImage())
                    .setMemberPrice(mTaoDetailBean.getMember_price())
                    .setYmClass(chatData.getYmaq_class())
                    .setYmId(chatData.getYmaq_id())
                    .build();

            new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        } else {
            Toast.makeText(mContext, "该服务暂未开通在线客服功能", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 分享设置
     *
     * @param pos
     */
    private void setShare(final int pos) {
        BBsShareData shareData = mListDatas.get(pos).getShareData();
        String shareUrl = shareData.getUrl();
        String shareContent = shareData.getContent();
        String shareImgUrl = shareData.getImg();
        String askOrshare1 = shareData.getAskorshare();
        ShareWechat mWechat = shareData.getWechat();
        ShareDetailPictorial mPictorial = shareData.getPictorial();

        myShareBoardlistener
                .setSinaText("#整形#" + shareContent + shareUrl + "分享自@悦美整形APP")
                .setSinaThumb(new UMImage(mContext, shareImgUrl))
                .setSmsText(shareContent + "，" + shareUrl)
                .setTencentUrl(shareUrl)
                .setTencentTitle(shareContent)
                .setTencentThumb(new UMImage(mContext, shareImgUrl))
                .setTencentDescription(shareContent)
                .setTencentText(shareContent)
                .setWechatData(mWechat)
                .setPictorialData(mPictorial)
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {
                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            mFunctionManager.showShort("分享成功啦");
                        }

                        int shareNumer = mListDatas.get(pos).getShare_num() + 1;
                        mListDatas.get(pos).setShare_num(shareNumer);
                        setLocalRefresh(pos, SHARE_NUMBER);

                        //增加积分
                        if (Utils.isLogin()) {
                            Utils.sumitHttpCode(mContext, "9");
                        }

                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                        mFunctionManager.showShort("分享失败啦");
                    }
                });

        buttomDialogView.show();
    }

    /**
     * 点赞设置
     *
     * @param holder
     * @param videoListData
     */
    private void setLikeData(@NonNull ViewHolder holder, VideoListData videoListData) {
        Log.e(TAG, "videoListData.getIsAgree() == " + videoListData.getIsAgree());
        Log.e(TAG, "点赞数 == " + videoListData.getAgree_num());
        if ("1".equals(videoListData.getIsAgree())) {
            mFunctionManager.setImgSrc(holder.mAgreeImage, R.drawable.video_list_view_praise);
        } else {
            mFunctionManager.setImgSrc(holder.mAgreeImage, R.drawable.video_list_view_not_praise);
        }

        int agree_num = videoListData.getAgree_num();
        String agreeNum;
        if (agree_num > 10000) {
            agreeNum = new DecimalFormat("#.0").format((float) agree_num / 10000);
            agreeNum = agreeNum + "W";
        } else if (agree_num < 10) {
            agreeNum = "赞";
        } else {
            agreeNum = agree_num + "";
        }
        holder.mAgreeNum.setText(agreeNum);
    }


    /**
     * 评论设置
     *
     * @param holder
     * @param videoListData
     */
    private void setCommentsData(@NonNull ViewHolder holder, VideoListData videoListData) {
        String answerNum = videoListData.getAnswer_num();
        Log.e(TAG, "评论数 == " + answerNum);
        if (TextUtils.isEmpty(answerNum) || "0".equals(answerNum)) {
            holder.mAnswerNum.setText("评论");
        } else {
            holder.mAnswerNum.setText(answerNum);
        }
    }

    /**
     * 分享设置
     *
     * @param holder
     * @param videoTao
     */
    private void setShareData(ViewHolder holder, VideoListData videoTao) {
        Log.e(TAG, "分享数 == " + videoTao.getShare_num());

        int share_num = videoTao.getShare_num();
        String shareNum;
        if (share_num >= 10000) {
            shareNum = new DecimalFormat("#.0").format((float) share_num / 10000);
            shareNum = shareNum + "W";
        } else if (share_num < 10) {
            shareNum = "分享";
        } else {
            shareNum = share_num + "";
        }

        holder.mShareNum.setText(shareNum);
    }

    /**
     * 获取数据列表
     *
     * @return
     */
    public List<VideoListData> getListDatas() {
        return mListDatas;
    }

    /**
     * 添加视频数据
     *
     * @param data
     */
    public void addData(List<VideoListData> data) {
        mListDatas.addAll(data);
    }

    /**
     * 获取当前播放中视频的数据
     *
     * @return
     */
    public PlayingVideoData getPlayingData() {
        return mPlayingData;
    }

    /**
     * 局部刷新
     *
     * @param pos
     * @param payload
     */
    public void setLocalRefresh(int pos, String payload) {
        notifyItemChanged(pos, payload);
    }

    /**
     * 设置当前播放中视频的数据
     *
     * @param mPlayingData
     */
    public void setPlayingData(PlayingVideoData mPlayingData) {
        this.mPlayingData = mPlayingData;
    }

    private ItemCommentClickListener itemCommentClickListener;

    public interface ItemCommentClickListener {
        void onItemCommentClick(View v, int pos);
    }

    public void setOnItemCommentClickListener(ItemCommentClickListener itemCommentClickListener) {
        this.itemCommentClickListener = itemCommentClickListener;
    }
}
