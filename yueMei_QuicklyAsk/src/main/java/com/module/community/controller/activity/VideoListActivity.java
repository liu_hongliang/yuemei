package com.module.community.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.bean.SumbitPhotoData;
import com.module.commonview.view.CommentDialog;
import com.module.commonview.view.DYLoadingView;
import com.module.commonview.view.DiaryCommentDialogView;
import com.module.community.controller.adapter.VideoListAdapter;
import com.module.community.controller.api.VideoListApi;
import com.module.community.model.bean.PlayingVideoData;
import com.module.community.model.bean.VideoListData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.PostMsgEvent;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.umeng.socialize.UMShareAPI;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class VideoListActivity extends YMBaseActivity {

    @BindView(R.id.activity_video_back)
    ImageView mVideoBack;
    @BindView(R.id.activity_video_gestures_image)
    ImageView mVideoGesturesImage;                  //上滑提示
    @BindView(R.id.activity_video_pager)
    RecyclerView mVideoPager;
    @BindView(R.id.dialog)
    DYLoadingView dialog;

    //评论请求的flag
    public static final int REQUEST_NUMBER = 120;

    //视频进度返回flag
    public static final int RETURN_VIDEO_PROGRESS = 100;

    private VideoListAdapter videoListAdapter;
    private PagerSnapHelper snapHelper;
    private LinearLayoutManager layoutManager;
    private List<VideoListData> videoListdatas = new ArrayList<>();
    private String TAG = "VideoListActivity";
    private VideoListApi videoListApi;
    private int mPage = 1;             //加载页码
    private String id;
    private String flag;
    private String img;
    private int progress;               //默认从0开始播放
    private int selectedPosition = 0;   //当前播放视频下标
    private CommentDialog commentDialog;
    private DiaryCommentDialogView mDiaryCommentDialogView;
    private SumbitPhotoData sumbitPhotoData;
    private int mAnswerNum;

    @Override
    public void onClick(View v) {
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onClick(v);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(PostMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 0:
                //评论数-1
                VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
                mAnswerNum = Integer.parseInt(videoListAdapter.getListDatas().get(videoViewHolder.getLayoutPosition()).getAnswer_num());
                mAnswerNum = mAnswerNum - (int) msgEvent.getData();
                if (mAnswerNum < 0) {
                    mAnswerNum = 0;
                }
                videoListAdapter.getListDatas().get(videoViewHolder.getLayoutPosition()).setAnswer_num(mAnswerNum + "");
                videoListAdapter.setLocalRefresh(videoViewHolder.getLayoutPosition(), videoListAdapter.COMMENTS_NUMBER);
                break;
            case 1:
                //评论数+1
                VideoListAdapter.ViewHolder videoViewHolder2 = getVideoViewHolder();
                mAnswerNum = Integer.parseInt(videoListAdapter.getListDatas().get(videoViewHolder2.getLayoutPosition()).getAnswer_num());
                mAnswerNum = mAnswerNum + 1;
                videoListAdapter.getListDatas().get(videoViewHolder2.getLayoutPosition()).setAnswer_num(mAnswerNum + "");
                videoListAdapter.setLocalRefresh(videoViewHolder2.getLayoutPosition(), videoListAdapter.COMMENTS_NUMBER);
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //判断是否有输入框 有的话弹出键盘(选择图片跳页再回来)
        if(mDiaryCommentDialogView != null && mDiaryCommentDialogView.isShowing()){
            mDiaryCommentDialogView.showKeyboard();
        }
        if(commentDialog != null
                && commentDialog.isShowing()
                && commentDialog.getDiaryCommentDialogView() != null
                && commentDialog.getDiaryCommentDialogView().isShowing()){
            commentDialog.getDiaryCommentDialogView().showKeyboard();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_list;
    }

    @Override
    protected void initView() {
        //修改状态栏字体颜色为白色
        QMUIStatusBarHelper.setStatusBarDarkMode(mContext);

        id = getIntent().getStringExtra("id");                              //帖子id，可以不传
        flag = getIntent().getStringExtra("flag");                          //列表类型标识 ，1加载单条，可以不传
        progress = getIntent().getIntExtra("progress", 0);     //视频进度
        dialog.start();

        Log.e(TAG, "flag == " + flag);

        //关闭
        ViewGroup.MarginLayoutParams backParams = (ViewGroup.MarginLayoutParams) mVideoBack.getLayoutParams();
        backParams.topMargin = statusbarHeight + Utils.dip2px(12);
        mVideoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //如果不是单个视频预览，上滑提示显示设置
        if (!"1".equals(flag)) {
            String slidePrompt = mFunctionManager.loadStr(FinalConstant.VIDEO_LIST_SLIDE_PROMPT, "0");
            if ("0".equals(slidePrompt)) {
                mFunctionManager.saveStr(FinalConstant.VIDEO_LIST_SLIDE_PROMPT, "1");
                mVideoGesturesImage.setVisibility(View.VISIBLE);

                Glide.with(mContext).load(R.drawable.video_list_gestures).into(mVideoGesturesImage);

                mVideoGesturesImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mVideoGesturesImage.setVisibility(View.GONE);
                    }
                });
            } else {
                mVideoGesturesImage.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void initData() {
        videoListApi = new VideoListApi();
        sumbitPhotoData = new SumbitPhotoData();
        loadData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
        if (videoViewHolder != null) {
            videoViewHolder.mVideoView.videoRestart();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause....");
        VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
        if (videoViewHolder != null) {
            videoViewHolder.mVideoView.videoSuspend();
            if (videoListAdapter != null) {
                PlayingVideoData playingData = videoListAdapter.getPlayingData();
                int currentPosition = videoViewHolder.mVideoView.getCurrentPosition() / 1000;
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BOFANG_TIME, "0", currentPosition + ""), playingData.getData().getEvent_params());
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BOFANG_OUT, "1"), playingData.getData().getEvent_params());
            }
        }
    }

    @Override
    public void onDestroy() {
//        Utils.clearVideoCacheFile();                  //清除缓存视频。目前不清除
        EventBus.getDefault().unregister(this);
        VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
        if (videoViewHolder != null) {
            videoViewHolder.mVideoView.videoStopPlayback();
            if (videoListAdapter != null) {
                PlayingVideoData playingData = videoListAdapter.getPlayingData();
                int currentPosition = videoViewHolder.mVideoView.getCurrentPosition() / 1000;
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BOFANG_TIME, "0", currentPosition + ""), playingData.getData().getEvent_params());
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BOFANG_OUT, "1"), playingData.getData().getEvent_params());
            }
        }

        if (mDiaryCommentDialogView != null) {
            mDiaryCommentDialogView.dismiss();
        }

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //qq回调单独处理
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        //评论刷新处理。暂时不做
//        if (requestCode == REQUEST_NUMBER && resultCode == CommentsListActivity.RETURN_NUMBER && data != null) {
//            VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
//            if (videoViewHolder != null && videoListAdapter != null) {
//                String answer_num = videoListAdapter.getListDatas().get(videoViewHolder.getLayoutPosition()).getAnswer_num();
//                int number = data.getIntExtra("number", Integer.parseInt(answer_num));
//                videoListAdapter.getListDatas().get(videoViewHolder.getLayoutPosition()).setAnswer_num(number + "");
//                videoListAdapter.setLocalRefresh(videoViewHolder.getLayoutPosition(), videoListAdapter.COMMENTS_NUMBER);
//            }
//        }

        switch (requestCode) {
            case DiaryCommentDialogView.PHOTO_IF_PUBLIC:            //公开还是私密设置
                if (data != null) {
                    String type = data.getStringExtra("type");
                    if (type.equals("1")) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("私密", "1");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("私密", "1");
                        }
                    } else {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setIsPublic("公开", "0");
                        } else {
                            mDiaryCommentDialogView.setIsPublic("公开", "0");
                        }
                    }
                }
                break;
            case DiaryCommentDialogView.REQUEST_CODE:                   //照片回调设置
                Log.e(TAG, "mContext.RESULT_OK ==" + Activity.RESULT_OK);
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        if (commentDialog != null && commentDialog.isShowing()) {
                            commentDialog.getDiaryCommentDialogView().setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            commentDialog.getDiaryCommentDialogView().gridviewInit();
                        } else {
                            mDiaryCommentDialogView.setmResults(data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS));
                            mDiaryCommentDialogView.gridviewInit();
                        }
                    }
                }
                break;
        }
    }

    /**
     * 加载数据
     */
    private void loadData() {
        //要选中的视频帖子Id
        if (!TextUtils.isEmpty(id)) {
            videoListApi.addData("id", id);
        }
        //列表类型标识
        if (!TextUtils.isEmpty(flag)) {
            videoListApi.addData("flag", flag);
        }
        videoListApi.addData("page", mPage + "");
        final long time = System.currentTimeMillis();
        videoListApi.getCallBack(mContext, videoListApi.getHashMap(), new BaseCallBackListener<List<VideoListData>>() {
            @Override
            public void onSuccess(List<VideoListData> data) {
                Log.i("time", System.currentTimeMillis() - time + "");
                mPage++;
                videoListdatas = data;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (videoListAdapter == null) {
                            setApater();
                        } else {
                            videoListAdapter.addData(videoListdatas);
                        }
                        mVideoBack.setVisibility(View.VISIBLE);
                        dialog.stop();
                        dialog.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    /**
     * 设置适配器
     */
    private void setApater() {
//        snapHelper = new PagerSnapHelper();
//        snapHelper.attachToRecyclerView(mVideoPager);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mVideoPager.setLayoutManager(layoutManager);
        videoListAdapter = new VideoListAdapter(mContext, videoListdatas, progress);
        mVideoPager.setAdapter(videoListAdapter);
        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mVideoPager);
        videoListAdapter.setOnItemCommentClickListener(new VideoListAdapter.ItemCommentClickListener() {
            @Override
            public void onItemCommentClick(View v, int pos) {
                if (Integer.parseInt(videoListdatas.get(pos).getAnswer_num()) > 0) {
                    //有评论出弹窗
                    mAnswerNum = Integer.parseInt(videoListdatas.get(pos).getAnswer_num());
                    commentDialog = new CommentDialog(mContext, videoListdatas.get(pos).getId(), videoListdatas.get(pos).getUserdata().getId(), videoListdatas.get(pos).getAskorshare(), videoListdatas.get(pos).getP_id(), mAnswerNum);
                    if (commentDialog != null) {
                        commentDialog.show();
                    }
                } else {
                    //没评论出输入框
                    startComments(pos);
                }
            }
        });
        addListener();
    }

    private void startComments(int pos) {
        if (Utils.isLoginAndBind(mContext)) {
            sumbitPhotoData.setUserid(videoListdatas.get(pos).getUserdata().getId());
            sumbitPhotoData.setQid(id);
            sumbitPhotoData.setAskorshare(videoListdatas.get(pos).getAskorshare());
            mDiaryCommentDialogView = new DiaryCommentDialogView(mContext, sumbitPhotoData);
            mDiaryCommentDialogView.showDialog();
            mDiaryCommentDialogView.setPicturesChooseGone(false);

            //评论提交完成回调
            mDiaryCommentDialogView.setOnSubmitClickListener(new DiaryCommentDialogView.OnSubmitClickListener() {
                @Override
                public void onSubmitClick(String id, ArrayList<String> imgLists, String content) {
                    VideoListAdapter.ViewHolder videoViewHolder2 = getVideoViewHolder();
                    mAnswerNum = Integer.parseInt(videoListAdapter.getListDatas().get(videoViewHolder2.getLayoutPosition()).getAnswer_num());
                    mAnswerNum = mAnswerNum + 1;
                    videoListAdapter.getListDatas().get(videoViewHolder2.getLayoutPosition()).setAnswer_num(mAnswerNum + "");
                    videoListAdapter.setLocalRefresh(videoViewHolder2.getLayoutPosition(), videoListAdapter.COMMENTS_NUMBER);
                }
            });
        }
    }

    /***
     * 设置监听
     */
    private void addListener() {
        mVideoPager.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE://停止滚动

                        VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder(recyclerView);
                        Log.e(TAG, "停止滚动 == " + videoViewHolder);
                        if (videoViewHolder != null && !videoViewHolder.mVideoView.isPlaying()) {
                            //如果获取的videoViewHolder不是null，且视频是暂停状态的
                            Log.e(TAG, "layoutPosition() == " + videoViewHolder.getLayoutPosition());
                            PlayingVideoData playingData = videoListAdapter.getPlayingData();
                            VideoListData videoListData = videoListAdapter.getListDatas().get(videoViewHolder.getLayoutPosition());
                            if (playingData != null && !playingData.getData().getId().equals(videoListData.getId())) {
                                //如果当前播放的数据和现在正在播放的id不是相同。说明已经切换到了下一个视频
                                playingData.getViewHolder().mVideoView.videoStopPlayback();             //关闭上一个视频
                                playingData.getViewHolder().mTaoClick.setVisibility(View.GONE);          //左边淘容器隐藏
                                videoListData.setSelected_tao(null);
                                playingData.setShowTao(true);
                                playingData.setUploadLookVideo(false);
                                playingData.setData(videoListData);
                                playingData.setViewHolder(videoViewHolder);
                                videoViewHolder.ll_video_end.setVisibility(View.GONE);
                                videoViewHolder.mVideoView.videoStartPlayer();
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VIDEO_BOFANG, "1"), videoListData.getEvent_params());

                                //上滑还是下滑
                                if (selectedPosition != videoViewHolder.getLayoutPosition()) {
                                    if (isUpOrDown(videoViewHolder.getLayoutPosition())) {
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEXT_VIDEO, "1"), videoListData.getEvent_params());
                                    } else {
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_VEDIO_SLIDING, "1"), videoListData.getEvent_params());
                                    }
                                }

                                if (videoListAdapter.getListDatas().size() - (videoViewHolder.getLayoutPosition() + 1) < 5) {
                                    //如果后边的的数据不足5条时
                                    loadData();
                                }
                            }
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING://拖动
                        Log.e(TAG, "拖动 == " + getVideoViewHolder(recyclerView));
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING://惯性滑动
                        Log.e(TAG, "惯性滑动 == " + getVideoViewHolder(recyclerView));
                        break;
                }
            }
        });
    }

    /**
     * 获取Adapter中的Holder
     *
     * @return
     */
    private VideoListAdapter.ViewHolder getVideoViewHolder() {
        return getVideoViewHolder(mVideoPager);
    }

    private VideoListAdapter.ViewHolder getVideoViewHolder(RecyclerView recyclerView) {
        if (snapHelper != null && layoutManager != null) {
            View view = snapHelper.findSnapView(layoutManager);
            RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
            if (viewHolder instanceof VideoListAdapter.ViewHolder) {
                return (VideoListAdapter.ViewHolder) viewHolder;
            }
        }
        return null;
    }

    /**
     * 判断是上滑还是下滑
     *
     * @param layoutPosition
     * @return true:上滑，false：下滑
     */
    private boolean isUpOrDown(int layoutPosition) {
        boolean isUpOrDown;
        if (selectedPosition - layoutPosition > 0) {
            isUpOrDown = false;
        } else {
            isUpOrDown = true;
        }
        Log.e(TAG, "isUpOrDown == " + isUpOrDown);
        selectedPosition = layoutPosition;
        return isUpOrDown;
    }

    @Override
    public void onBackPressed() {
        VideoListAdapter.ViewHolder videoViewHolder = getVideoViewHolder();
        if (videoViewHolder != null) {
            int progress = videoViewHolder.mVideoView.getCurrentPosition();
            Intent intent = new Intent();
            intent.putExtra(" progress", progress);
            setResult(RETURN_VIDEO_PROGRESS, intent);
        }
        super.onBackPressed();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //拦截返回键
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            //判断触摸UP事件才会进行返回事件处理
            if (event.getAction() == KeyEvent.ACTION_UP) {
                onBackPressed();
            }
            //只要是返回事件，直接返回true，表示消费掉
            return true;
        }
        return super.dispatchKeyEvent(event);
    }
}
