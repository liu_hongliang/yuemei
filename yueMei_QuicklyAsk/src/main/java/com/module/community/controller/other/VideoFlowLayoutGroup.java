package com.module.community.controller.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.module.commonview.module.bean.DiaryTagList;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

/**
 * 视频流列表中的标签
 * Created by 裴成浩 on 2019/5/9
 */
public class VideoFlowLayoutGroup {

    private final String TAG = "VideoFlowLayoutGroup";
    private final int mColor;
    private final int mSize;                //sp
    private Context mContext;
    private FlowLayout mPostFlowLayout;
    private List<DiaryTagList> mTags;


    public VideoFlowLayoutGroup(Context context, FlowLayout postFlowLayout, List<DiaryTagList> tags) {
       this(context,postFlowLayout,tags,Utils.setCustomColor("#BD7974"),15);
    }
    public VideoFlowLayoutGroup(Context context, FlowLayout postFlowLayout, List<DiaryTagList> tags, int color,int size) {
        this.mContext = context;
        this.mPostFlowLayout = postFlowLayout;
        this.mTags = tags;
        this.mColor = color;
        this.mSize = size;
        Log.e(TAG, "mTags == " + mTags.size());
        initView();
    }

    /**
     * 初始化样式
     */
    @SuppressLint("SetTextI18n")
    private void initView() {
        mPostFlowLayout.removeAllViews();
        for (int i = 0; i < mTags.size(); i++) {
            DiaryTagList data = mTags.get(i);

            TextView view = new TextView(mContext);
            view.setTextColor(mColor);
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP,mSize);
            view.setText("#" + data.getName());

            mPostFlowLayout.addView(view, i);

            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mTags.size(); i++) {
                if (i == selected) {
                    if (clickCallBack != null) {
                        clickCallBack.onClick(v, i, mTags.get(i));
                    }
                }
            }
        }
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos, DiaryTagList data);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }
}
