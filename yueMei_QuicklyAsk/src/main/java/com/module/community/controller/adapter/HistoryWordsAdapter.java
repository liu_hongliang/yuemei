/**
 * 
 */
package com.module.community.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.community.model.bean.HistorySearchWords;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 搜索历史记录适配器
 * 
 * @author Robin
 * 
 */
public class HistoryWordsAdapter extends BaseAdapter {

	private final String TAG = "HistoryWordsAdapter";

	private List<HistorySearchWords> mHistorySearchWords = new ArrayList<HistorySearchWords>();
	private Context mContext;
	private LayoutInflater inflater;
	private HistorySearchWords HistorySearchWords;
	ViewHolder viewHolder;

	public HistoryWordsAdapter(Context mContext,
			List<HistorySearchWords> mHistorySearchWords) {
		this.mContext = mContext;
		this.mHistorySearchWords = mHistorySearchWords;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView mPart1NameTV;
	}

	@Override
	public int getCount() {
		// return mHistorySearchWords.size();
		if (mHistorySearchWords.size() >= 5) {
			return 5;
		} else {
			return mHistorySearchWords.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return mHistorySearchWords.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_history_text, null);
			viewHolder = new ViewHolder();

			viewHolder.mPart1NameTV = convertView
					.findViewById(R.id.pop_history_item_name_tv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		HistorySearchWords = mHistorySearchWords.get(position);

		viewHolder.mPart1NameTV.setText(HistorySearchWords.getHwords());

		return convertView;
	}

	public void add(List<HistorySearchWords> infos) {
		mHistorySearchWords.addAll(infos);
	}
}
