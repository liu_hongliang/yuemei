package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatDataBean;
import com.module.commonview.module.bean.ChatDataGuiJi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.List;


/**
 * Created by 裴成浩 on 2019/8/14
 */
public class VideoListBottomDialogAdapter extends RecyclerView.Adapter<VideoListBottomDialogAdapter.ViewHolder> {

    private final Context mContext;
    private final List<HomeTaoData> mDatas;
    private final LayoutInflater mInflater;
    private final FunctionManager mFunctionManager;

    public VideoListBottomDialogAdapter(Context context, List<HomeTaoData> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(mContext);
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public VideoListBottomDialogAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.item_video_list_bottom_dialog_view, parent, false);
        return new VideoListBottomDialogAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull VideoListBottomDialogAdapter.ViewHolder holder, int position) {
        HomeTaoData taoData = mDatas.get(position);

        mFunctionManager.setRoundImageSrc(holder.img, taoData.getImg(), Utils.dip2px(7));
        holder.title.setText(taoData.getSubtitle());        //文案
        holder.price.setText(taoData.getPrice_discount());      //价格
        holder.unit.setText(taoData.getFeeScale());            //单位

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView title;
        TextView price;
        TextView unit;
        TextView chat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.item_buttom_video_list_img);
            title = itemView.findViewById(R.id.item_buttom_video_list_title);
            price = itemView.findViewById(R.id.item_buttom_video_list_price);
            unit = itemView.findViewById(R.id.item_buttom_video_list_unit);
            chat = itemView.findViewById(R.id.item_buttom_video_list_chat);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, mDatas.get(getLayoutPosition()));
                    }
                }
            });

            //咨询点击
            chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.noLoginChat()) {
                        toChatActivity();
                    } else {
                        if (Utils.isLoginAndBind(mContext)) {
                            toChatActivity();
                        }
                    }

                }
            });
        }

        private void toChatActivity() {
            ChatDataBean mChatData = mDatas.get(getLayoutPosition()).getChatData();
            String is_rongyun = mChatData.getIs_rongyun();
            String hos_userid = mChatData.getHos_userid();
            String ymaq_class = mChatData.getYmaq_class();
            String ymaq_id = mChatData.getYmaq_id();
            String obj_type = mChatData.getObj_type();
            String obj_id = mChatData.getObj_id();
            ChatDataGuiJi guiji = mChatData.getGuiji();
            String price = guiji.getPrice();
            String image = guiji.getImage();
            String member_price = guiji.getMember_price();
            String url = guiji.getUrl();
            String title = guiji.getTitle();
            HashMap<String, String> event_params = mDatas.get(getLayoutPosition()).getChatData().getEvent_params();
            if ("3".equals(is_rongyun)) {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(hos_userid)
                        .setObjId(obj_id)
                        .setObjType(obj_type)
                        .setYmClass(ymaq_class)
                        .setYmId(ymaq_id)
                        .setPrice(price)
                        .setMemberPrice(member_price)
                        .setImg(image)
                        .setUrl(url)
                        .setTitle(title)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);

                Log.e("dasdasasdasd", "event_params == " + event_params.toString());
                YmStatistics.getInstance().tongjiApp(event_params);
            } else {
                mFunctionManager.showShort("该服务暂未开通在线客服功能");
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View v, HomeTaoData data);
    }

    private OnItemClickListener onEventClickListener;

    public void setOnItemClickListener(OnItemClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
