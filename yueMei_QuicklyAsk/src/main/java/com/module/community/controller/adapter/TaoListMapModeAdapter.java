package com.module.community.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.module.base.view.FunctionManager;
import com.module.home.model.bean.SearchTao;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/5/24
 */
public class TaoListMapModeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mContext;
    private List<SearchTao> mDatas;
    private FunctionManager mFunctionManager;

    public TaoListMapModeAdapter(Activity context, List<SearchTao> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mFunctionManager = new FunctionManager(mContext);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_tao_list_map_mode_view, parent, false);
        return new TaoViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        setDataView((TaoViewHolder) holder, position);
    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    /**
     * 淘数据
     *
     * @param viewHolder
     * @param pos
     */
    @SuppressLint("SetTextI18n")
    private void setDataView(final TaoViewHolder viewHolder, int pos) {

        HomeTaoData taoData = mDatas.get(pos).getTao();
        if (taoData != null) {
            //设置海报的显示隐藏
            if ("1".equals(taoData.getShixiao())) {
                mFunctionManager.setRoundImageSrc(viewHolder.tao_list_image, R.drawable.sall_null_2x, Utils.dip2px(5));
            } else {
                mFunctionManager.setRoundImageSrc(viewHolder.tao_list_image, taoData.getImg(), Utils.dip2px(5));
            }
            //视频标签显示隐藏
            if ("1".equals(taoData.getIs_have_video())) {
                viewHolder.tao_list_image_video_tag.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tao_list_image_video_tag.setVisibility(View.GONE);
            }
            //搜索词高亮
            viewHolder.tao_list_title.setText("< " + taoData.getTitle() + ">" + taoData.getSubtitle());
            //设置医生
            viewHolder.tv_name.setText(taoData.getDoc_name());
            //设置医院
            viewHolder.tv_hos.setText(taoData.getHos_name());

            if (TextUtils.isEmpty(taoData.getBilateral_coupons())) {
                viewHolder.tv_manjian.setVisibility(View.GONE);
            } else {
                viewHolder.tv_manjian.setText(taoData.getBilateral_coupons());
                viewHolder.tv_manjian.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(taoData.getSale_type())) {
                if (taoData.getSale_type().equals("2")) {
                    viewHolder.tv_miaosha.setText("秒杀");
                    viewHolder.tv_miaosha.setVisibility(View.VISIBLE);
                } else if (taoData.getSale_type().equals("4")) {
                    viewHolder.tv_miaosha.setText("拼团");
                    viewHolder.tv_miaosha.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tv_miaosha.setVisibility(View.GONE);
                }
            } else {
                viewHolder.tv_miaosha.setVisibility(View.GONE);
            }
            viewHolder.tao_list_price_num.setText(taoData.getPrice_discount());
        }
    }


    public List<SearchTao> getActiveData() {
        return mDatas;
    }


    /**
     * 刷新数据
     *
     * @param
     */
    public void refreshData(List<SearchTao> infos) {
        mDatas = infos;
        notifyDataSetChanged();
    }

    public void addData(List<SearchTao> infos) {
        int size = mDatas.size();
        mDatas.addAll(infos);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }

    /**
     * 淘整型数据
     */
    public class TaoViewHolder extends RecyclerView.ViewHolder {
        ImageView tao_list_image;
        ImageView tao_list_image_video_tag;
        TextView tao_list_title;
        TextView tv_name;
        TextView tv_hos;
        TextView tao_list_price_num;
        TextView tv_manjian;
        TextView tv_miaosha;

        public TaoViewHolder(@NonNull View itemView) {
            super(itemView);
            tao_list_image = itemView.findViewById(R.id.tao_list_image);
            tao_list_image_video_tag = itemView.findViewById(R.id.tao_list_image_video_tag);
            tao_list_title = itemView.findViewById(R.id.tao_list_title);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_hos = itemView.findViewById(R.id.tv_hos);
            tao_list_price_num = itemView.findViewById(R.id.tao_list_price_num);
            tv_manjian = itemView.findViewById(R.id.tv_manjian);
            tv_miaosha = itemView.findViewById(R.id.tv_miaosha);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int layoutPosition = getLayoutPosition();
                    if (onItemClickListener != null) {
                        if (layoutPosition >= 0) {
                            HomeTaoData taoList = mDatas.get(layoutPosition).getTao();
                            if (taoList != null) {
                                onItemClickListener.onItemClick(taoList, layoutPosition);
                            }
                        }
                    }
                }
            });
        }
    }


    //item点击
    public interface OnItemClickListener {
        void onItemClick(HomeTaoData taoList, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
