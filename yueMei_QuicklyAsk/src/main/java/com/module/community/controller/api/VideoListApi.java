package com.module.community.controller.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.community.model.bean.VideoListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/5/9
 */
public class VideoListApi implements BaseCallBackApi {
    private String TAG = "VideoListApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public VideoListApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.BBS, "video", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.toString());
                if ("1".equals(mData.code)) {
                    List<VideoListData> listData = JSONUtil.jsonToArrayList(mData.data, VideoListData.class);
                    listener.onSuccess(listData);
                }else {
                    listener.onSuccess(null);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }

}
