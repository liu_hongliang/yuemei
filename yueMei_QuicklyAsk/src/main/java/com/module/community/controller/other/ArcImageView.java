package com.module.community.controller.other;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.Log;

import com.quicklyask.activity.R;

/**
 * 贝塞尔曲线实现图片底部圆弧效果
 * Created by 裴成浩 on 2018/8/30.
 */
public class ArcImageView extends AppCompatImageView {
    private int mArcHeight;                 //弧形高度变化高度
    private int mMaxHeight;           //弧度最大高度
    private String TAG = "ArcImageView";
    private Path mPath;

    public ArcImageView(Context context) {
        this(context, null);
    }

    public ArcImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArcImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ArcImageView);
        mArcHeight = typedArray.getDimensionPixelSize(R.styleable.ArcImageView_arcHeight, 0);

        mMaxHeight = mArcHeight;
        mPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int startY = getHeight() - mArcHeight;
        mPath.lineTo(0, startY);
        mPath.quadTo(getWidth() / 2, getHeight() + mArcHeight, getWidth(), startY);
        mPath.lineTo(getWidth(), 0);

        canvas.clipPath(mPath);

        super.onDraw(canvas);
    }


    /**
     * 设置弧度
     */
    public void setRadian(int arcHeight) {
        this.mArcHeight = arcHeight;
        invalidate();
    }

    /**
     * 获取最大高度
     *
     * @return
     */
    public int getMaxHeight() {
        return mMaxHeight;
    }
}