package com.module.community.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.utils.TabLayoutIndicator1;
import com.module.community.controller.adapter.CommunityPagerAdapter;
import com.module.community.controller.api.HomeCommunityApi;
import com.module.community.controller.fragment.CommunityAnswerFragment;
import com.module.community.controller.fragment.CommunityPostFragment;
import com.module.community.controller.fragment.CommunityStaggeredFragment;
import com.module.community.controller.fragment.CommunityWebViewFragment;
import com.module.community.controller.other.CommunityScrollView;
import com.module.community.model.bean.HomeCommunityData;
import com.module.community.model.bean.HomeCommunityTagData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.MoreQuanZiActivity;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.bean.SearchXSData;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.activity.SelectSendPostsActivity;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.CustomDialog;
import org.kymjs.kjframe.utils.SystemTool;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import simplecache.ACache;

public class CommunityActivity extends YMBaseActivity {

    @BindView(R.id.community_image_background)
    CommunityScrollView mImageBackground;
    @BindView(R.id.community_image_background_img)
    ImageView mImageBackgroundImg;
    @BindView(R.id.community_coordinator_latoyt)
    CoordinatorLayout mCoordinatorLatoyt;
    @BindView(R.id.community_appbar_latoyt)
    AppBarLayout mAppbarLatoyt;
    @BindView(R.id.community_search)
    LinearLayout communitySearch;
    @BindView(R.id.community_search_title)
    TextView communitySearchTitle;
    @BindView(R.id.community_group)
    ImageView communityGroup;

    @BindView(R.id.community_tab_latoyt)
    TabLayout mTabLatoyt;
    @BindView(R.id.community_view_page)
    ViewPager mViewPage;

    @BindView(R.id.community_img_camer)
    ImageView mImgCamer;

    private String TAG = "CommunityActivity";
    private HomeCommunityApi homeCommunityApi;
    private HomeCommunityData mData;
    private SearchXSData searchShow;    //搜索框显示
    public List<HomeCommunityTagData> tags;
    private final int NEWS_CODE = 1100;
    private int loginPos = 0;       //登录返回后的loginPos
    private List<String> mPageTitleList = new ArrayList<>();
    private List<YMBaseFragment> mFragmentList = new ArrayList<>();
    private String mTabCa = "0";      //默认选中的tab标签id,默认是第0个
    private int tabPos = 0;           //默认选中的下标，默认是0

    @Override
    protected int getLayoutId() {
        return R.layout.activity_community;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void initView() {
        QMUIStatusBarHelper.setStatusBarDarkMode(mContext);
        mTabCa = getIntent().getStringExtra("tab_ca");
        tabPos = getIntent().getIntExtra("pos", 0);

        Log.e(TAG, "tabca == " + mTabCa);
        Log.e(TAG, "tabPos == " + tabPos);
        if (TextUtils.isEmpty(mTabCa)) {
            mTabCa = "0";
        }

        int tabselect = Cfg.loadInt(CommunityActivity.this, "tabselect", 0);
        if (tabselect != 0) {
            tabPos = tabselect;
            Cfg.saveInt(CommunityActivity.this, "tabselect", 0);
        }

        mImageBackground.setSmoothScrollingEnabled(false);      //不执行手势操作滑动

        //设置背景联动
        mAppbarLatoyt.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                mImageBackground.smoothScrollTo(0, (228 * Math.abs(verticalOffset)) / 168);
            }
        });

    }

    @Override
    protected void initData() {
        homeCommunityApi = new HomeCommunityApi(mContext);
        ACache mCache = ACache.get(mContext);

        //联网请求
        try {
            if (SystemTool.checkNet(mContext)) {
                loadingData();
            } else {
                String homejson = mCache.getAsString(HomeCommunityApi.HOMEBBS_HEAD_JSON);
                if (homejson != null) {

                    mData = JSONUtil.TransformSingleBean(homejson, HomeCommunityData.class);
                    initViewData();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "e === " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        if (!initCity.equals(Utils.getCity())) {
//            loadingData();
//        }
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        homeCommunityApi.getCallBack(mContext, homeCommunityApi.getHashMap(), new BaseCallBackListener<HomeCommunityData>() {

            @Override
            public void onSuccess(HomeCommunityData data) {
                mData = data;
                initViewData();
            }
        });
    }

    /**
     * 设置加载后的数据
     */
    private void initViewData() {

        mPageTitleList.clear();
        mFragmentList.clear();

        AdertAdv.OtherBackgroundImgBean data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);
        if (data != null && data.getImg() != null) {
            Glide.with(mContext).load(data.getImg()).placeholder(R.drawable.home_focal_placeholder).error(R.drawable.home_focal_placeholder).into(mImageBackgroundImg);
        }else{
            Glide.with(mContext).load(mData.getImg()).placeholder(R.drawable.home_focal_placeholder).error(R.drawable.home_focal_placeholder).into(mImageBackgroundImg);
        }


        tags = mData.getTag();

        Log.e(TAG, "tags == " + tags.size());
        for (int i = 0; i < tags.size(); i++) {
            HomeCommunityTagData tag = tags.get(i);
            Log.e(TAG, "name == " + tag.getName());
            Log.e(TAG, "id == " + tag.getId());

            //获取到要选中的下标
            if (!TextUtils.isEmpty(mTabCa) && mTabCa.equals(tag.getController() + tag.getAction())) {
                tabPos = i;
            }

            switch (tag.getMoban()) {
                case 1:
                    mFragmentList.add(CommunityStaggeredFragment.newInstance(tag));
                    break;
                case 2:
                    mFragmentList.add(CommunityPostFragment.newInstance(tag));
                    break;
                case 3:
                    mFragmentList.add(CommunityAnswerFragment.newInstance(tag));
                    break;
                case 4:
                    mFragmentList.add(CommunityWebViewFragment.newInstance(tag));
                    break;
            }

            mPageTitleList.add(tag.getName());
        }

        CommunityPagerAdapter mPagerAdapter = new CommunityPagerAdapter(mContext, getSupportFragmentManager(), mFragmentList, mPageTitleList);
        mViewPage.setAdapter(mPagerAdapter);
        mTabLatoyt.setupWithViewPager(mViewPage);

        //设置自定义的item
        for (int i = 0; i < mTabLatoyt.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLatoyt.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(mPagerAdapter.getTabView(i, tags.get(i).getIs_hot()));
                if (tab.getCustomView() != null) {
                    View tabView = (View) tab.getCustomView().getParent();
                    tabView.setTag(i);
                }
            }
        }

        //设置指示器间距
        TabLayoutIndicator1.newInstance().reflex(mTabLatoyt, Utils.dip2px(15), Utils.dip2px(15), tags);

        //设置默认选中项
        Log.e(TAG, "tabPos === " + tabPos);
        mTabLatoyt.getTabAt(tabPos).select();
        tabSelected(mTabLatoyt.getTabAt(tabPos));

        mTabLatoyt.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabSelected...");
                tabSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabUnselected...");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabReselected...");
                needToLog(tab);
            }
        });

        //设置点击事件
        setMultiOnClickListener(communitySearch, communityGroup, mImgCamer);
//        initSearhShowData();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //首次进入 调转提问
        if (Cfg.loadStr(mContext, "first_into", "0").equals("0")) {
//            if (Utils.isLoginAndBind(mContext)) {
                Intent intent = new Intent();
                intent.putExtra("cateid", "0");
                intent.setClass(mContext, SelectSendPostsActivity.class);
                startActivity(intent);
                Utils.tongjiApp(mContext, "forum_write", "forum", "forum", "");
                Cfg.saveStr(mContext, "first_into", "1");
//            }

        }
    }

    /**
     * 选中时的调用
     *
     * @param tab
     */
    private void tabSelected(TabLayout.Tab tab) {

        tab.getPosition();
        HomeCommunityTagData tagData = tags.get(tab.getPosition());

        needToLog(tab);

        //动态设置hot标签
        View tabView = (View) tab.getCustomView().getParent();
        ImageView mImageView = tabView.findViewById(R.id.comminoty_tab_hot);
        if (tagData.getIs_hot() == 1 && mImageView.getVisibility() == View.VISIBLE) {
            mImageView.setVisibility(View.GONE);
        }
    }

    /**
     * 需要登录的item
     *
     * @param tab
     */
    private void needToLog(TabLayout.Tab tab) {
        Log.e(TAG, "tab.getPosition() == " + tab.getPosition());
        HomeCommunityTagData tagData = tags.get(tab.getPosition());
        int isLogin = tagData.getIs_login();
        if (isLogin == 1 && !Utils.isLogin()) {
            loginPos = tab.getPosition();
            Intent intent = new Intent(mContext, LoginActivity605.class);
            startActivityForResult(intent, NEWS_CODE);
        }
    }

//    private void initSearhShowData() {
//        Log.e(TAG, "FinalConstant.SEARCH_SHOW_DATA === " + FinalConstant.SEARCH_SHOW_DATA);
//        new SearchShowDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<SearchXSData>() {
//            @Override
//            public void onSuccess(SearchXSData searchXSData) {
//                Log.d(TAG, "onSuccess");
//                searchShow = searchXSData;
//
//            }
//        });
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case NEWS_CODE:
                Log.e(TAG, "loginPos === " + loginPos);
                if (Utils.isLoginAndBind(mContext)) {
                    if (mFragmentList.get(loginPos) instanceof CommunityPostFragment) {
                        ((CommunityPostFragment) mFragmentList.get(loginPos)).loadingData(true);
                    }
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.community_search:     //搜索
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_SEARCH, "forum"));

                Intent it2 = new Intent(mContext, SearchAllActivity668.class);
                it2.putExtra(SearchAllActivity668.POSITION, 2);
                it2.putExtra(SearchAllActivity668.TYPE, "3");
                it2.putExtra(SearchAllActivity668.KEYS, "");
                startActivity(it2);

                break;
            case R.id.community_group:      //话题
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "huati"));
                Intent it = new Intent(mContext, MoreQuanZiActivity.class);
                it.putParcelableArrayListExtra("listobj", mData.getParts());
                startActivity(it);
                break;
            case R.id.community_img_camer:      //写日记
                if (Utils.isFastDoubleClick()) {
                    return;
                }
//                if (Utils.isLoginAndBind(mContext)) {
                    Intent intent = new Intent();
                    intent.putExtra("cateid", "0");
                    intent.setClass(mContext, SelectSendPostsActivity.class);
                    startActivity(intent);

                    Utils.tongjiApp(mContext, "forum_write", "forum", "forum", "");
//                }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog1 = new CustomDialog(getParent(), R.style.mystyle, R.layout.customdialog);
            dialog1.setCanceledOnTouchOutside(false);
            dialog1.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
