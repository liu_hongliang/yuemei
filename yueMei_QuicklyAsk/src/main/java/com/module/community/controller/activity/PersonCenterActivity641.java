package com.module.community.controller.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.controller.fragment.PersonCenterFragment;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.adapter.MyFocusPagerAdapter;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 个人中心
 * Created by 裴成浩 on 2018/5/02.
 */
public class PersonCenterActivity641 extends AppCompatActivity {

    private CollapsingToolbarLayout mCollapsing;

    //头布局
    private ImageView headView;                     //头像
    private ImageView headViewFlag;                     //用户标识
    private TextView nameTv;                            //昵称
    private ImageView sexIv;                            //性别
    private TextView cityTv;                            //所在城市
    private LinearLayout mFans;                        //关注
    private ImageView imgageFans;                        //关注图标
    private TextView mContentFans;                        //关注文案

    //标题
    private Toolbar mToolbar;
    private ImageView back1;               //返回按钮
    private TextView titleName;            //标题
    private Button sixinRly;               //私信按钮

    private TabLayout mTabLayout;
    private ViewPager mViewpager;

    //其他变量
    private String TAG = "PersonCenterActivity641";
    private PersonCenterActivity641 mContext;
    private PageJumpManager pageJumpManager;
    private String mUid;         //当前的用户id
    private String myId;        //查看的用户id
    private LoadingProgress mDialog;
    private BBsDetailUserInfoApi mPersonCenterApi;
    private IsFocuData mIsFocuData;         //是否关注
    private FocusAndCancelData mfocusAndCancelData; //关注和取消关注
    private List<String> mPageTitleList = new ArrayList<>();
    private List<Fragment> mFragmentList = new ArrayList<>();
    private MyFocusPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_center641);

        mContext = PersonCenterActivity641.this;
        pageJumpManager = new PageJumpManager(mContext);
        mUid = Utils.getUid();
        myId = getIntent().getStringExtra("id");
        mDialog = new LoadingProgress(mContext);
        mPersonCenterApi = new BBsDetailUserInfoApi();

        initView();
        initCallback();
        getUserInfo();

        if (Utils.isLoginAndBind()) {
            isFocu();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (mfocusAndCancelData != null) {
            intent.putExtra("focus", mfocusAndCancelData.getIs_following());
        }
        setResult(100, intent);
        super.onBackPressed();
    }


    /**
     * 初始化ui
     */
    private void initView() {
        mCollapsing = findViewById(R.id.person_collapsing_toolbar);

        headView = findViewById(R.id.self_person_head_image_iv_1);
        headViewFlag = findViewById(R.id.self_person_head_flag);
        nameTv = findViewById(R.id.self_person_name_tv_1);
        sexIv = findViewById(R.id.self_name_sex_tv_1);
        cityTv = findViewById(R.id.self_person_city_tv1_1);
        mFans = findViewById(R.id.person_fans);
        imgageFans = findViewById(R.id.person_imgage_fans);
        mContentFans = findViewById(R.id.person_center_fans);

        mToolbar = findViewById(R.id.person_toolbar);
        back1 = findViewById(R.id.title_bar_back1);
        titleName = findViewById(R.id.title_name1);
        sixinRly = findViewById(R.id.person_sixin_rly1);

        mTabLayout = findViewById(R.id.person_centent_tab);
        mViewpager = findViewById(R.id.person_centent_pager);

        setSupportActionBar(mToolbar);

        mCollapsing.setExpandedTitleColor(Color.parseColor("#00ffffff"));//设置还没收缩时状态下字体颜色
        mCollapsing.setCollapsedTitleTextColor(Color.parseColor("#636a76"));//设置收缩后Toolbar上字体的颜色
        mCollapsing.setCollapsedTitleGravity(Gravity.CENTER); //设置收缩后标题的位置
        mCollapsing.setExpandedTitleGravity(Gravity.CENTER);////设置展开后标题的位置


        mPageTitleList.add("日记");
        mPageTitleList.add("帖子");

        mFragmentList.add(PersonCenterFragment.newInstance(myId, 0));
        mFragmentList.add(PersonCenterFragment.newInstance(myId, 1));

        mPagerAdapter = new MyFocusPagerAdapter(mContext, getSupportFragmentManager(), mFragmentList, mPageTitleList);
        mViewpager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewpager);

        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
    }

    /**
     * 回调监听
     */
    private void initCallback() {
        //返回按钮
        back1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        //私信按钮
        sixinRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Utils.noLoginChat()) {
                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                            .setDirectId(myId)
                            .setObjId("0")
                            .setObjType("0")
                            .setYmClass("0")
                            .setYmId("0")
                            .build();
                    pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                } else {
                    if (Utils.isLoginAndBind(mContext)){
                        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                .setDirectId(myId)
                                .setObjId("0")
                                .setObjType("0")
                                .setYmClass("0")
                                .setYmId("0")
                                .build();
                        pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                    }
                }
            }
        });

        //关注点击
        mFans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    switch (mIsFocuData.getFolowing()) {
                        case "0":          //改为已关注
                            FocusAndCancel();
                            setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                            break;
                        case "1":          //改为未关注
                        case "2":          //改为未关注
                            showDialogExitEdit();
                            break;
                    }
                }
            }
        });
    }

    /**
     * 获取用户信息
     */
    private void getUserInfo() {

        Map<String, Object> maps = new HashMap<>();
        maps.put("id", myId);
        maps.put("uid", mUid);

        mPersonCenterApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    String id = userData.get_id();
                    String img = userData.getImg();
                    String nickName = userData.getNickname();
                    String province = userData.getProvince();
                    String city = userData.getCity();
                    String sex = userData.getSex();
                    String daren = userData.getTalent();

                    if (daren != null && !daren.equals("0")) {
                        headViewFlag.setVisibility(View.VISIBLE);

                        if (daren.equals("10")) {
                            headViewFlag.setBackgroundResource(R.drawable.yuemei_officia_listl);
                        } else if (daren.equals("11")) {
                            headViewFlag.setBackgroundResource(R.drawable.talent_big);
                        } else if (daren.equals("13") || daren.equals("12")) {
                            headViewFlag.setBackgroundResource(R.drawable.renzheng_list);
                        }

                    } else {
                        headViewFlag.setVisibility(View.GONE);
                    }
                    nameTv.setText(nickName);
                    titleName.setText(nickName);
                    mCollapsing.setTitle(nickName);

                    Log.e(TAG, "province === " + province);
                    Log.e(TAG, "city === " + city);
                    cityTv.setText(province + " " + city);

                    if (sex.equals("1")) {
                        sexIv.setBackgroundResource(R.drawable.sex_nan2x);
                    } else {
                        sexIv.setBackgroundResource(R.drawable.sex_nv2x);
                    }

                    if (img.contains("https")) {
                        img = img.replace("https", "http");
                    }

                    Glide.with(mContext).load(img).transform(new GlideCircleTransform(mContext)).into(headView);

                    if (null != userData.getSixin()) {
                        if (userData.getSixin().equals("1")) {
                            sixinRly.setVisibility(View.VISIBLE);
                        } else {
                            sixinRly.setVisibility(View.INVISIBLE);
                        }
                    }
                }else {
                    Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * 判断是否关注
     */
    private void isFocu() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", myId);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                mIsFocuData = isFocuData;

                switch (mIsFocuData.getFolowing()) {
                    case "0":          //未关注
                        setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                        break;
                    case "1":          //已关注
                        setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                        break;
                    case "2":          //互相关注
                        setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.each_focus, "#999999", "互相关注");
                        break;
                }

            }
        });
    }

    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel();
                setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    /**
     * 关注和取消关注
     */
    private void FocusAndCancel() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", myId);
        hashMap.put("type", "6");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        mfocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mfocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mIsFocuData.setFolowing("0");
                                setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                                break;
                            case "1":          //已关注
                                mIsFocuData.setFolowing("1");
                                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                                break;
                            case "2":          //互相关注
                                mIsFocuData.setFolowing("2");
                                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.each_focus, "#999999", "互相关注");
                                break;
                        }

                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 修改关注样式
     *
     * @param drawable：
     * @param image
     * @param color
     * @param text
     */
    private void setFocusView(int drawable, int image, String color, String text) {
        mFans.setBackground(ContextCompat.getDrawable(mContext, drawable));
        imgageFans.setImageResource(image);
        mContentFans.setTextColor(Color.parseColor(color));
        mContentFans.setText(text);

        ViewGroup.LayoutParams layoutParams1 = mFans.getLayoutParams();
        switch (text.length()) {
            case 2:
                layoutParams1.width = Utils.dip2px(mContext, 68);
                break;
            case 3:
                layoutParams1.width = Utils.dip2px(mContext, 78);
                break;
            case 4:
                layoutParams1.width = Utils.dip2px(mContext, 90);
                break;
        }

        mFans.setLayoutParams(layoutParams1);
    }
}
