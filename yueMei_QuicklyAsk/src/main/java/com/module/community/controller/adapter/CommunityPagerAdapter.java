package com.module.community.controller.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/7/6.
 */
public class CommunityPagerAdapter extends FragmentPagerAdapter {

    private static final String COMMUNITY_TAB_HOT = "community_tab_hot";
    private final FragmentManager fm;
    private Context mContext;
    private List<YMBaseFragment> fragmentList;
    private List<String> titleList;

    public CommunityPagerAdapter(Context mContext, FragmentManager fm, List<YMBaseFragment> mFragmentList, List<String> mPageTitleList) {
        super(fm);
        this.mContext = mContext;
        this.fragmentList = mFragmentList;
        this.titleList = mPageTitleList;
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }

    public View getTabView(int position, int isHot) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.comminoty_tab_item, null);
        TextView tv = view.findViewById(R.id.comminoty_tab_title);
        ImageView mImageView = view.findViewById(R.id.comminoty_tab_hot);

        String title = titleList.get(position);
        tv.setText(title);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mImageView.getLayoutParams();
        layoutParams.leftMargin = Utils.dip2px(10) * title.length();
        mImageView.setLayoutParams(layoutParams);

        String tabHot = Cfg.loadStr(mContext, COMMUNITY_TAB_HOT, "");

        if (isHot == 1 && (TextUtils.isEmpty(tabHot) || !tabHot.equals("" + position + isHot))) {
            mImageView.setVisibility(View.VISIBLE);
            Cfg.saveStr(mContext, COMMUNITY_TAB_HOT, "" + position + isHot);
        } else {
            mImageView.setVisibility(View.GONE);
        }

        return view;
    }

}
