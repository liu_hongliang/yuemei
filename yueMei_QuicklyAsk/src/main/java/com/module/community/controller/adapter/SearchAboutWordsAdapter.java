/**
 * 
 */
package com.module.community.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.community.model.bean.SearchAboutData;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lenovo17
 * 
 */
public class SearchAboutWordsAdapter extends BaseAdapter {

	private final String TAG = "SearchAboutWordsAdapter";

	private List<SearchAboutData> mSearchAboutData = new ArrayList<SearchAboutData>();
	private Context mContext;
	private LayoutInflater inflater;
	private SearchAboutData SearchAboutData;
	ViewHolder viewHolder;

	public SearchAboutWordsAdapter(Context mContext,
			List<SearchAboutData> mSearchAboutData) {
		this.mContext = mContext;
		this.mSearchAboutData = mSearchAboutData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView mPart1NameTV;
	}

	@Override
	public int getCount() {
		return mSearchAboutData.size();
	}

	@Override
	public Object getItem(int position) {
		return mSearchAboutData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_about_text, null);
			viewHolder = new ViewHolder();

			viewHolder.mPart1NameTV = convertView
					.findViewById(R.id.pop_history_item_name_tv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		SearchAboutData = mSearchAboutData.get(position);

		viewHolder.mPart1NameTV.setText(SearchAboutData.getKeywords());

		return convertView;
	}

	public void add(List<SearchAboutData> infos) {
		mSearchAboutData.addAll(infos);
	}
}
