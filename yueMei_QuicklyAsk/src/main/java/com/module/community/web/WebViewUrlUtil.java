package com.module.community.web;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.HosImagShowActivity;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.module.api.AgainAddCarApi;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.module.api.CheckPhotoApi;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.module.bean.MainCity;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.model.bean.CalendarReminderData;
import com.module.community.view.CalendarReminderUtils;
import com.module.doctor.controller.api.CheckHospiatlAlbumApi;
import com.module.doctor.model.bean.CaseFinalPic;
import com.module.my.controller.activity.CreditActivity;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.activity.MyDaijinjuanActivity;
import com.module.my.controller.activity.PostingMessageActivity;
import com.module.my.controller.activity.ShoukuanMessageActivity;
import com.module.my.model.api.JiFenStroeApi;
import com.module.my.model.api.MemberUsersureaddressApi;
import com.module.my.model.bean.UserData;
import com.module.my.view.orderpay.ApplyMoneyBackActivity;
import com.module.my.view.orderpay.OrderMethodActivity594;
import com.module.my.view.orderpay.OrderZhiFuStatus1Activity;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyask.activity.R;
import com.quicklyask.entity.DataUrl;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.ActivitySkipUtil;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WriteNoteManager;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMMin;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.kymjs.aframe.ui.ViewInject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Response;

/**
 * type值识别工具类
 * Created by 裴成浩 on 2019/6/20
 */
public class WebViewUrlUtil {

    private String TAG = "WebViewUrlUtil";
    private static volatile WebViewUrlUtil webViewUrlUtil;

    private WebViewUrlUtil() {
    }

    public static WebViewUrlUtil getInstance() {
        if (webViewUrlUtil == null) {
            synchronized (WebViewUrlUtil.class) {
                if (webViewUrlUtil == null) {
                    webViewUrlUtil = new WebViewUrlUtil();
                }
            }
        }
        return webViewUrlUtil;
    }


    /**
     * 确认收货地址
     *
     * @param context ：
     * @param obj     ：
     * @throws JSONException ：
     */
    public void confirmGoods(final Context context, HashMap<String, String> obj) throws JSONException {
        MemberUsersureaddressApi memberUsersureaddressApi = new MemberUsersureaddressApi();
        memberUsersureaddressApi.addData("address_id", obj.get("address_id"));
        memberUsersureaddressApi.addData("id", obj.get("id"));
        memberUsersureaddressApi.getCallBack(context, memberUsersureaddressApi.getUsersureaddresstHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code) && context instanceof Activity) {
                    Intent intent = new Intent();
                    ((Activity) context).setResult(102, intent);
                    ((Activity) context).onBackPressed();
                }
            }
        });
    }

    /**
     * dialog提示（拼团是否是带支付状态）
     *
     * @param obj ：
     */
    public void showDialogSpell444(final Context context, final HashMap<String, String> obj) {

        int[] arr = {1, 2, 3, 4, 5, 6};

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; i++) {
                arr[j] = arr[j] - arr[j - 1];
                arr[j - 1] = 0;

            }
        }
        final EditExitDialog editDialog = new EditExitDialog(context, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("您已参加该团，订单尚未支付，现在去支付？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });


        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("去支付");
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    orderPayment(context, obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getUserInfo6120(final Context context) {
        BBsDetailUserInfoApi mBBsDetailApi = new BBsDetailUserInfoApi();
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", Utils.getUid());
        mBBsDetailApi.getCallBack(context, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    Intent intent = new Intent();
                    String aliPayNum = userData.getAlipay();
                    String relName = userData.getReal_name();
                    intent.putExtra("alipay", aliPayNum);
                    intent.putExtra("real_name", relName);
                    intent.setClass(context, ShoukuanMessageActivity.class);
                    if (context instanceof Activity) {
                        ((Activity) context).startActivityForResult(intent, 33);
                    } else {
                        context.startActivity(intent);
                    }
                } else {
                    Toast.makeText(context, serverData.message, Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public void orderPayment(final Context context, HashMap<String, String> obj) throws UnsupportedEncodingException {
        String order_id444 = obj.get("order_id");
        String tao_id444 = obj.get("tao_id");
        String taoname444 = URLDecoder.decode(obj.get("taoname"), "utf-8");
        String discountprice444 = obj.get("discountprice");
        String serviceid444 = obj.get("server_id");
        String order_time444 = obj.get("order_time");
        String is_repayment444 = obj.get("is_repayment");
        String is_repayment_mimo444 = obj.get("is_repayment_mimo");
        String number444 = obj.get("number");
        String isGroup444 = obj.get("is_group");
        String groupId444 = obj.get("group_id");

        Intent intent = new Intent(context, OrderMethodActivity594.class);

        Bundle bundle = new Bundle();
        bundle.putString("price", discountprice444);
        bundle.putString("tao_title", taoname444);
        bundle.putString("server_id", serviceid444);
        bundle.putString("order_id", order_id444);
        bundle.putString("taoid", tao_id444);
        bundle.putString("order_time", order_time444);
        bundle.putString("type", "2");
        bundle.putString("sku_type", "1");
        bundle.putString("is_repayment", is_repayment444);
        bundle.putString("is_repayment_mimo", is_repayment_mimo444);
        bundle.putString("weikuan", "0");
        bundle.putString("number", number444);
        bundle.putString("is_group", isGroup444);
        bundle.putString("group_id", groupId444);

        intent.putExtra("data", bundle);
        context.startActivity(intent);

    }

    /**
     * 分享设置
     *
     * @param hashMap
     */
    public void setShare6361(final Context mContext, final WebView webView, final String callback_url6361, HashMap<String, String> hashMap) {
        MyUMShareListener myUMShareListener = new MyUMShareListener((Activity) mContext);
        boolean installWeiXin = UMShareAPI.get(mContext).isInstall((Activity) mContext, SHARE_MEDIA.WEIXIN);

        if (!installWeiXin) {     //微信不存在
            MyToast.makeTextToast2(mContext, "请先安装微信", MyToast.SHOW_TIME).show();
            return;
        } else {

            UMWeb shareWeb = new UMWeb(hashMap.get("tencentUrl"));
            shareWeb.setTitle(hashMap.get("tencentTitle"));
            String shareImgUrl = hashMap.get("shareImgUrl");
            shareWeb.setThumb(new UMImage(mContext, shareImgUrl));
            shareWeb.setDescription(hashMap.get("tencentTitle"));
            new ShareAction((Activity) mContext).withText(hashMap.get("tencentText")).withMedia(shareWeb).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).setCallback(myUMShareListener).share();
        }

        myUMShareListener.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                MyToast.makeTextToast2(mContext, "分享成功啦", MyToast.SHOW_TIME).show();

                if (webView != null) {
                    webView.reload();
                }

                if (!"".equals(callback_url6361)) {
                    CookieConfig.getInstance().setCookie("https", "user.yuemei.com", "user.yuemei.com");
                    OkGo.post(callback_url6361).execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            Log.d(TAG, "s------------>" + s);
                        }
                    });
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                MyToast.makeTextToast2(mContext, "分享失败啦", MyToast.SHOW_TIME).show();
            }
        });
    }

    /**
     * 打电话 权限判断
     *
     * @param mContext
     * @param tel
     */
    public void phonePermissions(final Context mContext, final String tel) {
        //版本判断
        if (Build.VERSION.SDK_INT >= 23) {

            Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.CALL_PHONE).build(), new AcpListener() {
                @Override
                public void onGranted() {        //判断权限是否开启
                    phoneCall(tel, mContext);
                }

                @Override
                public void onDenied(List<String> permissions) {
                    ViewInject.toast("没有电话权限");
                }
            });
        } else {
            phoneCall(tel, mContext);
        }
    }

    /**
     * 拨打电话
     *
     * @param tel
     * @param mContext
     */
    private void phoneCall(String tel, Context mContext) {
        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
        t.setToNow(); // 取得系统时间。
        int hour = t.hour; // 0-23
        Log.e(TAG, "hour === " + hour);
        ViewInject.toast("正在拨打中·····");
        Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel));
        try {
            mContext.startActivity(it);
        } catch (Exception e) {
            Log.e(TAG, "e === " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 分享设置
     *
     * @param hashMap
     */
    public void setShare6352(Context mContext, HashMap<String, String> hashMap) {
        boolean installWeiXin = UMShareAPI.get(mContext).isInstall((Activity) mContext, SHARE_MEDIA.WEIXIN);

        if (!installWeiXin) {     //微信不存在
            MyToast.makeTextToast2(mContext, "请先安装微信", MyToast.SHOW_TIME).show();
        } else {
            UMMin umMin = new UMMin(hashMap.get("webpageUrl"));
            umMin.setThumb(new UMImage(mContext, hashMap.get("thumbImage")));
            umMin.setTitle(hashMap.get("title"));
            umMin.setDescription(hashMap.get("description"));
            umMin.setPath(hashMap.get("path"));
            umMin.setUserName(hashMap.get("userName"));
            new ShareAction((Activity) mContext).withMedia(umMin).setPlatform(SHARE_MEDIA.WEIXIN).setCallback(new MyUMShareListener((Activity) mContext)).share();
        }
    }

    public void sumitHttpCode5451(final Context mContext, final String flag) {
        SumitHttpAip sumitHttpAip = new SumitHttpAip();
        Map<String, Object> maps = new HashMap<>();
        maps.put("flag", flag);
        sumitHttpAip.getCallBack(mContext, maps, new BaseCallBackListener<JFJY1Data>() {
            @Override
            public void onSuccess(JFJY1Data jfjyData) {
                if (jfjyData != null) {
                    String jifenNu = jfjyData.getIntegral();
                    String jyNu = jfjyData.getExperience();

                    if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                        MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, MyToast.SHOW_TIME).show();
                    } else {
                        if (!jifenNu.equals("0")) {
                            MyToast.makeTexttext2Toast(mContext, jifenNu, 2000).show();

                            new CountDownTimer(1000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    if (mContext instanceof Activity) {
                                        ((Activity) mContext).finish();
                                    }
                                }
                            }.start();

                        } else {
                            if (!jyNu.equals("0")) {
                                MyToast.makeTexttext3Toast(mContext, jyNu, MyToast.SHOW_TIME).show();
                            }
                        }
                    }
                } else {
                    MyToast.makeTextToast2(mContext, "请求错误", MyToast.SHOW_TIME).show();
                }
            }
        });

    }

    public void daijinjuanLingqu6022(Context context, WebView webView, String jusnid) {
        daijinjuanLingqu6022(context, webView, jusnid, "", "");
    }

    /**
     * 领取优惠券
     *
     * @param jusnid
     */
    public void daijinjuanLingqu6022(final Context mContext, final WebView webView, String jusnid, final String is_fresher, final String alert) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", jusnid);
        new DaiJinJuanApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    if ("1".equals(alert)) {
                        MyToast.makeTextToast2(mContext, s.message, MyToast.SHOW_TIME).show();
                    } else {
                        showDialogExitEdit1(mContext, s.message);
                    }

                    if ("1".equals(is_fresher) && webView != null) {
                        webView.reload();
                    }
                } else {
                    MyToast.makeTextToast2(mContext, s.message, MyToast.SHOW_TIME).show();
                }
                EventBus.getDefault().post("6022");
            }
        });
    }

    private void showDialogExitEdit1(final Context mContext, String message) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setVisibility(View.VISIBLE);
        titleTv77.setText("领取成功！");
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText(message);
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("查看代金券");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, MyDaijinjuanActivity.class);
                intent.putExtra("link", "");
                mContext.startActivity(intent);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("确认");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

    }

    /**
     * 跳转到颜值币商城
     */
    public void toJifen6499(final Context mContext) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("act", "autologin");
        new JiFenStroeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (null != serverData) {
                    Log.d(TAG, "url===>" + serverData.code + "---" + serverData.data);
                    try {
                        DataUrl dataUrl = JSONUtil.TransformSingleBean(serverData.data, DataUrl.class);
                        String url = dataUrl.getUrl();
                        Intent intent = new Intent();
                        intent.setClass(mContext, CreditActivity.class);
                        intent.putExtra("navColor", "#fafafa");    //配置导航条的背景颜色，请用#ffffff长格式。
                        intent.putExtra("titleColor", "#636a76");    //配置导航条标题的颜色，请用#ffffff长格式。
                        intent.putExtra("url", url);    //配置自动登陆地址，每次需服务端动态生成。
                        mContext.startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    /**
     * 设置分享
     */
    public void setShare544(final Context mContext) {
        String shareContent = "小伙伴们，我发现了一款很棒的美容整形APP，双眼皮，隆鼻，隆胸十一项整形面面聚到，优惠尺度满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒~，想变美的妹纸们不！要！错！过！ 点击打开APP下载页";
        String shareUrl = "http://m.yuemei.com/app/ym.html";
        String sinaText = "小伙伴们，我发现了一款很棒的美容整形APP[太开心]，双眼皮，隆鼻，隆胸十一项整形面面聚到，优惠尺度满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒~，想变美的妹纸们不！要！错！过！[偷乐]APP下载地址：http://m.yuemei.com/app/ym.html（分享自@悦美整形APP）";
        String smsText = "我发现了一款很棒的美容整形APP，整形优惠满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒，快去看看吧~ APP下载地址：http://m.yuemei.com/app/ym.html";
        BaseShareView baseShareView = new BaseShareView((Activity) mContext);
        baseShareView.setShareContent(shareContent).ShareAction();

        baseShareView.getShareBoardlistener().setSinaText(sinaText).setSinaThumb(new UMImage(mContext, R.drawable.app_share)).setSmsText(smsText).setTencentUrl(shareUrl).setTencentTitle(shareContent).setTencentThumb(new UMImage(mContext, R.drawable.app_share)).setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    Toast.makeText(mContext, " 分享成功啦", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }

    public void showDialogExitEdit445(final Context mContext, final String order_id, final String paytype, final String repayment_type) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_moneyback);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv22 = editDialog.findViewById(R.id.dialog_exit_content_tv);

        if ("1".equals(repayment_type)) {
            titleTv22.setText("取消分期后，您将无法再享受此优惠价格，确定要取消分期？");
        } else {
            titleTv22.setText("退款后，您可能无法再享受此优惠价格，确定要申请退款？");
        }

        Button cancelBt22 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt22.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
        Button trueBt22 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt22.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                Intent intent = new Intent();
                intent.setClass(mContext, ApplyMoneyBackActivity.class);
                intent.putExtra("order_id", order_id);
                intent.putExtra("paytype", paytype);
                intent.putExtra("repayment_type", repayment_type);
                if (mContext instanceof Activity) {
                    ((Activity) mContext).startActivityForResult(intent, 10);
                } else {
                    mContext.startActivity(intent);
                }

                editDialog.dismiss();

            }
        });
    }

    /**
     * 查看图片
     *
     * @param context
     * @param obj
     */
    public void reviewImages(final Context context, HashMap<String, String> obj) {
        String link536 = obj.get("link");
        final String pos536 = obj.get("key");
        Log.d(TAG, "link:==" + link536);
        String[] split = link536.split("/");
        String id = split[4];
        String type = split[6];
        HashMap<String, Object> parmas = new HashMap<>();
        parmas.put("id", id);
        parmas.put("type", type);
        new CheckHospiatlAlbumApi().getCallBack(context, parmas, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Intent intent536 = new Intent();
                    List<CaseFinalPic> casePic = JSONUtil.TransformHosPic(serverData.data);
                    intent536.setClass(context, HosImagShowActivity.class);
                    intent536.putExtra("casefinaljson", (Serializable) casePic);
                    intent536.putExtra("pos", pos536);
                    context.startActivity(intent536);
                }
            }
        });
    }

    /**
     * 查看图片
     *
     * @param context
     * @param obj
     */
    public void reviewImages1(final Context context, HashMap<String, String> obj) {
        String id6015 = obj.get("id");
        String flag6015 = obj.get("flag");
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", id6015);
        maps.put("flag", flag6015);
        CheckPhotoApi checkPhotoApi = new CheckPhotoApi();
        checkPhotoApi.getCallBack(context, maps, new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String s) {
                Intent intent6015 = new Intent();
                List<CaseFinalPic> casePic = JSONUtil.TransformHosPic(s);
                intent6015.setClass(context, HosImagShowActivity.class);
                intent6015.putExtra("casefinaljson", (Serializable) casePic);
                context.startActivity(intent6015);
            }
        });
    }

    /**
     * 重新加入购物车
     *
     * @param mContext
     * @param obj
     */
    public void toRejoinCar(final Context mContext, HashMap<String, String> obj) {
        String orderId = obj.get("order_id");
        HashMap<String, Object> data = new HashMap<>();
        data.put("order_id", orderId);
        new AgainAddCarApi().getCallBack(mContext, data, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    mContext.startActivity(new Intent(mContext, ShoppingCartActivity.class));
                } else {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 获取发帖人信息页
     */
    public void getUserData(final Context context) {
        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("uid", Utils.getUid());
        keyValues.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(context, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    String full_name = userData.getFull_name();
                    String mobile = userData.getMobile();
                    if (!TextUtils.isEmpty(full_name) && !TextUtils.isEmpty(mobile)) {

                        if (!TextUtils.isEmpty(Utils.getUid())) {
                            WriteNoteManager.getInstance(context).ifAlert(null);
                        } else {
                            if (context instanceof Activity) {
                                ActivitySkipUtil.skipAnotherActivity((Activity) context, LoginActivity605.class, new HashMap<String, Object>());
                            }
                        }
                    } else {
                        Intent intent = new Intent(context, PostingMessageActivity.class);
                        intent.putExtra("tiaozhuan", "7");
                        context.startActivity(intent);
                    }
                } else {
                    Toast.makeText(context, serverData.message, Toast.LENGTH_SHORT).show();
                }

            }

        });
    }

    /**
     * 添加闹钟
     *
     * @param mContext
     * @param calendarss
     */
    public void setAlarmClock(Context mContext, String calendarss) {
        CalendarReminderData calendarReminderData = JSONUtil.TransformSingleBean(calendarss, CalendarReminderData.class);
        long startTime = Utils.phpToJavaTimeStamp(calendarReminderData.getStartTime());
        long endTime = Utils.phpToJavaTimeStamp(calendarReminderData.getEndTime());
        int advanceTime = Integer.parseInt(calendarReminderData.getAdvanceTime()) / 60;

        calendarReminderData.setStartTime(startTime + "");
        calendarReminderData.setEndTime(endTime + "");
        calendarReminderData.setAdvanceTime(advanceTime + "");

        boolean isAddCalendar = CalendarReminderUtils.addCalendarEvent(mContext, calendarReminderData);
        if (isAddCalendar) {
            MyToast.makeTextToast2(mContext, "添加提醒完成", MyToast.SHOW_TIME).show();
        } else {
            MyToast.makeTextToast2(mContext, "添加提醒失败", MyToast.SHOW_TIME).show();
        }
    }
}
