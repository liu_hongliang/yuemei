package com.module.community.web;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.BaikeFourActivity671;
import com.module.commonview.activity.BaikeTwoActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.HosServiceActivity;
import com.module.commonview.activity.InstructionWebActivity;
import com.module.commonview.activity.MapHospitalWebActivity;
import com.module.commonview.activity.ProjectDetailActivity638;
import com.module.commonview.activity.QuestionsActivity;
import com.module.commonview.activity.SpeltActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.activity.VideoChatActivity;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.module.api.AutoSendApi;
import com.module.commonview.module.api.AutoSendApi2;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.TaoPushApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.ChatParmsBean;
import com.module.commonview.module.bean.ContinueToSend;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.module.bean.Type5983Bean;
import com.module.commonview.view.SkuVideoChatDialog;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.community.model.api.CreateFaceVideoApi;
import com.module.community.model.api.MatchVideoDoctorsApi;
import com.module.community.model.api.TaoMatchVideoDoctorsApi;
import com.module.community.model.bean.CreateFaceVideoBean;
import com.module.community.model.bean.FaceVdieoBean;
import com.module.community.model.bean.MatchVideoDoctorBean;
import com.module.community.model.bean.TaoMatchVideoDoctorsBean;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.ExtrasBean;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.view.CalendarReminderDialog;
import com.module.doctor.controller.activity.DocQueListWebActivity;
import com.module.doctor.controller.activity.HosCommentActivity;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.controller.activity.TabDocAndHosListActivity;
import com.module.doctor.model.bean.HosShareData;
import com.module.event.MsgEvent;
import com.module.home.controller.activity.AllProjectActivity;
import com.module.home.controller.activity.BaikeHomeActivity;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.controller.activity.ChannelTwoActivity;
import com.module.home.controller.activity.MainCitySelectActivity560;
import com.module.home.controller.activity.MyZiXunQuestionActivity;
import com.module.home.controller.activity.ProjectMoreZTActivity;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.activity.SearchBaikeActivity;
import com.module.home.controller.activity.TaoLimitTimeActivity;
import com.module.home.controller.activity.TeMaiActivity;
import com.module.home.controller.activity.WebUrlTitleActivity;
import com.module.home.controller.activity.YueMingYiActivity;
import com.module.home.controller.activity.ZhengXingRijiActivity;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.home.model.bean.ProjectDetailsListData;
import com.module.my.controller.activity.BBsBaikeH5Activity;
import com.module.my.controller.activity.EssaySkuActivity;
import com.module.my.controller.activity.FeedbackActivity;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.activity.ModifyMyDataActivity;
import com.module.my.controller.activity.MyCollectActivity550;
import com.module.my.controller.activity.MyDaijinjuanActivity;
import com.module.my.controller.activity.MyOrdersActivity;
import com.module.my.controller.activity.NewAddressActivity;
import com.module.my.controller.activity.OnlineKefuWebActivity;
import com.module.my.controller.activity.OpeningMemberActivity;
import com.module.my.controller.activity.PeifuBasicWebActivity;
import com.module.my.controller.activity.PlusVipShareActivity;
import com.module.my.controller.activity.QuestionDetailsActivity;
import com.module.my.controller.activity.SelectSendPostsActivity;
import com.module.my.controller.activity.SignWebActivity;
import com.module.my.controller.activity.TaoCompensateWebActivity;
import com.module.my.controller.activity.TaolistActivity;
import com.module.my.controller.activity.TypeProblemActivity;
import com.module.my.controller.activity.WalletMingxiActivity;
import com.module.my.controller.activity.WriteSuibianLiaoActivity647;
import com.module.my.controller.activity.YuemeiWalletActitivy;
import com.module.my.controller.other.ParamsMap;
import com.module.my.model.bean.NoteBookListData;
import com.module.my.model.bean.TaoDataInfo7043;
import com.module.my.model.bean.VipSharejsonData;
import com.module.my.view.orderpay.OrderDetailActivity;
import com.module.other.activity.HomeDiarySXActivity;
import com.module.other.activity.LookRijiActivity;
import com.module.other.activity.ProjectContrastActivity;
import com.module.other.activity.SearchProjectActivity;
import com.module.other.activity.ShowWebImageActivity;
import com.module.other.activity.TaoLimitNewActivity;
import com.module.other.fragment.ProjectContrastFragment;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.module.other.other.RequestAndResultCode;
import com.module.shopping.controller.activity.MakeSureOrderActivity;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.entity.TaoDetailBean;
import com.quicklyask.util.ActivitySkipUtil;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.util.WriteNoteManager;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.NoteTipsDialog;
import com.quicklyask.view.PushNewDialog;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.module.other.activity.ProjectContrastActivity.DELETE_CUR;
import static com.quicklyask.view.NewuserConponsPop.jumpUrl;


/**
 * type值识别跳转类
 * Created by 裴成浩 on 2018/6/14.
 */
public class WebViewUrlLoading {
    private String TAG = "WebViewUrlLoading";

    /**
     * 单例
     */
    private static volatile WebViewUrlLoading webViewUrlLoading;

    private WebViewUrlLoading() {
    }

    public static WebViewUrlLoading getInstance() {
        if (webViewUrlLoading == null) {
            synchronized (WebViewUrlLoading.class) {
                if (webViewUrlLoading == null) {
                    webViewUrlLoading = new WebViewUrlLoading();
                }
            }
        }
        return webViewUrlLoading;
    }

    /**
     * 处理webview里面的按钮
     *
     * @param url :跳转连接
     */
    public void showWebDetail(Context context, String url) {
        showWebDetail(context, null, url);
    }

    public void showWebDetail(WebView webView, String url) {
        showWebDetail(webView.getContext(), webView, url);
    }

    public void showWebDetail(Context mContext, WebView webView, String url) {
        showWebDetail(mContext, webView, url, "");
    }

    public void showWebDetail(final Context mContext, final WebView webView, String url, final Object ohterPrams) {
        WebUrlData webUrlData;
        HashMap<String, String> parameter;
        Log.e(TAG, "url == " + url);

        try {
            Log.e(TAG, "11111111");

            webUrlData = parserPagrms(url);
            Log.e(TAG, "222222");
            parameter = webUrlData.getParameter();
            Log.e(TAG, "3333");
            Log.e(TAG, "webUrlData.getType() === " + webUrlData.getType());
            if (TextUtils.isEmpty(webUrlData.getType())) {
                ExtrasBean extrasBean = JSONUtil.TransformSingleBean(url, ExtrasBean.class);
                webUrlData.setType(extrasBean.getType());
                String link = extrasBean.getLink();
                if (!TextUtils.isEmpty(link)) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    parameter.put("link", link);
                    webUrlData.setParameter(hashMap);
                }
            }
            switch (webUrlData.getType()) {
                case "0":
                    String link = parameter.get("link");
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(link);
                    break;

                case "10":      // 跳转讨论组首页
                    if (!"0".equals(Utils.getUid())) {
                        WriteNoteManager.getInstance(mContext).ifAlert(null);
                    } else {
                        if (mContext instanceof Activity) {
                            ActivitySkipUtil.skipAnotherActivity((Activity) mContext, LoginActivity605.class);
                        }
                    }
                    break;
                case "11":      // 跳转讨论组首页

                    finish(mContext);

                    break;
                case "12":// 点击跳转至问题编辑页

                    mContext.startActivity(new Intent(mContext, TypeProblemActivity.class));

                    break;
                case "13":// 跳转到淘整形首页

                    finish(mContext);

                    break;
                case "14":// 跳转到讨论组首页

                    finish(mContext);

                    break;
                case "15":// 经验值有什么用
                    Intent intent15 = new Intent(mContext, InstructionWebActivity.class);
                    intent15.putExtra("type", "4");
                    mContext.startActivity(intent15);

                    break;
                case "16":// 我的经验明细
                    Intent intent16 = new Intent(mContext, InstructionWebActivity.class);
                    intent16.putExtra("type", "3");
                    mContext.startActivity(intent16);

                    break;
                case "411"://查看图片
                    Intent intent411 = new Intent(mContext, ShowWebImageActivity.class);
                    String link411 = parameter.get("link");
                    intent411.putExtra(ShowWebImageActivity.IMAGE_URLS, link411);
                    intent411.putExtra(ShowWebImageActivity.POSITION, 0);
                    mContext.startActivity(intent411);

                    break;

                case "418"://陶整形详情页
                    String id418 = parameter.get("id");
                    Intent intent418 = new Intent(mContext, TaoDetailActivity.class);
                    intent418.putExtra("id", id418);
                    intent418.putExtra("source", "10");
                    intent418.putExtra("objid", "0");
                    mContext.startActivity(intent418);
                    break;

                case "431"://个人信息
                    Intent intent431 = new Intent();
                    String id431 = parameter.get("id");
                    intent431.putExtra("id", id431);
                    intent431.setClass(mContext, PersonCenterActivity641.class);
                    mContext.startActivity(intent431);
                    break;
                case "432":
                    WebViewUrlUtil.getInstance().getUserData(mContext);
                    break;
                case "441":// 订单详情

                    Intent intent441 = new Intent(mContext, OrderDetailActivity.class);
                    intent441.putExtra("order_id", parameter.get("order_id"));
                    intent441.putExtra("flag", parameter.get("flag"));
                    mContext.startActivity(intent441);

                    break;
                case "444"://去支付
                    String is_alert444 = parameter.get("is_alert");
                    if ("1".equals(is_alert444)) {
                        WebViewUrlUtil.getInstance().showDialogSpell444(mContext, parameter);
                    } else {
                        WebViewUrlUtil.getInstance().orderPayment(mContext, parameter);
                    }
                    break;
                case "445"://申请退款
                    String order_id445 = parameter.get("order_id");
                    String paytype445 = parameter.get("paytype");

                    String repayment_type445 = parameter.get("repayment_type");

                    WebViewUrlUtil.getInstance().showDialogExitEdit445(mContext, order_id445, paytype445, repayment_type445);
                    break;
                case "446": //查看详情

                    Intent intent446 = new Intent();
                    intent446.setClass(mContext, TaoCompensateWebActivity.class);
                    mContext.startActivity(intent446);
                    break;
                case "447": //去逛逛
                    String tabbarindex = parameter.get("tabbarindex");
                    MainTableActivity.mainBottomBar.setCheckedPos(Integer.parseInt(tabbarindex));

                    Intent intent447 = new Intent(mContext, MainTableActivity.class);
                    mContext.startActivity(intent447);
                    finish(mContext);
                    break;
                case "448":// 在线留言
                    Intent intent448 = new Intent(mContext, PeifuBasicWebActivity.class);
                    mContext.startActivity(intent448);
                    break;
                case "511":// 医院详情
                    Intent intent511 = new Intent();
                    String hosid511 = parameter.get("hosid");
                    intent511.putExtra("hosid", hosid511);
                    intent511.setClass(mContext, HosDetailActivity.class);
                    mContext.startActivity(intent511);
                    break;
                case "516"://帖子最终页：点击查看主贴
                    Intent intent516 = new Intent();
                    String id516 = parameter.get("id");
                    String link516 = parameter.get("link");
                    link516 = FinalConstant.baseUrl + FinalConstant.VER + link516;
                    intent516.setClass(mContext, DiariesAndPostsActivity.class);
                    intent516.putExtra("qid", id516);
                    intent516.putExtra("url", link516);
                    mContext.startActivity(intent516);
                    break;
                case "522": //问题详情页
                    if (Utils.isLoginAndBind(mContext)) {
                        if (ohterPrams != null) {
                            String isAppRunning = (String) ohterPrams;
                            if ("1".equals(isAppRunning)) {
                                Log.e(TAG, "AppRunning ====");
                                MainTableActivity.mainBottomBar.setCheckedPos(3);
                                Intent intent522 = new Intent(mContext, MainTableActivity.class);
                                intent522.putExtra("acquiescencePage", 4);
                                mContext.startActivity(intent522);
                            } else {
                                Log.e(TAG, "NotAppRunning ====");
                                Intent it6 = new Intent(mContext, MainTableActivity.class);
                                it6.putExtra("type", 3); //对应下边第几个签
                                it6.putExtra("acquiescencePage", 4);//消息列表上面的第几个签从0开始
                                mContext.startActivity(it6);
                            }
                        } else {
                            Log.e(TAG, "NotAppRunning ohterPrams====null");
                            Intent it6 = new Intent(mContext, MainTableActivity.class);
                            it6.putExtra("type", 3); //对应下边第几个签
                            it6.putExtra("acquiescencePage", 4);//消息列表上面的第几个签从0开始
                            mContext.startActivity(it6);
                        }
                    }
//                    Intent intent522 = new Intent();
//                    String link522 = parameter.get("link");
//                    intent522.putExtra("link", link522);
//                    intent522.setClass(mContext, MyZiXunQuestionActivity.class);
//                    mContext.startActivity(intent522);
                    break;
                case "531":
                    String link531 = parameter.get("link");
                    Log.e(TAG, "link531 == " + link531);
                    String url531 = FinalConstant.baseUrl + FinalConstant.VER + "/" + link531;
                    String[] split = link531.split("/");
                    HashMap<String, Object> hashMap = new HashMap<>();
                    for (int i = 3; i < split.length; i += 2) {
                        if (i + 1 <= split.length) {
                            hashMap.put(split[i], split[i + 1]);
                        }
                    }
                    LodUrl(webView, url531, hashMap);
                    break;
                case "532": // 医院服务 全部淘整形/医院相册
                    Intent intent532 = new Intent();
                    String link532 = parameter.get("link");
                    intent532.putExtra("url", link532);
                    intent532.setClass(mContext, HosServiceActivity.class);
                    mContext.startActivity(intent532);
                    break;
                case "534"://医院评论
                    String link534 = parameter.get("link");
                    Utils.tongjiApp(mContext, "hospital_comment", "hospital", (String) ohterPrams, "13");
                    Intent it534 = new Intent();
                    it534.putExtra("url", link534);
                    it534.setClass(mContext, HosCommentActivity.class);
                    mContext.startActivity(it534);
                    break;
                case "535":
                    final String mPhone535 = parameter.get("tel");
                    MyToast.makeTextToast2(mContext, "正在拨打中·····", MyToast.SHOW_TIME);
                    WebViewUrlUtil.getInstance().phonePermissions(mContext, mPhone535);
                    break;
                case "536": // 案例详情/查看图片
                    CookieConfig.getInstance().setCookie("https", "sjapp.yuemei.com", "sjapp.yuemei.com");
                    WebViewUrlUtil.getInstance().reviewImages(mContext, parameter);
                    break;

                case "541":         // 医院地址
                    Intent intent541 = new Intent();
                    String hosid541 = parameter.get("hosid");
                    intent541.setClass(mContext, MapHospitalWebActivity.class);
                    intent541.putExtra("hosid", hosid541);
                    mContext.startActivity(intent541);
                    break;
                case "543": // 吐槽
                    Intent intent543 = new Intent();
                    intent543.setClass(mContext, FeedbackActivity.class);
                    mContext.startActivity(intent543);
                    break;
                case "544"://分享
                    WebViewUrlUtil.getInstance().setShare544(mContext);
                    break;
                case "999":// 悦美
                    Intent intent999 = new Intent();
                    String link999 = parameter.get("link");
                    intent999.putExtra("url", link999);
                    intent999.putExtra("shareTitle", "0");
                    intent999.putExtra("sharePic", "0");
                    intent999.setClass(mContext, SlidePicTitieWebActivity.class);
                    mContext.startActivity(intent999);
                    break;

                case "1000"://专题
                    Intent intent1000 = new Intent();
                    String url1000 = parameter.get("link");
                    if (parameter.containsKey("title")) {
                        String title1000 = URLDecoder.decode(parameter.get("title"), "utf-8");
                        intent1000.putExtra("title", title1000);
                    }
                    if (parameter.containsKey("id")) {
                        String ztid1000 = parameter.get("id");
                        intent1000.putExtra("ztid", ztid1000);
                    }
                    intent1000.putExtra("url", url1000);
                    intent1000.setClass(mContext, ZhuanTiWebActivity.class);
                    mContext.startActivity(intent1000);
                    break;

                case "5411":   // 预约流程
                    Intent intent5411 = new Intent();
                    intent5411.putExtra("type", "5");
                    intent5411.setClass(mContext, InstructionWebActivity.class);
                    mContext.startActivity(intent5411);
                    break;

                case "5413":    // 费用说明

                    Intent intent5413 = new Intent();
                    String link5413 = parameter.get("link");
                    intent5413.putExtra("type", link5413);
                    intent5413.setClass(mContext, InstructionWebActivity.class);
                    mContext.startActivity(intent5413);

                    break;

                case "5415": //改退规则
                    Intent intent5415 = new Intent();
                    intent5415.putExtra("type", "7");
                    intent5415.setClass(mContext, InstructionWebActivity.class);
                    mContext.startActivity(intent5415);
                    break;
                case "5421":    // 写日记
                    String hosid5422 = parameter.get("hosid");
                    String hosname5422 = URLDecoder.decode(parameter.get("hosname"), "utf-8");

                    NoteBookListData noteDataB5422 = new NoteBookListData();
                    noteDataB5422.setTitle("");
                    noteDataB5422.setImg("");
                    noteDataB5422.setSharetime("");
                    noteDataB5422.setIs_new("");
                    noteDataB5422.setT_id("");
                    noteDataB5422.set_id("");
                    noteDataB5422.setServer_id("");
                    noteDataB5422.setPrice("");
                    noteDataB5422.setDoc_id("");
                    noteDataB5422.setDocname("");
                    noteDataB5422.setHos_id(hosid5422);
                    noteDataB5422.setHosname(hosname5422);
                    noteDataB5422.setSubtitle("");
                    noteDataB5422.setPage("");
                    if (Utils.isLoginAndBind(mContext)) {
                        WriteNoteManager.getInstance(mContext).ifAlert(noteDataB5422);
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                    break;
                case "5426"://问答详情
                    String link5426 = parameter.get("link");
                    String qid5426 = parameter.get("id");
                    link5426 = FinalConstant.baseUrl + FinalConstant.VER + link5426;

                    Intent intent5426 = new Intent();
                    intent5426.setClass(mContext, DiariesAndPostsActivity.class);
                    intent5426.putExtra("url", link5426);
                    intent5426.putExtra("qid", qid5426);
                    mContext.startActivity(intent5426);
                    break;
                case "5445":    //银行卡限额
                    Intent intent5445 = new Intent();
                    intent5445.putExtra("type", "8");
                    intent5445.setClass(mContext, InstructionWebActivity.class);
                    mContext.startActivity(intent5445);
                    break;

                case "5451":// 领取
                    String flag5451 = parameter.get("flag");
                    if (Utils.isLogin()) {
                        WebViewUrlUtil.getInstance().sumitHttpCode5451(mContext, flag5451);
                    } else {
                        if (mContext instanceof Activity) {
                            ActivitySkipUtil.skipAnotherActivity((Activity) mContext, LoginActivity605.class, new HashMap<String, Object>());
                        }
                    }

                    break;
                case "5461":    // 面部整形术前照片标准

                    Intent intent5461 = new Intent();
                    String link5461 = parameter.get("link");
                    intent5461.setClass(mContext, WebUrlTitleActivity.class);
                    intent5461.putExtra("link", link5461);
                    intent5461.putExtra("title", "面部整形术前照片标准");
                    mContext.startActivity(intent5461);

                    break;
                case "5462":    // 身体整形术前照片标准

                    Intent intent5462 = new Intent();
                    String link5462 = parameter.get("link");
                    intent5462.setClass(mContext, WebUrlTitleActivity.class);
                    intent5462.putExtra("link", link5462);
                    intent5462.putExtra("title", "身体整形术前照片标准");
                    mContext.startActivity(intent5462);
                    break;
                case "5463":    // 皮肤整形术前照片标准

                    Intent intent5463 = new Intent();
                    String link5463 = parameter.get("link");
                    intent5463.setClass(mContext, WebUrlTitleActivity.class);
                    intent5463.putExtra("link", link5463);
                    intent5463.putExtra("title", "皮肤整形术前照片标准");
                    mContext.startActivity(intent5463);
                    break;
                case "5464":    // 毛发整形术前照片标准

                    Intent intent5464 = new Intent();
                    String link5464 = parameter.get("link");
                    intent5464.setClass(mContext, WebUrlTitleActivity.class);
                    intent5464.putExtra("link", link5464);
                    intent5464.putExtra("title", "毛发整形术前照片标准");
                    mContext.startActivity(intent5464);
                    break;
                case "5465":    // 牙齿整形术前照片标准

                    Intent intent5465 = new Intent();
                    String link5465 = parameter.get("link");
                    intent5465.setClass(mContext, WebUrlTitleActivity.class);
                    intent5465.putExtra("link", link5465);
                    intent5465.putExtra("title", "牙齿整形术前照片标准");
                    mContext.startActivity(intent5465);
                    break;

                case "5466":    //写日记关联
                    String taoid5466 = parameter.get("tao_id");
                    String hosname5466 = URLDecoder.decode(parameter.get("hosname"), "utf-8");
                    String hosid5466 = parameter.get("hosid");
                    String docname5466 = URLDecoder.decode(parameter.get("docname"), "utf-8");
                    String docid5466 = parameter.get("docid");
                    String price5466 = parameter.get("price");
                    final String server_id5466 = parameter.get("server_id");
                    String title5466 = URLDecoder.decode(parameter.get("title"), "utf-8");
                    NoteBookListData noteDataB5466 = new NoteBookListData();
                    noteDataB5466.setTitle(title5466);
                    noteDataB5466.setImg("");
                    noteDataB5466.setSharetime("");
                    noteDataB5466.setIs_new("");
                    noteDataB5466.setT_id("");
                    noteDataB5466.set_id(taoid5466);
                    noteDataB5466.setServer_id(server_id5466);
                    noteDataB5466.setPrice(price5466);
                    noteDataB5466.setDoc_id(docid5466);
                    noteDataB5466.setDocname(docname5466);
                    noteDataB5466.setHos_id(hosid5466);
                    noteDataB5466.setHosname(hosname5466);
                    noteDataB5466.setSubtitle("");
                    noteDataB5466.setPage("");
                    if (Utils.isLoginAndBind(mContext)) {
                        NoteTipsDialog notePop = new NoteTipsDialog(mContext, noteDataB5466, "2", null);
                        notePop.show();
                    }
                    break;

                case "5443":    //查看图片
                    Intent intent5443 = new Intent();
                    String link5433 = parameter.get("img");
                    intent5443.putExtra(ShowWebImageActivity.IMAGE_URLS, link5433);
                    intent5443.putExtra(ShowWebImageActivity.POSITION, 0);
                    intent5443.setClass(mContext, ShowWebImageActivity.class);
                    mContext.startActivity(intent5443);

                    break;
                case "5801"://医生服务
                    Intent intent5801 = new Intent();
                    String link5801 = parameter.get("link");
                    String url5801 = FinalConstant.baseUrl + FinalConstant.VER + link5801;
                    intent5801.setClass(mContext, DocQueListWebActivity.class);
                    intent5801.putExtra("type", "2");
                    intent5801.putExtra("link", url5801);
                    mContext.startActivity(intent5801);
                    break;

                case "5902"://跳百科二级

                    Intent intent5902 = new Intent();
                    String link5902 = parameter.get("link");
                    String url5902 = FinalConstant.baseUrl + FinalConstant.VER + link5902;
                    String name5902 = URLDecoder.decode(parameter.get("name"), "utf-8");
                    intent5902.putExtra("url", url5902);
                    intent5902.putExtra("name", name5902);
                    intent5902.setClass(mContext, BaikeTwoActivity.class);
                    mContext.startActivity(intent5902);

                    break;

                case "5903"://跳百科四级
                    Intent intent5903 = new Intent(mContext, BaikeFourActivity671.class);
                    String link5903 = parameter.get("link");
                    String url5903 = FinalConstant.baseUrl + FinalConstant.VER + link5903;
                    String name5903 = URLDecoder.decode(parameter.get("name"), "utf-8");
                    String parentId5903 = parameter.get("parentId");
                    String id5903 = parameter.get("id");
                    String page_index5903 = parameter.get("page_index");
                    String shareurl5903 = parameter.get("shareurl");
                    String sharetitle5903 = URLDecoder.decode(parameter.get("sharetitle"), "utf-8");
                    String sharecontent = URLDecoder.decode(parameter.get("sharecontent"), "utf-8");
                    String sharepic = URLDecoder.decode(parameter.get("sharepic"), "utf-8");
                    intent5903.putExtra("name", name5903);
                    intent5903.putExtra("url", url5903);
                    intent5903.putExtra("id", id5903);
                    intent5903.putExtra("parentId", parentId5903);
                    intent5903.putExtra("shareurl", shareurl5903);
                    intent5903.putExtra("sharetitle", sharetitle5903);
                    intent5903.putExtra("sharecontent", sharecontent);
                    intent5903.putExtra("sharepic", sharepic);
                    intent5903.putExtra("page_index", page_index5903);
                    mContext.startActivity(intent5903);

                    break;
                case "5921":    //代金劵使用说明
                    Intent intent5921 = new Intent();
                    String link5921 = parameter.get("link");
                    String title5921 = "代金劵使用说明";
                    link5921 = FinalConstant.baseUrl + FinalConstant.VER + link5921;
                    intent5921.setClass(mContext, BBsBaikeH5Activity.class);
                    intent5921.putExtra("url", link5921);
                    intent5921.putExtra("name", title5921);
                    mContext.startActivity(intent5921);
                    break;

                case "5961"://百科搜索
                    Intent intent5961 = new Intent();
                    intent5961.setClass(mContext, SearchBaikeActivity.class);
                    mContext.startActivity(intent5961);
                    break;
                case "5983":
                    String id5983 = parameter.get("id");
                    if (!TextUtils.isEmpty(id5983)) {
                        if (Utils.noLoginChat()) {
                            chat5983(mContext, ohterPrams, parameter, id5983);
                        } else {
                            if (Utils.isLoginAndBind(mContext)) {
                                chat5983(mContext, ohterPrams, parameter, id5983);
                            }
                        }
                    } else {
                        if (!TextUtils.isEmpty(id5983)) {
                            Type5983Bean type5983Bean = (Type5983Bean) ohterPrams;
                            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                                    .setDirectId(id5983)
                                    .setObjId(type5983Bean.getObjId())
                                    .setObjType(type5983Bean.getObjType5983())
                                    .setTitle(type5983Bean.getTitle5983())
                                    .setImg(type5983Bean.getImg5983())
                                    .setUrl(type5983Bean.getUrl())
                                    .setYmClass(type5983Bean.getmYmClass())
                                    .setYmId(type5983Bean.getmYmId())
                                    .build();
                            new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
                        }
                    }
                    break;
                case "5992":    //更多跳转
                    String one_id5992 = "";
                    String two_id5992 = "";
                    String three_id5992 = "";

                    //type:eq:5992:and:isAll:eq:0:and:one_id:eq:392:and:two_id:eq:8863:and:three_id:eq:8863:and:homeSource:eq:1
                    String title5992 = parameter.get("title");
                    String isAll5992 = parameter.get("isAll");
                    String level5992 = parameter.get("level");
                    String homeSource5992 = parameter.get("homeSource");
                    if (parameter.containsKey("one_id")) {
                        one_id5992 = parameter.get("one_id");
                    }
                    if (parameter.containsKey("two_id")) {
                        two_id5992 = parameter.get("two_id");
                    }
                    if (parameter.containsKey("three_id")) {
                        three_id5992 = parameter.get("three_id");
                    }


                    if ("1".equals(isAll5992)) {
                        Intent intent = new Intent(mContext, AllProjectActivity.class);
                        intent.putExtra("home_source", homeSource5992);
                        mContext.startActivity(intent);
                    } else {
                        switch (level5992) {
                            case "2":
                                //部位
                                Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                                intent2.putExtra("id", one_id5992);
                                intent2.putExtra("title", title5992);
                                intent2.putExtra("home_source", homeSource5992);
                                mContext.startActivity(intent2);
                                break;
                            case "3":
                                //二级
                                Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(ChannelTwoActivity.TITLE, title5992);
                                bundle.putString(ChannelTwoActivity.TWO_ID, two_id5992);
                                bundle.putString(ChannelTwoActivity.HOME_SOURCE, homeSource5992);
                                intent3.putExtra("data", bundle);
                                mContext.startActivity(intent3);
                                break;
                            case "4":
                                //四级
                                Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                                intent4.putExtra("id", three_id5992);
                                intent4.putExtra("title", title5992);
                                intent4.putExtra("home_source", homeSource5992);
                                mContext.startActivity(intent4);
                                break;
                        }
                    }


                    break;
                case "6001":
                    Intent it6001 = new Intent();
                    it6001.setClass(mContext, YueMingYiActivity.class);
                    mContext.startActivity(it6001);
                    break;
                case "6002":
                    Intent it6002 = new Intent();
                    it6002.setClass(mContext, TeMaiActivity.class);
                    mContext.startActivity(it6002);
                    break;
                case "6003":
                    Intent it6003 = new Intent();
                    it6003.setClass(mContext, TaoLimitNewActivity.class);
                    it6003.putExtra("type", "2");
                    mContext.startActivity(it6003);
                    break;
                case "6004"://整形百科
                    Intent it6004 = new Intent();
                    it6004.setClass(mContext, BaikeHomeActivity.class);
                    mContext.startActivity(it6004);
                    break;
                case "6006": //医院跳转
                    String labelID6006 = "";
                    String labelName6006 = "";
                    Intent intent6006 = new Intent(mContext, TabDocAndHosListActivity.class);
                    if (parameter.containsKey("labelID")) {
                        labelID6006 = parameter.get("labelID");
                    }
                    if (parameter.containsKey("labelName")) {
                        labelName6006 = parameter.get("labelName");
                    }
                    if (!TextUtils.isEmpty(labelID6006) && !TextUtils.isEmpty(labelName6006)) {
                        intent6006.putExtra("screen", new ProjectDetailsListData(labelID6006, URLDecoder.decode(labelName6006, "utf-8")));
                    }
                    intent6006.putExtra("type", "1");
                    mContext.startActivity(intent6006);

                    break;
                case "6008":
                    Intent it6008 = new Intent();
                    it6008.setClass(mContext, TaoLimitTimeActivity.class);
                    it6008.putExtra("type", "2");
                    mContext.startActivity(it6008);
                    break;
                case "6009":
                    Intent it6009 = new Intent();
                    it6009.setClass(mContext, LookRijiActivity.class);
                    mContext.startActivity(it6009);
                    break;
                case "6014":    //写日记
                    String id6014 = parameter.get("id");
                    String sharetime6014 = parameter.get("sharetime");
                    String before_max_day6014 = parameter.get("before_max_day");

                    Intent intent6014 = new Intent(mContext, ProjectDetailActivity638.class);

                    intent6014.putExtra("cateid", "1090");
                    intent6014.putExtra("userid", "");
                    intent6014.putExtra("hosid", "");
                    intent6014.putExtra("hosname", "");
                    intent6014.putExtra("fee", "");
                    intent6014.putExtra("taoid", "");
                    intent6014.putExtra("server_id", "");
                    intent6014.putExtra("sharetime", sharetime6014);

                    intent6014.putExtra("type", "3");
                    intent6014.putExtra("noteid", id6014);
                    intent6014.putExtra("notetitle", "");
                    intent6014.putExtra("addtype", "2");
                    intent6014.putExtra("beforemaxday", before_max_day6014);
                    intent6014.putExtra("consumer_certificate", "0");

                    if (mContext instanceof Activity) {
                        ((Activity) mContext).startActivityForResult(intent6014, 111);
                    } else {
                        mContext.startActivity(intent6014);
                    }

                    break;
                case "6015":    // 查看图片

                    WebViewUrlUtil.getInstance().reviewImages1(mContext, parameter);

                    break;
                case "6020"://整形日记
                    Intent it = new Intent();
                    it.setClass(mContext, ZhengXingRijiActivity.class);
                    mContext.startActivity(it);
                    break;
                case "6022":  // 领优惠券

                    if (Utils.isLoginAndBind(mContext)) {
                        String is_fresher = "";
                        String alert = "";
                        if (!parameter.containsKey("is_fresher")) {
                            is_fresher = parameter.get("is_fresher");
                        }
                        if (!parameter.containsKey("alert")) {
                            alert = parameter.get("alert");
                        }
                        String _id = parameter.get("id");
                        if (!TextUtils.isEmpty(is_fresher) && !TextUtils.isEmpty(alert)) {
                            WebViewUrlUtil.getInstance().daijinjuanLingqu6022(mContext, webView, _id, is_fresher, alert);
                        } else {
                            WebViewUrlUtil.getInstance().daijinjuanLingqu6022(mContext, webView, _id);
                        }
                    }

                    break;

                case "6061":// 观看直播

                    String fake_token6061 = Cfg.loadStr(mContext, "hj_token", "");

                    if (Utils.isLogin()) {
                        if (!TextUtils.isEmpty(fake_token6061)) {

                            String relatedId6061 = parameter.get("relateid");
                            String type6061 = parameter.get("flag");
                            Bundle bundle = new Bundle();
                            bundle.putInt("type", Integer.parseInt(type6061));
                            bundle.putString("sn", "");
                            bundle.putString("liveId", relatedId6061);
                            bundle.putString("replayId", relatedId6061);
                            bundle.putString("background", "");
                            bundle.putString("channel", "");
                            bundle.putString("usign", "");
                        }

                    } else {
                        if (mContext instanceof Activity) {
                            ActivitySkipUtil.skipAnotherActivity((Activity) mContext, LoginActivity605.class, new HashMap<String, Object>());
                        }
                    }

                    break;

                case "6074":

                    WebUtil.getInstance().startWebActivity(mContext, FinalConstant1.BASE_VER_URL + "/live/list/", "直播列表");

                    break;
                case "6091"://悦美快速专题
                    MainTableActivity.mainBottomBar.setCheckedPos(1);

                    Intent intent6091 = new Intent();
                    intent6091.setClass(mContext, MainTableActivity.class);
                    mContext.startActivity(intent6091);
                    finish(mContext);

                    break;
                case "6092":
                    Intent itt = new Intent();
                    itt.setClass(mContext, ProjectMoreZTActivity.class);
                    mContext.startActivity(itt);
                    break;
                case "6101":
                    String link6101 = parameter.get("link");
                    String title6101 = URLDecoder.decode(parameter.get("title"), "utf-8");

                    Intent intent6101 = new Intent();
                    link6101 = link6101 + "uid/" + Utils.getUid() + "/";
                    intent6101.setClass(mContext, WebUrlTitleActivity.class);
                    intent6101.putExtra("link", link6101);
                    intent6101.putExtra("title", title6101);
                    mContext.startActivity(intent6101);
                    break;
                case "6103"://选择发帖类型

                    Intent intent6103 = new Intent();
                    intent6103.putExtra("cateid", "0");
                    intent6103.setClass(mContext, SelectSendPostsActivity.class);
                    mContext.startActivity(intent6103);

                    break;
                case "6120":    // 修改返现信息
                    WebViewUrlUtil.getInstance().getUserInfo6120(mContext);
                    break;
                case "6135"://完善资料
                    Intent intent6135 = new Intent();
                    String flag6135 = parameter.get("flag");
                    intent6135.putExtra("type", "1");
                    intent6135.putExtra("flag", flag6135);
                    intent6135.setClass(mContext, ModifyMyDataActivity.class);
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).startActivityForResult(intent6135, 888);
                    } else {
                        mContext.startActivity(intent6135);
                    }

                    break;
                case "6152"://社区问题客服之类的
                    String title6152 = URLDecoder.decode(parameter.get("title"), "utf-8");
                    String link6152 = parameter.get("link");

                    Intent intent6152 = new Intent();
                    intent6152.setClass(mContext, OnlineKefuWebActivity.class);
                    intent6152.putExtra("link", link6152);
                    intent6152.putExtra("title", title6152);
                    mContext.startActivity(intent6152);
                    break;
                case "6180":// 我的订单列表
                    Intent intent6180 = new Intent();
                    intent6180.setClass(mContext, MyOrdersActivity.class);
                    intent6180.putExtra("type", "1");
                    mContext.startActivity(intent6180);

                    break;

                case "6230": //精选日记列表页跳转
                    Intent intent6230 = new Intent(mContext, HomeDiarySXActivity.class);
                    String labelID6230 = "";
                    String labelName6230 = "";
                    if (parameter.containsKey("labelID")) {
                        labelID6230 = parameter.get("labelID");
                    }
                    if (parameter.containsKey("labelName")) {
                        labelName6230 = parameter.get("labelName");
                    }
                    if (!TextUtils.isEmpty(labelID6230) && !TextUtils.isEmpty(labelName6230)) {
                        intent6230.putExtra("screen", new ProjectDetailsListData(labelID6230, URLDecoder.decode(labelName6230, "utf-8")));
                    }
                    mContext.startActivity(intent6230);
                    break;
                case "6231": //医生跳转
                    Intent intent6231 = new Intent(mContext, TabDocAndHosListActivity.class);
                    String labelID6231 = "";
                    String labelName6231 = "";
                    if (parameter.containsKey("labelID")) {
                        labelID6231 = parameter.get("labelID");
                    }
                    if (parameter.containsKey("labelName")) {
                        labelName6231 = parameter.get("labelName");
                    }
                    if (!TextUtils.isEmpty(labelID6231) && !TextUtils.isEmpty(labelName6231)) {
                        intent6231.putExtra("screen", new ProjectDetailsListData(labelID6231, URLDecoder.decode(labelName6231, "utf-8")));
                    }
                    intent6231.putExtra("type", "0");
                    mContext.startActivity(intent6231);
                    break;
                case "6232":
                    Intent it6232 = new Intent();
                    it6232.setClass(mContext, WebUrlTitleActivity.class);
                    it6232.putExtra("link", FinalConstant.CHEAP_SUPER_WEB);
                    it6232.putExtra("title", "低价超市");
                    it6232.putExtra("isRsa", "1");
                    it6232.putExtra("slidingFinish", "0");
                    mContext.startActivity(it6232);
                    break;
                case "6310"://返回按钮
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).onBackPressed();
                    } else {
                        finish(mContext);
                    }
                    break;

                case "6311"://图片发送

                    Intent intent6311 = new Intent();
                    intent6311.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 9);
                    intent6311.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
                    intent6311.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                    intent6311.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, null);
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).startActivityForResult(intent6311, 1);
                    } else {
                        mContext.startActivity(intent6311);
                    }

                    break;
                case "6342"://跳转到问题详情

                    String question_id6342 = parameter.get("question_id");
                    String url6342 = FinalConstant.baseUrl + FinalConstant.VER + "/taoask/info/";

                    Intent intent6342 = new Intent(mContext, QuestionDetailsActivity.class);
                    intent6342.putExtra("url", url6342);
                    intent6342.putExtra("title", "0");
                    intent6342.putExtra("questionId", question_id6342);
                    mContext.startActivity(intent6342);

                    break;
                case "6344":// 跳转到问题详情
                    String img6344 = parameter.get("img");
                    String tao_id6344 = parameter.get("tao_id");
                    String title6344 = URLDecoder.decode(parameter.get("title"), "utf-8");
                    String price6344 = parameter.get("price");
                    String price_x6344 = parameter.get("price_x");

                    Intent intent6344 = new Intent(mContext, QuestionsActivity.class);
                    intent6344.putExtra("taoid", tao_id6344);
                    intent6344.putExtra("img", img6344);
                    intent6344.putExtra("title", title6344);
                    intent6344.putExtra("priceDiscount", price6344);
                    intent6344.putExtra("price", price_x6344);
                    mContext.startActivity(intent6344);

                    break;
                case "6351"://悦美
                    String couponsId6351 = parameter.get("coupons_id");
                    Intent intent6351 = new Intent(mContext, TaolistActivity.class);
                    intent6351.putExtra("coupons_id", couponsId6351);
                    mContext.startActivity(intent6351);
                    break;
                case "6352":    //跳转到分享微信好友【小程序】

                    HashMap<String, String> hashMap6352 = new HashMap<>();
                    hashMap6352.put("title", URLDecoder.decode(parameter.get("title"), "utf-8"));
                    hashMap6352.put("description", URLDecoder.decode(parameter.get("description"), "utf-8"));
                    hashMap6352.put("webpageUrl", parameter.get("webpageUrl"));
                    hashMap6352.put("path", parameter.get("path"));
                    hashMap6352.put("thumbImage", parameter.get("thumbImage"));
                    hashMap6352.put("userName", parameter.get("userName"));
                    WebViewUrlUtil.getInstance().setShare6352(mContext, hashMap6352);

                    break;
                case "6354":
                    Intent intent6354 = new Intent();
                    intent6354.setClass(mContext, WebUrlTitleActivity.class);
                    intent6354.putExtra("link", "/tao/grouprule/");
                    intent6354.putExtra("title", "拼团规则");
                    intent6354.putExtra("isRsa", "1");
                    mContext.startActivity(intent6354);
                    break;

                case "6355":
                    String groupId6355 = parameter.get("group_id");
                    Intent intent = new Intent(mContext, SpeltActivity.class);
                    intent.putExtra("group_id", groupId6355);
                    intent.putExtra("type", "1");
                    mContext.startActivity(intent);
                    break;
                case "6312":
                    Log.e(TAG, "6312 == ");
                    if (Utils.isLoginAndBind(mContext)) {
                        if (ohterPrams != null) {
                            String isAppRunning = (String) ohterPrams;
                            if ("1".equals(isAppRunning)) {
                                Log.e(TAG, "AppRunning ====");
                                MainTableActivity.mainBottomBar.setCheckedPos(3);
                                Intent intent522 = new Intent(mContext, MainTableActivity.class);
                                intent522.putExtra("acquiescencePage", 0);
                                mContext.startActivity(intent522);
                            } else {
                                Log.e(TAG, "NotAppRunning ====");
                                Intent it6 = new Intent(mContext, MainTableActivity.class);
                                it6.putExtra("type", 3); //对应下边第几个签
                                it6.putExtra("acquiescencePage", 0);//消息列表上面的第几个签从0开始
                                mContext.startActivity(it6);
                            }
                        } else {
                            Log.e(TAG, "NotAppRunning ohterPrams====null");
                            Intent it6 = new Intent(mContext, MainTableActivity.class);
                            it6.putExtra("type", 3); //对应下边第几个签
                            it6.putExtra("acquiescencePage", 0);//消息列表上面的第几个签从0开始
                            mContext.startActivity(it6);
                        }
                    }
//                    if (Utils.isLoginAndBind(mContext)) {
//
//                        Intent it6 = new Intent(mContext, MainTableActivity.class);
//                        it6.putExtra("type",3); //对应下边第几个签
////                        it6.putExtra("acquiescencePage",2);//消息列表上面的第几个签从0开始
//                        mContext.startActivity(it6);
//                    }
                    break;

                case "6361":
                    String shareimg6361 = "";
                    String shareTitle = "";
                    String shareContent = "";
                    String callback_url6361 = parameter.get("callback_url");
                    String link6361 = parameter.get("link");


                    if (parameter.containsKey("title")) {
                        String title = parameter.get("title");
                        shareTitle = URLDecoder.decode(title.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");

                    }
                    if (parameter.containsKey("content")) {
                        String content = parameter.get("content");
                        shareContent = URLDecoder.decode(content.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");

                    }

                    if (parameter.containsKey("shareimg")) {
                        String shareimg = parameter.get("shareimg");
                        shareimg6361 = URLDecoder.decode(shareimg.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");

                    }

                    HashMap<String, String> shareParame = new LinkedHashMap<>();
                    shareParame.put("tencentUrl", link6361);
                    if (!TextUtils.isEmpty(shareTitle)) {
                        shareParame.put("tencentTitle", shareTitle);
                    }
                    if (!TextUtils.isEmpty(shareContent)) {
                        shareParame.put("tencentText", shareContent);
                    }
                    if (!TextUtils.isEmpty(shareimg6361)) {
                        shareParame.put("shareImgUrl", shareimg6361);
                    }

                    WebViewUrlUtil.getInstance().setShare6361(mContext, webView, callback_url6361, shareParame);
                    break;
                case "6460":
                    mContext.startActivity(new Intent(mContext.getApplicationContext(), YuemeiWalletActitivy.class));
                    break;
                case "6482":
                    String link6482 = parameter.get("link");
                    String url6482 = FinalConstant.baseUrl + FinalConstant.VER + link6482;
                    String[] split6482 = link6482.split("/");
                    HashMap<String, Object> mSingStr6482 = new HashMap<>();
                    mSingStr6482.put("flag", split6482[4]);

                    WebSignData addressAndHead6482 = SignUtils.getAddressAndHead(url6482, mSingStr6482);
                    webView.loadUrl(addressAndHead6482.getUrl(), addressAndHead6482.getHttpHeaders());
                    break;

                case "6491":            // 	跳转公共的webView页面
                    String link6491 = parameter.get("link");
                    String url6491 = FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + link6491;
                    WebUtil.getInstance().startWebActivity(mContext, url6491);
                    break;
                case "6492":            // 新增/修改/查看 收货地址
                    String flag6492 = parameter.get("flag");
                    String address_id6492 = parameter.get("address_id");

                    Intent intent6492 = new Intent(mContext, NewAddressActivity.class);
                    intent6492.putExtra("flag", flag6492);
                    intent6492.putExtra("address_id", address_id6492);
                    if (mContext instanceof Activity) {
                        ((Activity) mContext).startActivityForResult(intent6492, 100);
                    } else {
                        mContext.startActivity(intent6492);
                    }

                    break;
                case "6493":
                    WebViewUrlUtil.getInstance().confirmGoods(mContext, parameter);
                    break;

                case "6494":            //	跳转到我的优惠券
                    Intent intent6494 = new Intent(mContext, MyDaijinjuanActivity.class);
                    intent6494.putExtra("link", "");
                    mContext.startActivity(intent6494);
                    break;
                case "6495":            //	跳转到钱包明细
                    String flag6495 = parameter.get("flag");
                    String source6495 = parameter.get("source");
                    Intent intent6495 = new Intent(mContext, WalletMingxiActivity.class);
                    intent6495.putExtra("jump_type", "5");//1,钱包明细   2，钱包说明  3，提现说明 4，6495跳转
                    intent6495.putExtra("title", "钱包明细");
                    intent6495.putExtra("flag", flag6495);
                    intent6495.putExtra("source", source6495);
                    mContext.startActivity(intent6495);

                    break;
                case "6496":            // 会员续费
                    Log.e("huiyuanxufei", "33333");
                    String flag6496 = parameter.get("flag");
                    Intent intent6496 = new Intent(mContext, OpeningMemberActivity.class);
                    intent6496.putExtra("flag", flag6496);
                    mContext.startActivity(intent6496);
                    break;
                case "6497":                                // 跳转到项目淘列表选中会员筛选
                    String kind6497 = parameter.get("kind");

                    ArrayList<String> kindStr = new ArrayList<>();
                    kindStr.add(kind6497);

                    Intent intent6497 = new Intent(mContext, ProjectDetailActivity638.class);
                    intent6497.putExtra("oneid", "0");
                    intent6497.putExtra("onetitle", " 全部项目");
                    intent6497.putExtra("twoid", "0");
                    intent6497.putExtra("twotitle", " 全部项目");
                    intent6497.putExtra("threeid", "0");
                    intent6497.putExtra("threetitle", " 全部项目");
                    intent6497.putExtra("medthod", "0");
                    intent6497.putStringArrayListExtra("kindStr", kindStr);
                    mContext.startActivity(intent6497);
                    break;
                case "6498":            // 领券中心
                    Intent intent2 = new Intent(mContext, WalletMingxiActivity.class);
                    intent2.putExtra("jump_type", "4");
                    intent2.putExtra("title", "领券中心"); //1,钱包明细   2，钱包说明  3，提现说明 4，领劵中心
                    mContext.startActivity(intent2);

                    break;

                case "6499":            //	跳转到颜值币商城
                    WebViewUrlUtil.getInstance().toJifen6499(mContext);
                    break;
                case "64991":
                    String link64991 = parameter.get("link");
                    String url64991 = FinalConstant.baseUrl + FinalConstant.VER + link64991;
                    String[] split64991 = link64991.split("/");
                    HashMap<String, Object> mSingStr64991 = new HashMap<>();
                    mSingStr64991.put("flag", split64991[4]);
                    WebSignData addressAndHead64991 = SignUtils.getAddressAndHead(url64991, mSingStr64991);
                    webView.loadUrl(addressAndHead64991.getUrl(), addressAndHead64991.getHttpHeaders());
                    break;
                case "64995":            //	拉新分享
                    String sharejson64995 = URLDecoder.decode(parameter.get("sharejson"), "utf-8");
                    VipSharejsonData vipSharejsonData = JSONUtil.TransformSingleBean(sharejson64995, VipSharejsonData.class);
                    Intent intent64995 = new Intent(mContext, PlusVipShareActivity.class);
                    intent64995.putExtra("data", vipSharejsonData);
                    mContext.startActivity(intent64995);
                    break;
                case "6521"://搜本院
                    String searchkey = parameter.get("searchkey");
                    String sarch = URLDecoder.decode(searchkey, "utf-8");
                    String id = parameter.get("id");

                    Intent it6521 = new Intent(mContext, SearchAllActivity668.class);
                    it6521.putExtra(SearchAllActivity668.KEYS, sarch);
                    it6521.putExtra(SearchAllActivity668.HOSPITAL_ID, id);
                    it6521.putExtra(SearchAllActivity668.TYPE, "5");
                    mContext.startActivity(it6521);

                    break;
                case "6522": //我的
                    MainTableActivity.mainBottomBar.setCheckedPos(4);
                    Intent intent6522 = new Intent(mContext, MainTableActivity.class);
                    mContext.startActivity(intent6522);
                    break;
                case "6523"://医院分享
                    String sharedata = parameter.get("sharedata");
                    String decode = URLDecoder.decode(sharedata, "utf-8");
                    Log.e(TAG, "decode==" + decode);
                    try {
                        HosShareData hosShareData = JSONUtil.TransformSingleBean(decode, HosShareData.class);
                        String doc_name = hosShareData.getDoc_name();
                        String mHos_name = hosShareData.getHos_name();
                        String mHos_address = hosShareData.getAddress();
                        String shareUrl = hosShareData.getUrl();
                        String shareImgUrl = hosShareData.getImg();
                        ShareWechat mWechat = hosShareData.getWechat();

                        BaseShareView baseShareView = new BaseShareView((Activity) mContext);
                        baseShareView.setShareContent("111").ShareAction(mWechat);
                        baseShareView.getShareBoardlistener()
                                .setSinaText(mHos_name + "," + mHos_address + "," + shareUrl + "@悦美整形APP")
                                .setSinaThumb(new UMImage(mContext, shareImgUrl))
                                .setSmsText(mHos_name + "，" + doc_name + "，" + shareUrl)
                                .setTencentUrl(shareUrl)
                                .setTencentTitle(mHos_name)
                                .setTencentThumb(new UMImage(mContext, shareImgUrl))
                                .setTencentDescription(mHos_address)
                                .setTencentText(mHos_address)
                                .getUmShareListener()
                                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                                    @Override
                                    public void onShareResultClick(SHARE_MEDIA platform) {
                                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                                            Toast.makeText(mContext, " 分享成功啦", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                                    }
                                });

                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                    break;
                case "6571": //重新加入购物车
                    WebViewUrlUtil.getInstance().toRejoinCar(mContext, parameter);
                    break;
                case "6651":
                    mContext.startActivity(new Intent(mContext, TypeProblemActivity.class));
                    break;
                case "6671":
                    if (Utils.isLoginAndBind(mContext)) {
                        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
                        if (!b) {
                            PushNewDialog pushNewDialog = new PushNewDialog(mContext);
                            pushNewDialog.show();
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEW_SHOW_ALERT, "1", "0", "1"));
                            break;
                        } else {
                            String pushtime6671 = parameter.get("pushtime");
                            String id6671 = parameter.get("id");
                            String ztid6671 = parameter.get("ztid");
                            String isLogin6671 = parameter.get("isLogin");
                            String isCallback6671 = parameter.get("isCallback");
                            String title6671 = parameter.get("title");
                            String link6671 = parameter.get("link");
                            HashMap<String, Object> maps6671 = new HashMap<>();
                            if ("1".equals(isCallback6671)) {
                                maps6671.put("pushtime", pushtime6671);
                                maps6671.put("ztid", ztid6671);
                                maps6671.put("id", id6671);
                                maps6671.put("isLogin", isLogin6671);
                                maps6671.put("title", title6671);
                                maps6671.put("link", link6671);
                                new TaoPushApi().getCallBack(mContext, maps6671, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData serviceData) {
                                        if ("1".equals(serviceData.code)) {
                                            Log.e(TAG, "TaoPushApi");
                                        }
                                    }
                                });
                            }
                        }

                        //添加闹钟提醒
                        final String calendar = parameter.get("calendar");
                        if (!TextUtils.isEmpty(calendar)) {
                            final String calendarss = URLDecoder.decode(calendar, "utf-8");
                            Log.e(TAG, "calendarss == " + calendarss);
                            final CalendarReminderDialog yueMeiDialog = new CalendarReminderDialog(mContext);
                            yueMeiDialog.show();
                            yueMeiDialog.setBtnClickListener(new CalendarReminderDialog.BtnClickListener() {
                                @Override
                                public void rightBtnClick(View v) {
                                    int checkCallPhonePermission1 = ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALENDAR);
                                    int checkCallPhonePermission2 = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_CALENDAR);
                                    if (checkCallPhonePermission1 == PackageManager.PERMISSION_GRANTED && checkCallPhonePermission2 == PackageManager.PERMISSION_GRANTED) {
                                        WebViewUrlUtil.getInstance().setAlarmClock(mContext, calendarss);
                                        yueMeiDialog.dismiss();
                                    } else {
                                        Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR).build(), new AcpListener() {
                                            @Override
                                            public void onGranted() {
                                                WebViewUrlUtil.getInstance().setAlarmClock(mContext, calendarss);
                                                yueMeiDialog.dismiss();
                                            }

                                            @Override
                                            public void onDenied(List<String> permissions) {
                                                MyToast.makeTextToast2(mContext, "添加提醒失败", MyToast.SHOW_TIME).show();
                                                yueMeiDialog.dismiss();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }

                    break;
                case "6701"://我的收藏
                    if (Utils.isLoginAndBind(mContext)) {
                        Intent intent6701 = new Intent(mContext, MyCollectActivity550.class);
                        mContext.startActivity(intent6701);
                    }
                    break;
                case "6702"://我的订单-代写日记列表
                    if (Utils.isLoginAndBind(mContext)) {
                        Intent intent6702 = new Intent(mContext, MyOrdersActivity.class);
                        if (!parameter.containsKey("flag")) {
                            String flag = parameter.get("flag");
                            intent6702.putExtra("item", Integer.parseInt(flag));
                        }
                        mContext.startActivity(intent6702);
                    }
                    break;
                case "6721"://到签到页
                    Intent intent6721 = new Intent(mContext, SignWebActivity.class);
                    if (parameter.size() > 0) {
                        ParamsMap paramsMap = new ParamsMap();
                        paramsMap.setMap(parameter);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(SignWebActivity.PARAMS, paramsMap);
                        intent6721.putExtras(bundle);
                    }
                    mContext.startActivity(intent6721);
                    break;
                case "6722"://我的资料页
                    if (Utils.isLoginAndBind(mContext)) {
                        Intent it6722 = new Intent();
                        it6722.putExtra("type", "1");
                        it6722.putExtra("flag", "1");
                        it6722.setClass(mContext, ModifyMyDataActivity.class);
                        mContext.startActivity(it6722);
                    }
                    break;
                case "6723"://日记列表
                    if (Utils.isLoginAndBind(mContext)) {
                        Intent it6723 = new Intent();
                        it6723.putExtra("flag", "1");//1代表从我的日记跳过去的
                        it6723.setClass(mContext, HomeDiarySXActivity.class);
                        mContext.startActivity(it6723);
                    }
                    break;
                case "6724":// 颜究院
                    Intent intent6724 = new Intent();
                    MainTableActivity.mainBottomBar.setCheckedPos(2);
                    intent6724.setClass(mContext, MainTableActivity.class);
                    mContext.startActivity(intent6724);
                    break;
                case "6741":// 项目页
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    Intent mIntent = new Intent();
                    if (parameter.containsKey("medthod")) {
                        String medthod = parameter.get("medthod");
                        mIntent.putExtra("medthod", medthod);
                    }
                    if (parameter.containsKey("one_id")) {
                        String one_id = parameter.get("one_id");
                        mIntent.putExtra("oneid", one_id);
                    }
                    if (parameter.containsKey("two_id")) {
                        String two_id = parameter.get("two_id");
                        mIntent.putExtra("twoid", two_id);
                    }
                    if (parameter.containsKey("three_id")) {
                        String three_id = parameter.get("three_id");
                        mIntent.putExtra("threeid", three_id);
                    }
                    mIntent.setClass(mContext, ProjectDetailActivity638.class);
                    mContext.startActivity(mIntent);
                    break;
                case "6751":        //外链接跳转
                    String link6751 = parameter.get("link");
                    if (link6751.contains("chat.yuemei.com")) {
                        boolean loginAndBind = Utils.isLoginAndBind(mContext);
                        if (!loginAndBind) {
                            break;
                        }
                    }
                    //是否隐藏原生头部 0:不隐藏 1:隐藏（可选， 默认 0）
                    String isHide6751 = "0";
                    if (parameter.containsKey("isHide")) {
                        isHide6751 = parameter.get("isHide");
                    }
                    //是否可以下拉刷新 0:不可以 1:可以（可选， 默认 1）
                    String isRefresh6751 = "1";
                    if (parameter.containsKey("isRefresh")) {
                        isRefresh6751 = parameter.get("isRefresh");
                    }

                    String isRemoveUpper6751 = "0";
                    if (parameter.containsKey("isRemoveUpper")) {
                        isRemoveUpper6751 = parameter.get("isRemoveUpper");
                    }
                    //enableSafeArea: 是否开启顶部安全区域 0:关闭 1:开启（可选， 默认 0）状态栏是否显示
                    String enableSafeArea = "0";
                    if (parameter.containsKey("enableSafeArea")) {
                        enableSafeArea = parameter.get("enableSafeArea");
                    }

                    Log.e(TAG, "link6751 == " + link6751);
                    Log.e(TAG, "isHide6751 == " + isHide6751);
                    Log.e(TAG, "isRefresh6751 == " + isRefresh6751);
                    Log.e(TAG, "isRemoveUpper6751 == " + isRemoveUpper6751);
                    WebData webData = new WebData(link6751);
                    webData.setShowTitle("0".equals(isHide6751));
                    webData.setShowRefresh("1".equals(isRefresh6751));
                    webData.setShowTopbar("1".equals(enableSafeArea));
                    WebUtil.getInstance().startWebActivity(mContext, webData);

                    //关闭页面
                    if ("1".equals(isRemoveUpper6751)) {
                        finish(mContext);
                    }
                    break;
                case "6753":  //关注
                    if (Utils.isLoginAndBind(mContext)) {
                        HashMap<String, Object> parms = new HashMap<>();
                        if (parameter.containsKey("obj_type")) {
                            String obj_type = parameter.get("obj_type");
                            parms.put("type", obj_type);
                        }
                        if (parameter.containsKey("obj_id")) {
                            String obj_id = parameter.get("obj_id");
                            parms.put("objid", obj_id);
                        }
                        new FocusAndCancelApi().getCallBack(mContext, parms, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    try {
                                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                                        switch (focusAndCancelData.getIs_following()) {
                                            case "0":          //0:+关注
                                                webView.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        webView.loadUrl("javascript:changeText(\"关注\")");
                                                    }
                                                });
                                                webView.reload();
                                                break;
                                            case "1":          //1:已关注
                                                webView.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        webView.loadUrl("javascript:changeText(\"已关注\")");
                                                    }
                                                });
                                                webView.reload();
                                                if (TextUtils.isEmpty((String) ohterPrams)) return;
                                                HashMap<String, Object> hashMap = new HashMap<>();
                                                hashMap.put("hos_id", ohterPrams);
                                                hashMap.put("pos", "9");
                                                new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                                    @Override
                                                    public void onSuccess(ServerData s) {
                                                        if ("1".equals(s.code)) {
                                                            Log.e(TAG, s.message);
                                                        }
                                                    }
                                                });
                                                HashMap<String, Object> hashMap2 = new HashMap<>();
                                                hashMap2.put("obj_type", "9");
                                                hashMap2.put("obj_id", ohterPrams);
                                                hashMap2.put("hos_id", ohterPrams);
                                                new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                                    @Override
                                                    public void onSuccess(ServerData s) {
                                                        if ("1".equals(s.code)) {
                                                            Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                                        }
                                                    }
                                                });
                                                break;
                                            case "2":          //2:互相关注
                                                webView.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        webView.loadUrl("javascript:changeText(\"互相关注\")");
                                                    }
                                                });
                                                webView.reload();
                                                if (TextUtils.isEmpty((String) ohterPrams)) return;
                                                HashMap<String, Object> hashMap1 = new HashMap<>();
                                                hashMap1.put("hos_id", ohterPrams);
                                                hashMap1.put("pos", "9");
                                                new AutoSendApi().getCallBack(mContext, hashMap1, new BaseCallBackListener<ServerData>() {
                                                    @Override
                                                    public void onSuccess(ServerData s) {
                                                        if ("1".equals(s.code)) {
                                                            Log.e(TAG, s.message);
                                                        }
                                                    }
                                                });
                                                HashMap<String, Object> hashMap3 = new HashMap<>();
                                                hashMap3.put("obj_type", "9");
                                                hashMap3.put("obj_id", ohterPrams);
                                                hashMap3.put("hos_id", ohterPrams);
                                                new AutoSendApi2().getCallBack(mContext, hashMap3, new BaseCallBackListener<ServerData>() {
                                                    @Override
                                                    public void onSuccess(ServerData s) {
                                                        if ("1".equals(s.code)) {
                                                            Log.e(TAG, "AutoSendApi2 ==" + s.message);
                                                        }
                                                    }
                                                });
                                                break;
                                        }

                                        MyToast.makeTextToast2(mContext, serverData.message, MyToast.SHOW_TIME).show();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                    }
                    break;
                case "6821":
                    type6821Handle(mContext, parameter);
                    break;
                case "6881":
                    if (mContext instanceof Activity) {
                        Intent intent6881 = new Intent(mContext, MainCitySelectActivity560.class);
                        ((Activity) mContext).startActivityForResult(intent6881, RequestAndResultCode.WEB_VIEW_REQUEST_CODE);
                    }
                    break;
                case "6882":
                    Intent it6882 = new Intent(mContext, SearchAllActivity668.class);
                    if (parameter.containsKey("searchWord")) {
                        it6882.putExtra("keys", parameter.get("searchWord"));
                    } else {
                        it6882.putExtra("keys", "");
                    }
                    mContext.startActivity(it6882);
                    break;
                case "7041":
                    Intent it7041 = new Intent(mContext, ProjectContrastActivity.class);
                    it7041.putExtra("sku_id", parameter.get("tao_id"));
                    it7041.putExtra("index", parameter.get("index"));
                    it7041.putExtra("tao_source", parameter.get("tao_source"));
                    mContext.startActivity(it7041);
                    break;
                case "7042":
                    EventBus.getDefault().post(new MsgEvent(DELETE_CUR, parameter.get("tao_id")));
                    break;
                case "7043":
                    //跳转订单确认页
                    String taoData = URLDecoder.decode(parameter.get("tao_data"), "utf-8");
                    TaoDataInfo7043 taoDataInfo7043 = JSONUtil.TransformSingleBean(taoData, TaoDataInfo7043.class);
//                    parameter.get("change_number");
//                    parameter.get("group_id");
//                    parameter.get("is_group");
//                    parameter.get("order_type");

                    Intent intent7043 = new Intent(mContext, MakeSureOrderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("tao_id", taoDataInfo7043.getTao_id());
                    bundle.putString("number", taoDataInfo7043.getNumber());
                    bundle.putString("source", taoDataInfo7043.getSource());
                    bundle.putString("objid", taoDataInfo7043.getObjid());
                    bundle.putString("buy_for_cart", "0");
                    String bargainOrderId = taoDataInfo7043.getBargain_order_id();
                    if (!TextUtils.isEmpty(bargainOrderId)) {
                        bundle.putString("bargain_order_id", bargainOrderId);
                    }
//                    bundle.putString("u", mU);
                    bundle.putString("group_id", parameter.get("group_id"));
                    bundle.putString("is_group", parameter.get("is_group"));
//                    bundle.putString("bargain_order_id", parameter.get("bargain_order_id"));
                    intent7043.putExtra("data", bundle);
                    mContext.startActivity(intent7043);
                    break;
                case "7161"://显示筛选弹窗
                    String isTaoFilter ="0";
                    SkuVideoChatDialog skuVideoChatDialog = null;
                    if (parameter.containsKey("is_tao_filter")){ //1是sku弹出 0不是
                        isTaoFilter = parameter.get("is_tao_filter");
                    }
                    if (parameter.containsKey("request_params")) {
                        String requestParams = parameter.get("request_params");
                        String deRequestParams = URLDecoder.decode(requestParams, "utf-8");
                        Map<String, Object> mapForJson = JSONUtil.getMapForJson(deRequestParams);
                        Log.e(TAG,"mapForJson =="+mapForJson.toString());
                        FaceVdieoBean faceVdieoBean = new FaceVdieoBean();
                        faceVdieoBean.setParams(mapForJson);
                        String dialogUrl = "";
                        if (parameter.containsKey("link")){
                            dialogUrl = parameter.get("link");
                        }else {
                            dialogUrl = FinalConstant.VIDEOFILTER;
                        }
                        float dialogHeight;
                        if ("1".equals(isTaoFilter)){
                            dialogHeight = 0.5f;
                        }else {
                            dialogHeight = 0.68f;
                        }
                        skuVideoChatDialog = new SkuVideoChatDialog(mContext,dialogUrl, dialogHeight, faceVdieoBean);
                        skuVideoChatDialog.show();
                    }else {
                        String dialogUrl = "";
                        if (parameter.containsKey("link")){
                            dialogUrl = parameter.get("link");
                        }else {
                            dialogUrl = FinalConstant.VIDEOFILTER;
                        }
                        float dialogHeight;
                        if ("1".equals(isTaoFilter)){
                            dialogHeight = 0.68f;
                        }else {
                            dialogHeight = 0.5f;
                        }
                        skuVideoChatDialog = new SkuVideoChatDialog(mContext,dialogUrl, dialogHeight, null);
                        skuVideoChatDialog.show();
                    }
                    break;
                case "7162"://匹配面诊医生
                    handle7162(mContext, parameter);
                    break;
                case "7163"://淘整形是否有视频面诊
                    handle7163(mContext, parameter);
                    break;
                case "7164"://视频面诊聊天页
                    handle7164(mContext, parameter,ohterPrams);
                    break;
                case "7165"://匹配成功展示弹层
                    String hitHospitalAccountId = "";
                    if (parameter.containsKey("hit_hospital_account_id")){
                        hitHospitalAccountId = parameter.get("hit_hospital_account_id");
                    }
                    if (null != ohterPrams){
                        String faceVideoUrl = (String)ohterPrams;
                        if (!TextUtils.isEmpty(hitHospitalAccountId)){
                            new SkuVideoChatDialog(mContext,FinalConstant.HITDOCTORSINFO,0.45f,new FaceVdieoBean(faceVideoUrl,hitHospitalAccountId)).show();
                        }
                    }else {
                        new SkuVideoChatDialog(mContext,FinalConstant.HITDOCTORSINFO,0.45f,null).show();
                    }
                    break;
                case "7166"://弹出信息提示框
                    if (parameter.containsKey("message")){
                        String message = parameter.get("message");
                        String deMessage = URLDecoder.decode(message, "utf-8");
                        MyToast.makeTextToast2(mContext,deMessage,Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    if (url.startsWith("http")) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                    break;
            }

        } catch (Exception e) {
            Log.e(TAG, "e == " + e.toString());
            e.printStackTrace();
        }
    }

    private void handle7164(final Context mContext, HashMap<String, String> parameter,Object otherParams) throws UnsupportedEncodingException {
        if (parameter.containsKey("request_params")) {
            String requestParams = parameter.get("request_params");
            String deRequestParams = URLDecoder.decode(requestParams, "utf-8");
            Map<String, Object> mapForJson = JSONUtil.getMapForJson(deRequestParams);
            if (null != otherParams){
                if (!TextUtils.isEmpty((String)otherParams)){
                    mapForJson.put("hit_hospital_account_id",(String)otherParams);
                }
            }
            new CreateFaceVideoApi().getCallBack(mContext, mapForJson, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    if ("1".equals(serverData.code)) {
                        CreateFaceVideoBean createFaceVideoBean = JSONUtil.TransformSingleBean(serverData.data, CreateFaceVideoBean.class);
                        if (null != createFaceVideoBean){
                            Intent intent = new Intent(mContext, VideoChatActivity.class);
                            intent.putExtra(VideoChatActivity.VIDEO_CHATACTIVITY_VALUE,createFaceVideoBean);
                            mContext.startActivity(intent);
                        }
                    }else {
                        MyToast.makeTextToast2(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handle7162(final Context mContext, HashMap<String, String> parameter) throws UnsupportedEncodingException {
        if (parameter.containsKey("filter_params")) {
            String requestParams = parameter.get("filter_params");
            String deRequestParams = URLDecoder.decode(requestParams, "utf-8");
            Map<String, Object> mapForJson = JSONUtil.getMapForJson(deRequestParams);
            new MatchVideoDoctorsApi().getCallBack(mContext, mapForJson, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    if ("1".equals(serverData.code)) {
                        MatchVideoDoctorBean videoDoctorsBean = JSONUtil.TransformSingleBean(serverData.data, MatchVideoDoctorBean.class);
                        String isHitDoctors = videoDoctorsBean.getIs_hit_doctors();//是否匹配到面诊医生
                        String chatUrl = videoDoctorsBean.getChat_url();//私信type识别地址
                        String faceVideoUrl = videoDoctorsBean.getFace_video_url();//视频面诊type识别地址
                        String hitDoctorsInfoUrl = videoDoctorsBean.getHit_doctors_info_url();//匹配到医生信息展示弹层type识别地址
                        String showMessage = videoDoctorsBean.getShow_message();
                        if (!"1".equals(isHitDoctors)){
                            // 非命中 根据参数(chatUrl)type识别
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, chatUrl);
                            if (!TextUtils.isEmpty(showMessage)){
                                MyToast.makeTextToast2(mContext,showMessage,Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, null,hitDoctorsInfoUrl,faceVideoUrl);
                        }

                    }else {
                        MyToast.makeTextToast2(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void handle7163(final Context mContext, HashMap<String, String> parameter) throws UnsupportedEncodingException {
        if (parameter.containsKey("request_params")){
            String requestParams = parameter.get("request_params");
            String deRequestParams = URLDecoder.decode(requestParams, "utf-8");
            Map<String, Object> mapForJson = JSONUtil.getMapForJson(deRequestParams);
            new TaoMatchVideoDoctorsApi().getCallBack(mContext, mapForJson, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    if ("1".equals(serverData.code)){
                        TaoMatchVideoDoctorsBean videoDoctorsBean = JSONUtil.TransformSingleBean(serverData.data, TaoMatchVideoDoctorsBean.class);
                        String isHitDoctors = videoDoctorsBean.getIs_hit_doctors();//是否命中面诊医生
                        String showMessage = videoDoctorsBean.getShow_message();//展示弹层信息
                        String jumpUrl = videoDoctorsBean.getJump_url();//私信type识别地址/筛选H5页type识别地址
                        if (!"1".equals(isHitDoctors)){ //弹层提示错误信息(show_message), 跳转私信(jump_url)
                            if (!TextUtils.isEmpty(showMessage)){
                                MyToast.makeTextToast2(mContext,showMessage,Toast.LENGTH_SHORT).show();
                            }
                        }
                        // 命中 根据参数(jump_url)type识别(type:7161)请求筛选H5展示
                        if (!TextUtils.isEmpty(jumpUrl)){
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, jumpUrl);
                        }
                    }else {
                        MyToast.makeTextToast2(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    //type:eq:6821:and:postType:eq:2:and:itemId:eq:18854:and:itemName:eq:审美排行榜:and:urlParames:eq:{"post_source":"ztvote"}
    //6821处理
    private void type6821Handle(Context mContext, HashMap<String, String> parameter) {
        if (parameter.containsKey("postType")) {
            String itemId = "";
            String itemName = "";
            String urlParames = "";
            Intent intent6821 = new Intent(mContext, SelectSendPostsActivity.class);
            String postType6821 = parameter.get("postType");
            switch (postType6821) {
                case "0"://发提问
                    Intent intent0 = new Intent(mContext, TypeProblemActivity.class);
                    mContext.startActivity(intent0);
                    break;
                case "1"://写日记
                    if (Utils.isLoginAndBind(mContext)) {
                        WriteNoteManager.getInstance(mContext).ifAlert(null);
                    }
                    break;
                case "2"://随便聊聊
                    if (Utils.isLoginAndBind(mContext)) {
                        Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
                        it3.putExtra("source", "SelectSendPostsActivity");
                        if (parameter.containsKey("itemId")) {
                            itemId = parameter.get("itemId");
                            it3.putExtra("itemId", itemId);
                            it3.putExtra("cateid", itemId);
                        }
                        if (parameter.containsKey("itemName")) {
                            itemName = parameter.get("itemName");
                            it3.putExtra("itemName", itemName);
                        }
                        if (parameter.containsKey("urlParames")) {
                            urlParames = parameter.get("urlParames");
                            it3.putExtra("urlParames", urlParames);
                        }
                        mContext.startActivity(it3);
                        finish(mContext);
                    }
                    break;
                case "3"://更新日记
                    intent6821.putExtra("cateid", "3");
                    mContext.startActivity(intent6821);
                    break;
                case "4"://报名
                    intent6821.putExtra("cateid", "4");
                    mContext.startActivity(intent6821);
                    break;
                case "11"://发评价
                    String data6821 = Utils.unicodeDecode(parameter.get("data"));
                    Intent it6821 = new Intent(mContext, EssaySkuActivity.class);
                    it6821.putExtra("data", data6821);
                    mContext.startActivity(it6821);
                    break;
            }

        } else {
            Intent intent6821 = new Intent(mContext, SelectSendPostsActivity.class);
            intent6821.putExtra("cateid", "0");
            mContext.startActivity(intent6821);
        }
    }

    private void chat5983(Context mContext, Object ohterPrams, HashMap<String, String> parameter, String id5983) throws UnsupportedEncodingException {
        if (parameter.containsKey("chatData")) {
            String chatData = parameter.get("chatData");
            String decode = URLDecoder.decode(chatData, "utf-8");
            ChatParmsBean chatParmsBean = JSONUtil.TransformSingleBean(decode, ChatParmsBean.class);
            int ymaq_class = chatParmsBean.getYmaq_class();
            String ymaq_id = chatParmsBean.getYmaq_id();
            int obj_type = chatParmsBean.getObj_type();
            String obj_id = chatParmsBean.getObj_id();
            String zt_title = chatParmsBean.getZt_title();
            String zt_id = chatParmsBean.getZt_id();
            ChatParmsBean.GuijiBean guiji = chatParmsBean.getGuiji();

            if (guiji != null) {
                String image = guiji.getImage();
                String member_price = guiji.getMember_price();
                String price = guiji.getPrice();
                String title = guiji.getTitle();
                String url5983 = guiji.getUrl();

                String classid = "";
                String content = "";
                String quickReply = "";
                if (parameter.containsKey("continueToSend")) {
                    String continueToSend = parameter.get("continueToSend");
                    String continueData = URLDecoder.decode(continueToSend, "utf-8");
                    ContinueToSend singleBean = JSONUtil.TransformSingleBean(continueData, ContinueToSend.class);
                    classid = singleBean.getClassid();
                    content = singleBean.getContent();
                    quickReply = singleBean.getQuick_reply();
                }
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(id5983)
                        .setObjId(obj_id)
                        .setObjType(obj_type + "")
                        .setTitle(title)
                        .setImg(image)
                        .setUrl(url5983)
                        .setMemberPrice(member_price)
                        .setPrice(price)
                        .setYmClass(ymaq_class + "")
                        .setYmId(ymaq_id)
                        .setZt_id(zt_id)
                        .setZt_title(zt_title)
                        .setClassid(classid)
                        .setContent(content)
                        .setQuick_reply(quickReply)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            } else {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(id5983)
                        .setObjId(obj_id)
                        .setObjType(obj_type + "")
                        .setYmClass(ymaq_class + "")
                        .setYmId(ymaq_id)
                        .setZt_id(zt_id)
                        .setZt_title(zt_title)
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            }

        } else {
            if (ohterPrams != null && !"".equals(ohterPrams)) {
                Type5983Bean type5983Bean = (Type5983Bean) ohterPrams;
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(id5983)
                        .setObjId(type5983Bean.getObjId())
                        .setObjType(type5983Bean.getObjType5983())
                        .setTitle(type5983Bean.getTitle5983())
                        .setImg(type5983Bean.getImg5983())
                        .setUrl(type5983Bean.getUrl())
                        .setYmClass(type5983Bean.getmYmClass())
                        .setYmId(type5983Bean.getmYmId())
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            } else {
                ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                        .setDirectId(id5983)
                        .setObjId("0")
                        .setObjType("0")
                        .build();
                new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
            }
        }
    }

    /**
     * type值解析
     *
     * @param url : type 串
     * @return ：返回参数
     */
    public WebUrlData parserPagrms(String url) {
        HashMap<String, String> hashMap = new HashMap<>();
        String type = "";

        String pagramsStr[] = url.split(":and:");

        for (String strValue : pagramsStr) {
            String str[] = strValue.split(":eq:");
            if (str.length > 0) {
                if ("type".equals(str[0])) {
                    type = str[1];
                } else {
                    if (str.length == 2) {
                        hashMap.put(str[0], str[1]);
                    } else {
                        hashMap.put(str[0], "");
                    }
                }
            }
        }

        return new WebUrlData(type, hashMap);
    }


    /**
     * 加载web
     */
    public void LodUrl(WebView webView, String url, HashMap<String, Object> hashMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, hashMap);
        Log.d(TAG, "url:==" + addressAndHead.getUrl());
        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }


    /**
     * 关闭页面
     */
    private void finish(Context context) {
        if (context instanceof Activity) {
            ((Activity) context).finish();
        }
    }
}
