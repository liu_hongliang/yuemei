package com.module.community.web;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.module.other.netWork.netWork.FinalConstant1;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.WebUrlTypeUtil;

/**
 * 对加载Url回调统一处理
 * Created by 裴成浩 on 2019/6/11
 */
public class WebViewClientImpl extends WebViewClient {

    private String TAG = "WebViewClientImpl";

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        Log.e(TAG, "url == " + url);
        if (webView != null && url != null) {
            Context context = webView.getContext();
            if (url.startsWith("type")) {
                if (context instanceof Activity) {
                    webUrlLoading(webView, url);
                }
            } else {
                if (url.endsWith(".apk")) {
                    Uri apkUri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, apkUri);
                    context.startActivity(intent);
                } else if (url.startsWith("http")) {
                    WebUrlTypeUtil.getInstance(context).urlToApp(url);
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    if (isInstall(context, intent)) {
                        context.startActivity(intent);
                    }
                }
            }
        }
        return true;
    }

    /**
     * web页面跳转
     *
     * @param webView
     * @param url
     */
    private void webUrlLoading(WebView webView, String url) {
        try {
            WebUrlData webUrlData = WebViewUrlLoading.getInstance().parserPagrms(url);
            switch (webUrlData.getType()) {
                case "531":                //本页面跳转刷新
                case "64991":              //优惠劵页面本页面刷新
                    String mUrl = webUrlData.getParameter().get("link");
                    if (!mUrl.startsWith("http")) {
                        mUrl = FinalConstant1.BASE_HTML_URL+FinalConstant1.SYMBOL2+FinalConstant.VER + mUrl;
                        Context context = webView.getContext();
                        if (context instanceof WebViewActivity) {
                            ((WebViewActivity) context).mWebData.url=mUrl;
                        }
                    }
                    WebUtil.getInstance().loadPage(webView, mUrl);
                    break;
                default:
                    WebViewUrlLoading.getInstance().showWebDetail(webView, url);
            }
        } catch (Exception e) {
            WebViewUrlLoading.getInstance().showWebDetail(webView, url);
        }
    }

    // 判断app是否安装
    private boolean isInstall(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0;
    }
}
