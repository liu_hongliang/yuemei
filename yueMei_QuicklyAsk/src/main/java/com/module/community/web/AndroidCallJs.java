package com.module.community.web;

/**
 * andorid原生调用H5方法
 *
 * @author 裴成浩
 * @data 2019/10/16
 */
public class AndroidCallJs {

    private static final String javascript = "javascript:";

    //调取是否刷新方法,返回boolean类型
    public static final String htmlBackReFresh = javascript + "htmlBackReFresh()";
}
