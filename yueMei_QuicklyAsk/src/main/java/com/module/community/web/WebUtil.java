package com.module.community.web;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import android.webkit.WebView;

import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;

import org.apache.commons.lang.ArrayUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>url链接后边必须要有/  如：http://www.baidu.com错误    http://www.baidu.com/ 正确</>
 * WebView初始化和加载页面的工具类
 * Created by 裴成浩 on 2019/6/11
 */
public class WebUtil {

    private String TAG = "WebUtil";

    private WebUtil() {
    }

    private static class Holder {
        private static final WebUtil INSTANCE = new WebUtil();
    }

    public static WebUtil getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * web页面统一加载入口
     *
     * @param webView
     * @param url
     */
    public void loadPage(WebView webView, String url) {
        Log.e(TAG,"url === "+url);
        //如果是电话协议
        if (url.contains("tel:")) {
            callPhone(webView.getContext(), url);
            return;
        }

        //判断当前url是否是网络资源
        if (URLUtil.isNetworkUrl(url) || URLUtil.isAssetUrl(url)) {
            loadWebPage(webView, url);
        } else {
            loadLocalPage(webView, url);
        }
    }

    /**
     * 加载网络资源web页面
     *
     * @param webView
     * @param url
     */
    private void loadWebPage(WebView webView, String url) {
        if (webView != null) {
            Log.e(TAG, "url == " + url);
            if (url.startsWith("http")) {

                Uri mUri = Uri.parse(url);
                // 协议
                String scheme = mUri.getScheme();
                // 域名
                String host = mUri.getHost();
                // 路径
                String path = mUri.getPath();

                Log.e(TAG, "scheme == " + scheme);
                Log.e(TAG, "host == " + host);
                Log.e(TAG, "path == " + path);
                if (host != null && host.contains(FinalConstant1.YUEMEI_DOMAIN_NAME)) {
                    //参数分割
                    String[] parameters = path.split("/");

                    int initArrLen = parameters.length;
                    Log.e(TAG,"initArrLen =="+initArrLen);

                    String ver = null;
                    //去除版本号和空元素
                    String[] tempArr = path.split("/");
                    for (String parameter : tempArr) {
                        Log.e(TAG, "parameters == " + parameter);

                        if (TextUtils.isEmpty(parameter))
                            parameters = (String[]) ArrayUtils.removeElement(parameters, parameter);
                        else if (FinalConstant1.YUEMEI_VER.equals(parameter)) {
                            ver = FinalConstant1.YUEMEI_VER;
                            parameters = (String[]) ArrayUtils.removeElement(parameters, parameter);
                        }
                    }


                    //得到控制器和方法名

                    String controller = "";
                    String methodName = "";
                    if (initArrLen >=3){
                        controller = parameters[0];
                        methodName = parameters[1];
                    }else {
                        //https://sjapp.yuemei.com/usernew/scorelist/
                        if ("http".equals(scheme)){
                            StringBuilder builder = new StringBuilder(url);
                            builder.insert(4,"s");
                            String mUrl = builder.toString();
                            Log.e(TAG,"murl=="+mUrl);
                            webView.loadUrl(mUrl);
                        }else {
                            webView.loadUrl(url);
                        }
                        return;
                    }


                    //拼接请求链接
                    String mUrl = scheme + FinalConstant1.SYMBOL1 + host + FinalConstant1.SYMBOL2;

                    //拼接版本号
                    if (!TextUtils.isEmpty(ver)) {
                        //如果有版本号
                        mUrl = mUrl + ver + FinalConstant1.SYMBOL2;
                    } else if (host.equals(FinalConstant1.BASE_URL)) {
                        //如果没有版本号且是sjapp.yuemei.com 域名手动拼接
                        mUrl = mUrl + FinalConstant1.YUEMEI_VER + FinalConstant1.SYMBOL2;
                    }
                    mUrl = mUrl + controller + FinalConstant1.SYMBOL2 + methodName + FinalConstant1.SYMBOL2;
                    Log.e(TAG, "controller == " + controller);
                    Log.e(TAG, "methodName == " + methodName);
                    Log.e(TAG, "mUrl == " + mUrl);

                    //获取剩余参数
                    List<String> keys = new ArrayList<>();
                    List<String> values = new ArrayList<>();

                    for (int i = 2; i < parameters.length; i++) {
                        if (i % 2 == 0) {
                            Log.e(TAG, "keys == " + parameters[i]);
                            keys.add(parameters[i]);
                        } else {
                            Log.e(TAG, "values == " + parameters[i]);
                            values.add(parameters[i]);
                        }
                    }

                    //判断key和values个数是否一致
                    if (keys.size() == values.size()) {
                        HashMap<String, Object> hashMap = new HashMap<>();
                        for (int i = 0; i < keys.size(); i++) {
                            hashMap.put(keys.get(i), values.get(i));
                        }
                        WebSignData addressAndHead = SignUtils.getAddressAndHead(mUrl, hashMap);
                        Log.e(TAG, "url====" + addressAndHead.getUrl());
                        Log.e(TAG, "httpHeaders====" + addressAndHead.getHttpHeaders());
                        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

                    } else {
                        Log.e(TAG, "链接异常，检查链接拼接是否正确");
                    }
                } else {
                    if ("http".equals(scheme)){
                        StringBuilder builder = new StringBuilder(url);
                        builder.insert(4,"s");
                        String mUrl = builder.toString();
                        Log.e(TAG,"murl=="+mUrl);
                        webView.loadUrl(mUrl);
                    }else {
                        webView.loadUrl(url);
                    }
                }
            } else {
                webView.loadUrl(url);
            }

        } else {
            throw new NullPointerException("WebView is null!");
        }
    }

    /**
     * 如果是本地资源
     *
     * @param webView
     * @param url
     */
    private void loadLocalPage(WebView webView, String url) {
        loadWebPage(webView, "file:///android_assets/" + url);
    }

    /**
     * 电话类型协议
     *
     * @param context
     * @param uri
     */
    private void callPhone(Context context, String uri) {
        final Intent intent = new Intent(Intent.ACTION_DIAL);
        final Uri data = Uri.parse(uri);
        intent.setData(data);
        context.startActivity(intent);
    }

    /**
     * 打开WebActivity
     */
    public void startWebActivity(Context context, String url) {
        startWebActivity(context, url, null);
    }

    /**
     * 打开WebActivity
     */
    public void startWebActivity(Context context, final String url, final String title) {
        // WebView初始化
        WebData data = new WebData(url, title);
        startWebActivity(context, data);
    }

    /**
     * 打开WebActivity
     */
    public void startWebActivity(Context context, WebData data) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(WebViewActivity.WEB_DATA, data);
        context.startActivity(intent);
    }


    public Intent setIntnet(Context context, WebData data) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(WebViewActivity.WEB_DATA, data);
        return intent;
    }
}
