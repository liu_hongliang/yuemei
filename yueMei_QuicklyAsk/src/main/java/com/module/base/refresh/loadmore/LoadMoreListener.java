package com.module.base.refresh.loadmore;

/**
 * 加载更多监听
 * Created by 裴成浩 on 2018/3/1.
 */
public interface LoadMoreListener {
    
    void onLoadMore();

}
