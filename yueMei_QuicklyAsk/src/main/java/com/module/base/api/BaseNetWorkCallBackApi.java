package com.module.base.api;

import android.util.Log;

import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;

/**
 * 公共的回调
 * Created by 裴成浩 on 2019/6/24
 */
public class BaseNetWorkCallBackApi {

    private HashMap<String, Object> mHashMap;  //传值容器
    private final String mController;
    private final String mMethodName;
    private String TAG = "BaseNetWorkCallBackApi";

    public BaseNetWorkCallBackApi(String controller, String methodName) {
        mHashMap = new HashMap<>();
        this.mController = controller;
        this.mMethodName = methodName;
    }

    /**
     * 开始请求
     *
     * @param listener
     */
    public void startCallBack(final BaseCallBackListener<ServerData> listener) {
        NetWork.getInstance().call(mController, mMethodName, mHashMap, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                    listener.onSuccess(mData);
            }
        });
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

}
