package com.module.base.api;

import android.content.Context;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/9/29.
 */
public interface BaseCallBackApi {
    void getCallBack(Context context, Map<String, Object> maps, BaseCallBackListener listener);
}
