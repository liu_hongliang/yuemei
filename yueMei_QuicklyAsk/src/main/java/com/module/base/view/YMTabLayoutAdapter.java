package com.module.base.view;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import java.util.List;

/**
 * TabLayout适配器
 * Created by 裴成浩 on 2019/2/22
 */
public class YMTabLayoutAdapter extends FragmentStatePagerAdapter {
    private List<String> titleList;
    private List<YMBaseFragment> fragmentList;

    public YMTabLayoutAdapter(FragmentManager fm, List<String> mPageTitleList, List<YMBaseFragment> mFragmentList) {
        super(fm);
        this.titleList = mPageTitleList;
        this.fragmentList = mFragmentList;
    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }

    /**
     * 解决可能会出现的空指针问题
     *
     * @param state
     * @param loader
     */
    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }


}
