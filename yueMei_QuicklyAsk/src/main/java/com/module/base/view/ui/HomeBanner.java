package com.module.base.view.ui;

import android.content.Context;
import android.util.AttributeSet;

import com.quicklyask.util.Utils;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页banner
 * Created by 裴成浩 on 2019/9/18
 */
public class HomeBanner extends Banner {
    private Context mContext;
    private final String TAG = "YMBannebannerr";
    private List<String> mUrlImageList = new ArrayList<>();

    public HomeBanner(Context context) {
        this(context, null);
    }

    public HomeBanner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        initView();
    }

    private void initView() {
        //设置banner样式
        setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        setImageLoader(new GlideImageLoader(mContext, Utils.dip2px(6)));
        //设置banner动画效果
        setBannerAnimation(Transformer.Default);
        //设置自动轮播，默认为true
        isAutoPlay(true);
        //设置轮播时间
        setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        setIndicatorGravity(BannerConfig.RIGHT);
    }

    /**
     * 设置图片集合，并开始轮播
     *
     * @param urlImageList 图片集合数据
     */

    public void setImagesData(List<String> urlImageList) {
        this.mUrlImageList = urlImageList;
        setImages(mUrlImageList);
        start();
    }

    /**
     * 获取图片集合，用来判断banner是否已经开始播放了
     *
     * @return
     */
    public List<String> getUrlImageList() {
        return mUrlImageList;
    }
}
