package com.module.base.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.module.my.controller.activity.UserAgreementWebActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 用户协议
 * Created by 裴成浩 on 2018/12/29
 */
public class PrivacyAgreementDialog extends Dialog {
    private Context mContext;
    private String TEXT_VIEW1 = "感谢您的信任并使用悦美！\n" +
            "\n" +
            "我们依据最新的法律要求更新了隐私政策，特向您推送本提示。请您仔细阅读并充分理解相应条款，方便您了解自己的权利。注册协议和隐私政策主要包含如下内容：\n" +
            "\n" +
            "1.您在使用我们的产品和相关服务时，将会提供与具体功能相关的个人信息（可能涉及账号、位置、交易等信息）\n" +
            "\n" +
            "2.您可对上述信息进行查询、更正、删除以及用户注销等操作。\n" +
            "\n" +
            "3.未经您的同意我们不会把上述信息用于您未授权的其他用途或目的\n" +
            "\n" +
            "您点击同意即表示您已阅读并同意我们的";

    private String TEXT_VIEW2 = "《用户注册协议和隐私政策》";
    private String TEXT_VIEW3 = "。我们将尽全力保护您的个人信息及合法权益，再次感谢您的信任！";

    private String TAG = "PrivacyAgreementDialog";

    public PrivacyAgreementDialog(Context context) {
        this(context, R.style.mystyle);
    }

    public PrivacyAgreementDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.privacy_agreement_view, null);
        setContentView(view);

        setCanceledOnTouchOutside(false);

        TextView textTitle = view.findViewById(R.id.privacy_agreement_title);
        textTitle.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

        TextView textview = view.findViewById(R.id.privacy_agreement_textview);
        Button cancelBtn = view.findViewById(R.id.privacy_agreement_cancel);
        Button confirmBtn = view.findViewById(R.id.privacy_agreement_confirm);

        //设置富文本
        textview.setMovementMethod(LinkMovementMethod.getInstance());
        textview.setText(setStringText());

        //不同意
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCancelClick(view);
                }
            }
        });

        //同意
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onEventClickListener != null) {
                    onEventClickListener.onConfirmClick(view);
                }
            }
        });
    }

    public interface OnEventClickListener {
        void onCancelClick(View v);                          //不同意

        void onConfirmClick(View v);                         //同意
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    /**
     * @return
     */
    private SpannableString setStringText() {
        SpannableString spannableString = new SpannableString(TEXT_VIEW1 + TEXT_VIEW2 + TEXT_VIEW3);

        Log.e(TAG, "spannableString == " + spannableString);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                mContext.startActivity(new Intent(mContext, UserAgreementWebActivity.class));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Utils.getLocalColor(mContext, R.color.red));
                ds.setUnderlineText(false); //是否设置下划线
            }
        }, TEXT_VIEW1.length(), TEXT_VIEW1.length() + TEXT_VIEW2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableString;
    }
}