package com.module.base.view.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.quicklyask.activity.R;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.loader.ImageLoader;
import com.youth.banner.loader.ImageLoaderInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/27
 */
public class YMBanner extends Banner {
    private Context mContext;
    private final String TAG = "YMBanner";
    private List<String> urlImageList = new ArrayList<>();

    public YMBanner(Context context) {
        this(context, null);
    }

    public YMBanner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public YMBanner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        initView();
    }

    private void initView() {
        //设置banner样式
        setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        setImageLoader(new GlideImageLoader());
        //设置banner动画效果
        setBannerAnimation(Transformer.Default);
        //设置自动轮播，默认为true
        isAutoPlay(true);
        //设置轮播时间
        setDelayTime(3000);
        //设置指示器位置（当banner模式中有指示器时）
        setIndicatorGravity(BannerConfig.CENTER);


    }

    /**
     * 设置图片集合，并开始轮播
     *
     * @param urlImageList 图片集合数据
     */
    public void setImagesData(List<String> urlImageList) {
        this.urlImageList = urlImageList;
        setImages(urlImageList);
        start();
    }

    /**
     * 获取图片集合，用来判断banner是否已经开始播放了
     * @return
     */
    public List<String> getUrlImageList() {
        return urlImageList;
    }

    /**
     * 设置标题集合（当banner样式有显示title时）
     *
     * @param txtViewpager ：标题集合数据
     */
    public void setTitleseData(List<String> txtViewpager) {
        setBannerTitles(txtViewpager);
    }

    class GlideImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Log.e(TAG, "path === " + path);
            if (Util.isOnMainThread()) {
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                Glide.with(mContext).load((String) path).placeholder(R.drawable.home_focal_placeholder).error(R.drawable.home_focal_placeholder).into(imageView);
            }

        }
    }



}
