package com.module.base.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import com.module.home.view.LoadingProgress;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.util.Utils;

import java.io.UnsupportedEncodingException;

import butterknife.ButterKnife;

/**
 * 父类的activity
 * Created by 裴成浩 on 2018/3/30.
 */

@SuppressLint("Registered")
public abstract class YMBaseActivity extends AppCompatActivity implements View.OnClickListener {
    protected static final String TAG = YMBaseActivity.class.getSimpleName();
    protected YMBaseActivity mContext;
    protected LayoutInflater mInflater;//初始化获取布局类
    protected LoadingProgress mDialog;  //旋转等待
    protected int statusbarHeight;    //状态栏高度
    protected int windowsWight;     //屏幕宽度
    protected int windowsHeight;    //屏幕高度
    protected FunctionManager mFunctionManager;     //方法管理器
    private boolean isBackground = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutId());
        mContext = YMBaseActivity.this;
        ButterKnife.bind(mContext);
        //将当前的activity添加到ActivityManager中
        ActivityManager.getInstance().add(mContext);

        mInflater = LayoutInflater.from(mContext);
        mDialog = new LoadingProgress(mContext);

        //沉浸式布局
        QMUIStatusBarHelper.translucent(mContext);
        //状态栏字体颜色
        QMUIStatusBarHelper.setStatusBarLightMode(mContext);
        //状态栏高度
        statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);

        //获取屏幕高宽
        int[] ints = Utils.getScreenSize(mContext);
        windowsWight = ints[0];
        windowsHeight = ints[1];

        //获取方法管理器
        mFunctionManager = new FunctionManager(mContext);

        initTitle();
        initView();
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isBackground) {
            Utils.getInfo(mContext);
            isBackground = false;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isBackground = Utils.isBackground(mContext);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 提供加载的布局的方法
     *
     * @return ：布局引用
     */
    protected abstract int getLayoutId();

    /**
     * 初始化UI
     */
    protected abstract void initView();

    /**
     * 初始化内容数据
     */
    protected abstract void initData();

    /**
     * 初始化title
     */
    protected void initTitle() {
    }

    /**
     * 销毁当前的activity
     */
    public void removeCurrentActivity() {
        ActivityManager.getInstance().removeCurrent();
    }

    /**
     * 销毁所有的Activity
     */
    public void removeAll() {
        ActivityManager.getInstance().removeAll();
    }

    /**
     * 设置多点击监听器
     *
     * @param views
     */
    protected void setMultiOnClickListener(View... views) {
        for (View view : views) {
            view.setOnClickListener(mContext);
        }
    }

    /**
     * 设置布局的fragment
     */
    protected void setActivityFragment(int id, Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(id, fragment);
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
    }

}
