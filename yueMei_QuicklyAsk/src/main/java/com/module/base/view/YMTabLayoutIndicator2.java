package com.module.base.view;

import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xutils.common.util.DensityUtil;

import java.lang.reflect.Field;

/**
 * 自定义item设置TabLayout指示器长度设置
 * Created by 裴成浩 on 2019/3/5
 */
public class YMTabLayoutIndicator2 {
    private String TAG = "YMTabLayoutIndicator";

    public static YMTabLayoutIndicator2 newInstance() {
        return new YMTabLayoutIndicator2();
    }

    public void reflex(final TabLayout tabLayout, final int leftMargin, final int rightMargin) {
        //了解源码得知 线的宽度是根据 tabView的宽度来设置的
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    //拿到tabLayout的mTabStrip属性
                    LinearLayout mTabStrip = (LinearLayout) tabLayout.getChildAt(0);

                    for (int i = 0; i < mTabStrip.getChildCount(); i++) {
                        View tabView = mTabStrip.getChildAt(i);

                        //在API28下 调用：tabLayout.getDeclaredField("mTextView");
                        //在API28上 调用：tabLayout.getDeclaredField("textView");
//                        Field mTextViewField;
//                        Log.e(TAG, "Build.VERSION.SDK_INT == " + Build.VERSION.SDK_INT);
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//                            mTextViewField = tabView.getClass().getDeclaredField("textView");
//                        } else {
//                            mTextViewField = tabView.getClass().getDeclaredField("mTextView");
//                        }
                        Field mTextViewField = tabView.getClass().getDeclaredField("textView");
                        mTextViewField.setAccessible(true);

                        TextView mTextView = (TextView) mTextViewField.get(tabView);

                        tabView.setPadding(0, 0, 0, 0);

                        //因为我想要的效果是   字多宽线就多宽，所以测量mTextView的宽度
                        int width = 0;
                        width = mTextView.getWidth();
                        if (width == 0) {
                            mTextView.measure(0, 0);
                            width = mTextView.getMeasuredWidth();
                        }

                        //设置tab左右间距为10dp  注意这里不能使用Padding 因为源码中线的宽度是根据 tabView的宽度来设置的
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tabView.getLayoutParams();
//                        params.width = width+ DensityUtil.dip2px(18);
                        params.width = DensityUtil.dip2px(65);
                        params.leftMargin = leftMargin;
                        params.rightMargin = rightMargin;
                        tabView.setLayoutParams(params);

                        tabView.invalidate();
                    }

                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
