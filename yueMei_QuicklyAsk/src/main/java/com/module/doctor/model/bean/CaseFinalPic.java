package com.module.doctor.model.bean;

import java.io.Serializable;

/**
 * 案例最终页图片
 * 
 * @author Rubin
 * 
 */
public class CaseFinalPic implements Serializable{
	private String img;

	public CaseFinalPic(String img) {
		this.img = img;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "CaseFinalPic [img=" + img + "]";
	}

}
