package com.module.doctor.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.ProjcetData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 项目筛选接口
 * Created by Administrator on 2017/10/26.
 */

public class FilterDataApi implements BaseCallBackApi {
    private String TAG = "FilterDataApi";
    private HashMap<String, Object> mHashMap;

    public FilterDataApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.BOARD, "getScreen", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData === " + mData.data);
                if ("1".equals(mData.code)) {
                    ArrayList<ProjcetData> projcetSXData = null;
                    try {
                        projcetSXData = JSONUtil.jsonToArrayList(mData.data, ProjcetData.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "e === " + e.toString());
                    }
                    listener.onSuccess(projcetSXData);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
