package com.module.doctor.model.bean;

import java.util.HashMap;

public class CityDocDataitem {
	private String _id;
	private String name;
	private String ename;
	private String province_id;
	private HashMap<String,String> event_params;

	public HashMap<String, String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String, String> event_params) {
		this.event_params = event_params;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getProvince_id() {
		return province_id;
	}

	public void setProvince_id(String province_id) {
		this.province_id = province_id;
	}


}
