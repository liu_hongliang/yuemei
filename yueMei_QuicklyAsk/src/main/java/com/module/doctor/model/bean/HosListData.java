/**
 * 
 */
package com.module.doctor.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.LocationData;
import com.module.taodetail.model.bean.SkuLabelLevel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class HosListData implements Parcelable {

	private String bmsid;
	private String url;
	private String hos_id;
	private String hos_name;
	private String address;
	private String kind;
	private String distance;
	private String comment_score;
	private String comment_bili;
	private String comment_people;
	private String look;
	private String img;
	private List<DocListDataTaoList> tao;
	private String tagname;
	private String totalAppoint;
	private String tagDiaryTotal;
	private String diaryTotal;				//	日记数
	private String hospitalDiaryTotal;		//医院日记总数
	private SkuLabelLevel hospital_top;     //置顶
	private LocationData location;
	private String type;
	private String hospital_promotion_icon;
	private HashMap<String,String> event_params;

	protected HosListData(Parcel in) {
		bmsid = in.readString();
		url = in.readString();
		hos_id = in.readString();
		hos_name = in.readString();
		address = in.readString();
		kind = in.readString();
		distance = in.readString();
		comment_score = in.readString();
		comment_bili = in.readString();
		comment_people = in.readString();
		look = in.readString();
		img = in.readString();
		tao = in.createTypedArrayList(DocListDataTaoList.CREATOR);
		tagname = in.readString();
		totalAppoint = in.readString();
		tagDiaryTotal = in.readString();
		diaryTotal = in.readString();
		hospitalDiaryTotal = in.readString();
		type = in.readString();
		hospital_promotion_icon = in.readString();
		location = in.readParcelable(LocationData.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(bmsid);
		dest.writeString(url);
		dest.writeString(hos_id);
		dest.writeString(hos_name);
		dest.writeString(address);
		dest.writeString(kind);
		dest.writeString(distance);
		dest.writeString(comment_score);
		dest.writeString(comment_bili);
		dest.writeString(comment_people);
		dest.writeString(look);
		dest.writeString(img);
		dest.writeTypedList(tao);
		dest.writeString(tagname);
		dest.writeString(totalAppoint);
		dest.writeString(tagDiaryTotal);
		dest.writeString(diaryTotal);
		dest.writeString(hospitalDiaryTotal);
		dest.writeString(type);
		dest.writeString(hospital_promotion_icon);
		dest.writeParcelable(location, flags);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<HosListData> CREATOR = new Creator<HosListData>() {
		@Override
		public HosListData createFromParcel(Parcel in) {
			return new HosListData(in);
		}

		@Override
		public HosListData[] newArray(int size) {
			return new HosListData[size];
		}
	};

	public String getHospital_promotion_icon() {
		return hospital_promotion_icon;
	}

	public void setHospital_promotion_icon(String hospital_promotion_icon) {
		this.hospital_promotion_icon = hospital_promotion_icon;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocationData getLocation() {
		return location;
	}

	public void setLocation(LocationData location) {
		this.location = location;
	}

	public String getBmsid() {
		return bmsid;
	}

	public void setBmsid(String bmsid) {
		this.bmsid = bmsid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHos_id() {
		return hos_id;
	}

	public void setHos_id(String hos_id) {
		this.hos_id = hos_id;
	}

	public String getHos_name() {
		return hos_name;
	}

	public void setHos_name(String hos_name) {
		this.hos_name = hos_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getComment_score() {
		return comment_score;
	}

	public void setComment_score(String comment_score) {
		this.comment_score = comment_score;
	}

	public String getComment_bili() {
		return comment_bili;
	}

	public void setComment_bili(String comment_bili) {
		this.comment_bili = comment_bili;
	}

	public String getComment_people() {
		return comment_people;
	}

	public void setComment_people(String comment_people) {
		this.comment_people = comment_people;
	}

	public String getLook() {
		return look;
	}

	public void setLook(String look) {
		this.look = look;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public List<DocListDataTaoList> getTao() {
		return tao;
	}

	public void setTao(List<DocListDataTaoList> tao) {
		this.tao = tao;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getTotalAppoint() {
		return totalAppoint;
	}

	public void setTotalAppoint(String totalAppoint) {
		this.totalAppoint = totalAppoint;
	}

	public String getTagDiaryTotal() {
		return tagDiaryTotal;
	}

	public void setTagDiaryTotal(String tagDiaryTotal) {
		this.tagDiaryTotal = tagDiaryTotal;
	}

	public String getDiaryTotal() {
		return diaryTotal;
	}

	public void setDiaryTotal(String diaryTotal) {
		this.diaryTotal = diaryTotal;
	}

	public String getHospitalDiaryTotal() {
		return hospitalDiaryTotal;
	}

	public void setHospitalDiaryTotal(String hospitalDiaryTotal) {
		this.hospitalDiaryTotal = hospitalDiaryTotal;
	}

	public SkuLabelLevel getHospital_top() {
		return hospital_top;
	}

	public void setHospital_top(SkuLabelLevel hospital_top) {
		this.hospital_top = hospital_top;
	}

	public HashMap<String, String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String, String> event_params) {
		this.event_params = event_params;
	}
}
