package com.module.doctor.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.LocationData;

import java.util.HashMap;
import java.util.List;

public class DocListData implements Parcelable {
	private String bmsid;
	private String url;
	private String user_id;
	private String img;
	private String range;
	private String username;
	private String title;
	private String hspital_name;
	private String hospital_id;
	private String cooperation;
	private String pei;
	private String look;
	private List<DocListDataTaoList> tao;
	private String talent;
	private String comment_score;
	private String comment_bili;
	private String sku_order_num;
	private String distance;
	private String tagname;
	private String totalAppoint;
	private String tagDiaryTotal;
	private String diaryTotal;				//	日记数
	private HospitalTop hospital_top;
	private HashMap<String,String> event_params;
	private LocationData location;
	private String hospital_effect;//评分

	public String getHospital_effect() {
		return hospital_effect;
	}

	public void setHospital_effect(String hospital_effect) {
		this.hospital_effect = hospital_effect;
	}

	public String getHospital_id() {
		return hospital_id;
	}

	public void setHospital_id(String hospital_id) {
		this.hospital_id = hospital_id;
	}

	public LocationData getLocation() {
		return location;
	}

	public void setLocation(LocationData location) {
		this.location = location;
	}

	protected DocListData(Parcel in) {
		bmsid = in.readString();
		url = in.readString();
		user_id = in.readString();
		img = in.readString();
		range = in.readString();
		username = in.readString();
		title = in.readString();
		hspital_name = in.readString();
		hospital_id = in.readString();
		cooperation = in.readString();
		pei = in.readString();
		look = in.readString();
		tao = in.createTypedArrayList(DocListDataTaoList.CREATOR);
		talent = in.readString();
		comment_score = in.readString();
		comment_bili = in.readString();
		sku_order_num = in.readString();
		distance = in.readString();
		tagname = in.readString();
		totalAppoint = in.readString();
		tagDiaryTotal = in.readString();
		diaryTotal = in.readString();
		hospital_effect = in.readString();
		hospital_top = in.readParcelable(HospitalTop.class.getClassLoader());
		location = in.readParcelable(LocationData.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(bmsid);
		dest.writeString(url);
		dest.writeString(user_id);
		dest.writeString(img);
		dest.writeString(range);
		dest.writeString(username);
		dest.writeString(title);
		dest.writeString(hspital_name);
		dest.writeString(hospital_id);
		dest.writeString(cooperation);
		dest.writeString(pei);
		dest.writeString(look);
		dest.writeTypedList(tao);
		dest.writeString(talent);
		dest.writeString(comment_score);
		dest.writeString(comment_bili);
		dest.writeString(sku_order_num);
		dest.writeString(distance);
		dest.writeString(tagname);
		dest.writeString(totalAppoint);
		dest.writeString(tagDiaryTotal);
		dest.writeString(diaryTotal);
		dest.writeString(hospital_effect);
		dest.writeParcelable(location, flags);
		dest.writeParcelable((Parcelable) hospital_top, flags);

	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<DocListData> CREATOR = new Creator<DocListData>() {
		@Override
		public DocListData createFromParcel(Parcel in) {
			return new DocListData(in);
		}

		@Override
		public DocListData[] newArray(int size) {
			return new DocListData[size];
		}
	};

	public String getBmsid() {
		return bmsid;
	}

	public void setBmsid(String bmsid) {
		this.bmsid = bmsid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHspital_name() {
		return hspital_name;
	}

	public void setHspital_name(String hspital_name) {
		this.hspital_name = hspital_name;
	}

	public String getCooperation() {
		return cooperation;
	}

	public void setCooperation(String cooperation) {
		this.cooperation = cooperation;
	}

	public String getPei() {
		return pei;
	}

	public void setPei(String pei) {
		this.pei = pei;
	}

	public String getLook() {
		return look;
	}

	public void setLook(String look) {
		this.look = look;
	}

	public List<DocListDataTaoList> getTao() {
		return tao;
	}

	public void setTao(List<DocListDataTaoList> tao) {
		this.tao = tao;
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}

	public String getComment_score() {
		return comment_score;
	}

	public void setComment_score(String comment_score) {
		this.comment_score = comment_score;
	}

	public String getComment_bili() {
		return comment_bili;
	}

	public void setComment_bili(String comment_bili) {
		this.comment_bili = comment_bili;
	}

	public String getSku_order_num() {
		return sku_order_num;
	}

	public void setSku_order_num(String sku_order_num) {
		this.sku_order_num = sku_order_num;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getTagname() {
		return tagname;
	}

	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	public String getTotalAppoint() {
		return totalAppoint;
	}

	public void setTotalAppoint(String totalAppoint) {
		this.totalAppoint = totalAppoint;
	}

	public String getTagDiaryTotal() {
		return tagDiaryTotal;
	}

	public void setTagDiaryTotal(String tagDiaryTotal) {
		this.tagDiaryTotal = tagDiaryTotal;
	}

	public String getDiaryTotal() {
		return diaryTotal;
	}

	public void setDiaryTotal(String diaryTotal) {
		this.diaryTotal = diaryTotal;
	}

	public HashMap<String, String> getEvent_params() {
		return event_params;
	}

	public void setEvent_params(HashMap<String, String> event_params) {
		this.event_params = event_params;
	}

	public HospitalTop getHospital_top() {
		return hospital_top;
	}

	public void setHospital_top(HospitalTop hospital_top) {
		this.hospital_top = hospital_top;
	}
}
