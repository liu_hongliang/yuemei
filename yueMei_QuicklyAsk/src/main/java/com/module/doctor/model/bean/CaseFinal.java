package com.module.doctor.model.bean;

/**
 * 案例最终页 数据
 * 
 * @author Rubin
 * 
 */
public class CaseFinal {

	private String code;
	private String message;
	private CaseFinalData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CaseFinalData getData() {
		return data;
	}

	public void setData(CaseFinalData data) {
		this.data = data;
	}


}
