/**
 *
 */
package com.module.doctor.model.bean;


import org.kymjs.aframe.database.annotate.Id;

/**
 * @author lenovo17
 *
 */
public class MainCityData {

	@Id
	private int id;
	private String _id;
	private String name;
	private String ename;
	private String provinceId;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id
	 *            the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the ename
	 */
	public String getEname() {
		return ename;
	}

	/**
	 * @param ename
	 *            the ename to set
	 */
	public void setEname(String ename) {
		this.ename = ename;
	}

	/**
	 * @return the provinceId
	 */
	public String getProvinceId() {
		return provinceId;
	}

	/**
	 * @param provinceId
	 *            the provinceId to set
	 */
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	@Override
	public String toString() {
		return "MainCityData{" +
				"id=" + id +
				", _id='" + _id + '\'' +
				", name='" + name + '\'' +
				", ename='" + ename + '\'' +
				", provinceId='" + provinceId + '\'' +
				'}';
	}
}
