package com.module.doctor.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class HospitalTop implements Parcelable {
    private String level;
    private String desc;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.level);
        dest.writeString(this.desc);
    }

    public HospitalTop() {
    }

    protected HospitalTop(Parcel in) {
        this.level = in.readString();
        this.desc = in.readString();
    }

    public static final Parcelable.Creator<HospitalTop> CREATOR = new Parcelable.Creator<HospitalTop>() {
        @Override
        public HospitalTop createFromParcel(Parcel source) {
            return new HospitalTop(source);
        }

        @Override
        public HospitalTop[] newArray(int size) {
            return new HospitalTop[size];
        }
    };
}
