package com.module.doctor.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.HosYuYueData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by Administrator on 2017/10/25.
 */

public class HosYuYueApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOSPITAL, "cooperation", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.d("HosDetailActivity","HosYuYueApi:=="+mData.toString());
                if ("1".equals(mData.code)){
                    try {
                        HosYuYueData hosYuYueData = JSONUtil.TransformSingleBean(mData.data, HosYuYueData.class);
                        listener.onSuccess(hosYuYueData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
