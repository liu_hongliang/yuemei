package com.module.doctor.model.bean;

import com.module.community.model.bean.ZhuanTi;

import java.util.List;

public class DocTalkZT {

	private String code;
	private String message;
	private List<ZhuanTi> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ZhuanTi> getData() {
		return data;
	}

	public void setData(List<ZhuanTi> data) {
		this.data = data;
	}

}
