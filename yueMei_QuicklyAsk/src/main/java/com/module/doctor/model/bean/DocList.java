package com.module.doctor.model.bean;

import java.util.List;

public class DocList {

	private String code;
	private String message;
	private List<DocListData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<DocListData> getData() {
		return data;
	}

	public void setData(List<DocListData> data) {
		this.data = data;
	}

}
