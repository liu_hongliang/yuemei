package com.module.doctor.model.bean;

/**
 * 讨论组一级列表
 * 
 * @author Rubin
 * 
 */
public class GroupDiscData {

	private String _id;
	private String cate_name;
	private String t_num;
	private String pic_url;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getCate_name() {
		return cate_name;
	}

	public void setCate_name(String cate_name) {
		this.cate_name = cate_name;
	}

	public String getT_num() {
		return t_num;
	}

	public void setT_num(String t_num) {
		this.t_num = t_num;
	}

	public String getPic_url() {
		return pic_url;
	}

	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}

}
