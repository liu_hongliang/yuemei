package com.module.doctor.model.bean;

import java.util.List;

/**
 * 案例最终页数据
 * 
 * @author Rubin
 * 
 */
public class CaseFinalData {

	private String cooperation;
	private String sex;
	private String age;
	private String fee;
	private String dname;
	private String doctor_id;
	private String url;

	private List<CaseFinalPic> pic;

	public String getCooperation() {
		return cooperation;
	}

	public void setCooperation(String cooperation) {
		this.cooperation = cooperation;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(String doctor_id) {
		this.doctor_id = doctor_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<CaseFinalPic> getPic() {
		return pic;
	}

	public void setPic(List<CaseFinalPic> pic) {
		this.pic = pic;
	}

}
