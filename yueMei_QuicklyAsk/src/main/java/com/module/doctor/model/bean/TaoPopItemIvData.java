package com.module.doctor.model.bean;

public class TaoPopItemIvData {

	private String _id;
	private String name;
	private String img;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "TaoPopItemIvData{" +
				"_id='" + _id + '\'' +
				", name='" + name + '\'' +
				", img='" + img + '\'' +
				'}';
	}
}
