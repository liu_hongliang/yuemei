/**
 *
 */
package com.module.doctor.controller.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.bean.ProjectDetailsListData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;


/**
 * 医生与医院列表
 *
 * @author Robin
 */
@SuppressWarnings("deprecation")
public class TabDocAndHosListActivity extends TabActivity {

    private final String TAG = "TabDocAndHosList";

    public TabHost tabHost;
    TabSpec tab1, tab2;

    private int mCurTab = 0;

    public RadioButton bnBottom[] = new RadioButton[2];
    private TabDocAndHosListActivity mContext;
    public View docLine;
    public View hosLine;

    private RelativeLayout searchRly;

    private String city = "全国";

    private RelativeLayout mBack;
    private ProjectDetailsListData screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.tab_acty_doc_hos);
        mContext = TabDocAndHosListActivity.this;

        tabHost = findViewById(android.R.id.tabhost);

        mBack = findViewById(R.id.doc_back_search_rly);
        searchRly = findViewById(R.id.doc_list_search_rly);

        docLine = findViewById(R.id.doc_view_line1);
        hosLine = findViewById(R.id.hos_view_line2);


        tab1 = tabHost.newTabSpec("tab1");
        tab2 = tabHost.newTabSpec("tab2");

        String action = getIntent().getAction();
        //判断是自己app跳转的，还是其他app跳转的
        String type = "";
        if (!TextUtils.isEmpty(action) && Intent.ACTION_VIEW.equals(action)) {
            Uri uri = getIntent().getData();
            //获取指定参数值
            if (uri != null) {
                type = uri.getQueryParameter("type");
            }
        } else {
            type = getIntent().getStringExtra("type");
            screen = getIntent().getParcelableExtra("screen");
        }

        Intent it1 = new Intent(mContext, TabDocListFragActivity.class);
        if (screen != null) {
            it1.putExtra("screen",screen);
        }
        tab1.setIndicator("1").setContent(it1);


        Intent it2 = new Intent(mContext, TabHosListFragActivity.class);
        if (screen != null) {
            it2.putExtra("screen",screen);
        }
        tab2.setIndicator("2").setContent(it2);

        Log.e(TAG, "tabHost == " + tabHost);
        Log.e(TAG, "tab1 == " + tab1);
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);

        bnBottom[0] = findViewById(R.id.tab_doc_select_rbt);
        bnBottom[1] = findViewById(R.id.tab_hos_select_rbt);


        Log.e(TAG, "type = " + type);
        if ("1".equals(type)) {
            docLine.setVisibility(View.GONE);
            hosLine.setVisibility(View.VISIBLE);
            bnBottom[0].setChecked(false);
            bnBottom[1].setChecked(true);
            tabHost.setCurrentTab(1);
            mCurTab = 1;
        } else {
            docLine.setVisibility(View.VISIBLE);
            hosLine.setVisibility(View.GONE);
            bnBottom[0].setChecked(true);
            bnBottom[1].setChecked(false);
            tabHost.setCurrentTab(0);
            mCurTab = 0;
        }

        for (int i = 0; i < 2; i++) {
            final int j = i;
            bnBottom[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    tabHost.setCurrentTab(j);
                    mCurTab = j;

                    for (int m = 0; m < 2; m++) {
                        if (m == mCurTab) {
                            bnBottom[m].setChecked(true);
                        } else {
                            bnBottom[m].setChecked(false);
                        }
                    }

                    if (0 == mCurTab) {
                        docLine.setVisibility(View.VISIBLE);
                        hosLine.setVisibility(View.GONE);
                    } else {
                        docLine.setVisibility(View.GONE);
                        hosLine.setVisibility(View.VISIBLE);
                    }

                }
            });
        }

        mBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent it = new Intent();
                it.setClass(mContext, SearchAllActivity668.class);
                if (mCurTab == 0) {
                    it.putExtra("position", 3);
                } else {
                    it.putExtra("position", 4);
                }
                startActivity(it);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        city = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");

        if (city.length() > 0) {
            if (city.equals("失败")) {
                city = "全国";
            } else {
            }
        } else {
            city = "全国";
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case 4:
                if (data != null) {
                    String cityStr = data.getStringExtra("city");

                    if (cityStr.equals("失败")) {
                        cityStr = "全国";
                    }
                    city = cityStr;
                }
                break;

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "mCurTab === " + mCurTab);
        bnBottom[mCurTab].performClick();
        bnBottom[mCurTab].setChecked(true);
        if (mCurTab == 0) {
            docLine.setVisibility(View.VISIBLE);
            hosLine.setVisibility(View.GONE);
        } else {
            docLine.setVisibility(View.GONE);
            hosLine.setVisibility(View.VISIBLE);
        }
    }

}
