/**
 * 
 */
package com.module.doctor.controller.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.doctor.model.bean.CityDocDataitem;
import com.quicklyask.activity.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 城市搜索
 * 
 * @author Robin
 * 
 */
public class CitySearchAdapter extends BaseAdapter {

	private final String TAG = "CitySearchAdapter";

	private List<CityDocDataitem> mGroupData = new ArrayList<CityDocDataitem>();
	private Context mContext;
	private LayoutInflater inflater;
	private CityDocDataitem groupData;
	ViewHolder viewHolder;

	public CitySearchAdapter(Context mContext, List<CityDocDataitem> mGroupData) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView groupNameTV;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.adapter_chat, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.name);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		groupData = mGroupData.get(position);

		viewHolder.groupNameTV.setText(groupData.getName());
		return convertView;
	}

	public void add(List<CityDocDataitem> infos) {
		mGroupData.addAll(infos);
	}
}
