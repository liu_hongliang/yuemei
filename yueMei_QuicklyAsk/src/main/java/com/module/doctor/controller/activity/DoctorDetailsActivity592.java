/**
 *
 */
package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.AutoSendApi;
import com.module.commonview.module.api.AutoSendApi2;
import com.module.commonview.module.api.BaikeFourApi;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.IsCollectApi;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.commonview.module.api.SendEMSApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.commonview.module.bean.IsFocuData;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.doctor.controller.adapter.DiaryListAdapter;
import com.module.doctor.controller.adapter.DocDiaryListAdapter;
import com.module.doctor.controller.adapter.TeyaoFabiaoAdapter;
import com.module.doctor.controller.adapter.TeyaoTaoAdapter;
import com.module.doctor.controller.api.DocDeBBsListApi;
import com.module.doctor.controller.other.DoctorDetailsWebViewClien;
import com.module.doctor.model.api.DoctorHeadMessageApi;
import com.module.doctor.model.api.RiJiListApi;
import com.module.doctor.model.api.ShareDataApi;
import com.module.doctor.model.api.TaoListDataApi;
import com.module.doctor.model.bean.DocHeadData;
import com.module.doctor.model.bean.DocShareData;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.my.controller.activity.TypeProblemActivity;
import com.module.my.controller.activity.YuYueDocActivity;
import com.module.my.model.bean.Group;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.ScrollViewScrolCallBack;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.TongJiParams;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyScrollView;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.NoScrollListView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.json.JSONObject;
import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 医生详情页
 *
 * @author Robin
 */
public class DoctorDetailsActivity592 extends BaseActivity {

    private String TAG = "DoctorDetails";
    private DoctorDetailsActivity592 mContext;

    private RelativeLayout back;// 返回键
    private TextView titleBarTV1;// 没有头像时的title
    private TextView titleBarTV2;// 有头像时的title
    private ImageView titleIv;// 标题栏的头像
    private RelativeLayout shareRly;// 分享按钮
    private RelativeLayout collectRly;// 收藏按钮
    private ImageView collectIv;// 收藏图标

    // 普通医生的界面数据
    @BindView(id = R.id.putong_doc_content_ly)
    private LinearLayout putongDocLy;// 普通医生的界面
    @BindView(id = R.id.tao_detail_bottom_content_rly)
    private RelativeLayout putongBtom;// 普通医生吸底
    private ImageView fnegmianIv;// 封面
    private ImageView docHeadIv;// 医生头像
    private TextView docNameTv;// 医生名字
    private TextView hosTv;// 医院名称
    private ImageView guanzhuIv;// 关注度
    private ImageView hotxinIv;// 热心度

    @BindView(id = R.id.doc_fans1)
    private LinearLayout docFans1;// 普通医生关注
    @BindView(id = R.id.doc_imgage_fans1)
    private ImageView imgageFans1;
    @BindView(id = R.id.doc_center_fans1)
    private TextView centerFans1;

    ImageOptions imageOptions;
    ImageOptions imageOptions1;

    @BindView(id = R.id.doc_detail_to_question_bt)
    private Button toQuestionBt;// 预约面诊 toYuyueBt
    @BindView(id = R.id.doc_detail_to_yuyue_bt)
    private Button tiYuyueBt;// 咨询问题
    private String hosid;
    private String po;
    private String isyuyue;

    @BindView(id = R.id.tao_detail_scroll1)
    private MyScrollView myscroll;
    // 头部信息
    private DocHeadData dhdata;
    private String ifsixin;
    private String phoneTel;
    private String is_rongyun;// 是否是融云
    private String hos_userid;// 医院的单聊用户id

    TextView descriptionView;
    View expandView;
    int maxDescripLine = 3;
    @BindView(id = R.id.doc_nodata_ly)
    private LinearLayout docNodescLy;
    @BindView(id = R.id.description_layout)
    private LinearLayout docDescLy;

    // 淘整形的列表
    @BindView(id = R.id.taolist_content_ly)
    private LinearLayout taolistLy;
    @BindView(id = R.id.listView1)
    private NoScrollListView taoList;
    @BindView(id = R.id.tao_list_more_ly)
    private LinearLayout moretaolistLy;
    @BindView(id = R.id.taolist_more_tv)
    private TextView moretaoTv;
    private List<HomeTaoData> lvHomeTaoData = new ArrayList<>();
    private List<HomeTaoData> lvHomeTaoData1 = new ArrayList<>();
    private TaoAdpter623 homeTaoAdpter;

    // 日记的列表
    @BindView(id = R.id.rijilist_content_ly)
    private LinearLayout rijilistLy;
    @BindView(id = R.id.listView2)
    private NoScrollListView rijiList;
    @BindView(id = R.id.riji_list_more_ly)
    private LinearLayout morerijilistLy;
    @BindView(id = R.id.rijilist_more_tv)
    private TextView morerijiTv;
    private List<BBsListData550> lvrijiBBslistData = new ArrayList<>();
    private List<BBsListData550> lvrijiBBslistData1 = new ArrayList<>();
    private DiaryListAdapter bbsrijiListAdapter;

    // 帖子的列表
    @BindView(id = R.id.bbslist_content_ly)
    private LinearLayout bbslistLy;
    @BindView(id = R.id.listView3)
    private NoScrollListView bbsList;
    @BindView(id = R.id.bbs_list_more_ly)
    private LinearLayout morebbslistLy;
    @BindView(id = R.id.bbslist_more_tv)
    private TextView morebbsTv;
    private List<BBsListData550> lvBBslistData = new ArrayList<>();
    private List<BBsListData550> lvBBslistData1 = new ArrayList<>();
    private BBsListAdapter bbsListAdapter;

    private String docId;
    private String docName;
    private String partId;
    private String uid;
    private String isAutoSend = "";//是否自动发送机构信息
    private boolean ifcollect;

    @BindView(id = R.id.titlebar_name_iv_ly)
    private LinearLayout titlenameivLy;

    // 要分享的图片
    private DocShareData mDocShareData = new DocShareData();
    private String shareUrl = "";
    private String shareImgUrl = "";
    private String shareContent = "";

    private KJDB kjdb;
    private List<Group> groupList;

    // 特邀医生的数据
    @BindView(id = R.id.teyao_doc_bottom_content_rly)
    private RelativeLayout teyaoBtom;// 特邀医生吸底
    @BindView(id = R.id.slide_pic_linearlayout, click = true)
    private LinearLayout contentWeb;
    private WebView bbsDetWeb;
    public JSONObject obj_http;

    @BindView(id = R.id.doc_detail_to_question_bt1)
    private Button toSixinBt;// 私信
    @BindView(id = R.id.doc_detail_to_yuyue_bt1)
    private Button toYuyueBt;// 预约
    @BindView(id = R.id.tao_bottom_zixun_rly)
    private RelativeLayout phoneZixun;

    private String phoneStr = "";

    private PopupWindows zixunPop;

    @BindView(id = R.id.content_all_ly)
    private LinearLayout allcontent;

    private int windowsWight;

    //特邀医生新界面
    @BindView(id = R.id.tao_detail_scroll_top)
    private MyScrollView scroll1;

    @BindView(id = R.id.teyao_doc_content_ly)
    private LinearLayout teyaoContentLy;//特邀医生界面
    @BindView(id = R.id.doc_detail_docname_tv)
    private TextView teyaoDocnameTv;
    @BindView(id = R.id.doc_detail_subname_tv)
    private TextView teyaoDocSubNameTv;
    @BindView(id = R.id.doc_detail_hosname_tv)
    private TextView teyaoHosNameTv;
    @BindView(id = R.id.doc_detail_docba_iv)
    private ImageView teyaoBigDocIv;
    @BindView(id = R.id.teyao_doc_ardent_tv)
    private TextView teyaoArdentTv;
    @BindView(id = R.id.teyao_doc_attention_tv)
    private TextView teyaoAttentionTv;
    @BindView(id = R.id.teyao_doc_persign_ly)
    private LinearLayout teyaoDocDescLy;
    @BindView(id = R.id.teyao_doc_persign_tv)
    private TextView teyaoDocDescTv;
    @BindView(id = R.id.qiumeituijian_ly)
    private LinearLayout teyaoAttentionCly;

    @BindView(id = R.id.doc_teyao_head_jianbian_rly)
    private RelativeLayout teyaoHeadJianbianRly;

    @BindView(id = R.id.doc_fans2)
    private LinearLayout docFans2;// 特邀医生关注
    @BindView(id = R.id.doc_imgage_fans2)
    private ImageView imgageFans2;
    @BindView(id = R.id.doc_center_fans2)
    private TextView centerFans2;

    private List<BBsListData550> lvBBslistTeyaoFabiaoData = new ArrayList<>();
    private List<BBsListData550> lvBBslistTeyaoFabiaoData1 = new ArrayList<>();
    private TeyaoFabiaoAdapter teyaoFabiaoAdapter;
    @BindView(id = R.id.listView_docfabiao)
    private NoScrollListView teyaoFabiaolist;
    @BindView(id = R.id.docfabiaolist_content_ly)
    private LinearLayout teyaoFabiaoLLy;
    @BindView(id = R.id.teyao_fabiao_more_rly)
    private RelativeLayout teyaoMoreFabiaoRly;
    @BindView(id = R.id.teyao_fabiao_more_tv)
    private TextView teyaoMoreFabiaoTv;
    @BindView(id = R.id.zanwu_fabiao_tv)
    private TextView zanwuFabiaoTv;


    private List<BBsListData550> lvBBslistTeyaoPingjiaData = new ArrayList<>();
    private List<BBsListData550> lvBBslistTeyaoPingjiaData1 = new ArrayList<>();
    private DocDiaryListAdapter bbsListShareAdapter;
    @BindView(id = R.id.listView_sharelit)
    private NoScrollListView teyaoShaerlist;
    @BindView(id = R.id.docsharelist_content_ly)
    private LinearLayout teyaoPingjiaLLy;
    @BindView(id = R.id.teyao_share_more_rly)
    private RelativeLayout teyaoMoreShareRly;
    @BindView(id = R.id.teyao_share_more_tv)
    private TextView teyaoMoreShareTv;
    @BindView(id = R.id.zanwu_pingjia_tv)
    private TextView zanwuShareTv;


    private List<HomeTaoData> lvteyaoTaoData = new ArrayList<>();
    private TeyaoTaoAdapter teyaoTaoAdapter;
    @BindView(id = R.id.listView_leitao)
    private NoScrollListView teyaoTaolist;
    @BindView(id = R.id.docleitao_content_ly)
    private LinearLayout teyaoTaoLLy;
    @BindView(id = R.id.tao_detail_leitaolit_ly_zanwu)
    private LinearLayout zanwuTaoTv;

    private int pointYs[];

    @BindView(id = R.id.tab1_rlly)
    private RelativeLayout tabRlly1;
    @BindView(id = R.id.tab2_rlly)
    private RelativeLayout tabRlly2;
    @BindView(id = R.id.tab3_rlly)
    private RelativeLayout tabRlly3;
    @BindView(id = R.id.tab4_rlly)
    private RelativeLayout tabRlly4;
    @BindView(id = R.id.tab_content_lly)
    private LinearLayout tabContent;
    @BindView(id = R.id.tab1_line)
    private View tabLine1;
    @BindView(id = R.id.tab1_tv)
    private TextView tabTv1;
    @BindView(id = R.id.tab2_line)
    private View tabLine2;
    @BindView(id = R.id.tab2_tv)
    private TextView tabTv2;
    @BindView(id = R.id.tab3_line)
    private View tabLine3;
    @BindView(id = R.id.tab3_tv)
    private TextView tabTv3;
    @BindView(id = R.id.tab4_line)
    private View tabLine4;
    @BindView(id = R.id.tab4_tv)
    private TextView tabTv4;

    @BindView(id = R.id.getweb_hhhh)
    private LinearLayout getwebH;

    @BindView(id = R.id.wan_beautifu_linearlayout4)
    private LinearLayout contentWeb22;
    private WebView docDetWeb22;
    private PageJumpManager pageJumpManager;
    private ShareWechat mWechat;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private DoctorDetailsWebViewClien doctorDetailsWebViewClien;
    private IsFocuData mIsFocuData;
    private FocusAndCancelData mFocusAndCancelData;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_doc_details_);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent it0 = getIntent();
        String doctorUrl = Cfg.loadStr(DoctorDetailsActivity592.this, FinalConstant.IS_WEBDOCTOR, "");
        partId = it0.getStringExtra("partId");
        docName = it0.getStringExtra("docName");
        docId = it0.getStringExtra("docId");
        isAutoSend = it0.getStringExtra("isAutoSend");

        Log.e(TAG,"doctorUrl =="+doctorUrl);
        if (!TextUtils.isEmpty(doctorUrl)){
            String url=FinalConstant.baseUrl + FinalConstant.VER +doctorUrl+"id/"+docId+"/";
            Log.e(TAG,"url =="+url);
            WebData webData = new WebData(url);
            webData.setShowTitle(false);
            webData.setThemeResid(R.style.AppThemeprice);
            WebUtil.getInstance().startWebActivity(DoctorDetailsActivity592.this, webData);
            finish();
        }
        super.onCreate(savedInstanceState);
        mContext = DoctorDetailsActivity592.this;
        uid = Utils.getUid();
        kjdb = KJDB.create(mContext, "yuemei_group");




        ExposureLoginData exposureLoginData = new ExposureLoginData("3",docId);
        Cfg.saveStr(mContext,FinalConstant.EXPOSURE_LOGIN,new Gson().toJson(exposureLoginData));

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext, "9");
        baseWebViewClientMessage.setView(allcontent);
        doctorDetailsWebViewClien = new DoctorDetailsWebViewClien(mContext, partId, docName, docId);
        baseWebViewClientMessage.setBaseWebViewClientCallback(doctorDetailsWebViewClien);

        pageJumpManager = new PageJumpManager(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;

        findViewById();
        if (Utils.isLogin()) {
            isFocu();
        }
    }

    void findViewById() {

        back = findViewById(R.id.wan_beautiful_web_back);

        titleBarTV1 = findViewById(R.id.wan_beautifu_docname);
        titleBarTV2 = findViewById(R.id.wan_beautifu_docname1);
        titleIv = findViewById(R.id.doc_list_head_image_iv11);
        shareRly = findViewById(R.id.hos_web_share_rly);
        collectRly = findViewById(R.id.doc_web_collect);
        collectIv = findViewById(R.id.doc_web_if_collect);
        fnegmianIv = findViewById(R.id.doc_fengmian_iv);
        docHeadIv = findViewById(R.id.self_head_image_iv);
        docNameTv = findViewById(R.id.doc_detail_name_tv);
        hosTv = findViewById(R.id.hos_name_tv);
        guanzhuIv = findViewById(R.id.item_baike_guanzhu_iv);
        hotxinIv = findViewById(R.id.item_baike_grexin_iv);

        descriptionView = findViewById(R.id.description_view);
        expandView = findViewById(R.id.expand_view);
        initWebview22();

        imageOptions = new ImageOptions.Builder().setCircular(true).setPlaceholderScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();

        imageOptions1 = new ImageOptions.Builder().setPlaceholderScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();


        LayoutParams params1 = teyaoBigDocIv.getLayoutParams();
        params1.height = ((windowsWight * 534) / 750);
        teyaoBigDocIv.setLayoutParams(params1);

        zixunPop = new PopupWindows(mContext, allcontent);


        initDocheadMessage();
        setListener();

        myscroll.fullScroll(ScrollView.FOCUS_UP);

        pointYs = new int[4];

        new CountDownTimer(3000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        scroll1.fullScroll(ScrollView.FOCUS_UP);

                    }
                });


            }

        }.start();

    }


    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initWebview22() {
        docDetWeb22 = new WebView(mContext);

        docDetWeb22.getSettings().setPluginState(WebSettings.PluginState.ON);
        docDetWeb22.getSettings().setAllowFileAccess(true); // 允许访问文件
        docDetWeb22.getSettings().setLoadWithOverviewMode(true);
        docDetWeb22.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            docDetWeb22.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb22.setWebViewClient(baseWebViewClientMessage);
        docDetWeb22.getSettings().setJavaScriptEnabled(true);
        docDetWeb22.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        docDetWeb22.getSettings().setUseWideViewPort(true);// 关键点
        docDetWeb22.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        docDetWeb22.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        docDetWeb22.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb22.addView(docDetWeb22);
    }


    void setTab(int curtab) {
        if (curtab == 1) {
            tabLine1.setVisibility(View.VISIBLE);
            tabLine2.setVisibility(View.GONE);
            tabLine3.setVisibility(View.GONE);
            tabLine4.setVisibility(View.GONE);

            tabTv1.setTextColor(Color.parseColor("#ff5c77"));
            tabTv2.setTextColor(Color.parseColor("#333333"));
            tabTv3.setTextColor(Color.parseColor("#333333"));
            tabTv4.setTextColor(Color.parseColor("#333333"));
        }

        if (curtab == 2) {
            tabLine1.setVisibility(View.GONE);
            tabLine2.setVisibility(View.VISIBLE);
            tabLine3.setVisibility(View.GONE);
            tabLine4.setVisibility(View.GONE);

            tabTv1.setTextColor(Color.parseColor("#333333"));
            tabTv2.setTextColor(Color.parseColor("#ff5c77"));
            tabTv3.setTextColor(Color.parseColor("#333333"));
            tabTv4.setTextColor(Color.parseColor("#333333"));
        }

        if (curtab == 3) {
            tabLine1.setVisibility(View.GONE);
            tabLine2.setVisibility(View.GONE);
            tabLine3.setVisibility(View.VISIBLE);
            tabLine4.setVisibility(View.GONE);

            tabTv1.setTextColor(Color.parseColor("#333333"));
            tabTv2.setTextColor(Color.parseColor("#333333"));
            tabTv3.setTextColor(Color.parseColor("#ff5c77"));
            tabTv4.setTextColor(Color.parseColor("#333333"));
        }

        if (curtab == 4) {
            tabLine1.setVisibility(View.GONE);
            tabLine2.setVisibility(View.GONE);
            tabLine3.setVisibility(View.GONE);
            tabLine4.setVisibility(View.VISIBLE);

            tabTv1.setTextColor(Color.parseColor("#333333"));
            tabTv2.setTextColor(Color.parseColor("#333333"));
            tabTv3.setTextColor(Color.parseColor("#333333"));
            tabTv4.setTextColor(Color.parseColor("#ff5c77"));
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", docId);
        hashMap.put("type", "1");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                mIsFocuData = isFocuData;
                switch (mIsFocuData.getFolowing()) {
                    case "0":          //未关注
                        setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                        break;
                    case "1":          //已关注
                        setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                        break;
                    case "2":          //互相关注
                        setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.each_focus, "#999999", "互相关注");
                        break;
                }

                docFans1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e(TAG, "1111111 === " + mIsFocuData.getFolowing());
                        if (Utils.isLoginAndBind(mContext)) {
                            switch (mIsFocuData.getFolowing()) {
                                case "0":          //改为已关注
                                    FocusAndCancel();
                                    setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                                    break;
                                case "1":          //改为未关注
                                case "2":          //改为未关注
                                    showDialogExitEdit();
                                    break;
                            }
                        }
                    }
                });
                docFans2.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e(TAG, "222222 === " + mIsFocuData.getFolowing());
                        if (Utils.isLogin()) {
                            if (Utils.isBind()) {
                                switch (mIsFocuData.getFolowing()) {
                                    case "0":          //改为已关注
                                        FocusAndCancel();
                                        setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");

                                        break;
                                    case "1":          //改为未关注
                                    case "2":          //改为未关注
                                        showDialogExitEdit();
                                        break;
                                }
                            } else {
                                Utils.jumpBindingPhone(mContext);

                            }
                        } else {
                            Utils.jumpLogin(mContext);
                        }

                    }
                });
            }
        });
    }

    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel();
                setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 关注和取消关注
     */
    private void FocusAndCancel() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", docId);
        hashMap.put("type", "1");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        mFocusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (mFocusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mIsFocuData.setFolowing("0");
                                setFocusView(R.drawable.shape_focu_gradient_ff5c77, R.drawable.focus_plus_sign, "#ffffff", "关注");
                                break;
                            case "1":          //已关注
                                mIsFocuData.setFolowing("1");
                                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.focus_plus_sign_yes, "#999999", "已关注");
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("hos_id", hosid);
                                hashMap.put("pos", "8");
                                new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, s.message);
                                        }
                                    }
                                });
                                HashMap<String, Object> hashMap2 = new HashMap<>();
                                hashMap2.put("obj_type","8");
                                hashMap2.put("obj_id",hos_userid);
                                hashMap2.put("hos_id",hosid);
                                new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, "AutoSendApi2 =="+s.message);
                                        }
                                    }
                                });
                                break;
                            case "2":          //互相关注
                                mIsFocuData.setFolowing("2");
                                setFocusView(R.drawable.shape_focu_gradient_fffff, R.drawable.each_focus, "#999999", "互相关注");
                                break;
                        }

                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 修改关注样式
     *
     * @param drawable：
     * @param image
     * @param color
     * @param text
     */
    private void setFocusView(int drawable, int image, String color, String text) {
        docFans1.setBackground(ContextCompat.getDrawable(mContext, drawable));
        imgageFans1.setImageResource(image);
        centerFans1.setTextColor(Color.parseColor(color));
        centerFans1.setText(text);

        docFans2.setBackground(ContextCompat.getDrawable(mContext, drawable));
        imgageFans2.setImageResource(image);
        centerFans2.setTextColor(Color.parseColor(color));
        centerFans2.setText(text);

        LayoutParams layoutParams1 = docFans1.getLayoutParams();
        LayoutParams layoutParams2 = docFans2.getLayoutParams();
        switch (text.length()) {
            case 2:
                layoutParams1.width = Utils.dip2px(mContext, 68);
                layoutParams2.width = Utils.dip2px(mContext, 68);
                break;
            case 3:
                layoutParams1.width = Utils.dip2px(mContext, 78);
                layoutParams2.width = Utils.dip2px(mContext, 78);
                break;
            case 4:
                layoutParams1.width = Utils.dip2px(mContext, 90);
                layoutParams2.width = Utils.dip2px(mContext, 90);
                break;
        }

        docFans1.setLayoutParams(layoutParams1);
        docFans2.setLayoutParams(layoutParams2);
    }

    private boolean flag = false;

    /**
     * 加载医生详情头部信息
     */
    void initDocheadMessage() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", docId);

        new DoctorHeadMessageApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

                if (serverData.code.equals("1")) {
                    dhdata = JSONUtil.TransformDodecHead(serverData.data);
                    hosid = dhdata.getHospital_id();
                    po = dhdata.getPo();
                    ifsixin = dhdata.getSixin();
                    is_rongyun = dhdata.getIs_rongyun();  //判断私信是否开通
                    hos_userid = dhdata.getHos_userid();
                    phoneTel = dhdata.getTel();
                    if (!flag) {
                        if (!TextUtils.isEmpty(isAutoSend)) {
                            //2:从SKU详情页跳转到医生主页
                            //3:从医院主页跳转到医生主页
                            if (Utils.isLogin()) {
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("hos_id", hosid);
                                hashMap.put("pos", isAutoSend);
                                new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, s.message);
                                        }
                                    }
                                });
                                HashMap<String, Object> hashMap2 = new HashMap<>();
                                hashMap2.put("obj_type",isAutoSend);
                                hashMap2.put("obj_id",hos_userid);
                                hashMap2.put("hos_id",hosid);
                                new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, "AutoSendApi2 =="+s.message);
                                        }
                                    }
                                });
                            }
                        }
                        flag = true;
                    }

                    if (ifsixin.equals("0")) {//普通医生
                        tiYuyueBt.setText("向TA提问");
                        titleBarTV1.setText("医生主页");
                        initTaolistData();
                        initRijiListData();
                        initBbsData();

                        // 非特邀医生
                        teyaoContentLy.setVisibility(View.GONE);
                        contentWeb.setVisibility(View.GONE);
                        putongDocLy.setVisibility(View.VISIBLE);
                        putongBtom.setVisibility(View.VISIBLE);
                        teyaoBtom.setVisibility(View.GONE);

                    } else if (ifsixin.equals("1")) {//悦美特邀医生
                        tiYuyueBt.setText("咨询");
                        titleBarTV1.setText("悦美特邀医生");
                        // 特邀医生

                        initTeyaoTaolistData();


                        teyaoMoreFabiaoRly.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(mContext, DoctorSaidActivity.class);
                                intent.putExtra("id", docId);
                                startActivity(intent);
                            }
                        });


                        //查看全部评论点击事件
                        teyaoMoreShareRly.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent it = new Intent();
                                it.putExtra("docid", docId);
                                it.setClass(mContext, DocDeRijiListActivity.class);
                                startActivity(it);
                            }
                        });


                        lvBBslistTeyaoPingjiaData = dhdata.getSharelist();

                        int sssize = lvBBslistTeyaoPingjiaData.size();
                        Log.e("TAG", "sssize = " + sssize);
                        String sharenum = dhdata.getShareNum();
                        int sharenumInt = Integer.parseInt(sharenum);
                        if (sharenumInt > sssize) {
                            teyaoMoreShareRly.setVisibility(View.VISIBLE);
                            teyaoMoreShareTv.setText("查看全部" + sharenum + "个评价 >");
                        } else {
                            teyaoMoreShareRly.setVisibility(View.GONE);
                        }

                        if (sssize > 0) {
                            for (int i = 0; i < sssize; i++) {
                                lvBBslistTeyaoPingjiaData1.add(lvBBslistTeyaoPingjiaData.get(i));
                            }
                            bbsListShareAdapter = new DocDiaryListAdapter(mContext, lvBBslistTeyaoPingjiaData1);
                            teyaoShaerlist.setAdapter(bbsListShareAdapter);
                            teyaoShaerlist.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                    String id = lvBBslistTeyaoPingjiaData1.get(pos).getQ_id();
                                    Utils.tongjiApp(mContext, "doctor_post", id, docId, "9");
                                    String appmurl = lvBBslistTeyaoPingjiaData1.get(pos).getAppmurl();
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                }
                            });

                        } else {
                            zanwuShareTv.setText("暂无口碑日记");
                        }


                        lvBBslistTeyaoFabiaoData = dhdata.getPostlist();
                        int ssize = lvBBslistTeyaoFabiaoData.size();

                        String postnum = dhdata.getPostNum();

                        if (Integer.parseInt(postnum) > ssize) {
                            teyaoMoreFabiaoRly.setVisibility(View.VISIBLE);
                            teyaoMoreFabiaoTv.setText("查看全部" + postnum + "个发表 >");
                        } else {
                            teyaoMoreFabiaoRly.setVisibility(View.GONE);
                        }

                        if (ssize > 0) {
                            for (int i = 0; i < ssize; i++) {
                                lvBBslistTeyaoFabiaoData1.add(lvBBslistTeyaoFabiaoData.get(i));
                            }
                            teyaoFabiaoAdapter = new TeyaoFabiaoAdapter(mContext, lvBBslistTeyaoFabiaoData1);
                            teyaoFabiaolist.setAdapter(teyaoFabiaoAdapter);
                            teyaoFabiaolist.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                    String id = lvBBslistTeyaoFabiaoData1.get(pos).getQ_id();

                                    Utils.tongjiApp(mContext, "doctor_post", id, docId, "9");

                                    String url = lvBBslistTeyaoFabiaoData1.get(pos).getUrl();
                                    String appmurl = lvBBslistTeyaoFabiaoData1.get(pos).getAppmurl();

                                    Log.e(TAG, "url == " + url);
                                    Log.e(TAG, "id == " + id);
                                    Log.e(TAG, "appmurl == " + appmurl);

                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                }
                            });

                        } else {
                            zanwuFabiaoTv.setText("暂无医生发表");
                        }

                        ViewTreeObserver vto = teyaoHosNameTv.getViewTreeObserver();
                        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                teyaoHosNameTv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                teyaoHosNameTv.getHeight();

                                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) teyaoHosNameTv.getLayoutParams();
                                lp.setMargins(80, ((windowsWight * 534 / 750) - (teyaoHosNameTv.getHeight() / 2)), 80, 0);
                                teyaoHosNameTv.setLayoutParams(lp);

                                ViewGroup.MarginLayoutParams lp1 = (ViewGroup.MarginLayoutParams) docFans2.getLayoutParams();
                                lp1.setMargins(0, ((windowsWight * 534 / 750) - (teyaoHosNameTv.getHeight() / 2) - Utils.dip2px(mContext, 40)), 0, 0);
                                docFans2.setLayoutParams(lp1);

                                ViewGroup.MarginLayoutParams lp2 = (ViewGroup.MarginLayoutParams) teyaoDocSubNameTv.getLayoutParams();
                                lp2.setMargins(0, ((windowsWight * 534 / 750) - (teyaoHosNameTv.getHeight() / 2) - Utils.dip2px(mContext, 75)), 0, 0);
                                teyaoDocSubNameTv.setLayoutParams(lp2);

                                ViewGroup.MarginLayoutParams lp3 = (ViewGroup.MarginLayoutParams) teyaoDocnameTv.getLayoutParams();
                                lp3.setMargins(0, ((windowsWight * 534 / 750) - (teyaoHosNameTv.getHeight() / 2) - Utils.dip2px(mContext, 110)), 0, 0);
                                teyaoDocnameTv.setLayoutParams(lp3);

                                ViewGroup.MarginLayoutParams lp4 = (ViewGroup.MarginLayoutParams) teyaoHeadJianbianRly.getLayoutParams();
                                lp4.setMargins(0, 0, 0, teyaoHosNameTv.getHeight() / 2);
                                teyaoHeadJianbianRly.setLayoutParams(lp4);
                            }
                        });

                        x.image().bind(teyaoBigDocIv, dhdata.getBig_img(), imageOptions1);

                        int hos_nu = dhdata.getHospital_name().size();
                        String hosnames = "";
                        for (int k = 0; k < hos_nu; k++) {
                            hosnames = hosnames + dhdata.getHospital_name().get(k);
                            if (k < (hos_nu - 1)) {
                                hosnames = hosnames + "<br/>";
                            }
                        }
                        teyaoHosNameTv.setText(Html.fromHtml(hosnames));
                        teyaoHosNameTv.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent it = new Intent();
                                it.setClass(mContext, HosDetailActivity.class);
                                it.putExtra("hosid", hosid);
                                startActivity(it);
                                HashMap<String, Object> hashMap = new HashMap<>();
                                hashMap.put("hos_id", hosid);
                                hashMap.put("pos", "10");
                                new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, s.message);
                                        }
                                    }
                                });
                                HashMap<String, Object> hashMap2 = new HashMap<>();
                                hashMap2.put("obj_type","10");
                                hashMap2.put("obj_id",hos_userid);
                                hashMap2.put("hos_id",hosid);
                                new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                    @Override
                                    public void onSuccess(ServerData s) {
                                        if ("1".equals(s.code)) {
                                            Log.e(TAG, "AutoSendApi2 =="+s.message);
                                        }
                                    }
                                });


                            }
                        });
                        teyaoDocnameTv.setText(dhdata.getDoctor_name());
                        if (null != dhdata.getWork_year() && !dhdata.getWork_year().equals("0")) {
                            teyaoDocSubNameTv.setText(dhdata.getTitle() + ",从业" + dhdata.getWork_year() + "年");
                        } else {
                            teyaoDocSubNameTv.setText(dhdata.getTitle());
                        }

                        teyaoArdentTv.setText(dhdata.getTaoNum());
                        if (dhdata.getShareNum().equals("0")) {
                            teyaoAttentionCly.setVisibility(View.INVISIBLE);
                        } else {
                            teyaoAttentionTv.setText(dhdata.getShareNum());
                        }


                        if (null != dhdata.getPersign() && dhdata.getPersign().length() > 0) {
                            teyaoDocDescLy.setVisibility(View.VISIBLE);
                            teyaoDocDescTv.setText(Html.fromHtml(dhdata.getPersign()));
                        } else {
                            teyaoDocDescLy.setVisibility(View.GONE);
                        }

                        LodUrl2();

                        teyaoContentLy.setVisibility(View.VISIBLE);
                        contentWeb.setVisibility(View.GONE);
                        putongDocLy.setVisibility(View.GONE);
                        putongBtom.setVisibility(View.GONE);
                        teyaoBtom.setVisibility(View.VISIBLE);

                        initWebview();
                        LodUrl();


                        Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time
                        t.setToNow(); // 取得系统时间。
                        int hour = t.hour; // 0-23

                        if (hour > 8 && hour < 22) {
                            toSixinBt.setText("私信");
                        } else {
                            toSixinBt.setText("请留言");
                        }

                    }


                    String tao = dhdata.getTaoNum();
                    moretaoTv.setText("查看全部" + tao + "个服务");
                    String riji = dhdata.getShareNum();
                    if ("0".equals(riji)) {
                        morerijiTv.setText("查看全部");
                    } else {
                        morerijiTv.setText("查看全部" + riji + "个日记");
                    }
                    String bbs = dhdata.getPostNum();
                    morebbsTv.setText("查看全部" + bbs + "个帖子");
                    if ("3".equals(is_rongyun)) {
                        toQuestionBt.setText("私信");
                    } else {
                        isyuyue = dhdata.getCooperation();//判断预约TA是否开通   2可以预约
                        if (isyuyue.equals("2")) {
                            toQuestionBt.setText("预约TA");
                        } else {
                            toQuestionBt.setText("预约TA（未开通）");
                            toQuestionBt.setTextSize(13);
                            toQuestionBt.setBackgroundResource(R.drawable.shape_hui_bian);
                            toQuestionBt.setClickable(false);
                            toQuestionBt.setTextColor(getResources().getColor(R.color._bb));
                        }
                    }
                    titleBarTV2.setText(dhdata.getDoctor_name());


                    x.image().bind(docHeadIv, dhdata.getImg(), imageOptions);

                    x.image().bind(titleIv, dhdata.getImg(), imageOptions);

                    if (null != dhdata.getCoverp() && dhdata.getCoverp().equals("1")) {
                        fnegmianIv.setVisibility(View.VISIBLE);
                    } else {
                        fnegmianIv.setVisibility(View.GONE);
                    }

                    docName = dhdata.getDoctor_name();
                    doctorDetailsWebViewClien.setDocName(docName);
                    docNameTv.setText(dhdata.getDoctor_name());
                    List<String> hospital_name = dhdata.getHospital_name();
                    if (hospital_name.size() != 0) {
                        hosTv.setText(dhdata.getHospital_name().get(0));
                    }
                    if (null != dhdata.getAttention()) {
                        String guanzhu = dhdata.getAttention();
                        if (guanzhu.equals("0")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d0);
                        } else if (guanzhu.equals("1")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d1);
                        } else if (guanzhu.equals("2")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d2);
                        } else if (guanzhu.equals("3")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d3);
                        } else if (guanzhu.equals("4")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d4);
                        } else if (guanzhu.equals("5")) {
                            guanzhuIv.setBackgroundResource(R.drawable.item_bk_d5);
                        }
                    }
                    String safe = dhdata.getArdent();
                    if (safe.equals("0")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d0);
                    } else if (safe.equals("1")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d1);
                    } else if (safe.equals("2")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d2);
                    } else if (safe.equals("3")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d3);
                    } else if (safe.equals("4")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d4);
                    } else if (safe.equals("5")) {
                        hotxinIv.setBackgroundResource(R.drawable.item_bk_d5);
                    }

                    if (null != dhdata.getDesc() && dhdata.getDesc().length() > 0) {

                        docDescLy.setVisibility(View.VISIBLE);
                        docNodescLy.setVisibility(View.GONE);

                        descriptionView.setText(Html.fromHtml(dhdata.getDesc()));

                        descriptionView.setHeight(descriptionView.getLineHeight() * maxDescripLine);
                        descriptionView.post(new Runnable() {

                            @Override
                            public void run() {
                                expandView.setVisibility(descriptionView.getLineCount() > maxDescripLine ? View.VISIBLE : View.GONE);
                            }
                        });

                        findViewById(R.id.description_layout).setOnClickListener(new OnClickListener() {
                            boolean isExpand;

                            @Override
                            public void onClick(View v) {
                                isExpand = !isExpand;
                                descriptionView.clearAnimation();
                                final int deltaValue;
                                final int startValue = descriptionView.getHeight();
                                int durationMillis = 350;
                                if (isExpand) {
                                    deltaValue = descriptionView.getLineHeight() * descriptionView.getLineCount() - startValue;
                                    RotateAnimation animation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                    animation.setDuration(durationMillis);
                                    animation.setFillAfter(true);
                                    expandView.startAnimation(animation);
                                } else {
                                    deltaValue = descriptionView.getLineHeight() * maxDescripLine - startValue;
                                    RotateAnimation animation = new RotateAnimation(180, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                                    animation.setDuration(durationMillis);
                                    animation.setFillAfter(true);
                                    expandView.startAnimation(animation);
                                }
                                Animation animation = new Animation() {
                                    protected void applyTransformation(float interpolatedTime, Transformation t) {
                                        descriptionView.setHeight((int) (startValue + deltaValue * interpolatedTime));
                                    }

                                };
                                animation.setDuration(durationMillis);
                                descriptionView.startAnimation(animation);
                            }
                        });
                    } else {
                        docDescLy.setVisibility(View.GONE);
                        docNodescLy.setVisibility(View.VISIBLE);
                    }

                }
                scroll1.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

    /**
     * 加载web
     */
    public void LodUrl() {
        baseWebViewClientMessage.startLoading();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", docId);
        hashMap.put("flag", "1");

        Log.e(TAG, "docId == " + docId);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(FinalConstant.DOC_DETAIL, hashMap);
        if (null != bbsDetWeb) {
            bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }

    }

    private void LodUrl2() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("id", docId);
        Log.e(TAG, "docId == " + docId);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(FinalConstant.ABOUTDOC, hashMap);
        if (null != docDetWeb22) {
            docDetWeb22.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }

    void initShareData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", docId);
        new ShareDataApi().getCallBack(mContext, maps, new BaseCallBackListener<DocShareData>() {
            @Override
            public void onSuccess(DocShareData docShareData) {
                mDocShareData = docShareData;
                shareUrl = mDocShareData.getUrl();
                shareImgUrl = mDocShareData.getImg();

                if (mDocShareData.getDoc_name().length() > 0) {
                    shareContent = shareContent + mDocShareData.getDoc_name() + "，";
                }
                if (mDocShareData.getHos_name().length() > 0) {
                    shareContent = shareContent + mDocShareData.getHos_name() + "，";
                }
                if (mDocShareData.getRange().length() > 0) {
                    shareContent = shareContent + mDocShareData.getRange();
                }

                mWechat = mDocShareData.getWechat();
            }

        });

    }


    void initTeyaoTaolistData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("page", "1");
        maps.put("id", docId);
        new TaoListDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    lvteyaoTaoData = JSONUtil.TransformHomeTao548(serverData.data);

                    if (null != lvteyaoTaoData && lvteyaoTaoData.size() > 0) {
                        teyaoTaoAdapter = new TeyaoTaoAdapter(mContext, lvteyaoTaoData);
                        teyaoTaolist.setAdapter(teyaoTaoAdapter);


                        ViewTreeObserver vto = teyaoTaoLLy.getViewTreeObserver();
                        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                teyaoTaoLLy.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                teyaoTaoLLy.getHeight();

                                pointYs[0] = teyaoTaoLLy.getTop();
                                pointYs[1] = teyaoPingjiaLLy.getTop();
                                pointYs[2] = teyaoFabiaoLLy.getTop();
                                pointYs[3] = contentWeb22.getTop();

                            }
                        });


                        teyaoTaolist.setOnItemClickListener(new OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                String _id = lvteyaoTaoData.get(pos).get_id();
                                Intent it2 = new Intent();
                                it2.putExtra("id", _id);
                                it2.putExtra("source", "9");
                                it2.putExtra("objid", docId);
                                it2.setClass(mContext, TaoDetailActivity.class);
                                startActivity(it2);
                                if (Utils.isLogin()) {
                                    HashMap<String, Object> hashMap = new HashMap<>();
                                    hashMap.put("hos_id", hosid);
                                    hashMap.put("pos", "5");
                                    new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                        @Override
                                        public void onSuccess(ServerData s) {
                                            if ("1".equals(s.code)) {
                                                Log.e(TAG, s.message);
                                            }
                                        }
                                    });
                                    HashMap<String, Object> hashMap2 = new HashMap<>();
                                    hashMap2.put("obj_type","5");
                                    hashMap2.put("obj_id",_id);
                                    hashMap2.put("hos_id",hosid);
                                    new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                        @Override
                                        public void onSuccess(ServerData s) {
                                            if ("1".equals(s.code)) {
                                                Log.e(TAG, "AutoSendApi2 =="+s.message);
                                            }
                                        }
                                    });
                                }

                            }
                        });

                        scroll1.fullScroll(ScrollView.FOCUS_UP);
                    } else {
                        zanwuTaoTv.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
    }


    void initTaolistData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("page", "1");
        maps.put("id", docId);
        new TaoListDataApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData ServerData) {
                if ("1".equals(ServerData.code)) {
                    lvHomeTaoData = JSONUtil.TransformHomeTao548(ServerData.data);

                    if (null != lvHomeTaoData && lvHomeTaoData.size() > 0) {

                        taolistLy.setVisibility(View.VISIBLE);
                        if (lvHomeTaoData.size() > 3) {
                            lvHomeTaoData1.add(lvHomeTaoData.get(0));
                            lvHomeTaoData1.add(lvHomeTaoData.get(1));
                            lvHomeTaoData1.add(lvHomeTaoData.get(2));
                            Log.e("TaoAdpter623", "222222");
                            homeTaoAdpter = new TaoAdpter623(mContext, lvHomeTaoData1);
                            taoList.setAdapter(homeTaoAdpter);

                            taoList.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                                    String _id = lvHomeTaoData1.get(pos).get_id();
                                    Utils.tongjiApp(mContext, "doctor_tao", _id, docId, "9");
                                    Intent it2 = new Intent();
                                    it2.putExtra("id", _id);
                                    it2.putExtra("source", "9");
                                    it2.putExtra("objid", docId);
                                    it2.setClass(mContext, TaoDetailActivity.class);
                                    startActivity(it2);
                                    if (Utils.isLogin()) {
                                        HashMap<String, Object> hashMap = new HashMap<>();
                                        hashMap.put("hos_id", hosid);
                                        hashMap.put("pos", "5");
                                        new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                            @Override
                                            public void onSuccess(ServerData s) {
                                                if ("1".equals(s.code)) {
                                                    Log.e(TAG, s.message);
                                                }
                                            }
                                        });
                                        HashMap<String, Object> hashMap2 = new HashMap<>();
                                        hashMap2.put("obj_type","5");
                                        hashMap2.put("obj_id",_id);
                                        hashMap2.put("hos_id",hosid);
                                        new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                            @Override
                                            public void onSuccess(ServerData s) {
                                                if ("1".equals(s.code)) {
                                                    Log.e(TAG, "AutoSendApi2 =="+s.message);
                                                }
                                            }
                                        });
                                    }

                                }
                            });
                        } else {
                            moretaolistLy.setVisibility(View.GONE);
                            Log.e("TaoAdpter623", "33333");
                            homeTaoAdpter = new TaoAdpter623(mContext, lvHomeTaoData);
                            taoList.setAdapter(homeTaoAdpter);

                            taoList.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                    String _id = lvHomeTaoData.get(pos).get_id();

                                    Utils.tongjiApp(mContext, "doctor_tao", _id, docId, "9");

                                    Intent it2 = new Intent();
                                    it2.putExtra("id", _id);
                                    it2.putExtra("source", "9");
                                    it2.putExtra("objid", docId);
                                    it2.setClass(mContext, TaoDetailActivity.class);
                                    startActivity(it2);
                                    if (Utils.isLogin()) {
                                        HashMap<String, Object> hashMap = new HashMap<>();
                                        hashMap.put("hos_id", hosid);
                                        hashMap.put("pos", "5");
                                        new AutoSendApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
                                            @Override
                                            public void onSuccess(ServerData s) {
                                                if ("1".equals(s.code)) {
                                                    Log.e(TAG, s.message);
                                                }
                                            }
                                        });
                                        HashMap<String, Object> hashMap2 = new HashMap<>();
                                        hashMap2.put("obj_type","5");
                                        hashMap2.put("obj_id",_id);
                                        hashMap2.put("hos_id",hosid);
                                        new AutoSendApi2().getCallBack(mContext, hashMap2, new BaseCallBackListener<ServerData>() {
                                            @Override
                                            public void onSuccess(ServerData s) {
                                                if ("1".equals(s.code)) {
                                                    Log.e(TAG, "AutoSendApi2 =="+s.message);
                                                }
                                            }
                                        });
                                    }

                                }
                            });
                        }
                    } else {
                        taolistLy.setVisibility(View.GONE);
                    }
                } else {
                    taolistLy.setVisibility(View.GONE);
                }

            }
        });

    }

    void initRijiListData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("page", "1");
        maps.put("id", docId);
        final RiJiListApi riJiListApi = new RiJiListApi();
        riJiListApi.getCallBack(mContext, maps, new BaseCallBackListener<ArrayList<BBsListData550>>() {
            @Override
            public void onSuccess(ArrayList<BBsListData550> bbsListData550) {
                String json = riJiListApi.getJson();
                if (null != json && json.length() > 0) {
                    lvrijiBBslistData = bbsListData550;
                    if (null != lvrijiBBslistData && lvrijiBBslistData.size() > 0) {

                        rijilistLy.setVisibility(View.VISIBLE);

                        if (lvrijiBBslistData.size() > 3) {
                            lvrijiBBslistData1.add(lvrijiBBslistData.get(0));
                            lvrijiBBslistData1.add(lvrijiBBslistData.get(1));
                            lvrijiBBslistData1.add(lvrijiBBslistData.get(2));

                            bbsrijiListAdapter = new DiaryListAdapter(mContext, lvrijiBBslistData1, "");
                            rijiList.setAdapter(bbsrijiListAdapter);

                            rijiList.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                                    String id = lvrijiBBslistData1.get(pos).getQ_id();
                                    Utils.tongjiApp(mContext, "doctor_post", id, docId, "9");
                                    String url = lvrijiBBslistData1.get(pos).getUrl();
                                    String appmurl = lvrijiBBslistData1.get(pos).getAppmurl();
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                }
                            });
                        } else {
                            morerijilistLy.setVisibility(View.GONE);
                            bbsrijiListAdapter = new DiaryListAdapter(mContext, lvrijiBBslistData, "");
                            rijiList.setAdapter(bbsrijiListAdapter);

                            rijiList.setOnItemClickListener(new OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                    String id = lvrijiBBslistData.get(pos).getQ_id();

                                    Utils.tongjiApp(mContext, "doctor_post", id, docId, "9");

                                    String url = lvrijiBBslistData.get(pos).getUrl();
                                    String appmurl = lvrijiBBslistData.get(pos).getAppmurl();
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                                }
                            });
                        }
                    } else {
                        // 日记贴木有数据
                        rijilistLy.setVisibility(View.GONE);
                    }
                } else {
                    // 日记贴木有数据
                    rijilistLy.setVisibility(View.GONE);
                }
            }
        });
    }

    void initBbsData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("page", "1");
        maps.put("id", docId);
        final DocDeBBsListApi doctorDetailBBsApi = new DocDeBBsListApi();
        doctorDetailBBsApi.getCallBack(mContext, maps, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> bBsListData550) {
                lvBBslistData = bBsListData550;
                if (null != lvBBslistData && lvBBslistData.size() > 0) {

                    bbslistLy.setVisibility(View.VISIBLE);

                    if (lvBBslistData.size() > 3) {
                        lvBBslistData1.add(lvBBslistData.get(0));
                        lvBBslistData1.add(lvBBslistData.get(1));
                        lvBBslistData1.add(lvBBslistData.get(2));

                        bbsListAdapter = new BBsListAdapter(mContext, lvBBslistData1);
                        bbsList.setAdapter(bbsListAdapter);

                        bbsList.setOnItemClickListener(new OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                String id = lvBBslistData1.get(pos).getQ_id();

                                Utils.tongjiApp(mContext, "doctor_diary", id, docId, "9");

                                String url = lvBBslistData1.get(pos).getUrl();
                                String appmurl = lvBBslistData1.get(pos).getAppmurl();
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                            }
                        });
                    } else {
                        morebbslistLy.setVisibility(View.GONE);
                        bbsListAdapter = new BBsListAdapter(mContext, lvBBslistData);
                        bbsList.setAdapter(bbsListAdapter);

                        bbsList.setOnItemClickListener(new OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {


                                String id = lvBBslistData.get(pos).getQ_id();

                                Utils.tongjiApp(mContext, "doctor_diary", id, docId, "9");


                                String url = lvBBslistData.get(pos).getUrl();
                                String appmurl = lvBBslistData.get(pos).getAppmurl();
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
                            }
                        });
                    }
                } else {
                    // 日记贴木有数据
                    bbslistLy.setVisibility(View.GONE);
                }
            }

        });
    }


    void setListener() {

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        myscroll.setOnScrollChangedCallback(new ScrollViewScrolCallBack() {

            @Override
            public void scrolChanged(int l, int t) {
                if (t > 40) {
                    titleBarTV1.setVisibility(View.GONE);
                    titlenameivLy.setVisibility(View.VISIBLE);
                } else {
                    titleBarTV1.setVisibility(View.VISIBLE);
                    titlenameivLy.setVisibility(View.GONE);
                }
            }
        });


        scroll1.setOnScrollChangedCallback(new ScrollViewScrolCallBack() {

            @Override
            public void scrolChanged(int l, int t) {

                if (t > (pointYs[0] - 10)) {
                    tabContent.setVisibility(View.VISIBLE);
                } else {
                    tabContent.setVisibility(View.GONE);
                }

                if (t >= (pointYs[0] - 30) && t < (pointYs[1] - 30)) {
                    setTab(1);
                } else if (t >= (pointYs[1] - 30) && t < (pointYs[2] - 30)) {
                    setTab(2);
                } else if (t >= (pointYs[2] - 30) && t < (pointYs[3] - 30)) {
                    setTab(3);
                } else if (t >= (pointYs[3] - 30)) {
                    setTab(4);
                }

            }
        });


        tabRlly1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll1.smoothScrollTo(0, pointYs[0] - 30);
                setTab(1);
            }
        });

        tabRlly2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll1.smoothScrollTo(0, pointYs[1] - 30);
                setTab(2);
            }
        });

        tabRlly3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll1.smoothScrollTo(0, pointYs[2] - 30);
                setTab(3);
            }
        });

        tabRlly4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                scroll1.smoothScrollTo(0, pointYs[3] - 30);
                setTab(4);
            }
        });

        //分享点击
        shareRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Utils.tongjiApp(mContext, "doctor_share", "doctor", docId, "9");

                BaseShareView baseShareView = new BaseShareView(mContext);
                baseShareView.setShareContent(shareContent).ShareAction(mWechat);

                baseShareView
                        .getShareBoardlistener()
                        .setSinaText(shareContent + "，点击查看更多" + shareUrl + "分享自@悦美整形APP")
                        .setSmsText(shareContent + "，点击查看更多" + shareUrl)
                        .setSinaThumb(new UMImage(mContext, shareImgUrl))
                        .setTencentUrl(shareUrl)
                        .setTencentTitle(shareContent)
                        .setTencentThumb(new UMImage(mContext, shareImgUrl))
                        .setTencentDescription(shareContent)
                        .setTencentText(shareContent)
                        .getUmShareListener()
                        .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                            @Override
                            public void onShareResultClick(SHARE_MEDIA platform) {
                                if (platform.equals(SHARE_MEDIA.SMS)) {

                                } else {
                                    Toast.makeText(DoctorDetailsActivity592.this, " 分享成功啦", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                            }
                        });

            }
        });


        collectRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utils.tongjiApp(mContext, "doctor_collect", "doctor", docId, "9");

                uid = Utils.getUid();
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        if (ifcollect) {
                            deleCollectHttp();
                        } else {
                            collectHttp();
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    // ViewInject.toast("亲，登录后才可以收藏哟！");
                    Utils.jumpLogin(mContext);
                }
            }
        });


        moretaolistLy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.putExtra("docid", docId);
                it.setClass(mContext, DocDeTaoListActivity.class);
                startActivity(it);
            }
        });

        morerijilistLy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.putExtra("docid", docId);
                it.setClass(mContext, DocDeRijiListActivity.class);
                startActivity(it);
            }
        });

        morebbslistLy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.putExtra("docname", docName);
                it.putExtra("docid", docId);
                it.setClass(mContext, DocBbsListActivity.class);
                startActivity(it);
            }
        });


        toQuestionBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if ("3".equals(is_rongyun)) {
                    if (Utils.noLoginChat()) {
                        toChatActivity();
                    } else {
                        if (Utils.isLoginAndBind(mContext)) {
                            toChatActivity();
                        }
                    }

                } else {
                    Utils.tongjiApp(mContext, "doctor_order", "doctor", docId, "9");
                    String shareid = "0";
                    Intent it1 = new Intent();
                    it1.setClass(mContext, WantBeautifulActivity548.class);
                    it1.putExtra("po", po);
                    it1.putExtra("hosid", hosid);
                    it1.putExtra("docid", docId);
                    it1.putExtra("shareid", shareid);
                    it1.putExtra("cityId", "0");
                    it1.putExtra("partId", "0");
                    it1.putExtra("partId_two", "0");
                    startActivity(it1);
                }

            }
        });

        /**
         * 私信或向医生提问
         */
        tiYuyueBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ifsixin.equals("0")) {
                    startActivity(new Intent(mContext, TypeProblemActivity.class));
                } else {
                    if (Utils.noLoginChat()) {
                        toChatActivity();
                    } else {
                        if (Utils.isLoginAndBind(mContext)) {
                            toChatActivity();
                        }
                    }
                }
            }

        });

        /**
         * 私信
         */
        toSixinBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (Utils.noLoginChat()) {
                    if ("3".equals(is_rongyun)) {
                        toChatActivity();
                    }
                } else {
                    if (Utils.isLoginAndBind(mContext)) {
                        if ("3".equals(is_rongyun)) {
                            toChatActivity();
                        }
                    }
                }
            }
        });

        /**
         * 合作医生的预约面诊
         *
         */
        toYuyueBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Utils.tongjiApp(mContext, "doctor_order", "doctor", docId, "9");

                Intent it1 = new Intent();
                it1.setClass(mContext, YuYueDocActivity.class);
                it1.putExtra("docid", docId);
                it1.putExtra("partId", "0");
                it1.putExtra("docname", docName);
                startActivityForResult(it1, 88);
            }
        });

        /**
         * 电话咨询
         */
        phoneZixun.setOnClickListener(new OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {

                Utils.tongjiApp(mContext, "doctor_tel", "doctor", docId, "9");

                if (null != phoneTel && phoneTel.length() > 0) {
                    ViewInject.toast("正在拨打中·····");
                    Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneTel));
                    try {
                        startActivity(it);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void toChatActivity() {
        ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                .setDirectId(hos_userid)
                .setObjId(docId)
                .setObjType("3")
                .setTitle(dhdata.getTitle())
                .setImg(dhdata.getImg())
                .setYmClass("3")
                .setYmId(docId)
                .build();
        new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);
        TongJiParams tongJiParams = new TongJiParams.TongJiParamsBuilder()
                .setEvent_name("chat_hospital")
                .setEvent_pos("doctor")
                .setHos_id(dhdata.getHospital_id())
                .setDoc_id(docId)
                .setTao_id("0")
                .setEvent_others(dhdata.getHospital_id())
                .setId(docId)
                .setReferrer("17")
                .setType("3")
                .build();
        Utils.chatTongJi(mContext, tongJiParams);
    }

    /**
     * 收藏接口
     */
    void collectHttp() {
        BaikeFourApi baikeFourApi = new BaikeFourApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", docId);
        params.put("type", "1");
        baikeFourApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                ifcollect = true;
                collectIv.setImageResource(R.drawable.sku_collect_select);
                if (!serverData.isOtherCode){
                    MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "收藏成功", 1000).show();

                }
            }
        });

    }

    /**
     * 取消收藏
     */
    void deleCollectHttp() {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", docId);
        params.put("type", "1");
        cancelCollectApi.getCallBack(mContext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {
                ifcollect = false;
                collectIv.setImageResource(R.drawable.start_black);
                MyToast.makeImgAndTextToast(mContext, getResources().getDrawable(R.drawable.tips_smile), "取消收藏", 1000).show();
            }
        });
    }


    /**
     * 判断是否收藏
     */
    void ifCollect() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("objid", docId);
        maps.put("type", "1");
        new IsCollectApi().getCallBack(mContext, maps, new BaseCallBackListener<String>() {
            @Override
            public void onSuccess(String s) {
                if (s.equals("1")) {
                    ifcollect = true;
                    collectIv.setImageResource(R.drawable.start_pink);
                } else {
                    ifcollect = false;
                    collectIv.setImageResource(R.drawable.start_black);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 88:
                if (data != null) {
                    String code = data.getStringExtra("code");
                    new TiJiaoPopupWindows(mContext, allcontent);
                }
                break;
        }
    }

    @SuppressWarnings("deprecation")
    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    public void initWebview() {
        bbsDetWeb = new WebView(mContext);

        bbsDetWeb.getSettings().setPluginState(WebSettings.PluginState.ON);
        bbsDetWeb.getSettings().setAllowFileAccess(true); // 允许访问文件
        bbsDetWeb.getSettings().setLoadWithOverviewMode(true);
        bbsDetWeb.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容

        bbsDetWeb.getSettings().setJavaScriptEnabled(true);
        bbsDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        bbsDetWeb.getSettings().setUseWideViewPort(true);
        bbsDetWeb.getSettings().supportMultipleWindows();
        bbsDetWeb.getSettings().setNeedInitialFocus(true);
        bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
        bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(bbsDetWeb);
    }


    EditText phoneEt;
    EditText codeEt;
    RelativeLayout getCodeRly;
    TextView getCodeTv;

    /**
     * baseWebViewClientMessage
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_zixun_getphone, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            Button cancelBt = view.findViewById(R.id.cancel_bt);
            Button zixunBt = view.findViewById(R.id.zixun_bt);
            phoneEt = view.findViewById(R.id.no_pass_login_phone_et);
            codeEt = view.findViewById(R.id.no_pass_login_code_et);
            getCodeRly = view.findViewById(R.id.no_pass_yanzheng_code_rly);
            getCodeTv = view.findViewById(R.id.yanzheng_code_tv);

            zixunBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phone = phoneEt.getText().toString();
                    String codestr = codeEt.getText().toString();
                    if (phone.length() > 0) {
                        if (ifPhoneNumber()) {
                            if (codestr.length() > 0) {
                                initCode1();
                            } else {
                                ViewInject.toast("请输入验证码");
                            }
                        } else {
                            ViewInject.toast("请输入正确的手机号");
                        }
                    } else {
                        ViewInject.toast("请输入手机号");
                    }
                }
            });

            getCodeRly.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phone = phoneEt.getText().toString();
                    if (phone.length() > 0) {
                        if (ifPhoneNumber()) {
                            sendEMS();
                        } else {
                            ViewInject.toast("请输入正确的手机号");
                        }
                    } else {
                        ViewInject.toast("请输入手机号");
                    }

                }
            });

            cancelBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    private boolean ifPhoneNumber() {
        String textPhn = phoneEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

    void sendEMS() {
        String phone = phoneEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("phone", phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    ViewInject.toast("请求成功");

                    getCodeRly.setClickable(false);
                    codeEt.requestFocus();

                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // getCodeRly
                            // .setBackgroundResource(R.drawable.biankuang_hui);
                            getCodeTv.setTextColor(getResources().getColor(R.color.button_zi));
                            getCodeTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            getCodeTv.setTextColor(getResources().getColor(R.color.button_bian_hong1));
                            getCodeRly.setClickable(true);
                            getCodeTv.setText("重发验证码");
                        }
                    }.start();
                } else {
                    ViewInject.toast(s.message);
                }
            }
        });
    }

    void initCode1() {
        String codeStr = codeEt.getText().toString();
        String phone = phoneEt.getText().toString();
        phoneStr = phone;
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("phone", phone);
        maps.put("code", codeStr);
        maps.put("flag", "1");
        new SecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData)) {
                    Cfg.saveStr(mContext, FinalConstant.UPHONE, phoneStr);
                    zixunPop.dismiss();
                    baseWebViewClientMessage.stopLoading();
                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                            .setDirectId(hos_userid)
                            .setObjId("0")
                            .setObjType("3")
                            .setTitle(docName)
                            .setYmClass("3")
                            .setYmId(docId)
                            .build();
                    new PageJumpManager(mContext).jumpToChatBaseActivity(chatParmarsData);

                } else {
                    baseWebViewClientMessage.stopLoading();
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    public class TiJiaoPopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public TiJiaoPopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_yuyue_sumbit, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button iknownBt = view.findViewById(R.id.iknown_bt);

            iknownBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mContext = DoctorDetailsActivity592.this;
        uid = Utils.getUid();

        if (!"0".equals(uid)) {
            ifCollect();
            phoneStr = Cfg.loadStr(mContext, FinalConstant.UPHONE, "");
        }

        initShareData();

        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        contentWeb.removeAllViews();

        if (bbsDetWeb != null) {
            bbsDetWeb.clearHistory();
            bbsDetWeb.clearCache(true);
            bbsDetWeb.loadUrl("about:blank");
            bbsDetWeb.freeMemory();
            bbsDetWeb.pauseTimers();
            bbsDetWeb = null;
        }

    }

}
