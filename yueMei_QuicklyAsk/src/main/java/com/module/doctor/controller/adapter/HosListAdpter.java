/**
 *
 */
package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.doctor.model.bean.HosListData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.taodetail.model.bean.SkuLabelLevel;
import com.quicklyask.activity.R;

import org.xutils.image.ImageOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * 医院填充器
 *
 * @author Rubin
 */
public class HosListAdpter extends BaseAdapter {

    private final String TAG = "HosListAdpter";

    private List<HosListData> mDoctorData = new ArrayList<>();
    private Activity mContext;
    private LayoutInflater inflater;
    private HosListData doctorData;
    ViewHolder viewHolder;


    ImageOptions imageOptions;

    public HosListAdpter(Activity mContext, List<HosListData> mDoctorData) {
        this.mContext = mContext;
        this.mDoctorData = mDoctorData;
        inflater = LayoutInflater.from(mContext);

        imageOptions = new ImageOptions.Builder().setCircular(true).setConfig(Bitmap.Config.ARGB_4444).setPlaceholderScaleType(ImageView.ScaleType.FIT_XY).setLoadingDrawableId(R.drawable.radius_gray80).setFailureDrawableId(R.drawable.radius_gray80).build();
    }

    static class ViewHolder {
        public ImageView docHeadPic;
        public ImageView docYueIv;
        public ImageView docPeiIv;
        public TextView docNameTV;

        public RatingBar ratBar;
        public TextView scotreTv;
        public TextView docCommnetNumTV;
        public TextView docDiZhiTv;
        public TextView juliTv;
        public LinearLayout docTagContainer;
        public TextView docTagLevel;
        public TextView docTagName;

        public TextView docTao1NameTv;
        public TextView docTao2NameTv;
        public TextView docTao1PriceTv;
        public TextView docTao2PriceTv;
//        public TextView docMoreTaoTv;
        public LinearLayout docTao1Content;
        public LinearLayout docTao2Content;
        public LinearLayout docTao1PlusVibility;
        public LinearLayout docTao2PlusVibility;
        public TextView docTao1PlusPrice;
        public TextView docTao2PlusPrice;
        public ImageView docTao1Iv;
        public ImageView docTao2Iv;

        public int flag;

    }

    @Override
    public int getCount() {
        return mDoctorData.size();
    }

    @Override
    public Object getItem(int position) {
        return mDoctorData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null || ((ViewHolder) convertView.getTag()).flag != position) {
            convertView = inflater.inflate(R.layout.hos_list_item_view, null);
            viewHolder = new ViewHolder();

            viewHolder.flag = position;

            viewHolder.docHeadPic = convertView.findViewById(R.id.doc_list_head_image_iv);
            viewHolder.docYueIv = convertView.findViewById(R.id.doc_list_item_cooperation_iv);
            viewHolder.docPeiIv = convertView.findViewById(R.id.doc_list_item_pei_iv);
            viewHolder.docNameTV = convertView.findViewById(R.id.doc_list_item_name_tv);
            viewHolder.ratBar = convertView.findViewById(R.id.room_ratingbar);
            viewHolder.scotreTv = convertView.findViewById(R.id.comment_score_list_tv);
            viewHolder.docCommnetNumTV = convertView.findViewById(R.id.comment_num_hos_list_tv);

            viewHolder.juliTv = convertView.findViewById(R.id.doc_list_item_juli_tv);
            viewHolder.docTagContainer = convertView.findViewById(R.id.doc_list_item_tagcontianer);
            viewHolder.docTagLevel = convertView.findViewById(R.id.doc_list_item_level);
            viewHolder.docTagName = convertView.findViewById(R.id.doc_list_item_tagtitle);

            viewHolder.docDiZhiTv = convertView.findViewById(R.id.doc_list_item_shanchang_tv);

            viewHolder.docTao1NameTv = convertView.findViewById(R.id.doc_list_tao1_name_tv);
            viewHolder.docTao2NameTv = convertView.findViewById(R.id.doc_list_tao2_name_tv);
            viewHolder.docTao1PriceTv = convertView.findViewById(R.id.doc_list_tao1_jg_tv);
            viewHolder.docTao2PriceTv = convertView.findViewById(R.id.doc_list_tao2_jg_tv);
//            viewHolder.docMoreTaoTv = convertView.findViewById(R.id.doc_list_more_tao_tv);

            viewHolder.docTao1Content = convertView.findViewById(R.id.doc_list_tao1_ly);
            viewHolder.docTao2Content = convertView.findViewById(R.id.doc_list_tao2_ly);


            viewHolder.docTao1PlusVibility = convertView.findViewById(R.id.hos_list_plus_vibiliyt);
            viewHolder.docTao2PlusVibility = convertView.findViewById(R.id.hos_list_plus_vibiliyt2);
            viewHolder.docTao1PlusPrice = convertView.findViewById(R.id.hos_plus_price);
            viewHolder.docTao2PlusPrice = convertView.findViewById(R.id.hos_plus_price2);

            viewHolder.docTao1Iv = convertView.findViewById(R.id.hos_list_tao_iv1);
            viewHolder.docTao2Iv = convertView.findViewById(R.id.hos_list_tao_iv2);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        doctorData = mDoctorData.get(position);


        // 填充布局
        viewHolder.docNameTV.setText(doctorData.getHos_name());

        Glide.with(mContext).load(doctorData.getImg()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.ic_headpic_img).error(R.drawable.ic_headpic_img).into(viewHolder.docHeadPic);

        String kind = doctorData.getKind();

        if (kind.equals("1")) {
            viewHolder.docYueIv.setVisibility(View.VISIBLE);
            viewHolder.docPeiIv.setVisibility(View.GONE);
        } else {
            viewHolder.docYueIv.setVisibility(View.GONE);
            viewHolder.docPeiIv.setVisibility(View.VISIBLE);
        }

        if (null != doctorData.getDistance()) {
            if (doctorData.getDistance().length() > 0) {
                viewHolder.juliTv.setText(doctorData.getDistance());
            }
        }

        SkuLabelLevel hospitalTop = doctorData.getHospital_top();
        if (hospitalTop != null && !TextUtils.isEmpty(hospitalTop.getDesc())){
            viewHolder.docTagContainer.setVisibility(View.VISIBLE);
            viewHolder.docTagLevel.setText("NO."+hospitalTop.getLevel());
            viewHolder.docTagName.setText(hospitalTop.getDesc());
        }else {
            viewHolder.docTagContainer.setVisibility(View.GONE);
        }

        if (doctorData.getComment_bili() != null && doctorData.getComment_bili().length() > 0) {
            String startNum = doctorData.getComment_bili();

            if (startNum.length() > 0) {
                int num = Integer.parseInt(startNum);
                viewHolder.ratBar.setMax(100);
                viewHolder.ratBar.setProgress(num);

                if (num > 0) {
                    viewHolder.scotreTv.setText(doctorData.getComment_score());
                } else {
                    viewHolder.scotreTv.setText("暂无评价");
                }

            }

        }

        if (null != doctorData.getComment_people() && doctorData.getComment_people().length() > 0 && !doctorData.getComment_people().equals("0")) {
            viewHolder.docCommnetNumTV.setVisibility(View.VISIBLE);
            viewHolder.docCommnetNumTV.setText(doctorData.getComment_people() + "人预定");
        } else {
            viewHolder.docCommnetNumTV.setVisibility(View.GONE);
        }

        viewHolder.docDiZhiTv.setText(doctorData.getAddress());

        int taosize = doctorData.getTao().size();
        if (taosize > 0) {

            if (taosize == 1) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.GONE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao1PriceTv.setText("￥" + doctorData.getTao().get(0).getPrice_discount());

                String member_price = doctorData.getTao().get(0).getMember_price();
                int i = Integer.parseInt(member_price);
                if (i >= 0){
                    viewHolder.docTao1PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PlusPrice.setText("¥"+member_price);
                }else {
                    viewHolder.docTao1PlusVibility.setVisibility(View.GONE);
                }
                String iscu = doctorData.getTao().get(0).getDacu66_id();
                if (iscu.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

            } else if (taosize == 2) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao1PriceTv.setText("￥" + doctorData.getTao().get(0).getPrice_discount());
                viewHolder.docTao2NameTv.setText(doctorData.getTao().get(1).getTitle());
                viewHolder.docTao2PriceTv.setText("￥" + doctorData.getTao().get(1).getPrice_discount());

                String member_price = doctorData.getTao().get(0).getMember_price();
                String member_price2 = doctorData.getTao().get(1).getMember_price();

                int i = Integer.parseInt(member_price);
                int j = Integer.parseInt(member_price2);
                if (i >= 0){
                    viewHolder.docTao1PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PlusPrice.setText("¥"+member_price);
                }else {
                    viewHolder.docTao1PlusVibility.setVisibility(View.GONE);
                }
                if (j >= 0){
                    viewHolder.docTao2PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao2PlusPrice.setText("¥"+member_price2);
                }else {
                    viewHolder.docTao2PlusVibility.setVisibility(View.GONE);
                }
                String iscu1 = doctorData.getTao().get(0).getDacu66_id();
                if (iscu1.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

                String iscu2 = doctorData.getTao().get(1).getDacu66_id();
                if (iscu2.equals("1")) {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }
            }

            String isMore = doctorData.getLook();
//            if (isMore.length() > 0) {
//                viewHolder.docMoreTaoTv.setVisibility(View.VISIBLE);
//                viewHolder.docMoreTaoTv.setText(isMore);
//
//            } else {
//                viewHolder.docMoreTaoTv.setVisibility(View.GONE);
//            }

        } else {

            viewHolder.docTao1Content.setVisibility(View.GONE);
            viewHolder.docTao2Content.setVisibility(View.GONE);

//            viewHolder.docMoreTaoTv.setVisibility(View.GONE);
        }

        convertView.setBackgroundResource(R.color.white);
        return convertView;
    }


    class MyAdapterListener implements View.OnClickListener {

        private int position;
        private int posi;

        public MyAdapterListener(int pos, int pos_i) {
            position = pos;
            posi = pos_i;
        }

        @Override
        public void onClick(View v) {

            Intent it1 = new Intent();
            it1.putExtra("id", mDoctorData.get(position).getTao().get(posi).getTao_id());
            it1.putExtra("source", "0");
            it1.putExtra("objid", "0");
            it1.setClass(mContext, TaoDetailActivity.class);
            mContext.startActivity(it1);
        }
    }


    public void add(List<HosListData> infos) {
        mDoctorData.addAll(infos);
    }
}
