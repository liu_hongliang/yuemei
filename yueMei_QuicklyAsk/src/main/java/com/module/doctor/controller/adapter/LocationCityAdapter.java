package com.module.doctor.controller.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.doctor.model.bean.RecentVisitCityData;
import com.quicklyask.activity.R;


import java.util.List;

/**
 * 定位城市
 */
public class LocationCityAdapter extends BaseAdapter {

    private final String TAG = "LocationCityAdapter";

    private Context mContext;
    private LayoutInflater inflater;
    private String mIsShowLocation;
    private int mItemWidth;
    private String mProvinceLocation;
    List<RecentVisitCityData> mList;
    ViewHolder viewHolder;

    public LocationCityAdapter(Context context, List<RecentVisitCityData> list, int itemWidth, String isShowLocation, String provinceLocation) {
        this.mContext = context;
        this.mList = list;
        this.mIsShowLocation = isShowLocation;
        this.mItemWidth = itemWidth;
        this.mProvinceLocation = provinceLocation;
        //去除和定位城市相同的城市
        if (!TextUtils.isEmpty(mProvinceLocation) && !mProvinceLocation.equals("失败")) {
            //倒叙遍历 或者
            for (int i = mList.size() - 1; i >= 0; i--) {
                if (mList.get(i).getName().equals(mProvinceLocation)) {
                    mList.remove(i);
                }
            }
        }
        //为了保证一行 最多两个最近访问城市
        if(mList.size() != 0 && mList.size() > 2){
            mList = mList.subList(0,2);
        }
        inflater = LayoutInflater.from(mContext);
    }

    class ViewHolder {
        public TextView groupNameTV;
    }

    @Override
    public int getCount() {
        if (mIsShowLocation.equals("0")) {
            return mList.size();
        } else {
            return 1;
        }
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_pop_home_xiala2, null);
            viewHolder = new ViewHolder();
            viewHolder.groupNameTV = convertView
                    .findViewById(R.id.home_pop_part_name_tv);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) viewHolder.groupNameTV.getLayoutParams();
        linearParams.width = mItemWidth;
        viewHolder.groupNameTV.setLayoutParams(linearParams);

//        if (Utils.getCity().equals(mList.get(position).getName())) {
//            viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_location_ff527f);
//            viewHolder.groupNameTV.setTextColor(Color.parseColor("#FF527F"));
//        } else {
//            viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_location_f6f6f6);
//            viewHolder.groupNameTV.setTextColor(Color.parseColor("#333333"));
//        }
        viewHolder.groupNameTV.setText(mList.get(position).getName());
        return convertView;
    }

}
