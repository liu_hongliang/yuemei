/**
 * 
 */
package com.module.doctor.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.doctor.model.bean.CityDocDataitem;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * 热门城市
 * 
 * @author Robin
 * 
 */
public class HotCityAdapter2 extends BaseAdapter {

	private final String TAG = "HotCityAdapter";

	private List<CityDocDataitem> mGroupData = new ArrayList<CityDocDataitem>();
	private Context mContext;
	private LayoutInflater inflater;
	private CityDocDataitem groupData;
	ViewHolder viewHolder;

	public HotCityAdapter2(Context mContext, List<CityDocDataitem> mGroupData) {
		this.mContext = mContext;
		this.mGroupData = mGroupData;
		inflater = LayoutInflater.from(mContext);
	}

	static class ViewHolder {
		public TextView groupNameTV;
	}

	@Override
	public int getCount() {
		return mGroupData.size();
	}

	@Override
	public Object getItem(int position) {
		return mGroupData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_pop_home_xiala2, null);
			viewHolder = new ViewHolder();
			viewHolder.groupNameTV = convertView
					.findViewById(R.id.home_pop_part_name_tv);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		groupData = mGroupData.get(position);
//		if (Utils.getCity().equals(groupData.getName())) {
//			viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_location_ff527f);
//			viewHolder.groupNameTV.setTextColor(Color.parseColor("#FF527F"));
//		} else {
//			viewHolder.groupNameTV.setBackgroundResource(R.drawable.shape_location_f6f6f6);
//			viewHolder.groupNameTV.setTextColor(Color.parseColor("#333333"));
//		}
		viewHolder.groupNameTV.setText(groupData.getName());
		return convertView;
	}

	public void add(List<CityDocDataitem> infos) {
		mGroupData.addAll(infos);
	}
}
