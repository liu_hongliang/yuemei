/**
 *
 */
package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.BaseWebViewReload;
import com.module.commonview.view.webclient.BaseWebViewTel;
import com.module.community.model.bean.ExposureLoginData;
import com.module.doctor.controller.other.HosDetailWebViewClient;
import com.module.doctor.model.api.HosDetailShareApi;
import com.module.doctor.model.api.HosYuYueApi;
import com.module.doctor.model.bean.HosShareData;
import com.module.doctor.model.bean.HosYuYueData;
import com.module.my.controller.activity.LoginActivity605;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 医院详情页
 *
 * @author Rubin
 */
public class HosDetailActivity extends BaseActivity {

    private final String TAG = "HosDetailActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
    private LinearLayout contentWeb;

    private ScrollWebView docDetWeb;

    private HosDetailActivity mContext;

    public JSONObject obj_http;
    private String hosid;

    // 分享
    private HosShareData mHosShareData;
    private String shareUrl = "";
    private String shareImgUrl = "";
    private String mHos_name;

    @BindView(id = R.id.content_all_ly)
    private LinearLayout allcontent;
    @BindView(id = R.id.wan_beautif_iv)
    private ImageView mImageView;

    private String phone400;// 400电话
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private String url;
    private boolean isLogin = Utils.isLogin();
    private String mSearchHitBoardId;
    private String mTaoId;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_hos_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = HosDetailActivity.this;
        QMUIStatusBarHelper.translucent(mContext);
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.LayoutParams layoutParams = mImageView.getLayoutParams();
        layoutParams.height = statusbarHeight;
        mImageView.setLayoutParams(layoutParams);
        mImageView.setBackgroundColor(ContextCompat.getColor(mContext, R.color._f6));
        QMUIStatusBarHelper.setStatusBarLightMode(mContext);
        Intent it = getIntent();
        String action = it.getAction();
        mTaoId = it.getStringExtra("tao_id");
        //判断是自己app跳转的，还是其他app跳转的
        if (!TextUtils.isEmpty(action) && Intent.ACTION_VIEW.equals(action)) {
            Uri uri = it.getData();
            //获取指定参数值
            if (uri != null) {
                hosid = uri.getQueryParameter("hosid");
            }
        } else {
            hosid = it.getStringExtra("hosid");
            mSearchHitBoardId = it.getStringExtra("search_hit_board_id");
        }

        Log.e(TAG, "hosid = " + hosid);

        ExposureLoginData exposureLoginData = new ExposureLoginData("4", hosid);
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));


        GetHosYuyueData();
        initShareData();


        initWebview();
        url = FinalConstant.HOSPTIPL_DETAIL;
        Log.e(TAG, "url == " + url);
        HashMap<String, Object> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(mSearchHitBoardId)){
            hashMap.put("search_hit_board_id",mSearchHitBoardId);
        }
        if (!TextUtils.isEmpty(mTaoId)){
            hashMap.put("tao_id", mTaoId);
        }
        LodUrl1(url,hashMap);

    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(100, intent);
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public Dialog dialog = null;

    void initShareData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("hosid", hosid);
        new HosDetailShareApi().getCallBack(mContext, maps, new BaseCallBackListener<HosShareData>() {

            @Override
            public void onSuccess(HosShareData hosShareData) {
                mHosShareData = hosShareData;
                mHos_name = mHosShareData.getHos_name();
                shareUrl = mHosShareData.getUrl();
                shareImgUrl = mHosShareData.getImg();
                baseWebViewClientMessage.setParameter5983(hosid, "4", mHos_name, shareImgUrl, shareUrl, "4", hosid);

            }

        });

    }

    void GetHosYuyueData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("hosid", hosid);
        Log.d(TAG, "=============>");
        new HosYuYueApi().getCallBack(mContext, maps, new BaseCallBackListener<HosYuYueData>() {
            @Override
            public void onSuccess(HosYuYueData hosYuyue) {
                Log.d(TAG, "HosYuYueData=============>" + hosYuyue.toString());
                phone400 = hosYuyue.getTel();
                if (phone400.contains(",")) {
                    String[] split = phone400.split(",");
                    phone400 = split[0];
                }
            }

        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {
        docDetWeb = new ScrollWebView(mContext);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext, "1", hosid, docDetWeb);
        baseWebViewClientMessage.setView(allcontent);
        baseWebViewClientMessage.setBaseWebViewReload(new BaseWebViewReload() {
            @Override
            public void reload() {
                webReload();
            }
        });
        baseWebViewClientMessage.setBaseWebViewTel(new BaseWebViewTel() {
            @Override
            public void tel(WebView view, String url) {
                telPhone(url);
            }
        });

        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        //设置 缓存模式
        docDetWeb.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        docDetWeb.getSettings().setDomStorageEnabled(true);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.getSettings().supportMultipleWindows();
        docDetWeb.getSettings().setNeedInitialFocus(true);
        docDetWeb.getSettings().setSupportZoom(true); // 可以缩放
        docDetWeb.getSettings().setLoadWithOverviewMode(true);// 缩放至屏幕的大小
        docDetWeb.getSettings().setUseWideViewPort(true);// 将图片调整到适合webview大小
        docDetWeb.getSettings().setBuiltInZoomControls(false);// 设置支持缩放
        docDetWeb.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN); //支持内容重新布局
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                String[] strs = message.split("\n");
                Log.e(TAG, "url11 == " + url);
                Log.e(TAG, "message111 == " + message);
                Log.e(TAG, "result111 == " + result);
                showDialogExitEdit("确定", message, result, strs.length);
                return true;
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                String[] strs = message.split("\n");
                Log.e(TAG, "url == " + url);
                Log.e(TAG, "message == " + message);
                Log.e(TAG, "result == " + result);
                Log.e(TAG, "strs == " + strs);
                showDialogExitEdit2(message, result, strs.length);
                return true;
            }

        });
        contentWeb.addView(docDetWeb);


        WebSettings settings = docDetWeb.getSettings();
        // User settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
        }
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放

        CookieManager.getInstance().setAcceptCookie(true);

        if (Build.VERSION.SDK_INT > 8) {
            settings.setPluginState(WebSettings.PluginState.ON_DEMAND);
        }
        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);

        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);
        docDetWeb.setOnScrollChangeListener(new ScrollWebView.OnScrollChangeListener() {

            @Override
            public void onPageEnd(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onPageTop(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
            }
        });

    }


    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit(String title, String content, final JsResult result, int num) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);

        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                result.confirm();       //关闭js的弹窗
                dialog.dismiss();
            }
        });
    }

    void showDialogExitEdit2(final String content, final JsResult result, final int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dialog_edit_exit2);

        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确定");
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Log.e(TAG, "aaaaa");
                Toast.makeText(HosDetailActivity.this, "取关成功", Toast.LENGTH_SHORT).show();
                result.confirm();
                dialog.dismiss();
            }
        });

        Button cancelBt99 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setText("取消");
        cancelBt99.setTextColor(0xff1E90FF);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "bbbbb");
                result.cancel();
                dialog.dismiss();
            }
        });

    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }


    /**
     * 加载web
     */
    public void LodUrl1(String url, HashMap<String, Object> hashMap) {
        hashMap.put("hosid", hosid);
        baseWebViewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, hashMap);
        if (null != docDetWeb) {
            Log.d(TAG, "url:==" + addressAndHead.getUrl());
            docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isLogin) {
            if (!Utils.isLogin()) {
                LodUrl1(url, new HashMap<String, Object>());
                isLogin = Utils.isLogin();
            }
        } else {
            if (Utils.isLogin()) {
                LodUrl1(url, new HashMap<String, Object>());
                isLogin = Utils.isLogin();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void telPhone(String url) {
        String aa = url.replace("tel:", "");
        ViewInject.toast("正在拨打中·····");
        Intent it = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + aa));
        try {
            startActivity(it);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 100 && resultCode == LoginActivity605.REQUEST_CODE) {
            LodUrl1(url, new HashMap<String, Object>());
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        super.onDestroy();
    }
}
