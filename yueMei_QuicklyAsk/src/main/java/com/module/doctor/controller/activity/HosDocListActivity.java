/**
 * 
 */
package com.module.doctor.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;

/**
 * 医院医生列表
 * 
 * @author Rubin
 * 
 */
public class HosDocListActivity extends BaseActivity {

	private final String TAG = "HosDocListActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview4, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.wan_beautiful_web_back, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private HosDocListActivity mContex;

	public JSONObject obj_http;
	private String uid;
	private String url;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_hos_doc_list);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = HosDocListActivity.this;
		uid = Utils.getUid();

		Intent it = getIntent();
		url = it.getStringExtra("url");
		url = FinalConstant.baseUrl + FinalConstant.VER + url;
		scollwebView.GetLinearLayout(contentWeb);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();

		LodUrl1(url);

		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						HosDocListActivity.this.finish();
					}
				});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}


	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}


	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 * 
	 * @param urlStr
	 */
	@SuppressLint("NewApi")
	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 医生详情页
				try {
					String id = obj.getString("id");
					String docname = URLDecoder.decode(
							obj.getString("docname"), "utf-8");
					Intent it = new Intent();
					it.setClass(mContex, DoctorDetailsActivity592.class);
					it.putExtra("docId", id);
					it.putExtra("docName", docname);
					it.putExtra("partId", "");
					startActivity(it);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.wan_beautiful_web_back:
			onBackPressed();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
