package com.module.doctor.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.community.model.bean.BBsListData550;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;

import org.kymjs.aframe.ui.ViewInject;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dwb on 17/2/23.
 */

public class DocDiaryListAdapter extends BaseAdapter {

    private final String TAG = "DocDiaryListAdapter";

    private List<BBsListData550> mHotIssues = new ArrayList<BBsListData550>();
    private Context mContext;
    private LayoutInflater inflater;
    private BBsListData550 hotIsData;
    ViewHolder viewHolder;

    private Animation ain;
    ImageOptions imageOptions;
    ImageOptions imageOptions1;

    private int windowsWight;

    public DocDiaryListAdapter(Context mContext, List<BBsListData550> mHotIssues) {
        this.mContext = mContext;
        this.mHotIssues = mHotIssues;
        Log.e("TAG", "mHotIssues == " + mHotIssues.size());
        inflater = LayoutInflater.from(mContext);

        ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);

        imageOptions1 = new ImageOptions.Builder()
                .setCircular(true)
                .setPlaceholderScaleType(ImageView.ScaleType.FIT_XY)
                .setConfig(Bitmap.Config.RGB_565)
                .setAnimation(ain)
                .setUseMemCache(true)
                .build();

        imageOptions = new ImageOptions.Builder()
                // 加载中或错误图片的ScaleType
                .setPlaceholderScaleType(ImageView.ScaleType.FIT_XY)
                .setConfig(Bitmap.Config.RGB_565)
                .setAnimation(ain)
//				.setImageScaleType(ImageView.ScaleType.FIT_XY)
                .setUseMemCache(true)
                .build();
    }


    static class ViewHolder {
        public ImageView mBBsHeadPic;

        public TextView mBBsName;
        public TextView mBBsTitle;

        public LinearLayout mSingalLy;
        public ImageView mSingalPic;

        public LinearLayout mDuozhangLy;
        public ImageView mBBsIv1;
        public ImageView mBBsIv2;

        public TextView mCommentNum;
        public TextView mSeenum;
        public int flag;

        public ImageView mBBsPicRuleIv1;
        public ImageView mBBsPicRuleIv2;

        public LinearLayout mTagLy;
        public TextView mTag;
        public LinearLayout tagLy;
        public TextView mPrice;
    }

    @Override
    public int getCount() {
        return mHotIssues.size();
    }

    @Override
    public Object getItem(int position) {
        return mHotIssues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({ "NewApi", "InlinedApi", "InflateParams" })
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        hotIsData = mHotIssues.get(position);

        if (convertView == null|| ((ViewHolder) convertView.getTag()).flag != position) {

            convertView = inflater.inflate(R.layout.item_diary_docdetail, null);

            viewHolder = new ViewHolder();
            viewHolder.flag = position;

            viewHolder.mBBsHeadPic = convertView
                    .findViewById(R.id.bbs_list_head_image_iv);
            viewHolder.mBBsName = convertView
                    .findViewById(R.id.bbs_list_name_tv);
            viewHolder.mBBsTitle = convertView
                    .findViewById(R.id.bbs_list_title_tv);

            viewHolder.mDuozhangLy = convertView
                    .findViewById(R.id.bbs_list_duozhang_ly);
            viewHolder.mBBsIv1= convertView
                    .findViewById(R.id.bbs_list_duozhang_iv1);
            viewHolder.mBBsIv2= convertView
                    .findViewById(R.id.bbs_list_duozhang_iv2);


            viewHolder.mSingalLy = convertView
                    .findViewById(R.id.bbs_list_pic_danzhang_ly);
            viewHolder.mSingalPic = convertView
                    .findViewById(R.id.bbs_list_pic_danzhang);

            viewHolder.mSeenum = convertView
                    .findViewById(R.id.bbs_list_see_num_tv);
            viewHolder.mCommentNum = convertView
                    .findViewById(R.id.bbs_list_comment_num_tv);

            viewHolder.mBBsPicRuleIv1= convertView
                    .findViewById(R.id.bbs_list_picrule1);
            viewHolder.mBBsPicRuleIv2= convertView
                    .findViewById(R.id.bbs_list_picrule2);

            viewHolder.mTagLy = convertView
                    .findViewById(R.id.bbs_list_tag_ly);

            viewHolder.mTag = convertView
                    .findViewById(R.id.bbs_list_tag_tv);
            viewHolder.tagLy = convertView
                    .findViewById(R.id.tag_ly_550);
            viewHolder.mPrice = convertView
                    .findViewById(R.id.bbs_list_price_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        x.image().bind(viewHolder.mBBsHeadPic,hotIsData.getUser_img(),imageOptions1);

        viewHolder.mBBsName.setText(hotIsData.getUser_name());
        viewHolder.mBBsTitle.setText(hotIsData.getTitle());
        viewHolder.mCommentNum.setText(hotIsData.getAnswer_num());

        if (null != hotIsData.getView_num()) {
            viewHolder.mSeenum.setText(hotIsData.getView_num());
        }

        String picRule=hotIsData.getPicRule();
        if(picRule.equals("1")){
            viewHolder.mBBsPicRuleIv1.setVisibility(View.VISIBLE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.VISIBLE);
        }else {
            viewHolder.mBBsPicRuleIv1.setVisibility(View.GONE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.GONE);
        }


        // 标签
        if (null != hotIsData.getTag()) {
            if (hotIsData.getTag().size() > 0) {
                viewHolder.tagLy.setVisibility(View.VISIBLE);
                viewHolder.mTagLy.setVisibility(View.VISIBLE);
                int tagsize = hotIsData.getTag().size();
                String tagStr = "";
                for (int i = 0; i < tagsize; i++) {
                    tagStr = hotIsData.getTag().get(i).getName()+"  " +tagStr;
                }
                viewHolder.mTag.setText(tagStr);
            } else {
                viewHolder.mTagLy.setVisibility(View.GONE);
                viewHolder.tagLy.setVisibility(View.GONE);
            }
        } else {
            viewHolder.mTagLy.setVisibility(View.GONE);
            viewHolder.tagLy.setVisibility(View.GONE);
        }
        // 标签


        if (null != hotIsData.getPrice() && !hotIsData.getPrice().equals("0")
                && hotIsData.getPrice().length() > 0) {
            viewHolder.tagLy.setVisibility(View.VISIBLE);
            viewHolder.mPrice.setText("（￥" + hotIsData.getPrice()+"）");
        } else {
            viewHolder.mPrice.setVisibility(View.GONE);
            if (null != hotIsData.getTag()) {
                if (hotIsData.getTag().size() > 0) {
                    viewHolder.tagLy.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.tagLy.setVisibility(View.GONE);
                }
            }else {
                viewHolder.tagLy.setVisibility(View.GONE);
            }
        }


        // 图片

        if(null!=hotIsData.getPic()) {

            if (hotIsData.getPic().size() > 0) {
                if (hotIsData.getPic().size() == 1) {//单张图片的时候

                    viewHolder.mDuozhangLy.setVisibility(View.GONE);
                    viewHolder.mSingalLy.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = viewHolder.mSingalLy.getLayoutParams();
                    params.height = (windowsWight-40) *340 / 710;
                    viewHolder.mSingalLy.setLayoutParams(params);

                    try {
                        x.image().bind(viewHolder.mSingalPic,hotIsData.getPic().get(0).getImg(),imageOptions);
                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }
                } else if (hotIsData.getPic().size() > 1) {//两张对比图

                    viewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    viewHolder.mSingalLy.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = viewHolder.mDuozhangLy.getLayoutParams();
                    params.height = (windowsWight-40)  / 2;
                    viewHolder.mDuozhangLy.setLayoutParams(params);

                    try {
                        x.image().bind(viewHolder.mBBsIv1,hotIsData.getPic().get(0).getImg(),imageOptions);
                        x.image().bind(viewHolder.mBBsIv2,hotIsData.getPic().get(1).getImg(),imageOptions);

                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }
                }
            } else {
                viewHolder.mDuozhangLy.setVisibility(View.GONE);
                viewHolder.mSingalLy.setVisibility(View.GONE);
            }

        }else {
            viewHolder.mDuozhangLy.setVisibility(View.GONE);
            viewHolder.mSingalLy.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void add(List<BBsListData550> infos) {
        mHotIssues.addAll(infos);
    }
}