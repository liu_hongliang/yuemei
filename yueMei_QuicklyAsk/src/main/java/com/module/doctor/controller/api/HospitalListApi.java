package com.module.doctor.controller.api;

import android.content.Context;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.doctor.model.bean.HosListData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.ArrayList;
import java.util.Map;

/**
 * 医院列表的api
 * Created by 裴成浩 on 2018/3/13.
 */

public class HospitalListApi implements BaseCallBackApi {
    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.HOSPITAL, "list", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if("1".equals(mData.code)){
                    ArrayList<HosListData> docData = JSONUtil.jsonToArrayList(mData.data,HosListData.class);
                    listener.onSuccess(docData);
                }
            }
        });
    }
}
