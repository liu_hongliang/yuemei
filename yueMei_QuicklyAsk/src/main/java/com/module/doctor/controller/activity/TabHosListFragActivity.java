/**
 *
 */
package com.module.doctor.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseProjectPopupwindows2;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.adapter.HosListAdpter;
import com.module.doctor.controller.api.HospitalListApi;
import com.module.doctor.model.api.PartDataApi;
import com.module.doctor.model.bean.HosListData;
import com.module.doctor.model.bean.PartAskData;
import com.module.home.model.bean.ProjectDetailsListData;
import com.module.home.view.LoadingProgress;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Location;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.quicklyask.view.DropDownListView.OnDropDownListener;
import com.quicklyask.view.EditExitDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.utils.Log;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * 医院列表
 *
 * @author Rubin
 */
public class TabHosListFragActivity extends FragmentActivity {

    private final String TAG = "TabHosListFragActivity";

    private TabHosListFragActivity mContex;

    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;

    private List<HosListData> lvHotIssueData1 = new ArrayList<>();
    private List<HosListData> lvHotIssueMoreData1 = new ArrayList<>();
    private HosListAdpter hotAdpter1;

    private LinearLayout nodataTv;// 数据为空时候的显示

    private RelativeLayout cityRly;
    private RelativeLayout partRly;
    private RelativeLayout ifpublicRly;
    private RelativeLayout sortRly;
    private TextView cityTv;
    private TextView partTv;
    private TextView ifpublicTv;
    private TextView sortTv;
    private ImageView cityIv;
    private ImageView partIv;
    private ImageView ifpublicIv;
    private ImageView sortIv;


    private List<TaoPopItemData> lvSortData = new ArrayList<>();
    private List<TaoPopItemData> lvifpublicData = new ArrayList<>();


    private BaseProjectPopupwindows2 projectPop;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private BaseSortPopupwindows ifPublicPop;

    private String partId = "0";
    private String sortStr;// 排序
    private String ifpublicStr;

    public static int mPosition;

    private LinearLayout nowifiLy;
    private Button refreshBt;

    private LocationManager lm;
    private List<PartAskData> lvGroupData;
    private int GPS_REQUEST_CODE = 100;
    private int sortPos = 0;
    private LoadingProgress mDialog;
    private HospitalListApi hospitalListApi;
    private HashMap<String, Object> hospitalListMap = new HashMap<>();

    private String provinceLocation;
    private LocationClient locationClient;
    private Location location;
    private ProjectDetailsListData screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_tab_hos_select);
        mContex = TabHosListFragActivity.this;
        mDialog = new LoadingProgress(mContex);
        hospitalListApi = new HospitalListApi();

        findView();
        initList();
        setListner();

        screen = getIntent().getParcelableExtra("screen");
        if (screen != null) {
            partId = screen.getId();
            partTv.setText(screen.getName());
        }

        sortStr = "1";
        ifpublicStr = "0";

        cityTv.setText(Utils.getCity());
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

    }

    @Override
    protected void onResume() {
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        if (SystemTool.checkNet(mContex)) {
            nowifiLy.setVisibility(View.GONE);
        } else {
            nowifiLy.setVisibility(View.VISIBLE);
        }

        refreshBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (SystemTool.checkNet(mContex)) {
                            nowifiLy.setVisibility(View.GONE);
                            initList();
                            setListner();

                        } else {
                            nowifiLy.setVisibility(View.VISIBLE);
                            ViewInject.toast(getResources().getString(R.string.no_wifi_tips));
                        }
                    }
                }.start();
            }
        });
        super.onResume();
    }

    void findView() {
        mlist = findViewById(R.id.my_hos_list_view);

        partRly = findViewById(R.id.project_part_pop_rly);
        ifpublicRly = findViewById(R.id.project_shaixuan_pop_rly);
        sortRly = findViewById(R.id.project_sort_pop_rly);
        cityRly = findViewById(R.id.project_diqu_pop_rly);
        partTv = findViewById(R.id.project_part_pop_tv);
        ifpublicTv = findViewById(R.id.project_shaixuan_pop_tv);
        sortTv = findViewById(R.id.project_sort_pop_tv);
        partIv = findViewById(R.id.project_part_pop_iv);
        ifpublicIv = findViewById(R.id.project_shaixuan_pop_iv);
        sortIv = findViewById(R.id.project_sort_pop_iv);
        cityTv = findViewById(R.id.project_diqu_pop_tv);
        cityIv = findViewById(R.id.project_diqu_pop_iv);
        nowifiLy = findViewById(R.id.no_wifi_ly1);
        refreshBt = findViewById(R.id.refresh_bt);
        nodataTv = findViewById(R.id.my_collect_post_tv_nodata);

        setPopData();

        cityPop = new BaseCityPopwindows(mContex, partRly);
        projectPop = new BaseProjectPopupwindows2(mContex, partRly, lvGroupData);
        sortPop = new BaseSortPopupwindows(mContex, partRly, lvSortData);
        ifPublicPop = new BaseSortPopupwindows(mContex, partRly, lvifpublicData);

        //城市点击回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                cityTv.setText(city);

                Cfg.saveStr(TabHosListFragActivity.this.mContex, FinalConstant.DWCITY, city);

                onreshData();

                cityPop.dismiss();
                initpop();
            }
        });

        //智能排序点击回调
        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                sortPos = pos;
                if (sortPos == 1) {
                    boolean gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    boolean network = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    Log.e(TAG, "gps == " + gps);
                    Log.e(TAG, "network == " + network);
                    if (gps || network) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Acp.getInstance(TabHosListFragActivity.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                                @Override
                                public void onGranted() {
                                    initLocation();
                                }
                                @Override
                                public void onDenied(List<String> permissions) {
                                    sortPop.dismiss();
                                }
                            });
                        } else {
                            initLocation();
                        }
                    } else {
                        sortPop.dismiss();
                        showDialogExitEdit3();
                    }
                } else {
                    intelligentSorting(sortPos);
                }
            }
        });

        //筛选点击回调
        ifPublicPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                ifPublicPop.setAdapter(pos);
                ifpublicStr = lvifpublicData.get(pos).get_id();
                ifpublicTv.setText(lvifpublicData.get(pos).getName());
                ifPublicPop.dismiss();
                initpop();
                onreshData();
            }
        });

        loadPartList();
    }

    /**
     * 设置智能排序和筛选所需要的数据
     */
    private void setPopData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("2");
        a2.setName("离我最近");
        lvSortData.add(a1);
        lvSortData.add(a2);

        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("0");
        a3.setName("不限");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("1");
        a4.setName("公立");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("2");
        a5.setName("民营");
        lvifpublicData.add(a3);
        lvifpublicData.add(a4);
        lvifpublicData.add(a5);
    }

    /**
     * 获取项目的接口
     */
    void loadPartList() {
        mPosition = 0;
        new PartDataApi().getCallBack(mContex, new HashMap<String, Object>(), new BaseCallBackListener<List<PartAskData>>() {
            @Override
            public void onSuccess(List<PartAskData> partAskData) {
                lvGroupData = partAskData;
                projectPop.setmData(lvGroupData);
                projectPop.setLeftView();
                projectPop.setRightView(projectPop.getmOnePos());

                projectPop.setOnItemSelectedClickListener(new BaseProjectPopupwindows2.OnItemSelectedClickListener() {
                    @Override
                    public void onItemSelectedClick(String id, String name) {
                        partId = id;
                        if (!TextUtils.isEmpty(name)) {
                            partTv.setText(name);
                        }
                        initpop();
                        onreshData();
                    }
                });

                projectPop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss() {
                        initpop();
                    }
                });
            }

        });
    }

    void initpop() {
        if (projectPop.isShowing()) {
            partTv.setTextColor(Color.parseColor("#E95165"));
            partIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            partTv.setTextColor(Color.parseColor("#414141"));
            partIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (sortPop.isShowing()) {
            sortTv.setTextColor(Color.parseColor("#E95165"));
            sortIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            sortTv.setTextColor(Color.parseColor("#414141"));
            sortIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (ifPublicPop.isShowing()) {
            ifpublicTv.setTextColor(Color.parseColor("#E95165"));
            ifpublicIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            ifpublicTv.setTextColor(Color.parseColor("#414141"));
            ifpublicIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (cityPop.isShowing()) {
            cityTv.setTextColor(Color.parseColor("#E95165"));
            cityIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            cityTv.setTextColor(Color.parseColor("#414141"));
            cityIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }

    }

    void setListner() {

        sortPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        ifPublicPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        cityPop.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        projectPop.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                initpop();
            }
        });

        ifpublicRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (ifPublicPop.isShowing()) {
                    ifPublicPop.dismiss();
                } else {
                    ifPublicPop.showPop();
                }
                initpop();
            }
        });

        sortRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (sortPop.isShowing()) {
                    sortPop.dismiss();
                } else {
                    sortPop.showPop();
                }
                initpop();
            }
        });


        cityRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (cityPop.isShowing()) {
                    cityPop.dismiss();
                } else {
                    cityPop.showPop();
                }
                initpop();
            }
        });

        partRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (projectPop.isShowing()) {
                    projectPop.dismiss();
                } else {
                    projectPop.showPop();
                }
                initpop();
            }
        });
    }

    /**
     * dialog提示
     */
    void showDialogExitEdit3() {
        final EditExitDialog editDialog = new EditExitDialog(mContex.getParent(), R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        Log.e(TAG, "editDialog == " + editDialog);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setVisibility(View.VISIBLE);
        titleTv77.setText("您未打开定位权限");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("请允许悦美获取您当前位置，以帮您查询附近医院");
        titleTv88.setHeight(Utils.dip2px(mContex, 35));

        LinearLayout llFengexian = editDialog.findViewById(R.id.ll_fengexian);
        llFengexian.setVisibility(View.VISIBLE);


        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("去设置");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // 转到手机设置界面，用户设置GPS
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, GPS_REQUEST_CODE); // 设置完成后返回到原来的界面

                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setVisibility(View.VISIBLE);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    private void intelligentSorting(int pos) {
        sortPop.setAdapter(pos);
        sortStr = lvSortData.get(pos).get_id();

        sortTv.setText(lvSortData.get(pos).getName());
        sortPop.dismiss();
        initpop();
        onreshData();
    }

    void onreshData() {
        lvHotIssueData1 = null;
        lvHotIssueMoreData1 = null;
        mCurPage = 1;
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }

    void initList() {

        mHandler = getHandler();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvHotIssueData1 = null;
                lvHotIssueMoreData1 = null;
                mCurPage = 1;
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if(lvHotIssueData1 != null){
                    String hosid = lvHotIssueData1.get(pos).getHos_id();
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "hospital|list_" + Utils.getCity() + "_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, lvHotIssueData1.get(pos).getBmsid(), "1"), lvHotIssueData1.get(pos).getEvent_params());
                    Intent it = new Intent();
                    it.setClass(mContex, HosDetailActivity.class);
                    it.putExtra("hosid", hosid);
                    startActivity(it);
                }
            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {
        mDialog.startLoading();
        Log.e(TAG, "1111 === " + Utils.getCity());
        hospitalListMap.put("cateid", partId);
        hospitalListMap.put("page", mCurPage + "");
        hospitalListMap.put("sort", sortStr);
        hospitalListMap.put("kind", ifpublicStr);

        hospitalListApi.getCallBack(mContex, hospitalListMap, new BaseCallBackListener<List<HosListData>>() {
            @Override
            public void onSuccess(List<HosListData> hosListDatas) {
                Log.e(TAG, "hosListDatas === " + hosListDatas.size());
                Log.e(TAG, "hosListDatas === " + hosListDatas.toString());
                mDialog.stopLoading();
                Message msg;
                if (isDonwn) {
                    if (mCurPage == 1) {
                        lvHotIssueData1 = hosListDatas;
                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    lvHotIssueMoreData1 = hosListDatas;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }

                mCurPage++;
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (lvHotIssueData1 != null && lvHotIssueData1.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);

                            hotAdpter1 = new HosListAdpter(TabHosListFragActivity.this, lvHotIssueData1);
                            mlist.setAdapter(hotAdpter1);

                            mlist.onBottomComplete();
                        } else {
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (lvHotIssueMoreData1 != null && lvHotIssueMoreData1.size() > 0) {

                            hotAdpter1.add(lvHotIssueMoreData1);
                            hotAdpter1.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GPS_REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                Acp.getInstance(TabHosListFragActivity.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        initLocation();
                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        sortPop.dismiss();
                    }
                });
            } else {
                initLocation();
            }
        }
    }

    private void initLocation() {
        try {
            locationClient = new LocationClient(getApplicationContext());
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(new TabHosListFragActivity.MyBDLocationListener());
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();
            // 请求定位

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定位结果的响应函数
     *
     * @author
     */
    public class MyBDLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            // 设置editTextLocation为返回的百度地图获取到的街道信息
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                location = new Location();
                location.setAddress(bdLocation.getAddrStr());

                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);

                intelligentSorting(sortPos);
                String ss = bdLocation.getCity();

                if (ss != null && ss.length() > 1) {

                    if (ss.contains("省")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("市")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("自治区")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }

                    Cfg.saveStr(TabHosListFragActivity.this, "city_dingwei", provinceLocation);

                    Cfg.saveStr(TabHosListFragActivity.this, FinalConstant.TAOCITY, provinceLocation);

                    Cfg.saveStr(TabHosListFragActivity.this, FinalConstant.DWCITY, provinceLocation);

                    locationClient.unRegisterLocationListener(new TabHosListFragActivity.MyBDLocationListener());

                } else {
                    provinceLocation = "失败";
                    Cfg.saveStr(TabHosListFragActivity.this, "city_dingwei", "全国");

                    Cfg.saveStr(TabHosListFragActivity.this, FinalConstant.TAOCITY, "全国");

                    Cfg.saveStr(TabHosListFragActivity.this, FinalConstant.DWCITY, "全国");

                    locationClient.unRegisterLocationListener(new TabHosListFragActivity.MyBDLocationListener());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
