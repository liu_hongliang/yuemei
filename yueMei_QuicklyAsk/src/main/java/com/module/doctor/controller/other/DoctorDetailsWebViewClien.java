package com.module.doctor.controller.other;

import android.content.Intent;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.AboutDocBbsListActivity;
import com.module.doctor.controller.activity.CaseFinalPageActivity;
import com.module.doctor.controller.activity.CaseListActivity;
import com.module.doctor.controller.activity.DocBbsListActivity;
import com.module.doctor.controller.activity.DocQueListWebActivity;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.WantBeautifulActivity548;
import com.module.doctor.model.api.CaseDetailApi;
import com.module.my.controller.activity.TypeProblemActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 裴成浩 on 2018/1/9.
 */

public class DoctorDetailsWebViewClien implements BaseWebViewClientCallback {

    private String partId;
    private String docName;
    private String docId;
    private Intent intent;
    private DoctorDetailsActivity592 mActivity;

    public DoctorDetailsWebViewClien(DoctorDetailsActivity592 activity,String partId,String docName,String docId) {
        this.mActivity = activity;
        this.partId = partId;
        this.docName = docName;
        this.docId = docId;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws JSONException {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws JSONException {
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页

                String id1 = obj.getString("id");
                intent.putExtra("docId", id1);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                mActivity.startActivity(intent);

                break;
            case "2":   //跳到提问帖

                mActivity.startActivity(new Intent(mActivity, TypeProblemActivity.class));

                break;
            case "3":   // 跳到预约
                String po3 = obj.getString("po");
                String hosid3 = obj.getString("hosid");
                String docid3 = obj.getString("docid");
                String shareid3 = "0";

                intent.setClass(mActivity, WantBeautifulActivity548.class);
                intent.putExtra("po", po3);
                intent.putExtra("hosid", hosid3);
                intent.putExtra("docid", docid3);
                intent.putExtra("shareid", shareid3);
                intent.putExtra("cityId", "0");
                intent.putExtra("partId", "0");
                intent.putExtra("partId_two", "0");
                mActivity.startActivity(intent);
                break;
            case "4"://案例详情

                final String id4 = obj.getString("id");
                final String hosid4 = obj.getString("hosid");
                Map<String, Object> maps = new HashMap<>();
                maps.put("id", id4);
                new CaseDetailApi().getCallBack(mActivity, maps, new BaseCallBackListener<String>() {
                    @Override
                    public void onSuccess(String s) {
                        Intent it = new Intent();

                        it.setClass(mActivity, CaseFinalPageActivity.class);
                        it.putExtra("casefinaljson", s);
                        it.putExtra("id", id4);
                        it.putExtra("hosid", hosid4);
                        mActivity.startActivity(it);
                    }

                });

                break;
            case "5": //案例列表
                String link5 = obj.getString("link");

                intent.setClass(mActivity, CaseListActivity.class);
                intent.putExtra("link", link5);
                mActivity.startActivity(intent);
                break;


            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;
            case "7":  //问答列表

                String link7 = obj.getString("link");

                intent.setClass(mActivity, DocQueListWebActivity.class);
                intent.putExtra("type", "1");
                intent.putExtra("link", link7);
                mActivity.startActivity(intent);
                break;

            case "5431" :   // 医生的帖子

                intent.putExtra("docname", docName);
                intent.putExtra("docid", docId);
                intent.setClass(mActivity, DocBbsListActivity.class);
                mActivity.startActivity(intent);

                break;

            case "5432" :   //关于医生的帖子
                intent.putExtra("docname", docName);
                intent.putExtra("docid", docId);
                intent.setClass(mActivity, AboutDocBbsListActivity.class);
                mActivity.startActivity(intent);
                break;
        }
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

}
