package com.module.doctor.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;


/**
 * Created by 裴成浩 on 2019/8/14
 */
public class StaScoreBar extends FrameLayout {
    private final Context mContext;
    private ProgressBar progressBar;
    public StaScoreBar(Context context) {
        this(context, null);
    }

    public StaScoreBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StaScoreBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void initView() {

        //进度条
        progressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setProgressDrawable(Utils.getLocalDrawable(mContext,R.drawable.shape_sta_score_bar_progress));
        progressBar.setMax(100);

        //星级评分图
        ImageView imageView = new ImageView(mContext);
        imageView.setBackgroundResource(R.drawable.shape_sta_score_bar_src);

        LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(progressBar,lp);
        addView(imageView,lp);


    }

    /**
     * 设置进度
     */
    public void setProgressBar(int progress) {
        if (progressBar != null) {
            progressBar.setProgress(progress);
        }
    }
}