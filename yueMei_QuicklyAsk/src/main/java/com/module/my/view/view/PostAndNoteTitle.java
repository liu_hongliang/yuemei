package com.module.my.view.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.lang.reflect.Field;

/**
 * 日记本帖子标题
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostAndNoteTitle extends LinearLayout {

    private String TAG = "PostAndNoteTitle";
    private Context mContext;
    private EditText mEditText;
    private TextView mTextView;
    private PostAndNoteContentEditText contentEditText;
    private ClickCallBack clickCallBack;
    public static final String EditTitle1 = "标题：清晰的描述您的疑问6~25字";
    public static final String EditTitle2 = "为你的日记起一个响亮的名字吧~";
    public static final String EditTitle3 = "请在这里输入4~25个字的标题";
    private int minLength;

    public PostAndNoteTitle(Context context) {
        this(context, null);
    }

    public PostAndNoteTitle(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostAndNoteTitle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        initView();
        initCallback();
    }


    /**
     * 设置样式
     */
    @SuppressLint("SetTextI18n")
    private void initView() {
        setOrientation(VERTICAL);
        setTitleSelected(true);    //默认不获取焦点

        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(HORIZONTAL);
        LayoutParams linearLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(linearLayoutParams);

        //标题输入框
        mEditText = new EditText(mContext);
        setMaxLength(25);          //设置最大长度
        mEditText.setGravity(Gravity.TOP);
        mEditText.setBackground(null);
        setCursorColor();    //设置光标颜色
        mEditText.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        mEditText.setTextSize(16);
        mEditText.setHintTextColor(Utils.getLocalColor(mContext, R.color._bb));
        mEditText.setClickable(true);
        mEditText.setSingleLine();
        mEditText.setPadding(Utils.dip2px(5), Utils.dip2px(5), Utils.dip2px(5), Utils.dip2px(5));
        LayoutParams editTextParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        editTextParams.weight = 1;
        editTextParams.setMargins(Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(10), Utils.dip2px(5));
        mEditText.setLayoutParams(editTextParams);

        //输入的文字
        mTextView = new TextView(mContext);
        mTextView.setTextColor(Utils.getLocalColor(mContext, R.color._bb));
        mTextView.setTextSize(14);
        mTextView.setText(getTitleLength() + "/" + getMaxLength());
        LayoutParams textViewParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textViewParams.rightMargin = Utils.dip2px(15);
        textViewParams.gravity = Gravity.CENTER;
        mTextView.setLayoutParams(textViewParams);

        linearLayout.addView(mEditText);
        linearLayout.addView(mTextView);

        //设置分割线
        View line = new View(mContext);
        line.setBackgroundColor(Utils.getLocalColor(mContext, R.color.light_gray_7));
        LayoutParams lineParams = new LayoutParams(LayoutParams.MATCH_PARENT, Utils.dip2px(1));
        lineParams.setMargins(Utils.dip2px(15), 0, Utils.dip2px(15), 0);
        line.setLayoutParams(lineParams);

        //设置子布局
        addView(linearLayout);
        addView(line);
    }

    /**
     * 初始化回调
     */
    private void initCallback() {
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
                if (contentEditText != null) {
                    if (contentEditText.getContentLength() >= contentEditText.getMinLength() && getTitleLength() >= getMinLength()) {
                        if (clickCallBack != null) {
                            clickCallBack.onReleaseClick(true);
                        }
                    } else {
                        if (clickCallBack != null) {
                            clickCallBack.onReleaseClick(false);
                        }
                    }
                }

                mTextView.setText(getTitleLength() + "/" + getMaxLength());
            }
        });

        //标题编辑框点击
        mEditText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCallBack != null) {
                    clickCallBack.onTitleEditorClick(v);
                }
            }
        });

    }

    /**
     * 修改光标的颜色（反射）
     */
    private void setCursorColor() {
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(mEditText, R.drawable.editext_cursor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取标题
     *
     * @return
     */
    public String getTitle() {
        return mEditText.getText().toString().trim();
    }

    /**
     * 获取标题长度
     *
     * @return
     */
    public int getTitleLength() {
        return mEditText.getText().toString().trim().length();
    }

    /**
     * 设置提示语
     *
     * @param hint
     */
    public void setTitleHint(String hint) {
        mEditText.setHint(hint);
    }

    /**
     * 设置帖子内容编辑框
     *
     * @param contentEditText
     */
    public void setContenteEditText(PostAndNoteContentEditText contentEditText) {
        this.contentEditText = contentEditText;
    }

    /**
     * 标题编辑是否获取焦点
     *
     * @param focusable ：true:获取焦点，flase：失去焦点
     */
    private void setTitleSelected(boolean focusable) {
        setFocusable(focusable);
        setFocusableInTouchMode(focusable);             //不弹出键盘
//        requestFocus();
    }


    /**
     * 获取标题最大长度
     *
     * @return
     */
    public int getMaxLength() {
        int length = 0;
        try {
            InputFilter[] inputFilters = mEditText.getFilters();
            for (InputFilter filter : inputFilters) {
                Class<?> c = filter.getClass();
                if (c.getName().equals("android.text.InputFilter$LengthFilter")) {
                    Field[] f = c.getDeclaredFields();
                    for (Field field : f) {
                        if (field.getName().equals("mMax")) {
                            field.setAccessible(true);
                            length = (Integer) field.get(filter);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }

    /**
     * 设置标题的最大长度
     *
     * @return
     */
    public void setMaxLength(int num) {
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(num)}); //设置最大长度
    }


    /**
     * 回调
     *
     * @param clickCallBack
     */
    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

    /**
     * 设置标题最少字数
     *
     * @param minLength
     */
    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    /**
     * 获取标题最少字数
     *
     * @return
     */
    public int getMinLength() {
        return minLength;
    }

    /**
     * 帖子标题的回调
     */
    public interface ClickCallBack {
        void onTitleEditorClick(View v);

        void onReleaseClick(boolean isClick);
    }
}
