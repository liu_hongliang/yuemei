package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.TaoDataApi;
import com.module.commonview.utils.StatisticalManage;
import com.module.community.model.bean.ExposureLoginData;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.activity.InSureActivity;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.controller.activity.OrderPreferentialActivity639;
import com.module.my.model.api.CommitSecurityCodeInLoginApi;
import com.module.my.model.api.GetUserPhoneNumberApi;
import com.module.my.model.api.InitCode1Api;
import com.module.my.model.api.InitYouHuiApi;
import com.module.my.model.api.InsuranceMessageApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.bean.BaoXianData;
import com.module.my.model.bean.UserData;
import com.module.my.model.bean.YouHuiCoupons;
import com.module.my.model.bean.YouHuiData;
import com.module.my.view.view.PopupWindows;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.api.PayOrderApi;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoDetailData639;
import com.quicklyask.entity.TaoMemberData;
import com.quicklyask.entity.TaoPayPrice;
import com.quicklyask.entity.UserPhoneData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.BaoxianPopWindow;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.quicklyask.activity.R.id.yanzheng_code_rly;

//import com.module.commonview.view.NumberAddSubView;


/**
 * 重构后的填写订单信息
 *
 * @author 裴成浩
 */
public class OrderWriteMessageActivity639 extends BaseActivity {

    @BindView(id = R.id.order_time_all_ly)
    private LinearLayout allcontent;

    //***********不可退款说明start**************************
    @BindView(id = R.id.yuding_refund_tips_ly)
    private LinearLayout yudingRefundLy; //点击
    @BindView(id = R.id.yuding_refund_tips_tv)
    private TextView yudingRefundTv;    //文案
    //***********不可退款说明end****************************

    //***********预定人信息start***************************
    @BindView(id = R.id.login_tips_ly, click = true)
    private LinearLayout lginTipsLy;    //登录提示框

    //未登录手机验证
    @BindView(id = R.id.input_order_message_ly)
    private LinearLayout tianMessageLy;// 验证手机号框
    @BindView(id = R.id.order_phone_number_et)
    private EditText phoneNumberEt;
    @BindView(id = R.id.yanzheng_code_rly123)
    private RelativeLayout sendEMSRly;
    @BindView(id = R.id.yanzheng_code_tv123)
    private TextView emsTv;
    @BindView(id = R.id.order_phone_code_et)
    private EditText phoneCode;
    @BindView(id = R.id.yanzheng_code_rly, click = true)
    private RelativeLayout noCodeRly;// 没收到
    @BindView(id = R.id.yanzheng_code_tv)
    private TextView noCodeTv;

    //已绑定手机号（已登陆）
    @BindView(id = R.id.write_phone_bangding_ly, click = true)
    private RelativeLayout bangdingLy;// 绑定手机框
    @BindView(id = R.id.write_bangding_phone_tv)
    private TextView phoneTv;// 绑定的手机号

    //留言框
    @BindView(id = R.id.input_order_other_et)
    private EditText otherEt;
    //***********预定人信息end****************************

    //***********悦美价及数量start****************************

    //***********悦美价及数量end****************************

    //***********优惠价格start****************************
    //订金券
    @BindView(id = R.id.order_method_vip_rly)
    private RelativeLayout vipRly;              //会员首单立减框
    @BindView(id = R.id.order_method_vip_tv)
    private TextView vipTv;                   //会员首单立减金额

    @BindView(id = R.id.order_method_dingjin_rly, click = true)
    private RelativeLayout dingjinRly;              //订金劵框
    @BindView(id = R.id.order_method_dingjin_tv)
    private TextView dingjinTv;                  //订金劵文案

    //尾款券
    @BindView(id = R.id.order_method_daijinjuan_rly, click = true)
    private RelativeLayout weikuanRly;       //尾款劵框
    @BindView(id = R.id.order_method_daijinjuan_title)
    private TextView weikuanTit;              //代金券还是尾款劵
    @BindView(id = R.id.order_method_daijinjuan_tv)
    private TextView weikuanTv;              //尾款劵文案

    //尾款红包
    @BindView(id = R.id.order_method_hongbao_rly, click = true)
    private RelativeLayout hongbaoRly;          //红包框
    @BindView(id = R.id.order_method_hongbao_title)
    private TextView hongbaoTit;                 //红包文案
    @BindView(id = R.id.order_method_hongbao_tv)
    private TextView hongbaoTv;                 //红包文案

    //保险
    @BindView(id = R.id.baoxian_tips_iv, click = true)
    private ImageView baoxianTips;          //保险谈层说明
    @BindView(id = R.id.baoxian_content_ly)
    private LinearLayout baoxianContentLy;  //保险框
    @BindView(id = R.id.baoxian_title_tv)
    private TextView baoxianTv;             //保险文案
    @BindView(id = R.id.baoxian_fee_tv)
    private TextView baoxianFeeTv;          //保险价格
    @BindView(id = R.id.baoxian_is_ly)
    private LinearLayout baoxianMessageLy;  //被保险人信息
    @BindView(id = R.id.baoxian_name_and_phone_tv)
    private TextView baoxianMessageTv;      //被保险人
    @BindView(id = R.id.baoxian_updatemessage_tv, click = true)
    private TextView baoxianUpdateTv;       //更改被保险人
    @BindView(id = R.id.baoxian_check_iv)
    private CheckBox baoxianCheckIv;       //是否选择保险

    //***********优惠价格end****************************

    //***********价格展示start****************************
    @BindView(id = R.id.yd_yuemei_jia_tv)
    private TextView dingdanJiaTv;      // 订单金额

    @BindView(id = R.id.yd_baoxian_jiage_rly)   //保险金额框
    private RelativeLayout baoxianJiaLy;
    @BindView(id = R.id.yd_baoxian_jiage_tv)    //保险金额
    private TextView baoxianJiaTv;

    @BindView(id = R.id.yd_vip_jiage_rly)       //PLUS会员首单立减框
    private RelativeLayout vipJiaLy;
    @BindView(id = R.id.yd_vip_jiage_tv)        //PLUS会员首单立减金额
    private TextView vipJiaTv;

    @BindView(id = R.id.yd_red_packet_rly)
    private RelativeLayout redPacjetRly;// 订金优惠金额框
    @BindView(id = R.id.concessionary_title)
    private TextView concessionaryTitle;// 订金优惠金额文案框
    @BindView(id = R.id.yd_red_packet_tv)
    private TextView redPacketjinTv;// 订金优惠金额

    @BindView(id = R.id.yd_balance_rly)
    private RelativeLayout balanceRly;// 到院支付尾款优惠框
    @BindView(id = R.id.yd_balance_tv)
    private TextView balancejinTv;// 到院支付尾款优惠金额

    @BindView(id = R.id.yd_daoyuan_zhifu_rly)
    private RelativeLayout daoyuanRly;// 到院支付框
    @BindView(id = R.id.yd_daoyuan_zhifu_tv)
    private TextView daoYuanTv;// 到院支付

    //***********价格展示end******************************

    //***********支付金额strat****************************
    @BindView(id = R.id.oder_shifu_or_dingdan)
    private TextView shifuOrdingdanTv;      //在线支付文案
    @BindView(id = R.id.order_method_shifu_tv)
    private TextView shifuTv;               //在线支付金额

    @BindView(id = R.id.tao_detail_share_fan_ly)
    private LinearLayout shareRijiLy;       //分享日记返现框

    //***********支付金额end******************************

    //***********其他start**************************
    //标题
    @BindView(id = R.id.order_time_back, click = true)
    private RelativeLayout back;// 返回
    //提交按钮
    @BindView(id = R.id.sumbit_order_bt, click = true)
    private Button sumnbit;     //提交按钮
    //***********其他end**************************

    //其他常量
    private final String TEXT1 = "未登录无法使用订金券";
    private final String TEXT2 = "未登录无法使用尾款券";
    private final String TEXT3 = "未登录无法使用尾款红包";
    private final String TEXT4 = "参与拼团不可使用订金券";
    private final String TEXT5 = "参与拼团不可使用尾款券";
    private final String TEXT6 = "参与拼团不可使用尾款红包";
    private final String TEXT7 = "由于优惠较多，订金订购本项目不支持退款，全款预订支持退款，但一次订购多个，一旦到院确认消费后，没有使用完的部分不支持退款，但仍然可以继续使用";
    private final String TEXT8 = "由于优惠较多，预订本项目不支持退款，请详细确认预订信息";
    private final String TEXT9 = "正在拨打您的电话，请注意接听";
    private final String TEXT10 = "数字错误，请重新输入";


    private final int REQUEST_CODE1 = 6;
    private final int REQUEST_CODE2 = 8;
    private final int REQUEST_CODE3 = 9;
    private final int REQUEST_CODE4 = 99;
    private final int REQUEST_CODE5 = 101;
    private final int REQUEST_CODE6 = 18;

    //初始化参数
    private String TAG = "OrderWriteActivity639";
    private OrderWriteMessageActivity639 mContext;
    private PageJumpManager pageJumpManager;
    private String mUid;
    private LoadingProgress mDialog;
    private String mLatitude;     //所在纬度
    private String mLongitude;   //所在经度

    //传过来的参数
    private String mTaoid;      //订单id
    private String mSource;
    private String mPayType;    //支付类型
    private String mObjid;
    private String mGroupId;
    private String mType;       //判断拼团时是拼团购买还是单独购买

    //预订信息的数据
    private String mTitle;
    private String mIsOrder;        //允许使用定金全款标识
    private String mFeeScale;       //规格

    private float mDingjin;        // 普通订金价
    private String mIsGroup;        //是否是拼团
    private String mGroupNumber;    //起拼人数
    private float mGroupPrice;        //拼团价格
    private float mGroupDingjin;   //拼团订金价
    private String mIsMember;       //是否是会员下单
    private float mMemberPrice;     //会员价格
    private float mVipFristPrice;   //会员首单立减价格
    private String mGroupIsOrder;   //拼团支持 全款或订金
    private float mPriceDiscount;     //悦美价（大促时自动变为大促价）
    private float mHosPrice;       //到院剩余价格
    private String mRefund;         //是否可以退款
    private int mMaxNumber;      //最高限购数量
    private int mMinNumber;      //最低限购数
    private String mIsBao;          //是否有保险
    private String mBaoxianTitle;   //保险的报销本内容显示
    private float mBaoPrice;       //保险金额
    private String mIsFanxian;      //是否有返现

    //获取保险信息
    private String baoXianName;     //姓名
    private String baoXianSex;      //性别
    private String baoXianPhone;    //电话
    private String baoXianIdcard;   //身份证

    //根据mUid获取绑定的手机号
    private String mUphone;


    //使用的代金券信息

    //优惠券信息
    private String mCouponsnum;       //可使用的优惠券个数
    private ArrayList<YouHuiCoupons> dingjinLists = new ArrayList<>();//订金券数据
    private ArrayList<YouHuiCoupons> weikuanLists = new ArrayList<>();//尾款券数据
    private ArrayList<YouHuiCoupons> hongbaoLists = new ArrayList<>(); //红包数据
    private YouHuiCoupons djCoupon = new YouHuiCoupons();    //当前使用的订金券数据
    private YouHuiCoupons wkqCoupon = new YouHuiCoupons();    //当前使用的尾款券数据
    private YouHuiCoupons hbCoupon = new YouHuiCoupons();    //当前使用的红包数据

    //其他
    private int buyNum = 1;          //购买数量
    private float mYuemeiAndGroup;  //(悦美/拼团/plus会员)价格
    private float mActualPay;        //支付金额
    private PopupWindows mPopupWindows; //验证码谈层
    private BaoxianPopWindow mbaoxianPop;   //保险谈层
    private GradientDrawable drawable1;
    private GradientDrawable drawable2;
    private String phone;
    private String mPostStr;
    private String mU;
    private String mReferrer;
    private String mReferrerId;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_order_write_message639);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = OrderWriteMessageActivity639.this;
        pageJumpManager = new PageJumpManager(mContext);
        mDialog = new LoadingProgress(mContext);
        mLatitude = Cfg.loadStr(mContext, FinalConstant.DW_LATITUDE, "0");
        mLongitude = Cfg.loadStr(mContext, FinalConstant.DW_LONGITUDE, "0");

        initView();         //初始化UI
        initClick();        //各个事件回调0
        String loadStr = Cfg.loadStr(this, FinalConstant.EXPOSURE_LOGIN, "");
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(loadStr)) {
            ExposureLoginData exposureLoginData = new Gson().fromJson(loadStr, ExposureLoginData.class);
            mReferrer = exposureLoginData.getReferrer();
            mReferrerId = exposureLoginData.getReferrer_id();
            hashMap.put("referrer", mReferrer);
            hashMap.put("referrer_id", mReferrerId);
        }else{
            hashMap.put("referrer", "0");
            hashMap.put("referrer_id", "0");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mUid = Utils.getUid();
    }

    /**
     * 点击回调
     *
     * @param v
     */
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        if (Utils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.order_time_back:         //返回按钮

                showDialogExitEdit();

                break;
            case R.id.sumbit_order_bt:// 确认下单

                goPlaceOrder();

                break;
            case R.id.write_phone_bangding_ly:// 更改手机号

                Intent it = new Intent();
                it.setClass(mContext, OrderPhoneModifyActivity.class);
                startActivityForResult(it, REQUEST_CODE1);

                break;
            case R.id.order_method_dingjin_rly: //添加订金券
                Log.e(TAG, "dingjinLists === " + dingjinLists.size());
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
                        intent.putExtra("youhui_coupon", djCoupon);                          //选中的
                        intent.putParcelableArrayListExtra("youhui_coupons", dingjinLists);  //列表
                        startActivityForResult(intent, REQUEST_CODE6);
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }

                break;
            case R.id.order_method_daijinjuan_rly:// 添加尾款券
                Log.e(TAG, "weikuanLists === " + weikuanLists.size());
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
                        intent.putExtra("youhui_coupon", wkqCoupon);                 //选中的
                        intent.putParcelableArrayListExtra("youhui_coupons", weikuanLists);  //列表
                        startActivityForResult(intent, REQUEST_CODE2);
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }

                break;
            case R.id.order_method_hongbao_rly:            //添加医院红包
                Log.e(TAG, "hongbaoLists === " + hongbaoLists.size());
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
                        intent.putExtra("youhui_coupon", hbCoupon);                  //选中的
                        intent.putParcelableArrayListExtra("youhui_coupons", hongbaoLists);  //列表
                        startActivityForResult(intent, REQUEST_CODE3);
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.login_tips_ly:// 提示登录
                Intent intent = new Intent(mContext, LoginActivity605.class);
                startActivityForResult(intent, REQUEST_CODE5);
                break;
            case yanzheng_code_rly:// 没收到 语音验证码

                mPopupWindows.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

                break;
            case R.id.baoxian_tips_iv://保险说明弹窗

                mbaoxianPop.showAtLocation(allcontent, Gravity.CENTER, 0, 0);

                break;
            case R.id.baoxian_updatemessage_tv://更改保险人信息
                Intent it88 = new Intent();
                it88.setClass(mContext, InSureActivity.class);
                it88.putExtra("insure_name", baoXianName);
                it88.putExtra("insure_phone", baoXianPhone);
                it88.putExtra("insure_card", baoXianIdcard);
                it88.putExtra("insure_sex", baoXianSex);
                it88.putExtra("taoid", mTaoid);
                startActivityForResult(it88, REQUEST_CODE4);
                break;
        }
    }

    /**
     * 去下单
     */
    private void goPlaceOrder() {

        if ("0".equals(mMaxNumber)) {
            showDialogXianE("2");
            return;
        }

        if (!TextUtils.isEmpty(mUphone)) {              //如果没有绑定手机号（未登录/已登录未绑定手机号）
            if (mActualPay * buyNum > 50000) {
                showDialogXianE("1");
            } else {
                sumnbit.setText("提交中...");
                sumnbit.setClickable(false);
                initPayOrder();
            }
        } else {
            String codeStr = phoneCode.getText().toString();
            mUphone = phoneNumberEt.getText().toString();

            if (mUphone.length() > 0) {
                if (codeStr.length() > 0) {
                    if (mActualPay * buyNum > 50000) {
                        showDialogXianE("1");
                    } else {
                        sumnbit.setText("提交中...");
                        sumnbit.setClickable(false);
                        if (Utils.isLogin()) {
                            initYesLoggedCode();
                        } else {
                            initNotLoggedCode();
                        }
                    }
                } else {
                    ViewInject.toast("请输入验证码！");
                }
            } else {
                ViewInject.toast("请输入手机号！");
            }

        }
    }

    /**
     * 初始化UI
     */
    private void initView() {
        Intent intent = getIntent();
        mTaoid = intent.getStringExtra("id");
        mPayType = intent.getStringExtra("payType");
        mSource = intent.getStringExtra("source");
        mObjid = intent.getStringExtra("objid");
        mGroupId = intent.getStringExtra("group_id");
        Log.e(TAG, "mGroupId === " + mGroupId);
        mType = intent.getStringExtra("type");
        mPostStr = intent.getStringExtra("postStr");
        mU = intent.getStringExtra("u");
        TaoDetailData639 mTaoData = getIntent().getParcelableExtra("data");

        //根据是否登录设置不同的UI界面
        setLoginBox();

        //获取手机号
        if (Utils.isLogin()) {
            initGetPhone();
        }
        //被保险人文案添加下划线
        baoxianUpdateTv.setText(Html.fromHtml(("<u>" + "更改信息>" + "</u>")));

        //获取需要的数据
        if (mTaoData != null) {
            analyticalTaoData(mTaoData);
        } else {
            loadTaoData();
        }

        //初始化需要的谈层
        mPopupWindows = new PopupWindows(mContext, allcontent);

        HashMap<String, Object> urlMap = new HashMap<>();
        urlMap.put("tao_id", mTaoid);
        mbaoxianPop = new BaoxianPopWindow(mContext, FinalConstant.TAO_BAOXIAN, urlMap);

        //初始化验证码框的变化
        drawable1 = new GradientDrawable();
        drawable1.setShape(GradientDrawable.RECTANGLE); // 画框
        drawable1.setStroke(1, getResources().getColor(R.color.red_ff4965)); // 边框粗细及颜色
        drawable1.setColor(0xFFFFFFFF); // 边框内部颜色

        drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE); // 画框
        drawable2.setStroke(1, getResources().getColor(R.color.button_bian2)); // 边框粗细及颜色
        drawable2.setColor(0xFFFFFFFF); // 边框内部颜色

        sendEMSRly.setBackground(drawable1); // 设置背景（效果就是有边框及底色）

        //设置优惠文案
        if (isReservation()) {                          //预定
            concessionaryTitle.setText("订金优惠金额：");
        } else {                                        //全款
            concessionaryTitle.setText("优惠金额：");
        }

        //设置上边价格文案
        Log.e(TAG, "isPinTuan === " + isPinTuan());

        //设置代金券和红包的文案
        if (isReservation()) {
            weikuanTit.setText("尾款劵");
            hongbaoTit.setText("尾款红包");
        } else {
            weikuanTit.setText("代金券");
            hongbaoTit.setText("医院红包");
        }
    }

    /**
     * 事件回调
     */
    private void initClick() {

        //验证码谈层
        mPopupWindows.setOnTureClickListener(new PopupWindows.OnTureClickListener() {
            @Override
            public void onTureClick(String codes) {
                yanzhengCode(codes);
            }
        });

        //发送验证码
        sendEMSRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendCode();
            }
        });

        //保险的选择
        baoxianCheckIv.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Log.e(TAG, "isChecked == " + isChecked);
                if (isChecked) {
                    if (!TextUtils.isEmpty(baoXianName) && !TextUtils.isEmpty(baoXianPhone) && !TextUtils.isEmpty(baoXianIdcard) && !TextUtils.isEmpty(baoXianSex)) {   //是否授权
                        initNumChange();
                    } else {
                        Intent it88 = new Intent();
                        it88.setClass(mContext, InSureActivity.class);
                        it88.putExtra("insure_name", baoXianName);
                        it88.putExtra("insure_phone", baoXianPhone);
                        it88.putExtra("insure_card", baoXianIdcard);
                        it88.putExtra("insure_sex", baoXianSex);
                        it88.putExtra("taoid", mTaoid);
                        startActivityForResult(it88, REQUEST_CODE4);
                    }
                } else {
                    initNumChange();
                }
            }
        });
    }

    /**
     * 发送验证码
     */
    private void sendCode() {
        String phones = phoneNumberEt.getText().toString().trim();
        if (phones.length() > 0 && null != phones) {
            String textPhn = phoneNumberEt.getText().toString();
            if (Utils.isMobile(textPhn)) {
                sendEMSRly.setClickable(false);
                sendEMS();

                noCodeRly.setVisibility(View.VISIBLE);
                noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？" + "</u>"));
                // phoneNumberEt.clearFocus();
                phoneCode.requestFocus();
                InputMethodManager inputManager = (InputMethodManager) phoneCode.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(phoneCode, 0);

                new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {
                        sendEMSRly.setBackground(drawable2);
                        emsTv.setTextColor(ContextCompat.getColor(mContext, R.color.button_zi));
                        emsTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                    }

                    @Override
                    public void onFinish() {
                        sendEMSRly.setBackground(drawable1);
                        emsTv.setTextColor(ContextCompat.getColor(mContext, R.color.button_bian_hong1));
                        sendEMSRly.setClickable(true);
                        emsTv.setText("重新获取验证码");
                    }
                }.start();
            } else {
                ViewInject.toast("请输入正确的手机号");
            }
        } else {
            ViewInject.toast("请输入手机号");
        }
    }


    /**
     * 获取订单信息
     */
    private void loadTaoData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mTaoid);
        maps.put("lon", mLongitude);
        maps.put("lat", mLatitude);
        maps.put("source", mSource);
        maps.put("objid", mObjid);
        TaoDataApi taoDataApi = new TaoDataApi();
        taoDataApi.setIntent(getIntent());
        taoDataApi.getCallBack(mContext, maps, new BaseCallBackListener<TaoDetailData639>() {
            @Override
            public void onSuccess(TaoDetailData639 taoData) {
                analyticalTaoData(taoData);
            }
        });
    }

    /**
     * 解析获取的数据
     */
    @SuppressLint("SetTextI18n")
    private void analyticalTaoData(TaoDetailData639 taoData) {
        mTitle = taoData.getTitle();
        TaoMemberData mMemberData = taoData.getMember_data();
        TaoPayPrice mPayPrice = taoData.getPay_price();

        //获取订单信息
        mIsOrder = mPayPrice.getIs_order();
        mIsMember = mPayPrice.getIs_member();

        mFeeScale = taoData.getFeeScale();
        mDingjin = Float.parseFloat(mPayPrice.getDingjin());

        mIsGroup = taoData.getIs_group();
        mGroupNumber = taoData.getGroup_number();
        mGroupPrice = Float.parseFloat(taoData.getGroup_price());
        mGroupDingjin = Float.parseFloat(taoData.getGroup_dingjin());
        mGroupIsOrder = taoData.getGroup_is_order();
        mPriceDiscount = Float.parseFloat(taoData.getPrice_discount());

        Log.e(TAG, "mPayPrice.getDiscountPrice()" + mPayPrice.getDiscountPrice());

        mHosPrice = Float.parseFloat(mPayPrice.getHos_price());
        mRefund = taoData.getRefund();
        mMaxNumber = Integer.parseInt(taoData.getNumber());
        mMinNumber = Integer.parseInt(taoData.getStart_number());

        mIsBao = taoData.getIs_bao();
        mBaoxianTitle = taoData.getBaoxian();
        mBaoPrice = Float.parseFloat(taoData.getBao_price());

        mIsFanxian = taoData.getIs_fanxian();

        mMemberPrice = Float.parseFloat(mMemberData.getMember_price());
        mVipFristPrice = Float.parseFloat(mMemberData.getFrist_lijian_price());

        //设置初始化购买数量
        buyNum = mMinNumber;
        //设置悦美价/拼团价/plus会员价
        if (isMember()) {
            mYuemeiAndGroup = mMemberPrice;
            //plus会员标识是否显示
//            pricePulsLogo.setVisibility(View.VISIBLE);
        } else {
//            pricePulsLogo.setVisibility(View.GONE);

            if (isPinTuan()) {
                mYuemeiAndGroup = mGroupPrice;
            } else {
                mYuemeiAndGroup = mPriceDiscount;
            }
        }

        //首单立减提示显示和隐藏
        Log.e(TAG, "mVipFristPrice === " + mVipFristPrice);
        if (isVipFirstSingle()) {
            if (mYuemeiAndGroup > 0) {
                vipRly.setVisibility(View.VISIBLE);
                vipJiaLy.setVisibility(View.VISIBLE);
                vipTv.setText("-￥" + mVipFristPrice);
                vipJiaTv.setText("-￥" + mVipFristPrice);
            } else {
                vipRly.setVisibility(View.GONE);
                vipJiaLy.setVisibility(View.GONE);
            }
        } else {
            vipRly.setVisibility(View.GONE);
            vipJiaLy.setVisibility(View.GONE);
        }

        //设置支付的金额
        if (isPinTuan()) {
            if (isReservation()) {
                mActualPay = mGroupDingjin;
            } else {
                mActualPay = mGroupPrice;
            }
        } else {
            if (isReservation()) {
                mActualPay = mDingjin;
            } else {
                mActualPay = mPriceDiscount;
            }
        }

        Log.e(TAG, "mTaoid ===" + mTaoid);
        Log.e(TAG, "mSource ===" + mSource);
        Log.e(TAG, "mObjid ===" + mObjid);
        Log.e(TAG, "mType ===" + mType);

        Log.e(TAG, "mIsOrder ===" + mIsOrder);
        Log.e(TAG, "mFeeScale ===" + mFeeScale);
        Log.e(TAG, "mDingjin ===" + mDingjin);
        Log.e(TAG, "mIsGroup ===" + mIsGroup);

        Log.e(TAG, "mGroupNumber ===" + mGroupNumber);
        Log.e(TAG, "mGroupPrice ===" + mGroupPrice);
        Log.e(TAG, "mGroupDingjin ===" + mGroupDingjin);
        Log.e(TAG, "mGroupIsOrder ===" + mGroupIsOrder);
        Log.e(TAG, "mPriceDiscount ===" + mPriceDiscount);

        Log.e(TAG, "mHosPrice ===" + mHosPrice);
        Log.e(TAG, "mRefund ===" + mRefund);
        Log.e(TAG, "mMaxNumber ===" + mMaxNumber);
        Log.e(TAG, "mMinNumber ===" + mMinNumber);

        Log.e(TAG, "mIsBao ===" + mIsBao);
        Log.e(TAG, "mBaoxianTitle ===" + mBaoxianTitle);
        Log.e(TAG, "mBaoPrice ===" + mBaoPrice);

        Log.e(TAG, "mIsFanxian ===" + mIsFanxian);


        Log.e(TAG, "buyNum ===" + buyNum);
        Log.e(TAG, "mYuemeiAndGroup ===" + mYuemeiAndGroup);
        Log.e(TAG, "mActualPay ===" + mActualPay);

        setViewData();
    }

    /**
     * 开始设置联网请求视图的数据
     */
    private void setViewData() {

        //拼团规则设置
        if (isPinTuan()) {
            if (mGroupPrice > 500) {                //拼团价格大于500元
                if (!isFanxian()) {                   //是否显示返现
                    shareRijiLy.setVisibility(View.GONE);         //分享日记返现隐藏
                } else {
                    shareRijiLy.setVisibility(View.VISIBLE);         //分享日记返现显示
                }
            } else {
                shareRijiLy.setVisibility(View.GONE);            //分享日记返现隐藏
            }
        } else {
            if (mGroupPrice > 500) {
                shareRijiLy.setVisibility(View.VISIBLE);         //分享日记返现显示
            } else {
                shareRijiLy.setVisibility(View.GONE);            //分享日记返现隐藏
            }
        }

        //预定全框规则设置
        if (isReservation()) {// 预订

            daoyuanRly.setVisibility(View.VISIBLE);     //到院决定消费后再付给医院金额
            shifuOrdingdanTv.setText("在线支付订金");

        } else {    // 全款

            daoyuanRly.setVisibility(View.GONE);
            shifuOrdingdanTv.setText("在线支付金额");        //在线支付定金框隐藏

        }

        //为代金券和红包设置规则
        if (Utils.isLogin()) {                                //如果是登录状态
            getYouHuiData();
        } else {
            dingjinTv.setText(TEXT1);
            weikuanTv.setText(TEXT2);
            hongbaoTv.setText(TEXT3);
        }

        //设置保险规则
        if (isBaoXian()) {
            if (Utils.isLogin()) {
                baoxianMessageLy.setVisibility(View.VISIBLE);
            } else {
                baoxianMessageLy.setVisibility(View.GONE);
            }
            baoxianContentLy.setVisibility(View.VISIBLE);
            baoxianTv.setText(mBaoxianTitle);
            setinsDetails();
            getBaoXianData();
        } else {
            baoxianContentLy.setVisibility(View.GONE);
        }

        //是否可退款规则设置
        switch (mRefund) {
            case "1":
                yudingRefundLy.setVisibility(View.GONE);
                break;
            case "2":
                yudingRefundLy.setVisibility(View.GONE);
                break;
            case "3":
                yudingRefundLy.setVisibility(View.VISIBLE);
                yudingRefundTv.setText(TEXT8);
                break;
        }

        //设置悦美价/拼团价/puls会员价
//        yuemeiJiaTv1.setText("￥" + ((int) mYuemeiAndGroup));

        //设置限购数量
//        nbAddsub.setMinValue(mMinNumber);
//        nbAddsub.setMaxValue(mMaxNumber);
//        xiangouNumber.setText("（限购" + mMaxNumber + "个）");

        //在线支付金额
        initNumChange();
    }

    /**
     * 获取保险信息
     */
    private void getBaoXianData() {
        Map<String, Object> maps = new HashMap<>();
        new InsuranceMessageApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {

                        if (!TextUtils.isEmpty(serverData.data)) {

                            BaoXianData mBaoXianData = JSONUtil.TransformSingleBean(serverData.data, BaoXianData.class);
                            baoXianName = mBaoXianData.getName();
                            baoXianSex = mBaoXianData.getSex();
                            baoXianPhone = mBaoXianData.getPhone();
                            baoXianIdcard = mBaoXianData.getIdcard();
                            Log.e(TAG, "baoXianName == " + baoXianName);
                            if (!TextUtils.isEmpty(baoXianName) && !TextUtils.isEmpty(baoXianPhone) && !TextUtils.isEmpty(baoXianIdcard) && !TextUtils.isEmpty(baoXianSex)) {
                                baoxianMessageLy.setVisibility(View.VISIBLE);
                                baoxianCheckIv.setChecked(true);

                                baoxianMessageTv.setText("被保险人：" + baoXianName + " (" + baoXianPhone.substring(0, 3) + "****" + baoXianPhone.substring(7, baoXianPhone.length()) + ")");

                            } else {
                                baoxianMessageLy.setVisibility(View.GONE);
                                baoxianCheckIv.setChecked(false);
                            }

                        } else {
                            baoxianMessageLy.setVisibility(View.GONE);
                            baoxianCheckIv.setChecked(false);
                        }

                        setinsDetails();
                        initNumChange();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 获取订金劵、尾款券、红包的数据（订金劵、尾款券、红包统称优惠券）
     */
    private void getYouHuiData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("pay_type", mPayType);                 //支付类型
//        maps.put("num", nbAddsub.getValue() + "");       //购买数量
        maps.put("tao_id", mTaoid);                     //淘id
        new InitYouHuiApi().getCallBack(mContext, maps, new BaseCallBackListener<YouHuiData>() {
            @Override
            public void onSuccess(YouHuiData youHuiData) {
                dingjinLists.clear();
                weikuanLists.clear();
                hongbaoLists.clear();
                mCouponsnum = youHuiData.getCouponsnum();
                for (YouHuiCoupons coupon : youHuiData.getCoupons()) {

                    switch (coupon.getCouponsType()) {
                        case "1":
                            if ((mActualPay * buyNum) >= coupon.getLowest_consumption()) {     //可用订金劵

                                dingjinLists.add(coupon);
                            }
                            break;
                        case "2":
                            if ((mYuemeiAndGroup * buyNum) >= coupon.getLowest_consumption()) {     //可用尾款劵
                                weikuanLists.add(coupon);
                            }
                            break;
                        case "3":
                            if ((mYuemeiAndGroup * buyNum) >= coupon.getLowest_consumption()) {     //可用尾款红包
                                hongbaoLists.add(coupon);
                            }
                            break;
                    }

                }
                setAppropriatePrefere();
                setWeiKuanNum();
                setDingJinNum();
                setHongbaoNum();
                initNumChange();
            }

        });
    }

    /**
     * 根据mUid获取绑定的手机号
     */
    private void initGetPhone() {
        Map<String, Object> maps = new HashMap<>();
        new GetUserPhoneNumberApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserPhoneData userPhoneData = JSONUtil.TransformUserPhone(serverData.data);
                    mUphone = userPhoneData.getPhone();

                    if ("0".equals(mUphone) || "".equals(mUphone)) {
                        bangdingLy.setVisibility(View.GONE);
                        tianMessageLy.setVisibility(View.VISIBLE);
                    } else {

                        bangdingLy.setVisibility(View.VISIBLE);
                        tianMessageLy.setVisibility(View.GONE);

                        String a = mUphone.substring(0, 3);
                        String b = mUphone.substring(mUphone.length() - 4, mUphone.length());
                        String ss = a + "****" + b;
                        phoneTv.setText(ss);
                    }

                }
            }

        });
    }

    /**
     * 获取验证码
     */
    private void sendEMS() {
        phone = phoneNumberEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                String code1 = serverData.code;
                String message = serverData.message;
                if ("1".equals(code1)) {
                    ViewInject.toast(message);
                } else {
                    ViewInject.toast(message);
                }
            }
        });
    }


    /**
     * 下单时提交验证码（未登录）
     */
    private void initNotLoggedCode() {
        String phones = phoneNumberEt.getText().toString().trim();
        String codeStr = phoneCode.getText().toString();
        Map<String, Object> params = new HashMap<>();
        params.put("phone", phones);
        params.put("code", codeStr);
        if (!TextUtils.isEmpty(mReferrer)){
            params.put("referrer",mReferrer);
        }
        if (!TextUtils.isEmpty(mReferrerId)){
            params.put("referrer_id",mReferrerId);
        }
        new InitCode1Api().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData mUserData = JSONUtil.TransformLogin(serverData.data);
                    String id = mUserData.get_id();
                    String img = mUserData.getImg();
                    String nickName = mUserData.getNickname();
                    String province = mUserData.getProvince();
                    String city = mUserData.getCity();
                    String sex = mUserData.getSex();
                    String birthday = mUserData.getBirthday();

                    Utils.setUid(id);
                    Cfg.saveStr(mContext, FinalConstant.UHEADIMG, img);
                    Cfg.saveStr(mContext, FinalConstant.UNAME, nickName);
                    Cfg.saveStr(mContext, FinalConstant.UPROVINCE, province);
                    Cfg.saveStr(mContext, FinalConstant.UCITY, city);
                    Cfg.saveStr(mContext, FinalConstant.USEX, sex);
                    Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, birthday);

                    PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);

                    StatisticalManage.getInstance().growingIO("logon");
                    initPayOrder();
                } else {
                    sumnbit.setClickable(true);
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    /**
     * 下单时提交验证码（已登录没有绑定手机号）
     */
    private void initYesLoggedCode() {

        String codeStr = phoneCode.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phone);
        maps.put("code", codeStr);
        new CommitSecurityCodeInLoginApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                String code1 = serverData.code;
                String message = serverData.message;
                if ("1".equals(code1)) {
                    initPayOrder();
                } else {
                    sumnbit.setClickable(true);
                    ViewInject.toast(message);
                }
            }
        });
    }

    /**
     * 验证码确定
     *
     * @param codes
     */
    private void yanzhengCode(String codes) {

        String phones = phoneNumberEt.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phones);
        maps.put("code", codes);
        maps.put("flag", "codelogin");
        new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {

                    mPopupWindows.dismiss();

                    ViewInject.toast(TEXT9);
                } else {
                    ViewInject.toast(TEXT10);
                }
            }
        });
    }

    /**
     * 支付订单
     */
    private void initPayOrder() {
        final int[] dates = getDate();
        String dateStr = dates[2] + "-" + dates[1] + "-" + dates[0];    //日期
        String otherStr = otherEt.getText().toString();                 //客服留言
        final String djid = djCoupon.getCard_id();
        final String juanid = wkqCoupon.getCard_id();
        String hongbapid = hbCoupon.getCard_id();

        HashMap<String, Object> maps = new HashMap<>();
        maps.put("arrive_time", dateStr);
        maps.put("username", mUphone);
        maps.put("beizhu", TextUtils.isEmpty(otherStr) ? "无" : otherStr);
        maps.put("phone", mUphone);
        maps.put("pay_type", mPayType);
        maps.put("pay_id", "0");
        maps.put("discount_id", "1");
        maps.put("tao_id", mTaoid);
        maps.put("hos_price", mHosPrice + "");
        maps.put("price", (mActualPay * buyNum) + "");
        maps.put("integral", "0");
        maps.put("priceintegral", "0");
        maps.put("allow_call", "1");
        maps.put("lijian_type", "0");
        maps.put("source", mSource);
        maps.put("objid", mObjid);

        if (isBaoXian() && baoxianCheckIv.isChecked()) {
            maps.put("is_insure", "1");
        } else {
            maps.put("is_insure", "0");
        }
        maps.put("insure_name", TextUtils.isEmpty(baoXianName) ? "0" : baoXianName);
        maps.put("insure_phone", TextUtils.isEmpty(baoXianPhone) ? "0" : baoXianPhone);
        maps.put("insure_sex", TextUtils.isEmpty(baoXianSex) ? "0" : baoXianSex);
        maps.put("insure_card_id", TextUtils.isEmpty(baoXianIdcard) ? "0" : baoXianIdcard);
        maps.put("hospital_hongbao", "0".equals(hongbapid) ? "0" : "1");
        maps.put("hos_hongbao_card_id", hongbapid);
        maps.put("deposit_coupons_card_id", djid);
        if (!TextUtils.isEmpty(mGroupId)) {
            maps.put("group_id", mGroupId);
        }
        if (isPinTuan()) {     //拼团
            maps.put("is_group", "1");
        } else {
            maps.put("is_group", "0");
        }
        maps.put("card_id", juanid);
        maps.put("postStr", mPostStr);
        if (!"".equals(mU) && mU != null){
            maps.put("u",mU);
        }


        //是否使用了首单立减
        if (isVipFirstSingle()) {
            maps.put("is_have_first_reduce", "1");
        } else {
            maps.put("is_have_first_reduce", "0");
        }

        Log.e(TAG, "dateStr === " + dateStr);
        Log.e(TAG, "mUphone === " + mUphone);
        Log.e(TAG, "otherStr === " + (TextUtils.isEmpty(otherStr) ? "无" : otherStr));
        Log.e(TAG, "mPayType === " + mPayType);
        Log.e(TAG, "mTaoid === " + mTaoid);
        Log.e(TAG, "mHosPrice === " + mHosPrice);
        Log.e(TAG, "mActualPay * buyNum === " + mActualPay * buyNum);
        Log.e(TAG, "mSource === " + mSource);
        Log.e(TAG, "mObjid === " + mObjid);
        Log.e(TAG, "mIsBao === " + mIsBao);
        Log.e(TAG, "baoXianName === " + (TextUtils.isEmpty(baoXianName) ? "0" : baoXianName));
        Log.e(TAG, "baoXianPhone === " + (TextUtils.isEmpty(baoXianPhone) ? "0" : baoXianPhone));
        Log.e(TAG, "baoXianSex === " + (TextUtils.isEmpty(baoXianSex) ? "0" : baoXianSex));
        Log.e(TAG, "baoXianIdcard === " + (TextUtils.isEmpty(baoXianIdcard) ? "0" : baoXianIdcard));
        Log.e(TAG, "hongbapid === " + hongbapid);
        Log.e(TAG, "mGroupId === " + mGroupId);
        Log.e(TAG, "juanid === " + juanid);

        new PayOrderApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
//                try {
//                    if ("1".equals(serverData.code)) {
//                        mDialog.stopLoading();
//                        XiaDanData xiaData = JSONUtil.TransformSingleBean(serverData.data, XiaDanData.class);
//
//                        String server_id = xiaData.getServer_id();
//                        String order_id = xiaData.getOrder_id();
//                        String money_ = xiaData.getMoney();
//                        String order_time = xiaData.getOrder_time();
//                        String is_repayment = xiaData.getIs_repayment();
//                        String is_repayment_mimo = xiaData.getIs_repayment_mimo();
//                        String isGroup = xiaData.getIs_group();
//                        mGroupId = xiaData.getGroup_id();
//                        Log.e(TAG, "1111isGroup == " + isGroup);
//                        Log.e(TAG, "1111groupId == " + mGroupId);
//
//
//                        //打点统计
//                        HashMap<String, String> hashMap = new HashMap<>();
//                        hashMap.put("Time", dates[2] + dates[1] + dates[0] + "");
////                        hashMap.put("quantity", numSl + "");
//                        hashMap.put("XDO1_jine", mActualPay * buyNum + "");
//
////                        Order order = Order.createOrder(order_id, (int) Float.parseFloat(money_), "CNY").addItem("淘整形", "taoid:" + mTaoid, (int) (mActualPay * buyNum * 100), numSl);
////                        TalkingDataAppCpa.onPlaceOrder(mUid, order);
//
////                        Cfg.saveInt(mContext, order_id + "sl", numSl);
//                        Cfg.saveInt(mContext, order_id + "ymj", (int) mActualPay * buyNum);
//
//
//                        // 下单成功后跳转
//                        if (Float.parseFloat(money_) <= 0) {
//                            HashMap<String, String> mMap = new HashMap<>();
//                            if (isPinTuan()) { //拼团
//                                Intent intent = new Intent(mContext, SpeltActivity.class);
//                                intent.putExtra("group_id", mGroupId);
//                                intent.putExtra("order_id", order_id);
//                                intent.putExtra("type", "2");
//                                startActivity(intent);
//                            } else {                                     //不是拼团
//                                mMap.put("payType", "2");
//                                mMap.put("pay_type", "1");
//                                mMap.put("server_id", server_id);
//                                mMap.put("order_id", order_id);
//                                mMap.put("taoid", mTaoid);
//                                mMap.put("sku_type", "1");
//                                mMap.put("is_repayment", is_repayment);
//                                mMap.put("is_repayment_mimo", is_repayment_mimo);
//                                mMap.put("price", (mActualPay * buyNum) + "");
//                                pageJumpManager.jumpToOrderZhiFuStatus1Activity(mMap);
//                            }
//
//                            Cfg.saveStr(mContext, "server_id", server_id);
//                            Cfg.saveStr(mContext, "order_id", order_id);
//                            Cfg.saveStr(mContext, "taotitle", mTitle);
//                            Cfg.saveStr(mContext, "price", money_);
//                            Cfg.saveStr(mContext, "taoid", mTaoid);
//                            Cfg.saveStr(mContext, "sku_type", "1");
//                            Cfg.saveStr(mContext, "is_repayment", is_repayment);
//                            Cfg.saveStr(mContext, "is_repayment_mimo", is_repayment_mimo);
//                            Cfg.saveStr(mContext, "group_id", mGroupId);
//                            Cfg.saveStr(mContext, "is_group", isGroup);
//                            finish();
//
//                        } else {
//
//                            Intent intent = new Intent(mContext, OrderMethodActivity594.class);
//                            intent.putExtra("price", money_);
//                            intent.putExtra("tao_title", mTitle);
//                            intent.putExtra("server_id", server_id);
//                            intent.putExtra("order_id", order_id);
//                            intent.putExtra("taoid", mTaoid);
//                            intent.putExtra("order_time", order_time);
//                            intent.putExtra("type", "1");
//                            intent.putExtra("sku_type", "1");
//                            intent.putExtra("is_repayment", is_repayment);
//                            intent.putExtra("is_repayment_mimo", is_repayment_mimo);
//                            intent.putExtra("weikuan", "0");
////                            intent.putExtra("number", numSl + "");
//                            intent.putExtra("is_group", isGroup);
//                            intent.putExtra("group_id", mGroupId);
//                            startActivity(intent);
//
//                            Cfg.saveStr(mContext, "server_id", server_id);
//                            Cfg.saveStr(mContext, "order_id", order_id);
//                            Cfg.saveStr(mContext, "taotitle", mTitle);
//                            Cfg.saveStr(mContext, "price", money_);
//                            Cfg.saveStr(mContext, "taoid", mTaoid);
//                            Cfg.saveStr(mContext, "sku_type", "1");
//                            Cfg.saveStr(mContext, "is_repayment", is_repayment);
//                            Cfg.saveStr(mContext, "is_repayment_mimo", is_repayment_mimo);
//                            Cfg.saveStr(mContext, "group_id", mGroupId);
//                            Cfg.saveStr(mContext, "is_group", isGroup);
//
//                            finish();
//                        }
//
//                    } else {
//                        ViewInject.toast(serverData.message);
//                        sumnbit.setClickable(true);
//                        sumnbit.setText("提交");
//                        TCAgent.onEvent(mContext, "订单提交失败", "普通淘_" + serverData.message);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        });
    }

    /**
     * 计算各处价格变化
     */
    @SuppressLint("SetTextI18n")
    private void initNumChange() {
        //订单金额
        dingdanJiaTv.setText("￥" + dingdanAmount());

        //订金优惠金额
        redPacketjinTv.setText("-￥" + depositPreAmount());

        //到院支付尾款优惠金额
        balancejinTv.setText("-￥" + balancePreAmount());

        //保险费用
        baoxianJiaTv.setText("￥" + baoxianAmount());

        //到院决定消费后再付给医院
        daoYuanTv.setText("￥" + consumptionAfterAmount());

        //支付金额
        shifuTv.setText("￥" + payAmount());

    }

    /**
     * 计算订金金额
     *
     * @return ：订金金额
     */
    private float dingdanAmount() {
        float paySumPrice;
        if (isPinTuan()) {
            if (isReservation()) {
                paySumPrice = mGroupDingjin * buyNum;
            } else {
                paySumPrice = mGroupPrice * buyNum;
            }
        } else {
            if (isReservation()) {
                paySumPrice = mDingjin * buyNum;
            } else {
                paySumPrice = mPriceDiscount * buyNum;
            }
        }

        return paySumPrice;
    }

    /**
     * 计算保险费用
     *
     * @return : 保险费用
     */
    private float baoxianAmount() {

        Log.e(TAG, "baoxianCheckIv.isChecked()333 = " + baoxianCheckIv.isChecked());
        if (baoxianCheckIv.isChecked()) {
            baoxianJiaLy.setVisibility(View.VISIBLE);
        } else {
            baoxianJiaLy.setVisibility(View.GONE);
        }

        return mBaoPrice;
    }


    /**
     * 计算订金优惠金额
     *
     * @return ：订金优惠金额
     */
    private float depositPreAmount() {
        float price = djCoupon.getMoney();
        if (price > 0 && !isPinTuan()) {
            redPacjetRly.setVisibility(View.VISIBLE);
        } else {
            redPacjetRly.setVisibility(View.GONE);
        }

        return price;
    }

    /**
     * 计算到院支付尾款优惠金额
     *
     * @return ：到院支付尾款优惠金额
     */
    private float balancePreAmount() {
        float price = hbCoupon.getMoney() + wkqCoupon.getMoney();   //红包优惠金额 + 代金券优惠金额
        if (price > 0 && !isPinTuan()) {
            balanceRly.setVisibility(View.VISIBLE);
        } else {
            balanceRly.setVisibility(View.GONE);
        }

        return price;
    }

    /**
     * 计算到院决定消费后再付给医院
     *
     * @return ：到院决定消费后再付给医院金额
     */
    private float consumptionAfterAmount() {
        float daoYuanPrice;
        if (!isPinTuan()) {
            float price = hbCoupon.getMoney() + wkqCoupon.getMoney();   //红包优惠金额 + 代金券优惠金额
            daoYuanPrice = mHosPrice * buyNum - price;
        } else {
            daoYuanPrice = mHosPrice * buyNum;
        }

        return daoYuanPrice < 0 ? 0 : daoYuanPrice;
    }

    /**
     * 计算支付金额
     *
     * @return ：支付金额
     */
    private float payAmount() {
        float shifuPrice;
        if (!isPinTuan()) {
            float youhuiPrice = djCoupon.getMoney();
            shifuPrice = (mActualPay * buyNum) - youhuiPrice;
        } else {
            shifuPrice = mActualPay * buyNum;
        }

        shifuPrice = (shifuPrice < 0 ? 0 : shifuPrice);
        Log.e(TAG, "baoxianCheckIv.isChecked() == " + baoxianCheckIv.isChecked());
        if (isBaoXian() && baoxianCheckIv.isChecked()) {
            shifuPrice = shifuPrice + mBaoPrice;
        }

        //会员优惠框是显示的，不是拼团订单，是会员下单，且首单立减还没有使用过
        Log.e(TAG, "isMember() === " + isMember());
        Log.e(TAG, "isVipFirstSingle() === " + isVipFirstSingle());
        if (vipJiaLy.getVisibility() == View.VISIBLE && isVipFirstSingle()) {
            shifuPrice = shifuPrice - mVipFristPrice < 0 ? 0 : shifuPrice - mVipFristPrice;
        }

        return shifuPrice;
    }


    /**
     * 退出该页面时的提示
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTvDilog = editDialog.findViewById(R.id.dialog_exit_content_tv);
        Button trueBtDilog = editDialog.findViewById(R.id.confirm_btn1_edit);
        Button cancelBtDilog = editDialog.findViewById(R.id.cancel_btn1_edit);

        titleTvDilog.setText("确认放弃本次下单");
        cancelBtDilog.setText("是");
        trueBtDilog.setText("继续下单");

        cancelBtDilog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
                onBackPressed();
            }
        });

        trueBtDilog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE1:                 //更改手机号的回调

                if (null != data) {
                    String phonenum = data.getStringExtra("phone");
                    mUphone = phonenum;
                    String spj = mUphone;
                    String a = spj.substring(0, 3);
                    String b = spj.substring(spj.length() - 4, spj.length());
                    String ss = a + "****" + b;
                    phoneTv.setText(ss);
                }
                break;

            case REQUEST_CODE6:                                             //使用定金券的回调
                djCoupon = data.getParcelableExtra("youhui_coupon");
                setDingJinNum();
                initNumChange();
                break;
            case REQUEST_CODE2:                                             //使用尾款券的回调
                wkqCoupon = data.getParcelableExtra("youhui_coupon");
                setWeiKuanNum();
                initNumChange();
                break;
            case REQUEST_CODE3:                                             //医院红包的回调
                hbCoupon = data.getParcelableExtra("youhui_coupon");
                Log.e(TAG, "hbCoupon == " + hbCoupon.toString());
                setHongbaoNum();
                initNumChange();
                break;
            case REQUEST_CODE4:      //保险信息回调
                if (null != data) {
                    baoXianName = data.getStringExtra("name");
                    baoXianPhone = data.getStringExtra("phone");
                    baoXianIdcard = data.getStringExtra("card");
                    baoXianSex = data.getStringExtra("sex");

                    if (!TextUtils.isEmpty(baoXianName) && !TextUtils.isEmpty(baoXianPhone) && !TextUtils.isEmpty(baoXianIdcard) && !TextUtils.isEmpty(baoXianSex)) {

                        baoxianMessageLy.setVisibility(View.VISIBLE);
                        baoxianMessageTv.setText("被保险人：" + baoXianName + " (" + baoXianPhone.substring(0, 3) + "****" + baoXianPhone.substring(7, baoXianPhone.length()) + ")");
                        baoxianCheckIv.setChecked(true);

                        setinsDetails();
                        initNumChange();
                    } else {
                        baoxianMessageLy.setVisibility(View.GONE);
                        baoxianCheckIv.setChecked(false);
                    }
                }
                break;
            case REQUEST_CODE5:     //登录后返回
                setLoginBox();
                if (Utils.isLogin()) {
                    getYouHuiData();
                    initGetPhone();
                }
                break;
        }

    }

    /***
     * 未登录和已登录显示同的UI
     */
    private void setLoginBox() {
        if (Utils.isLogin()) {
            lginTipsLy.setVisibility(View.GONE); //登录提示框
            tianMessageLy.setVisibility(View.GONE);  //手机号验证码框
            bangdingLy.setVisibility(View.VISIBLE); //登录后的手机信息框

        } else {
            lginTipsLy.setVisibility(View.VISIBLE); //登录提示框
            tianMessageLy.setVisibility(View.VISIBLE);  //手机号验证码框
            bangdingLy.setVisibility(View.GONE); //登录后的手机信息框

            weikuanTv.setText(TEXT2);
            hongbaoTv.setText(TEXT3);
        }
    }

    /**
     * 设置保险详情
     */
    private void setinsDetails() {
        if (mBaoPrice == 0) {
            baoxianFeeTv.setText("免费");
            baoxianFeeTv.setTextColor(Color.parseColor("#ff5c77"));
        } else {
            baoxianFeeTv.setText("￥" + mBaoPrice);
            baoxianFeeTv.setTextColor(Color.parseColor("#333333"));
        }
    }

    /**
     * 设置最合适的优惠
     */
    private void setAppropriatePrefere() {
        //找到最适合的订金劵
        if (dingjinLists.size() > 0) {
            int tempPos1 = 0;
            float tempPreferePrice1 = dingjinLists.get(0).getMoney();
            for (int i = 0; i < dingjinLists.size(); i++) {
                YouHuiCoupons youHuiCoupons = dingjinLists.get(i);
                float djMoney = youHuiCoupons.getMoney();
                if (mActualPay * buyNum - djMoney <= 0 && tempPreferePrice1 != djMoney) {
                    tempPos1 = i;
                    tempPreferePrice1 = djMoney;
                }
            }

            djCoupon = dingjinLists.get(tempPos1);
        } else {
            djCoupon = new YouHuiCoupons();
        }

        //如果支付尾款不是0
        if (mYuemeiAndGroup * buyNum - dingdanAmount() != 0) {

            //找到最适合的尾款红包
            boolean tempJudge2 = false;
            int tempPos2 = 0;
            if (hongbaoLists.size() > 0) {
                float tempPreferePrice2 = hongbaoLists.get(0).getMoney();
                for (int i = 0; i < hongbaoLists.size(); i++) {
                    YouHuiCoupons youHuiCoupons = hongbaoLists.get(i);
                    float hbMoney = youHuiCoupons.getMoney();
                    if (mYuemeiAndGroup * buyNum - dingdanAmount() - hbMoney <= 0) {
                        tempJudge2 = true;
                        if (tempPreferePrice2 != hbMoney) {
                            tempPos2 = i;
                            tempPreferePrice2 = hbMoney;
                        }
                    }
                }
            }

            //如果尾款红包自己的优惠金额超过了总金额
            if (tempJudge2) {
                if (hongbaoLists.size() > 0) {
                    hbCoupon = hongbaoLists.get(tempPos2);
                }
                wkqCoupon = new YouHuiCoupons();
                Log.e(TAG, "111111");
            } else {
                Log.e(TAG, "222");
                //找到最合适的尾款劵和尾款红包
                if (weikuanLists.size() > 0 && hongbaoLists.size() > 0) {                               //有尾款劵和尾款红包

                    wkqCoupon = weikuanLists.get(0);

                    hbCoupon = hongbaoLists.get(0);

                    float tempPreferePrice3 = mYuemeiAndGroup * buyNum - dingdanAmount() - wkqCoupon.getMoney() - hbCoupon.getMoney();

                    for (int i = 0; i < weikuanLists.size(); i++) {

                        float weikuanMoney = weikuanLists.get(i).getMoney();
                        Log.e(TAG, "aaaa === " + weikuanMoney);

                        for (int j = 0; j < hongbaoLists.size(); j++) {

                            float hongbaoMoney = hongbaoLists.get(j).getMoney();
                            Log.e(TAG, "bbbb === " + hongbaoMoney);

                            float hosPrice = mYuemeiAndGroup * buyNum - dingdanAmount() - weikuanMoney - hongbaoMoney;

                            if (hosPrice <= 0) {

                                if (tempPreferePrice3 < hosPrice) {
                                    Log.e(TAG, "tempPreferePrice3 == " + tempPreferePrice3);
                                    Log.e(TAG, "hosPrice == " + hosPrice);
                                    tempPreferePrice3 = hosPrice;

                                    wkqCoupon = weikuanLists.get(i);

                                    hbCoupon = hongbaoLists.get(j);

                                } else if (tempPreferePrice3 == hosPrice) {
                                    float money1 = hbCoupon.getMoney();
                                    float money2 = hongbaoLists.get(j).getMoney();

                                    Log.e(TAG, "money1 == " + money1);
                                    Log.e(TAG, "money2 == " + money2);

                                    if (money2 > money1) {

                                        tempPreferePrice3 = hosPrice;

                                        wkqCoupon = weikuanLists.get(i);

                                        hbCoupon = hongbaoLists.get(j);

                                    }
                                }

                            }
                        }
                    }
                } else if (weikuanLists.size() > 0 && hongbaoLists.size() == 0) {                          //有尾款劵没有尾款红包
                    int tempPos4 = 0;
                    float tempPreferePrice4 = weikuanLists.get(0).getMoney();
                    for (int i = 0; i < weikuanLists.size(); i++) {
                        YouHuiCoupons youHuiCoupons = weikuanLists.get(i);
                        float wkMoney = youHuiCoupons.getMoney();
                        if (mYuemeiAndGroup * buyNum - dingdanAmount() - wkMoney <= 0 && tempPreferePrice4 != wkMoney) {
                            tempPos4 = i;
                            tempPreferePrice4 = wkMoney;
                        }
                    }

                    wkqCoupon = weikuanLists.get(tempPos4);
                    hbCoupon = new YouHuiCoupons();

                } else if (weikuanLists.size() == 0 && hongbaoLists.size() > 0) {                          //没有尾款劵有尾款红包

                    int tempPos5 = 0;
                    float tempPreferePrice5 = hongbaoLists.get(0).getMoney();
                    for (int i = 0; i < hongbaoLists.size(); i++) {
                        YouHuiCoupons youHuiCoupons = hongbaoLists.get(i);
                        float hbMoney = youHuiCoupons.getMoney();
                        if (mYuemeiAndGroup * buyNum - dingdanAmount() - hbMoney <= 0 && tempPreferePrice5 != hbMoney) {
                            tempPos5 = i;
                            tempPreferePrice5 = hbMoney;
                        }
                    }

                    wkqCoupon = new YouHuiCoupons();
                    hbCoupon = hongbaoLists.get(tempPos5);
                }
            }
        } else {
            wkqCoupon = new YouHuiCoupons();
            hbCoupon = new YouHuiCoupons();
        }
    }


    /**
     * 设置订金券数据
     */
    private void setDingJinNum() {
        String juanid = djCoupon.getCard_id();
        String juantitle = djCoupon.getTitle();
        float juanMoney = djCoupon.getMoney();

        if (Utils.isLogin()) {
            if ("0".equals(juanid)) {// 未选悦美代金券
                if (isPinTuan()) { //拼团
                    dingjinTv.setText(TEXT4);
                } else {

                    if (dingjinLists.size() > 0) {
                        dingjinTv.setText(dingjinLists.size() + "张可用");
                    } else {
                        dingjinTv.setText("0张可用");
                    }
                }

            } else {                // 已选悦美代金券
                if (isPinTuan()) { //拼团
                    dingjinTv.setText(TEXT4);
                } else {
                    if (dingjinLists.size() > 0) {
                        dingjinTv.setText("-￥" + juanMoney);
                    } else {
                        dingjinTv.setText("0张可用");
                    }
                }
            }
        } else {
            dingjinTv.setText(TEXT1);
        }
    }

    /**
     * 设置尾款券数据
     */
    private void setWeiKuanNum() {

        String juanid = wkqCoupon.getCard_id();
        String juantitle = wkqCoupon.getTitle();
        float juanMoney = wkqCoupon.getMoney();

        if (Utils.isLogin()) {
            if ("0".equals(juanid)) {// 未选悦美代金券
                if (isPinTuan()) { //拼团
                    weikuanTv.setText(TEXT5);
                } else {

                    if (weikuanLists.size() > 0) {
                        weikuanTv.setText(weikuanLists.size() + "张可用");
                    } else {
                        weikuanTv.setText("0张可用");
                    }
                }

            } else {                // 已选悦美代金券
                if (isPinTuan()) { //拼团
                    weikuanTv.setText(TEXT5);
                } else {
                    if (weikuanLists.size() > 0) {
                        weikuanTv.setText("-￥" + juanMoney);
                    } else {
                        weikuanTv.setText("0张可用");
                    }
                }
            }
        } else {
            weikuanTv.setText(TEXT2);
        }

    }

    /**
     * 设置红包数据
     */
    private void setHongbaoNum() {

        String juanid = hbCoupon.getCard_id();
        String juantitle = hbCoupon.getTitle();
        float juanMoney = hbCoupon.getMoney();

        if (Utils.isLogin()) {
            if ("0".equals(juanid)) {// 未选悦美代金券
                if (isPinTuan()) { //拼团
                    hongbaoTv.setText(TEXT6);
                } else {

                    if (hongbaoLists.size() > 0) {
                        hongbaoTv.setText(hongbaoLists.size() + "张可用");
                    } else {
                        hongbaoTv.setText("0张可用");
                    }
                }

            } else {                // 已选悦美代金券
                if (isPinTuan()) { //拼团
                    hongbaoTv.setText(TEXT6);
                } else {
                    if (hongbaoLists.size() > 0) {
                        hongbaoTv.setText("-￥" + juanMoney);
                    } else {
                        hongbaoTv.setText("0张可用");
                    }
                }
            }
        } else {
            hongbaoTv.setText(TEXT3);
        }
    }


    /**
     * 数量变化
     */
    private void numberChanges() {
//        buyNum = nbAddsub.getValue();    //购买数量
        //购买多个的提示框
//        if (buyNum == 1) {
//            moreTipsTv.setVisibility(View.GONE);
//        } else {
//            moreTipsTv.setVisibility(View.VISIBLE);
//        }

        Log.e(TAG, "mCouponsnum == " + mCouponsnum);
        if (!TextUtils.isEmpty(mCouponsnum) && !"0".equals(mCouponsnum)) {
            getYouHuiData();    //优惠劵获取
        }
    }


    /**
     * 获取当前日期
     */
    private int[] getDate() {
        Calendar mCalendar = Calendar.getInstance();

        int day = mCalendar.get(Calendar.DAY_OF_MONTH);

        int month = mCalendar.get(Calendar.MONTH) + 1;

        int year = mCalendar.get(Calendar.YEAR);

        int[] datas = {day, month, year};

        return datas;
    }


    /**
     * 判断是否是会员价下单
     *
     * @return true 是会员价下单
     */
    private boolean isMember() {
        return "1".equals(mIsMember);
    }

    /**
     * 判断是否使用过首单立减
     *
     * @return true 没有使用过
     */
    private boolean isVipFirstSingle() {
        return mVipFristPrice > 0;
    }

    /**
     * 判断订单是否是拼团
     *
     * @return：true:代表这个订单是拼团订单
     */
    private boolean isPinTuanSKU() {
        return "1".equals(mIsGroup);
    }

    /**
     * 判断订单是否是拼团，且是拼团下单的
     *
     * @return：true:代表这个订单是拼团订单且是拼团下单的
     */
    private boolean isPinTuan() {
        return "1".equals(mIsGroup) && "1".equals(mType);
    }

    /**
     * 是否是预定
     *
     * @return：true:是预定
     */
    private boolean isReservation() {
        return "3".equals(mPayType);
    }

    /**
     * 判断是否是返现
     *
     * @return：true:是返现
     */
    private boolean isFanxian() {
        return !"0".equals(mIsFanxian);
    }

    /**
     * 是否有保险
     *
     * @return：true:有保险
     */
    private boolean isBaoXian() {
        return "1".equals(mIsBao);
    }

    /**
     * 订单提示
     *
     * @param flag：1：订单金额大于5万。 2：购买数量超过限购要求
     */
    private void showDialogXianE(String flag) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_xiane);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv = editDialog.findViewById(R.id.dialog_exit_content_tv);

        if ("1".endsWith(flag)) {
            titleTv.setText("您的订单金额已超过5万限额，无法进行线上支付，建议分次购买");
        } else {
            titleTv.setText("购买数量超过限购要求，您当前可购买0个，请重新选择");
        }

        Button trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
            }
        });
    }

}
