package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.commonview.module.api.SendEMSApi;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.model.bean.GroupRongIMData;
import com.module.my.controller.activity.OnlineKefuWebActivity;
import com.module.my.controller.other.OrderDetailWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * 订单详情
 *
 * @author Rubin
 */
public class OrderDetailActivity extends BaseActivity {

    private final String TAG = "OrderDetailActivity";

    @BindView(id = R.id.wan_beautiful_web_back, click = true)
    private RelativeLayout back;// 返回

    @BindView(id = R.id.wan_beautifu_web_det_scrollview4, click = true)
    private MyElasticScrollView scollwebView;
    @BindView(id = R.id.wan_beautifu_linearlayout4, click = true)
    private LinearLayout contentWeb;
    @BindView(id = R.id.oder_time_next, click = true)
    private RelativeLayout next;// 客服

    @BindView(id = R.id.order_detail_rly, click = true)
    private RelativeLayout order_detail_Rly;// 订单详情
    @BindView(id = R.id.order_zhuizong_rly, click = true)
    private RelativeLayout order_zhuiz_Rly;// 订单追踪
    @BindView(id = R.id.order_detail_tv)
    private TextView order_detail_tv;
    @BindView(id = R.id.order_zhuizong_tv)
    private TextView order_zhuiz_tv;
    @BindView(id = R.id.order_detail_line)
    private View order_detail_line;
    @BindView(id = R.id.order_zhuizong_line)
    private View order_zhuiz_line;

    private WebView docDetWeb;

    private OrderDetailActivity mContex;

    public JSONObject obj_http;
    private String order_id;
    private String flag;

    public final Calendar mCalendar = Calendar.getInstance();
    public int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    public int month = mCalendar.get(Calendar.MONTH);

    public int year = mCalendar.get(Calendar.YEAR);


    private phonePopupWindows zixunPop;
    private String phoneStr = "";
    @BindView(id = R.id.all_content_ly)
    private LinearLayout conetnLy;

    private PageJumpManager pageJumpManager;
    public GroupRongIMData groupRIMdata1;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private HashMap<String, Object> urlMap = new HashMap<>();

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_order_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = OrderDetailActivity.this;

        pageJumpManager = new PageJumpManager(mContex);
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setView(conetnLy);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new OrderDetailWebViewClient(mContex));

        Intent it = getIntent();
        order_id = it.getStringExtra("order_id");
        flag = it.getStringExtra("flag");

        Log.e(TAG, "order_id == " + order_id);
        Log.e(TAG, "flag == " + flag);

        scollwebView.GetLinearLayout(contentWeb);
        initWebview();
        LodUrl1(FinalConstant.ORDER_DETAIL);

        zixunPop = new phonePopupWindows(mContex, conetnLy);

        scollwebView.setonRefreshListener(new OnRefreshListener1() {

            @Override
            public void onRefresh() {
                webReload();
            }
        });

    }

    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData() {
        scollwebView.onRefreshComplete();
    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
            scollwebView.onRefreshComplete();
        }
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        String latitude = Cfg.loadStr(mContex, FinalConstant.DW_LATITUDE, "");
        String longtitude = Cfg.loadStr(mContex, FinalConstant.DW_LONGITUDE, "");
        baseWebViewClientMessage.startLoading();
        urlMap.put("order_id",order_id);
        if (!TextUtils.isEmpty(flag)){
            urlMap.put("flag",flag);
        }
        if (TextUtils.isEmpty(latitude)){
            urlMap.put("lon","0");
        }else {
            urlMap.put("lon",longtitude);
        }
        if (TextUtils.isEmpty(latitude)){
            urlMap.put("lat","0");
        }else {
            urlMap.put("lat",latitude);
        }
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr,urlMap);
        if (docDetWeb != null) {
            docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }
    }


    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.wan_beautiful_web_back:
                finish();
                break;
            case R.id.order_detail_rly:// 订单详情
                order_detail_tv.setTextColor(Color.parseColor("#ff5c77"));
                order_zhuiz_tv.setTextColor(Color.parseColor("#333333"));
                order_zhuiz_line.setVisibility(View.GONE);
                order_detail_line.setVisibility(View.VISIBLE);
                LodUrl1(FinalConstant.ORDER_DETAIL);
                break;
            case R.id.order_zhuizong_rly:
                order_detail_tv.setTextColor(Color.parseColor("#333333"));
                order_zhuiz_tv.setTextColor(Color.parseColor("#ff5c77"));
                order_zhuiz_line.setVisibility(View.VISIBLE);
                order_detail_line.setVisibility(View.GONE);
                LodUrl1(FinalConstant.ORDER_SEARCH);
                break;
            case R.id.oder_time_next:// 客服
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent it = new Intent();
                it.setClass(mContex, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case 10:
                if (data != null) {
                    webReload();
                }
                break;

        }
    }

    EditText phoneEt;
    EditText codeEt;
    RelativeLayout getCodeRly;
    TextView getCodeTv;

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class phonePopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public phonePopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext,
                    R.layout.pop_zixun_getphone, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            Button cancelBt = view.findViewById(R.id.cancel_bt);
            Button zixunBt = view.findViewById(R.id.zixun_bt);
            phoneEt = view.findViewById(R.id.no_pass_login_phone_et);
            codeEt = view.findViewById(R.id.no_pass_login_code_et);
            getCodeRly = view.findViewById(R.id.no_pass_yanzheng_code_rly);
            getCodeTv = view.findViewById(R.id.yanzheng_code_tv);

            zixunBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phone = phoneEt.getText().toString();
                    String codestr = codeEt.getText().toString();
                    if (phone.length() > 0) {
                        if (ifPhoneNumber()) {
                            if (codestr.length() > 0) {
                                initCode1();
                            } else {
                                ViewInject.toast("请输入验证码");
                            }
                        } else {
                            ViewInject.toast("请输入正确的手机号");
                        }
                    } else {
                        ViewInject.toast("请输入手机号");
                    }
                }
            });

            getCodeRly.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phone = phoneEt.getText().toString();
                    if (phone.length() > 0) {
                        if (ifPhoneNumber()) {
                            sendEMS();
                        } else {
                            ViewInject.toast("请输入正确的手机号");
                        }
                    } else {
                        ViewInject.toast("请输入手机号");
                    }
                }
            });

            cancelBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    private boolean ifPhoneNumber() {
        String textPhn = phoneEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

    void sendEMS() {
        String phone = phoneEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", Utils.getUid());
        maps.put("phone", phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    ViewInject.toast("请求成功");

                    getCodeRly.setClickable(false);
                    codeEt.requestFocus();

                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {

                            getCodeTv.setTextColor(getResources()
                                    .getColor(R.color.button_zi));
                            getCodeTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            getCodeTv
                                    .setTextColor(getResources()
                                            .getColor(
                                                    R.color.button_bian_hong1));
                            getCodeRly.setClickable(true);
                            getCodeTv.setText("重发验证码");
                        }
                    }.start();
                } else {
                    ViewInject.toast(s.message);
                }
            }
        });
    }

    void initCode1() {
        String codeStr = codeEt.getText().toString();
        String phone = phoneEt.getText().toString();
        phoneStr = phone;
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", Utils.getUid());
        maps.put("phone", phone);
        maps.put("code", codeStr);
        maps.put("flag", "1");
        new SecurityCodeApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    Cfg.saveStr(mContex, FinalConstant.UPHONE, phoneStr);
                    zixunPop.dismiss();
                    baseWebViewClientMessage.stopLoading();
                    ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                            .setDirectId("")
                            .setObjId("0")
                            .setObjType("0")
                            .setYmClass("0")
                            .setYmId("0")
                            .build();
                    pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
                } else {
                    baseWebViewClientMessage.stopLoading();
                    ViewInject.toast(s.message);
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
        webReload();
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

}
