/**
 * 
 */
package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.my.controller.activity.OnlineKefuWebActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyElasticScrollView;
import com.quicklyask.view.MyElasticScrollView.OnRefreshListener1;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.net.URLDecoder;
import java.util.HashMap;

/**
 * 申请退款成功
 * 
 * @author Robin
 * 
 */
public class MoneyBackSuccessActivity extends BaseActivity {

	private final String TAG = "MoneyBackSuccessActivity";

	@BindView(id = R.id.wan_beautifu_web_det_scrollview3, click = true)
	private MyElasticScrollView scollwebView;
	@BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.wan_beautiful_web_back, click = true)
	private RelativeLayout back;// 返回
	@BindView(id = R.id.wan_beautifu_docname)
	private TextView titleBarTv;
	@BindView(id = R.id.tao_web_share_rly111, click = true)
	private RelativeLayout shareBt;// 悦美客服

	private WebView docDetWeb;

	private MoneyBackSuccessActivity mContext;

	public JSONObject obj_http;

	private String uid;

	private String refund_id;
	private String repayment_type;
	private HashMap<String, Object> urlMap = new HashMap<>();
	private BaseWebViewClientMessage mBaseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_moneyback_success);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = MoneyBackSuccessActivity.this;
		uid = Utils.getUid();

		Intent it = getIntent();
        refund_id = it.getStringExtra("refund_id");

		scollwebView.GetLinearLayout(contentWeb);
		mBaseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
		mBaseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();
		LodUrl(FinalConstant.MONEYBACK_SUECC);

		titleBarTv.setText("申请退款");


		scollwebView.setonRefreshListener(new OnRefreshListener1() {

			@Override
			public void onRefresh() {
				webReload();
			}
		});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						MoneyBackSuccessActivity.this.finish();
					}
				});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint({ "SetJavaScriptEnabled", "InlinedApi" })
	public void initWebview() {
		docDetWeb = new WebView(mContext);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.getSettings().supportMultipleWindows();
		docDetWeb.getSettings().setNeedInitialFocus(true);
		docDetWeb.setWebViewClient(mBaseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		contentWeb.addView(docDetWeb);
	}


	public void webReload() {
		if (docDetWeb != null) {
			mBaseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 医生详情页
				try {
					String id = obj.getString("id");
					String docname = URLDecoder.decode(
							obj.getString("docname"), "utf-8");

					Intent it = new Intent();
					it.setClass(mContext, DoctorDetailsActivity592.class);
					it.putExtra("docId", id);
					it.putExtra("docName", docname);
					it.putExtra("partId", "");
					startActivity(it);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}


			if (obj.getString("type").equals("6")) {// 问答详情
				String link = obj.getString("link");
				String qid = obj.getString("id");

				Intent it = new Intent();
				it.setClass(mContext, DiariesAndPostsActivity.class);
				it.putExtra("url", link);
				it.putExtra("qid", qid);
				startActivity(it);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl(String url) {
		mBaseWebViewClientMessage.startLoading();

		urlMap.put("refund_id",refund_id);
		WebSignData addressAndHead = SignUtils.getAddressAndHead(url,urlMap);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.wan_beautiful_web_back:
			onBackPressed();
			// finish();
			break;
		case R.id.tao_web_share_rly111:// 填码得劵

			Intent it = new Intent();
			it.setClass(mContext, OnlineKefuWebActivity.class);
			it.putExtra("link", "/service/zxzx/");
			it.putExtra("title", "在线客服");
			startActivity(it);

			break;
		}
	}


	public void onResume() {
		super.onResume();
		StatService.onResume(this);
		MobclickAgent.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		StatService.onPause(this);
		MobclickAgent.onPause(this);
		TCAgent.onPause(this);
	}
}
