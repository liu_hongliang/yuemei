/**
 *
 */
package com.module.my.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.CancelCollectApi;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.controller.adapter.HosListAdpter;
import com.module.doctor.model.bean.HosListData;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.CollectHosApi;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收藏的医院
 *
 * @author Robin
 */
public class CollectHosFragment extends ListFragment {

    private final String TAG = "CollectHosFragment";

    private Activity mCotext;

    private LinearLayout docText;
    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;

    private List<HosListData> lvHotIssueData1 = new ArrayList<HosListData>();
    private List<HosListData> lvHotIssueMoreData1 = new ArrayList<HosListData>();
    private HosListAdpter hotAdpter1;

    // private String key;
    // private String city;
    private LinearLayout nodataTv;

    private String uid;
    private LoadingProgress mDialog;
    private CollectHosApi collectHosApi;
    private HashMap<String, Object> collectHosMap = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_collect_san_550, container,
                false);
        nodataTv = v
                .findViewById(R.id.my_collect_post_tv_nodata);
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mCotext = getActivity();
        mDialog = new LoadingProgress(mCotext);

        collectHosApi = new CollectHosApi();

        if (isAdded()) {
            uid = Utils.getUid();
        }
        // key = getArguments().getString("key");
        // city = getArguments().getString("city");
        mlist = (DropDownListView) getListView();
        initList();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
        StatService.onResume(getActivity());

        initList();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Log.e(TAG, "onStart");
        uid = Utils.getUid();
    }

    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long arg3) {

                DeleteDialog(pos);

                return true;
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {

                if (lvHotIssueData1 != null && null != lvHotIssueData1.get(pos).getUrl() && lvHotIssueData1.get(pos).getUrl().length() > 0) {

                    String hosid = lvHotIssueData1.get(pos).getHos_id();
                    Intent it = new Intent();
                    it.setClass(getActivity(), HosDetailActivity.class);
                    it.putExtra("hosid", hosid);
                    startActivity(it);
                } else {
                    ViewInject.toast("该医院已下线");
                }

            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {

        collectHosMap.put("id", uid);
        collectHosMap.put("page", mCurPage + "");
        collectHosApi.getCallBack(mCotext, collectHosMap, new BaseCallBackListener<List<HosListData>>() {
            @Override
            public void onSuccess(List<HosListData> hosListDatas) {
                Message msg = null;
                if (isDonwn) {
                    if (mCurPage == 1) {
                        lvHotIssueData1 = hosListDatas;

                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    mCurPage++;
                    lvHotIssueMoreData1 = hosListDatas;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (null != lvHotIssueData1 && lvHotIssueData1.size() > 0) {
                            nodataTv.setVisibility(View.GONE);

                            mDialog.stopLoading();
                            if (null != getActivity()) {
                                hotAdpter1 = new HosListAdpter(getActivity(),
                                        lvHotIssueData1);
                                setListAdapter(hotAdpter1);
                            }
                            // Utils.setListViewHeightBasedOnChildren(mlist);

                            mlist.onDropDownComplete();
                            mlist.onBottomComplete();
                        } else {
                            mDialog.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            // ViewInject.toast("您还没有收藏的专家");
                        }
                        break;
                    case 2:
                        if (null != lvHotIssueMoreData1
                                && lvHotIssueMoreData1.size() > 0) {
                            hotAdpter1.add(lvHotIssueMoreData1);
                            hotAdpter1.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    /**
     * 长按item删除对话框
     *
     * @param pos
     */
    private void DeleteDialog(final int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("      确定取消对该医院的收藏？");
        // 确定按钮监听
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String userid = lvHotIssueData1.get(pos).getHos_id();
                deleteCollect(userid);

                lvHotIssueData1.remove(pos);

                if (lvHotIssueData1.equals("") || lvHotIssueData1 == null) {
                    nodataTv.setVisibility(View.VISIBLE);
                }

                if (lvHotIssueData1.size() == 1 && pos == 0) {
                    nodataTv.setVisibility(View.VISIBLE);
                    mlist.setVisibility(View.GONE);
                }

                hotAdpter1.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
        builder.create().setCanceledOnTouchOutside(false);
    }

    /**
     * 取消对专家的收藏
     *
     * @param
     */
    void deleteCollect(String hosid) {
        CancelCollectApi cancelCollectApi = new CancelCollectApi();
        Map<String, Object> params = new HashMap<>();
        params.put("uid", uid);
        params.put("objid", hosid);
        params.put("type", "3");
        cancelCollectApi.getCallBack(mCotext, params, new BaseCallBackListener() {
            @Override
            public void onSuccess(Object o) {

            }
        });

    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
        StatService.onPause(getActivity());
    }
}
