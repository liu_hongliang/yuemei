package com.module.my.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;

import com.module.base.view.YMBaseFragment;
import com.module.my.controller.other.UploadStatusException;
import com.module.my.view.view.ImageVideoUploadView;
import com.module.other.other.RequestAndResultCode;
import com.quicklyask.activity.R;

import butterknife.BindView;

/**
 * 图片和视频Fragment
 * Created by 裴成浩 on 2019/9/29
 */
public class PostingAndNoteImageVideoFragment extends YMBaseFragment {
    @BindView(R.id.post_note_img_video)
    public ImageVideoUploadView mImgVideo;                         //图片列表

    public static PostingAndNoteImageVideoFragment newInstance() {
        PostingAndNoteImageVideoFragment fragment = new PostingAndNoteImageVideoFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragmet_post_image_video_view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void initView(View view) {
        mImgVideo.initView(true);

        mImgVideo.setOnEventClickListener(new ImageVideoUploadView.OnEventClickListener() {
            @Override
            public void onVideoCoverClick(int visibility) {
                Log.e(TAG, "visibility == " + visibility);
                if (onEventClickListener != null) {
                    onEventClickListener.onVideoCoverClick(visibility);
                }
            }

            @Override
            public void onVideoCoverPathClick(String qiNiukey, String path) {
                Log.e(TAG, "path == " + path);
                if (onEventClickListener != null) {
                    onEventClickListener.onVideoCoverPathClick(qiNiukey, path);
                }
            }

            @Override
            public void startFragmentForResult(Intent intent, int requestCode) {
                startActivityForResult(intent, requestCode);
            }
        });
    }

    @Override
    protected void initData(View view) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case RequestAndResultCode.VIDEO_REQUEST_CODE:      //选择视频的回调
                    if (data != null) {
                        mImgVideo.setRequestVideo(data);
                    }
                    break;

                case RequestAndResultCode.IMAGE_REQUEST_CODE:        //选择图片回调
                    if (resultCode == Activity.RESULT_OK) {
                        if (data != null) {
                            mImgVideo.setRequestImage(data);
                        }
                    }
                    break;

                case RequestAndResultCode.ACTION_REQUEST_EDITIMAGE:  //添加贴纸后的回调
                    if (null != data) {
                        mImgVideo.setStickersData(data);
                    }
                    break;
            }
        } catch (UploadStatusException e) {
            Log.e(TAG, "e === " + e.getMsg());
        }
    }

    public interface OnEventClickListener {
        void onVideoCoverClick(int visibility);

        void onVideoCoverPathClick(String qiNiukey, String path);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
