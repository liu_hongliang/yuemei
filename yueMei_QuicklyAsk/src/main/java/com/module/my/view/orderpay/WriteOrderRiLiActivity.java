package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.YuDingApi;
import com.module.commonview.module.bean.YuDingData;
import com.module.community.model.bean.ExposureLoginData;
import com.module.my.controller.activity.InSureActivity;
import com.module.my.model.api.CommitSecurityCodeInLoginApi;
import com.module.my.model.api.GetUserPhoneNumberApi;
import com.module.my.model.api.InitCode1Api;
import com.module.my.model.api.InitGetJifenApi;
import com.module.my.model.api.InitOrderMessage;
import com.module.my.model.api.InitPayOrderApi;
import com.module.my.model.api.InsuranceMessageApi;
import com.module.my.model.api.SecuritycodeApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.bean.BaoXianData;
import com.module.my.model.bean.JiFenMoenyData;
import com.module.my.model.bean.KeFuData;
import com.module.my.model.bean.LoginData;
import com.module.my.model.bean.SaoXiadanData;
import com.module.my.model.bean.TaoOrderInfoData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.UserPhoneData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.BaoxianPopWindow;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.quicklyask.view.MyScrollView;
import com.quicklyask.view.MyToast;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.appcpa.Order;
import com.tendcloud.appcpa.TalkingDataAppCpa;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.utils.Log;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * 带日历的填写订单
 * 
 * @author dwb
 * 
 */
public class WriteOrderRiLiActivity extends BaseActivity {

	private final String TAG = "WriteOrderRiLiActivity";

	@BindView(id = R.id.order_time_back, click = true)
	private RelativeLayout back;// 返回
	@BindView(id = R.id.oder_time_next, click = true)
	private RelativeLayout next;// 下一步
	@BindView(id = R.id.oder_time_time_select_rly, click = true)
	private RelativeLayout timeSelectRly;// 选择时间
	@BindView(id = R.id.order_time_date_tv)
	private TextView dateTv;// 时间显示
	private String tipsStr = "请选择预计到院时间";

	private Context mContex;

	private final Calendar mCalendar = Calendar.getInstance();

	private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

	private int month = mCalendar.get(Calendar.MONTH);

	private int year = mCalendar.get(Calendar.YEAR);

	private String dateStr = "";
	private String taoid;
	private String docid;

	private String hosName;
	private String titleContetn;
	private String docName;
	private String hosPrice;
	private String netPrice;
	private String payType;

	@BindView(id = R.id.input_order_name_et)
	private EditText nameEt;
	@BindView(id = R.id.input_order_other_et)
	private EditText otherEt;

	@BindView(id = R.id.order_phone_number_et)
	private EditText phoneNumberEt;
	@BindView(id = R.id.order_phone_code_et)
	private EditText phoneCode;

	@BindView(id = R.id.yanzheng_code_rly)
	private RelativeLayout sendEMSRly;
	@BindView(id = R.id.yanzheng_code_tv)
	private TextView emsTv;

	@BindView(id = R.id.order_write_name_tv)
	private TextView titleNameTv;

	private String uphone;
	private String phone;
	private String codeStr;

	private UserData userData;
	private LoginData loginData;
	GradientDrawable drawable1;
	GradientDrawable drawable2;

	private String uid;

	@BindView(id = R.id.write_scoll_content_s1)
	private MyScrollView ss1;

	@BindView(id = R.id.sumbit_order_bt, click = true)
	private Button sumnbit;

	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout contentAllly;

	private boolean isSoftCX = false;

	@BindView(id = R.id.oder_shifu_or_dingdan)
	private TextView shifuOrdingdanTv;
	@BindView(id = R.id.order_method_shifu_tv)
	private TextView shifuTv;

	@BindView(id = R.id.yd_yuemei_jia_tv)
	private TextView yuemeiJiaTv;// 悦美价
	@BindView(id = R.id.yd_zhiding_lijian_tv)
	private TextView zhidingLijianTv;// 直订立减
	@BindView(id = R.id.yd_zhiding_lijian_rly)
	private RelativeLayout zhidingRly;// 直订立减框
	@BindView(id = R.id.yd_online_dingjin_tv)
	private TextView onlineDingjinTv;// 在线支付定金
	@BindView(id = R.id.yd_online_dingjin_rly)
	private RelativeLayout onlineDingjinRly;// 在线支付定金框
	@BindView(id = R.id.yd_daoyuan_zhifu_tv)
	private TextView daoYuanTv;// 到院支付
	@BindView(id = R.id.yd_daoyuan_zhifu_rly)
	private RelativeLayout daoyuanRly;// 到院支付框

	private String zhifuStr;

	private YuDingData ydData = new YuDingData();

	private String ifUseJifen = "0";
	private String discount_id;
	private String priceintegral;
	private String integral;

	@BindView(id = R.id.order_phone_if_huifang_ly)
	private RelativeLayout switchRly;
	@BindView(id = R.id.order_jifen_tv)
	private TextView jifenTv;

	String server_id;
	String order_id;

	private KeFuData kefuData;

	@BindView(id = R.id.write_phone_bangding_ly, click = true)
	private RelativeLayout bangdingLy;// 绑定手机框
	@BindView(id = R.id.write_bangding_phone_tv)
	private TextView phoneTv;// 绑定的手机号
	@BindView(id = R.id.input_order_message_ly)
	private LinearLayout tianMessageLy;// 验证手机号框

	private String yuemeiStr;
	private String lijianStr;

	@BindView(id = R.id.nocode_message_rly, click = true)
	private RelativeLayout nocodeRly;
	@BindView(id = R.id.nocode_message_tv)
	private TextView nocodeTv;

	private PopupWindows yuyinCodePop;
	@BindView(id = R.id.order_time_all_ly)
	private LinearLayout allcontent;

	@BindView(id = R.id.login_tips_rly)
	private RelativeLayout loginTipsRly;
	@BindView(id = R.id.login_tips_ly, click = true)
	private LinearLayout loginLy;

	private String source="0";
	private String objid="0";

	boolean noModify = true;

	//保险
	private String is_bao="0";
	private String baoxian="";
	private float baoxianJia=0;
	private String isbaoxian="0";

	@BindView(id = R.id.yd_online_isbaoxian_rly)
	private RelativeLayout baoxianJiaLy;
	@BindView(id = R.id.yd_online_isbaoxian_tv)
	private TextView baoxianJiaTv;

	@BindView(id = R.id.baoxian_content_ly)
	private LinearLayout baoxianContentLy;
	@BindView(id = R.id.baoxian_title_tv)
	private TextView baoxianTv;
	@BindView(id = R.id.baoxian_check_ly, click = true)
	private RelativeLayout baoxianCheckLy;
	@BindView(id = R.id.baoxian_check_iv)
	private ImageView baoxianCheckIv;
	@BindView(id = R.id.baoxian_tips_iv, click = true)
	private ImageView baoxianTipsIv;
	@BindView(id = R.id.baoxian_fee_tv)
	private TextView baoxianFeeTv;
	@BindView(id = R.id.baoxian_is_ly)
	private LinearLayout baoxianMessageLy;
	@BindView(id = R.id.baoxian_name_and_phone_tv)
	private TextView baoxianMessageTv;
	@BindView(id = R.id.baoxian_updatemessage_tv, click = true)
	private TextView baoxianUpdateTv;


	private BaoXianData bxData;

	private BaoxianPopWindow baoxianPop;

	private String isBaoxinMessage="0";

	private String insureNmae="";
	private String insurePhone="";
	private String insureIDcard="";
	private String insureSex="";

	private String finalPrice;// 最后支付价格

	private GetPhoneCodePopWindow phoneCodePop;
	private PageJumpManager pageJumpManager;
	private String isGroup;
	private String groupId;
	private String mReferrer;
	private String mReferrerId;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_rili_yuyue_write);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = WriteOrderRiLiActivity.this;
		pageJumpManager = new PageJumpManager(mContex);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		uid = Utils.getUid();

		Intent it = getIntent();
		taoid = it.getStringExtra("taoid");
		payType = it.getStringExtra("payType");
		docid = it.getStringExtra("docid");
		source = it.getStringExtra("source");
		objid = it.getStringExtra("objid");
		isGroup = it.getStringExtra("is_group");
		groupId = it.getStringExtra("group_id");
		if(groupId == null){
			groupId = "";
		}

		initOrderMessage();

		baoxianUpdateTv.setText(Html.fromHtml(("<u>" + "更改信息>" + "</u>")));
		String loadStr = Cfg.loadStr(this, FinalConstant.EXPOSURE_LOGIN, "");
		HashMap<String, String> hashMap = new HashMap<>();
		if (!TextUtils.isEmpty(loadStr)) {
			ExposureLoginData exposureLoginData = new Gson().fromJson(loadStr, ExposureLoginData.class);
			mReferrer = exposureLoginData.getReferrer();
			mReferrerId = exposureLoginData.getReferrer_id();
			hashMap.put("referrer", mReferrer);
			hashMap.put("referrer_id", mReferrerId);
		}else{
			hashMap.put("referrer", "0");
			hashMap.put("referrer_id", "0");
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);

		uid = Utils.getUid();
		if (Utils.isLogin()) {
			if (noModify) {
				initGetPhone();
			}
			initGetJifen();
			loginTipsRly.setVisibility(View.GONE);
			titleNameTv.setVisibility(View.VISIBLE);
		} else {
			bangdingLy.setVisibility(View.GONE);
			tianMessageLy.setVisibility(View.VISIBLE);
			loginTipsRly.setVisibility(View.VISIBLE);
			titleNameTv.setVisibility(View.GONE);
		}

		initYudingData();

		((CompoundButton) findViewById(R.id.switch_if_jifen))
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
												 boolean isChecked) {
						if (isChecked) {
							ifUseJifen = "1";
						} else {
							ifUseJifen = "0";
						}
					}
				});

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						WriteOrderRiLiActivity.this.finish();
					}
				});

		drawable1 = new GradientDrawable();
		drawable1.setShape(GradientDrawable.RECTANGLE); // 画框
		drawable1.setStroke(1, getResources().getColor(R.color.red_ff4965)); // 边框粗细及颜色
		drawable1.setColor(0xFFFFFFFF); // 边框内部颜色

		drawable2 = new GradientDrawable();
		drawable2.setShape(GradientDrawable.RECTANGLE); // 画框
		drawable2.setStroke(1, getResources().getColor(R.color.button_bian2)); // 边框粗细及颜色
		drawable2.setColor(0xFFFFFFFF); // 边框内部颜色

		sendEMSRly.setBackgroundDrawable(drawable1); // 设置背景（效果就是有边框及底色）

		sendEMSRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String phones = phoneNumberEt.getText().toString().trim();
				if (phones.length() > 0 && null != phones) {
					if (ifPhoneNumber()) {
						sendEMSRly.setClickable(false);
						sendEMS();

						nocodeRly.setVisibility(View.VISIBLE);
						nocodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
								+ "</u>"));
						// phoneNumberEt.clearFocus();
						phoneCode.requestFocus();
						InputMethodManager inputManager = (InputMethodManager) phoneCode
								.getContext().getSystemService(
										Context.INPUT_METHOD_SERVICE);
						inputManager.showSoftInput(phoneCode, 0);
						// phoneCode.clearFocus();

						new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

							@Override
							public void onTick(long millisUntilFinished) {
								sendEMSRly.setBackgroundDrawable(drawable2);
								emsTv.setTextColor(getResources().getColor(
										R.color.button_zi));
								emsTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
							}

							@Override
							public void onFinish() {
								sendEMSRly.setBackgroundDrawable(drawable1);
								emsTv.setTextColor(getResources().getColor(
										R.color.button_bian_hong1));
								sendEMSRly.setClickable(true);
								emsTv.setText("重新获取验证码");
							}
						}.start();
					} else {
						ViewInject.toast("请输入正确的手机号");
					}
				} else {
					ViewInject.toast("请输入手机号");
				}
			}
		});


	}

	void initYudingData() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("id",taoid);
		new YuDingApi().getCallBack(mContex, maps, new BaseCallBackListener<YuDingData>() {
			@Override
			public void onSuccess(YuDingData ydData) {
				is_bao=ydData.getIs_bao();
				baoxian=ydData.getBaoxian();
				baoxianJia=Float.parseFloat(ydData.getBao_price());

				yuemeiJiaTv.setText("￥" + ydData.getPrice());

				if(baoxianJia<=0){
					baoxianJiaTv.setText("￥0");
				}else {
					baoxianJiaTv.setText("￥"+baoxianJia);
				}

				onlineDingjinTv.setText("￥" + ydData.getDingjin());

				if (Integer.parseInt(ydData.getLijian()) == 0) {
					zhidingRly.setVisibility(View.GONE);
				} else {
					zhidingRly.setVisibility(View.VISIBLE);
				}

				if (payType.equals("3")) {// 预订
					shifuTv.setText("￥" + ydData.getDingjin());
					zhifuStr = ydData.getDingjin();
					finalPrice=zhifuStr;

					zhidingRly.setVisibility(View.GONE);
					shifuOrdingdanTv.setText("在线支付订金");

					lijianStr = "0";
				} else if (payType.equals("4")) {// 全款
					int yuemeiInt = Integer.parseInt(ydData
							.getPrice());// 悦美价
					yuemeiStr = yuemeiInt + "";
					int lijianInt = Integer.parseInt(ydData
							.getLijian());// 立减
					lijianStr = lijianInt + "";
					int onlineInt = yuemeiInt - lijianInt;

					daoYuanTv.setText("￥" + "0");
					daoyuanRly.setVisibility(View.GONE);

					shifuTv.setText("￥" + onlineInt);
					zhifuStr = onlineInt + "";
					finalPrice=zhifuStr;

					onlineDingjinRly.setVisibility(View.GONE);
					shifuOrdingdanTv.setText("在线支付金额");
				}


				if(is_bao.equals("1")){
					baoxianContentLy.setVisibility(View.VISIBLE);
					baoxianTv.setText(baoxian);

					if(baoxianJia==0){
						baoxianFeeTv.setText("免费");
						baoxianFeeTv.setTextColor(Color.parseColor("#ff5c77"));
					}else {
						baoxianFeeTv.setText("￥"+baoxianJia);
						baoxianFeeTv.setTextColor(Color.parseColor("#333333"));
					}

					uid = Utils.getUid();
					if(Utils.isLogin()){
						getBaoXianMessage();
					}else {
						baoxianMessageLy.setVisibility(View.GONE);
					}

				}else {
					baoxianContentLy.setVisibility(View.GONE);

				}
			}


		});
	}


	void getBaoXianMessage(){
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",uid);
		new InsuranceMessageApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					try {
						bxData=JSONUtil.TransformSingleBean(serverData.data,BaoXianData.class);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(null!=bxData){
						if(null!=bxData.getName()&&bxData.getName().length()>0){

							isBaoxinMessage="1";
							baoxianMessageLy.setVisibility(View.VISIBLE);
							String name=bxData.getName();
							insureNmae=name;
							String phone=bxData.getPhone();
							insurePhone=phone;
							insureIDcard=bxData.getIdcard();
							insureSex=bxData.getSex();

							baoxianMessageTv.setText("被保险人："+name+" ("+phone.substring(0,3)+"****"+phone.substring(7,phone.length())+")");
							isbaoxian="1";
							baoxianCheckIv.setBackgroundResource(R.drawable.xuanzhong_2x);

							baoxianJiaLy.setVisibility(View.VISIBLE);
							float allp=baoxianJia+Float.parseFloat(zhifuStr);
							shifuTv.setText("￥" + allp);
							finalPrice=allp+"";

						}else {
							baoxianMessageLy.setVisibility(View.GONE);

						}
					}else {
						baoxianMessageLy.setVisibility(View.GONE);

					}
				}
			}

		});
	}

	private boolean ifPhoneNumber() {
		String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	/**
	 * 获取手机号
	 */
	void initGetPhone() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",uid);
		new GetUserPhoneNumberApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					UserPhoneData userPhoneData = JSONUtil.TransformUserPhone(serverData.data);
					uphone = userPhoneData.getPhone();
					phone = uphone;
					String spj = uphone;
					if (phone.equals("0")) {
						bangdingLy.setVisibility(View.GONE);
						tianMessageLy.setVisibility(View.VISIBLE);
					} else {
						bangdingLy.setVisibility(View.VISIBLE);
						tianMessageLy.setVisibility(View.GONE);

						String a = spj.substring(0, 3);
						String b = spj.substring(phone.length() - 4,
								spj.length());
						String ss = a + "****" + b;
						phoneTv.setText(ss);
					}
				}
			}

		});
	}

	/**
	 * 获取用户颜值币
	 */
	void initGetJifen() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("id",taoid);
		maps.put("uid",uid);
		new InitGetJifenApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					JiFenMoenyData jifdata = JSONUtil.TransformJFMoney(serverData.data);

					discount_id = jifdata.getDiscount_id();
					priceintegral = jifdata.getPriceintegral();// 可用颜值币
					integral = jifdata.getIntegral();// 颜值币

					jifenTv.setText(integral);
					if (integral.equals("0")) {
						switchRly.setVisibility(View.GONE);
					}
				}
			}

		});
	}

	/**
	 * 获取订单信息
	 */
	void initOrderMessage() {
		Map<String,Object>maps=new HashMap<>();
		maps.put("uid",taoid);
		new InitOrderMessage().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					TaoOrderInfoData taoData = JSONUtil.TransformOrderInfo(serverData.data);
					hosName = taoData.getHos_name();
					docName = taoData.getDoc_name();
					titleContetn = taoData.getTitle();
					hosPrice = taoData.getPrice_discount();// 到院支付
					netPrice = taoData.getDiscountprice();// 在线支付

					titleNameTv.setText(titleContetn);
				}
			}

		});
	}

	void sendEMS() {
		phone = phoneNumberEt.getText().toString();
		Map<String,Object> maps=new HashMap<>();
		maps.put("phone",phone);
		maps.put("flag","1");
		new SendEMSApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					ViewInject.toast(serverData.message);
				} else {
					ViewInject.toast(serverData.message);
				}
			}

		});
	}

	void initPayOrder() {
		uid = Utils.getUid();
		String otherStr = "";
		otherStr = otherEt.getText().toString();

		Map<String,Object>maps=new HashMap<>();
		maps.put("uid", uid);
		maps.put("phone", phone);
		maps.put("price", zhifuStr);
		maps.put("docid", docid);
		// params.put("doc_assistant_id", doc_assistant_id);
		maps.put("order_id", "0");
		maps.put("beizhu", otherStr);
		maps.put("tao_id", taoid);
		maps.put("source", source);
		maps.put("objid", objid);

		//保险人信息
		maps.put("is_insure", isbaoxian);
		maps.put("insure_name", insureNmae);
		maps.put("insure_phone", insurePhone);
		maps.put("insure_sex", insureSex);
		maps.put("insure_card_id", insureIDcard);
		new InitPayOrderApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					try {
						SaoXiadanData sdata = JSONUtil.TransformSingleBean(serverData.data,
								SaoXiadanData.class);
						String server_id = sdata.getServer_id();
						String order_id = sdata.getOrder_id();
						String title = sdata.getTitle();
						String order_time = sdata.getOrder_time();
						String moeny_=sdata.getMoney();

						Intent intent = new Intent(mContex,OrderZhiFuStatus1Activity.class);

						Bundle bundle = new Bundle();
						bundle.putString("price",moeny_);
						bundle.putString("tao_title",title);
						bundle.putString("server_id",server_id);
						bundle.putString("order_id",order_id);
						bundle.putString("taoid",taoid);
						bundle.putString("order_time",order_time);
						bundle.putString("type","1");
						bundle.putString("sku_type","2");
						bundle.putString("is_repayment","0");
						bundle.putString("is_repayment_mimo", "");
						bundle.putString("weikuan","0");
						bundle.putString("number","1");
						bundle.putString("is_group",isGroup);
						bundle.putString("group_id",groupId);

						intent.putExtra("data",bundle);
						startActivity(intent);


						finish();

						Order order = Order.createOrder(order_id,(int)Float.parseFloat(moeny_), "CNY").addItem("淘整形","taoid:"+taoid,(int)(Float.parseFloat(yuemeiStr)*100),1);
						TalkingDataAppCpa.onPlaceOrder(uid, order);


					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					MyToast.makeTextToast(mContex, serverData.message, 1000).show();
				}
			}

		});

	}


	/**
	 * 未登录状态下提交验证码
	 */
	void initCode1() {
		codeStr = phoneCode.getText().toString();
		Map<String,Object>params=new HashMap<>();
		params.put("imei", Utils.getImei());
		params.put("phone", phone);
		params.put("code", codeStr);
		if (!TextUtils.isEmpty(mReferrer)){
			params.put("referrer",mReferrer);
		}
		if (!TextUtils.isEmpty(mReferrerId)){
			params.put("referrer_id",mReferrerId);
		}

		new InitCode1Api().getCallBack(mContex, params, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if ("1".equals(serverData.code)){
					userData = JSONUtil.TransformLogin(serverData.data);
					String id = userData.get_id();
					String img = userData.getImg();
					String nickName = userData.getNickname();
					String province = userData.getProvince();
					String city = userData.getCity();
					String sex = userData.getSex();
					String birthday = userData.getBirthday();

					Utils.setUid(id);
					Cfg.saveStr(mContex, FinalConstant.UHEADIMG, img);
					Cfg.saveStr(mContex, FinalConstant.UNAME, nickName);
					Cfg.saveStr(mContex, FinalConstant.UPROVINCE,
							province);
					Cfg.saveStr(mContex, FinalConstant.UCITY, city);
					Cfg.saveStr(mContex, FinalConstant.USEX, sex);
					Cfg.saveStr(mContex, FinalConstant.UBIRTHDAY, birthday);

					PushManager.startWork(getApplicationContext(),
							PushConstants.LOGIN_TYPE_API_KEY,
							Utils.BAIDU_PUSHE_KEY);

					initPayOrder();
				}else {
					sumnbit.setClickable(true);
					ViewInject.toast(serverData.message);
				}
			}
		});
	}

	/**
	 * 登录状态下提交验证码
	 */
	void initCode2() {
		codeStr = phoneCode.getText().toString();
		Map<String,Object>maps=new HashMap<>();
		maps.put("phone",phone);
		maps.put("code",codeStr);
		new CommitSecurityCodeInLoginApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				if (serverData.code.equals("1")) {
					initPayOrder();
				} else {
					sumnbit.setClickable(true);
					ViewInject.toast(serverData.message);
				}
			}

		});
	}


	@SuppressLint("NewApi")
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.order_time_back:
			if (Utils.isFastDoubleClick()) {
				return;
			}
			onBackPressed();
			break;
		case R.id.sumbit_order_bt:// 确认下单
			if (Utils.isFastDoubleClick()) {
				return;
			}
			String nameStr = nameEt.getText().toString();
			String otherStr = "";
			otherStr = otherEt.getText().toString();
			dateStr = year + "-" + (month + 1) + "-" + day;
			nameStr = "yuemei";

			if (null != uphone && uphone.length() > 0) {
				sumnbit.setText("提交中...");
				sumnbit.setClickable(false);
				initPayOrder();

			} else {
				if (dateStr.length() > 0) {
					if (nameStr.length() > 0) {
						uid = Utils.getUid();

						if (Utils.isLogin()) {
							codeStr = phoneCode.getText().toString();
							phone = phoneNumberEt.getText().toString();

							if (phone.length() > 0) {
								if (codeStr.length() > 0) {
									sumnbit.setText("提交中...");
									sumnbit.setClickable(false);
									initCode2();
								} else {
									ViewInject.toast("请输入验证码！");
								}
							} else {
								ViewInject.toast("请输入手机号！");
							}
						} else {
							codeStr = phoneCode.getText().toString();
							phone = phoneNumberEt.getText().toString();
							if (phone.length() > 0) {
								if (codeStr.length() > 0) {
									sumnbit.setText("提交中...");
									sumnbit.setClickable(false);
									Log.e(TAG, "33333");
									initCode1();
								} else {
									ViewInject.toast("请输入验证码！");
								}
							} else {
								ViewInject.toast("请输入手机号！");
							}
						}
					} else {
						ViewInject.toast("称呼不能为空");
					}
				} else {
					ViewInject.toast("请您先选择预计到院时间");
				}

			}
			break;
		case R.id.write_phone_bangding_ly:// 更改手机号
			Intent it = new Intent();
			it.setClass(mContex, OrderPhoneModifyActivity.class);
			startActivityForResult(it, 6);
			break;
		case R.id.nocode_message_rly:// 没收到 语音验证码

			yuyinCodePop = new PopupWindows(mContex, allcontent);
			yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

			Glide.with(mContex)
					.load(FinalConstant.TUXINGCODE)
					.into(codeIv);

//			SystemTool.hideKeyBoard(WriteOrderRiLiActivity.this);
//			phoneCodePop=new GetPhoneCodePopWindow(mContex);
//			phoneCodePop.showAtLocation(allcontent,Gravity.BOTTOM, 0, 0);
			break;
		case R.id.login_tips_ly:
			Utils.jumpLogin(mContex);
			break;
		case R.id.baoxian_check_ly:// 是否选择保险
				//判空

				if(isbaoxian.equals("1")){

					isbaoxian="0";
					baoxianCheckIv.setBackgroundResource(R.drawable.raido_cuo3x);
					baoxianJiaLy.setVisibility(View.GONE);

					shifuTv.setText("￥" + zhifuStr);
					finalPrice = zhifuStr;

				}else if(isbaoxian.equals("0")){

					if(isBaoxinMessage.equals("1")){
						isbaoxian="1";
						baoxianCheckIv.setBackgroundResource(R.drawable.xuanzhong_2x);
//						if(baoxianJia<=0){
//							baoxianJiaLy.setVisibility(View.GONE);
//						}else {
//							baoxianJiaLy.setVisibility(View.VISIBLE);
//						}

						baoxianJiaLy.setVisibility(View.VISIBLE);

						float allp=baoxianJia+Float.parseFloat(zhifuStr);
						shifuTv.setText("￥" + allp);
						finalPrice=allp+"";

					}else {
						Intent it88=new Intent();
						it88.setClass(WriteOrderRiLiActivity.this,InSureActivity.class);
						it88.putExtra("insure_name",insureNmae);
						it88.putExtra("insure_phone",insurePhone);
						it88.putExtra("insure_card",insureIDcard);
						it88.putExtra("insure_sex",insureSex);
						it88.putExtra("taoid",taoid);
						startActivityForResult(it88, 99);
					}
				}

				break;
			case R.id.baoxian_tips_iv://保险说明弹窗
				String urlss=FinalConstant.TAO_BAOXIAN;

				HashMap<String, Object> urlMap = new HashMap<>();
				urlMap.put("tao_id",taoid);

				baoxianPop=new BaoxianPopWindow(mContex,urlss,urlMap);
				baoxianPop.showAtLocation(findViewById(R.id.order_time_all_ly),Gravity.CENTER,0,0);
				break;
			case R.id.baoxian_updatemessage_tv://更改保险人信息
				Intent it88=new Intent();
				it88.setClass(WriteOrderRiLiActivity.this,InSureActivity.class);
				it88.putExtra("insure_name",insureNmae);
				it88.putExtra("insure_phone",insurePhone);
				it88.putExtra("insure_card",insureIDcard);
				it88.putExtra("insure_sex",insureSex);
				it88.putExtra("taoid",taoid);
				startActivityForResult(it88, 99);
				break;
		}
	}

	EditText codeEt;
	ImageView codeIv;

	/**
	 * 获取手机号 并验证
	 * 
	 * @author dwb
	 * 
	 */
	public class PopupWindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PopupWindows(final Context mContext, View parent) {

			final View view = View.inflate(mContext, R.layout.pop_yuyincode,
					null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			// showAtLocation(parent, Gravity.BOTTOM, 0, 0);
			update();

			Button cancelBt = view.findViewById(R.id.cancel_bt);
			Button tureBt = view.findViewById(R.id.zixun_bt);
			codeEt = view.findViewById(R.id.no_pass_login_code_et);
			codeIv = view.findViewById(R.id.yuyin_code_iv);

			RelativeLayout rshCodeRly = view
					.findViewById(R.id.no_pass_yanzheng_code_rly);

			rshCodeRly.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);
				}
			});

			tureBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String codes = codeEt.getText().toString();
					if (codes.length() > 1) {
						yanzhengCode(codes);
					} else {
						ViewInject.toast("请输入图中数字");
					}
				}
			});

			cancelBt.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

		}
	}

	void yanzhengCode(String codes) {
		String phones = phoneNumberEt.getText().toString().trim();

		Map<String,Object> maps=new HashMap<>();
		maps.put("code","9");
		maps.put("phone",phones);
		maps.put("code",codes);
		maps.put("flag","codelogin");
		new SecuritycodeApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {

				if (serverData.code.equals("1")) {
					yuyinCodePop.dismiss();
					ViewInject.toast("正在拨打您的电话，请注意接听");
				} else {
					ViewInject.toast("数字错误，请重新输入");
				}


			}

		});

	}


	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onActivityResult(int, int,
	 * android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 6:
			if (null != data) {
				String phonenum = data.getStringExtra("phone");
				phone = phonenum;
				noModify=false;

				String spj = phone;
				String a = spj.substring(0, 3);
				String b = spj.substring(spj.length() - 4, spj.length());
				String ss = a + "****" + b;
				phoneTv.setText(ss);
			}
			break;
			case 99://保险
				if(null!=data) {
					insureNmae = data.getStringExtra("name");
					insurePhone = data.getStringExtra("phone");
					insureIDcard = data.getStringExtra("card");
					insureSex = data.getStringExtra("sex");

					if (null != insureNmae && insureNmae.length() > 0) {

						baoxianMessageLy.setVisibility(View.VISIBLE);
						baoxianMessageTv.setText("被保险人：" + insureNmae + " (" + insurePhone.substring(0, 3) + "****" + insurePhone.substring(7, insurePhone.length()) + ")");
						isBaoxinMessage = "1";
						isbaoxian = "1";
						baoxianCheckIv.setBackgroundResource(R.drawable.xuanzhong_2x);

						baoxianJiaLy.setVisibility(View.VISIBLE);
						float allp=baoxianJia+Float.parseFloat(zhifuStr);
						shifuTv.setText("￥" + allp);
						finalPrice=allp+"";
					}
				}
				break;

		}
	}
}
