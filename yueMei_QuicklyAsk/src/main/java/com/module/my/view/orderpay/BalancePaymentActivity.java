package com.module.my.view.orderpay;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.umeng.socialize.utils.Log;

import butterknife.BindView;

/**
 * 尾款结算页
 */
public class BalancePaymentActivity extends YMBaseWebViewActivity {

    @BindView(R.id.balance_payment_top)
    CommonTopBar mTop;
    @BindView(R.id.balance_problems_content)
    FrameLayout contentWeb;

    private String TAG = "BalancePaymentActivity";
    private String mTitle;
    private String url;
    private BaseWebViewClientMessage baseWebViewClientMessage;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_balance_payment;
    }

    @Override
    protected void initView() {
        super.initView();

        url = getIntent().getStringExtra("url");
        mTitle = getIntent().getStringExtra("title");

        Log.e(TAG, "尾款结算页 === " + url);
        Log.e(TAG, "mWebView === " + mWebView);


        mWebView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        contentWeb.addView(mWebView);
        String[] split6491 = url.split("://");
        if ("http".equals(split6491[0])) {
            url = "https://" + split6491[1];
        }

        Log.e(TAG, "url === " + url);
        CookieManager cookieManager =  CookieManager.getInstance();
        cookieManager.setCookie(url, "yuemeiinfo=" + Utils.getYuemeiInfo());
        cookieManager.getCookie(url);
        mWebView.loadUrl(url);
    }

    @Override
    protected void initData() {
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        WebView.HitTestResult hitTestResult = view.getHitTestResult();
        int hitType = hitTestResult.getType();
        android.util.Log.e(TAG, "hitTestResult == " + hitTestResult);
        android.util.Log.e(TAG, "hitType == " + hitType);
        android.util.Log.e(TAG, "url == " + url);
            if (url.startsWith("type")) {
                baseWebViewClientMessage.showWebDetail(url);
            } else {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
            }
        return super.ymShouldOverrideUrlLoading(view, url);
    }

    @Override
    protected boolean onYmJsAlert(WebView view, String url, String message, JsResult result) {
        String[] strs = message.split("\n");
        showDialogExitEdit2("确定", message, result, strs.length);
        return true;
    }

    @Override
    protected void onYmReceivedTitle(WebView view, String title) {
        super.onYmReceivedTitle(view, title);
        if ("0".equals(mTitle)) {
            mTop.setCenterText(title);
        } else {
            mTop.setCenterText(mTitle);
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mWebView.reload();
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mContext, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
    }

}
