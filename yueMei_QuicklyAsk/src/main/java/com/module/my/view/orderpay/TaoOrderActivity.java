package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 淘整形 预约界面
 *
 * @author Rubin
 */
public class TaoOrderActivity extends BaseActivity {

    private final String TAG = "TaoDetailActivity";

    @BindView(id = R.id.tao_oder_de_web_det_linearlayout, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.tao_oder_top)
    private CommonTopBar mTop;// 返回

    public JSONObject obj_http;
    private WebView docDetWeb;
    private TaoOrderActivity mContex;

    private String id = "0";
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private HashMap<String, Object> urlMap = new HashMap<>();

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_tao_oder);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = TaoOrderActivity.this;

        mTop.setCenterText("预约");

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        initWebview();

        Intent it = getIntent();
        id = it.getStringExtra("id");

        LodUrl(id);

        // http://sjapp.yuemei.com/V410/tao/list/partId/项目id/cityId/地区id/
        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        TaoOrderActivity.this.finish();
                    }
                });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @SuppressLint({"SetJavaScriptEnabled", "InlinedApi"})
    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(docDetWeb);
    }

    /**
     * 加载web
     */
    public void LodUrl(String id) {
        baseWebViewClientMessage.startLoading();

        String tyUrl = FinalConstant.TAO_ORDER;
        urlMap.put("id", id);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(tyUrl, urlMap);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
