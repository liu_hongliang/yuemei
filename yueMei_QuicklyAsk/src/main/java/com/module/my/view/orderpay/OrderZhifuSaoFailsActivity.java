package com.module.my.view.orderpay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * 支付失败
 * 
 * @author dwb
 * 
 */
public class OrderZhifuSaoFailsActivity extends BaseActivity {

	private final String TAG = "OrderZhifuSaoFailsActivity";

	@BindView(id = R.id.order_phone_test_back, click = true)
	private RelativeLayout back;// 返回

	@BindView(id = R.id.sumbit_order_again_bt, click = true)
	private Button order_againBt;// 重新支付
	@BindView(id = R.id.more_zhifu_question_tv, click = true)
	private TextView zhifu_question;// 更多支付问题查询

	private Context mContex;

	private String server_id;
	private String order_id;
	private String price;
	private String taotitle;
	private String order_time;

	private String uid;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_order_sao_zhifu_fails);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = OrderZhifuSaoFailsActivity.this;

		uid = Utils.getUid();

		Intent it = getIntent();
		server_id = it.getStringExtra("server_id");
		order_id = it.getStringExtra("order_id");
		price = it.getStringExtra("price");
		taotitle = it.getStringExtra("taotitle");
		order_time = it.getStringExtra("order_time");

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						OrderZhifuSaoFailsActivity.this.finish();
					}
				});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {

		case R.id.order_phone_test_back:
			onBackPressed();
			break;
		case R.id.sumbit_order_again_bt:// 重新支付
			Intent it1 = new Intent();
			it1.setClass(mContex, OrdePaySaoActivity.class);
			it1.putExtra("price", price);
			it1.putExtra("tao_title", taotitle);
			it1.putExtra("server_id", server_id);
			it1.putExtra("order_id", order_id);
			it1.putExtra("order_time", order_time);
			startActivity(it1);
			finish();
			break;
		case R.id.more_zhifu_question_tv:// 支付更多为问题
			Intent it2 = new Intent();
			it2.setClass(mContex, ZhiFuHelp1Activity.class);
			startActivity(it2);
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

	public String getOutTradeNo() {
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
				Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);

		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		return key;
	}
}
