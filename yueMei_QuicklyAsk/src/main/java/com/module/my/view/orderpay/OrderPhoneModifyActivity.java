/**
 *
 */
package com.module.my.view.orderpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.my.model.api.CommitSecurityCodeInLoginApi;
import com.module.my.model.api.SendEMSApi;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.MakeSureOrderActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 修改手机号
 *
 * @author peich
 */
public class OrderPhoneModifyActivity extends YMBaseActivity {

    private final String TAG = "OrderPhoneModifyActivity";

    @BindView(R.id.order_phone_number_et)
    EditText phoneNumberEt;
    @BindView(R.id.order_phone_code_et)
    EditText phoneCode;
    @BindView(R.id.yanzheng_code_rly)
    RelativeLayout sendEMSRly;
    @BindView(R.id.yanzheng_code_tv)
    TextView emsTv;
    @BindView(R.id.sumbit_order_bt)
    Button sumnbit;
    @BindView(R.id.nocode_message_rly)
    RelativeLayout nocodeRly;
    @BindView(R.id.nocode_message_tv)
    TextView nocodeTv;
    @BindView(R.id.order_time_all_ly)
    LinearLayout allcontent;

    private String phone;
    private String codeStr;
    private PopupWindows yuyinCodePop;

    GradientDrawable drawable1;
    GradientDrawable drawable2;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_orderphone_modify_570;
    }

    @Override
    protected void initView() {
        setMultiOnClickListener(sumnbit, nocodeRly);
    }

    @Override
    protected void initData() {

        drawable1 = new GradientDrawable();
        drawable1.setShape(GradientDrawable.RECTANGLE); // 画框
        drawable1.setStroke(1, getResources().getColor(R.color.red_ff4965)); // 边框粗细及颜色
        drawable1.setColor(0xFFFFFFFF); // 边框内部颜色

        drawable2 = new GradientDrawable();
        drawable2.setShape(GradientDrawable.RECTANGLE); // 画框
        drawable2.setStroke(1, getResources().getColor(R.color.button_bian2)); // 边框粗细及颜色
        drawable2.setColor(0xFFFFFFFF); // 边框内部颜色

        sendEMSRly.setBackground(drawable1); // 设置背景（效果就是有边框及底色）

        sendEMSRly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String phones = phoneNumberEt.getText().toString().trim();
                if (!TextUtils.isEmpty(phones)) {
                    if (ifPhoneNumber()) {
                        sendEMSRly.setClickable(false);
                        sendEMS();

                        nocodeRly.setVisibility(View.VISIBLE);
                        nocodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？" + "</u>"));
                        phoneCode.requestFocus();
                        InputMethodManager inputManager = (InputMethodManager) phoneCode.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.showSoftInput(phoneCode, 0);

                        new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onTick(long millisUntilFinished) {
                                sendEMSRly.setBackground(drawable2);
                                emsTv.setTextColor(getResources().getColor(R.color.button_zi));
                                emsTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                            }

                            @Override
                            public void onFinish() {
                                sendEMSRly.setBackground(drawable1);
                                emsTv.setTextColor(getResources().getColor(R.color.button_bian_hong1));
                                sendEMSRly.setClickable(true);
                                emsTv.setText("重新获取验证码");
                            }
                        }.start();
                    } else {
                        mFunctionManager.showShort("请输入正确的手机号");
                    }
                } else {
                    mFunctionManager.showShort("请输入手机号");
                }
            }
        });
    }

    /**
     * 点击事件
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.sumbit_order_bt:// 确认下单
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                String phonestr = phoneNumberEt.getText().toString().trim();
                String codestr = phoneCode.getText().toString().trim();

                if (phonestr.length() > 0) {
                    if (codestr.length() > 0) {
                        initCode2();
                    } else {
                        mFunctionManager.showShort("请输入验证码");
                    }
                } else {
                    mFunctionManager.showShort("请输入手机号");
                }
                break;
            case R.id.nocode_message_rly:// 没收到 语音验证码
                yuyinCodePop = new PopupWindows();
                yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);
                Glide.with(mContext).load(FinalConstant.TUXINGCODE)
                        .skipMemoryCache(true) // 不使用内存缓存
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                        .into(codeIv);
                break;
        }
    }

    /**
     * 判断手机号码是否存在
     *
     * @return
     */
    private boolean ifPhoneNumber() {
        return Utils.isMobile(phoneNumberEt.getText().toString());
    }

    /**
     * 判断验证码是否存在
     */
    private void sendEMS() {
        phone = phoneNumberEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mFunctionManager.showShort(serverData.message);
            }
        });
    }

    /**
     * 登录状态下提交验证码
     */
    private void initCode2() {
        codeStr = phoneCode.getText().toString();
        phone = phoneNumberEt.getText().toString();

        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phone);
        maps.put("code", codeStr);
        new CommitSecurityCodeInLoginApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    Intent it = new Intent(mContext, OrderWriteMessageActivity639.class);
                    it.putExtra("phone", phone);
                    setResult(MakeSureOrderActivity.REQUEST_CODE1, it);
                    finish();
                } else {
                    mFunctionManager.showShort(serverData.message);
                }
            }

        });
    }

    /**
     * 未登录状态下提交验证码
     * @param codes
     */
    private void yanzhengCode(String codes) {
        String phones = phoneNumberEt.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phones);
        maps.put("code", codes);
        maps.put("flag", "codelogin");
        new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    yuyinCodePop.dismiss();
                    mFunctionManager.showShort("正在拨打您的电话，请注意接听");
                } else {
                    mFunctionManager.showShort("数字错误，请重新输入");
                }
            }
        });
    }

    /**
     * 获取手机号 并验证
     */
    EditText codeEt;
    ImageView codeIv;

    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows() {
            View view = View.inflate(mContext, R.layout.pop_yuyincode, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            Button cancelBt = view.findViewById(R.id.cancel_bt);
            Button tureBt = view.findViewById(R.id.zixun_bt);
            codeEt = view.findViewById(R.id.no_pass_login_code_et);
            codeIv = view.findViewById(R.id.yuyin_code_iv);

            RelativeLayout rshCodeRly = view.findViewById(R.id.no_pass_yanzheng_code_rly);

            rshCodeRly.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Glide.with(mContext).load(FinalConstant.TUXINGCODE)
                            .skipMemoryCache(true) // 不使用内存缓存
                            .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                            .into(codeIv);
                }
            });

            tureBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    String codes = codeEt.getText().toString();
                    if (codes.length() > 1) {
                        yanzhengCode(codes);
                    } else {
                        mFunctionManager.showShort("请输入图中数字");

                    }
                }
            });

            cancelBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }
}
