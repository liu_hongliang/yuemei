package com.module.my.view.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 进度不断变换的ViewGroup
 * Created by 裴成浩 on 2018/8/6.
 */
public class PostNoteUploadItem extends RelativeLayout {
    private final Context mContext;
    private PostNoteUploadImageView uploadImage;          //上传的图片
    private TextView uploadContent;         //上传文案
    private TextView uploadProgress;        //上传进度
    public static final String CONTEXT1 = "图片上传中";
    public static final String CONTEXT2 = "视频上传中";
    public static final String CONTEXT3 = "失败";
    public static final String CONTEXT4 = "点击重试";

    public PostNoteUploadItem(Context context) {
        this(context, null);
    }

    public PostNoteUploadItem(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostNoteUploadItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        intView();
    }

    /**
     * 初始化UI
     */
    private void intView() {
        View view = View.inflate(mContext, R.layout.post_note_upload_image, this);
        uploadImage = view.findViewById(R.id.post_note_upload_image);
        uploadContent = view.findViewById(R.id.post_note_upload_content);
        uploadProgress = view.findViewById(R.id.post_note_upload_progress);
    }

    /**
     * 开始上传
     *
     * @param progress 进度
     */
    @SuppressLint("SetTextI18n")
    public void startUpload(int progress) {
        uploadImage.startUpload(progress);
        uploadProgress.setText(progress + "%");
    }

    /**
     * 显示隐藏
     *
     * @param visibility1 图片上传文案显示隐藏
     * @param visibility2 进度显示隐藏
     */
    public void setContentShow(int visibility1, int visibility2) {
        uploadContent.setVisibility(visibility1);
        uploadProgress.setVisibility(visibility2);
    }

    /**
     * 设置文案
     *
     * @param content
     */
    public void setContent(String content) {
        uploadContent.setText(content);
        uploadContent.setTextColor(Utils.getLocalColor(mContext,R.color._33));
        uploadProgress.setTextColor(Utils.getLocalColor(mContext,R.color._33));
    }

    /**
     * 设置点击失败文案
     * @param content
     * @param progContent
     */
    public void setContent(String content,String progContent) {
        uploadContent.setText(content);
        uploadProgress.setText(progContent);
        uploadContent.setTextColor(Utils.getLocalColor(mContext,R.color.red_ff4965));
        uploadProgress.setTextColor(Utils.getLocalColor(mContext,R.color.red_ff4965));
    }

    public PostNoteUploadImageView getUploadImage() {
        return uploadImage;
    }
}
