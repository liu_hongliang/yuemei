package com.module.my.view.orderpay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.api.InitXianXiaPayApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

public class OrderSaoXiadanActivity extends BaseActivity {

	private final String TAG = "OrderSaoXiadanActivity";

	@BindView(id = R.id.order_phone_test_back, click = true)
	private RelativeLayout back;// 返回

	@BindView(id = R.id.zhifu_suess_orderid_tv)
	private TextView orderIdTv;// 订单号
	@BindView(id = R.id.order_sao_price)
	private TextView orderPriceTv;// 支付金额

	private Context mContex;

	private String order_id;
	private String price;

	@BindView(id = R.id.sumbit_order_bt, click = true)
	private Button finishBt;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_order_sao_xiadan_successd);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = OrderSaoXiadanActivity.this;

		Intent it = getIntent();
		order_id = it.getStringExtra("order_id");
		price = it.getStringExtra("price");

		orderIdTv.setText(order_id);
		orderPriceTv.setText("￥" + price);
		initXianXiaPay();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						OrderSaoXiadanActivity.this.finish();
					}
				});
	}

	void initXianXiaPay() {
		Map<String,Object> maps=new HashMap<>();
		maps.put("order_id",order_id);
		new InitXianXiaPayApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {

			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.order_phone_test_back:
			if (Utils.isFastDoubleClick()) {
				return;
			}
			onBackPressed();
			break;
		case R.id.sumbit_order_bt:
			finish();
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}