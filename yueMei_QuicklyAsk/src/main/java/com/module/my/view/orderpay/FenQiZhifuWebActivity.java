package com.module.my.view.orderpay;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.util.EncodingUtils;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;



/**
 * Created by dwb on 16/12/22.
 */
public class FenQiZhifuWebActivity  extends BaseActivity {

    private final String TAG = "FenQiZhifuWebActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.web_zhifu_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private FenQiZhifuWebActivity mContext;

    public JSONObject obj_http;

    private String url;
    private String title;
    private String uid;
    private String postDate;
    private String order_id;
    private String installments_num;
    private static final int REQUEST_FENQI_CODE = 999999;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;

    @BindView(id = R.id.zhifu_title_bar_rly)
    private LinearLayout biaoView;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_web_zhifu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = FenQiZhifuWebActivity.this;
        uid = Utils.getUid();

        windowsH = Cfg.loadInt(mContext, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        biaoView.measure(w, h);
        bTheight =biaoView.getMeasuredHeight();

        Rect rectangle= new Rect();
        Window window= getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight= rectangle.top;

        Intent it=getIntent();
        order_id=it.getStringExtra("order_id");
        installments_num=it.getStringExtra("installments_num");

        postDate = "source=3&pay_method=14&order_id="+order_id+"&installments_num="+installments_num;

        url = FinalConstant.JDPAYURL;

        mTop.setCenterText("分期支付");

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContext);
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                if(url.startsWith(FinalConstant.FENQIPAYSUCCESSURL)){//支付成功
                    Intent it=new Intent();
                    it.putExtra("type_code", "1");
                    it.setClass(mContext, OrderMethodActivity594.class);
                    setResult(REQUEST_FENQI_CODE, it);
                    finish();
                }else if(url.startsWith(FinalConstant.FENQIPAYFAILDURL)){//支付失败
                    Intent it=new Intent();
                    it.putExtra("type_code", "0");
                    it.setClass(mContext, OrderMethodActivity594.class);
                    setResult(REQUEST_FENQI_CODE, it);
                    finish();
                }else if(url.startsWith("http://m.yuemei.com/home/")){
                    Intent it=new Intent();
                    it.putExtra("type_code", "2");
                    it.setClass(mContext, OrderMethodActivity594.class);
                    setResult(REQUEST_FENQI_CODE, it);
                    finish();
                } else {
                    docDetWeb.loadUrl(url);
                }
            }
        });
        initWebview();

        LodUrl1(url);

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContext);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        docDetWeb.getSettings().setLoadWithOverviewMode(true);
        docDetWeb.getSettings().setSaveFormData(true);	//设置webview保存表单数据
        docDetWeb.getSettings().setSavePassword(true);	//设置webview保存密码
        docDetWeb.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);	//设置中等像素密度，medium=160dpi
        docDetWeb.getSettings().setSupportZoom(true);	//支持缩放
        docDetWeb.getSettings().setUserAgentString("YueMei_QuicklyAsk_Android");
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = (windowsH-bTheight-statusBarHeight*2);
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);

        contentWeb.addView(docDetWeb);
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
//        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

        docDetWeb.postUrl(addressAndHead.getUrl(), EncodingUtils.getBytes(postDate, "BASE64"));
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}