package com.module.my.view.view;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

public class AutoLoginPop extends PopupWindow {


    public AutoLoginPop(final Context context){
        final View view = View.inflate(context, R.layout.auto_login_tip, null);
        setAnimationStyle(R.style.AnimBottom);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        int windowH = Cfg.loadInt(context, FinalConstant.WINDOWS_W, 0);
        setWidth((windowH - Utils.dip2px(106)));
        setFocusable(true);
        setContentView(view);
        update();

        RelativeLayout close = view.findViewById(R.id.auto_login_close);
        Button btn = view.findViewById(R.id.auto_btn);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                context.startActivity(new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS));
            }
        });
    }
}
