package com.module.my.view.orderpay;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.controller.activity.OnlineKefuWebActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;

/**
 * 支付状态2 支付失败
 *
 * @author 裴成浩
 */
public class OrderZhiFuStatus2Activity extends YMBaseActivity {

    private String TAG = "OrderZhiFuStatus2Activity";

    @BindView(R.id.order_zhifu_failure_top)
    CommonTopBar mTop;              //标题栏
    @BindView(R.id.oder_tell_yuemei_tv)
    TextView zixunTv;// 咨询电话

    @BindView(R.id.sumbit_order_again_bt)
    Button order_againBt;// 重新支付
    @BindView(R.id.more_zhifu_question_tv)
    TextView zhifu_question;// 更多支付问题查询

    private String server_id;
    private String order_id;
    private String price;
    private String order_time;
    private String sku_type;

    private String is_repayment = "";
    private String is_repayment_mimo = "";
    private String groupId;
    private String isGroup;
    private String weikuan;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_order_zhifu_status2;
    }

    @Override
    protected void initView() {

        Bundle bundle = getIntent().getBundleExtra("data");
        server_id = bundle.getString("server_id");
        order_id = bundle.getString("order_id");
        price = bundle.getString("price");
        order_time = bundle.getString("order_time");
        sku_type = bundle.getString("sku_type");
        is_repayment = bundle.getString("is_repayment");
        is_repayment_mimo = bundle.getString("is_repayment_mimo");
        groupId = bundle.getString("group_id");
        isGroup = bundle.getString("is_group");
        weikuan = bundle.getString("weikuan");

        Log.e(TAG, "server_id == " + server_id);
        Log.e(TAG, "order_id == " + order_id);
        Log.e(TAG, "price == " + price);
        Log.e(TAG, "order_time == " + order_time);
        Log.e(TAG, "sku_type == " + sku_type);
        Log.e(TAG, "is_repayment == " + is_repayment);
        Log.e(TAG, "is_repayment_mimo == " + is_repayment_mimo);
        Log.e(TAG, "groupId == " + groupId);
        Log.e(TAG, "isGroup == " + isGroup);

        //右边文字点击
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext, OrderDetailActivity.class);
                it.putExtra("order_id", order_id);
                it.putExtra("flag", "1");
                startActivity(it);
            }
        });

        setMultiOnClickListener(zixunTv, order_againBt, zhifu_question);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.oder_tell_yuemei_tv:// 打电话给悦美
                if (Utils.isLoginAndBind(mContext)) {
                    Intent it1 = new Intent();
                    it1.setClass(mContext, OnlineKefuWebActivity.class);
                    it1.putExtra("link", "/service/zxzx/");
                    it1.putExtra("title", "在线客服");
                    startActivity(it1);
                }
                break;
            case R.id.sumbit_order_again_bt:// 重新支付

                Bundle bundle = new Bundle();
                bundle.putString("price", price);
                bundle.putString("server_id", server_id);
                bundle.putString("order_id", order_id);
                bundle.putString("order_time", order_time);
                bundle.putString("type", "2");
                bundle.putString("sku_type", sku_type);
                bundle.putString("is_repayment", is_repayment);
                bundle.putString("is_repayment_mimo", is_repayment_mimo);
                bundle.putString("weikuan", weikuan);
                bundle.putString("num", "1");
                bundle.putString("is_group", isGroup);
                bundle.putString("group_id", groupId);
                mFunctionManager.goToActivity(OrderMethodActivity594.class, bundle);

                finish();
                break;
            case R.id.more_zhifu_question_tv:// 支付更多为问题
                Intent it2 = new Intent();
                it2.setClass(mContext, ZhiFuHelp1Activity.class);
                startActivity(it2);
                break;
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
