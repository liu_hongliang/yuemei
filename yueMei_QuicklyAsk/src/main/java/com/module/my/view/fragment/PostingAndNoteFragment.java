package com.module.my.view.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.adapter.RandomChatVote2Adapter;
import com.module.commonview.adapter.RandomChatVoteAdapter;
import com.module.commonview.module.api.QiNiuTokenApi;
import com.module.commonview.module.bean.QiNiuBean;
import com.module.commonview.utils.DialogUtils;
import com.module.commonview.view.Expression;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.VoteMsgEvent;
import com.module.my.controller.activity.SetCoverActivity;
import com.module.my.controller.activity.WriteFontActivity;
import com.module.my.controller.adapter.PostAndNoteLabelLayout;
import com.module.my.controller.other.UploadStatusException;
import com.module.my.model.bean.ForumShareData;
import com.module.my.model.bean.ForumTextData;
import com.module.my.model.bean.PostingAndNoteVideo;
import com.module.my.model.bean.PostingVideoCover;
import com.module.my.view.view.PostAndNoteAfterDay;
import com.module.my.view.view.PostAndNoteContentEditText;
import com.module.my.view.view.PostAndNoteTitle;
import com.module.my.view.view.PostAndNoteVideoCover;
import com.module.my.view.view.PostAndNoteVideoCover.CoverState;
import com.module.other.activity.InitiateVoteActivity;
import com.module.other.activity.MakeNewNoteActivity2;
import com.module.other.module.bean.MakeTagListListData;
import com.module.other.module.bean.SearchTaoDate;
import com.module.other.netWork.netWork.QiNiuConfigration;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.FileCompressionUtils;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.YueMeiDialog2;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.module.my.view.view.PostAndNoteVideoCover.CoverState.ON_CROSS;

/**
 * 发帖统一的Fragmnet
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteFragment extends YMBaseFragment {

    @BindView(R.id.post_note_video_cover)
    PostAndNoteVideoCover mVideoCover;          //视频封面
    @BindView(R.id.post_note_title)
    public PostAndNoteTitle mTitle;             //帖子标题
    @BindView(R.id.post_note_after_time)
    PostAndNoteAfterDay mAfterTime;             //术后天数选择
    @BindView(R.id.post_post_content)
    public PostAndNoteContentEditText mContent; //帖子内容列表
    @BindView(R.id.post_note_chat_container)
    LinearLayout mChatContainer;                //同步私信可见容器
    @BindView(R.id.post_note_chat)
    Switch mChat;                               //同步私信可见开关
    @BindView(R.id.post_note_no_off_container)
    LinearLayout mNoOffContainer;               //是否医生可见容器
    @BindView(R.id.post_note_no_off)
    Switch mNoOff;                              //是否医生可见开关
    @BindView(R.id.post_note_label_title)
    TextView mLabelTitle;                       //标签标题
    @BindView(R.id.post_note_label)
    FlowLayout mLabel;                          //标签列表
    Unbinder unbinder;

    @BindView(R.id.tv_edit_vote)                //编辑投票
            TextView tvEditVote;
    @BindView(R.id.rv_vote)                     //编辑投票RecycleView
            RecyclerView rvVote;
    @BindView(R.id.ll_vote_list)                //编辑投票列表部分布局
            RelativeLayout llVoteList;
    @BindView(R.id.tv_vote)                     //投票标题
            TextView mTv_vote;
    @BindView(R.id.ll_initiate_vote)            //发起投票按钮
            LinearLayout llInitiateVote;
    @BindView(R.id.switch_if_multiple)          //switch
            Switch switchIfMultiple;
    @BindView(R.id.ll_vote)                     //发起投票部分布局
            LinearLayout llVote;
    @BindView(R.id.ll_push_vote)                //推送开关
            LinearLayout llPushVote;


    private String TAG = "PostingAndNoteFragment";
    private int mType;    //1：提问帖，2：日记帖，3：随聊帖 13：投票贴
    private ArrayList<SearchTaoDate.TaoListBean> rvList;
    private String mSku_id;//投票帖选择的sku
    private String mVoteTitle;//投票帖选项标题
    private String mIsMultiple;//投票帖类型 单选还是多选 1 2
    private String mIsPush = "1";//投票帖是否开始推送0否1是
    private String mFrom;

    private static final int POSTING_NOTE_FONT = 1;
    private static final int VIDEO_EDITOR_COVER = 4;
    public static final int TAG_SELECTION = 6;

    private final String LABEL_TITLE1 = "添加标签可以让更多人阅读到你发的内容";
    private final String LABEL_TITLE2 = "添加标签，方便医生帮你解答";
    private PostAndNoteLabelLayout mLabelLayout;
    private Gson mGson;
    private YueMeiDialog2 mYueMeiDialog;
    private PostingAndNoteImageVideoFragment postingAndNoteImageVideoFragment;
    private RandomChatVoteAdapter randomChatVoteAdapter;
    private RandomChatVote2Adapter randomChatVote2Adapter;
    private ArrayList<String> txtList;


    public static PostingAndNoteFragment newInstance(int type) {
        PostingAndNoteFragment fragment = new PostingAndNoteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static PostingAndNoteFragment newInstance(int type, ArrayList<SearchTaoDate.TaoListBean> list, String sku_id, String voteTitle, String isMultiple, String from) {
        PostingAndNoteFragment fragment = new PostingAndNoteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putParcelableArrayList("data", list);
        bundle.putString("sku_id", sku_id);
        bundle.putString("voteTitle", voteTitle);
        bundle.putString("isMultiple", isMultiple);
        bundle.putString("from", from);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static PostingAndNoteFragment newInstance(int type, ArrayList<String> list, String voteTitle, String isMultiple, String from) {
        PostingAndNoteFragment fragment = new PostingAndNoteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putStringArrayList("txtList", list);
        bundle.putString("voteTitle", voteTitle);
        bundle.putString("isMultiple", isMultiple);
        bundle.putString("from", from);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(VoteMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 4:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragmet_post_note;
    }

    @Override
    protected void initView(View view) {
        mType = getArguments().getInt("type");
        rvList = getArguments().getParcelableArrayList("data");
        txtList = getArguments().getStringArrayList("txtList");
        mSku_id = getArguments().getString("sku_id");
        mVoteTitle = getArguments().getString("voteTitle");
        mIsMultiple = getArguments().getString("isMultiple");
        mFrom = getArguments().getString("from");
        //不同的发帖形式设置不同的文案
        switch (mType) {
            case 1:         //提问帖
                mTitle.setMinLength(6);
                mTitle.setTitleHint(PostAndNoteTitle.EditTitle1);

                mAfterTime.setVisibility(View.GONE);

                mChatContainer.setVisibility(View.VISIBLE);

                mNoOffContainer.setVisibility(View.VISIBLE);

                mContent.setContentHint(PostAndNoteContentEditText.EditTitle1);

                mLabelTitle.setText(LABEL_TITLE2);

                break;
            case 2:         //日记帖（暂时不做）

                mTitle.setMinLength(4);
                mTitle.setTitleHint(PostAndNoteTitle.EditTitle2);

                mAfterTime.setVisibility(View.VISIBLE);
                mNoOffContainer.setVisibility(View.GONE);

                mContent.setContentHint(PostAndNoteContentEditText.EditTitle2);
                break;
            case 3:         //随聊帖

                mTitle.setMinLength(4);
                mTitle.setTitleHint(PostAndNoteTitle.EditTitle3);

                mAfterTime.setVisibility(View.GONE);

                mChatContainer.setVisibility(View.GONE);

                mNoOffContainer.setVisibility(View.GONE);

                mContent.setContentHint(PostAndNoteContentEditText.EditTitle3);

                mLabelTitle.setText(LABEL_TITLE1);

                upDataVoteUi();
                break;
            case 4:         //报名贴

                mTitle.setMinLength(4);
                mTitle.setTitleHint(PostAndNoteTitle.EditTitle3);

                mAfterTime.setVisibility(View.GONE);

                mChatContainer.setVisibility(View.GONE);

                mNoOffContainer.setVisibility(View.GONE);

                mContent.setContentHint(PostAndNoteContentEditText.EditTitle4);

                mLabelTitle.setText(LABEL_TITLE1);
                break;
            case 13:
                //投票
                mTitle.setMinLength(4);
                mTitle.setTitleHint(PostAndNoteTitle.EditTitle3);

                mAfterTime.setVisibility(View.GONE);

                mChatContainer.setVisibility(View.GONE);

                mNoOffContainer.setVisibility(View.GONE);

                mContent.setContentHint(PostAndNoteContentEditText.EditTitle3);

                mLabelTitle.setText(LABEL_TITLE1);
                break;
        }

        //视频封面点击
        mVideoCover.setTextClickListener(new PostAndNoteVideoCover.ClickCallBack() {
            @Override
            public void onClick(View v) {
                try {
                    PostingAndNoteVideo videoData = postingAndNoteImageVideoFragment.mImgVideo.getVideoData();

                    switch (mVideoCover.getCoverState()) {
                        case ON_CROSS:                  //上传中
                            mFunctionManager.showShort("封面上传中...");
                            break;
                        case UPLOADED_SUCCESS:          //上传成功
                            Intent intent = new Intent(mContext, SetCoverActivity.class);
                            intent.putExtra("selectNum", videoData.getVideoPath());                     //本地视频存储路径
                            intent.putExtra("imgCoverPath", videoData.getVideoCover().getImg());      //本地封面存储路径
                            startActivityForResult(intent, VIDEO_EDITOR_COVER);
                            break;
                        case UPLOAD_FAILED:             //上传失败
                            Intent intent2 = new Intent(mContext, SetCoverActivity.class);
                            intent2.putExtra("selectNum", videoData.getVideoPath());                     //本地视频存储路径
                            intent2.putExtra("imgCoverPath", videoData.getVideoCover().getImg());      //本地封面存储路径
                            startActivityForResult(intent2, VIDEO_EDITOR_COVER);
//                            Log.e(TAG, "videoData.getVideoCover() == " + videoData.getVideoCover().getImg());
//                            uploadCoverImage(QiNuConfig.getKey(), videoData.getVideoCover().getImg());      //上传失败后重新上传
                            break;
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }
        });

        //设置标题编辑框
        mTitle.setContenteEditText(mContent);
        mTitle.setClickCallBack(new PostAndNoteTitle.ClickCallBack() {
            @Override
            public void onTitleEditorClick(View v) {

            }

            @Override
            public void onReleaseClick(boolean isClick) {
                Log.e(TAG, "isClick11111 === " + isClick);
                if (onClickListener != null) {
                    onClickListener.onButtonStateListener(isClick);
                }
            }
        });

        //设置内容编辑框
        mContent.setContenteEditText(mTitle);
        mContent.setClickCallBack(new PostAndNoteContentEditText.ClickCallBack() {
            @Override
            public void onContentEditorClick(View v) {
                Intent it = new Intent(mContext, WriteFontActivity.class);
                it.putExtra("text_str", mContent.getContent());
                startActivityForResult(it, POSTING_NOTE_FONT);
            }

            @Override
            public void onReleaseClick(boolean isClick) {
                Log.e(TAG, "isClick22222 === " + isClick);
                if (onClickListener != null) {
                    onClickListener.onButtonStateListener(isClick);
                }
            }
        });

        switchIfMultiple.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mIsPush = "1";
                } else {
//                    **发起投票--**关闭‘委托小悦悦’
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_PUSH_CLOSE), new ActivityTypeData("170"));
                    mIsPush = "0";
                }
            }
        });

        //是否只有医生可见
        mNoOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    showDialogExitEdit();
                }
            }
        });

        //同步私信开关
        mChat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (mLabelLayout.getmDatas().size() == 0) {
                        mYueMeiDialog.show();
                    }
                }
            }
        });

        //标签设置

        mLabelLayout = new PostAndNoteLabelLayout(mContext, mLabel);

        //标签回调
        mLabelLayout.setClickCallBack(new PostAndNoteLabelLayout.ClickCallBack() {
            @Override
            public void onClick(View v, int pos) {

                Intent intent = new Intent(mContext, MakeNewNoteActivity2.class);
                intent.putParcelableArrayListExtra("datas", mLabelLayout.getmDatas());
                startActivityForResult(intent, TAG_SELECTION);
            }

            @Override
            public void onDeleteClick(View v, int pos) {
                mLabelLayout.deleteData(pos);
            }

        });

        llInitiateVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                发起投票--发起按钮点击**（随聊发帖页面）**
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.LAUNCH_POST_VOTE_CLICK), new ActivityTypeData("170"));
                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
                intent.putExtra("from", "PostingAndNoteFragment");
                startActivityForResult(intent, 10111);
            }
        });

        tvEditVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
//                intent.putExtra("voteTitle", mVoteTitle);
//                intent.putExtra("isMultiple", mIsMultiple);
//                intent.putParcelableArrayListExtra("votedata", rvList);
//                intent.putExtra("from", "PostingAndNoteFragment");
//                startActivityForResult(intent, 1002);
                //删除
                DialogUtils.showVoteDialog(mContext, "删除了就需要重新编辑哦", "取消", "确认", new DialogUtils.CallBack2() {
                    @Override
                    public void onYesClick() {
                        DialogUtils.closeDialog();
                        if (rvList != null && rvList.size() > 0) {
                            rvList = null;
                        }
                        if (txtList != null && txtList.size() > 0) {
                            txtList = null;
                        }
                        //更新投票相关布局
                        upDataVoteUi();
                        //更新数据
                        setReleaseData();
                    }

                    @Override
                    public void onNoClick() {
                        DialogUtils.closeDialog();
                    }
                });
            }
        });


        postingAndNoteImageVideoFragment = PostingAndNoteImageVideoFragment.newInstance();
        setActivityFragment(R.id.post_note_img_video_framelayout, postingAndNoteImageVideoFragment);

        postingAndNoteImageVideoFragment.setOnEventClickListener(new PostingAndNoteImageVideoFragment.OnEventClickListener() {
            @Override
            public void onVideoCoverClick(int visibility) {
                mVideoCover.setVisibility(visibility);
            }

            @Override
            public void onVideoCoverPathClick(String qiNiukey, String path) {
                mVideoCover.setCoverImgSrc(path);
                uploadCoverImage(qiNiukey, path);
            }
        });
    }

    //更新投票相关布局
    private void upDataVoteUi() {
        if (rvList != null && !rvList.isEmpty()) {
            //设置sku投票数据列表
            setAdapter();
            llPushVote.setVisibility(View.VISIBLE);
            llVoteList.setVisibility(View.VISIBLE);
            llVote.setVisibility(View.GONE);
        }

        if (txtList != null && !txtList.isEmpty()) {
            //设置普通投票数据列表
            setOrdinaryAdapter();
            llPushVote.setVisibility(View.VISIBLE);
            llVoteList.setVisibility(View.VISIBLE);
            llVote.setVisibility(View.GONE);
        }

        if (txtList == null && rvList == null) {
            llPushVote.setVisibility(View.GONE);
            llVoteList.setVisibility(View.GONE);
            llVote.setVisibility(View.VISIBLE);
        }
    }

    private void setOrdinaryAdapter() {
        mTv_vote.setText(mVoteTitle);
        //不获取焦点
        rvVote.setFocusable(false);
        rvVote.setNestedScrollingEnabled(false);
        randomChatVote2Adapter = new RandomChatVote2Adapter(mContext, txtList);
        YMLinearLayoutManager layoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rvVote.setLayoutManager(layoutManager);
        rvVote.setAdapter(randomChatVote2Adapter);
        randomChatVote2Adapter.setOnItemClickListener(new RandomChatVote2Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
                intent.putExtra("voteTitle", mVoteTitle);
                intent.putExtra("isMultiple", mIsMultiple);
                intent.putStringArrayListExtra("txtList", txtList);
                intent.putExtra("from", "PostingAndNoteFragment");
                startActivityForResult(intent, 1002);
            }
        });

        llVoteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
                intent.putExtra("voteTitle", mVoteTitle);
                intent.putExtra("isMultiple", mIsMultiple);
                intent.putStringArrayListExtra("txtList", txtList);
                intent.putExtra("from", "PostingAndNoteFragment");
                startActivityForResult(intent, 1002);
            }
        });

    }

    private void setAdapter() {
        mTv_vote.setText(mVoteTitle);
        //不获取焦点
        rvVote.setFocusable(false);
        rvVote.setNestedScrollingEnabled(false);
        randomChatVoteAdapter = new RandomChatVoteAdapter(mContext, rvList);
        YMLinearLayoutManager layoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rvVote.setLayoutManager(layoutManager);
        rvVote.setAdapter(randomChatVoteAdapter);
        randomChatVoteAdapter.setOnItemClickListener(new RandomChatVoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
//                //详情页
//                Intent intent = new Intent(mContext, TaoDetailActivity.class);
//                intent.putExtra("id", randomChatVoteAdapter.getDate().get(pos).get_id());
//                intent.putExtra("source", "0");
//                intent.putExtra("objid", "0");
//                mContext.startActivity(intent);
                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
                intent.putExtra("voteTitle", mVoteTitle);
                intent.putExtra("isMultiple", mIsMultiple);
                intent.putParcelableArrayListExtra("votedata", rvList);
                intent.putExtra("from", "PostingAndNoteFragment");
                startActivityForResult(intent, 1002);
            }
        });

        llVoteList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InitiateVoteActivity.class);
                intent.putExtra("voteTitle", mVoteTitle);
                intent.putExtra("isMultiple", mIsMultiple);
                intent.putParcelableArrayListExtra("votedata", rvList);
                intent.putExtra("from", "PostingAndNoteFragment");
                startActivityForResult(intent, 1002);
            }
        });

    }

    @Override
    protected void initData(View view) {
        mGson = new Gson();

        mYueMeiDialog = new YueMeiDialog2(mContext, "请选择问题标签，方便帮您匹配适合的问答对象", "我知道了");
        mYueMeiDialog.setCanceledOnTouchOutside(false);
        mYueMeiDialog.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
            @Override
            public void BtnClick() {
                mChat.setChecked(false);
                mYueMeiDialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 1003) {
            try {
                switch (requestCode) {
                    case 10111:
                        if (data != null) {
                            mIsMultiple = data.getStringExtra("isMultiple");
                            mVoteTitle = data.getStringExtra("voteTitle");
                            mSku_id = data.getStringExtra("sku_id");
                            rvList = data.getParcelableArrayListExtra("sku_data");
                            txtList = data.getStringArrayListExtra("txtList");
                            setReleaseData();

                            if(rvList != null){
                                if (rvList != null && !rvList.isEmpty()) {
                                    setAdapter();
                                    llPushVote.setVisibility(View.VISIBLE);
                                    llVoteList.setVisibility(View.VISIBLE);
                                    llVote.setVisibility(View.GONE);
                                } else {
                                    llPushVote.setVisibility(View.GONE);
                                    llVoteList.setVisibility(View.GONE);
                                    llVote.setVisibility(View.VISIBLE);
                                }
                            }else{
                                if (txtList != null && !txtList.isEmpty()) {
                                    setOrdinaryAdapter();
                                    llPushVote.setVisibility(View.VISIBLE);
                                    llVoteList.setVisibility(View.VISIBLE);
                                    llVote.setVisibility(View.GONE);
                                }else{
                                    llPushVote.setVisibility(View.GONE);
                                    llVoteList.setVisibility(View.GONE);
                                    llVote.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        break;
                    case 1002:
                        if (data != null) {
                            mIsMultiple = data.getStringExtra("isMultiple");
                            mVoteTitle = data.getStringExtra("voteTitle");
                            mSku_id = data.getStringExtra("sku_id");
                            rvList = data.getParcelableArrayListExtra("sku_data");
                            txtList = data.getStringArrayListExtra("txtList");
                            setReleaseData();

                            if(rvList != null){
                                if (rvList != null && !rvList.isEmpty()) {
                                    setAdapter();
                                    llPushVote.setVisibility(View.VISIBLE);
                                    llVoteList.setVisibility(View.VISIBLE);
                                    llVote.setVisibility(View.GONE);
                                } else {
                                    llPushVote.setVisibility(View.GONE);
                                    llVoteList.setVisibility(View.GONE);
                                    llVote.setVisibility(View.VISIBLE);
                                }
                            }else{
                                if (txtList != null && !txtList.isEmpty()) {
                                    setOrdinaryAdapter();
                                    llPushVote.setVisibility(View.VISIBLE);
                                    llVoteList.setVisibility(View.VISIBLE);
                                    llVote.setVisibility(View.GONE);
                                }else{
                                    llPushVote.setVisibility(View.GONE);
                                    llVoteList.setVisibility(View.GONE);
                                    llVote.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        break;
                    case POSTING_NOTE_FONT:        //内容编辑返回
                        if (data != null) {
                            setQequestContent(data);
                        }
                        break;

                    case VIDEO_EDITOR_COVER:        //选择视频封面回调
                        String videoCoverPath = data.getStringExtra("imgPath");
                        Log.e(TAG, "videoCoverPath === " + videoCoverPath);
                        if (!TextUtils.isEmpty(videoCoverPath)) {
                            String qiNiukey = QiNuConfig.getKey();
                            uploadCoverImage(qiNiukey, videoCoverPath);      //自己选择的封面上传

                            int[] imageSize = getImageSize(videoCoverPath);
                            PostingVideoCover videoCoverData = new PostingVideoCover();
                            Log.e(TAG, "qiNiukey2222 == " + qiNiukey);
                            Log.e(TAG, "imageSize == " + imageSize[0]);
                            Log.e(TAG, "imageSize == " + imageSize[1]);
                            videoCoverData.setImg(qiNiukey);
                            videoCoverData.setWidth(imageSize[0]);
                            videoCoverData.setHeight(imageSize[1]);

                            postingAndNoteImageVideoFragment.mImgVideo.getVideoData().setVideoCover(videoCoverData);
                            mVideoCover.setCoverImgSrc(videoCoverPath);
                        }

                        break;

                    case TAG_SELECTION:             //标签选择的回调
                        ArrayList<MakeTagListListData> datas;
                        if (data != null) {
                            datas = data.getParcelableArrayListExtra("datas");
                        } else {
                            datas = new ArrayList<>();
                        }
                        Log.e(TAG, "datas == " + datas);
                        Log.e(TAG, "datas个数 == " + datas.size());

                        mLabelLayout.addDatas(datas);

                        break;
                }
            } catch (UploadStatusException e) {
                Log.e(TAG, "e === " + e.getMsg());
            }
        }

    }

    /**
     * 设置编辑框内容
     *
     * @param data
     */
    private void setQequestContent(Intent data) {
        String ss = data.getStringExtra("text_str");
        if (ss.length() > 0) {
            try {
                SpannableStringBuilder stringBuilder = Expression.handlerEmojiText1(ss, mContext, Utils.dip2px(12));
                mContent.setText(stringBuilder);
                mContent.setSelection(mContent.getContentLength()); //将光标移至文字末尾
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 上传视频封面图
     */
    private void uploadCoverImage(final String qiNiukey, final String path) {

        if (Utils.isLoginAndBind(mContext)) {
            String qiniutoken = Cfg.loadStr(mContext, FinalConstant.QINIUTOKEN, "");
            mVideoCover.setCoverState(ON_CROSS);       //视频封面上传中

            UploadManager uploadManager = QiNiuConfigration.getInstance().init();
            File file = FileCompressionUtils.compressionFile(path);
            uploadManager.put(file, qiNiukey, qiniutoken, new UpCompletionHandler() {
                @Override
                public void complete(String key, ResponseInfo info, JSONObject response) {
                    if (mVideoCover != null) {
                        if (info.isOK()) {
                            mVideoCover.setCoverState(CoverState.UPLOADED_SUCCESS);
                        } else {
                            try {
                                if (response != null) {
                                    String error = response.getString("error");
                                    if ("expired token".equals(error)) {
                                        new QiNiuTokenApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                                            @Override
                                            public void onSuccess(ServerData serverData) {
                                                if ("1".equals(serverData.code)) {
                                                    try {
                                                        QiNiuBean qiNiuBean = JSONUtil.TransformSingleBean(serverData.data, QiNiuBean.class);
                                                        Cfg.saveStr(mContext, FinalConstant.QINIUTOKEN, qiNiuBean.getQiniu_token());
                                                        if (path != null) {
                                                            uploadCoverImage(qiNiukey, path);
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        });
                                    } else {
                                        Log.e(TAG, "Upload Fail");
                                        mVideoCover.setCoverState(CoverState.UPLOAD_FAILED);
                                    }
                                } else {
                                    mVideoCover.setCoverState(CoverState.UPLOAD_FAILED);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "e1111 === " + e.toString());
                            }

                        }
                    }
                    Log.e(TAG, "key==" + key + "info==" + info + "response==" + response);
                }
            }, null);
        }

    }

    /**
     * 图片或视频上传中提示
     *
     * @return true 上传完成  false正在上传
     */
    public boolean uploadingPrompt() {
        if (postingAndNoteImageVideoFragment != null) {
            return postingAndNoteImageVideoFragment.mImgVideo.uploadingPrompt();
        } else {
            return true;
        }
    }

    /**
     * dialog提示仅医生可见
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("限制图片/视频仅医生可见，可能会错过更多优质的回复，确定关闭？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mNoOff.setChecked(true);
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 获取图片宽高
     *
     * @param path
     * @return
     */
    private int[] getImageSize(String path) {
        int[] imageSize = new int[2];
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path); // 此时返回的bitmap为null

            if (null != bitmap){
                imageSize[0] = bitmap.getWidth();
                imageSize[1] = bitmap.getHeight();

                // bitmap回收防止内存溢出
                if (!bitmap.isRecycled()) {
                    bitmap.recycle();
                }
                System.gc();
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Log.e(TAG, "e === " + e.toString());
            imageSize[0] = 0;
            imageSize[1] = 0;
        }
        return imageSize;
    }

    /**
     * 发布帖子数据
     *
     * @return
     */
    public ForumShareData setReleaseData() {
        ForumShareData mForumShareData = new ForumShareData();

        mForumShareData.setTitle(mTitle.getTitle());            //标题
        mForumShareData.setCateid(setLabelString());            //标签id串
        mForumShareData.setContent(mGson.toJson(new ForumTextData(mContent.getContent())));      //内容

        //图片/视频仅医生可见  默认是(1)
        if (mNoOff.isChecked()) {
            mForumShareData.setVisibility("0");
        } else {
            mForumShareData.setVisibility("1");
        }

        //提问帖，日记帖，随聊帖标识
        switch (mType) {
            case 1:
                mForumShareData.setAskorshare("0");
                break;
            case 2:
                mForumShareData.setAskorshare("1");
                break;
            case 3:
            case 4:
                mForumShareData.setAskorshare("4");
                break;
        }

        //sku投票
        if (rvList != null && rvList.size() > 0) {
            mForumShareData.setAskorshare("13");
            mForumShareData.setVote_tao(mSku_id);
            mForumShareData.setVote_title(mVoteTitle);
            mForumShareData.setVote_type(mIsMultiple);
            mForumShareData.setIs_push(mIsPush);
        }

        //普通投票
        if (txtList != null && txtList.size() > 0) {
            mForumShareData.setAskorshare("13");
            mForumShareData.setVote_text(mGson.toJson(txtList));
            mForumShareData.setVote_title(mVoteTitle);
            mForumShareData.setVote_type(mIsMultiple);
            mForumShareData.setIs_push(mIsPush);
        }

        //是否同步私信
        if (mChatContainer.getVisibility() == View.VISIBLE) {
            if (mChat.isChecked()) {
                mForumShareData.setIs_sixin("1");
            } else {
                mForumShareData.setIs_sixin("0");
            }
        } else {
            mForumShareData.setIs_sixin("0");
        }

        mForumShareData.setVideo(postingAndNoteImageVideoFragment.mImgVideo.getVideoJson());                                                   //视频json
        mForumShareData.setImage(postingAndNoteImageVideoFragment.mImgVideo.getImageJson());                                                   //照片json
        mForumShareData.setSurgeryafterdays(mAfterTime.getAfterDay() + "");                                                                    //术后天数
        mForumShareData.setCover_photo(postingAndNoteImageVideoFragment.mImgVideo.getCoverJson());                                             //图片封面图json

        return mForumShareData;
    }

    /**
     * 设置标签id的串
     *
     * @return
     */
    public String setLabelString() {
        StringBuilder labelString = new StringBuilder();
        List<MakeTagListListData> datas = mLabelLayout.getmDatas();

        for (int i = 0; i < datas.size(); i++) {
            MakeTagListListData data = datas.get(i);
            if (i != 0) {    //不是第一个
                labelString.append(",").append(data.getId());
            } else {
                labelString.append(data.getId());
            }
        }
        return labelString.toString();
    }

    private OnClickListener onClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnClickListener {
        void onButtonStateListener(boolean isClick);                      //发布按钮状态回调
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        getFocus();
//    }
//
//    private void getFocus() {
//        getView().setFocusable(true);
//        getView().setFocusableInTouchMode(true);
//        getView().requestFocus();
//        getView().setOnKeyListener(new View.OnKeyListener() {
//
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
//                    // 监听到返回按钮点击事件
//                    PostingAndNoteFragment.this.getActivity().finish();
//                    return true;// 未处理
//                }
//                return false;
//            }
//        });
//    }

}
