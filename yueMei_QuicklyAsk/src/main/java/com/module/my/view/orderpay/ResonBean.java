package com.module.my.view.orderpay;

public class ResonBean {
    private String reson;
    private  boolean state;

    public ResonBean(String reson, boolean state) {
        this.reson = reson;
        this.state = state;
    }

    public String getReson() {
        return reson;
    }

    public void setReson(String reson) {
        this.reson = reson;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
