package com.module.my.view.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

/**
 * 视频封面选择
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostAndNoteVideoCover extends FrameLayout {

    private Context mContext;
    private ImageView imageView;
    private TextView textView;
    private final String TEXT_CONTENT1 = "上传封面中...";
    private final String TEXT_CONTENT2 = "编辑视频封面";
    private final String TEXT_CONTENT3 = "上传失败，点击重试";

    ClickCallBack mICallBack = null;
    private String TAG = "PostAndNoteVideoCover";

    public PostAndNoteVideoCover(@NonNull Context context) {
        this(context, null);
    }

    public PostAndNoteVideoCover(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PostAndNoteVideoCover(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    /**
     * 设置样式
     */
    private void initView() {
        //设置父容器
        setBackgroundColor(Utils.setCustomColor("#524c4e"));

        //设置图片尺寸
        imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        setCoverSize(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);

        //设置内容
        textView = new TextView(mContext);
        textView.setText(TEXT_CONTENT1);
        textView.setBackgroundResource(R.drawable.shape_cc333333);
        textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        textView.setTextSize(12);
        LayoutParams textParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.gravity = Gravity.CENTER;        //外部居中
        textView.setLayoutParams(textParams);

        addView(imageView);
        addView(textView);
    }

    /**
     * 设置图片尺寸
     *
     * @param wrapContent
     * @param matchParent
     */
    public void setCoverSize(int wrapContent, int matchParent) {
        Log.e(TAG, "wrapContent == " + wrapContent);
        Log.e(TAG, "matchParent == " + matchParent);
        LayoutParams imageParams = new LayoutParams(wrapContent, matchParent);
        imageParams.gravity = Gravity.CENTER;        //外部居中
        imageView.setLayoutParams(imageParams);
    }

    /**
     * 设置当前视频封面状态
     */
    public void setCoverState(final CoverState coverState) {
        textView.post(new Runnable() {
            @Override
            public void run() {
                switch (coverState) {
                    case ON_CROSS:             //视频封面上传中
                        textView.setText(TEXT_CONTENT1);
                        break;
                    case UPLOADED_SUCCESS:     //视频封面上传成功
                        textView.setText(TEXT_CONTENT2);
                        break;
                    case UPLOAD_FAILED:        //视频封面上传失败
                        textView.setText(TEXT_CONTENT3);
                        break;
                }
            }
        });
    }

    /**
     * 视频封面的状态
     *
     * @return
     */
    public CoverState getCoverState() {
        switch (textView.getText().toString()) {
            case TEXT_CONTENT1:

                return CoverState.ON_CROSS;

            case TEXT_CONTENT2:

                return CoverState.UPLOADED_SUCCESS;
            case TEXT_CONTENT3:

                return CoverState.UPLOAD_FAILED;
        }
        return CoverState.UPLOADED_SUCCESS;
    }


    /**
     * 设置视频封面图
     */
    public void setCoverImgSrc(String video) {
        Glide.with(mContext).load(video).into(imageView);
    }

    /**
     * 文字点击事件
     *
     * @param rtc
     */
    public void setTextClickListener(final ClickCallBack rtc) {
        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                if (rtc != null) {
                    rtc.onClick(v);
                }
            }
        });
    }


    /**
     * 视频封面状态
     */
    public enum CoverState {
        ON_CROSS, UPLOADED_SUCCESS, UPLOAD_FAILED
    }

    /**
     * 文字点击
     */
    public interface ClickCallBack {
        void onClick(View v);
    }
}
