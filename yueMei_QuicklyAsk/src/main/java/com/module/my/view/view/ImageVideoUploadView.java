package com.module.my.view.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.module.base.view.FunctionManager;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.my.controller.activity.SelectVideoActivity;
import com.module.my.controller.activity.VideoPlayerActivity;
import com.module.my.controller.adapter.PostingAndNoteAdapter;
import com.module.my.controller.other.UploadStatusException;
import com.module.my.model.bean.PostingAndNoteButton;
import com.module.my.model.bean.PostingAndNoteData;
import com.module.my.model.bean.PostingAndNoteImage;
import com.module.my.model.bean.PostingAndNoteUpload;
import com.module.my.model.bean.PostingAndNoteVideo;
import com.module.my.model.bean.PostingUploadImage;
import com.module.my.model.bean.PostingVideoCover;
import com.module.my.model.bean.UploadImageSuccessData;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.other.RequestAndResultCode;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.UploadVideoJson;
import com.quicklyask.entity.WriteVideoResult;
import com.quicklyask.util.MyUploadImage648;
import com.quicklyask.util.Utils;
import com.quicklyask.util.VideoUploadUpyun;
import com.xinlan.imageeditlibrary.editimage.EditImageActivity;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * 上传图片视频的列表view
 * Created by 裴成浩 on 2019/9/29
 */
public class ImageVideoUploadView extends RecyclerView {

    private PostingAndNoteAdapter postingAndNoteAdapter;
    private FunctionManager mFunctionManager;
    private PostingAndNoteHandler mHandler;
    private final Gson mGson;
    private Activity mActivity;

    private final String TAG = "ImageVideoUploadView";
    private final String mShowMsg1 = "视频上传中，请稍候再试...";
    private final String mShowMsg2 = "图片上传中，请稍候再试...";
    private int maxImgNum = 9;  //图片最的可选数量

    private ArrayList<PostingAndNoteData> mAdapterDatas = new ArrayList<>();

    public ImageVideoUploadView(Context context) {
        this(context, null);
    }

    public ImageVideoUploadView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageVideoUploadView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mActivity = (Activity) context;
        mHandler = new PostingAndNoteHandler();
        mFunctionManager = new FunctionManager(context);
        mGson = new Gson();
    }

    public void setMaxImgNum(int maxImgNum) {
        this.maxImgNum = maxImgNum;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initView(boolean haveVideo) {
        //设置上传图片视频数据
        mAdapterDatas.add(addPostingAndNoteButton(false));
        if (haveVideo){
            mAdapterDatas.add(addPostingAndNoteButton(true));
        }

        //设置上传图片视频适配器
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mActivity, 4);
        ((DefaultItemAnimator) Objects.requireNonNull(getItemAnimator())).setSupportsChangeAnimations(false);
        gridLayoutManager.setScrollEnable(true);
        postingAndNoteAdapter = new PostingAndNoteAdapter(mActivity, mAdapterDatas);
        setLayoutManager(gridLayoutManager);
        setAdapter(postingAndNoteAdapter);


        //适配器的回调
        postingAndNoteAdapter.setOnClickListener(new PostingAndNoteAdapter.OnClickListener() {
            @Override
            public void onItemButtonClick(boolean isVideo, int pos) {
                if (isFastDoubleClick()) {
                    if (isVideo) {                                                //视频点击
                        Intent intent = new Intent(mActivity, SelectVideoActivity.class);

                        if (onEventClickListener != null) {
                            onEventClickListener.startFragmentForResult(intent, RequestAndResultCode.VIDEO_REQUEST_CODE);
                        }
                        mActivity.overridePendingTransition(R.anim.activity_open, 0);
                    } else {                                                      //图片点击

                        String sdcardState = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(sdcardState)) {

                            Intent intent = new Intent(mActivity, ImagesSelectorActivity.class);
                            intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, maxImgNum);
                            intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
                            intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
                            intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, postingAndNoteAdapter.getImgPaths());

                            if (onEventClickListener != null) {
                                onEventClickListener.startFragmentForResult(intent, RequestAndResultCode.IMAGE_REQUEST_CODE);
                            }

                        } else {
                            mFunctionManager.showShort(" sdcard已拔出，不能选择照片");
                        }
                    }
                }
            }

            @Override
            public void onImageDelete(boolean isVideo, int pos) {                                   //删除图片回调
                try {
                    if (isFastDoubleClick()) {
                        //如果删除的是封面图
                        if (!isVideo && postingAndNoteAdapter.getImageData(pos).isCover()) {
                            postingAndNoteAdapter.deleteData(pos);
                            if (postingAndNoteAdapter.notButton() != 0) {
                                if (maxImgNum == 3){
                                    postingAndNoteAdapter.getImageData(0).setCover(false);
                                }else {
                                    postingAndNoteAdapter.getImageData(0).setCover(true);
                                }
                                postingAndNoteAdapter.notifyItemPayload(0, PostingAndNoteAdapter.UPLOAD_COVER);
                            }
                        } else {
                            postingAndNoteAdapter.deleteData(pos);
                        }

                        if (isVideo) {
                            postingAndNoteAdapter.addData(1, addPostingAndNoteButton(true));
                            if (onEventClickListener != null) {
                                onEventClickListener.onVideoCoverClick(View.GONE);
                            }
                        } else {
                            //如果图片小于9，且添加图片按钮不存在
                            if (postingAndNoteAdapter.notButton() < maxImgNum && !postingAndNoteAdapter.haveButton(false)) {
                                if (maxImgNum == 3){//面诊投诉最的可选三张
                                    postingAndNoteAdapter.addData(2, addPostingAndNoteButton(false));
                                }else {
                                    postingAndNoteAdapter.addData(postingAndNoteAdapter.buttonPos(true), addPostingAndNoteButton(false));
                                }

                            }

                        }
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }

            @Override
            public void onItemImageView(boolean isVideo, int pos) {
                try {
                    if (isFastDoubleClick()) {
                        PostingAndNoteUpload.FileUploadState fileUploadState = postingAndNoteAdapter.getUploadData(pos).getFileUploadState();

                        if (fileUploadState == PostingAndNoteUpload.FileUploadState.UPLOADED_SUCCESSFULLY) {            //上传成功
                            if (isVideo) {
                                Intent intent = new Intent(mActivity, VideoPlayerActivity.class);
                                intent.putExtra("selectNum", postingAndNoteAdapter.getVideoData().getVideoPath());
                                mActivity.startActivity(intent);
                            } else {
                                String imgPath = postingAndNoteAdapter.getImageData(pos).getImgPath();
                                Intent it = new Intent(mActivity, EditImageActivity.class);
                                it.putExtra(EditImageActivity.FILE_PATH, imgPath);
                                it.putExtra(EditImageActivity.EXTRA_COVER, "1");
                                File outputFile = FileUtils.getEmptyFile("yuemei" + System.currentTimeMillis() + ".jpg");
                                it.putExtra(EditImageActivity.EXTRA_OUTPUT, outputFile.getAbsolutePath());
                                it.putExtra("pos", pos + "");
                                if (postingAndNoteAdapter.coverData().getImgPath().equals(imgPath)) {
                                    it.putExtra("isCover", true);
                                } else {
                                    it.putExtra("isCover", false);
                                }

                                if (onEventClickListener != null) {
                                    onEventClickListener.startFragmentForResult(it, RequestAndResultCode.ACTION_REQUEST_EDITIMAGE);
                                }
                            }
                        } else if (fileUploadState == PostingAndNoteUpload.FileUploadState.UPLOAD_FAILED) {             //上传失败
                            if (isVideo) {
                                VideoUploadUpyun.getVideoUploadUpyun(mActivity, mHandler).uploadVideo(postingAndNoteAdapter.getVideoData().getVideoPath());
                            } else {
                                postingAndNoteAdapter.getUploadData(pos).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
                                MyUploadImage648.getMyUploadImage(mActivity, mHandler, postingAndNoteAdapter.getImageData(pos).getImgPath(), false).uploadImage(QiNuConfig.getKey());
                            }
                        } else {            //未上传，上传中
                            if (isVideo) {
                                mFunctionManager.showShort(mShowMsg1);
                            } else {
                                mFunctionManager.showShort(mShowMsg2);
                            }
                        }
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }
        });

        //视频上传回调
        mHandler.setOnClickListener(new PostingAndNoteHandler.OnClickListener() {
            @Override
            public void onProgressClick(boolean isVideo, int progress) {                            //进度回调
                try {
                    if (isVideo) {
                        //上传状态 ->上传中
                        postingAndNoteAdapter.getUploadData(0).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
                        //刷新状态
                        postingAndNoteAdapter.notifyItemPart(0, progress);
                    } else {

                        postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadingState()).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);

                        postingAndNoteAdapter.notifyItemPart(postingAndNoteAdapter.uploadingState(), progress);
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }

            @Override
            public void onSuccessClick(boolean isVideo, WriteVideoResult writeVideoResult, UploadImageSuccessData imgUrl) {        //上传成功
                try {
                    if (isVideo) {
                        //上传状态 ->上传成功
                        postingAndNoteAdapter.getUploadData(0).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADED_SUCCESSFULLY);
                        postingAndNoteAdapter.getVideoData().setWriteVideoResult(writeVideoResult);
                    } else {
                        //上传状态 ->上传成功
                        postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadingState()).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADED_SUCCESSFULLY);

                        //设置上传后的网络地址
                        PostingUploadImage uploadImage = new PostingUploadImage();
                        uploadImage.setWidth(imgUrl.getWidth());
                        uploadImage.setHeight(imgUrl.getHeight());
                        uploadImage.setImg(imgUrl.getImageUrl());
                        postingAndNoteAdapter.getImageData(postingAndNoteAdapter.uploadSuccessState()).setImgUrlData(uploadImage);

                        //判断如果不是最后一个
                        if (postingAndNoteAdapter.uploadSuccessState() < postingAndNoteAdapter.notButton() - 1 && imgUrl.isContinuous()) {
                            postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadSuccessState() + 1).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
                            MyUploadImage648.getMyUploadImage(mActivity, mHandler, postingAndNoteAdapter.getImageData(postingAndNoteAdapter.uploadSuccessState() + 1).getImgPath()).uploadImage(QiNuConfig.getKey());
                        }
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }

            @Override
            public void onFailureClick(boolean isVideo) {                                           //上传失败
                try {
                    if (isVideo) {
                        postingAndNoteAdapter.getUploadData(0).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOAD_FAILED);           //上传状态 ->上传失败
                        postingAndNoteAdapter.getUploadData(0).setProgress(0);
                        postingAndNoteAdapter.notifyItem(0);
                    } else {
                        postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadingState()).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOAD_FAILED);
                        postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadingState()).setProgress(0);
                        postingAndNoteAdapter.notifyItem(postingAndNoteAdapter.uploadingState());

                        //判断如果不是最后一个
                        if (postingAndNoteAdapter.uploadSuccessState() < postingAndNoteAdapter.notButton() - 1) {
                            postingAndNoteAdapter.getUploadData(postingAndNoteAdapter.uploadSuccessState() + 1).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
                            MyUploadImage648.getMyUploadImage(mActivity, mHandler, postingAndNoteAdapter.getImageData(postingAndNoteAdapter.uploadSuccessState() + 1).getImgPath()).uploadImage(QiNuConfig.getKey());
                        }
                    }
                } catch (UploadStatusException e) {
                    Log.e(TAG, "e === " + e.getMsg());
                }
            }
        });
    }

    /**
     * 设置视频数据
     *
     * @param data
     */
    public void setRequestVideo(Intent data) throws UploadStatusException {
        String videoPath = data.getStringExtra("selectNum");            //视频路径
        if (!TextUtils.isEmpty(videoPath)) {

            long videoDuration = Integer.parseInt(data.getStringExtra("duration"));         //视频时间

            //删除添加视频按钮
            if (onEventClickListener != null) {
                onEventClickListener.onVideoCoverClick(View.VISIBLE);
            }
            postingAndNoteAdapter.deleteData(postingAndNoteAdapter.buttonPos(true));

            //添加一条视频数据
            PostingAndNoteData adapterData = new PostingAndNoteData();
            adapterData.setButton(false);

            PostingAndNoteUpload postingAndNoteUpload = new PostingAndNoteUpload();
            postingAndNoteUpload.setVideo(true);
            postingAndNoteUpload.setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);

            PostingAndNoteVideo postingAndNoteVideo = new PostingAndNoteVideo();
            postingAndNoteVideo.setVideoPath(videoPath);
            postingAndNoteVideo.setVideoDuration(videoDuration);
            postingAndNoteVideo.setVideoThumbnail(Uri.fromFile(new File(videoPath)));
            postingAndNoteUpload.setPostingAndNoteVideo(postingAndNoteVideo);

            adapterData.setPostingAndNoteUpload(postingAndNoteUpload);

            postingAndNoteAdapter.addData(0, adapterData);

            //上传视频到又拍云
            VideoUploadUpyun.getVideoUploadUpyun(mActivity, mHandler).uploadVideo(videoPath);

            //上传视频封面到悦美服务器（获取默认第一帧的图片）
            saveVideoPicture(videoPath);
        }
    }

    /**
     * 设置图片数据
     *
     * @param data
     */
    public void setRequestImage(Intent data) throws UploadStatusException {
        ArrayList<String> mResults = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);

        //删除在选择图片时取消选中的
        ArrayList<String> imgPaths = new ArrayList<>(postingAndNoteAdapter.getImgPaths());
        imgPaths.removeAll(mResults);

        //删除取消选中的
        for (String img : imgPaths) {
            postingAndNoteAdapter.deleteData(postingAndNoteAdapter.getImgPathOfPos(img));
        }

        //如果封面图删除了。当前列表有图,且视频封面图不存在
        if (postingAndNoteAdapter.notButton() > 0 && !postingAndNoteAdapter.haveCoverData()) {
            if (maxImgNum == 3){
                postingAndNoteAdapter.getImageData(0).setCover(false);
            }else {
                postingAndNoteAdapter.getImageData(0).setCover(true);
            }
            postingAndNoteAdapter.notifyItemPayload(0, PostingAndNoteAdapter.UPLOAD_COVER);
        }

        //把相同的剔除
        mResults.removeAll(postingAndNoteAdapter.getImgPaths());

        ArrayList<PostingAndNoteData> adapterDatas = new ArrayList<>();
        for (int i = 0; i < mResults.size(); i++) {
            String result = mResults.get(i);
            //添加一条视频数据
            PostingAndNoteData adapterData = new PostingAndNoteData();
            adapterData.setButton(false);

            PostingAndNoteUpload postingAndNoteUpload = new PostingAndNoteUpload();
            postingAndNoteUpload.setVideo(false);
            PostingAndNoteImage postingAndNoteImage = new PostingAndNoteImage();
            postingAndNoteImage.setImgPath(result);

            if (postingAndNoteAdapter.notButton() == 0 && i == 0) {
                if (maxImgNum == 3){ //面诊投诉最的可选三张，不加封面
                    postingAndNoteImage.setCover(false);
                }else {
                    postingAndNoteImage.setCover(true);
                }
            } else {
                postingAndNoteImage.setCover(false);
            }

            if (i == 0) {
                postingAndNoteUpload.setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
            } else {
                postingAndNoteUpload.setFileUploadState(PostingAndNoteUpload.FileUploadState.NOT_UPLOAD);
            }

            postingAndNoteUpload.setPostingAndNoteImage(postingAndNoteImage);
            adapterData.setPostingAndNoteUpload(postingAndNoteUpload);

            adapterDatas.add(adapterData);
        }

        //添加数据
        postingAndNoteAdapter.addData(postingAndNoteAdapter.buttonPos(false), adapterDatas);

        //如果已经有9张图片了，删除添加图片按钮
        if (postingAndNoteAdapter.notButton() == maxImgNum) {
            postingAndNoteAdapter.deleteData(postingAndNoteAdapter.buttonPos(false));
        }

        //图片上传
        MyUploadImage648.getMyUploadImage(mActivity, mHandler, postingAndNoteAdapter.getImageData(postingAndNoteAdapter.uploadingState()).getImgPath()).uploadImage(QiNuConfig.getKey());
    }

    /**
     * 设置贴纸数据
     *
     * @param data
     */
    public void setStickersData(Intent data) throws UploadStatusException {
        String newFilePath = data.getStringExtra("save_file_path");
        int poss = Integer.parseInt(data.getStringExtra("pos"));
        String dele = data.getStringExtra("dele");
        boolean isCover = data.getBooleanExtra("isCover", false);               //当前这个

        if ("0".equals(dele)) {       //没有删除
            PostingAndNoteImage imageData = postingAndNoteAdapter.getImageData(poss);

            //如果设置了当前封面
            if (isCover) {

                int coverPos = postingAndNoteAdapter.coverPos();
                postingAndNoteAdapter.coverData().setCover(false);
                postingAndNoteAdapter.notifyItemPayload(coverPos, PostingAndNoteAdapter.UPLOAD_COVER);

                //设置是否是封面
                postingAndNoteAdapter.getImageData(poss).setCover(true);
                postingAndNoteAdapter.notifyItemPayload(poss, PostingAndNoteAdapter.UPLOAD_COVER);
            }

            //添加了贴纸的图片 需要重新上传
            if (!imageData.getImgPath().equals(newFilePath)) {

                postingAndNoteAdapter.getImageData(poss).setImgPath(newFilePath);
                postingAndNoteAdapter.getUploadData(poss).setProgress(0);
                postingAndNoteAdapter.getUploadData(poss).setFileUploadState(PostingAndNoteUpload.FileUploadState.UPLOADING);
                postingAndNoteAdapter.notifyItemPayload(poss, PostingAndNoteAdapter.UPLOAD_IMAGE);

                MyUploadImage648.getMyUploadImage(mActivity, mHandler, postingAndNoteAdapter.getImageData(poss).getImgPath(), false).uploadImage(QiNuConfig.getKey());

            }

        } else {                      //删除了该图片
            boolean deleteCoverPos = false;
            if (postingAndNoteAdapter.coverPos() == poss) {
                deleteCoverPos = true;
            }
            postingAndNoteAdapter.deleteData(poss);

            if (deleteCoverPos && postingAndNoteAdapter.notButton() != 0) {
                postingAndNoteAdapter.getImageData(0).setCover(true);
                postingAndNoteAdapter.notifyItemPayload(0, PostingAndNoteAdapter.UPLOAD_COVER);
            }
        }
    }


    /**
     * 视频保存缩略图
     *
     * @param path
     * @return
     */
    public void saveVideoPicture(String path) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        Bitmap bitmap = retriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);    //获取第一帧的数据

        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);   // 建立这个路径的文件或文件夹
        if (!path1.exists()) {      // 如果不存在，就建立文件夹
            path1.mkdirs();
        }

        String pathPic = pathS + "/yuemei_" + System.currentTimeMillis() + ".jpg";

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(pathPic);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            fos.close();

            Log.e(TAG, "pathPic === " + pathPic);

            String qiNiukey = QiNuConfig.getKey();
            PostingVideoCover videoCover = new PostingVideoCover();
            Log.e(TAG, "qiNiukey11111 == " + qiNiukey);
            videoCover.setImg(qiNiukey);
            videoCover.setWidth(bitmap.getWidth());
            videoCover.setHeight(bitmap.getHeight());

            postingAndNoteAdapter.getVideoData().setVideoCover(videoCover);

            //设置封面设置
            if (onEventClickListener != null) {
                onEventClickListener.onVideoCoverPathClick(qiNiukey, pathPic);
            }

            // 回收并且置为null
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
            }
            System.gc();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "e === " + e.toString());
        }
    }

    /**
     * 设置图片json
     */
    public String getImageJson() {

        List<PostingAndNoteData> datas = getDatas();
        ArrayList<PostingUploadImage> imageDatas = new ArrayList<>();

        for (PostingAndNoteData data : datas) {
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo()) {
                imageDatas.add(data.getPostingAndNoteUpload().getPostingAndNoteImage().getImgUrlData());
            }
        }

        return mGson.toJson(imageDatas);
    }


    /**
     * 设置视频json
     *
     * @return
     */
    public String getVideoJson() {
        try {
            PostingAndNoteVideo videoData = getVideoData();
            WriteVideoResult videoResult = videoData.getWriteVideoResult();             //上传后的数据
            PostingVideoCover videoCover = videoData.getVideoCover();

            if (videoResult != null) {
                if (videoCover != null) {
                    UploadVideoJson uploadVideoJson = new UploadVideoJson();
                    uploadVideoJson.setCover(videoCover);
                    uploadVideoJson.setFile_size(videoResult.getFile_size() + "");
                    uploadVideoJson.setMimetype(videoResult.getMimetype());
                    uploadVideoJson.setUrl(videoResult.getUrl());
                    uploadVideoJson.setVideo_time((videoData.getVideoDuration() / 1000) + "");

                    return mGson.toJson(uploadVideoJson);

                } else {
                    showDialogExitEdit("上传视频封面失败，请重试", "确定");
                    return "";
                }
            }

            return "";

        } catch (UploadStatusException e) {
            Log.e(TAG, "e == " + e.getMsg());
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 设置封面图json
     *
     * @return
     */
    public String getCoverJson() {
        try {
            if (postingAndNoteAdapter.isUploadVideo()) {        //上传的是视频
                PostingVideoCover videoCover = getVideoData().getVideoCover();
                return mGson.toJson(videoCover);
            } else {
                PostingAndNoteImage postingAndNoteImage = coverData();
                PostingUploadImage imgUrlData = postingAndNoteImage.getImgUrlData();

                return mGson.toJson(imgUrlData);

            }
        } catch (UploadStatusException e) {
            Log.e(TAG, "e === " + e.getMsg());
            return "";
        }
    }


    /**
     * 防爆力点击
     *
     * @return true 单击 false快速点击
     */
    private boolean isFastDoubleClick() {
        if (Utils.isFastDoubleClick()) {
            return false;
        }

        return uploadingPrompt();
    }

    /**
     * 图片或视频上传中提示
     *
     * @return true 上传完成  false正在上传
     */
    public boolean uploadingPrompt() {
        if (postingAndNoteAdapter.isUpload()) {
            return true;
        } else {
            if (postingAndNoteAdapter.isUploadVideo()) {
                mFunctionManager.showShort("视频正在上传，请稍候再试...");
            } else {
                mFunctionManager.showShort("图片正在上传，请稍候再试...");
            }
            return false;
        }
    }

    /**
     * 添加按钮
     *
     * @param b
     * @return
     */
    public PostingAndNoteData addPostingAndNoteButton(boolean b) {
        PostingAndNoteData mAdapterData = new PostingAndNoteData();
        mAdapterData.setButton(true);
        PostingAndNoteButton postingAndNoteButton = new PostingAndNoteButton();
        postingAndNoteButton.setVideoButton(b);
        mAdapterData.setPostingAndNoteButton(postingAndNoteButton);
        return mAdapterData;
    }


    /**
     * 获取视频的数据
     */
    public PostingAndNoteVideo getVideoData() throws UploadStatusException {
        return postingAndNoteAdapter.getVideoData();
    }

    /**
     * 获取封面图item数据
     *
     * @return
     */
    public PostingAndNoteImage coverData() throws UploadStatusException {
        return postingAndNoteAdapter.coverData();
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     */
    public void showDialogExitEdit(String title, String content) {
        postingAndNoteAdapter.showDialogExitEdit(title, content);
    }

    /**
     * 获取数据
     *
     * @return
     */
    public List<PostingAndNoteData> getDatas() {
        return postingAndNoteAdapter.getDatas();
    }

    public interface OnEventClickListener {
        void onVideoCoverClick(int visibility);

        void onVideoCoverPathClick(String qiNiukey, String path);

        void startFragmentForResult(Intent intent, int requestCode);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
