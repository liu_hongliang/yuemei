package com.module.my.controller.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.module.base.view.YMBaseActivity;
import com.module.my.controller.adapter.OrderPreferentialAdapter;
import com.module.my.view.orderpay.OrderWriteMessageActivity639;
import com.module.shopping.controller.activity.MakeSureOrderActivity;
import com.module.shopping.model.bean.YouHuiCoupons;
import com.quicklyask.activity.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 预定SKU优惠劵选择页面
 *
 * @author 裴成浩
 */
public class OrderPreferentialActivity639 extends YMBaseActivity {

    private String TAG = "OrderPreferential";

    @BindView(R.id.wan_beautiful_web_back)
    RelativeLayout mBack;       //返回
    @BindView(R.id.tao_web_share_rly111)
    RelativeLayout shareBt;// 填码得劵
    @BindView(R.id.ll_coupons_null)
    LinearLayout couponsNull;
    @BindView(R.id.ls_youhuiquan)
    ListView mListView;

    private OrderPreferentialActivity639 mContext;
    private YouHuiCoupons mYouHui;
    private ArrayList<YouHuiCoupons> mYouhuiData;
    private OrderPreferentialAdapter orderPreferentialAdapter;
    private YouHuiCoupons youhuiNull;
    private int mHosPos;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_use_daijinjuan639;
    }

    @Override
    protected void initView() {
        mContext = OrderPreferentialActivity639.this;
        mYouHui = getIntent().getParcelableExtra("youhui_coupon");
        mHosPos = getIntent().getIntExtra("hos_pos", -1);
        mYouhuiData = getIntent().getParcelableArrayListExtra("youhui_coupons");
        Log.e(TAG, "mYouHui === " + mYouHui);
        Log.e(TAG, "mHosPos === " + mHosPos);
        Log.e(TAG, "mYouhuiData === " + mYouhuiData);

        youhuiNull = new YouHuiCoupons();

        //判断是否有优惠卷，显示不同的view
        if (mYouhuiData != null && mYouhuiData.size() > 0) {
            mListView.setVisibility(View.VISIBLE);
            couponsNull.setVisibility(View.GONE);
        } else {
            mListView.setVisibility(View.GONE);
            couponsNull.setVisibility(View.VISIBLE);
        }

        orderPreferentialAdapter = new OrderPreferentialAdapter(mContext, mYouhuiData, mYouHui);
        mListView.setAdapter(orderPreferentialAdapter);
    }

    @Override
    protected void initData() {
        initCallback();
    }

    /**
     * 初始化回调
     */
    private void initCallback() {
        //返回
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();    //调用手机自带返回键，自动finish
            }
        });

        //填码得劵
        shareBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it1 = new Intent();
                it1.setClass(mContext, TianMaDeJuanActivity.class);
                startActivity(it1);
                overridePendingTransition(R.anim.activity_open, 0);
            }
        });

        //选择优惠券
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                YouHuiCoupons youHuiCoupons = mYouhuiData.get(position);
                if (!youHuiCoupons.getCard_id().equals(mYouHui.getCard_id())) {
                    mYouHui = youHuiCoupons;
                } else {
                    mYouHui = youhuiNull;
                }
                orderPreferentialAdapter.setYouHuiCoupons(mYouHui);
                orderPreferentialAdapter.notifyDataSetChanged();
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(mContext, OrderWriteMessageActivity639.class);
        intent.putExtra("youhui_coupon", mYouHui);
        if (mHosPos >= 0) {
            intent.putExtra("hos_pos", mHosPos);
        }
        setResult(MakeSureOrderActivity.REQUEST_CODE2, intent);
        super.onBackPressed();
    }

}
