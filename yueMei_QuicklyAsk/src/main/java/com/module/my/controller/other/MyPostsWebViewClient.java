package com.module.my.controller.other;

import android.content.Intent;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.module.my.controller.activity.MyPostsActivity;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/1/18.
 */

public class MyPostsWebViewClient implements BaseWebViewClientCallback {

    private Intent intent;
    private MyPostsActivity mActivity;
    private String uid;
    private String TAG = "MyPostsWebViewClient";

    public MyPostsWebViewClient(MyPostsActivity activity) {
        this.mActivity = activity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        String qid1 = obj.getString("id");
        String askorshare = obj.getString("askorshare");
        String link = obj.getString("link");
        switch (mType) {
            case "1":// 问题详情页


                intent.putExtra("url", link);
                intent.putExtra("qid", qid1);
                intent.putExtra("askorshare", askorshare);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);
                break;

            case "6":// 查看更多问题

                if (urlStr.contains("userid")) {
                    String userid = obj.getString("userid");
                    Intent it = new Intent();
                    it.setClass(mActivity, DiariesAndPostsActivity.class);
                    it.putExtra("url", link);
                    it.putExtra("louc", "0");
                    it.putExtra("qid", "0");
                    it.putExtra("userid", userid);
                    it.putExtra("typeroot", "0");
                    it.putExtra("qid", qid1);
                    it.putExtra("askorshare", askorshare);
                    mActivity.startActivity(it);
                } else {
                    Intent it = new Intent();
                    it.setClass(mActivity, BBsFinalWebActivity.class);
                    it.putExtra("url", link);
                    it.putExtra("louc", "0");
                    it.putExtra("qid", "0");
                    it.putExtra("userid", "0");
                    it.putExtra("typeroot", "0");
                    it.putExtra("qid", qid1);
                    it.putExtra("askorshare", askorshare);
                    mActivity.startActivity(it);
                }

                break;

        }
    }
}
