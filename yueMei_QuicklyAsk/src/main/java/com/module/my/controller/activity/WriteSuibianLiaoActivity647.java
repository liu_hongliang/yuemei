package com.module.my.controller.activity;

import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.event.VoteMsgEvent;
import com.module.my.model.api.PostTextQueApi;
import com.module.my.model.bean.ForumShareData;
import com.module.my.view.fragment.PostingAndNoteFragment;
import com.module.other.module.bean.SearchTaoDate;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;

/**
 * 重构后的随聊贴
 *
 * @author 裴成浩
 */
public class WriteSuibianLiaoActivity647 extends YMBaseActivity {

    @BindView(R.id.post_suibian_top)
    CommonTopBar mTop;                          //标题

    private String TAG = "WriteSuibianLiaoActivity647";
    private PostingAndNoteFragment postingAndNoteFragment;
    private PostTextQueApi postTextQueApi;
    private String mCateid;
    private String isMultiple;
    private String voteTitle;
    private String sku_id;
    private String mFrom;
    private String itemId;
    private String itemName;
    private String urlParames;
    private ArrayList<SearchTaoDate.TaoListBean> selectList2;
    private ArrayList<String> txtList;
    private Map<String, Object> mMapForJson;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_write_suibian_liao647;
    }

    @Override
    protected void initView() {

        mCateid = getIntent().getStringExtra("cateid");
        isMultiple = getIntent().getStringExtra("isMultiple");
        voteTitle = getIntent().getStringExtra("voteTitle");
        sku_id = getIntent().getStringExtra("sku_id");
        selectList2 = getIntent().getParcelableArrayListExtra("sku_data");
        mFrom = getIntent().getStringExtra("from");
        itemId = getIntent().getStringExtra("itemId");
        itemName = getIntent().getStringExtra("itemName");
        urlParames = getIntent().getStringExtra("urlParames");
        //普通投票数据
        txtList = getIntent().getStringArrayListExtra("txtList");
        if (!TextUtils.isEmpty(getIntent().getStringExtra("source")) && (getIntent().getStringExtra("source")).equals("SelectSendPostsActivity")) {
//            mTop.setCenterText("随便聊聊");
            mTop.setCenterText("发帖");
        } else {
//            mTop.setCenterText("发起投票");
            mTop.setCenterText("发帖");
        }

        if (!TextUtils.isEmpty(urlParames)) {
            try {
                String decode = URLDecoder.decode(urlParames, "UTF-8");
                mMapForJson = JSONUtil.getMapForJson(decode);
                Log.e(TAG, "mMapForJson ==" + mMapForJson.toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        //设置发布默认是不可点击的
        mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));

//        postingAndNoteFragment = PostingAndNoteFragment.newInstance(3);
        if (txtList != null && txtList.size() > 0) {
            postingAndNoteFragment = PostingAndNoteFragment.newInstance(3, txtList, voteTitle, isMultiple, mFrom);
        } else {
            postingAndNoteFragment = PostingAndNoteFragment.newInstance(3, selectList2, sku_id, voteTitle, isMultiple, mFrom);
        }

        setActivityFragment(R.id.write_suibian_post_view, postingAndNoteFragment);

        postingAndNoteFragment.setOnClickListener(new PostingAndNoteFragment.OnClickListener() {
            @Override
            public void onButtonStateListener(boolean isClick) {
                if (isClick) {
                    mTop.setRightTextColor((Utils.getLocalColor(mContext, R.color.title_red_new)));
                } else {
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                }
            }
        });

        //关闭按钮点击
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                showDialogExitEdit();
            }
        });

        //发布按钮点击
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postingAndNoteFragment.uploadingPrompt()) {
                    ForumShareData forumShareData = postingAndNoteFragment.setReleaseData();
                    if (!TextUtils.isEmpty(forumShareData.getCover_photo())) {

                        if (postingAndNoteFragment.mTitle.getTitleLength() >= postingAndNoteFragment.mTitle.getMinLength()) {
                            if (postingAndNoteFragment.mContent.getContentLength() >= postingAndNoteFragment.mContent.getMinLength()) {
                                uploadData(forumShareData);
                            } else {
                                mFunctionManager.showShort("内容最少" + postingAndNoteFragment.mContent.getMinLength() + "字");
                            }
                        } else {
                            mFunctionManager.showShort("标题字数需要" + postingAndNoteFragment.mTitle.getMinLength() + "~" + postingAndNoteFragment.mTitle.getMaxLength() + "字");
                        }


                    } else {
                        mFunctionManager.showShort("请至少添加一张图片或一个视频");
                    }
                }
            }
        });
    }

    @Override
    protected void initData() {
        postTextQueApi = new PostTextQueApi();
    }

    /**
     * 上传数据
     */
    private void uploadData(ForumShareData forumShareData) {
        mDialog.startLoading();
        postTextQueApi.getHashMap().clear();
        Log.e(TAG, "forumShareData.getTitle() === " + forumShareData.getTitle());
        Log.e(TAG, "forumShareData.getCateid() === " + forumShareData.getCateid());
        Log.e(TAG, "forumShareData.getContent() === " + forumShareData.getContent());
        Log.e(TAG, "forumShareData.getVisibility() === " + forumShareData.getVisibility());
        Log.e(TAG, "forumShareData.getAskorshare() === " + forumShareData.getAskorshare());
        Log.e(TAG, "forumShareData.getImage() === " + forumShareData.getImage());
        Log.e(TAG, "forumShareData.getVideo() === " + forumShareData.getVideo());
        Log.e(TAG, "forumShareData.getCover_photo() === " + forumShareData.getCover_photo());

        postTextQueApi.addData("title", forumShareData.getTitle());                                 //日记本标题
        postTextQueApi.addData("content", forumShareData.getContent());                             //内容
        postTextQueApi.addData("visibility", forumShareData.getVisibility());                       //图片/视频仅医生可见  默认是   （1）
        postTextQueApi.addData("askorshare", forumShareData.getAskorshare());                       //标识

        if (!TextUtils.isEmpty(forumShareData.getVote_tao())) {
            postTextQueApi.addData("vote_tao", forumShareData.getVote_tao().replace(" ", ""));
            postTextQueApi.addData("vote_title", forumShareData.getVote_title());
            postTextQueApi.addData("vote_type", forumShareData.getVote_type());
            postTextQueApi.addData("is_push", forumShareData.getIs_push());
        }

        if (!TextUtils.isEmpty(forumShareData.getVote_text())) {
            postTextQueApi.addData("vote_text", forumShareData.getVote_text());
            postTextQueApi.addData("vote_type", forumShareData.getVote_type());
            postTextQueApi.addData("vote_title", forumShareData.getVote_title());
            postTextQueApi.addData("is_push", forumShareData.getIs_push());
        }


        //判断标签串是否是空的
        if (!TextUtils.isEmpty(forumShareData.getCateid())) {
            String cateid = forumShareData.getCateid();
            if (!TextUtils.isEmpty(mCateid)) {
                cateid = cateid + "," + mCateid;
            }
            postTextQueApi.addData("cateid", cateid);                                               //标签串
        } else {
            if (!TextUtils.isEmpty(mCateid)) {
                postTextQueApi.addData("cateid", mCateid);                                      //标签串
            }
        }
        if (mMapForJson != null) {
            for (Map.Entry<String, Object> entry : mMapForJson.entrySet()) {
                postTextQueApi.addData(entry.getKey(), (String) entry.getValue());
            }
        }
        //判断是上传图片还是上传视频
        if (!TextUtils.isEmpty(forumShareData.getVideo())) {
            postTextQueApi.addData("video", forumShareData.getVideo());                                 //视频json
            postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
        } else if (!TextUtils.isEmpty(forumShareData.getImage())) {
            postTextQueApi.addData("image", forumShareData.getImage());                                 //图片json
            postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
        }

        postTextQueApi.getCallBack(mContext, postTextQueApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                try {
                    if ("1".equals(serverData.code)) {
                        mFunctionManager.showShort(serverData.message);
                        WriteResultData data = JSONUtil.TransformSingleBean(serverData.data, WriteResultData.class);
                        String id = data.get_id();              //帖子Id
                        String appmurl = data.getAppmurl();     //	跳转地址
                        String onelogin = data.getOnelogin();   //	是否是首次登录 1是
                        Log.e(TAG, "id === " + id);
                        Log.e(TAG, "appmurl === " + appmurl);
                        Log.e(TAG, "onelogin === " + onelogin);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl);
//                        Intent it = new Intent();
//                        it.setClass(mContext, DiariesAndPostsActivity.class);
//                        it.putExtra("url", appmurl);
//                        it.putExtra("qid", id);
//                        it.putExtra("is_new", onelogin);
//                        startActivity(it);
                        onBackPressed();
                    } else {
                        mFunctionManager.showShort(serverData.message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * dialog提示，是否退出此次编辑
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("确定退出此次编辑吗？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDialog.dismiss();
                EventBus.getDefault().post(new VoteMsgEvent(4));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            showDialogExitEdit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
