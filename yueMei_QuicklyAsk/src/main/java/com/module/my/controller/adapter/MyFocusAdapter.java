package com.module.my.controller.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import java.util.HashMap;
import java.util.List;

/**
 * 关注粉丝适配器
 * Created by 裴成浩 on 2018/4/12.
 */

public class MyFocusAdapter extends BaseAdapter {

    private Activity mContext;
    private LayoutInflater mInflater;
    private List<MyFansData> mFansDatas;
    private String mType;           //0：推荐内容，1：我的关注，2：我的粉丝
    private ViewHolder viewHolder = null;
    private String TAG = "MyFocusAdapter";

    public MyFocusAdapter(Activity context, List<MyFansData> myFansDatas,String type) {
        this.mContext = context;
        this.mFansDatas = myFansDatas;
        this.mType = type;
        Log.e(TAG, "mFansDatas === " + mFansDatas.size());
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mFansDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mFansDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.e(TAG, "position === " + position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_my_focus, null);
            viewHolder.ficusImg = convertView.findViewById(R.id.item_ficus_img);
            viewHolder.ficusV = convertView.findViewById(R.id.item_ficus_v);
            viewHolder.ficusName = convertView.findViewById(R.id.item_ficus_name);
            viewHolder.ficusDesc = convertView.findViewById(R.id.item_ficus_desc);
            viewHolder.eachFollowing = convertView.findViewById(R.id.ficus_each_following);
            viewHolder.imgageFans = convertView.findViewById(R.id.following_imgage_fans);
            viewHolder.centerFans = convertView.findViewById(R.id.following_center_fans);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MyFansData fansData = mFansDatas.get(position);
        Log.e(TAG, "fansData.getImg() === " + fansData.getImg());
        Glide.with(mContext).load(fansData.getImg())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ten_ball_placeholder)
                .error(R.drawable.ten_ball_placeholder)
                .into(viewHolder.ficusImg);

        viewHolder.ficusName.setText(fansData.getName());
        viewHolder.ficusDesc.setText(fansData.getDesc());

        switch (fansData.getV()) {
            case "0":          //无
                viewHolder.ficusV.setVisibility(View.GONE);
                break;
            case "10":          //红色--官方
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext,R.drawable.official_bottom));
                break;
            case "12":          //蓝色--认证
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext,R.drawable.renzheng_bottom));
                break;
            case "13":          //蓝色--认证
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext,R.drawable.renzheng_bottom));
                break;
            case "11":          //达人
                viewHolder.ficusV.setVisibility(View.VISIBLE);
                viewHolder.ficusV.setBackground(ContextCompat.getDrawable(mContext,R.drawable.talent_bottom));
                break;
        }

        //关注样式
        final String eachFollowing = fansData.getEach_following();
        initFocus1(eachFollowing);

        //关注点击事件

        viewHolder.eachFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initFocus2(eachFollowing,position);
            }
        });

        return convertView;
    }

    /**
     * 关注和取消关注
     * @param position
     */
    private void FocusAndCancel(final int position) {
        HashMap<String, Object> hashMap = new HashMap<>();
        Log.e(TAG, "objid === " + mFansDatas.get(position).getObj_id());
        Log.e(TAG, "type === " + mFansDatas.get(position).getObj_type());
        hashMap.put("objid", mFansDatas.get(position).getObj_id());
        hashMap.put("type", mFansDatas.get(position).getObj_type());
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {

                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        Log.e(TAG, "focusAndCancelData.getIs_following() == " +  focusAndCancelData.getIs_following());
                        initFocus3(focusAndCancelData.getIs_following(), position);
                        initFocus4(focusAndCancelData.getIs_following(), position);
                        notifyDataSetChanged();
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void initFocus1(String eachFollowing) {
        if("1".equals(mType)){
            switch (eachFollowing) {
                case "0":          //已关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.focus_plus_sign_yes, R.color._99, "已关注");
                    break;
                case "1":          //互相关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.each_focus, R.color._99, "互相关注");
                    break;
                case "2":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
            }
        }else if("0".equals(mType)){
            switch (eachFollowing) {
                case "0":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
                case "1":          //已关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.focus_plus_sign_yes, R.color._99, "已关注");
                    break;
                case "2":          //互相关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.each_focus, R.color._99, "互相关注");
                    break;
            }
        }else{
            switch (eachFollowing) {
                case "0":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
                case "1":          //互相关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.each_focus, R.color._99, "互相关注");
                    break;
                case "2":          //已关注
                    setFocusView(R.drawable.shape_tuiyuan3_cdcdcd, R.drawable.focus_plus_sign_yes, R.color._99, "已关注");
                    break;
            }
        }
    }

    private void initFocus2(String eachFollowing,int position) {
        if("1".equals(mType)){
            switch (eachFollowing) {
                case "0":          //已关注
                case "1":          //互相关注
                    showDialogExitEdit(position);
                    break;
                case "2":          //未关注
                    FocusAndCancel(position);
                    break;
            }
        }else if("0".equals(mType)){
            switch (eachFollowing) {
                case "0":          //未关注
                    FocusAndCancel(position);
                    break;
                case "1":          //已关注
                case "2":          //互相关注
                    showDialogExitEdit(position);
                    break;
            }
        }else{
            switch (eachFollowing) {
                case "0":          //未关注
                    FocusAndCancel(position);
                    break;
                case "1":          //互相关注
                case "2":          //已关注
                    showDialogExitEdit(position);
                    break;
            }
        }
    }

    private void initFocus3(String isFollowing, int position) {
        if("1".equals(mType)) {

            switch (isFollowing) {
                case "0":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
                case "1":          //已关注
                    setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "已关注");
                    break;
                case "2":          //互相关注
                    setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "互相关注");
                    break;
            }

        }else if("0".equals(mType)){

            switch (isFollowing) {
                case "0":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
                case "1":          //已关注
                    setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "已关注");
                    break;
                case "2":          //互相关注
                    setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "互相关注");
                    break;
            }
        }else{
            switch (isFollowing) {
                case "0":          //未关注
                    setFocusView(R.drawable.shape_bian_tuoyuan3_ff5c77, R.drawable.focus_plus_sign1, R.color.red_ff4965, "关注");
                    break;
                case "1":         //互相关注
                    setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "已关注");
                    break;
                case "2":           //已关注
                    setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, R.color._99, "互相关注");
                    break;
            }
        }
    }

    private void initFocus4(String isFollowing, int position) {
        if("1".equals(mType)) {
            switch (isFollowing) {
                case "0":          //未关注
                    mFansDatas.get(position).setEach_following("2");
                    break;
                case "1":          //已关注
                    mFansDatas.get(position).setEach_following("0");
                    break;
                case "2":          //互相关注
                    mFansDatas.get(position).setEach_following("1");
                    break;
            }

        }else if("0".equals(mType)){

            switch (isFollowing) {
                case "0":          //未关注
                    mFansDatas.get(position).setEach_following("0");
                    break;
                case "1":          //已关注
                    mFansDatas.get(position).setEach_following("1");
                    break;
                case "2":          //互相关注
                    mFansDatas.get(position).setEach_following("2");
                    break;
            }
        }else{
            switch (isFollowing) {
                case "0":          //未关注
                    mFansDatas.get(position).setEach_following("0");
                    break;
                case "1":         //互相关注
                    mFansDatas.get(position).setEach_following("2");
                    break;
                case "2":           //已关注
                    mFansDatas.get(position).setEach_following("1");
                    break;
            }
        }
    }

    /**
     * 关注样式设置
     *
     * @param drawable1
     * @param drawable2
     * @param drawable3
     * @param text
     */
    private void setFocusView(int drawable1, int drawable2, int drawable3, String text) {
        viewHolder.eachFollowing.setBackground(ContextCompat.getDrawable(mContext,drawable1));
        viewHolder.imgageFans.setBackground(ContextCompat.getDrawable(mContext,drawable2));
        viewHolder.centerFans.setTextColor(ContextCompat.getColor(mContext,drawable3));
        viewHolder.centerFans.setText(text);

        ViewGroup.LayoutParams layoutParams1 =  viewHolder.eachFollowing.getLayoutParams();
        switch (text.length()) {
            case 2:
                layoutParams1.width = Utils.dip2px(mContext, 68);
                break;
            case 3:
                layoutParams1.width = Utils.dip2px(mContext, 78);
                break;
            case 4:
                layoutParams1.width = Utils.dip2px(mContext, 90);
                break;
        }

        viewHolder.eachFollowing.setLayoutParams(layoutParams1);
    }

    static class ViewHolder {
        ImageView ficusImg;
        ImageView ficusV;
        TextView ficusName;
        TextView ficusDesc;
        LinearLayout eachFollowing;
        ImageView imgageFans;
        TextView centerFans;
    }

    private void showDialogExitEdit(final int position) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(position);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 添加加载更多的数据
     *
     * @return
     */
    public void addData(List<MyFansData> myFansDatas) {
        mFansDatas.addAll(myFansDatas);
        notifyDataSetChanged();
    }

    public List<MyFansData> getmFansDatas() {
        return mFansDatas;
    }

    /**
     * 跳转返回后刷新的数据
     * @param mTempPos
     * @param focus
     */
    public void setEachFollowing(int mTempPos, String focus) {
        initFocus4(focus,mTempPos);
        notifyDataSetChanged();
    }
}
