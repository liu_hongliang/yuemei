package com.module.my.controller.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.BaseWebViewTel;
import com.module.my.controller.other.MyDaizhifuWebClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.widget.pullRefreshLayout.QMUIPullRefreshLayout;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.util.HashMap;

public class MyDaizhifuActivity extends AppCompatActivity {
    public static final String TAG="MyDaizhifuActivity";
    private WebView mWebView;
    private MyPullRefresh mMyPullRefresh;
    private ViewGroup nowifiLy;
    private Button refreshBt;
    private String mFlag;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private int page=1;
    private String mTitle;
    private String mDid;
    private String mZid;
    private String mUrl;
    private CommonTopBar mTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_daizhifu);

        initView();
    }

    private void initView() {
        RelativeLayout contentLy =findViewById(R.id.fragment_order_vp_content);
        mTop = findViewById(R.id.insure_tow_top);
        mMyPullRefresh = findViewById(R.id.daizhifu_refresh);
        mWebView = findViewById(R.id.daizhifu_webview);
        nowifiLy = findViewById(R.id.daizhifu_no_wifi);
        refreshBt = findViewById(R.id.refresh_bt);
        Intent intent = getIntent();
        mFlag = intent.getStringExtra("flag");
        mTitle = intent.getStringExtra("name");
        mDid = intent.getStringExtra("did");
        mZid = intent.getStringExtra("zid");
        if (mDid != null){
            if (!"0".equals(mDid)){
                mUrl=FinalConstant.MY_DIARY;
                mFlag=mDid;
            }
        }else if("足迹".equals(mZid)){
            mUrl=FinalConstant.MY_BROWSELOG;
            mFlag="";

        }else {
            mUrl=FinalConstant.MY_ORDER;
        }

        mTop.setCenterText(mTitle);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().supportMultipleWindows();
        mWebView.getSettings().setNeedInitialFocus(true);

        baseWebViewClientMessage = new BaseWebViewClientMessage(MyDaizhifuActivity.this);
        baseWebViewClientMessage.setView(contentLy);
        baseWebViewClientMessage.setParameter5983("0","32","0","0");
        baseWebViewClientMessage.setBaseWebViewClientCallback(new MyDaizhifuWebClient(MyDaizhifuActivity.this));
        baseWebViewClientMessage.setBaseWebViewTel(new BaseWebViewTel() {
            @Override
            public void tel(WebView view, String url) {
                telPhone(url);
            }
        });
        mWebView.setWebViewClient(baseWebViewClientMessage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        LodUrl1(mUrl,mFlag,"");
        /**
         * 下拉刷新监听
         */

        mMyPullRefresh.setOnPullListener(new QMUIPullRefreshLayout.OnPullListener() {
            @Override
            public void onMoveTarget(int offset) {
                Log.e(TAG, "11111");
            }

            @Override
            public void onMoveRefreshView(int offset) {
                Log.e(TAG, "222222");
            }

            @Override
            public void onRefresh() {
                Log.e(TAG, "33333");
                mMyPullRefresh.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mMyPullRefresh.finishRefresh();
                        page++;
                        LodUrl1(mUrl,mFlag,page+"");
                    }
                }, 2000);
            }
        });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        if (SystemTool.checkNet(MyDaizhifuActivity.this)) {
            mMyPullRefresh.setVisibility(View.VISIBLE);
            nowifiLy.setVisibility(View.GONE);

        } else {
            mMyPullRefresh.setVisibility(View.GONE);
            nowifiLy.setVisibility(View.VISIBLE);
        }

        refreshBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                baseWebViewClientMessage.startLoading();
                new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (SystemTool.checkNet(MyDaizhifuActivity.this)) {
                            nowifiLy.setVisibility(View.GONE);
                            baseWebViewClientMessage.stopLoading();
                            mMyPullRefresh.setVisibility(View.VISIBLE);
                            LodUrl1(mUrl,mFlag,"");

                        } else {
                            baseWebViewClientMessage.stopLoading();
                            nowifiLy.setVisibility(View.VISIBLE);
                            ViewInject.toast("网络连接失败");
                        }
                    }
                }.start();
            }
        });
    }

    public void LodUrl1(String url,String flag,String page) {
        baseWebViewClientMessage.startLoading();
        HashMap<String, Object> hashMap = new HashMap<>();
        if (!"".equals(flag)){
            if (url.endsWith("mydiary/")){
                hashMap.put("id",flag);
            }else {
                hashMap.put("flag",flag);
            }

            if (!page.equals("")){
                hashMap.put("page",page);
            }
        }

        WebSignData addressAndHead = SignUtils.getAddressAndHead(url,hashMap);
        if (null != mWebView) {
            Log.d(TAG,"url:=="+addressAndHead.getUrl());
            mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }

    }

    private void telPhone(String url) {
        String aa = url.replace("tel:", "");
        ViewInject.toast("正在拨打中·····");
        Intent intent = new Intent(Intent.ACTION_CALL, Uri
                .parse("tel:" + aa));
        try {
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
