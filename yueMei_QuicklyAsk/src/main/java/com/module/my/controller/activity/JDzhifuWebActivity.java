package com.module.my.controller.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.my.view.orderpay.OrderMethodActivity594;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 京东支付
 *
 * Created by dwb on 16/8/24.
 */
public class JDzhifuWebActivity extends BaseActivity {

    private final String TAG = "JDzhifuWebActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.basic_web_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private JDzhifuWebActivity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;
    private String uid;
    private String postDate;
    private String order_id;
    private static final int REQUEST_JD_CODE = 888888;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;

    @BindView(id = R.id.title_bar_rly)
    private RelativeLayout biaoView;
    private BaseWebViewClientMessage baseWebViewClientMessage;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_peifu_basic_web);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = JDzhifuWebActivity.this;
        uid= Utils.getUid();

        windowsH = Cfg.loadInt(mContex, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
        biaoView.measure(w, h);
        bTheight =biaoView.getMeasuredHeight();

        Rect rectangle= new Rect();
        Window window= getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight= rectangle.top;

        Intent it=getIntent();
        order_id=it.getStringExtra("order_id");

        postDate = "source=3&pay_method=13&order_id="+order_id;

        url = FinalConstant.JDPAYURL;

        mTop.setCenterText("京东支付");

        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                if(url.startsWith(FinalConstant.JDPAYSUCCESSURL)){//支付成功
                    Intent it=new Intent();
                    it.putExtra("type_code", "1");
                    it.setClass(mContex, OrderMethodActivity594.class);
                    setResult(REQUEST_JD_CODE, it);
                    finish();
                }else if(url.startsWith(FinalConstant.JDPAYFAILDURL)){//支付失败
                    Intent it=new Intent();
                    it.putExtra("type_code", "0");
                    it.setClass(mContex, OrderMethodActivity594.class);
                    setResult(REQUEST_JD_CODE, it);
                    finish();
                }else if(url.startsWith("http://m.yuemei.com/home/")){
                    Intent it=new Intent();
                    it.putExtra("type_code", "2");
                    it.setClass(mContex, OrderMethodActivity594.class);
                    setResult(REQUEST_JD_CODE, it);
                    finish();
                } else {
                    docDetWeb.loadUrl(url);
                }
            }
        });
        initWebview();

        LodUrl1(url);

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.getSettings().setUserAgentString("YueMei_QuicklyAsk");
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = windowsH-bTheight-statusBarHeight*2;
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);

        contentWeb.addView(docDetWeb);
    }
    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();
        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
