package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.SumitHttpAip;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.view.MyToast;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 我的经验
 * 
 * @author Rubin
 * 
 */
public class MyExperiencePointsActivity extends BaseActivity {

	private final String TAG = "MyExperiencePointsActivity";

	@BindView(id = R.id.jingyan_web_det_scrollview3, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.jingyan_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.my_experience_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private MyExperiencePointsActivity mContex;

	public JSONObject obj_http;

	private String uid;
	private static final int SHOW_TIME = 1000;

	@BindView(id=R.id.content_ly)
	private LinearLayout contentLy;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_myexperience_points);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = MyExperiencePointsActivity.this;
		uid = Utils.getUid();

		mTop.setCenterText("我的经验等级");

		scollwebView.GetLinearLayout(contentWeb);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setView(contentLy);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						MyExperiencePointsActivity.this.finish();
					}
				});
		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	public void onResume() {
		super.onResume();

		LodUrl1(FinalConstant.MY_EXPENCE);
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 * 
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("3")) {// 新手好礼
				String flag = obj.getString("flag");
				sumitHttpCode(flag);
			}

			if (obj.getString("type").equals("4")) {// 签到
				String flag = obj.getString("flag");
				String qd = obj.getString("qd");
				if (qd.equals("1")) {
					sumitHttpCode(flag);
				} else {
					MyToast.makeImgAndTextToast(mContex,
							getResources().getDrawable(R.drawable.tips_smile),
							"今日已经签到", SHOW_TIME).show();
				}
			}

			if (obj.getString("type").equals("5")) {// 完善资料
				String flag = obj.getString("flag");
				Intent it0 = new Intent();
				it0.putExtra("type", "1");
				it0.putExtra("flag", flag);
				it0.setClass(mContex, ModifyMyDataActivity.class);
				startActivity(it0);
			}


			if (obj.getString("type").equals("7")) {// 邀请好友
				// String flag = obj.getString("flag");
				Intent it0 = new Intent();
				it0.setClass(mContex, InvitationFriendActivity.class);
				startActivity(it0);
			}


		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();

		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

	}

	void sumitHttpCode(final String flag) {
		SumitHttpAip sumitHttpAip = new SumitHttpAip();
		Map<String, Object> maps = new HashMap<>();
		maps.put("flag",flag);
		maps.put("uid",uid);
		sumitHttpAip.getCallBack(mContex, maps, new BaseCallBackListener<JFJY1Data>() {
			@Override
			public void onSuccess(JFJY1Data jfjyData) {
				if (jfjyData != null){
					String jifenNu = jfjyData.getIntegral();
					String jyNu = jfjyData.getExperience();

					if (!jifenNu.equals("0") && !jyNu.equals("0")) {
						MyToast.makeTexttext4Toast(mContex, jifenNu, jyNu,
								SHOW_TIME).show();
					} else {
						if (!jifenNu.equals("0")) {
							MyToast.makeTexttext2Toast(mContex, jifenNu,
									SHOW_TIME).show();
						} else {
							if (!jyNu.equals("0")) {
								MyToast.makeTexttext3Toast(mContex, jyNu,
										SHOW_TIME).show();

							}
						}
					}
					docDetWeb.loadUrl(FinalConstant.MY_EXPENCE + uid + "/" + Utils.getTokenStr());
				}
			}
		});
	}


	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}