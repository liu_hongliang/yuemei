package com.module.my.controller.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.my.controller.other.UploadStatusException;
import com.module.my.model.bean.PostingAndNoteButton;
import com.module.my.model.bean.PostingAndNoteData;
import com.module.my.model.bean.PostingAndNoteImage;
import com.module.my.model.bean.PostingAndNoteUpload;
import com.module.my.model.bean.PostingAndNoteVideo;
import com.module.my.view.view.PostNoteUploadItem;
import com.quicklyask.activity.R;
import com.quicklyask.view.EditExitDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * 发帖视频图片适配器
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int BUTTON = 1;
    private static final int UPLOAD = 2;
    private List<PostingAndNoteData> mDatas;
    private Activity mContext;
    private String TAG = "PostingAndNoteAdapter";
    public static final String UPLOAD_PROGRESS = "upload_progress";
    public static final String UPLOAD_COVER = "upload_cover";
    public static final String UPLOAD_IMAGE = "upload_image";

    public PostingAndNoteAdapter(Activity context, List<PostingAndNoteData> data) {
        this.mContext = context;
        this.mDatas = data;
    }

    @Override
    public int getItemViewType(int position) {
        if (mDatas.get(position).isButton()) {
            return BUTTON;
        } else {
            return UPLOAD;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == UPLOAD) {
            return new ViewHolderUpload(LayoutInflater.from(mContext).inflate(R.layout.item_post_note_img_video, parent, false));
        } else {
            return new ViewHolderButton(LayoutInflater.from(mContext).inflate(R.layout.item_post_note_button, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            if (holder instanceof ViewHolderUpload) {
                ViewHolderUpload viewHolderUpload = (ViewHolderUpload) holder;

                for (Object payload : payloads) {
                    switch ((String) payload) {
                        case UPLOAD_PROGRESS:                      //上传进度更新
                            int progress = mDatas.get(position).getPostingAndNoteUpload().getProgress();

                            if (mDatas.get(position).getPostingAndNoteUpload().isVideo()) {
                                viewHolderUpload.mUploadImg.setContent(PostNoteUploadItem.CONTEXT2);
                            } else {
                                viewHolderUpload.mUploadImg.setContent(PostNoteUploadItem.CONTEXT1);
                            }

                            viewHolderUpload.mUploadImg.setContentShow(View.VISIBLE, View.VISIBLE);
                            viewHolderUpload.mUploadImg.startUpload(progress);

                            if (progress == 100) {
                                viewHolderUpload.mUploadImg.setContentShow(View.GONE, View.GONE);

                                if (!mDatas.get(position).getPostingAndNoteUpload().isVideo()) {
                                    boolean isCover = mDatas.get(position).getPostingAndNoteUpload().getPostingAndNoteImage().isCover();      //是否是封面
                                    if (isCover) {
                                        viewHolderUpload.mCover.setVisibility(View.VISIBLE);
                                    } else {
                                        viewHolderUpload.mCover.setVisibility(View.GONE);
                                    }
                                }
                            }

                            break;

                        case UPLOAD_COVER:                      //视频图片封面刷新

                            if (!mDatas.get(position).getPostingAndNoteUpload().isVideo()) {
                                boolean isCover = mDatas.get(position).getPostingAndNoteUpload().getPostingAndNoteImage().isCover();      //是否是封面
                                Log.e(TAG, "isCover === " + isCover);
                                if (isCover && mDatas.get(position).getPostingAndNoteUpload().getFileUploadState() == PostingAndNoteUpload.FileUploadState.UPLOADED_SUCCESSFULLY) {
                                    viewHolderUpload.mCover.setVisibility(View.VISIBLE);
                                } else {
                                    viewHolderUpload.mCover.setVisibility(View.GONE);
                                }
                            }

                            break;
                        case UPLOAD_IMAGE:                      //重新上传照片

                            if (mDatas.get(position).getPostingAndNoteUpload().isVideo()) {

                                Glide.with(mContext).load(mDatas.get(position).getPostingAndNoteUpload().getPostingAndNoteVideo().getVideoPath()).into(viewHolderUpload.mUploadImg.getUploadImage());
                            } else {
                                Glide.with(mContext).load(mDatas.get(position).getPostingAndNoteUpload().getPostingAndNoteImage().getImgPath()).into(viewHolderUpload.mUploadImg.getUploadImage());
                            }

                            break;
                    }

                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderUpload) {

            setUploadView((ViewHolderUpload) holder, position);

        } else {
            setButtonView((ViewHolderButton) holder, position);

        }
    }

    /**
     * 设置图片的样式
     *
     * @param holder
     * @param position
     */
    private void setUploadView(ViewHolderUpload holder, int position) {
        PostingAndNoteData data = mDatas.get(position);
        PostingAndNoteUpload mUploads = data.getPostingAndNoteUpload();
        boolean isVideo = mUploads.isVideo();           //是否是视频

        holder.mCover.setVisibility(View.GONE);

        //设置文案
        if (mUploads.getFileUploadState() == PostingAndNoteUpload.FileUploadState.NOT_UPLOAD || mUploads.getFileUploadState() == PostingAndNoteUpload.FileUploadState.UPLOADING) {
            holder.mUploadImg.setContentShow(View.VISIBLE, View.GONE);
            if (isVideo) {
                holder.mUploadImg.setContent(PostNoteUploadItem.CONTEXT2);
            } else {
                holder.mUploadImg.setContent(PostNoteUploadItem.CONTEXT1);
            }
        } else if (mUploads.getFileUploadState() == PostingAndNoteUpload.FileUploadState.UPLOAD_FAILED) {  //上传失败
            holder.mUploadImg.setContentShow(View.VISIBLE, View.VISIBLE);
            holder.mUploadImg.setContent(PostNoteUploadItem.CONTEXT3, PostNoteUploadItem.CONTEXT4);
        } else {                                                                                           //上传成功
            holder.mUploadImg.setContentShow(View.GONE, View.GONE);
        }

        holder.mUploadImg.getUploadImage().startUpload(mUploads.getProgress());

        if (isVideo) {
            PostingAndNoteVideo postingAndNoteVideo = mUploads.getPostingAndNoteVideo();

            holder.mUploadImg.getUploadImage().setVideoButton(true);

            holder.mUploadImg.getUploadImage().startUpload(mUploads.getProgress());

            Glide.with(mContext).load(postingAndNoteVideo.getVideoPath()).into(holder.mUploadImg.getUploadImage());

        } else {
            PostingAndNoteImage postingAndNoteImage = mUploads.getPostingAndNoteImage();

            holder.mUploadImg.getUploadImage().setVideoButton(false);

            Glide.with(mContext).load(postingAndNoteImage.getImgPath()).into(holder.mUploadImg.getUploadImage());

        }
    }

    /**
     * 设置按钮的数据
     *
     * @param holder
     * @param position
     */
    private void setButtonView(ViewHolderButton holder, int position) {
        PostingAndNoteData data = mDatas.get(position);
        PostingAndNoteButton buttons = data.getPostingAndNoteButton();
        if (buttons.isVideoButton()) {
            holder.mButton.setImageResource(R.drawable.upload_video_button);
        } else {
            holder.mButton.setImageResource(R.drawable.upload_image_button);
        }

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolderUpload extends RecyclerView.ViewHolder {

        private PostNoteUploadItem mUploadImg;
        private ImageView mDelete;
        private TextView mCover;

        public ViewHolderUpload(View itemView) {
            super(itemView);

            mUploadImg = itemView.findViewById(R.id.post_note_img_video);
            mDelete = itemView.findViewById(R.id.post_note_img_video_delete);
            mCover = itemView.findViewById(R.id.post_note_img_video_content);

            //删除按钮点击
            mDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) {
                        onClickListener.onImageDelete(mDatas.get(getLayoutPosition()).getPostingAndNoteUpload().isVideo(), getLayoutPosition());
                    }
                }
            });

            //图片点击
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickListener != null) {
                        onClickListener.onItemImageView(mDatas.get(getLayoutPosition()).getPostingAndNoteUpload().isVideo(), getLayoutPosition());
                    }
                }
            });
        }
    }

    public class ViewHolderButton extends RecyclerView.ViewHolder {

        private ImageView mButton;

        public ViewHolderButton(View itemView) {
            super(itemView);

            mButton = itemView.findViewById(R.id.post_note_button);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mDatas.get(getLayoutPosition()).getPostingAndNoteButton().isVideoButton()) {      //视频按钮点击
                        if (!mDatas.get(0).isButton() && !mDatas.get(0).getPostingAndNoteUpload().isVideo()) {      //如果第一个不是按钮，且第一个是图片
                            showDialogExitEdit("只能选择视频或图片一种形式", "我知道了");
                        } else {
                            if (onClickListener != null) {
                                onClickListener.onItemButtonClick(mDatas.get(getLayoutPosition()).getPostingAndNoteButton().isVideoButton(), getLayoutPosition());
                            }
                        }
                    } else {              //图片按钮点击
                        if (!mDatas.get(0).isButton() && mDatas.get(0).getPostingAndNoteUpload().isVideo()) {       //如果第一个不是按钮，且第一个是视频
                            showDialogExitEdit("只能选择视频或图片一种形式", "我知道了");
                        } else {
                            if (onClickListener != null) {
                                onClickListener.onItemButtonClick(mDatas.get(getLayoutPosition()).getPostingAndNoteButton().isVideoButton(), getLayoutPosition());
                            }
                        }
                    }
                }
            });
        }
    }

    /**
     * 获取数据
     *
     * @return
     */
    public List<PostingAndNoteData> getDatas() {
        return mDatas;
    }

    /**
     * 获取最后一个上传成功，或者上传失败的图片下标
     *
     * @return
     */
    public int uploadSuccessState() throws UploadStatusException {
        int pos = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            PostingAndNoteData data = mDatas.get(i);
            if (!data.isButton()) {
                PostingAndNoteUpload.FileUploadState fileUploadState = data.getPostingAndNoteUpload().getFileUploadState();
                if (fileUploadState == PostingAndNoteUpload.FileUploadState.UPLOADED_SUCCESSFULLY || fileUploadState == PostingAndNoteUpload.FileUploadState.UPLOAD_FAILED) {
                    pos = i;
                }
            }
        }

        isNegative("没有获取到最后一个上传成功，或者上传失败的图片下标", pos);

        return pos;
    }

    /**
     * 获取等待上传的下标
     *
     * @return
     */
    public int uploadingState() throws UploadStatusException {
        int pos = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            PostingAndNoteData data = mDatas.get(i);
            if (!data.isButton()) {
                PostingAndNoteUpload.FileUploadState fileUploadState = data.getPostingAndNoteUpload().getFileUploadState();
                if (fileUploadState == PostingAndNoteUpload.FileUploadState.UPLOADING) {
                    pos = i;
                }
            }
        }

        isNegative("没有获取到等待上传图片的下标", pos);

        return pos;
    }

    /**
     * 添加多条数据
     */
    public void addData(List<PostingAndNoteData> datas) {
        mDatas.addAll(datas);
        notifyItemRangeChanged(0, datas.size() + 2);
    }

    /**
     * 从第几条添加多条数据
     *
     * @param pos
     * @param datas
     */
    public void addData(int pos, List<PostingAndNoteData> datas) {
        mDatas.addAll(pos, datas);
        Log.e(TAG, "pos == " + pos);
        Log.e(TAG, "datas.size() == " + datas.size());
        notifyItemRangeChanged(pos, datas.size() + 2);
    }

    /**
     * 添加一条数据
     *
     * @param pos
     * @param data
     */
    public void addData(int pos, PostingAndNoteData data) {
        mDatas.add(pos, data);
        notifyItemInserted(pos);
    }

    /**
     * 删除一条数据
     *
     * @param pos
     */
    public void deleteData(int pos) {
        mDatas.remove(pos);             //删除数据源
        notifyItemRemoved(pos);         //刷新被删除的地方
//        notifyItemRangeChanged(pos, getItemCount()); //刷新被删除数据，以及其后面的数据
    }

    /**
     * 刷新某一条数据
     *
     * @param pos
     */
    public void notifyItem(int pos) {
        Log.e(TAG, "3333pos === " + pos);
        notifyItemChanged(pos);
    }

    /**
     * 进度刷新
     *
     * @param pos
     * @param progress
     */
    public void notifyItemPart(int pos, int progress) {
        mDatas.get(pos).getPostingAndNoteUpload().setProgress(progress);
        notifyItemPayload(pos, UPLOAD_PROGRESS);
    }

    /**
     * 刷新某一条部分位置数据
     *
     * @param pos
     * @param payload
     */
    public void notifyItemPayload(int pos, String payload) {
        notifyItemChanged(pos, payload);
    }

    /**
     * 获取视频图片按钮下标
     *
     * @param isVideo true 获取添加视频按钮的下标，flase 获取添加图片按钮的下标
     * @return
     */
    public int buttonPos(boolean isVideo) throws UploadStatusException {
        int pos = -1;
        if (isVideo) {
            for (int i = 0; i < mDatas.size(); i++) {
                if (mDatas.get(i).isButton() && mDatas.get(i).getPostingAndNoteButton().isVideoButton()) {
                    pos = i;
                }
            }
        } else {
            for (int i = 0; i < mDatas.size(); i++) {
                if (mDatas.get(i).isButton() && !mDatas.get(i).getPostingAndNoteButton().isVideoButton()) {
                    pos = i;
                }
            }
        }

        if (isVideo) {
            isNegative("没有获取到视频按钮下标", pos);
        } else {
            isNegative("没有获取到图片按钮下标", pos);
        }

        return pos;
    }

    /**
     * 视频或者图片按钮是否存在
     *
     * @param isVideo
     * @return
     */
    public boolean haveButton(boolean isVideo) {
        if (isVideo) {
            for (int i = 0; i < mDatas.size(); i++) {
                if (mDatas.get(i).isButton() && mDatas.get(i).getPostingAndNoteButton().isVideoButton()) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < mDatas.size(); i++) {
                if (mDatas.get(i).isButton() && !mDatas.get(i).getPostingAndNoteButton().isVideoButton()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 获取视频的数据
     */
    public PostingAndNoteVideo getVideoData() throws UploadStatusException {
        PostingAndNoteVideo data = null;
        if (mDatas.size() > 0 && !mDatas.get(0).isButton() && mDatas.get(0).getPostingAndNoteUpload().isVideo()) {
            data = mDatas.get(0).getPostingAndNoteUpload().getPostingAndNoteVideo();
        }

        isNegative("未获取到视频的数据", data);

        return data;
    }

    /**
     * 获取图片路径集合
     *
     * @return
     */
    public ArrayList<String> getImgPaths() {
        ArrayList<String> imgs = new ArrayList<>();
        for (PostingAndNoteData data : mDatas) {
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo()) {
                imgs.add(data.getPostingAndNoteUpload().getPostingAndNoteImage().getImgPath());
            }
        }
        return imgs;
    }

    /**
     * 获取非按钮的item个数
     *
     * @return
     */
    public int notButton() {
        int pos = 0;
        for (PostingAndNoteData data : mDatas) {
            if (!data.isButton()) {
                pos++;
            }
        }
        return pos;
    }

    /**
     * 获取封面图item数据
     *
     * @return
     */
    public PostingAndNoteImage coverData() throws UploadStatusException {
        PostingAndNoteImage coverData = null;
        for (PostingAndNoteData data : mDatas) {
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo() && data.getPostingAndNoteUpload().getPostingAndNoteImage().isCover()) {
                coverData = data.getPostingAndNoteUpload().getPostingAndNoteImage();
            }
        }

        isNegative("未获取到封面图数据", coverData);

        return coverData;
    }

    /**
     * 封面数据是否存在
     *
     * @return
     */
    public boolean haveCoverData() {
        for (PostingAndNoteData data : mDatas) {
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo() && data.getPostingAndNoteUpload().getPostingAndNoteImage().isCover()) {
                return true;
            }
        }

        return false;
    }

    /**
     * 获取封面图pos
     *
     * @return
     */
    public int coverPos() throws UploadStatusException {
        int pos = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            PostingAndNoteData data = mDatas.get(i);
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo() && data.getPostingAndNoteUpload().getPostingAndNoteImage().isCover()) {
                pos = i;
            }
        }

        isNegative("没有获取到封面图下标", pos);

        return pos;
    }

    /**
     * 根据下标获取某条图片数据
     *
     * @param pos
     * @return
     */
    public PostingAndNoteImage getImageData(int pos) throws UploadStatusException {
        PostingAndNoteImage data;
        if (!mDatas.get(pos).isButton() && !mDatas.get(pos).getPostingAndNoteUpload().isVideo()) {
            data = mDatas.get(pos).getPostingAndNoteUpload().getPostingAndNoteImage();
        } else {
            data = null;
        }

        isNegative("未获取到图片数据", data);

        return data;
    }


    /**
     * 根据下标获取某条视频或者图片的数据
     *
     * @param pos
     * @return
     */
    public PostingAndNoteUpload getUploadData(int pos) throws UploadStatusException {
        PostingAndNoteUpload data;
        if (!mDatas.get(pos).isButton()) {
            data = mDatas.get(pos).getPostingAndNoteUpload();
        } else {
            data = null;
        }

        isNegative("未获取视频或者图片的数据", data);

        return data;
    }

    /**
     * 通过图片路径获取下标
     *
     * @param imgPath 图片本地路径
     * @return
     */
    public int getImgPathOfPos(String imgPath) throws UploadStatusException {
        int pos = -1;
        for (int i = 0; i < mDatas.size(); i++) {
            PostingAndNoteData data = mDatas.get(i);
            if (!data.isButton() && !data.getPostingAndNoteUpload().isVideo()) {
                PostingAndNoteImage imgData = data.getPostingAndNoteUpload().getPostingAndNoteImage();
                if (imgPath.equals(imgData.getImgPath())) {
                    pos = i;
                }
            }
        }

        isNegative("没有通过图片路径获取到下标", pos);

        return pos;
    }

    /**
     * 判断图片或者视频是否全部上传完成了
     *
     * @return true全部上传完成了，false正在上传中
     */
    public boolean isUpload() {
        for (PostingAndNoteData data : mDatas) {
            if (!data.isButton() && (data.getPostingAndNoteUpload().getFileUploadState() == PostingAndNoteUpload.FileUploadState.NOT_UPLOAD || data.getPostingAndNoteUpload().getFileUploadState() == PostingAndNoteUpload.FileUploadState.UPLOADING)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 当前上传的是否是视频
     *
     * @return true 视频
     */
    public boolean isUploadVideo() {

        return mDatas.size() > 0 && !mDatas.get(0).isButton() && mDatas.get(0).getPostingAndNoteUpload().isVideo();

    }

    /**
     * 如果获取的是负数，抛异常
     *
     * @param pos
     * @throws UploadStatusException
     */
    private void isNegative(String prompt, int pos) throws UploadStatusException {

        if (pos < 0) {
            throw new UploadStatusException(prompt);
        }
    }

    /**
     * 获取数据如果是null，抛异常
     *
     * @param prompt
     * @param data
     * @throws UploadStatusException
     */
    private void isNegative(String prompt, Object data) throws UploadStatusException {

        if (data == null) {
            throw new UploadStatusException(prompt);
        }
    }

    private OnClickListener onClickListener;

    public interface OnClickListener {
        void onItemButtonClick(boolean isVideo, int pos);                      //按钮点击回调

        void onImageDelete(boolean isVideo, int pos);                                                   //删除按钮

        void onItemImageView(boolean isVideo, int pos);                                                 //图片点击
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * dialog提示
     *
     * @param title
     * @param content
     */
    public void showDialogExitEdit(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setTextColor(Color.parseColor("#ffa5cc"));
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }
}
