package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.my.view.orderpay.OrderWriteMessageActivity639;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.BaoxianPopWindow;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 保险授权页
 *
 * Created by dwb on 16/9/6.
 */
public class InSureActivity extends BaseActivity {

    private final String TAG = "InSureActivity";
    private InSureActivity mContex;

    @BindView(id = R.id.insure_tow_top)
    private CommonTopBar mTop;// 返回

    @BindView(id = R.id.reg_username)
    private EditText insureNameEt;
    @BindView(id = R.id.reg_phone)
    private EditText insurePhoneEt;
    @BindView(id = R.id.reg_sfz)
    private EditText insureSFZEt;

    @BindView(id = R.id.insure_baoxina_tv)
    private TextView insureTitleTv;

    @BindView(id = R.id.insure_skip_bt, click = true)
    private Button skipBt;//
    @BindView(id = R.id.insure_tijiao_bt, click = true)
    private Button sumbitBt;//

    @BindView(id = R.id.insure_nan_iv, click = true)
    private ImageView nanIv;
    @BindView(id = R.id.insure_nv_iv, click = true)
    private ImageView nvIv;

    private String sexS="女";

    private BaoxianPopWindow baoxianPop;

    private String taoid;
    private String insure_name;
    private String insure_phone;
    private String insure_card;
    private String insure_sex;

    private boolean isModiy=false;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_insure);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContex = InSureActivity.this;
        mTop.setCenterText("保险授权页");

        Intent it = getIntent();
        insure_name = it.getStringExtra("insure_name");
        insure_phone = it.getStringExtra("insure_phone");
        insure_card = it.getStringExtra("insure_card");
        insure_sex = it.getStringExtra("insure_sex");

        if(!TextUtils.isEmpty(insure_name)){

            insureNameEt.setText(insure_name);
            insurePhoneEt.setText(insure_phone);
            insureSFZEt.setText(insure_card);
            isModiy=true;

            if("1".equals(insure_sex)){
                nanIv.setBackgroundResource(R.drawable.insure_nan_yes);
                nvIv.setBackgroundResource(R.drawable.insure_nv_no);
                insure_sex="1";
                sexS="男";
            }else {
                nanIv.setBackgroundResource(R.drawable.insure_nan_no);
                nvIv.setBackgroundResource(R.drawable.insure_nv_yes);
                insure_sex="2";
                sexS="女";
            }
        }



        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        InSureActivity.this.finish();
                    }
                });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String aa="本页所填写的内容仅用于投保“悦美·平安安心整形险"+"<u><font color=\"#ff5c77\"> (详细条款) </font> </u>"+"之用,不会泄露给第三方。如您同意此条款之内容并申请投保，请填写并提交一下信息：";
        insureTitleTv.setText(Html.fromHtml(aa));

        insureTitleTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlss=FinalConstant.TAO_BAOXIAN;

                HashMap<String, Object> urlMap6080 = new HashMap<>();
                urlMap6080.put("tao_id",taoid);
                baoxianPop=new BaoxianPopWindow(mContex,urlss,urlMap6080);
                baoxianPop.showAtLocation(findViewById(R.id.sildingFinishLayout), Gravity.CENTER,0,0);
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.insure_skip_bt://跳过
                onBackPressed();
                break;
            case R.id.insure_tijiao_bt://提交
                insure_name = insureNameEt.getText().toString().trim();
                insure_phone = insurePhoneEt.getText().toString().trim();
                insure_card = insureSFZEt.getText().toString().trim();

                if(!TextUtils.isEmpty(insure_name)){
                    if(!TextUtils.isEmpty(insure_phone)){
                        if (judgeEmailAndPhone(insure_phone)) {
                            if(personIdValidation(insure_card)){

                                showDialogModiyMessage();

                            }else {
                                ViewInject.toast("请输入正确的身份证号");
                            }
                        }else {
                            ViewInject.toast("请正确填写您手机号");
                        }
                    }else {
                        ViewInject.toast("请填写您的手机号");
                    }
                }else {
                    ViewInject.toast("请填写被保人姓名");
                }
                break;
            case R.id.insure_nan_iv:
                if("2".equals(insure_sex)){
                    nanIv.setBackgroundResource(R.drawable.insure_nan_yes);
                    nvIv.setBackgroundResource(R.drawable.insure_nv_no);
                    insure_sex="1";
                    sexS="男";
                }
                break;
            case R.id.insure_nv_iv:
                if("1".equals(insure_sex)){
                    nanIv.setBackgroundResource(R.drawable.insure_nan_no);
                    nvIv.setBackgroundResource(R.drawable.insure_nv_yes);
                    insure_sex="2";
                    sexS="女";
                }
                break;
        }
    }

    void showDialogModiyMessage() {
        final EditExitDialog editDialog = new EditExitDialog(mContex,
                R.style.mystyle, R.layout.dialog_baoxian_suremessage);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView contenTv = editDialog.findViewById(R.id.dialog_exit_content_tv);
        String txxt="姓名："+insure_name+"；身份证号："+insure_card+"；性别："+sexS+"；手机号："+insure_phone;
        contenTv.setText(txxt);

        Button cancleBt = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancleBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDialog.dismiss();
            }
        });

        Button   trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();

                if(isModiy){
                    ViewInject.toast("修改成功");

                    onBackPressed();

                }else {
                    showDialogLingquSuecss(insure_name,insure_phone,insure_card);
                }


            }
        });
    }



    void showDialogLingquSuecss(final String name,final String phone,final String card) {
        final EditExitDialog editDialog = new EditExitDialog(mContex,
                R.style.mystyle, R.layout.dialog_baoxian_shouquan);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        Button trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                onBackPressed();
                editDialog.dismiss();
            }
        });
    }


    private boolean judgeEmailAndPhone(String nameStr) {
        if (nameStr.contains("@")) {
            return Utils.emailFormat(nameStr);
        } else {
            return Utils.isMobile(nameStr);
        }
    }

    /**
     * 验证身份证号是否符合规则
     * @param text 身份证号
     * @return
     */
    public boolean personIdValidation(String text) {
        String regx = "[0-9]{17}x";
        String reg1 = "[0-9]{15}";
        String regex = "[0-9]{18}";
        return text.matches(regx) || text.matches(reg1) || text.matches(regex);
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    public void onBackPressed() {

        Intent it = new Intent();
        it.putExtra("name", insure_name);
        it.putExtra("phone", insure_phone);
        it.putExtra("card", insure_card);
        it.putExtra("sex", insure_sex);
        it.setClass(mContex,
                OrderWriteMessageActivity639.class);
        setResult(99, it);

        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);


    }
}
