package com.module.my.controller.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.util.WebUrlTypeUtil;

import org.xutils.common.util.DensityUtil;

import java.util.List;

/**
 * 文 件 名: PersonGvBallAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-07 12:33
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class PersonGvBallAdapter extends BaseQuickAdapter<AdertAdv.BigPromotionBallBean, BaseViewHolder> {
    private Context mContext;
    private final int mWindowsWight;
    private List<AdertAdv.BigPromotionBallBean> mData;

    public PersonGvBallAdapter(Context context, int layoutResId, @Nullable List<AdertAdv.BigPromotionBallBean> data, int windowsWight) {
        super(layoutResId, data);
        this.mContext = context;
        this.mData = data;
        this.mWindowsWight = windowsWight;
    }


    @Override
    protected void convert(BaseViewHolder helper, final AdertAdv.BigPromotionBallBean item) {
//        LinearLayout itemView = helper.getView(R.id.item_ball);
//        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) itemView.getLayoutParams();
//        layoutParams.width = (mWindowsWight - DensityUtil.dip2px(50)) / mData.size();
//        itemView.setLayoutParams(layoutParams);

        ImageView iv_ball = helper.getView(R.id.iv_ball);
        TextView tv_title = helper.getView(R.id.tv_ball_title);
        tv_title.setText(item.getTitle());
        Glide.with(mContext).load(item.getImg()).transform(new GlideCircleTransform(mContext)).into(iv_ball);
        iv_ball.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YmStatistics.getInstance().tongjiApp(item.getEvent_params());
                WebUrlTypeUtil.getInstance(mContext).urlToApp(item.getUrl());
            }
        });
        tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YmStatistics.getInstance().tongjiApp(item.getEvent_params());
                WebUrlTypeUtil.getInstance(mContext).urlToApp(item.getUrl());
            }
        });
    }

}
