package com.module.my.controller.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.view.CommonTopBar;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.RenZhengApi;
import com.module.my.model.api.RenZhengOkApi;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.Validator;
import com.quicklyask.view.YueMeiDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RenZhengNameActivity extends YMBaseActivity {
    public static final String TAG="RenZhengNameActivity";
    @BindView(R.id.wallet_renzheng_title)
    CommonTopBar mWalletMingxiTitle;
    @BindView(R.id.renzheng_tip)
    TextView mRenzhengTip;
    @BindView(R.id.renzheng_name)
    EditText mRenzhengName;  //姓名
    @BindView(R.id.renzheng_cardid)
    EditText mRenzhengCardid; //身份证号
    @BindView(R.id.renzheng_btn)
    Button mRenzhengBtn;
    Context mContext;

    private String mRealName;
    private String mCardId;
    private String mUid;
    private YueMeiDialog mYueMeiDialog;
    private String mFaceType;
    private LoadingProgress loadingProgress;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_renzhengz_name;
    }

    @Override
    protected void initView() {
        mContext=RenZhengNameActivity.this;
        mWalletMingxiTitle.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                //客服跳转
                Intent it = new Intent();
                it.setClass(mContext, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
            }
        });
    }

    @Override
    protected void initData() {
        Intent intent = getIntent();
        mRealName = intent.getStringExtra("real_name");
        mCardId = intent.getStringExtra("card_id");
        if (!TextUtils.isEmpty(mRealName) && !TextUtils.isEmpty(mCardId)) {//资料页跳过来
            String substring = mRealName.substring(1);
            mRenzhengName.setText("*" + substring);
            mRenzhengCardid.setText(mCardId);
            mRenzhengBtn.setVisibility(View.GONE);
            mRenzhengTip.setVisibility(View.GONE);
            setEditTextState(mRenzhengName, false);
            setEditTextState(mRenzhengCardid, false);
        } else {

            mRenzhengBtn.setVisibility(View.VISIBLE);
            mRenzhengTip.setVisibility(View.VISIBLE);
            setEditTextState(mRenzhengName, true);
            setEditTextState(mRenzhengCardid, true);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
//        mFaceType = Cfg.loadStr(mContext, "face_type", "");
        mFaceType="1";
        TextChange textChange = new TextChange();
        mRenzhengName.addTextChangedListener(textChange);
        mRenzhengCardid.addTextChangedListener(textChange);
        mUid = Utils.getUid();
        loadingProgress = new LoadingProgress(RenZhengNameActivity.this);


    }

    @Override
    protected void onResume() {
        super.onResume();
         redirectTo();
    }

    private void redirectTo(){
       String scheme = getIntent().getScheme();    // 获得Scheme名称
        String data = getIntent().getDataString();  // 获得Uri全部路径

        Log.e(TAG, "scheme == " + scheme);
        Log.e(TAG, "data == " + data);
        if (!TextUtils.isEmpty(data)) {
            if (data.contains("yuemeischemes")){
                finish();
                HashMap<String, Object> maps = new HashMap<>();
                String biz_no = mFunctionManager.loadStr("biz_no", "");
                if (TextUtils.isEmpty(biz_no))return;
                maps.put("biz_no", biz_no);
                new RenZhengOkApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData s) {
                        if ("1".equals(s.code)) {
                            mFunctionManager.saveStr("is_real","1");
                            mFunctionManager.showShort(" 芝麻验证成功");
                            finish();
                            BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
                            Map<String, Object> params = new HashMap<>();
                            params.put("id", mUid);
                            userInfoApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                                @Override
                                public void onSuccess(ServerData serverData) {
                                    if ("1".equals(serverData.code)){
                                        UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                                        String cardId = userData.getCard_id();
                                        String real_name = userData.getReal_name();
                                        Cfg.saveStr(mContext,"card_id",cardId);
                                        Cfg.saveStr(mContext,"real_name",real_name);
                                    }else {
                                        Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                                    }


                                }
                            });
                        }
                    }
                });
            }else {
                mFunctionManager.showShort(" 芝麻验证失败");
            }

        }
    }

    @OnClick(R.id.renzheng_btn)
    public void onViewClicked() {
        final String name = mRenzhengName.getText().toString().trim();
        final String idCard = mRenzhengCardid.getText().toString().trim();
        if (Validator.isIDCard(idCard)){
            mYueMeiDialog = new YueMeiDialog(mContext, "我们将使用您的信息进行支付宝芝麻信用实名认证，认证成功后将无法修改，请确保您的信息真实有效", "修改", "继续");
            mYueMeiDialog.setCanceledOnTouchOutside(false);
            mYueMeiDialog.show();
            mYueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                @Override
                public void leftBtnClick() {
                    mYueMeiDialog.dismiss();
                }

                @Override
                public void rightBtnClick() {
                    //调取芝麻认证
                    if (Utils.isFastDoubleClick()){
                        return;
                    }
                    mYueMeiDialog.dismiss();
                    loadingProgress.startLoading();
                   HashMap<String,Object> maps=new HashMap<>();
                   maps.put("card_id",idCard);
                   maps.put("real_name",name);
                   maps.put("face_type",mFaceType);
                   new RenZhengApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                       @Override
                       public void onSuccess(ServerData serverData) {
                           if ("1".equals(serverData.code)){
                               try {
                                   JSONObject jsonObject = new JSONObject(serverData.data);
                                   String biz_no = jsonObject.getString("biz_no");
                                    mFunctionManager.saveStr("biz_no",biz_no);
                                   String url = jsonObject.getString("url");
                                   String appId = jsonObject.getString("appId");
                                   doVerify(url);
                                   Log.e(TAG,"biz_no=="+biz_no);
                               } catch (JSONException e) {
                                   e.printStackTrace();
                               }

                           }else {

                               mFunctionManager.showShort(serverData.message);
                           }
                       }
                   });
                }
            });
        }else {
            mFunctionManager.showShort("请输入有效身份证号");
        }

    }


    /**
     * 启动支付宝进行认证
     * @param url 开放平台返回的URL
     */
    private void doVerify(String url) {
        if (hasApplication()) {
            Intent action = new Intent(Intent.ACTION_VIEW);
            StringBuilder builder = new StringBuilder();
            // 这里使用固定appid 20000067
            builder.append("alipays://platformapi/startapp?appId=20000067&url=");


                builder.append(URLEncoder.encode(url));


            Log.e(TAG,"StringBuilder=="+builder);
            action.setData(Uri.parse(builder.toString()));
            startActivity(action);
            finish();
            loadingProgress.stopLoading();
        } else {
            // 处理没有安装支付宝的情况
            new AlertDialog.Builder(this)
                    .setMessage("是否下载并安装支付宝完成认证?")
                    .setPositiveButton("好的", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent action = new Intent(Intent.ACTION_VIEW);
                            action.setData(Uri.parse("https://m.alipay.com"));
                            startActivity(action);
                        }
                    }).setNegativeButton("算了", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }

    /**
     * 判断是否安装了支付宝
     * @return true 为已经安装
     */
    private boolean hasApplication() {
        PackageManager manager = getPackageManager();
        Intent action = new Intent(Intent.ACTION_VIEW);
        action.setData(Uri.parse("alipays://"));
        List list = manager.queryIntentActivities(action, PackageManager.GET_RESOLVED_FILTER);
        return list != null && list.size() > 0;
    }

    private void setEditTextState(EditText editText,boolean flag){
        editText.setCursorVisible(flag);
        editText.setFocusable(flag);
        editText.setFocusableInTouchMode(flag);
    }




    class TextChange implements TextWatcher {
        @Override
        public void afterTextChanged(Editable arg0) {
        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
        @Override
        public void onTextChanged(CharSequence cs, int start, int before, int count) {
            boolean Sign1 = mRenzhengName.getText().length() > 0;
            boolean Sign2 = mRenzhengCardid.getText().length() > 0;
            if (Sign1&Sign2) {
                mRenzhengBtn.setEnabled(true);
                mRenzhengBtn.setBackgroundResource(R.drawable.renzheng_background);
            }
            //在layout文件中，对Button的text属性应预先设置默认值，否则刚打开程序的时候Button是无显示的
            else {
                mRenzhengBtn.setEnabled(false);
                mRenzhengBtn.setBackgroundResource(R.drawable.shoukuan_save_background);
            }
        }
    }

}
