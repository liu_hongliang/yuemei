/**
 * 
 */
package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.activity.InstructionWebActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 填码得劵
 * 
 * @author Robin
 * 
 */
public class TianMaDeJuanActivity extends BaseActivity {

	private final String TAG = "TianMaDeJuanActivity";

	@BindView(id = R.id.jifen_linearlayout3, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.login_close, click = true)
	private RelativeLayout back;// 返回

	private WebView docDetWeb;

	private TianMaDeJuanActivity mContex;

	public JSONObject obj_http;

	private String uid;
	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_tianmadejuan);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = TianMaDeJuanActivity.this;
		uid = Utils.getUid();


		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();

		LodUrl1(FinalConstant.TIANMADEJUAN);
	}

	public void onResume() {
		super.onResume();
		uid = Utils.getUid();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}



	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 * 
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("1")) {// 颜值币明细页
				Intent it0 = new Intent();
				it0.putExtra("type", "1");
				it0.setClass(mContex, InstructionWebActivity.class);
				startActivity(it0);
			}

			if (obj.getString("type").equals("2")) {// 颜值币讲解
				Intent it0 = new Intent();
				it0.putExtra("type", "2");
				it0.setClass(mContex, InstructionWebActivity.class);
				startActivity(it0);
			}

			if (obj.getString("type").equals("561")) {// 我知道啦
				finish();
			}

		} catch (JSONException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl1(String urlstr) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());

	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.login_close:
			finish();
			break;
		}
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

	@Override
	public void finish() {
		// 关闭窗体动画显示
		this.overridePendingTransition(R.anim.activity_close, 0);
		super.finish();
	}
}