package com.module.my.controller.activity;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.bean.ShareWechat;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.model.bean.ShareVipPictorial;
import com.module.my.model.bean.VipSharejsonData;
import com.quicklyask.activity.R;
import com.quicklyask.util.ExternalStorage;
import com.quicklyask.util.Utils;
import com.umeng.socialize.bean.SHARE_MEDIA;

import butterknife.BindView;

/**
 * 会员拉新分享页面
 * Created by 裴成浩 on 2018/9/11.
 */
public class PlusVipShareActivity extends YMBaseActivity {

    @BindView(R.id.activity_plus_vip_share)
    ScrollView shareImg;
    @BindView(R.id.activity_plus_vip_container)
    RelativeLayout shareContainer;
    @BindView(R.id.plus_vip_img_top)
    RelativeLayout plusVipImgTop;
    @BindView(R.id.plus_vip_img_back)
    ImageView plusVipImgBack;
    @BindView(R.id.plus_vip_img_share)
    ImageView plusVipImgShare;
    @BindView(R.id.plus_vip_img)
    ImageView plusVipImg;
    @BindView(R.id.plus_vip_img_code)
    ImageView plusVipImgCode;
    private ShareWechat wechat;
    private BaseShareView baseShareView;
    private String TAG = "PlusVipShareActivity";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plus_vip_share;
    }

    @Override
    protected void initView() {
        VipSharejsonData mData = getIntent().getParcelableExtra("data");
        wechat = mData.getWechat();
        ShareVipPictorial pictorial = mData.getPictorial();

        String sun_img = pictorial.getSun_img();        //小程序码
        String img = pictorial.getImg().get(0).getImg();
        Log.e(TAG, "img == " + img);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) plusVipImgTop.getLayoutParams();
        layoutParams.topMargin = statusbarHeight + Utils.dip2px(20);
        plusVipImgTop.setLayoutParams(layoutParams);

        //分享的图片
        Glide.with(mContext).load(img).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                int intrinsicWidth = resource.getIntrinsicWidth();
                int intrinsicHeight = resource.getIntrinsicHeight();
                Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
                Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);

                int imgHeight = (windowsWight * intrinsicHeight) / intrinsicWidth;

                Log.e(TAG, "imgWidth == " + windowsWight);
                Log.e(TAG, "imgHeight == " + imgHeight);

                ViewGroup.LayoutParams params = shareContainer.getLayoutParams();
                params.width = windowsWight;
                params.height = imgHeight;
                shareContainer.setLayoutParams(params);

                ViewGroup.LayoutParams params2 = plusVipImg.getLayoutParams();
                params2.width = windowsWight;
                params2.height = imgHeight;
                plusVipImg.setLayoutParams(params2);

                plusVipImg.setImageDrawable(resource);
            }
        });


        //小程序码
        Glide.with(mContext).load(sun_img).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(plusVipImgCode);


    }

    @Override
    protected void initData() {
        //返回按钮
        plusVipImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //分享
        plusVipImgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setShare();
            }
        });

    }

    /**
     * 分享设置
     */
    private void setShare() {

        Bitmap bitmapFromView1 = ExternalStorage.getBitmapByView(shareImg);
        baseShareView = new BaseShareView(mContext);
        baseShareView.setShareContent("").setShareSms(false).ShareAction(wechat);

        baseShareView.getShareBoardlistener().setUMIimage(bitmapFromView1).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    mFunctionManager.showShort("分享成功啦");
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }
}
