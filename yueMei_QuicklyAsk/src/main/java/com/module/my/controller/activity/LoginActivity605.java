package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.other.AutoLoginControler;
import com.module.my.model.api.InitCode1Api;
import com.module.my.model.api.LoginApi;
import com.module.my.model.api.LoginOtherHttpApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.bean.UserData;
import com.module.my.view.view.AutoLoginPop;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.service.NotificationService;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.PropertyAnimation;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.quicklyask.view.YueMeiDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.kjframe.utils.SystemTool;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jpush.android.api.JPushInterface;


/**
 * 登录
 * <p>
 * Created by dwb on 16/7/5.
 */
public class LoginActivity605 extends YMBaseActivity {

    public static final int REQUEST_CODE = 1006;
    private final String TAG = "LoginActivity605";

    private UserData userData;

    private final int BACK3 = 3;//注册返回回调
    private final int BACK2 = 2;//未绑定手机号回调

    @BindView(R.id.login_top)
    CommonTopBar mTop;
    //账号密码登录
    @BindView(R.id.login_username)
    EditText userNameEt;// 输入名字
    @BindView(R.id.login_password)
    EditText userPasswordEt;// 输入密码
    @BindView(R.id.login_bt)
    Button loginBt;// 登录
    @BindView(R.id.login_forget_mima_tv)
    TextView forgetMimaTv;// 忘记密码

    //第三方平台登录
    @BindView(R.id.login_weixin_bt_rly)
    RelativeLayout weixinBt;
    @BindView(R.id.login_qq_bt_rly)
    RelativeLayout qqBt;
    @BindView(R.id.login_weibo_bt_rly)
    RelativeLayout weiboBt;

    @BindView(R.id.password_if_ming_iv)
    ImageView passIfShowIv;
    boolean passIsShow = false;

    @BindView(R.id.login_caidan_type1)
    RelativeLayout loginType1;//无密码登录
    @BindView(R.id.login_caidan_type2)
    RelativeLayout loginType2;//账号密码登录
    @BindView(R.id.login_type_1)
    LinearLayout loginTypeLy1;//无密码登录界面
    @BindView(R.id.login_type_2)
    LinearLayout loginTypeLy2;//账号密码登录界面

    //无密码快捷登录
    @BindView(R.id.no_pass_login_phone_et)
    EditText phoneNumberEt; //无密码登录手机输入框
    @BindView(R.id.no_pass_login_code_et)
    EditText codeEt;//无密码登录验证码输入框
    @BindView(R.id.no_pass_login_bt)
    Button np_loginBt;//无密码登录按钮

    @BindView(R.id.no_pass_yanzheng_code_rly)
    RelativeLayout sendEMSRly;//无密码登录发送验证码
    @BindView(R.id.yanzheng_code_tv)
    TextView emsTv;

    @BindView(R.id.nocde_message_tv)
    TextView noCodeTv;// 没收到验证码

    PopupWindows yuyinCodePop;
    @BindView(R.id.order_time_all_ly)
    LinearLayout allcontent;
    @BindView(R.id.yuemei_auto_login)
    TextView yuemeiAutoLoginTxt;

    //第三方登录
    @BindView(R.id.login_disanfang_onandoff)
    LinearLayout onAndoffLy;
    @BindView(R.id.login_qq_weiobo_ly)
    LinearLayout qqweiboweixinLy;
    @BindView(R.id.login_line_qqweibo_jiantou_iv)
    ImageView qqweiboJiantouIv;

    @BindView(R.id.yuemei_yinsi_ly)
    LinearLayout yuemeiYinsiLy;
    @BindView(R.id.yueme_agremment)
    TextView yuemeiAgremment;

    // 第三方登录
    private UMShareAPI mShareAPI = null;
    private String loginSex;
    private String loginName;
    private String oauthId;
    private String openid = "";
    private String unionid = "";
    private String iconurl = "";
    private String fromSite;

    private String np_phone;
    private String np_codeStr;

    private String loginType = "1";

    private String loginUser = "";

    private GetPhoneCodePopWindow phoneCodePop;
    private String localDelivery;
    private String mCity;
    private String mProvince;

    private String newsCode;            //是否是回调
    private String mId;
    private String mYuemeiinfo;
    private String skuJump = "0";       //SKU页面是否是回调
    private LoadingProgress mDialog;
    private String mRegistrationID;
    private String phone;
    private String mReferrer;
    private String mReferrerId;


    @Override
    protected int getLayoutId() {
        return R.layout.acty_login_605;
    }

    @Override
    protected void initView() {
        mDialog = new LoadingProgress(mContext);
        newsCode = getIntent().getStringExtra("news");
        skuJump = getIntent().getStringExtra("skujump");
        mRegistrationID = JPushInterface.getRegistrationID(mContext);

        initAgremment();
        //手机注册
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                Intent it2 = new Intent();
                it2.setClass(mContext, RegisterActivity548.class);
                startActivityForResult(it2, BACK3);
            }
        });
        Cfg.saveStr(this,"is_jump_login","0");

        setMultiOnClickListener(loginBt, forgetMimaTv, weixinBt, qqBt, weiboBt, passIfShowIv, loginType1, loginType2, np_loginBt, sendEMSRly, noCodeTv, onAndoffLy, yuemeiYinsiLy,yuemeiAutoLoginTxt);
    }

    private void initAgremment() {
        SpannableString spannableString = new SpannableString("注册即表示您同意并愿意遵守《悦美用户使用协议》《隐私政策》");
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#666666")), 0, 12, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#4d7bbc")), 13,spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new Clickable(clickListener), 13, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new Clickable(clickListener2), 23, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        yuemeiAgremment.setText(spannableString);
        yuemeiAgremment.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    protected void initData() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        String loadStr = Cfg.loadStr(this, FinalConstant.EXPOSURE_LOGIN, "");
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(loadStr)) {
            ExposureLoginData exposureLoginData = new Gson().fromJson(loadStr, ExposureLoginData.class);
            mReferrer = exposureLoginData.getReferrer();
            mReferrerId = exposureLoginData.getReferrer_id();
            hashMap.put("referrer", mReferrer);
            hashMap.put("referrer_id", mReferrerId);
        }else{
            hashMap.put("referrer", "0");
            hashMap.put("referrer_id", "0");
        }

        Log.e(TAG, "hashMap == " + hashMap.toString());
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.LOGIN_BAOGUANG), hashMap, new ActivityTypeData("155"));
        localDelivery = mFunctionManager.loadStr(FinalConstant.SIGN_FLAG, "0");
        mCity = Cfg.loadStr(LoginActivity605.this, "city_dingwei", "");
        mProvince = Cfg.loadStr(LoginActivity605.this, FinalConstant.DWPROVINCE, "");

        /** init auth api**/
        mShareAPI = UMShareAPI.get(LoginActivity605.this);

        setCutLoginType("1");
    }

    @Override
    public void onBackPressed() {
        setResult(LoginActivity605.REQUEST_CODE, new Intent());
        super.onBackPressed();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    void setCutLoginType(String tab) {
        if (tab.equals("1")) {
            loginType1.setBackground(null);
            loginType2.setBackgroundResource(R.drawable.bian_ecec_dbdb);
            loginTypeLy1.setVisibility(View.VISIBLE);
            loginTypeLy2.setVisibility(View.GONE);
        } else {
            loginType1.setBackgroundResource(R.drawable.bian_ecec_dbdb);
            loginType2.setBackground(null);
            loginTypeLy1.setVisibility(View.GONE);
            loginTypeLy2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.no_pass_yanzheng_code_rly://发送验证码
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (Utils.isNetworkAvailable(mContext)){
                    phone = phoneNumberEt.getText().toString().trim();
                    if (phone.length() > 0) {
                        if (ifPhoneNumber()) {
                            sendEMS();
                            noCodeTv.setVisibility(View.VISIBLE);
                            noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？" + "</u>"));
                        } else {
                            ViewInject.toast("请输入正确的手机号");
                        }
                    } else {
                        ViewInject.toast("请输入手机号");
                    }
                }else {
                    ViewInject.toast("请检查当前手机是否连接网络");
                }

                break;
            case R.id.login_caidan_type1:
                setCutLoginType("1");
                break;
            case R.id.login_caidan_type2:
                setCutLoginType("2");
                break;
            case R.id.login_disanfang_onandoff:

                PropertyAnimation propertyAnimation = new PropertyAnimation(this);  //自己定义的属性动画类
                if (qqweiboweixinLy.getVisibility() == View.GONE) {
                    propertyAnimation.animateOpen(qqweiboweixinLy);
                    propertyAnimation.animationIvOpen(qqweiboJiantouIv);
                } else {
                    propertyAnimation.animateClose(qqweiboweixinLy);
                    propertyAnimation.animationIvClose(qqweiboJiantouIv);
                }

                break;
            case R.id.no_pass_login_bt:// 登录
                if (Utils.isFastDoubleClick())return;

                if (!Utils.isNetworkAvailable(mContext)){
                    mFunctionManager.showShort("网络不可用");
                }
                loginType = "1";

                np_codeStr = codeEt.getText().toString();
                np_phone = phoneNumberEt.getText().toString().trim();
                if (np_phone.length() > 0) {
                    if (np_codeStr.length() > 0) {
                        mDialog.startLoading();
                        initCode1();
                    } else {
                        ViewInject.toast("请输入验证码！");
                    }
                } else {
                    ViewInject.toast("请输入手机号！");
                }
                break;
            case R.id.nocde_message_tv:// 没收到验证码
                yuyinCodePop = new PopupWindows(mContext, allcontent);
                yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

                break;
            case R.id.login_bt:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                loginType = "1";
                if (!Utils.isNetworkAvailable(mContext)){
                    mFunctionManager.showShort("网络不可用");
                }
                if (!judgeEmailAndPhone()) {
                    ViewInject.toast("请输入正确的手机号或邮箱");
                } else {
                    String nameStr = userNameEt.getText().toString().trim();
                    loginUser = nameStr;
                    String passStr = userPasswordEt.getText().toString();
                    // startLoading();
                    if (!TextUtils.isEmpty(passStr)) {
                        loginBt.setBackgroundResource(R.drawable.shape_f6cbd1);
                        loginBt.setText("登录中...");
                        loginBt.setClickable(false);
                        String netStatus = Cfg.loadStr(MyApplication.getContext(), FinalConstant.NO_NETWORK, "0");
                        if ("1".equals(netStatus)){
                            loginBt.setBackgroundResource(R.drawable.login_bt);
                            loginBt.setText("登录");
                            loginBt.setClickable(true);
                        }
                        Map<String, Object> maps = new HashMap<>();
                        maps.put("user", "login");
                        maps.put("username", nameStr);
                        maps.put("password", passStr);
                        if (!TextUtils.isEmpty(mReferrer)){
                            maps.put("referrer",mReferrer);
                        }
                        if (!TextUtils.isEmpty(mReferrerId)){
                            maps.put("referrer_id",mReferrerId);
                        }
                        new LoginApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(localDelivery)) {
                                    if (!NotificationService.isRunning(LoginActivity605.this)) {         //开启本地推送的线程
                                        Intent startIntent = new Intent(LoginActivity605.this, NotificationService.class);
                                        startService(startIntent);
                                    }
                                }
                                if ("1".equals(serverData.code)) {
                                    UserData userData = JSONUtil.TransformLogin1(serverData.data);
                                    mId = userData.get_id();
                                    mFunctionManager.showShort(serverData.message);
                                    Cfg.saveStr(mContext,FinalConstant.HOME_PERSON_UID, mId);
                                    Utils.getUserInfoLogin(mContext,mId,"1");


                                } else {
                                    mFunctionManager.showShort(serverData.message);
                                    loginBt.setBackgroundResource(R.drawable.login_bt);
                                    loginBt.setText("登录");
                                    loginBt.setClickable(true);
                                }
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id",serverData.code);
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.LOGINCLICK,serverData.message,phone),hashMap);
                            }
                        });
                    } else {
                        ViewInject.toast("请输入密码");
                    }

                }
                break;
            case R.id.login_forget_mima_tv:
                Intent it1 = new Intent();
                it1.putExtra("type", "1");
                it1.setClass(mContext, RestPassWordActivity.class);
                startActivity(it1);
                break;
            case R.id.login_weixin_bt_rly:

                if (mShareAPI.isInstall(this, SHARE_MEDIA.WEIXIN)) {
                    loginType = "2";
                    mShareAPI.doOauthVerify(LoginActivity605.this, SHARE_MEDIA.WEIXIN, umAuthListener);
                } else {
                    mFunctionManager.showShort("请安装微信客户端");
                }

                break;
            case R.id.login_qq_bt_rly:

                if (mShareAPI.isInstall(this, SHARE_MEDIA.QQ)) {
                    loginType = "2";
                    mShareAPI.doOauthVerify(LoginActivity605.this, SHARE_MEDIA.QQ, umAuthListener);
                } else {
                    mFunctionManager.showShort("请安装QQ客户端");
                }

                break;
            case R.id.login_weibo_bt_rly:

                if (mShareAPI.isInstall(this, SHARE_MEDIA.SINA)) {
                    loginType = "2";
                    mShareAPI.doOauthVerify(LoginActivity605.this, SHARE_MEDIA.SINA, umAuthListener);
                } else {
                    mFunctionManager.showShort("请安装新浪微博客户端");
                }

                break;
            case R.id.password_if_ming_iv:// 密码是否铭文
                if (passIsShow) {
                    passIfShowIv.setBackgroundResource(R.drawable.miwen_);
                    userPasswordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    Editable etext = userPasswordEt.getText();
                    Selection.setSelection(etext, etext.length());
                    passIsShow = false;
                } else {
                    passIfShowIv.setBackgroundResource(R.drawable.mingwen_);
                    userPasswordEt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    Editable etext = userPasswordEt.getText();
                    Selection.setSelection(etext, etext.length());
                    passIsShow = true;
                }
                break;
            case R.id.yuemei_auto_login:


                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ONE_CLICK_LOGIN),new ActivityTypeData("155"));

                    boolean verifyEnable = JVerificationInterface.checkVerifyEnable(this);
                    if (!verifyEnable){
                        showJumpPop();//不符合一键登录网络状态
                    }else {
                        startActivity(new Intent(mContext, AutoLoginControler.class)); //一键登录
                        finish();
                    }
                break;
        }
    }







    private final int LOGIN_WAIT = 1;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case LOGIN_WAIT:
                    Toast.makeText(mContext, "登录中", Toast.LENGTH_LONG).show();
                    mHandler.sendEmptyMessageDelayed(LOGIN_WAIT, 1000);
                    break;
            }
        }
    };



    /**
     * 未开启流量弹窗
     */
    private void showJumpPop(){
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = 0.5f;
        getWindow().setAttributes(lp);
        AutoLoginPop chatJumpPopwindow = new AutoLoginPop(mContext);
        chatJumpPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.alpha = 1f;
                getWindow().setAttributes(lp);
            }
        });
        chatJumpPopwindow.showAtLocation(allcontent, Gravity.CENTER, 0, 0);
    }
    /**
     * 无密码登录验证验证码
     */
    void initCode1() {
        np_codeStr = codeEt.getText().toString();
        Map<String, Object> params = new HashMap<>();
        params.put("phone", np_phone);
        params.put("code", np_codeStr);
        if (!TextUtils.isEmpty(mReferrer)){
            params.put("referrer",mReferrer);
        }
        if (!TextUtils.isEmpty(mReferrerId)){
            params.put("referrer_id",mReferrerId);
        }
        new InitCode1Api().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {

                    if ("1".equals(localDelivery)) {
                        if (!NotificationService.isRunning(LoginActivity605.this)) {         //开启本地推送的线程
                            Log.e("TAG", "NotificationService.isRunning(this) == " + NotificationService.isRunning(LoginActivity605.this));
                            Intent startIntent = new Intent(LoginActivity605.this, NotificationService.class);
                            startService(startIntent);
                        }
                    }

                    mDialog.stopLoading();
                    userData = JSONUtil.TransformLogin(s.data);
                    String id = userData.get_id();

                    StatisticalManage.getInstance().growingIO("logon");
                    Cfg.saveStr(mContext,FinalConstant.HOME_PERSON_UID, id);
                    Utils.getUserInfoLogin(mContext,id,"1");
                    SystemTool.hideKeyBoard(LoginActivity605.this);
                } else {
                    mDialog.stopLoading();
                    mFunctionManager.showShort(s.message);
                }
            }
        });
    }


    @SuppressLint("NewApi")
    void loginOtherHttp() {
        Map<String, Object> params = new HashMap<>();
        params.put("from_site", fromSite);
        params.put("oauth_id", oauthId);
        params.put("openid", openid);
        params.put("unionid", unionid);
        params.put("name", loginName);
        params.put("sex", loginSex);
        params.put("avatar", iconurl);
        if (!TextUtils.isEmpty(mReferrer)){
            params.put("referrer",mReferrer);
        }
        if (!TextUtils.isEmpty(mReferrerId)){
            params.put("referrer_id",mReferrerId);
        }

        Log.e(TAG, "map == " + hashMap.toString());
        Log.e(TAG, "iconurl == " + iconurl);
        Log.e(TAG, "fromSite == " + fromSite);
        Log.e(TAG, "oauthId == " + oauthId);
        Log.e(TAG, "openid == " + openid);
        Log.e(TAG, "unionid == " + unionid);
        Log.e(TAG, "loginName == " + loginName);
        Log.e(TAG, "loginSex == " + loginSex);


        new LoginOtherHttpApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    userData = JSONUtil.TransformLogin(serverData.data);
                    String id = userData.get_id();
                    Log.e(TAG, "----->" + id);
                    StatisticalManage.getInstance().growingIO("logon");
                    Cfg.saveStr(mContext,FinalConstant.HOME_PERSON_UID, id);
                    Utils.getUserInfoLogin(mContext,id,"1");
                } else {
                    ViewInject.toast(serverData.message);
                }
            }
        });

    }


    private boolean judgeEmailAndPhone() {
        String nameStr = userNameEt.getText().toString().trim();
        if (nameStr.contains("@")) {
            return Utils.emailFormat(nameStr);
        } else {
            return Utils.isMobile(nameStr);
        }
    }


    private boolean ifPhoneNumber() {
        String textPhn = phoneNumberEt.getText().toString().trim();
        return Utils.isMobile(textPhn);
    }

    void sendEMS() {
        np_phone = phoneNumberEt.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        Log.e(TAG, "np_phone == " + np_phone);
        maps.put("phone", np_phone);
        maps.put("flag", "1");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {

                    ViewInject.toast(serverData.message);

                    sendEMSRly.setClickable(false);
                    codeEt.requestFocus();

                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {
                            sendEMSRly.setBackgroundResource(R.drawable.biankuang_hui);
                            emsTv.setTextColor(getResources().getColor(R.color.button_zi));
                            emsTv.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            sendEMSRly.setBackgroundResource(R.drawable.shape_bian_ff5c77);
                            emsTv.setTextColor(getResources().getColor(R.color.button_bian_hong1));
                            sendEMSRly.setClickable(true);
                            emsTv.setText("重发验证码");
                        }
                    }.start();
                } else {
                    ViewInject.toast(serverData.message);
                }
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id",serverData.code);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.GETCODE,serverData.message,phone),hashMap);
            }

        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        mShareAPI.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BACK2:

                break;
            case BACK3:
                if (data != null) {
                    onBackPressed();
                }
                break;
        }

    }


    EditText codeEt1;
    ImageView codeIv;

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(final Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_yuyincode, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            // showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button cancelBt = view.findViewById(R.id.cancel_bt);
            Button tureBt = view.findViewById(R.id.zixun_bt);
            codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
            codeIv = view.findViewById(R.id.yuyin_code_iv);

            RelativeLayout rshCodeRly = view.findViewById(R.id.no_pass_yanzheng_code_rly);
            Glide.with(LoginActivity605.this).load(FinalConstant.TUXINGCODE)
                    .skipMemoryCache(true) // 不使用内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                    .into(codeIv);
            rshCodeRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Glide.with(LoginActivity605.this).load(FinalConstant.TUXINGCODE)
                            .skipMemoryCache(true) // 不使用内存缓存
                            .diskCacheStrategy(DiskCacheStrategy.NONE) // 不使用磁盘缓存
                            .into(codeIv);
                }
            });

            tureBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String codes = codeEt1.getText().toString();
                    if (codes.length() > 1) {
                        yanzhengCode(codes);
                    } else {
                        Toast.makeText(mContext,"请输入图中数字",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            cancelBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    void yanzhengCode(String codes) {
        String phones = phoneNumberEt.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phones);
        maps.put("code", codes);
        maps.put("flag", "codelogin");
        new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    yuyinCodePop.dismiss();
                    Toast.makeText(mContext,s.message,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext,s.message,Toast.LENGTH_SHORT).show();
                }
            }

        });
    }


    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    HashMap<String, String> hashMap = new HashMap<>();
    /**
     * auth callback interface
     **/
    private UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
            Log.e(TAG,"onStart==");
        }

        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            Log.e(TAG,"onComplete=="+data.toString());
            if (platform.equals(SHARE_MEDIA.WEIXIN)) {


                mShareAPI.getPlatformInfo(LoginActivity605.this, platform, new UMAuthListener() {
                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {

                        loginSex = map.get("gender") + "";
                        loginName = map.get("screen_name") + "";
                        fromSite = "weixin";
                        oauthId = map.get("unionid") + "";
                        openid = map.get("openid") + "";
                        unionid = map.get("unionid") + "";
                        iconurl = map.get("profile_image_url") + "";

                        hashMap.putAll(map);
                        loginOtherHttp();
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media, int i) {

                    }
                });

            } else if (platform.equals(SHARE_MEDIA.QQ)) {

                oauthId = data.get("openid") + "";
                openid = data.get("openid") + "";


                mShareAPI.getPlatformInfo(LoginActivity605.this, platform, new UMAuthListener() {
                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {

                        loginSex = map.get("gender") + "";
                        iconurl = map.get("iconurl") + "";
                        loginName = map.get("name") + "";

                        if (loginSex.equals("男")) {
                            loginSex = "1";
                        } else {
                            loginSex = "2";
                        }
                        loginName = map.get("screen_name") + "";
                        fromSite = "qq";
                        loginOtherHttp();
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media, int i) {

                    }
                });


            } else if (platform.equals(SHARE_MEDIA.SINA)) {

                oauthId = data.get("uid") + "";

                mShareAPI.getPlatformInfo(LoginActivity605.this, platform, new UMAuthListener() {
                    @Override
                    public void onStart(SHARE_MEDIA share_media) {

                    }

                    @Override
                    public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {

                        String sex = map.get("gender") + "";
                        loginName = map.get("name") + "";
                        iconurl = map.get("iconurl") + "";

                        if (sex.equals("m")) {
                            loginSex = "1";
                        } else {
                            loginSex = "2";
                        }
                        loginName = map.get("screen_name") + "";
                        fromSite = "sina";

                        loginOtherHttp();
                    }

                    @Override
                    public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

                    }

                    @Override
                    public void onCancel(SHARE_MEDIA share_media, int i) {

                    }
                });

            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText(getApplicationContext(), "Authorize fail", Toast.LENGTH_SHORT).show();

            Log.e("AAAAA", "onError==" + t);

        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Toast.makeText(getApplicationContext(), "Authorize cancel", Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent it = new Intent();
            it.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,1);
            it.setClass(mContext, UserAgreementWebActivity.class);
            startActivity(it);
        }
    };

    private View.OnClickListener clickListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent it = new Intent();
            it.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,0);
            it.setClass(mContext, UserAgreementWebActivity.class);
            startActivity(it);
        }
    };

    private class Clickable extends ClickableSpan {
        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            mListener = l;
        }
        /**
         * 重写父类点击事件
         */
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        /**
         * 重写父类updateDrawState方法  我们可以给TextView设置字体颜色,背景颜色等等...
         */
        @Override
        public void updateDrawState(TextPaint ds) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Cfg.saveStr(MyApplication.getContext(), NetWork.IS_SHOW_LOGIN,"0");
    }
}
