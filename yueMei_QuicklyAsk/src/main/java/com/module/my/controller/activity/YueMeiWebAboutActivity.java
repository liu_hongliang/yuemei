/**
 * 
 */
package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 关于悦美
 * 
 * @author Robin
 * 
 */
public class YueMeiWebAboutActivity extends BaseActivity {

	private final String TAG = "YueMeiWebAboutActivity";

	@BindView(id = R.id.about_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.about_web_top)
	private CommonTopBar mTop;// 返回

	private WebView bbsDetWeb;

	private YueMeiWebAboutActivity mContex;
	public JSONObject obj_http;

	private BaseWebViewClientMessage baseWebViewClientMessage;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_acty_about_yuemei);
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = YueMeiWebAboutActivity.this;

		mTop.setCenterText("关于悦美");

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
			@Override
			public void otherJump(String urlStr) {
				showWebDetail(urlStr);
			}
		});
		initWebview();

		LodUrl(FinalConstant.ABOUTURL);
		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						YueMeiWebAboutActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	public void initWebview() {
		bbsDetWeb = new WebView(mContex);
		bbsDetWeb.getSettings().setUseWideViewPort(true);
		bbsDetWeb.setWebViewClient(baseWebViewClientMessage);
		bbsDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(bbsDetWeb);
	}


	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

			if (obj.getString("type").equals("541")) {// 悦美简介
				try {
					String link = obj.getString("link");
					Intent it = new Intent();
					it.setClass(mContex, AboutOursWebActivity.class);
					it.putExtra("url", link);
					startActivity(it);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 加载web
	 */
	public void LodUrl(String url) {
		baseWebViewClientMessage.startLoading();
		WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
		bbsDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
