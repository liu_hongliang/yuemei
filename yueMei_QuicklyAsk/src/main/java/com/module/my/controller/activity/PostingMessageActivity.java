package com.module.my.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.my.model.api.GetPostingMessageApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.util.WriteNoteManager;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

public class PostingMessageActivity extends BaseActivity {

    private final String TAG = "PostingMessageActivity";
    private final Activity mActivity = PostingMessageActivity.this;

    @BindView(id = R.id.activity_posting_message)
    private LinearLayout contentLy;

    @BindView(id = R.id.message_colse)
    private RelativeLayout msgColse;

    @BindView(id = R.id.no_message_name_et)
    private EditText mName;
    @BindView(id = R.id.no_message_phone_et)
    private EditText mPhone;

    @BindView(id = R.id.iv_msg_choose)
    private ImageView mChoose;

    @BindView(id = R.id.no_message_submit_bt)
    private Button mSubmit;

    @BindView(id = R.id.tv_msg_centent)
    private TextView mCentent;

    //登录id
    private String uid;
    //选择框
    private boolean selected = true;
    private String cateid;
    private String userid;
    private String tiaozhuan;
    private String hosid;
    private String hosname;
    private String docname;
    private String fee;
    private String taoid;
    private String server_id;
    private String sharetime;
    private String type;
    private String noteid;
    private String notetitle;
    private String riaozhuan;
    private PageJumpManager pageJumpManager;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_posting_message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        pageJumpManager = new PageJumpManager(mActivity);

        uid = Utils.getUid();
        cateid = getIntent().getStringExtra("cateid");
        userid = getIntent().getStringExtra("userid");

        hosid = getIntent().getStringExtra("hosid");
        hosname = getIntent().getStringExtra("hosname");
        docname = getIntent().getStringExtra("docname");
        fee = getIntent().getStringExtra("fee");
        taoid = getIntent().getStringExtra("taoid");

        server_id = getIntent().getStringExtra("server_id");
        sharetime = getIntent().getStringExtra("sharetime");
        type = getIntent().getStringExtra("type");
        noteid = getIntent().getStringExtra("noteid");
        notetitle = getIntent().getStringExtra("notetitle");
        riaozhuan = getIntent().getStringExtra("riaozhuan");

        tiaozhuan = getIntent().getStringExtra("tiaozhuan");

        //点击之后的背景颜色
        mCentent.setHighlightColor(getResources().getColor(android.R.color.white));

        SpannableString spanableInfo = new SpannableString("发帖须知：根据2017年6月1日正式实施的《中华人民共和国网络安全法》最终版第二十四条，用户在互联网上发布信息，需提供真实信息");
        spanableInfo.setSpan(new Clickable(clickListener), 21, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mCentent.setText(spanableInfo);
        mCentent.setMovementMethod(LinkMovementMethod.getInstance());


        //返回按钮
        msgColse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = mName.getText().toString().length();
                int length1 = mPhone.getText().toString().length();
                if (length > 0 && selected && length1 == 11) {
                    mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_ff5c77));
                    mSubmit.setEnabled(true);
                } else {
                    mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_f6cbd1));
                    mSubmit.setEnabled(false);
                }
            }
        });


        //手机号输入监听
        mPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = mName.getText().toString().length();
                int length1 = mPhone.getText().toString().length();
                if (length > 0 && selected && length1 == 11) {
                    mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_ff5c77));
                    mSubmit.setEnabled(true);
                } else {
                    mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_f6cbd1));
                    mSubmit.setEnabled(false);
                }
            }
        });


        //选择框点击事件
        mChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selected) {
//                    mChoose.setImageResource(R.drawable.posting_message_agreed_no);
                    mChoose.setBackgroundDrawable(getResources().getDrawable(R.drawable.posting_message_agreed_no));
                    mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_f6cbd1));
                    mSubmit.setEnabled(false);
                    selected = false;
                } else {
                    String full_name = mName.getText().toString();
                    String mobile = mPhone.getText().toString();
//                    mChoose.setImageResource(R.drawable.posting_message_agreed);
                    mChoose.setBackgroundDrawable(getResources().getDrawable(R.drawable.posting_message_agreed));
                    selected = true;
                    if (mobile.length() == 11 && full_name.length() > 0) {
                        mSubmit.setBackground(getResources().getDrawable(R.drawable.shape_ff5c77));
                        mSubmit.setEnabled(true);
                    }
                }
            }
        });

        //提交按钮点击事件
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String full_name = mName.getText().toString();
                String mobile = mPhone.getText().toString();
                if (mobile.length() == 11) {
                    getPostingMessage(full_name, mobile);
                }


            }
        });
    }

    /**
     * 网络安全法的点击事件
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = "http://www.miit.gov.cn/n1146295/n1146557/n1146614/c5345009/content.html";
            WebUrlTypeUtil.getInstance(PostingMessageActivity.this).urlToApp(url, "33", "0");
        }
    };


    /**
     * 获取发帖人信息页
     *
     * @param full_name
     * @param mobile
     */
    private void getPostingMessage(String full_name, String mobile) {

        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("full_name", full_name);
        keyValues.put("mobile", mobile);
        keyValues.put("uid", uid);
        new GetPostingMessageApi().getCallBack(mActivity, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    Toast.makeText(PostingMessageActivity.this, "信息提交成功", Toast.LENGTH_SHORT).show();

                    Intent it = new Intent();
                    if("1".equals(tiaozhuan)){
                        mActivity.startActivity(new Intent(mActivity, TypeProblemActivity.class));
                    }else if("2".equals(tiaozhuan)){

                        WriteNoteManager.getInstance(mActivity).ifAlert( null);

                    }else if("3".equals(tiaozhuan)){

                        it.putExtra("cateid", cateid + ",1090");
                        it.setClass(mActivity, WriteSuibianLiaoActivity647.class);
                        startActivity(it);
                    }else if("4".equals(tiaozhuan)){

                        it.putExtra("cateid", cateid);
                        it.setClass(mActivity, SelectSendPostsActivity.class);
                        startActivity(it);

                    }else if("5".equals(tiaozhuan)){
                        it.putExtra("cateid", cateid);
                        it.setClass(mActivity, WriteSuibianLiaoActivity647.class);
                        startActivity(it);
                    }else if("6".equals(tiaozhuan)){
                        WriteNoteManager.getInstance(mActivity).ifAlert( null);
                    }else if("7".equals(tiaozhuan)){
                        WriteNoteManager.getInstance(mActivity).ifAlert( null);
                    }else if("8".equals(tiaozhuan)){

                        HashMap<String, String> mMap = new HashMap<>();
                        mMap.put("cateid",cateid);
                        mMap.put("userid",userid);
                        mMap.put("hosid",hosid);
                        mMap.put("hosname",hosname);
                        mMap.put("docname",docname);
                        mMap.put("fee",fee);
                        mMap.put("taoid",taoid);
                        mMap.put("server_id",server_id);
                        mMap.put("sharetime",sharetime);
                        mMap.put("type","2");
                        mMap.put("noteid",noteid);
                        mMap.put("notetitle",notetitle);
                        mMap.put("consumer_certificate", "0");

                        pageJumpManager.jumpToWriteNoteActivity(PageJumpManager.DEFAULT_LOGO, mMap);

                    }

                    finish();
                }else {
                    Toast.makeText(PostingMessageActivity.this, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }

        });
    }


    class Clickable extends ClickableSpan {
        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            mListener = l;
        }

        /**
         * 重写父类点击事件
         */
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        /**
         * 重写父类updateDrawState方法  我们可以给TextView设置字体颜色,背景颜色等等...
         */
        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(Color.parseColor("#4D7BBC"));
            ds.setUnderlineText(true);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }
}
