package com.module.my.controller.adapter;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.my.model.bean.TypeProblemData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * 提问帖问题类型适配器
 * Created by 裴成浩 on 2019/3/14
 */
public class TypeProblemAdapter extends BaseQuickAdapter<TypeProblemData, BaseViewHolder> {

    public TypeProblemAdapter(int layoutResId, @Nullable List<TypeProblemData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TypeProblemData item) {
        helper.setText(R.id.item_type_problem_title, item.getTitle());

        helper.setText(R.id.item_type_problem_content, item.getDesc());

        LinearLayout typeProblem = helper.getView(R.id.item_type_problem);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) typeProblem.getLayoutParams();
        if (helper.getLayoutPosition() == 0) {
            layoutParams.bottomMargin = Utils.dip2px(7.5f);
        } else if (helper.getLayoutPosition() == mData.size()) {
            layoutParams.topMargin = Utils.dip2px(7.5f);
        } else {
            layoutParams.topMargin = Utils.dip2px(7.5f);
            layoutParams.bottomMargin = Utils.dip2px(7.5f);
        }
        typeProblem.setLayoutParams(layoutParams);
    }
}
