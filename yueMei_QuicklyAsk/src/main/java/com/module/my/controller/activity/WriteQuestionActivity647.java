package com.module.my.controller.activity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.my.model.api.PostTextQueApi;
import com.module.my.model.bean.ForumShareData;
import com.module.my.view.fragment.PostingAndNoteFragment;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.WriteResultData;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.YueMeiDialog;

import butterknife.BindView;

/**
 * 重构后的提问帖
 *
 * @author 裴成浩
 */
public class WriteQuestionActivity647 extends YMBaseActivity {

    @BindView(R.id.post_question_top)
    CommonTopBar mTop;                          //标题

    private PostingAndNoteFragment postingAndNoteFragment;
    private PostTextQueApi postTextQueApi;
    private String TAG = "WriteQuestionActivity647";
    private YueMeiDialog mYueMeiDialog;
    private BaseWebViewClientMessage webViewClientManager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_write_question647;
    }

    @Override
    protected void initView() {

        mYueMeiDialog = new YueMeiDialog(mContext, "建议填写选填项/上传照片，医生更能对症下药", "去添加", "继续提交");
        mYueMeiDialog.setCanceledOnTouchOutside(false);

        mTop.setCenterText("向专家提问");
        //设置发布默认是不可点击的
        mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));

        postingAndNoteFragment = PostingAndNoteFragment.newInstance(1);
        setActivityFragment(R.id.write_question_post_view, postingAndNoteFragment);

        postingAndNoteFragment.setOnClickListener(new PostingAndNoteFragment.OnClickListener() {
            @Override
            public void onButtonStateListener(boolean isClick) {
                if (isClick) {
                    mTop.setRightTextColor((Utils.getLocalColor(mContext, R.color.title_red_new)));
                } else {
                    mTop.setRightTextColor(Utils.getLocalColor(mContext, R.color.gary));
                }
            }
        });

        //关闭按钮点击
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                showDialogExitEdit();
            }
        });

        //发布按钮点击
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (postingAndNoteFragment.uploadingPrompt()) {
                    ForumShareData forumShareData = postingAndNoteFragment.setReleaseData();
                    if (postingAndNoteFragment.mTitle.getTitleLength() >= postingAndNoteFragment.mTitle.getMinLength()) {
                        if (postingAndNoteFragment.mContent.getContentLength() >= postingAndNoteFragment.mContent.getMinLength()) {
                            if (!TextUtils.isEmpty(forumShareData.getCover_photo())) {
                                uploadData(forumShareData);
                            } else {
                                mYueMeiDialog.show();
                            }
                        } else {
                            mFunctionManager.showShort("问题描述最少" + postingAndNoteFragment.mContent.getMinLength() + "个字");
                        }
                    } else {
                        mFunctionManager.showShort("标题字数限" + postingAndNoteFragment.mTitle.getMinLength() + "~" + postingAndNoteFragment.mTitle.getMaxLength() + "字");
                    }
                }
            }
        });

        //发表问题时视频图片为空时的提示
        mYueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
            @Override
            public void leftBtnClick() {
                mYueMeiDialog.dismiss();
            }

            @Override
            public void rightBtnClick() {
                mYueMeiDialog.dismiss();
                uploadData(postingAndNoteFragment.setReleaseData());
            }
        });
    }

    @Override
    protected void initData() {
        postTextQueApi = new PostTextQueApi();
        webViewClientManager = new BaseWebViewClientMessage(mContext);
    }

    /**
     * 上传数据
     */
    private void uploadData(ForumShareData forumShareData) {
        if (Utils.isLoginAndBind(mContext)) {
            mDialog.startLoading();
            Log.e(TAG, "forumShareData.getTitle() === " + forumShareData.getTitle());
            Log.e(TAG, "forumShareData.getCateid() === " + forumShareData.getCateid());
            Log.e(TAG, "forumShareData.getContent() === " + forumShareData.getContent());
            Log.e(TAG, "forumShareData.getVisibility() === " + forumShareData.getVisibility());
            Log.e(TAG, "forumShareData.getAskorshare() === " + forumShareData.getAskorshare());
            Log.e(TAG, "forumShareData.getImage() === " + forumShareData.getImage());
            Log.e(TAG, "forumShareData.getVideo() === " + forumShareData.getVideo());
            Log.e(TAG, "forumShareData.getCover_photo() === " + forumShareData.getCover_photo());
            Log.e(TAG, "forumShareData.getIs_sixin() === " + forumShareData.getIs_sixin());

            postTextQueApi.addData("title", forumShareData.getTitle());                                 //日记本标题
            postTextQueApi.addData("content", forumShareData.getContent());                             //内容
            postTextQueApi.addData("visibility", forumShareData.getVisibility());                       //图片/视频仅医生可见  默认是   （1）
            postTextQueApi.addData("askorshare", forumShareData.getAskorshare());                       //标识

            //判断标签串是否是空的
            if (!TextUtils.isEmpty(forumShareData.getCateid())) {
                postTextQueApi.addData("cateid", forumShareData.getCateid());                               //标签串
            }

            //判断是上传图片还是上传视频
            if (!TextUtils.isEmpty(forumShareData.getVideo())) {
                postTextQueApi.addData("video", forumShareData.getVideo());                                 //视频json
                postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
            } else if (!TextUtils.isEmpty(forumShareData.getImage())) {
                postTextQueApi.addData("image", forumShareData.getImage());                                 //图片json
                postTextQueApi.addData("cover_photo", forumShareData.getCover_photo());                     //封面图
            }

            postTextQueApi.addData("is_sixin", forumShareData.getIs_sixin());                               //是否同时发私信


            postTextQueApi.getCallBack(mContext, postTextQueApi.getHashMap(), new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData serverData) {
                    mDialog.stopLoading();
                    try {
                        if ("1".equals(serverData.code)) {
                            WriteResultData data = JSONUtil.TransformSingleBean(serverData.data, WriteResultData.class);
                            String id = data.get_id();              //帖子Id
                            String appmurl = data.getAppmurl();     //	跳转地址
                            String onelogin = data.getOnelogin();   //	是否是首次登录 1是
                            Log.e(TAG, "id === " + id);
                            Log.e(TAG, "appmurl === " + appmurl);
                            Log.e(TAG, "onelogin === " + onelogin);

                            webViewClientManager.showWebDetail(appmurl);

                            onBackPressed();
                        } else {
                            mFunctionManager.showShort(serverData.message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * dialog提示，是否退出此次编辑
     */
    private void showDialogExitEdit() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_dianhuazixun);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText("确定退出此次编辑吗？");

        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

        Button cancelBt99 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt99.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editDialog.dismiss();
                onBackPressed();
            }
        });
    }
}
