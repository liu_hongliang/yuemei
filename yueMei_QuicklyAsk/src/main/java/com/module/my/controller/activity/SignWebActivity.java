package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.model.bean.ExposureLoginData;
import com.module.my.controller.other.ParamsMap;
import com.module.my.controller.other.SignCallBack;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;

public class SignWebActivity extends YMBaseWebViewActivity {
    public static final String TAG = "SignWebActivity";
    @BindView(R.id.sign_paent_container)
    RelativeLayout signPaentContainer;
    @BindView(R.id.wan_beautiful_web_back)
    RelativeLayout wanBeautifulWebBack;
    @BindView(R.id.wan_beautifu_docname)
    TextView wanBeautifuDocname;
    @BindView(R.id.sign_container)
    RelativeLayout signContainer;
    @BindView(R.id.all_ly)
    LinearLayout allLy;
    @BindView(R.id.sign_web_view)
    SmartRefreshLayout taoWebView;
    public WebView docDetWeb;
    private Context mContex;
    private SignCallBack signCallBack;
    private String isHide;
    private String link;
    private StringBuilder baseUrl;

    HashMap<String, Object> maps = new HashMap<>();
    public static final String PARAMS = "params";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_web;
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void initView() {
        super.initView();
        mContex = SignWebActivity.this;
        //type:eq:6721:and:link:eq:/homenew/samecity/labelID/709/:and:labelIDs:eq:709:and:isHide:eq:1
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        link = getIntent().getStringExtra("link");
        Bundle bundle = getIntent().getExtras();
        baseUrl = new StringBuilder(FinalConstant.baseUrl + FinalConstant.VER+"/");
        ParamsMap paramsMap = (ParamsMap) bundle.get(PARAMS);
        if (paramsMap != null){
            Map<String, String> map = paramsMap.getMap();
            for (String key : map.keySet()) {
                String value = map.get(key);
                if ("link".equals(key)) {
                    if (value != null){
                        String[] urls = value.split("/");
                        for (int i = 1; i < urls.length; i++) {
                            if (i == 1) {
                                baseUrl.append(urls[1]).append("/");
                            } else if (i == 2) {
                                baseUrl.append(urls[2]).append("/");
                            } else {
                                maps.put(urls[i], urls[i + 1]);
                                i++;
                            }
                        }
                    }
                } else if ("isHide".equals(key)) {
                    isHide = value;
                } else {
                    maps.put(key, value);
                }
            }
        }else {
            baseUrl.append(link);
        }

        if (baseUrl.toString().contains("/task/checkinpage/")){
            ExposureLoginData exposureLoginData = new ExposureLoginData("137", "0");
            Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));
        }



        taoWebView.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                if (maps.size() > 0) {
                    loadUrl(baseUrl.toString(), maps);
                } else {
                    loadUrl(baseUrl.toString());
                }
            }
        });
        initWebView();
        //设置标题
        if (!TextUtils.isEmpty(isHide)) {
            if ("1".equals(isHide)) {
                signPaentContainer.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) docDetWeb.getLayoutParams();
                layoutParams.topMargin = statusbarHeight;
                docDetWeb.setLayoutParams(layoutParams);
            } else {
                signPaentContainer.setVisibility(View.VISIBLE);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) signPaentContainer.getLayoutParams();
                params.topMargin = statusbarHeight;
                signPaentContainer.setLayoutParams(params);
            }
        } else {
            signPaentContainer.setVisibility(View.VISIBLE);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) signPaentContainer.getLayoutParams();
            params.topMargin = statusbarHeight;
            signPaentContainer.setLayoutParams(params);
        }

        wanBeautifulWebBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void initData() {

    }


    private void initWebView() {

        docDetWeb = new WebView(mContex);
        docDetWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        signContainer.removeAllViews();
        signContainer.addView(docDetWeb);
        BaseWebViewClientMessage baseWebViewClientMessage = new BaseWebViewClientMessage(SignWebActivity.this);
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        signCallBack = new SignCallBack(this, docDetWeb);
        docDetWeb.addJavascriptInterface(signCallBack, "android");
        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                wanBeautifuDocname.setText(title);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    taoWebView.finishRefresh();
                }
                super.onProgressChanged(view, newProgress);
            }

            //重写WebChromeClient的onGeolocationPermissionsShowPrompt
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (maps.size() > 0) {
            loadUrl(baseUrl.toString(), maps);
        } else {
            loadUrl(baseUrl.toString());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
        final String isSign = sign.getString(FinalConstant.SIGN_FLAG, "0");
        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
        if (b) {
            Log.e(TAG, "onRestart ==容许");
            docDetWeb.post(new Runnable() {
                @Override
                public void run() {
                    docDetWeb.loadUrl("javascript:appNoticeStatusCallBack(\"1|\"" + isSign + "\")");
                }
            });


        } else {
            Log.e(TAG, "onRestart ==不容许");
            docDetWeb.post(new Runnable() {
                @Override
                public void run() {
                    docDetWeb.loadUrl("javascript:appNoticeStatusCallBack(\"0|\"" + isSign + "\")");
                }
            });

        }
        docDetWeb.reload();
    }

    protected void loadUrl(String url) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        Log.e(TAG,addressAndHead.getHttpHeaders().toString());
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }


    protected void loadUrl(String url, Map<String, Object> paramMap) {
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, paramMap);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }


    @Override
    protected void onDestroy() {
        if (baseUrl.toString().contains("/task/checkinpage/")){
            Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        }
        super.onDestroy();
    }
}
