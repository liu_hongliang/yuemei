package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.home.view.LoadingProgress;
import com.module.my.model.api.ConsumVoucherApi;
import com.module.my.model.api.UpdateMypostDataApi;
import com.module.my.model.bean.PostInfoData;
import com.module.my.model.bean.PostInfoPic;
import com.module.other.netWork.netWork.QiNuConfig;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.util.MyUploadImage;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.zfdang.multiple_images_selector.ImagesSelectorActivity;
import com.zfdang.multiple_images_selector.SelectorSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * 补充信息
 * <p>
 * Created by dwb on 16/3/28.
 */
public class AddMoreNoteMessageActivity extends BaseActivity {

    private final String TAG = "AddMoreNoteMessage";

    private AddMoreNoteMessageActivity mContext;

    private String[] drr_ = new String[4];

    @BindView(id = R.id.addmore_top)
    private CommonTopBar addmoreTop;
    @BindView(id = R.id.add_title)
    private EditText addTitleEt;
    @BindView(id = R.id.edi_title_rly)
    private RelativeLayout editTitleRly;

    @BindView(id = R.id.add_hosname)
    private EditText addHosnameEt;
    @BindView(id = R.id.edi_hosname_rly)
    private RelativeLayout editHosNameRly;

    @BindView(id = R.id.add_docname)
    private EditText addDocnameEt;
    @BindView(id = R.id.edi_docname_rly)
    private RelativeLayout editDocNameRly;

    @BindView(id = R.id.add_fee)
    private EditText addFeeEt;
    @BindView(id = R.id.edit_fee_rly)
    private RelativeLayout editFeeRly;

    private String ratN1 = "0";
    private String ratN2 = "0";
    private String ratN3 = "0";

    private String mainid = "";

    private String uid;
    //    private String content = "";// 提交的内容
    private String hosname = "";// 医院名称
    private String docname = "";// 医生名字
    private String money = "";// 费用
    private String title = "";//日记标题

    Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    // 评分系统
    @BindView(id = R.id.room_ratingbar1)
    private RatingBar rating1;
    @BindView(id = R.id.room_ratingbar2)
    private RatingBar rating2;
    @BindView(id = R.id.room_ratingbar3)
    private RatingBar rating3;

    @BindView(id = R.id.rating_tv1)
    private TextView ratingTv1;
    @BindView(id = R.id.rating_tv2)
    private TextView ratingTv2;
    @BindView(id = R.id.rating_tv3)
    private TextView ratingTv3;

    private PostInfoData postUpdateData;

    @BindView(id = R.id.rootview_lyly)
    private LinearLayout allContent;// 整个界面容器

    @BindView(id = R.id.add_iv1)
    private ImageView addIv1;
    private boolean isIv1 = false;
    private String pic_id1 = "0";
    @BindView(id = R.id.add_iv1_bt1)
    private Button addIvBt1;
    @BindView(id = R.id.add_iv2)
    private ImageView addIv2;
    private boolean isIv2 = false;
    private String pic_id2 = "0";
    @BindView(id = R.id.add_iv1_bt2)
    private Button addIvBt2;
    @BindView(id = R.id.add_iv3)
    private ImageView addIv3;
    private boolean isIv3 = false;
    private String pic_id3 = "0";
    @BindView(id = R.id.add_iv1_bt3)
    private Button addIvBt3;
    @BindView(id = R.id.add_iv4)
    private ImageView addIv4;
    private boolean isIv4 = false;
    private String pic_id4 = "0";
    @BindView(id = R.id.add_iv1_bt4)
    private Button addIvBt4;

    @BindView(id = R.id.click_consumer_certificate)
    private LinearLayout click_consumer_certificate;
    @BindView(id = R.id.modification)
    private TextView modification;

    private String iv_type = "1";


    private List<PostInfoPic> piclist = new ArrayList<>();
    public static final int FROM_GALLERY = 777;

    ImageOptions imageOptions;

    private String pic_id_ = "0";
    private LoadingProgress progress;
    private ArrayList<String> mResults2 = new ArrayList<>();
    public static final int CONSUMER_CERTIFICATE_START = 13;     //消费凭证开始上传
    public static final int CONSUMER_CERTIFICATE_LODING = 14;     //消费凭证上传中
    public static final int CONSUMER_CERTIFICATE_SUCCESS = 15;     //消费凭证上传完成
    HashMap<String, JSONObject> mVoucher = new HashMap<>();            //消费凭证上传成功后的集合。key：本地存储路径，vle：本地存储路径
    private Handler mHandler = new MyHandler(this);
    private String relustHosname = "";
    private String relustDocname = "";
    private float relustProgress = 0;
    private int[] mImageWidthHeight;
    private String mKey;

    private static class MyHandler extends Handler {
        private final WeakReference<AddMoreNoteMessageActivity> mActivity;

        public MyHandler(AddMoreNoteMessageActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            AddMoreNoteMessageActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case CONSUMER_CERTIFICATE_START:
                        theActivity.upLoadFile();
                        break;
                    case CONSUMER_CERTIFICATE_LODING:
                        theActivity.modification.setText("上传中");
                        break;
                    case CONSUMER_CERTIFICATE_SUCCESS:
                        theActivity.modification.setText("修改");
                        ViewInject.toast("消费凭证上传成功！");
                        JSONObject jsonObject = theActivity.setJson();
                        theActivity.mVoucher.put(theActivity.mResults2.get(0), jsonObject);
                        break;
                }
            }

        }
    }

    private JSONObject setJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("width", mImageWidthHeight[0]);
            jsonObject.put("height", mImageWidthHeight[1]);
            jsonObject.put("img", mKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_addmore_notemessage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = AddMoreNoteMessageActivity.this;

        Intent it = getIntent();
        if (it != null) {
            mainid = it.getStringExtra("mainQid");
        }
        initView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        progress = new LoadingProgress(mContext);

        rating1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv1.setText("差");
                    ratN1 = 1 + "";
                } else if (rating == 2) {
                    ratingTv1.setText("不满意");
                    ratN1 = 2 + "";
                } else if (rating == 3) {
                    ratingTv1.setText("一般");
                    ratN1 = 3 + "";
                } else if (rating == 4) {
                    ratingTv1.setText("比较满意");
                    ratN1 = 4 + "";
                } else if (rating == 5) {
                    ratingTv1.setText("非常满意");
                    ratN1 = 5 + "";
                } else {
                    ratingTv1.setText("");
                    ratN1 = 0 + "";
                }
            }
        });

        rating2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv2.setText("差");
                    ratN2 = 1 + "";
                } else if (rating == 2) {
                    ratingTv2.setText("不满意");
                    ratN2 = 2 + "";
                } else if (rating == 3) {
                    ratingTv2.setText("一般");
                    ratN2 = 3 + "";
                } else if (rating == 4) {
                    ratingTv2.setText("比较满意");
                    ratN2 = 4 + "";
                } else if (rating == 5) {
                    ratingTv2.setText("非常满意");
                    ratN2 = 5 + "";
                } else {
                    ratingTv2.setText("");
                    ratN2 = 0 + "";
                }
            }
        });

        rating3.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating == 1) {
                    ratingTv3.setText("差");
                    ratN3 = 1 + "";
                } else if (rating == 2) {
                    ratingTv3.setText("不满意");
                    ratN3 = 2 + "";
                } else if (rating == 3) {
                    ratingTv3.setText("一般");
                    ratN3 = 3 + "";
                } else if (rating == 4) {
                    ratingTv3.setText("比较满意");
                    ratN3 = 4 + "";
                } else if (rating == 5) {
                    ratingTv3.setText("非常满意");
                    ratN3 = 5 + "";
                } else {
                    ratingTv3.setText("");
                    ratN3 = 0 + "";
                }
            }
        });

        getUpdateMypostData();

        /**
         * 添加照片一
         */
        addIv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                new PopupWindows(AddMoreNoteMessageActivity.this, allContent);
                iv_type = "1";
            }
        });

        /**
         * 添加照片二
         */
        addIv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                new PopupWindows(AddMoreNoteMessageActivity.this, allContent);
                iv_type = "2";
            }
        });

        /**
         * 添加照片三
         */
        addIv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                new PopupWindows(AddMoreNoteMessageActivity.this, allContent);
                iv_type = "3";
            }
        });

        /**
         * 添加照片四
         */
        addIv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                new PopupWindows(AddMoreNoteMessageActivity.this, allContent);
                iv_type = "4";
            }
        });

        /**
         * 删除照片一
         */
        addIvBt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt1.setVisibility(View.GONE);
                addIv1.setImageDrawable(null);
                if (!"0".equals(pic_id1)) {
                    isIv1 = true;
                }
            }
        });

        /**
         * 删除照片二
         */
        addIvBt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt2.setVisibility(View.GONE);
                addIv2.setImageDrawable(null);
                if (!"0".equals(pic_id2)) {
                    isIv2 = true;
                }
            }
        });

        /**
         * 删除照片三
         */
        addIvBt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt3.setVisibility(View.GONE);
                addIv3.setImageDrawable(null);
                if (!pic_id3.equals("0")) {
                    isIv3 = true;
                }
            }
        });

        /**
         * 删除照片四
         */
        addIvBt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIvBt4.setVisibility(View.GONE);
                addIv4.setImageDrawable(null);
                if (!pic_id4.equals("0")) {
                    isIv4 = true;
                }
            }
        });


        /**
         * 编辑标题
         */
        editTitleRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("TAG", "aaaa");
                addTitleEt.setEnabled(true);
                addTitleEt.setFocusable(true);
            }
        });


        /**
         * 编辑价钱
         */
        editFeeRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFeeEt.setEnabled(true);
                addFeeEt.setFocusable(true);
            }
        });


        /**
         * 编辑医院名字
         */
        editHosNameRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addHosnameEt.setEnabled(true);
                addHosnameEt.setFocusable(true);
            }
        });

        /**
         * 编辑医生名字
         */
        editDocNameRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDocnameEt.setEnabled(true);
                addDocnameEt.setFocusable(true);
            }
        });
        click_consumer_certificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            toXIangce();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
//                                    Toast.makeText(mContext, "没有权限", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    toXIangce();
                }
            }
        });

    }

    void toXIangce() {
        Log.e(TAG, "mResults2 === " + mResults2.size());
        // 启动多个图片选择器
        Intent intent = new Intent(AddMoreNoteMessageActivity.this, ImagesSelectorActivity.class);
        // 要选择的图像的最大数量
        intent.putExtra(SelectorSettings.SELECTOR_MAX_IMAGE_NUMBER, 1);
        // 将显示的最小尺寸图像;用于过滤微小的图像(主要是图标)
        intent.putExtra(SelectorSettings.SELECTOR_MIN_IMAGE_SIZE, 50000);
        // 显示摄像机或不
        intent.putExtra(SelectorSettings.SELECTOR_SHOW_CAMERA, true);
        // 将当前选定的图像作为初始值传递
        intent.putStringArrayListExtra(SelectorSettings.SELECTOR_INITIAL_SELECTED_LIST, mResults2);
        // 开始选择器
        startActivityForResult(intent, FROM_GALLERY);

    }

    void upLoadFile() {
        // 压缩图片
        String pathS = Environment.getExternalStorageDirectory().toString() + "/YueMeiImage";
        File path1 = new File(pathS);// 建立这个路径的文件或文件夹
        if (!path1.exists()) {// 如果不存在，就建立文件夹
            path1.mkdirs();
        }
        File file = new File(path1, "yuemei_" + System.currentTimeMillis() + ".jpg");
        String desPath = file.getPath();
        FileUtils.compressPicture(mResults2.get(0), desPath);
        mImageWidthHeight = FileUtils.getImageWidthHeight(desPath);
        mKey = QiNuConfig.getKey();
        MyUploadImage.getMyUploadImage(mContext, mHandler, desPath).upConsumerCertificate(mKey);
    }

    /**
     * 获取更新数据
     */
    void getUpdateMypostData() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mainid);
        new UpdateMypostDataApi().getCallBack(mContext, maps, new BaseCallBackListener<PostInfoData>() {
            @Override
            public void onSuccess(PostInfoData postUpdateData) {
                if ("0".equals(postUpdateData.getServer_id())) {
                    click_consumer_certificate.setVisibility(View.VISIBLE);

                }
                if ("1".equals(postUpdateData.getIs_consumer_voucher())) {
                    modification.setText("修改");
                }
                rating1.setRating(Integer.parseInt(postUpdateData.getService()));
                ratN1 = postUpdateData.getService();
                if (ratN1.equals("1")) {
                    ratingTv1.setText("差");
                } else if (ratN1.equals("2")) {
                    ratingTv1.setText("不满意");
                } else if (ratN1.equals("3")) {
                    ratingTv1.setText("一般");
                } else if (ratN1.equals("4")) {
                    ratingTv1.setText("比较满意");
                } else if (ratN1.equals("5")) {
                    ratingTv1.setText("非常满意");
                } else {
                    ratingTv1.setText("");
                }
                rating2.setRating(Integer.parseInt(postUpdateData.getEffect()));
                ratN2 = postUpdateData.getEffect();
                if (ratN2.equals("1")) {
                    ratingTv2.setText("差");
                } else if (ratN2.equals("2")) {
                    ratingTv2.setText("不满意");
                } else if (ratN2.equals("3")) {
                    ratingTv2.setText("一般");
                } else if (ratN2.equals("4")) {
                    ratingTv2.setText("比较满意");
                } else if (ratN2.equals("5")) {
                    ratingTv2.setText("非常满意");
                } else {
                    ratingTv2.setText("");
                }
                rating3.setRating(Integer.parseInt(postUpdateData.getPf_doctor()));
                ratN3 = postUpdateData.getPf_doctor();
                if (ratN3.equals("1")) {
                    ratingTv3.setText("差");
                } else if (ratN3.equals("2")) {
                    ratingTv3.setText("不满意");
                } else if (ratN3.equals("3")) {
                    ratingTv3.setText("一般");
                } else if (ratN3.equals("4")) {
                    ratingTv3.setText("比较满意");
                } else if (ratN3.equals("5")) {
                    ratingTv3.setText("非常满意");
                } else {
                    ratingTv3.setText("");
                }

                if (null != postUpdateData.getHosname() && postUpdateData.getHosname().length() > 0 && !postUpdateData.getHosname().equals("0")) {
                    hosname = postUpdateData.getHosname();
                    addHosnameEt.setText(hosname);
                    if (postUpdateData.getHos_id().equals("0")) {

                        editHosNameRly.setVisibility(View.VISIBLE);
                        addHosnameEt.setEnabled(true);

                    } else {
                        addHosnameEt.setTextColor(Color.parseColor("#333333"));
                        addHosnameEt.setEnabled(false);
                        editHosNameRly.setVisibility(View.GONE);

                    }
                }
                if (null != postUpdateData.getDocname() && postUpdateData.getDocname().length() > 0 && !postUpdateData.getDocname().equals("0")) {
                    docname = postUpdateData.getDocname();
                    addDocnameEt.setText(docname);

                    if (postUpdateData.getDoc_id().equals("0")) {
                        editDocNameRly.setVisibility(View.VISIBLE);
                        addDocnameEt.setEnabled(true);

                    } else {
                        addDocnameEt.setTextColor(Color.parseColor("#333333"));
                        addDocnameEt.setEnabled(false);

                        editDocNameRly.setVisibility(View.GONE);
                    }
                }

                if (null != postUpdateData.getMoney() && postUpdateData.getMoney().length() > 0 && !postUpdateData.getMoney().equals("0")) {
                    money = postUpdateData.getMoney();
                    addFeeEt.setText(money);
                    addFeeEt.setTextColor(Color.parseColor("#333333"));
                    addFeeEt.setEnabled(false);
                }

                if (null != postUpdateData.getTitle() && postUpdateData.getTitle().length() > 0) {
                    title = postUpdateData.getTitle();
                    addTitleEt.setText(title);
                    addTitleEt.setTextColor(Color.parseColor("#333333"));
                    Log.e("TAG", "bbb");
                    addTitleEt.setEnabled(false);
                }

                piclist = postUpdateData.getPic();

                if (null != piclist && piclist.size() > 0) {
                    for (int i = 0; i < piclist.size(); i++) {
                        String img = piclist.get(i).getImg();
                        String wight = piclist.get(i).getWeight();
                        String pic_id = piclist.get(i).getPic_id();

                        if (wight.equals("1")) {
                            pic_id1 = pic_id;
                            addIvBt1.setVisibility(View.VISIBLE);

                            x.image().bind(addIv1, img, imageOptions);

                        } else if (wight.equals("2")) {
                            pic_id2 = pic_id;
                            addIvBt2.setVisibility(View.VISIBLE);

                            x.image().bind(addIv2, img, imageOptions);
                        } else if (wight.equals("3")) {
                            pic_id3 = pic_id;
                            addIvBt3.setVisibility(View.VISIBLE);

                            x.image().bind(addIv3, img, imageOptions);
                        } else if (wight.equals("4")) {
                            pic_id4 = pic_id;
                            addIvBt4.setVisibility(View.VISIBLE);

                            x.image().bind(addIv4, img, imageOptions);
                        }

                    }
                }
            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        uid = Utils.getUid();
    }

    void initView() {
        addmoreTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        addmoreTop.setClickable(false);
                        progress.startLoading();
                        postFileQue();
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
            }
        });
        modification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
            }
        });

    }

    /**
     * 弹出照相 图库选择
     *
     * @author lenovo17
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.item_popupwindows, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.FILL_PARENT);
            setHeight(ViewGroup.LayoutParams.FILL_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view.findViewById(R.id.item_popupwindows_camera);
            Button bt2 = view.findViewById(R.id.item_popupwindows_Photo);
            Button bt3 = view.findViewById(R.id.item_popupwindows_cancel);
            // 拍照
            bt1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    photo();
                    dismiss();
                }
            });
            // 点击 之外 消失
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });
            // 相册
            bt2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(galleryIntent, IMAGE_REQUEST_CODE);
                    dismiss();
                }
            });
            bt3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    private static final int IMAGE_REQUEST_CODE = 0;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int RESIZE_REQUEST_CODE = 2;

    private Uri mOutPutFileUri;
    private Uri imageUri;

    public void photo() {
        try {
            Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            String pathS = Environment.getExternalStorageDirectory().toString() + "/AyuemeiImage";
            File path1 = new File(pathS);

            if (!path1.exists()) {
                path1.mkdirs();
            }
            File file = new File(path1, "yuemei" + System.currentTimeMillis() + ".JPEG");

            mOutPutFileUri = Uri.fromFile(file);

            openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mOutPutFileUri);

            startActivityForResult(openCameraIntent, CAMERA_REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMAGE_REQUEST_CODE:
                if (data != null) {
                    resizeImage(data.getData());
                }
                break;
            case CAMERA_REQUEST_CODE:
                if (resultCode == -1) {
                    resizeImage(mOutPutFileUri);
                }
                break;
            case RESIZE_REQUEST_CODE:
                if (data != null && resultCode == -1) {
                    showResizeImage(data);
                }
                break;
            case FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    if (null != data) {
                        mResults2 = data.getStringArrayListExtra(SelectorSettings.SELECTOR_RESULTS);
                        if (mResults2 != null && mResults2.size() > 0) {
                            Message msg = Message.obtain();
                            msg.what = CONSUMER_CERTIFICATE_START;
                            mHandler.sendMessage(msg);

                        }
                    }
                }
                break;
        }
    }

    public void resizeImage(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        // 不启用人脸识别
        intent.putExtra("noFaceDetection", false);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, RESIZE_REQUEST_CODE);
    }

    private void showResizeImage(Intent data) {

        if (data != null) {

            Bundle extras = data.getExtras();
            if (extras != null) {
                Bitmap photo = extras.getParcelable("data");
                final String picName = "yuemei" + System.currentTimeMillis();
                FileUtils.saveBitmap(photo, picName);// 保存图片
                String filePath = FileUtils.SDPATH + picName + ".JPEG";
                Drawable drawable = new BitmapDrawable(photo);
                if (iv_type.equals("1")) {
                    addIv1.setImageDrawable(drawable);
                    drr_[0] = filePath;
                    addIvBt1.setVisibility(View.VISIBLE);
                } else if (iv_type.equals("2")) {
                    addIv2.setImageDrawable(drawable);
                    drr_[1] = filePath;
                    addIvBt2.setVisibility(View.VISIBLE);
                } else if (iv_type.equals("3")) {
                    addIv3.setImageDrawable(drawable);
                    drr_[2] = filePath;
                    addIvBt3.setVisibility(View.VISIBLE);
                } else if (iv_type.equals("4")) {
                    addIv4.setImageDrawable(drawable);
                    drr_[3] = filePath;
                    addIvBt4.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    /**
     * 上传图片文件
     */
    void postFileQue() {

        hosname = addHosnameEt.getText().toString();
        docname = addDocnameEt.getText().toString();
        money = addFeeEt.getText().toString();
        title = addTitleEt.getText().toString();

        if (isIv1) {
            pic_id_ = pic_id1;
        }
        if (isIv2) {
            pic_id_ = pic_id_ + "," + pic_id2;
        }
        if (isIv3) {
            pic_id_ = pic_id_ + "," + pic_id3;
        }
        if (isIv4) {
            pic_id_ = pic_id_ + "," + pic_id4;
        }

        Map<String, Object> maps = new HashMap<>();
        maps.put("id", mainid);

        if(!TextUtils.isEmpty(hosname)){
            maps.put("hosname", hosname);
        }

        if(!TextUtils.isEmpty(docname)){
            maps.put("docname", docname);
        }

        if(!TextUtils.isEmpty(title)){
            maps.put("title", title);
        }

        if(!TextUtils.isEmpty(money)){
            maps.put("money", money);
        }
        maps.put("service", ratN1);
        maps.put("effect", ratN2);
        maps.put("pf_doctor", ratN3);
        maps.put("pic_id", pic_id_);
        if (mVoucher.size() > 0 && mResults2.size() > 0) {
            if (null != mVoucher.get(mResults2.get(0))) {
                maps.put("consumer_voucher", mVoucher.get(mResults2.get(0)).toString());
            }
        }


        Log.e(TAG, "id === " + mainid);
        Log.e(TAG, "hosname === " + hosname);
        Log.e(TAG, "docname === " + docname);
        Log.e(TAG, "title === " + title);
        Log.e(TAG, "money === " + money);
        Log.e(TAG, "ratN1 === " + ratN1);
        Log.e(TAG, "ratN2 === " + ratN2);
        Log.e(TAG, "ratN3 === " + ratN3);
        Log.e(TAG, "pic_id_ === " + pic_id_);

        Log.e(TAG, "maps === " + maps.toString());

        new ConsumVoucherApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "serverData === " + serverData.data);
                progress.stopLoading();

                if (serverData.code.equals("1")) {
                    MyToast.makeImgToast(mContext, getResources().getDrawable(R.drawable.tips_submit_success2x), 1000).show();

                    relustHosname = hosname;
                    relustDocname = docname;
                    relustProgress = (Integer.parseInt(ratN1) * 100 + Integer.parseInt(ratN2) * 100 + Integer.parseInt(ratN3) * 100) / 15;

                    Intent intent = new Intent();
                    intent.putExtra("hos", relustHosname);
                    intent.putExtra("doc", relustDocname);
                    intent.putExtra("progress", (int) relustProgress);
                    setResult(100, intent);
                    finish();
                } else {
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }


    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}