package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.my.model.api.ShareDataApi;
import com.module.my.model.bean.GetCodeData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 邀请好友注册
 *
 * @author Rubin
 */
public class InvitationFriendActivity extends BaseActivity {

    private final String TAG = "InvitationFriendActivity";

    @BindView(id = R.id.invi_code_tv)
    private TextView inviCodeTv;
    @BindView(id = R.id.invi_bt, click = true)
    private Button inviBt;

    @BindView(id = R.id.share_code_top)
    private CommonTopBar mTop;// 返回

    @BindView(id = R.id.invi_you_tv1)
    private TextView contetnTv1;
    @BindView(id = R.id.invi_you_tv2)
    private TextView contetnTv2;
    @BindView(id = R.id.invi_you_tv3)
    private TextView contetnTv3;
    @BindView(id = R.id.share_code_title_iv)
    private ImageView titleIv;

    private InvitationFriendActivity mContex;

    private String uid;
    private String type;
    private GetCodeData getCodeData;

    private String shareCode;
    private String shareUrl;
    private String logoUrl;

    private int windowsWight;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_share_code);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = InvitationFriendActivity.this;
        uid = Utils.getUid();
        getShareData();

        mTop.setCenterText("邀请好友");
        windowsWight = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        LayoutParams params0 = titleIv.getLayoutParams();
        params0.height = (int) (windowsWight / 1.89);
        titleIv.setLayoutParams(params0);

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        InvitationFriendActivity.this.finish();
                    }
                });

        String text = "1.点击邀请好友，将邀请码分享给好友，好友登录悦美整形APP，注册成功进入我的颜值币输入邀请码，即可获得50颜值币。";
        SpannableStringBuilder style1 = new SpannableStringBuilder(text);
        // style.setSpan(new BackgroundColorSpan(Color.RED), 2, 5,
        // Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置textview的背景颜色
        style1.setSpan(new ForegroundColorSpan(Color.RED), 51, 53,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置文字的颜色
        contetnTv1.setText(style1);

        String text1 = "2.每日最多可邀请3名好友。";
        SpannableStringBuilder style2 = new SpannableStringBuilder(text1);
        // style.setSpan(new BackgroundColorSpan(Color.RED), 2, 5,
        // Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置textview的背景颜色
        style2.setSpan(new ForegroundColorSpan(Color.RED), 9, 10,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置文字的颜色
        contetnTv2.setText(style2);

        String text2 = "3.1颜值币等价于1元，可用于淘整形支付使用。";
        SpannableStringBuilder style3 = new SpannableStringBuilder(text2);
        // style.setSpan(new BackgroundColorSpan(Color.RED), 2, 5,
        // Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置textview的背景颜色
        style3.setSpan(new ForegroundColorSpan(Color.RED), 2, 3,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置文字的颜色
        style3.setSpan(new ForegroundColorSpan(Color.RED), 8, 9,
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置文字的颜色
        contetnTv3.setText(style3);

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    void getShareData() {
        Map<String,Object> maps=new HashMap<>();
        maps.put("uid",uid);
        new ShareDataApi().getCallBack(mContex, maps, new BaseCallBackListener<GetCodeData>() {
            @Override
            public void onSuccess(GetCodeData getCodeData) {
                shareCode = getCodeData.getInvitationCode();
                shareUrl = getCodeData.getUrl();
                logoUrl = getCodeData.getLogo();
                inviCodeTv.setText(shareCode);
            }

        });
    }


    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.invi_bt://分享
                if (null != shareUrl) {
                    setShare();
                }
                break;
        }
    }

    /**
     * 设置分享
     */
    private void setShare() {
        BaseShareView baseShareView = new BaseShareView(mContex);
        baseShareView
                .setShareContent("邀请你加入悦美美容整形社区！")
                .ShareAction();

        baseShareView.getShareBoardlistener()
                .setSinaText("小伙伴们，最近我发现了一个不错的美容整形社区，这里的姊妹们通过整形简直美翻了!~特邀你一起加入围观，下载APP地址"
                        + shareUrl
                        + "，注册悦美输入我的邀请号为："
                        + shareCode
                        + "，可以获得xx颜值币（1颜值币=1元）哦！分享自@悦美整形APP")
                .setSinaThumb(new UMImage(mContex, logoUrl))
                .setSmsText("我刚刚发现了一个不错的整形分享社区，特邀你一起加入，帮忙下载APP地址  " + shareUrl
                        + " ，注册时输入我的邀请号为： " + shareCode
                        + " ，就可获得50颜值币(1颜值币=1元)等价兑换整形优惠！（分享自@悦美整形APP）")
                .setTencentUrl(shareUrl)
                .setTencentTitle("邀请你加入悦美美容整形社区！")
                .setTencentThumb(new UMImage(mContex, logoUrl))
                .setTencentDescription("下载悦美APP，注册悦美输入我的邀请号：" + shareCode +
                        " ，就可获得50颜值币(1颜值币=1元)等价兑换整形优惠！")
                .setTencentText("下载悦美APP，注册悦美输入我的邀请号：" + shareCode +
                        " ，就可获得50颜值币(1颜值币=1元)等价兑换整形优惠！")
                .getUmShareListener()
                .setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
                    @Override
                    public void onShareResultClick(SHARE_MEDIA platform) {

                        if (!platform.equals(SHARE_MEDIA.SMS)) {
                            Toast.makeText(mContex, " 分享成功啦",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
