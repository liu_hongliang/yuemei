/**
 * 
 */
package com.module.my.controller.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.view.fragment.CollectBBsFragment;
import com.module.my.view.fragment.CollectBaikeFragment;
import com.module.my.view.fragment.ColletTaoFragment;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

/**
 * 我的收藏
 * 
 * @author Robin
 * 
 */
public class MyCollectActivity550 extends YMBaseActivity {

	private final String TAG = "MyCollectActivity550";
	private Context mContext;

	// 四个tab
	private RelativeLayout taoRly;// 淘整形
	private RelativeLayout bbsRly;// 帖子
	private RelativeLayout baikeRly;// 百科
	private TextView taoTv;
	private TextView bbsTv;
	private TextView baikeTv;
	private View taoLine;
	private View bbsLine;
	private View baikeLine;
	private CommonTopBar mTop;

	private ViewPager mViewPager;
	private TabFragmentPagerAdapter mAdapter;


	void findViewById() {
		taoRly = findViewById(R.id.search_tao_rly);
		taoTv = findViewById(R.id.search_tao_tv);
		taoLine = findViewById(R.id.search_tao_line);
		bbsRly = findViewById(R.id.search_bbs_rly);
		bbsTv = findViewById(R.id.search_bbs_tv);
		bbsLine = findViewById(R.id.search_bbs_line);

		baikeRly = findViewById(R.id.search_baike_rly);
		baikeTv = findViewById(R.id.search_baike_tv);
		baikeLine = findViewById(R.id.search_baike_line);

		mViewPager = findViewById(R.id.mViewPager);
		mTop = findViewById(R.id.collect_top);
		mAdapter = new TabFragmentPagerAdapter(getSupportFragmentManager());
		mViewPager.setAdapter(mAdapter);
		int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
		ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTop.getLayoutParams();
		layoutParams.topMargin = statusbarHeight;

	}

	private void initTab(int curPostion) {
		if (curPostion == 0) {
			taoLine.setVisibility(View.VISIBLE);
			bbsLine.setVisibility(View.GONE);
			baikeLine.setVisibility(View.GONE);
			taoTv.setTextColor(getResources().getColor(R.color._33));
			bbsTv.setTextColor(getResources().getColor(R.color._88));
			baikeTv.setTextColor(getResources().getColor(R.color._88));
		} else if (curPostion == 1) {
			taoLine.setVisibility(View.GONE);
			bbsLine.setVisibility(View.VISIBLE);
			baikeLine.setVisibility(View.GONE);
			taoTv.setTextColor(getResources().getColor(R.color._88));
			bbsTv.setTextColor(getResources().getColor(R.color._33));
			baikeTv.setTextColor(getResources().getColor(R.color._88));

		} else if (curPostion == 2) {
			taoLine.setVisibility(View.GONE);
			bbsLine.setVisibility(View.GONE);
			baikeLine.setVisibility(View.VISIBLE);
			taoTv.setTextColor(getResources().getColor(R.color._88));
			bbsTv.setTextColor(getResources().getColor(R.color._88));
			baikeTv.setTextColor(getResources().getColor(R.color._33));

		}
	}

	void setListener() {

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				if (position == 0) {
					initTab(0);
				} else if (position == 1) {
					initTab(1);
				} else if (position == 2) {
					initTab(2);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

		taoRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				initTab(0);
				mViewPager.setCurrentItem(0);
			}
		});

		bbsRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				initTab(1);
				mViewPager.setCurrentItem(1);
			}
		});

		baikeRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				initTab(2);
				mViewPager.setCurrentItem(2);
			}
		});

	}

	public class TabFragmentPagerAdapter extends FragmentStatePagerAdapter {

		public TabFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg) {
			Fragment ft = null;
			switch (arg) {
			case 0:
				ft = new ColletTaoFragment();
				break;
			case 1:
				ft = new CollectBBsFragment();
				break;
			case 2:
				ft = new CollectBaikeFragment();
				break;
			}
			return ft;
		}

		@Override
		public int getCount() {
			return 3;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.acty_collect_550;
	}

	@Override
	protected void initView() {
		mContext = MyCollectActivity550.this;
		findViewById();
		setListener();
	}

	@Override
	protected void initData() {

	}

}
