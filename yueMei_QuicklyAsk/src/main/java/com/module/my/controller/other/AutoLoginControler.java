package com.module.my.controller.other;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.utils.StatisticalManage;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.controller.activity.LoginActivity605;
import com.module.my.model.api.LoginOtherHttpApi;
import com.module.my.model.api.PhoneLoginApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.PhoneLoginBean;
import com.module.my.model.bean.ShenHeData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.jiguang.verifysdk.api.AuthPageEventListener;
import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.JVerifyUIClickCallback;
import cn.jiguang.verifysdk.api.JVerifyUIConfig;
import cn.jiguang.verifysdk.api.PreLoginListener;
import cn.jiguang.verifysdk.api.VerifyListener;

public class AutoLoginControler extends Activity {
    private String TAG = "AutoLoginControler";
    private Activity mActivity;
    private UMShareAPI umShareAPI;
    private boolean isAutoThird = false;
    private boolean mIsWeixin;
    private String mReferrer;
    private String mReferrerId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = AutoLoginControler.this;
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        //不接受触摸屏事件
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        boolean verifyEnable = JVerificationInterface.checkVerifyEnable(mActivity);
        if (verifyEnable) {
            autoLogin(mActivity);
        } else {
            jumpLogin(mActivity);
        }
        Cfg.saveStr(this,"is_jump_login","0");
    }



    public void autoLogin(final Context activity) {

        int weight = Cfg.loadInt(activity, FinalConstant.WINDOWS_W, 0);
        int height = Cfg.loadInt(activity, FinalConstant.WINDOWS_H, 0);
        float marginBottom = (float) (height * 0.15);
        int marginLift = (weight - Utils.dip2px(303)) / 2;

        umShareAPI = UMShareAPI.get(activity);

        String loadStr = Cfg.loadStr(this, FinalConstant.EXPOSURE_LOGIN, "");
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(loadStr)) {
            ExposureLoginData exposureLoginData = new Gson().fromJson(loadStr, ExposureLoginData.class);
            mReferrer = exposureLoginData.getReferrer();
            hashMap.put("referrer", mReferrer);
            mReferrerId = exposureLoginData.getReferrer_id();
            hashMap.put("referrer_id", mReferrerId);
        }else{
            hashMap.put("referrer", "0");
            hashMap.put("referrer_id", "0");
        }
        Log.e(TAG, "hashMap == " + hashMap.toString());
        TextView otherLogin = new TextView(activity);
        otherLogin.setText("社交账号登录");
        otherLogin.setTextColor(ContextCompat.getColor(activity, R.color._99));
        otherLogin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.setMargins(0, 0, 0, (int) marginBottom + Utils.dip2px(57));
        otherLogin.setLayoutParams(layoutParams);


        Button mPhoneBtn = new Button(activity);
        mPhoneBtn.setBackground(ContextCompat.getDrawable(activity, R.drawable.auto_login_phone));
        RelativeLayout.LayoutParams mLayoutParamsPhone = new RelativeLayout.LayoutParams(Utils.dip2px(42), Utils.dip2px(42));
        mLayoutParamsPhone.setMargins(marginLift, 0, 0, (int) marginBottom);
        mLayoutParamsPhone.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        mLayoutParamsPhone.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        mPhoneBtn.setLayoutParams(mLayoutParamsPhone);

        Button mWeixinBtn = new Button(activity);
        mWeixinBtn.setBackground(ContextCompat.getDrawable(activity, R.drawable.btn_share_weixin3x));
        RelativeLayout.LayoutParams mLayoutParams1 = new RelativeLayout.LayoutParams(Utils.dip2px(42), Utils.dip2px(42));
        mLayoutParams1.setMargins((marginLift + Utils.dip2px(87)), 0, 0, (int) marginBottom);
        mLayoutParams1.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        mLayoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        mWeixinBtn.setLayoutParams(mLayoutParams1);


        Button mQQBtn = new Button(activity);
        mQQBtn.setBackground(ContextCompat.getDrawable(activity, R.drawable.btn_share_qq3x));
        RelativeLayout.LayoutParams mLayoutParams3 = new RelativeLayout.LayoutParams(Utils.dip2px(42), Utils.dip2px(42));
        mLayoutParams3.setMargins(0, 0, (marginLift + Utils.dip2px(87)), (int) marginBottom);
        mLayoutParams3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        mLayoutParams3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        mQQBtn.setLayoutParams(mLayoutParams3);

        Button mWeiboBtn = new Button(activity);
        mWeiboBtn.setBackground(ContextCompat.getDrawable(activity, R.drawable.btn_share_weibo3x));
        RelativeLayout.LayoutParams mLayoutParams4 = new RelativeLayout.LayoutParams(Utils.dip2px(42), Utils.dip2px(42));
        mLayoutParams4.setMargins(0, Utils.dip2px(400), marginLift, (int) marginBottom);
        mLayoutParams4.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        mLayoutParams4.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        mWeiboBtn.setLayoutParams(mLayoutParams4);

        JVerifyUIConfig uiConfig = new JVerifyUIConfig.Builder()
                .setAuthBGImgPath("main_bg")
                .setNavColor(ContextCompat.getColor(activity, R.color.white))
                .setNavText("登录")
                .setNavTextColor(ContextCompat.getColor(activity, R.color._33))
                .setNavReturnImgPath("back_black")
                .setLogoWidth(75)
                .setLogoHeight(75)
                .setLogoHidden(false)
                .setNumberColor(ContextCompat.getColor(activity, R.color._33))
                .setNumFieldOffsetY(160)
                .setLogBtnText("本机号码一键登录")
                .setLogBtnTextColor(0xffffffff)
                .setLogBtnImgPath("auto_login_btn")
                .setLogBtnHeight(47)
                .setLogBtnTextSize(15)
                .setAppPrivacyOne("悦美隐私政策", FinalConstant.USERAGREMMENT)
                .setAppPrivacyColor(0xff666666, 0xff0085d0)
                .setUncheckedImgPath("umcsdk_uncheck_image")
                .setCheckedImgPath("umcsdk_check_image")
                .setSloganTextColor(0xff999999)
                .setLogoOffsetY(60)
                .setLogoImgPath("ic_launcher")
                .setSloganOffsetY(180)
                .setLogBtnOffsetY(224)
                .setPrivacyState(true)
                .addCustomView(otherLogin, false, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {

                    }
                })
                .addCustomView(mPhoneBtn, true, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {
                        jumpLogin((Activity) activity);
                    }
                })
                .addCustomView(mWeixinBtn, true, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {
                        if (umShareAPI.isInstall((Activity) activity, SHARE_MEDIA.WEIXIN)) {
                            isAutoThird = true;
                            mIsWeixin = true;
                            umShareAPI.doOauthVerify(mActivity, SHARE_MEDIA.WEIXIN, umAuthListener);
                        } else {
                            Toast.makeText(context, "请安装微信客户端", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addCustomView(mQQBtn, true, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {
                        if (umShareAPI.isInstall((Activity) activity, SHARE_MEDIA.QQ)) {
                            isAutoThird = true;
                            umShareAPI.doOauthVerify(mActivity, SHARE_MEDIA.QQ, umAuthListener);
                        } else {
                            Toast.makeText(context, "请安装QQ客户端", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addCustomView(mWeiboBtn, true, new JVerifyUIClickCallback() {
                    @Override
                    public void onClicked(Context context, View view) {
                        if (umShareAPI.isInstall((Activity) activity, SHARE_MEDIA.SINA)) {
                            isAutoThird = true;
                            umShareAPI.doOauthVerify(mActivity, SHARE_MEDIA.SINA, umAuthListener);
                        } else {
                            Toast.makeText(context, "请安装新浪微博客户端", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setPrivacyOffsetY(35).build();
        JVerificationInterface.setCustomUIWithConfig(uiConfig);

        autoAuthLogin((Activity) activity);

        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PHONELOGIN_BAOGUANG), hashMap, new ActivityTypeData("155"));
    }




    /**
     * 授权一键登录
     *
     * @param context
     */
    private void autoAuthLogin(final Activity context) {
        JVerificationInterface.loginAuth(context, true, new VerifyListener() {
            @Override
            public void onResult(int code, String content, String operator) {
                if (code == 6000) {
                    Log.e(TAG, "code=" + code + ", token=" + content + " ,operator=" + operator);
                    Toast.makeText(context, "登录中...", Toast.LENGTH_SHORT).show();
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("loginToken", content);
                    if (!TextUtils.isEmpty(mReferrer)){
                        map.put("referrer",mReferrer);
                    }
                    if (!TextUtils.isEmpty(mReferrerId)){
                        map.put("referrer_id",mReferrerId);
                    }
                    new PhoneLoginApi().getCallBack(context, map, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData s) {
                            if ("1".equals(s.code)) {
                                try {
                                    PhoneLoginBean phoneLoginBean = JSONUtil.TransformSingleBean(s.data, PhoneLoginBean.class);
                                    String user_id = phoneLoginBean.get_id();
                                    Cfg.saveStr(mActivity, FinalConstant.HOME_PERSON_UID, user_id);
                                    Utils.getUserInfoLogin(context, user_id, "1");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(context, s.message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Log.e(TAG, "code=" + code + ", message=" + content);
                    if (code != 6002) {
                        if (code == 6001){
                            Toast.makeText(context, "请检查您的网络状态", Toast.LENGTH_SHORT).show();
                        }
                        jumpLogin(context);
                    } else {
                        if (!isAutoThird) {
                            finish();
                        }
                    }
                }
            }
        }, new AuthPageEventListener() {
            @Override
            public void onEvent(int cmd, String msg) {
                Log.e(TAG, "[onEvent]. [" + cmd + "]message=" + msg);
            }
        });
    }


    private void jumpLogin(Activity context) {
        Log.e(TAG, "jumpLogin ====");
        Intent it550 = new Intent();
        it550.setClass(context, LoginActivity605.class);
        context.startActivity(it550);
        finish();
    }


    private void initShenHe(Activity context) {
        new ShenHeApi().getCallBack(context, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {

            @Override
            public void onSuccess(ShenHeData shenHeData) {
                if (shenHeData != null) {


                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mIsWeixin){
            finish();
        }
        Log.e(TAG,"onRestart");
    }

    private UMAuthListener umAuthListener = new UMAuthListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            Log.e(TAG, "onComplete ==data----" + data.toString());
            getPlatformInfo(umShareAPI, platform, data);
        }

        @Override
        public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

        }

        @Override
        public void onCancel(SHARE_MEDIA share_media, int i) {

        }
    };


    private void getPlatformInfo(UMShareAPI umShareAPI, SHARE_MEDIA platform, final Map<String, String> data) {
        final HashMap<String, Object> hashMap = new HashMap<>();


        umShareAPI.getPlatformInfo(mActivity, platform, new UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {

            }

            @Override
            public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
                Log.e(TAG, "map ---" + map.toString());
                String loginSex = "";
                String loginName = "";
                String fromSite = "";
                String oauthId = "";
                String openid = "";
                String unionid = "";
                String iconurl = "";
                switch (share_media) {
                    case WEIXIN:
                        loginSex = map.get("gender") + "";
                        loginName = map.get("screen_name") + "";
                        fromSite = "weixin";
                        oauthId = map.get("unionid") + "";
                        openid = map.get("openid") + "";
                        unionid = map.get("unionid") + "";
                        iconurl = map.get("profile_image_url") + "";
                        break;
                    case QQ:
                        oauthId = data.get("openid") + "";
                        openid = data.get("openid") + "";
                        loginSex = map.get("gender") + "";
                        iconurl = map.get("iconurl") + "";
                        loginName = map.get("name") + "";

                        if (loginSex.equals("男")) {
                            loginSex = "1";
                        } else {
                            loginSex = "2";
                        }
                        loginName = map.get("screen_name") + "";
                        fromSite = "qq";
                        break;
                    case SINA:
                        oauthId = data.get("uid") + "";
                        String sex = map.get("gender") + "";
                        loginName = map.get("name") + "";
                        iconurl = map.get("iconurl") + "";
                        if (sex.equals("m")) {
                            loginSex = "1";
                        } else {
                            loginSex = "2";
                        }
                        loginName = map.get("screen_name") + "";
                        fromSite = "sina";
                        break;
                }
                hashMap.put("from_site", fromSite);
                hashMap.put("oauth_id", oauthId);
                if (!TextUtils.isEmpty(openid)) {
                    hashMap.put("openid", openid);
                }
                hashMap.put("unionid", unionid);
                hashMap.put("name", loginName);
                hashMap.put("sex", loginSex);
                hashMap.put("avatar", iconurl);
                if (!TextUtils.isEmpty(mReferrer)){
                    hashMap.put("referrer",mReferrer);
                }
                if (!TextUtils.isEmpty(mReferrerId)){
                    hashMap.put("referrer_id",mReferrerId);
                }
                loginHttp(hashMap);
            }

            @Override
            public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {

            }

            @Override
            public void onCancel(SHARE_MEDIA share_media, int i) {

            }
        });
    }


    private void loginHttp(HashMap<String, Object> hashMap) {
        new LoginOtherHttpApi().getCallBack(mActivity, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    UserData userData = JSONUtil.TransformLogin(serverData.data);
                    String id = userData.get_id();
                    Log.e(TAG, "----->" + id);
                    Cfg.saveStr(mActivity, FinalConstant.HOME_PERSON_UID, id);
                    Utils.setUid(id);
                    StatisticalManage.getInstance().growingIO("logon");
                    Utils.getUserInfoLogin(mActivity, id, "2");
                } else {
                    ViewInject.toast(serverData.message);
                }
                finish();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        umShareAPI.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Cfg.saveStr(MyApplication.getContext(), NetWork.IS_SHOW_LOGIN,"0");
    }
}
