package com.module.my.controller.other;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

import com.module.my.controller.activity.ScrollViewListener;

@SuppressLint("Override")
public class FlexibleScrollView extends ScrollView {
    public static final String TAG="FlexibleScrollView";
    private static final float DEFAULT_LOAD_FACTOR = 3.0F;
    private View mHeaderView;
    private int mOriginHeight;
    private int mZoomedHeight;
    private ScrollViewListener scrollViewListener = null;

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    public FlexibleScrollView(Context context) {
        super(context);
    }

    public FlexibleScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FlexibleScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (scrollViewListener != null) {
            scrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

        if(null != mHeaderView) {
            if(isTouchEvent && deltaY < 0) {
                mZoomedHeight = mHeaderView.getHeight();
                if (mZoomedHeight < 676){
                    mHeaderView.getLayoutParams().height += Math.abs(deltaY / DEFAULT_LOAD_FACTOR);
                    mHeaderView.requestLayout();
                }
            }
        }
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        if(null != mHeaderView && 0 != mOriginHeight && 0 != mZoomedHeight) {

            int action = ev.getAction();
            if(MotionEvent.ACTION_UP == action || MotionEvent.ACTION_CANCEL == action) {
                resetHeaderViewHeight();
            }
            if (MotionEvent.ACTION_DOWN == action){
                return false;
            }

        }

        return super.onTouchEvent(ev);
    }


    public void setHeaderView(View headerView) {
        this.mHeaderView = headerView;
        updateHeaderViewHeight();
    }

    private void updateHeaderViewHeight() {
        mOriginHeight = null == mHeaderView ? 0 : mHeaderView.getHeight();
        if(0 == mOriginHeight && null != mHeaderView) {
            post(new Runnable() {
                @Override
                public void run() {
                    mOriginHeight = mHeaderView.getHeight();
                }
            });
        }
    }



    @SuppressLint("NewApi")
    private void resetHeaderViewHeight() {
        if(mHeaderView.getLayoutParams().height != mOriginHeight) {
            ValueAnimator valueAnimator = ValueAnimator.ofInt(mZoomedHeight, mOriginHeight);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    mHeaderView.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                    mHeaderView.requestLayout();
                }
            });
            valueAnimator.setDuration(200);
            valueAnimator.start();
        }
    }


}
