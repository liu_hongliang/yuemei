package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.model.api.SaveAiliMessageApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.ShouKuan;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.KeyBoardUtils;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountShoukuanActivity extends YMBaseActivity {
    public static final String TAG="AccountShoukuanActivity";
    @BindView(R.id.wallet_shoukuan_title)
    CommonTopBar mWalletShoukuanTitle;
    @BindView(R.id.shoukuan_name)
    EditText mShoukuanName;  //姓名
    @BindView(R.id.shoukuan_cardid)
    EditText mShoukuanCardid;  //支付宝账号
//    @BindView(R.id.shoukuan_tip)
//    TextView mShoukuanTip;
    @BindView(R.id.shoukuan_btn)
    Button mShoukuanBtn;
    @BindView(R.id.shoukuan_select)
    RadioGroup mShoukuanSelect;
    @BindView(R.id.shoukuan_zhifubao)
    RadioButton mShoukuanzhifubao;
    @BindView(R.id.shoukuan_bankcard)
    RadioButton mShoukuanbankcard;
    @BindView(R.id.shoukuan_type)
    TextView mShoukuanType;
    private boolean isUnBind;
    private Context mContext;
    private String mSk_name;
    private String mSk_account;
    private String mType="0";
    private int mSk_account_type;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_account_shoukuan;
    }

    @Override
    protected void initView() {
        mContext=AccountShoukuanActivity.this;
        Intent intent = getIntent();
        mSk_name = intent.getStringExtra("sk_name");
        mSk_account =intent.getStringExtra("sk_account");
        mSk_account_type = intent.getIntExtra("sk_account_type", -1);
        if (TextUtils.isEmpty(mSk_name) && TextUtils.isEmpty(mSk_account)){
            isUnBind=false;
            mShoukuanType.setVisibility(View.GONE);
            mShoukuanSelect.setVisibility(View.VISIBLE);
        }else {
            isUnBind=true;
            setEditTextState(mShoukuanName,false);
            setEditTextState(mShoukuanCardid,false);
            mShoukuanBtn.setText("解绑收款账号");
            mShoukuanType.setVisibility(View.VISIBLE);
            mShoukuanSelect.setVisibility(View.GONE);
            switch (mSk_account_type){
                case 0:
                    mShoukuanType.setText("支付宝");
                    break;
                case 1:
                    mShoukuanType.setText("银行卡");
                    break;
            }
        }
        mWalletShoukuanTitle.setBottomLineVisibility(View.GONE);
        mWalletShoukuanTitle.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                //客服跳转
                Intent it = new Intent();
                it.setClass(mContext, OnlineKefuWebActivity.class);
                it.putExtra("link", "/service/zxzx/");
                it.putExtra("title", "在线客服");
                startActivity(it);
            }
        });

        mShoukuanSelect.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.shoukuan_zhifubao:
                        mType="0";
                        break;
                    case R.id.shoukuan_bankcard:
                        mType="1";
                        break;
                }
            }
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        TextChange textChange = new TextChange();
        mShoukuanName.addTextChangedListener(textChange);
        mShoukuanCardid.addTextChangedListener(textChange);
        if (!TextUtils.isEmpty(mSk_name)){
            String substring = mSk_name.substring(1);
            mShoukuanName.setText("*"+substring);
        }else {
            String real_name = Cfg.loadStr(mContext, "real_name", "");
            if (!TextUtils.isEmpty(real_name)){
                setEditTextState(mShoukuanName,false);
                mShoukuanName.setText(real_name);
            }else {
                setEditTextState(mShoukuanName,true);
                mShoukuanName.setText(mSk_name);
            }

        }
        mShoukuanCardid.setText(mSk_account);

    }

    @OnClick(R.id.shoukuan_btn)
    public void onViewClicked() {
        if (!isUnBind){
            String name = mShoukuanName.getText().toString().trim();
            String account = mShoukuanCardid.getText().toString().trim();
            if ("1".equals(mType)){
                if (Utils.checkBankCard(account)){
                    saveMessage(name,account);
                }else {
                    mFunctionManager.showShort("银行卡号输入有误！");
                }
            }else {
                if (Utils.isMobile(account) || Utils.emailFormat(account)){
                    saveMessage(name,account);
                }else {
                    mFunctionManager.showShort("请输入手机号或邮箱！");
                }

            }
        }else {
            final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "解除绑定后，如果有正在提现的申请将支付失败，确认要解绑？", "取消", "解除绑定");
            yueMeiDialog.setCanceledOnTouchOutside(false);
            yueMeiDialog.show();
            yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                @Override
                public void leftBtnClick() {
                    yueMeiDialog.dismiss();
                }

                @Override
                public void rightBtnClick() {
                    saveMessage("","");
                    yueMeiDialog.dismiss();
                }
            });
        }

    }

    private void setEditTextState(EditText editText,boolean flag){
        editText.setCursorVisible(flag);
        editText.setFocusable(flag);
        editText.setFocusableInTouchMode(flag);
    }


    private void saveMessage(final String name, final String account){
        Map<String,Object> maps=new HashMap<>();
        maps.put("real_name", name);
        maps.put("alipay", account);
        maps.put("type",mType);
        new SaveAiliMessageApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    if (TextUtils.isEmpty(name) && TextUtils.isEmpty(account)){
                        mShoukuanBtn.setText("保存");

                        mFunctionManager.showShort("解绑成功！");
                        isUnBind=false;
                        String real_name = Cfg.loadStr(mContext, "real_name", "");
                        if (!TextUtils.isEmpty(real_name)){
                            setEditTextState(mShoukuanName,false);
                            mShoukuanName.setText(real_name);
                        }else {
                            setEditTextState(mShoukuanName,true);
                            mShoukuanName.setText("");
                        }
                        setEditTextState(mShoukuanCardid,true);
                        mShoukuanCardid.setText("");
                        mShoukuanType.setVisibility(View.GONE);
                        mShoukuanSelect.setVisibility(View.VISIBLE);
                    }else {
                        ShouKuan.DataBean data = JSONUtil.TransformShouKuan(serverData.data);
                        String alipay = data.getAlipay();
                        String real_name = data.getReal_name();
                        mFunctionManager.showShort("保存成功！");
                        mShoukuanType.setVisibility(View.VISIBLE);
                        mShoukuanSelect.setVisibility(View.GONE);
                        switch (mType){
                            case "0":
                                mShoukuanType.setText("支付宝");
                                break;
                            case "1":
                                mShoukuanType.setText("银行卡");
                                break;
                        }
                        String substring = real_name.substring(1);
                        mShoukuanName.setText("*"+substring);
                        mShoukuanCardid.setText(alipay);
                        setEditTextState(mShoukuanName,false);
                        setEditTextState(mShoukuanCardid,false);
                        mShoukuanBtn.setText("解绑收款账号");
                        KeyBoardUtils.hideKeyBoard(mContext, mShoukuanName);
                        KeyBoardUtils.hideKeyBoard(mContext, mShoukuanCardid);
                        isUnBind=true;
                    }
                    } else {
                    mFunctionManager.showShort(serverData.message);
                    }
            }
        });
    }


    class TextChange implements TextWatcher {
        @Override
        public void afterTextChanged(Editable arg0) {

        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
        @Override
        public void onTextChanged(CharSequence cs, int start, int before,
                                  int count) {
            boolean Sign1 = mShoukuanName.getText().length() > 0;
            boolean Sign2 = mShoukuanCardid.getText().length() > 0;
            if (Sign1&Sign2) {
                mShoukuanBtn.setEnabled(true);
                mShoukuanBtn.setBackgroundResource(R.drawable.renzheng_background);
            }
            //在layout文件中，对Button的text属性应预先设置默认值，否则刚打开程序的时候Button是无显示的
            else {
                mShoukuanBtn.setEnabled(false);
                mShoukuanBtn.setBackgroundResource(R.drawable.shoukuan_save_background);
            }
        }
    }
}
