/**
 *
 */
package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;

/**
 * 我的代金劵
 *
 * @author Robin
 */
public class MyDaijinjuanActivity extends YMBaseActivity {

    private final String TAG = "MyDaijinjuanActivity";

    private LinearLayout contentWeb;

    private CommonTopBar mTop;

    private WebView docDetWeb;

    private Context mContext;

    public JSONObject obj_http;

    private String uid;
    private String link;
    String url = "";
    private BaseWebViewClientMessage mBaseWebViewClientMessage;
    private String linkTemp;
    private String mFlag = "1";
    private WebSignData mAddressAndHead;
    private String mUrl1;


    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void initWebview() {
        docDetWeb = new WebView(mContext);
        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(mBaseWebViewClientMessage);
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        contentWeb.addView(docDetWeb);
    }

    public void showWebDetail(String urlStr) {
        Log.e(TAG, urlStr);
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");
                    String docname = URLDecoder.decode(
                            obj.getString("docname"), "utf-8");

                    Intent it = new Intent();
                    it.setClass(mContext, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", docname);
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");

                Intent it = new Intent();
                it.setClass(mContext, DiariesAndPostsActivity.class);
                it.putExtra("url", link);
                it.putExtra("qid", qid);
                startActivity(it);
            }


            if (obj.getString("type").equals("6202")) {

                String link = obj.getString("link");
                Log.e(TAG, "link == " + link);

                String[] split = link.split("/");
                mFlag=split[4]+"";
                mUrl1 = FinalConstant.baseUrl+split[1]+"/"+split[2]+"/";

                contentWeb.removeAllViews();
                docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                contentWeb.addView(docDetWeb);
                LodUrl(mUrl1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, e.toString());
        }

    }

    /**
     * 加载web
     */
    public void LodUrl(String url) {
        mBaseWebViewClientMessage.startLoading();
        if ("1".equals(mFlag)) {
            mAddressAndHead = SignUtils.getAddressAndHead(url);
        } else {
            HashMap<String, Object> map2 = new HashMap<>();
            map2.put("flag", mFlag);
            mAddressAndHead = SignUtils.getAddressAndHead(url, map2);
        }

        Log.d(TAG, "url===>" + mAddressAndHead.getUrl());
        if (null != docDetWeb) {
            docDetWeb.loadUrl(mAddressAndHead.getUrl(), mAddressAndHead.getHttpHeaders());
        }
    }

    public void onResume() {
        super.onResume();
        StatService.onResume(this);
        MobclickAgent.onResume(this);
        TCAgent.onResume(this);
        if ("1".equals(mFlag)) {
            LodUrl(url);
        } else {
            LodUrl(url + "flag/" + mFlag + "/");
        }
    }

    public void onPause() {
        super.onPause();
        StatService.onPause(this);
        MobclickAgent.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (docDetWeb != null) {
            docDetWeb.removeAllViews();
            docDetWeb.destroy();
            docDetWeb = null;
            contentWeb.removeAllViews();
            contentWeb = null;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.acty_my_daijinjuan;
    }

    @Override
    protected void initView() {
        mContext = MyDaijinjuanActivity.this;
        uid = Utils.getUid();

        contentWeb = findViewById(R.id.wan_beautifu_linearlayout3);
        mTop = findViewById(R.id.my_daijinjuan_top);

        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTop.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        Cfg.saveStr(mContext, "mydaijinjuan", "1");
        mBaseWebViewClientMessage = new BaseWebViewClientMessage(MyDaijinjuanActivity.this);
        mBaseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });
        initWebview();
        linkTemp = getIntent().getStringExtra("link");

        url = FinalConstant.MY_DAIJINJUAN;
        LodUrl(url);

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        MyDaijinjuanActivity.this.finish();
                    }
                });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent();
                it.setClass(mContext, TianMaDeJuanActivity.class);
                startActivity(it);
                overridePendingTransition(R.anim.activity_open, 0);
            }
        });
    }

    @Override
    protected void initData() {

    }


}
