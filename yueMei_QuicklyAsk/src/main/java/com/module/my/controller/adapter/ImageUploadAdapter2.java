package com.module.my.controller.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.quicklyask.activity.R;
import com.quicklyask.view.ProcessImageView;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2017/7/3.
 */

public class ImageUploadAdapter2 extends BaseAdapter {
    private static final String TAG = "ImageUploadAdapter2";
    private final Context mContext;
    private final ImageOptions imageOptions;
    private final Animation ain;
    private final String mImgCover;
    private HashMap<String, Object> mSameData;
    private HashMap<String, String> mErrorImg;
    private LayoutInflater listContainer;

    private boolean shape;
    private ArrayList<String> mResults = new ArrayList<>();
    private HashMap<String, ProcessImageView> imgs = new HashMap();

    public ImageUploadAdapter2(Context context, ArrayList<String> mResults, HashMap<String, Object> mSameData, HashMap<String, String> mErrorImg, String mImgCover) {
        this.mContext = context;
        this.mResults = mResults;
        this.mSameData = mSameData;
        this.mErrorImg = mErrorImg;
        this.mImgCover = mImgCover;
        listContainer = LayoutInflater.from(context);
        ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
        imageOptions = new ImageOptions.Builder()
                .setConfig(Bitmap.Config.ARGB_8888)
                .setIgnoreGif(false)
                .setAnimation(ain)
                .setCrop(true)//是否对图片进行裁剪
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setUseMemCache(true)
                .setLoadingDrawableId(R.drawable.radius_gray80)
                .setFailureDrawableId(R.drawable.radius_gray80)
                .build();
    }

    public int getCount() {
        if (mResults.size() < 9) {
            return mResults.size() + 2;
        } else {
            return mResults.size() + 1;
        }
    }

    public Object getItem(int arg0) {

        return mResults.get(arg0);
    }

    public long getItemId(int arg0) {

        return arg0;
    }

    /**
     * ListView Item设置
     */
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.e("TAG", "getView == .........");
        // 自定义视图
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            // 获取list_item布局文件的视图
            convertView = listContainer.inflate(R.layout.item_published_grida, null);
            // 获取控件对象
            holder.image = convertView.findViewById(R.id.item_grida_image);
            holder.bt = convertView.findViewById(R.id.item_grida_bt);
            holder.rlShangchuan = convertView.findViewById(R.id.rl_shangchuan);
            holder.rlUploadImage = convertView.findViewById(R.id.rl_upload_image);
            holder.rlUploadVideo = convertView.findViewById(R.id.rl_upload_video);
            // 设置控件集到convertView
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (position == mResults.size()) {              //视频上传按钮
            holder.image.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI

            holder.rlUploadVideo.setVisibility(View.VISIBLE);

            holder.rlShangchuan.setVisibility(View.GONE);         //取消按钮隐藏

        } else if (position == mResults.size() + 1) {      //如果是最后一个 + 1 个（图片上传按钮）
            holder.image.startHua(ProcessImageView.WANC_HENG, 100);      //设置图片UI

            holder.rlUploadImage.setVisibility(View.VISIBLE);

            holder.rlShangchuan.setVisibility(View.GONE);         //取消按钮隐藏
            if (position == 9) {                        //如果是第10个，那么添加按钮可以隐藏了
                holder.image.setVisibility(View.GONE);
            }
        } else {            //不是最后一个添加本地图片
            String imgUrl = mResults.get(position);     //本地路径

            x.image().bind(holder.image, imgUrl, imageOptions);

            if (mSameData.get(imgUrl) != null ) {            //说明是已经上传过
                if (mImgCover.equals(imgUrl)) {
                    Log.e(TAG, "11111111");
                    holder.image.startHua(ProcessImageView.WANC_HENG, 100, false, true);
                } else {
                    holder.image.startHua(ProcessImageView.WANC_HENG, 100);
                }
            } else {                             //没有上传过的(上传失败/新增图片)
                Log.e("TAG", "mErrorImg.get(imgUrl) == " + mErrorImg.get(imgUrl));
                if (mErrorImg.get(imgUrl) != null) {          //上传失败的图片
                    Log.e("TAG", "失败图片 == " + ProcessImageView.SHI_BAI);
                    holder.image.startHua(ProcessImageView.SHI_BAI, 0);

                } else {
                    Log.e("TAG", "新增图片");
                    holder.image.startHua(ProcessImageView.DENG_DAI, 0);         //新增图片
                }
            }
            imgs.put(mResults.get(position), holder.image);
        }

        holder.bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemDeleteListener.onDeleteClick(position);
            }
        });

        return convertView;
    }

    public boolean isShape() {
        return shape;
    }

    public void setShape(boolean shape) {
        this.shape = shape;
    }

    public HashMap<String, ProcessImageView> getProcessImage() {
        return imgs;
    }

    public class ViewHolder {
        ProcessImageView image;
        RelativeLayout bt;
        RelativeLayout rlShangchuan;
        RelativeLayout rlUploadImage;
        RelativeLayout rlUploadVideo;
    }

    /**
     * 删除按钮的监听接口
     */
    public interface onItemDeleteListener {
        void onDeleteClick(int i);
    }

    private onItemDeleteListener mOnItemDeleteListener;

    public void setOnItemDeleteClickListener(onItemDeleteListener mOnItemDeleteListener) {
        this.mOnItemDeleteListener = mOnItemDeleteListener;
    }
}
