package com.module.my.controller.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.bean.ChatParmarsData;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.my.controller.adapter.TypeProblemAdapter;
import com.module.my.model.api.TypeProblemApi;
import com.module.my.model.bean.TypeProblemData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 提问帖问题类型页面
 *
 * @author 裴成浩
 */
public class TypeProblemActivity extends YMBaseActivity {

    @BindView(R.id.type_problem_background)
    ImageView mBackground;
    @BindView(R.id.type_problem_back)
    FrameLayout mBack;
    @BindView(R.id.type_problem_title)
    TextView mTitle;
    @BindView(R.id.type_problem_recycler)
    RecyclerView mRecycler;

    private final String TAG = "TypeProblemActivity";
    private TypeProblemApi mTypeProblemApi;
    private TypeProblemAdapter mTypeProblemAdapter;
    private PageJumpManager pageJumpManager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_type_problem;
    }

    @Override
    protected void initView() {
        pageJumpManager = new PageJumpManager(mContext);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mBack.getLayoutParams();
        params.topMargin = statusbarHeight;
        mBack.setLayoutParams(params);
    }

    @Override
    protected void initData() {
        mTypeProblemApi = new TypeProblemApi();
        loadData();
    }

    /**
     * 加载数据
     */
    private void loadData() {
        mTypeProblemApi.getCallBack(mContext, mTypeProblemApi.getHashMap(), new BaseCallBackListener<List<TypeProblemData>>() {
            @Override
            public void onSuccess(List<TypeProblemData> serverData) {

                ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                scrollLinearLayoutManager.setScrollEnable(false);
                mRecycler.setLayoutManager(scrollLinearLayoutManager);
                mTypeProblemAdapter = new TypeProblemAdapter(R.layout.item_type_problem, serverData);
                mRecycler.setAdapter(mTypeProblemAdapter);

                //点击事件
                mTypeProblemAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        if (Utils.noLoginChat()) {
                            toChatActivity(position);
                        } else {
                            if (Utils.isLoginAndBind(mContext)) {
                                toChatActivity(position);
                            }
                        }

                    }
                });
            }
        });
    }

    private void toChatActivity(int position) {
        TypeProblemData data = mTypeProblemAdapter.getItem(position);
        Utils.tongjiApp(mContext, data.getEvent_name(), data.getEvent_pos(), data.getEvent_others(), "0");

        if ("3".equals(data.getIs_rongyun())) {
            ChatParmarsData chatParmarsData = new ChatParmarsData.ChatParmarsBuilder()
                    .setDirectId(data.getHos_userid())
                    .setObjId("0")
                    .setObjType("0")
                    .setYmClass("0")
                    .setYmId("0")
                    .build();
            pageJumpManager.jumpToChatBaseActivity(chatParmarsData);
        } else {
            if (Utils.isLoginAndBind(mContext)) {
                Intent intent = new Intent(mContext, WriteQuestionActivity647.class);
                intent.putExtra("cateid", data.getTag_id());
                startActivity(intent);
                onBackPressed();
            }
        }
    }

    @OnClick(R.id.type_problem_back)
    public void onClick() {
        onBackPressed();
    }

}
