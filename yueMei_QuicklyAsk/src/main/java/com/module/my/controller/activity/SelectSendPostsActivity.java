package com.module.my.controller.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.other.activity.InitiateVoteActivity;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WriteNoteManager;

import org.xutils.common.util.DensityUtil;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 选择发帖 体验或提问
 *
 * @author Rubin
 */
public class SelectSendPostsActivity extends YMBaseActivity {

    private final String TAG = "SelectSendPostsActivity";

    @BindView(R.id.send_to_post_cancel_rly)
    RelativeLayout cancelBt;
    @BindView(R.id.sen_to_posts_message)
    RelativeLayout tiwenRly;
    @BindView(R.id.send_to_posts_experience)
    RelativeLayout experRly;
    @BindView(R.id.send_to_posts_suibian)
    RelativeLayout suibianRly;
    @BindView(R.id.all_content)
    LinearLayout contentLy;
    @BindView(R.id.setting_iv_tel2)
    ImageView ivTel2;
    @BindView(R.id.initiate_vote_rl)
    RelativeLayout initiate_vote_rl;
    @BindView(R.id.iv_initiate_vote)
    ImageView iv_initiate_vote;

    private String cateid;

    @Override
    protected int getLayoutId() {
        return R.layout.acty_sen_to_post;
    }

    @Override
    protected void initView() {
        cateid = getIntent().getStringExtra("cateid");


        //加载gif图
        Glide.with(mContext)
                .load(R.drawable.write_git_diary)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(DensityUtil.dip2px(80),DensityUtil.dip2px(80))
                .error(R.drawable.home_other_placeholder)
                .into(ivTel2);
        //加载gif图
        Glide.with(mContext)
                .load(R.drawable.vote)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .override(DensityUtil.dip2px(80),DensityUtil.dip2px(80))
                .error(R.drawable.home_other_placeholder)
                .into(iv_initiate_vote);

        //关闭按钮
        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //向医生提问点击
        tiwenRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                            if (Utils.isLoginAndBind(mContext)) {

                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "bbs"));

                mFunctionManager.goToActivity(TypeProblemActivity.class);
                onBackPressed();
//                            }
            }
        });

        //写日记
        experRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.tongjiApp(mContext, "forum_write_alert", "2", "forum", "0");
                if (Utils.isLoginAndBind(mContext)) {
                    WriteNoteManager.getInstance(mContext).ifAlert(null);
                    onBackPressed();
                }

            }
        });

        //随便聊聊
        suibianRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isLoginAndBind(mContext)) {
                    Utils.tongjiApp(mContext, "forum_write_alert", "3", "forum", "");
                    Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
//                                it3.putExtra("from", "SelectSendPostsActivity");
                    it3.putExtra("source", "SelectSendPostsActivity");
                    it3.putExtra("cateid", cateid + ",1090");
                    startActivity(it3);
                    onBackPressed();
                }
            }
        });
        //发起投票
        initiate_vote_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                            if (Utils.isLoginAndBind(mContext)) {
//                                发起投票--发起按钮点击**（提问入口页面）**
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.LAUNCH_ENTRANCE_VOTE_CLICK), new ActivityTypeData("170"));
                Intent intent = new Intent(SelectSendPostsActivity.this, InitiateVoteActivity.class);
                intent.putExtra("from","SelectSendPostsActivity");
                startActivity(intent);
                onBackPressed();
//                            }
            }
        });
    }

    @Override
    protected void initData() {
        getUserData();
    }

    /**
     * 获取发帖人信息页
     */
    private void getUserData() {
        Map<String, Object> keyValues = new HashMap<>();

        keyValues.put("uid", Utils.getUid());
        keyValues.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
//                    tiwenRly.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
////                            if (Utils.isLoginAndBind(mContext)) {
//
//                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "bbs"));
//
//                            mFunctionManager.goToActivity(TypeProblemActivity.class);
//                            onBackPressed();
////                            }
//                        }
//                    });
//
//                    //写日记
//                    experRly.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Utils.tongjiApp(mContext, "forum_write_alert", "2", "forum", "0");
//                            if (Utils.isLoginAndBind(mContext)) {
//                                WriteNoteManager.getInstance(mContext).ifAlert(null);
//                                onBackPressed();
//                            }
//
//                        }
//                    });
//
//                    //随便聊聊
//                    suibianRly.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            if (Utils.isLoginAndBind(mContext)) {
//                                Utils.tongjiApp(mContext, "forum_write_alert", "3", "forum", "");
//                                Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
////                                it3.putExtra("from", "SelectSendPostsActivity");
//                                it3.putExtra("source", "SelectSendPostsActivity");
//                                it3.putExtra("cateid", cateid + ",1090");
//                                startActivity(it3);
//                                onBackPressed();
//                            }
//                        }
//                    });
//                    initiate_vote_rl.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
////                            if (Utils.isLoginAndBind(mContext)) {
////                                发起投票--发起按钮点击**（提问入口页面）**
//                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.LAUNCH_ENTRANCE_VOTE_CLICK), new ActivityTypeData("170"));
//                            Intent intent = new Intent(SelectSendPostsActivity.this, InitiateVoteActivity.class);
//                            intent.putExtra("from","SelectSendPostsActivity");
//                            startActivity(intent);
//                            onBackPressed();
////                            }
//                        }
//                    });
                } else {
//                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
