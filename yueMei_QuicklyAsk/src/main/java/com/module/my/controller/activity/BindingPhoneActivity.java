package com.module.my.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.module.MainTableActivity;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.SecurityCodeApi;
import com.module.commonview.module.api.SendEMSApi;
import com.module.commonview.view.CommonTopBar;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.CallAndSecurityCodeApi;
import com.module.home.view.LoadingProgress;
import com.module.my.controller.other.ActivityCollector;
import com.module.my.view.view.AutoLoginPop;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.GetPhoneCodePopWindow;
import com.quicklyask.view.YueMeiDialog;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

import cn.jiguang.verifysdk.api.JVerificationInterface;

/**
 * 绑定手机
 * <p>
 * Created by dwb on 16/7/7.
 */
public class BindingPhoneActivity extends BaseActivity {

    private final String TAG = "BindingPhoneActivity";
    private BindingPhoneActivity mContext;

    @BindView(id = R.id.bindingphone_top)
    private CommonTopBar mTop;// 返回


    @BindView(id = R.id.no_pass_login_phone_et)
    private EditText phoneNumberEt;
    @BindView(id = R.id.no_pass_login_code_et)
    private EditText codeEt;
    @BindView(id = R.id.no_pass_login_bt, click = true)
    private Button loginBt;
    @BindView(id = R.id.yuemei_auto_bind, click = true)
    private TextView autoBind;

    @BindView(id = R.id.no_pass_yanzheng_code_rly)
    private RelativeLayout sendEMSRly;
    @BindView(id = R.id.yanzheng_code_tv)
    private TextView emsTv;

    private String phone;
    private String codeStr;

    private static final int BACK2 = 3;

    private String uid;

    @BindView(id = R.id.nocde_message_tv, click = true)
    private TextView noCodeTv;// 没收到验证码

    private PopupWindows yuyinCodePop;
    @BindView(id = R.id.order_time_all_ly)
    private LinearLayout allcontent;

    private GetPhoneCodePopWindow phoneCodePop;


    private LoadingProgress mDialog;
    private String mFlag;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_bindingphone);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = BindingPhoneActivity.this;

        String loadStr = Cfg.loadStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(loadStr)) {
            ExposureLoginData exposureLoginData = new Gson().fromJson(loadStr, ExposureLoginData.class);
            hashMap.put("referrer", exposureLoginData.getReferrer());
            hashMap.put("referrer_id", exposureLoginData.getReferrer_id());
        }else{
            hashMap.put("referrer", "0");
            hashMap.put("referrer_id", "0");
        }

        Log.e(TAG, "hashMap == " + hashMap.toString());
        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PHONEBIND_BAOGUANG), hashMap, new ActivityTypeData("153"));
        uid = Utils.getUid();
        mFlag = getIntent().getStringExtra("flag");
        mDialog = new LoadingProgress(mContext);

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout
                .setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

                    @Override
                    public void onSildingFinish() {
                        BindingPhoneActivity.this.finish();
                    }
                });

        // loginBt.setPressed(true);
        // loginBt.setClickable(false);

        sendEMSRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String phone = phoneNumberEt.getText().toString();
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (phone.length() > 0) {

                    if (ifPhoneNumber()) {
                        sendEMS();
                        noCodeTv.setVisibility(View.VISIBLE);
                        noCodeTv.setText(Html.fromHtml("<u>" + "没收到验证码？"
                                + "</u>"));
                    } else {
                        ViewInject.toast("请输入正确的手机号");
                    }
                } else {
                    ViewInject.toast("请输入手机号");
                }
            }
        });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

    }

    private boolean ifPhoneNumber() {
        String textPhn = phoneNumberEt.getText().toString();
        return Utils.isMobile(textPhn);
    }

    void sendEMS() {
        phone = phoneNumberEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phone);
        maps.put("flag", "2");
        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {

                if ("1".equals(s.code)) {
                    if (!"".equals(s.is_alert_message) && s.is_alert_message != null) {
                        showDialog(s.is_alert_message);
                    }
                    ViewInject.toast("请求成功");
                    sendEMSRly.setClickable(false);
                    codeEt.requestFocus();

                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @Override
                        public void onTick(long millisUntilFinished) {
                            sendEMSRly
                                    .setBackgroundResource(R.drawable.biankuang_hui);
                            emsTv.setTextColor(getResources().getColor(
                                    R.color.button_zi));
                            emsTv.setText("(" + millisUntilFinished
                                    / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            sendEMSRly
                                    .setBackgroundResource(R.drawable.shape_bian_ff5c77);
                            emsTv.setTextColor(getResources().getColor(
                                    R.color.button_bian_hong1));
                            sendEMSRly.setClickable(true);
                            emsTv.setText("重发验证码");
                        }
                    }.start();
                } else {
                    ViewInject.toast(s.message);
                }
            }
        });
    }

    void initCode1() {
        codeStr = codeEt.getText().toString();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("phone", phone);
        maps.put("code", codeStr);
        maps.put("flag", "2");
        new SecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                mDialog.stopLoading();
                if ("1".equals(serverData.code)) {
                    Utils.getUserInfoLogin(mContext, uid, "1");

                } else {
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            showDialog();
            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    private void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = View.inflate(mContext, R.layout.dialog_bind_phone, null);
        builder.setView(view);
        builder.setPositiveButton("绑定手机号", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setNegativeButton("返回", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCollector.finishAll();
                if (!"".equals(mFlag) && mFlag != null) {
//                    MainTableActivity.tabHost.setCurrentTab(4);
////                    MainTableActivity.bnBottom[0].setChecked(false);
////                    MainTableActivity.bnBottom[1].setChecked(false);
////                    MainTableActivity.bnBottom[2].setChecked(false);
////                    MainTableActivity.bnBottom[3].setChecked(false);
////                    MainTableActivity.bnBottom[4].setChecked(true);
                    MainTableActivity.mainBottomBar.setCheckedPos(4);
                }
            }
        }).create().show();
    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = View.inflate(mContext, R.layout.dialog_bind_phone, null);
        TextView textView = view.findViewById(R.id.dialog_exit_content_tv);
        textView.setText(message);
        builder.setView(view);
        builder.setPositiveButton("我知道啦", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }


    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.no_pass_login_bt:// 登录
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                codeStr = codeEt.getText().toString();
                phone = phoneNumberEt.getText().toString().trim();
                if (phone.length() > 0) {
                    if (codeStr.length() > 0) {
                        mDialog.startLoading();
                        initCode1();
                    } else {
                        ViewInject.toast("请输入验证码！");
                    }
                } else {
                    ViewInject.toast("请输入手机号！");
                }
                break;
            case R.id.nocde_message_tv:// 没收到验证码
                yuyinCodePop = new PopupWindows(mContext, allcontent);
                yuyinCodePop.showAtLocation(allcontent, Gravity.BOTTOM, 0, 0);

                Glide.with(mContext).load(FinalConstant.TUXINGCODE).into(codeIv);
                break;
            case R.id.yuemei_auto_bind:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ONE_CLICK_BINDLOGINPHONE,"1"),new ActivityTypeData("153"));
//                if (checkNetWorkForLocation()){
//                    final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "您需要在“设置”中为该应用打开移动数据", "取消", "确定");
//                    yueMeiDialog.setCanceledOnTouchOutside(false);
//                    yueMeiDialog.show();
//                    yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
//                        @Override
//                        public void leftBtnClick() {
//                            yueMeiDialog.dismiss();
//                        }
//
//                        @Override
//                        public void rightBtnClick() {
//                            yueMeiDialog.dismiss();
//                            Intent localIntent = new Intent();
//                            localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            if (Build.VERSION.SDK_INT >= 9) {
//                                localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
//                                localIntent.setData(Uri.fromParts("package", getPackageName(), null));
//                            } else if (Build.VERSION.SDK_INT <= 8) {
//                                localIntent.setAction(Intent.ACTION_VIEW);
//
//                                localIntent.setClassName("com.android.settings",
//                                        "com.android.settings.InstalledAppDetails");
//
//                                localIntent.putExtra("com.android.settings.ApplicationPkgName", getPackageName());
//                            }
//                            startActivity(localIntent);
//                        }
//                    });
//                }else {
                    boolean verifyEnable = JVerificationInterface.checkVerifyEnable(this);
                    if (!verifyEnable) {
                        showJumpPop();//不符合一键登录网络状态
                    } else {
                        Utils.jumpBindingPhone(mContext);
                        finish();
                    }
//                }
                break;

        }
    }


    /**
     * 未开启流量弹窗
     */
    private void showJumpPop() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = 0.5f;
        getWindow().setAttributes(lp);
        AutoLoginPop chatJumpPopwindow = new AutoLoginPop(mContext);
        chatJumpPopwindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.alpha = 1f;
                getWindow().setAttributes(lp);
            }
        });
        chatJumpPopwindow.showAtLocation(allcontent, Gravity.CENTER, 0, 0);
    }

    EditText codeEt1;
    ImageView codeIv;

    /**
     * 获取手机号 并验证
     *
     * @author dwb
     */
    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.pop_yuyincode,
                    null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            Button cancelBt = view.findViewById(R.id.cancel_bt);
            Button tureBt = view.findViewById(R.id.zixun_bt);
            codeEt1 = view.findViewById(R.id.no_pass_login_code_et);
            codeIv = view.findViewById(R.id.yuyin_code_iv);

            RelativeLayout rshCodeRly = view
                    .findViewById(R.id.no_pass_yanzheng_code_rly);

            rshCodeRly.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Glide.with(BindingPhoneActivity.this).load(FinalConstant.TUXINGCODE).into(codeIv);
                }
            });

            tureBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String codes = codeEt1.getText().toString();
                    if (codes.length() > 1) {
                        yanzhengCode(codes);
                    } else {
                        ViewInject.toast("请输入图中数字");
                    }
                }
            });

            cancelBt.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    void yanzhengCode(String codes) {
        String phones = phoneNumberEt.getText().toString().trim();
        Map<String, Object> maps = new HashMap<>();
        maps.put("phone", phones);
        maps.put("code", codes);
        maps.put("flag", "bundling");
        new CallAndSecurityCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    yuyinCodePop.dismiss();
                    ViewInject.toast("正在拨打您的电话，请注意接听");
                } else {
                    ViewInject.toast("数字错误，请重新输入");
                }
            }

        });
    }

    public  boolean checkNetWorkForLocation() {

        /**
         *  判断应用权限管理中该应用是否打开了允许使用网络的权限
         *  -1: 没有打开  0: 已经打开
         */
        int permission = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.INTERNET);

        if(permission == 0) {
            return true;
        } else {
            return false;
        }
    }


    public void onResume() {
        super.onResume();
        ActivityCollector.addActivity(BindingPhoneActivity.this);
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}