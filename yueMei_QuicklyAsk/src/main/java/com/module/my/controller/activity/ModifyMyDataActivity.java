package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.TimePickerView;
import com.bumptech.glide.Glide;
import com.lzy.okgo.model.HttpParams;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.my.model.api.InitCityDataApi;
import com.module.my.model.api.JiFenStroeApi;
import com.module.my.model.api.ModifyMyDataApi;
import com.module.my.model.bean.Doparts;
import com.module.my.model.bean.Province2ListData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.photo.FileUtils;
import com.quicklyask.activity.R;
import com.quicklyask.entity.DataUrl;
import com.quicklyask.entity.JFJY1Data;
import com.quicklyask.entity.ProvinceBean;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.WordWrapView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.bitmap.KJBitmap;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.quicklyask.util.Utils.getDate;
import static com.quicklyask.util.Utils.getTime;

/**
 * 修改我的资料
 *
 * @author Rubin
 */
public class ModifyMyDataActivity extends BaseActivity {

    private final String TAG = "ModifyMyDataActivity";

    private String sex = "";
    private String city = "";
    private String province = "";
    private String name = "";
    private String imgUrl = "";

    private Activity mContext;
    private String filePath;
    private int sexInt = 1;

    @BindView(id = R.id.myprofile_head_img)
    private ImageView headIv;// 头像

    @BindView(id = R.id.myprofile_head_name_tv, click = true)
    private EditText nameEt;
    @BindView(id = R.id.myprofile_head_city_tv)
    private TextView cityTv;
    @BindView(id = R.id.myprofile_head_sex_tv)
    private TextView sexTv;
    @BindView(id = R.id.myprofile_head_birthday_tv)
    private TextView dateTv;

    @BindView(id = R.id.modify_back, click = true)
    private RelativeLayout back;

    @BindView(id = R.id.myprofile_all)
    private LinearLayout allContent;// 整个界面容器

    @BindView(id = R.id.myprofile_head_img_rly, click = true)
    private RelativeLayout headRly;
    @BindView(id = R.id.myprofile_head_name_rly)
    private RelativeLayout nameRly;
    @BindView(id = R.id.myprofile_head_city_rly, click = true)
    private RelativeLayout cityRly;
    @BindView(id = R.id.myprofile_head_sex_rly, click = true)
    private RelativeLayout sexRly;
    @BindView(id = R.id.myprofile_head_birthday_rly, click = true)
    private RelativeLayout dateRly;

    @BindView(id = R.id.myprofile_shiming_rly, click = true)
    private RelativeLayout shimingRly;

    @BindView(id = R.id.myprofile_head_sumbit_rly, click = true)
    private RelativeLayout saveRly;

    @BindView(id = R.id.myprofile_head_password_rly, click = true)
    private RelativeLayout nodifyPasswordRly;// 修改密码
    @BindView(id = R.id.myprofile_head_phone_rly, click = true)
    private RelativeLayout addPhoneRly;// 添加手机号
    @BindView(id = R.id.myprofile_head_phone_tv)
    private TextView addPhoneTv;
    @BindView(id = R.id.myprofile_mynuber_title_tv)
    private TextView mynuberTitleTv;
    @BindView(id = R.id.myprofile_head_password_tv)
    private TextView mynuberTipsTv;
    @BindView(id = R.id.myprofile_renzheng_state_tv)
    private TextView myrenzhengState;
    @BindView(id = R.id.myprofile_upsetuser_desc_rl, click = true)
    private RelativeLayout mUpsetuserDescBackground;//
    @BindView(id = R.id.myprofile_upsetuser_desc_tv)
    private TextView mUpsetuserDescTxt;

    private Handler mHandler;
    private String reslutStr;

    private String shengID = "0";
    private String cityID = "0";
    private List<Province2ListData> provinceList = new ArrayList<>();

    private static final int IMAGE_REQUEST_CODE = 0;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int RESIZE_REQUEST_CODE = 2;

    Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    private String type;
    private static final int BACK2 = 3;
    private static final int BACK3 = 33;

    private String dateBirthday = "";

    @BindView(id = R.id.view_wordwrap1)
    private WordWrapView wordWrapView1;
    @BindView(id = R.id.view_wordwrap2)
    private WordWrapView wordWrapView2;


    private List<Doparts> doParts = new ArrayList<>();
    private List<Doparts> parts = new ArrayList<>();

    private TextView[] doPartTv;
    private String[] doPartCheck;
    private int curDopart = 0;

    private TextView[] partTv;
    private String[] partCheck;
    private int curPart = 0;

    private String myNumber;

    private String aliPayNum;
    private String relName;
    private String birthday;

    TimePickerView pvTime;
    OptionsPickerView pvOptions;

    private ArrayList<ProvinceBean> options1Items = new ArrayList<>();

    OptionsPickerView pvCityOptions;
    private ArrayList<ProvinceBean> optionsCityItems = new ArrayList<>();
    private ArrayList<ArrayList<String>> optionsCityItems2 = new ArrayList<>();
    private String cardId;
    private final int IMG_REQUEST_MAIN_THREAD = 100;
    private ModifyMyDataApi modifyMyDataApi;
    private File path1;
    private Uri mOutputUri;
    private boolean isActivityResult = true;//是否用户选择新头像

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_myprofile);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = ModifyMyDataActivity.this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Intent it = getIntent();
        type = it.getStringExtra("type");
        // 初始化 用户信息
        modifyMyDataApi = new ModifyMyDataApi();

        String pathS = Environment.getExternalStorageDirectory().toString() + "/AyuemeiImage";
        path1 = new File(pathS);
        if (!path1.exists()) {
            path1.mkdirs();
        }

        getUserInfo();

        sex = Cfg.loadStr(mContext, FinalConstant.USEX, "");
        name = Cfg.loadStr(mContext, FinalConstant.UNAME, "");
        city = Cfg.loadStr(mContext, FinalConstant.UCITY, "");
        province = Cfg.loadStr(mContext, FinalConstant.UPROVINCE, "");
        imgUrl = Cfg.loadStr(mContext, FinalConstant.UHEADIMG, "");

        initCityData();

        mHandler = getHandler();
        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

            @Override
            public void onSildingFinish() {
                ModifyMyDataActivity.this.finish();
            }
        });

        nameEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {

            }

        });


    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    private void getUserInfo() {
        BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
        Map<String, Object> params = new HashMap<>();
        params.put("id", Utils.getUid());
        userInfoApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    myNumber = userData.getMynumber();
                    Log.e(TAG, "myNumber === " + myNumber);
                    aliPayNum = userData.getAlipay();
                    relName = userData.getReal_name();
                    birthday = userData.getBirthday();
                    cardId = userData.getCard_id();
                    String isReal = userData.getIs_real();
                    if ("1".equals(isReal)) {
                        myrenzhengState.setText("已认证");
                    } else {
                        myrenzhengState.setText("未认证");
                    }
                    String names = userData.getNickname();
                    nameEt.setText(names);
                    nameEt.setSelection(nameEt.getText().length());
                    if (isActivityResult) {
                        Glide.with(mContext).load(userData.getImg()).transform(new GlideCircleTransform(mContext)).into(headIv);
                        isActivityResult = false;
                    }

                    String upsetuser_desc = userData.getUpsetuser_desc();
                    if (TextUtils.isEmpty(upsetuser_desc)){
                        mUpsetuserDescBackground.setVisibility(View.GONE);
                    }else {
                        mUpsetuserDescBackground.setVisibility(View.VISIBLE);
                        mUpsetuserDescTxt.setText(upsetuser_desc);
                    }
                    Log.e(TAG, "userData.getProvince() === " + userData.getProvince());
                    Log.e(TAG, "userData.getCity() === " + userData.getCity());
                    cityTv.setText(userData.getProvince() + "," + userData.getCity());
                    String sex1 = userData.getSex();
                    if (sex1.equals("1")) {
                        sexTv.setText("男");
                    } else {
                        sexTv.setText("女");
                    }

                    //选项选择器
                    pvOptions = new OptionsPickerView(mContext);
                    //选项1
                    options1Items.clear();
                    options1Items.add(new ProvinceBean(0, "男", "", ""));
                    options1Items.add(new ProvinceBean(1, "女", "", ""));
                    //三级联动效果
                    pvOptions.setPicker(options1Items);
                    //设置选择的三级单位
                    pvOptions.setTitle("选择性别");
                    pvOptions.setCyclic(false, true, true);
                    //设置默认选中的三级项目
                    //监听确定选择按钮
                    if (sex1.equals("1")) {
                        pvOptions.setSelectOptions(0);
                        sexInt = 1;
                    } else {
                        pvOptions.setSelectOptions(1);
                        sexInt = 2;
                    }
                    pvOptions.setCancelable(true);
                    pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

                        @Override
                        public void onOptionsSelect(int options1, int option2, int options3) {
                            //返回的分别是三个级别的选中位置
                            String tx = options1Items.get(options1).getPickerViewText();
                            sexTv.setText(tx);
                            if (tx.equals("男")) {
                                sexInt = 1;
                            } else {
                                sexInt = 2;
                            }
                        }
                    });


                    //时间选择器
                    pvTime = new TimePickerView(mContext, TimePickerView.Type.YEAR_MONTH_DAY);
                    //控制时间范围
                    Calendar calendar = Calendar.getInstance();
                    pvTime.setRange(calendar.get(Calendar.YEAR) - 50, calendar.get(Calendar.YEAR));//要在setTime 之前才有效果哦

                    pvTime.setCyclic(false);
                    pvTime.setCancelable(true);
                    //时间选择后回调
                    pvTime.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {

                        @Override
                        public void onTimeSelect(Date date) {
                            Log.e(TAG, "date == " + date.toString());
                            dateTv.setText(getTime(date));
                            dateBirthday = getTime(date);
                        }
                    });

                    if (null != birthday && birthday.length() > 0) {
                        dateTv.setText(birthday);
                        dateBirthday = birthday;
                        pvTime.setTime(getDate(birthday));
                    } else {
                        pvTime.setTime(new Date());
                    }

                    if (null != myNumber) {
                        addPhoneTv.setText(myNumber);
                    }
                    doParts.clear();
                    parts.clear();
                    doParts = userData.getDoparts();
                    parts = userData.getParts();
                    Log.e(TAG, "doParts" + doParts.size());
                    Log.e(TAG, "parts" + parts.size());
                    //做过的项目 感兴趣的项目
                    doPartTv = new TextView[doParts.size()];
                    doPartCheck = new String[doParts.size()];
                    partTv = new TextView[parts.size()];
                    partCheck = new String[parts.size()];

                    LayoutInflater mInflater = getLayoutInflater();
                    wordWrapView1.removeAllViews();
                    wordWrapView2.removeAllViews();
                    for (int i = 0; i < doParts.size(); i++) {
                        final int j = i;
                        View headView = mInflater.inflate(R.layout.item_tips_tuoyuan, null);
                        doPartTv[i] = headView.findViewById(R.id.home_pop_part_name_tv);
                        doPartTv[i].setText(doParts.get(i).getName());
                        wordWrapView1.addView(headView);

                        if (doParts.get(i).getChecked().equals("1")) {
                            doPartTv[i].setTextColor(Color.parseColor("#ff5c77"));
                            doPartTv[i].setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
                            doPartCheck[i] = "1";
                        } else {
                            doPartTv[i].setTextColor(Color.parseColor("#666666"));
                            doPartTv[i].setBackgroundResource(R.drawable.shape_tuiyuan2_cdcdcd);
                            doPartCheck[i] = "0";
                        }

                        doPartTv[i].setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                curDopart = j;
                                if (doPartCheck[curDopart].equals("1")) {
                                    doPartTv[curDopart].setTextColor(Color.parseColor("#666666"));
                                    doPartTv[curDopart].setBackgroundResource(R.drawable.shape_tuiyuan2_cdcdcd);
                                    doPartCheck[curDopart] = "0";
                                } else {
                                    doPartTv[curDopart].setTextColor(Color.parseColor("#ff5c77"));
                                    doPartTv[curDopart].setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
                                    doPartCheck[curDopart] = "1";
                                }
                            }
                        });

                    }


                    for (int i = 0; i < parts.size(); i++) {
                        final int j = i;

                        View headView = mInflater.inflate(R.layout.item_tips_tuoyuan, null);
                        partTv[i] = headView.findViewById(R.id.home_pop_part_name_tv);
                        partTv[i].setText(parts.get(i).getName());
                        wordWrapView2.addView(headView);

                        if (parts.get(i).getChecked().equals("1")) {
                            partTv[i].setTextColor(Color.parseColor("#ff5c77"));
                            partTv[i].setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
                            partCheck[i] = "1";
                        } else {
                            partTv[i].setTextColor(Color.parseColor("#666666"));
                            partTv[i].setBackgroundResource(R.drawable.shape_tuiyuan2_cdcdcd);
                            partCheck[i] = "0";
                        }

                        partTv[i].setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                curPart = j;
                                if (partCheck[curPart].equals("1")) {
                                    partTv[curPart].setTextColor(Color.parseColor("#666666"));
                                    partTv[curPart].setBackgroundResource(R.drawable.shape_tuiyuan2_cdcdcd);
                                    partCheck[curPart] = "0";
                                } else {
                                    partTv[curPart].setTextColor(Color.parseColor("#ff5c77"));
                                    partTv[curPart].setBackgroundResource(R.drawable.shape_bian_tuoyuan2_ff5c77);
                                    partCheck[curPart] = "1";
                                }
                            }
                        });

                    }
                }else {
                    Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    /**
     * 初始化城市列表
     */
    void initCityData() {
        new InitCityDataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                provinceList = JSONUtil.TransformCity2(serverData.data);
                // cityList = provinceList.get(0).getList();
                //选项选择器
                pvCityOptions = new OptionsPickerView(mContext);

                int it1 = 0;
                int it2 = 0;

                for (int i = 0; i < provinceList.size(); i++) {
                    optionsCityItems.add(new ProvinceBean(i, provinceList.get(i).getName(), "", ""));

                    ArrayList<String> options2Items_00 = new ArrayList<>();
                    for (int j = 0; j < provinceList.get(i).getList().size(); j++) {
                        options2Items_00.add(provinceList.get(i).getList().get(j).getName());
                    }
                    optionsCityItems2.add(options2Items_00);

                    if (province.length() > 1) {
                        if (provinceList.get(i).getName().equals(province)) {
                            shengID = provinceList.get(i).get_id();
                            it1 = i;
                            for (int j = 0; j < provinceList.get(i).getList().size(); j++) {
                                options2Items_00.add(provinceList.get(i).getList().get(j).getName());
                                if (provinceList.get(i).getList().get(j).getName().equals(city)) {
                                    cityID = provinceList.get(i).getList().get(j).get_id();
                                    it2 = j;
                                }
                            }
                        }
                    }
                }

                pvCityOptions.setPicker(optionsCityItems, optionsCityItems2, true);

                pvCityOptions.setTitle("选择城市");
                pvCityOptions.setCyclic(false, false, true);
                //设置默认选中的三级项目
                //监听确定选择按钮
                pvCityOptions.setSelectOptions(it1, it2, 1);
                pvCityOptions.setCancelable(true);
                pvCityOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {

                    @Override
                    public void onOptionsSelect(int options1, int option2, int options3) {
                        //返回的分别是三个级别的选中位置
                        String tx1 = optionsCityItems.get(options1).getPickerViewText();
                        String tx2 = optionsCityItems2.get(options1).get(option2);
                        cityTv.setText(tx1 + "," + tx2);

                        shengID = provinceList.get(options1).get_id();
                        cityID = provinceList.get(options1).getList().get(option2).get_id();

                    }
                });
            }

        });
    }

    @SuppressLint("NewApi")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.myprofile_head_img_rly:// 换头像
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(ModifyMyDataActivity.this).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            new PopupWindows(ModifyMyDataActivity.this, allContent);
                        }

                        @Override
                        public void onDenied(List<String> permissions) {
                        }
                    });
                } else {
                    new PopupWindows(ModifyMyDataActivity.this, allContent);
                }

                break;
            case R.id.myprofile_head_city_rly:// 换城市
                View view1 = getWindow().peekDecorView();
                if (view1 != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view1.getWindowToken(), 0);
                }
                if (null != pvCityOptions) {
                    pvCityOptions.show();
                }
                break;
            case R.id.myprofile_head_sex_rly:// 换性别
                View view2 = getWindow().peekDecorView();
                if (view2 != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view2.getWindowToken(), 0);
                }
                if (null != pvOptions) {
                    pvOptions.show();
                }

                break;
            case R.id.myprofile_head_sumbit_rly:// 保存

                String feedContent = nameEt.getText().toString();
                Matcher matcher = emoji.matcher(feedContent);

                if (matcher.find()) {
                    Toast.makeText(ModifyMyDataActivity.this, "暂不支持表情输入", Toast.LENGTH_SHORT).show();
                } else {
                    if (nameEt.getText().length() >= 2 && Utils.isStringFormatCorrect(feedContent)) {
                        saveData();
                    } else {
                        Toast.makeText(ModifyMyDataActivity.this, "昵称请输入在4-20个字符（汉字、字母、数字、下划线）", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.modify_back:

                onBackPressed();

                break;
            case R.id.myprofile_head_password_rly:// 修改密码
                    Intent it1 = new Intent();
                    it1.putExtra("type", "2");
                    it1.setClass(mContext, RestPassWordActivity.class);
                    startActivity(it1);
                break;
            case R.id.myprofile_head_phone_rly:// 添加手机号


                break;
            case R.id.myprofile_head_birthday_rly://修改生日

                if (null != pvTime) {
                    pvTime.show();
                }

                break;

            case R.id.myprofile_shiming_rly:
                Intent intent = new Intent(mContext, RenZhengNameActivity.class);
                intent.putExtra("real_name", relName);
                intent.putExtra("card_id", cardId);
                startActivity(intent);
                break;
            case R.id.myprofile_upsetuser_desc_rl:
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        toJifen();

                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }
                } else {
                    Intent it5 = new Intent();
                    it5.setClass(mContext, LoginActivity605.class);
                    startActivity(it5);
                }
                break;
        }
    }


    private void toJifen() {
        Map<String, Object> maps = new HashMap<>();
        maps.put("act", "autologin");
        new JiFenStroeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (null != serverData) {
                    Log.d(TAG, "url===>" + serverData.code + "---" + serverData.data);
                    try {
                        DataUrl dataUrl = JSONUtil.TransformSingleBean(serverData.data, DataUrl.class);
                        String url = dataUrl.getUrl();
                        Intent intent = new Intent();
                        intent.setClass(mContext, CreditActivity.class);
                        intent.putExtra("navColor", "#fafafa");    //配置导航条的背景颜色，请用#ffffff长格式。
                        intent.putExtra("titleColor", "#636a76");    //配置导航条标题的颜色，请用#ffffff长格式。
                        intent.putExtra("url", url);    //配置自动登陆地址，每次需服务端动态生成。
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public class PopupWindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public PopupWindows(Context mContext, View parent) {

            final View view = View.inflate(mContext, R.layout.item_popupwindows, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.FILL_PARENT);
            setHeight(LayoutParams.FILL_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            Button bt1 = view.findViewById(R.id.item_popupwindows_camera);
            Button bt2 = view.findViewById(R.id.item_popupwindows_Photo);
            Button bt3 = view.findViewById(R.id.item_popupwindows_cancel);
            // 拍照
            bt1.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    photo();
                    dismiss();
                }
            });
            // 点击 之外 消失
            view.setOnTouchListener(new OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {

                    int height = view.findViewById(R.id.ll_popup).getTop();
                    int y = (int) event.getY();
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (y < height) {
                            dismiss();
                        }
                    }
                    return true;
                }
            });
            // 相册
            bt2.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(galleryIntent, IMAGE_REQUEST_CODE);

                    dismiss();
                }
            });
            bt3.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    public void photo() {
        try {
            Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File file = new File(path1, "yuemei" + ".jpg");
            openCameraIntent.addCategory("android.intent.category.DEFAULT");
            Uri uri = uriFromFile(mContext, file);
            openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(openCameraIntent, CAMERA_REQUEST_CODE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Uri uriFromFile(Activity activity, File file) {
        Uri fileUri;
        //7.0以上进行适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String p = activity.getPackageName() + ".FileProvider";
            fileUri = FileProvider.getUriForFile(activity, p, file);
        } else {
            fileUri = Uri.fromFile(file);
        }
        return fileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IMAGE_REQUEST_CODE:                 //相册照片的选择
                if (data != null) {
                    File imageCrop = new File(path1, "yuemei_crop_" + System.currentTimeMillis() + ".JPEG");
                    resizeImage(data.getData(), imageCrop);
                }
                break;
            case CAMERA_REQUEST_CODE:               //也是拍照后的选择
                if (resultCode == -1) {
                    File file = new File(path1, "yuemei" + ".jpg");
                    Uri filtUri = uriFromFile(mContext, file);
                    File imageCrop = new File(path1, "yuemei_crop_" + System.currentTimeMillis() + ".jpg");
                    resizeImage(filtUri, imageCrop);
                }
                break;
            case RESIZE_REQUEST_CODE:                 //调整大小后的选择（这个真正执行的地方）
                if (data != null && resultCode == -1) {
                    showResizeImage(data);
                }
                break;
            case BACK2:
                if (null != data) {
                    String phone = data.getStringExtra("phone");
                    addPhoneTv.setText(phone);
                }
                break;
            case BACK3:
                if (null != data) {
                    aliPayNum = data.getStringExtra("alipay");
                    relName = data.getStringExtra("real_name");
                    cardId = data.getStringExtra("card_id");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void resizeImage(Uri uri, File outputFile) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        setIntentDataAndType(mContext, intent, "image/*", uri, true);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        // 不启用人脸识别
        intent.putExtra("noFaceDetection", false);
        //return-data为true时，直接返回bitmap，可能会很占内存，不建议，小米等个别机型会出异常！！！
        //所以适配小米等个别机型，裁切后的图片，不能直接使用data返回，应使用uri指向
        //裁切后保存的URI，不属于我们向外共享的，所以可以使用fill://类型的URI
        mOutputUri = Uri.fromFile(outputFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mOutputUri);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        startActivityForResult(intent, RESIZE_REQUEST_CODE);
    }

    public void setIntentDataAndType(Context context, Intent intent, String type, Uri fileUri, boolean writeAble) {
        //7.0以上进行适配
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setDataAndType(fileUri, type);
            //临时赋予读写Uri的权限
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (writeAble) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
        } else {
            intent.setDataAndType(fileUri, type);
        }
    }

    private void showResizeImage(Intent data) {

        if (data != null) {
            Bitmap photo = null;
            try {
                photo = BitmapFactory.decodeStream(getContentResolver().openInputStream(mOutputUri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            final String picName = "yuemei" + System.currentTimeMillis();
            FileUtils.saveBitmap(photo, picName);// 保存图片
            filePath = FileUtils.SDPATH + picName + ".jpg";
            Glide.with(mContext).load(mOutputUri).transform(new GlideCircleTransform(mContext)).into(headIv);
        }
    }

    Map<String, Object> maps = new HashMap<>();
    HttpParams params = new HttpParams();

    /**
     * 上传资料
     */
    private void saveData() {

        String nickName = nameEt.getText().toString();
        String sexs = sexInt + "";

        String dopartIds = "";
        for (int i = 0; i < doParts.size(); i++) {
            if (doPartCheck[i].equals("1")) {
                dopartIds = dopartIds + doParts.get(i).getId() + ",";
            }
        }

        String partIds = "";
        for (int i = 0; i < parts.size(); i++) {
            if (partCheck[i].equals("1")) {
                partIds = partIds + parts.get(i).getId() + ",";
            }
        }
        maps.put("nickname", nickName);
        maps.put("province", shengID);
        maps.put("city_id", cityID);
        maps.put("sex", sexs);
        maps.put("birthday", dateBirthday);
        if (!"".equals(dopartIds)) {
            maps.put("doparts", dopartIds);
        }
        if (!"".equals(partIds)) {
            maps.put("parts", partIds);
        }

        params.put("nickname", nickName);
        params.put("province", shengID);
        params.put("city_id", cityID);
        params.put("sex", sexs);
        params.put("birthday", dateBirthday);
        if (!"".equals(dopartIds)) {
            params.put("doparts", dopartIds);
        }
        if (!"".equals(partIds)) {
            params.put("parts", partIds);
        }
        if (filePath != null) {
            params.put("avatar", new File(filePath));
        }

        Log.e(TAG, "nickName === " + nickName);
        Log.e(TAG, "shengID === " + shengID);
        Log.e(TAG, "sexs === " + sexs);
        Log.e(TAG, "dateBirthday === " + dateBirthday);
        Log.e(TAG, "dopartIds === " + dopartIds);
        Log.e(TAG, "partIds === " + partIds);

        modifyMyDataApi.getCallBack(mContext, maps, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.d(TAG, serverData + "");
                try {
                    if ("1".equals(serverData.code)) {
                        isActivityResult = true;
                        JFJY1Data jfjyData = JSONUtil.TransformSingleBean(serverData.data, JFJY1Data.class);

                        String jifenNu = jfjyData.getIntegral();
                        String jyNu = jfjyData.getExperience();

                        if (!jifenNu.equals("0") && !jyNu.equals("0")) {
                            MyToast.makeTexttext4Toast(mContext, jifenNu, jyNu, 1000).show();
                        } else {
                            if (!jifenNu.equals("0")) {
                                MyToast.makeTexttext2Toast(mContext, jifenNu, 1000).show();
                            } else {
                                if (!jyNu.equals("0")) {
                                    MyToast.makeTexttext3Toast(mContext, jyNu, 1000).show();
                                }
                            }
                        }


                        String ifmodiy = Cfg.loadStr(mContext, "modiy", "");
                        if (ifmodiy.length() > 0) {

                        } else {
                            Cfg.saveStr(mContext, "modiy", "1");
                        }

                        if (type.equals("1")) {

                            Intent it888 = new Intent();
                            it888.setClass(mContext, MyPointsActivity.class);
                            it888.putExtra("ok", "1");
                            setResult(888, it888);
                            finish();
                        } else {
                            onBackPressed();
                        }

                    } else {
                        ViewInject.toast(serverData.message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case IMG_REQUEST_MAIN_THREAD:
                        break;
                }

            }
        };
    }


    public void onResume() {
        super.onResume();
        getUserInfo();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }


}
