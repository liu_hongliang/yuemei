package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.quicklyask.view.ElasticScrollView;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 隐私声明
 * 
 * @author Rubin
 * 
 */
public class UserAgreementWebActivity extends BaseActivity {

	private final String TAG = "UserAgreementWebActivity";

	@BindView(id = R.id.xieyi_web_det_scrollview1, click = true)
	private ElasticScrollView scollwebView;
	@BindView(id = R.id.xieyi_linearlayout, click = true)
	private LinearLayout contentWeb;

	@BindView(id = R.id.user_xieyi_top)
	private CommonTopBar mTop;// 返回

	private WebView docDetWeb;

	private UserAgreementWebActivity mContex;

	private String uid;

	public JSONObject obj_http;
	private BaseWebViewClientMessage baseWebViewClientMessage;
	public static String mIsYueMeiAgremment ="isyuemeiagremment";  //0 隐私协议  1 悦美用户使用协议
	private int mIsuseroryuemei;

	@Override
	public void setRootView() {
		setContentView(R.layout.web_user_xieyi);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContex = UserAgreementWebActivity.this;
		uid = Utils.getUid();
		mIsuseroryuemei = getIntent().getIntExtra(mIsYueMeiAgremment, 0);

		mTop.setCenterText("悦美协议");

		scollwebView.GetLinearLayout(contentWeb);

		baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
		initWebview();

		LodUrl1();

		SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
		mSildingFinishLayout
				.setOnSildingFinishListener(new OnSildingFinishListener() {

					@Override
					public void onSildingFinish() {
						UserAgreementWebActivity.this.finish();
					}
				});

		mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, R.anim.base_slide_right_out);
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebview() {
		docDetWeb = new WebView(mContex);
		docDetWeb.getSettings().setJavaScriptEnabled(true);
		docDetWeb.getSettings().setUseWideViewPort(true);
		docDetWeb.setWebViewClient(baseWebViewClientMessage);
		docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		contentWeb.addView(docDetWeb);
	}

	protected void OnReceiveData(String str) {
		scollwebView.onRefreshComplete();
	}

	public void webReload() {
		if (docDetWeb != null) {
			baseWebViewClientMessage.startLoading();
			docDetWeb.reload();
		}
	}

	/**
	 * 处理webview里面的按钮
	 * 
	 * @param urlStr
	 */
	public void showWebDetail(String urlStr) {
		try {
			ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
			parserWebUrl.parserPagrms(urlStr);
			JSONObject obj = parserWebUrl.jsonObject;
			obj_http = obj;

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 加载web
	 */
	public void LodUrl1() {
		String tyUrl;
		baseWebViewClientMessage.startLoading();
		if (1 == mIsuseroryuemei){
			tyUrl = FinalConstant.YUEMEIUSERAGREMMENT;
		}else {
			tyUrl = FinalConstant.USERAGREMMENT;
		}


		WebSignData addressAndHead = SignUtils.getAddressAndHead(tyUrl);
		docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
