package com.module.my.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.my.model.api.InitBindOpenApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.quicklyask.wheel.widget.SildingFinishLayout.OnSildingFinishListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * 消息提醒我开关
 *
 * @author Rubin
 */
public class SwitchRemindMeActivity extends BaseActivity {

    @BindView(id = R.id.meesage_open, click = true)
    private RelativeLayout openRly;
    @BindView(id = R.id.meesage_close, click = true)
    private RelativeLayout closeRly;

    @BindView(id = R.id.message_open_iv, click = true)
    private ImageView opneIv;
    @BindView(id = R.id.message_close_iv, click = true)
    private ImageView closeIv;

    @BindView(id = R.id.switch_message_top)
    private CommonTopBar mTop;// 返回

    private Context mContext;

    private String userid = "";
    private String channelId = "";
    private String uid = "";

    private String flag = "";

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_switch_message);
    }

    @Override
    protected void initWidget() {
        super.initWidget();
        mContext = SwitchRemindMeActivity.this;

        mTop.setCenterText("新回答提醒");

        uid = Utils.getUid();
        String message = Cfg.loadStr(mContext, "message", "");
        if ("2".equals(message)) {
            opneIv.setVisibility(View.GONE);
            closeIv.setVisibility(View.VISIBLE);
        }
        // ViewInject.toast(open);
        initOpen();

        SildingFinishLayout mSildingFinishLayout = findViewById(R.id.sildingFinishLayout);
        mSildingFinishLayout.setOnSildingFinishListener(new OnSildingFinishListener() {

            @Override
            public void onSildingFinish() {
                SwitchRemindMeActivity.this.finish();
            }
        });
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    void initOpen() {
        String userid = Utils.userId;
        String channelId = Utils.channelId;
        String uid = Utils.getUid();
        String token = "0";
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("ymid", uid);
        keyValues.put("channelid", channelId);
        keyValues.put("userid", userid);
        keyValues.put("laiyuan", "0");
        keyValues.put("flag", flag);
        keyValues.put("token", token);
        new InitBindOpenApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e("AAAAAAAAAAA", "baidu====" + serverData.data);
            }

        });
    }

    @SuppressWarnings("deprecation")
    @Override
    public void widgetClick(View v) {
        super.widgetClick(v);
        switch (v.getId()) {
            case R.id.meesage_open:
                opneIv.setVisibility(View.VISIBLE);
                closeIv.setVisibility(View.GONE);
                // open = "1";
                binBaidu();
                flag = "1";
                // finish();
                initHttp();
                binBaidu();
                onBackPressed();
                Cfg.saveStr(mContext, "message", "1");
                Intent intent = new Intent(SwitchRemindMeActivity.this, SelfSettingActivity.class);
                intent.putExtra("flag", "1");
                startActivity(intent);
                finish();
                break;
            case R.id.meesage_close:
                Log.e("66666", "1111111111");
                opneIv.setVisibility(View.GONE);
                closeIv.setVisibility(View.VISIBLE);
                PushManager.stopWork(getApplicationContext());
//			PushManager.unbind(mContext);
                // finish();
                flag = "2";
                initHttp();
                onBackPressed();
                Cfg.saveStr(mContext, "message", "2");
                Intent intent1 = new Intent(SwitchRemindMeActivity.this, SelfSettingActivity.class);
                intent1.putExtra("flag", "1");
                startActivity(intent1);
                finish();
                break;
        }
    }

    void binBaidu() {
        // *****************消息推送*********************//
        Utils.logStringCache = Utils.getLogText(getApplicationContext());

        if (!Utils.hasBind(getApplicationContext())) {
            PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
        }

        // ********************************************//
    }

    void initHttp() {
        String userid = Utils.userId;
        String channelId = Utils.channelId;
        String uid = Utils.getUid();
        String token = "0";

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("ymid", uid);
        keyValues.put("channelid", channelId);
        keyValues.put("userid", userid);
        keyValues.put("laiyuan", "0");
        keyValues.put("flag", flag);
        keyValues.put("token", token);

        new InitBindOpenApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e("AAAAAAAAAAA", "baidu====" + serverData.code);
            }

        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
