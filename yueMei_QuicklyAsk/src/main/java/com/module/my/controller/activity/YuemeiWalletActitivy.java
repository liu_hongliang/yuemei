package com.module.my.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.community.web.WebUtil;
import com.module.my.controller.other.FlexibleScrollView;
import com.module.my.model.api.YuemeiWalletApi;
import com.module.my.model.bean.YuemeiWalletBean;
import com.module.other.netWork.netWork.ServerData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;
import com.quicklyask.view.YueMeiDialog2;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YuemeiWalletActitivy extends YMBaseActivity {

    public static final String TAG = "YuemeiWalletActitivy";
    @BindView(R.id.ymwallet_sview)
    FlexibleScrollView mYmFlexibleScrollView;
    @BindView(R.id.ymwallet_back)
    LinearLayout mYmwalletBack;
    @BindView(R.id.ymwallet_common_ask)
    LinearLayout mYmwalletCommonAsk;
    @BindView(R.id.ymwallet_money)
    TextView mYmwalletMoney;
    @BindView(R.id.ymwallet_desc)
    TextView mYmwalletDesc;
    @BindView(R.id.ymwallet_tixian_text)
    TextView mYmwalletTixianText;
    @BindView(R.id.ymwallet_tixian_money)
    TextView mYmwalletTixianMoney;
    @BindView(R.id.ymwallet_tixian_rly)
    RelativeLayout mYmwalletTixianRly;
    @BindView(R.id.ymwallet_mingxi_text)
    TextView mYmwalletMingxiText;
    @BindView(R.id.ymwallet_mingxi_rly)
    RelativeLayout mYmwalletMingxiRly;
    @BindView(R.id.ymwallet_account_txt)
    TextView mYmwalletAccountTxt;
    @BindView(R.id.ymwallet_account_rly)
    RelativeLayout mYmwalletAccountRly;
    @BindView(R.id.myprofile_shoukuan_goto)
    TextView mMyprofileShoukuanGoto;
    @BindView(R.id.ymwallet_notixian_money)
    TextView ymwalletNotixianMoney;
    @BindView(R.id.ymwallet_notixian_rly)
    RelativeLayout ymwalletNotixianRly;
    @BindView(R.id.ymwallet_http)
    CheckBox ymwalletHttp;
    @BindView(R.id.ymwallet_txt)
    TextView ymwalletTxt;
    @BindView(R.id.ymwallet_http_click)
    LinearLayout ymwalletHttpClick;
    private String mBalance; //钱包余额
    private String mDesc;  //描述
    private String mProceeds_account; //收款账号
    private String mReal_name;
    private int mMonth_max_money;
    private int mDay_max_limit;
    private int mWithdrawNum;
    private float mWithdrawMonthNum;
    private String mIs_real;
    private String mPoundage;
    private float mMonth_money;
    private String mOn_extract_balance;
    private int mEveryMinMoney;
    private int mAccount_type;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ymwallet;
    }

    @Override
    protected void initView() {
        ButterKnife.bind(this);

        mYmFlexibleScrollView.setHeaderView(findViewById(R.id.ymwallet_head));
        mYmFlexibleScrollView.smoothScrollTo(0, 0);
        String wallet_check = mFunctionManager.loadStr("wallet_check", "1");
        if ("1".equals(wallet_check)){
            ymwalletHttp.setChecked(true);
        }else {
            ymwalletHttp.setChecked(false);
        }
        ymwalletHttp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    mFunctionManager.saveStr("wallet_check","1");
                }else {
                    mFunctionManager.saveStr("wallet_check","0");
                }
            }
        });
    }

    @Override
    protected void initData() {
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "onResume");
        mIs_real = Cfg.loadStr(mContext, "is_real", "");
        loadData();


    }


    private void loadData() {
        new YuemeiWalletApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serviceData) {
                if ("1".equals(serviceData.code)) {
                    try {
                        YuemeiWalletBean yuemeiWalletBean = JSONUtil.TransformSingleBean(serviceData.data, YuemeiWalletBean.class);
                        mBalance = yuemeiWalletBean.getBalance();
                        mDesc = yuemeiWalletBean.getDesc();
                        //可提现余额
                        mOn_extract_balance = yuemeiWalletBean.getOn_extract_balance();
                        //收款账号
                        mProceeds_account = yuemeiWalletBean.getProceeds_account();

                        mAccount_type = yuemeiWalletBean.getAccount_type();
                        //收款姓名
                        mReal_name = yuemeiWalletBean.getReal_name();
                        //每月最大可体现金额
                        mMonth_max_money = yuemeiWalletBean.getMonth_max_money();
                        //	每天最大可提现次数
                        mDay_max_limit = yuemeiWalletBean.getDay_max_limit();
                        //用户当天申请提现次数
                        mWithdrawNum = yuemeiWalletBean.getWithdrawNum();
                        //实名用户当月的提现总金额
                        mWithdrawMonthNum = yuemeiWalletBean.getWithdrawMonthNum();
                        //用户是否实名 1实名
                        mIs_real = yuemeiWalletBean.getIs_real();
                        mFunctionManager.saveStr("is_real",mIs_real);
                        Log.e(TAG,"mIs_real == "+mIs_real);
                        //手续费
                        mPoundage = yuemeiWalletBean.getPoundage();
                        //用户本月可提现金额
                        mMonth_money = yuemeiWalletBean.getMonth_money();
                        //	每次提现最低金额
                        mEveryMinMoney = yuemeiWalletBean.getEvery_min_money();
                        //不可提现余额
                        String un_extract_balance = yuemeiWalletBean.getUn_extract_balance();
                        mYmwalletMoney.setText("¥" + mBalance);
                        mYmwalletDesc.setText(mDesc);
                        mYmwalletTixianMoney.setText(mOn_extract_balance);

                        ymwalletNotixianMoney.setText(un_extract_balance);
                        if (TextUtils.isEmpty(mProceeds_account)) {
                            mYmwalletAccountTxt.setText("添加");
                        } else {
                            switch (mAccount_type) {
                                case 0:
                                    mYmwalletAccountTxt.setText("支付宝");
                                    break;
                                case 1:
                                    mYmwalletAccountTxt.setText("银行卡");
                                    break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    mFunctionManager.showShort(serviceData.message);
                }

            }
        });
    }

    @OnClick({R.id.ymwallet_back, R.id.ymwallet_common_ask, R.id.ymwallet_tixian_rly, R.id.ymwallet_notixian_rly, R.id.ymwallet_mingxi_rly, R.id.ymwallet_account_rly,R.id.ymwallet_txt,R.id.ymwallet_http})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ymwallet_back:
                finish();
                break;
            case R.id.ymwallet_common_ask:
                Intent intent3 = new Intent(this, WalletMingxiActivity.class);
                intent3.putExtra("jump_type", "2");//1,钱包明细   2，钱包说明  3，提现说明
                intent3.putExtra("title", "说明");
                startActivity(intent3);
                break;
            case R.id.ymwallet_tixian_rly:
                if ("1".equals(mIs_real)) {  //1实名
                    if (!TextUtils.isEmpty(mReal_name) && !TextUtils.isEmpty(mProceeds_account)) {
                        if (ymwalletHttp.isChecked()){
                            Intent intent = new Intent(this, TixianActivity.class);
                            intent.putExtra("on_extract_balance", mOn_extract_balance); //可提现余额
                            intent.putExtra("month_money", mMonth_money); //用户本月可提现金额
                            intent.putExtra("month_max_money", mMonth_max_money); //每月最大可提现金额
                            intent.putExtra("day_max_limit", mDay_max_limit); //每天最大可提现次数
                            intent.putExtra("withdrawNum", mWithdrawNum); //用户当天申请提现次数
                            intent.putExtra("withdrawMonthNum", mWithdrawMonthNum); //实名用户当月的提现总金额
                            intent.putExtra("real_name", mReal_name); //姓名
                            intent.putExtra("proceeds_account", mProceeds_account); //支付宝账号
                            intent.putExtra("account_type", mAccount_type); //账户类型
                            intent.putExtra("every_min_money", mEveryMinMoney); //每次提现最低金额
                            startActivity(intent);
                        }else {
                            final YueMeiDialog2 dialog2 = new YueMeiDialog2(this, "需同意并勾选底部悦美结算协议", "确定");
                            dialog2.setCanceledOnTouchOutside(false);
                            dialog2.show();
                            dialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                                @Override
                                public void BtnClick() {
                                    dialog2.dismiss();
                                }
                            });
                        }

                    } else {
                        final YueMeiDialog2 dialog2 = new YueMeiDialog2(this, "您还没有绑定收款账号，请先绑定您的账号信息", "确定");
                        dialog2.setCanceledOnTouchOutside(false);
                        dialog2.show();
                        dialog2.setBtnClickListener(new YueMeiDialog2.BtnClickListener2() {
                            @Override
                            public void BtnClick() {
                                dialog2.dismiss();
                            }
                        });

                    }

                } else {
                    final YueMeiDialog yueMeiDialog = new YueMeiDialog(this, "您还没有实名认证，请先认证您的个人信息", "取消", "去认证");
                    yueMeiDialog.setCanceledOnTouchOutside(false);
                    yueMeiDialog.show();
                    yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                        @Override
                        public void leftBtnClick() {
                            yueMeiDialog.dismiss();

                        }

                        @Override
                        public void rightBtnClick() {
                            Intent intent = new Intent(mContext, RenZhengNameActivity.class);
                            startActivity(intent);
                            yueMeiDialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.ymwallet_mingxi_rly:
                Intent intent2 = new Intent(this, WalletMingxiActivity.class);
                intent2.putExtra("jump_type", "1");//1,钱包明细   2，钱包说明  3，提现说明
                intent2.putExtra("title", "钱包明细");
                startActivity(intent2);
                break;
            case R.id.ymwallet_account_rly:
                if ("1".equals(mIs_real)) {  //1实名
                    Intent intent = new Intent(this, AccountShoukuanActivity.class);
                    intent.putExtra("sk_name", mReal_name);
                    intent.putExtra("sk_account", mProceeds_account);
                    intent.putExtra("sk_account_type", mAccount_type);
                    startActivity(intent);
                } else {
                    final YueMeiDialog yueMeiDialog = new YueMeiDialog(this, "您还没有实名认证，请先认证您的个人信息", "取消", "去认证");
                    yueMeiDialog.setCanceledOnTouchOutside(false);
                    yueMeiDialog.show();
                    yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                        @Override
                        public void leftBtnClick() {
                            yueMeiDialog.dismiss();

                        }

                        @Override
                        public void rightBtnClick() {
                            startActivity(new Intent(mContext, RenZhengNameActivity.class));
                            yueMeiDialog.dismiss();
                        }
                    });
                }

                break;
            case R.id.ymwallet_notixian_rly:
                MainTableActivity.mainBottomBar.setCheckedPos(1);
                Intent intent = new Intent(mContext, MainTableActivity.class);
                startActivity(intent);
                break;
            case R.id.ymwallet_http:

                break;
            case R.id.ymwallet_txt:
                WebUtil.getInstance().startWebActivity(mContext,FinalConstant.WALLET_HTTP);
                break;
        }
    }


//    @OnClick(R.id.button)
//    public void onViewClicked() {
//        HashMap<String,Object> maps=new HashMap<>();
//        maps.put("card_id","");
//        maps.put("real_name","");
//        maps.put("face_type","1");
//        new RenZhengApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
//            @Override
//            public void onSuccess(ServerData serverData) {
//                if ("1".equals(serverData.code)){
//                    showShort("成功");
//                    mIs_real="0";
//                    Cfg.saveStr(mContext,"is_real","");
//                }
//            }
//        });
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
