package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mobstat.StatService;
import com.leon.channel.helper.ChannelReaderUtil;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cookie.store.CookieStore;
import com.module.MainTableActivity;
import com.module.MyApplication;
import com.module.SplashActivity;
import com.module.api.VersionApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.share.BaseShareView;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.my.model.api.DisturbApi;
import com.module.my.model.api.JPushClosedApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.BotherData;
import com.module.my.model.bean.NoLoginBean;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.VersionJCData;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.DataCleanManager;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.ProgDialog;
import com.quicklyask.wheel.widget.SildingFinishLayout;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.kymjs.aframe.utils.SystemTool;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;
import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * 个人设置
 *
 * @author Rubin
 */
public class SelfSettingActivity extends YMBaseActivity {

    @BindView(R.id.self_setting_top)
    CommonTopBar mTop;// 返回

    @BindView(R.id.set_switch_message_rly)
    RelativeLayout setMessageOpenRly;
    @BindView(R.id.set_message_open_tv)
    TextView setMessageOpenTv;

    @BindView(R.id.set_switch_bother_rly)
    RelativeLayout setBotherOpenRly;
    @BindView(R.id.setting_if_kaiqi_darao)
    TextView setBotherOpenTv;

    @BindView(R.id.set_switch_qiandao_rly)
    RelativeLayout setQianDaoOpenRly;
    @BindView(R.id.set_qiandao_open_tv)
    TextView setQianDaoOpenTv;

    @BindView(R.id.setting_jiance_tv)
    TextView versionTv;
    @BindView(R.id.set_to_yuemei)
    RelativeLayout setToYuemei;// 来电找悦美
    @BindView(R.id.set_to_weixin)
    RelativeLayout setToWeixin;// 微信推荐
    @BindView(R.id.set_to_xinlang)
    RelativeLayout setToXinlang;// 微博推荐
    @BindView(R.id.set_to_app)
    RelativeLayout setToApp;// app推荐
    @BindView(R.id.qingchu_huancun_rly)
    RelativeLayout cleanHuancunRLy;// 清除缓存
    @BindView(R.id.qingchu_huancun_tv)
    TextView cleanDataTv;// 缓存的数据 单位M
    @BindView(R.id.hot_app_rly)
    RelativeLayout hotAppRly;// 热门推荐

    @BindView(R.id.set_to_feedback)
    RelativeLayout setToFeedback;// 吐槽
    @BindView(R.id.set_to_aboutour)
    RelativeLayout setToAboutOur;// 关于我们
    @BindView(R.id.set_to_share_rly)
    RelativeLayout shareRLy;// 分享给好友
    @BindView(R.id.set_to_license_rly)
    RelativeLayout licenseRLy;// 分享给好友
    @BindView(R.id.setting_jiance_new_iv2)
    ImageView ivVrJc;
    @BindView(R.id.vision_jiance_rly)
    RelativeLayout versionjcRly;// 检测提示升级

    @BindView(R.id.privacy_settings)
    RelativeLayout privacy_settings;// 隐私设置
    @BindView(R.id.user_privite_rly)
    RelativeLayout userPriviteRly;// 隐私政策
    @BindView(R.id.user_protoclo_rly)
    RelativeLayout userProtocolRly;// 用户协议
    @BindView(R.id.exit_login_bt)
    Button exitBt;// 退出登录

    @BindView(R.id.jiance_vosion_tv_ssssss)
    TextView versionsssTv;

    @BindView(R.id.sildingFinishLayout)
    SildingFinishLayout mSildingFinishLayout;

    private String type = "";

    private String apkUrl;
    private String vserCodeHttp;
    private int vserHttpInt;
    private String vserCodeBendi;
    private int vserLocInt;

    // 版本升级
    private final int UPDATA_NONEED = 0;
    private final int UPDATA_CLIENT = 1;
    private final int GET_UNDATAINFO_ERROR = 2;
    private final int SDCARD_NOMOUNTED = 3;
    private final int DOWN_ERROR = 4;

    private static boolean interceptFlag = false;
    private String switchFlag;

    // 第三方登录

    // 要分享的图片
    private String shareUrl = "";
    private String shareContent = "";
    private String mPath;
    private String mRegistrationID;
    private String TAG = "SelfSettingActivity";

    @Override
    protected int getLayoutId() {
        return R.layout.acty_self_setting;
    }

    @Override
    protected void initView() {
        String message = Cfg.loadStr(mContext, "message", "");
        Log.e(TAG, "message === " + message);
        if ("2".equals(message)) {
            setMessageOpenTv.setText("已关闭");
        } else {
            setMessageOpenTv.setText("已开启");
        }

        mTop.setCenterText("设置");
        mRegistrationID = JPushInterface.getRegistrationID(mContext);

        vserCodeBendi = SystemTool.getAppVersion(mContext);
        versionTv.setText("版本： " + vserCodeBendi);
        initVsersion();

        mPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.quicklyask.activity";
        cleanDataTv.setText(getCacheSize());

        mSildingFinishLayout.setOnSildingFinishListener(new SildingFinishLayout.OnSildingFinishListener() {

            @Override
            public void onSildingFinish() {
                SelfSettingActivity.this.finish();
            }
        });

        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            long[] mHints = new long[5];

            @Override
            public void onClick(View v) {
                Log.e(TAG, "11111");
                System.arraycopy(mHints, 1, mHints, 0, mHints.length - 1);
                mHints[mHints.length - 1] = SystemClock.uptimeMillis();
                if (SystemClock.uptimeMillis() - mHints[0] <= 3000) {
                    mHints[0] = 0;
                    mHints[1] = 0;
                    mHints[2] = 0;
                    mHints[3] = 0;
                    mHints[4] = 0;
                    String channel = ChannelReaderUtil.getChannel(MyApplication.getContext());
                    Log.e(TAG,"channel == "+channel);

                    showDialogExitEdit3(FinalConstant1.YUEMEI_MARKET + ":" + SystemTool.getAppVersion(mContext), "确定");
                }
            }
        });

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setMultiOnClickListener(exitBt, versionjcRly, licenseRLy, shareRLy, setToAboutOur, setToFeedback, setMessageOpenRly, setMessageOpenTv, setBotherOpenRly, setBotherOpenTv, setQianDaoOpenRly, setQianDaoOpenTv, setToYuemei, setToWeixin, setToXinlang, setToApp, cleanHuancunRLy, hotAppRly, privacy_settings,userPriviteRly,userProtocolRly);

    }

    @Override
    protected void initData() {

    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);

        if (Utils.isLogin()) {
            initOpen();
            initQianDaoOpen();
            exitBt.setVisibility(View.VISIBLE);
        } else {
            exitBt.setVisibility(View.GONE);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    private void initVsersion() {
        Map<String, Object> keyValues = new HashMap<>();
        new VersionApi().getCallBack(mContext, keyValues, new BaseCallBackListener<VersionJCData>() {
            @Override
            public void onSuccess(VersionJCData vrData) {
                vserCodeHttp = vrData.getVer();
                apkUrl = vrData.getUrl();
                Log.e(TAG, "vserCodeHttp == " + vserCodeHttp);
                Log.e(TAG, "apkUrl == " + apkUrl);

                String vserIntNet = vserCodeHttp.replace(".", "");
                int vserInt = Integer.parseInt(vserIntNet);
                vserHttpInt = vserInt;

                String versionName = Utils.getVersionName();
                String versionCode = versionName.length() == 3 ? versionName + "0" : versionName;
                int vserIntLoc1 = Integer.parseInt(versionCode);
                vserLocInt = vserIntLoc1;
                vserHttpInt = vserInt;

                Log.e(TAG, "vserLocInt == " + vserLocInt);
                Log.e(TAG, "vserHttpInt == " + vserHttpInt);

                if (vserIntLoc1 < vserInt) {
                    ivVrJc.setVisibility(View.VISIBLE);
                    versionsssTv.setVisibility(View.GONE);
                } else {
                    ivVrJc.setVisibility(View.GONE);
                    versionsssTv.setVisibility(View.VISIBLE);
                }
            }

        });
    }

    void initQianDaoOpen() {
        switchFlag = Cfg.loadStr(mContext, FinalConstant.SWITTCH, "");
        if (switchFlag.equals("0")) {
            setQianDaoOpenTv.setText("已关闭");
        } else {
            setQianDaoOpenTv.setText("已开启");
        }
    }

    void initOpen() {

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("flag", "1");

        new DisturbApi().getCallBack(mContext, keyValues, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                try {
                    BotherData data = JSONUtil.TransformSingleBean(serverData.data, BotherData.class);
                    type = data.getType();

                    if ("2".equals(type)) {
                        setBotherOpenTv.setText("已关闭");
                    } else {
                        setBotherOpenTv.setText("已开启");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.set_switch_message_rly:// 消息开关
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent it0 = new Intent();
                        it0.setClass(mContext, SwitchRemindMeActivity.class);
                        startActivity(it0);
                        finish();
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.set_switch_bother_rly:// 打扰开关
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent it1 = new Intent();
                        it1.setClass(mContext, SwitchBotherMeActivity.class);
                        startActivity(it1);
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.set_switch_qiandao_rly:// 签到开关
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        Intent it1 = new Intent();
                        it1.setClass(mContext, SwitchQianDaoActivity.class);
                        startActivity(it1);
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }

                } else {
                    Utils.jumpLogin(mContext);
                }
                break;
            case R.id.set_to_yuemei:
                mFunctionManager.goToActivity(Page400Activity.class);
                break;
            case R.id.set_to_weixin:
                break;
            case R.id.set_to_app:
                break;
            case R.id.set_to_feedback:
                mFunctionManager.goToActivity(FeedbackActivity.class);
                break;
            case R.id.set_to_aboutour:// 关于悦美
                mFunctionManager.goToActivity(YueMeiWebAboutActivity.class);
                break;
            case R.id.vision_jiance_rly:
                CheckVersionTask cv = new CheckVersionTask();
                new Thread(cv).start();
                break;
            case R.id.qingchu_huancun_rly:// 清除缓存
                Log.e(TAG, "mPath == " + mPath);
                DataCleanManager.deleteFolderFile(mPath, false);

                cleanDataTv.setText("清理中···");
                new CountDownTimer(3000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        cleanDataTv.setText(getCacheSize());
                    }
                }.start();
                break;
            case R.id.hot_app_rly:
                mFunctionManager.goToActivity(AppRecommendActivity.class);
                break;
            case R.id.exit_login_bt:// 退出登录
                showDialog();
                break;
            case R.id.set_to_share_rly:// 分享给好友
                // 分享文案
                setShare();
                break;
            case R.id.set_to_license_rly:// 经营执照
                Log.e(TAG, "经营执照");
                WebData webData6751 = new WebData(FinalConstant1.BASE_VER_URL+"/homenew/businesslicense/");
                webData6751.setShowRefresh(false);
                WebUtil.getInstance().startWebActivity(mContext, webData6751);
                break;
            case R.id.privacy_settings:
                //隐私设置
                PrivacySettingsActivity.invoke(mContext);
                break;
            case R.id.user_privite_rly:
                Intent it = new Intent();
                it.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,0);
                it.setClass(mContext, UserAgreementWebActivity.class);
                startActivity(it);
                break;
            case R.id.user_protoclo_rly:
                Intent it1 = new Intent();
                it1.putExtra(UserAgreementWebActivity.mIsYueMeiAgremment,1);
                it1.setClass(mContext, UserAgreementWebActivity.class);
                startActivity(it1);
                break;
        }

    }

    /**
     * 获取缓存大小
     *
     * @return
     */
    private String getCacheSize() {
        String sm2;
        try {
            sm2 = DataCleanManager.getCacheSize(new File(mPath));
            if ("0.0Byte".equals(sm2)) {
                sm2 = "0.0M";
            }
            Log.e(TAG, "sm2 111= " + sm2);
        } catch (Exception e) {
            e.printStackTrace();
            sm2 = "0.0M";
        }
        return sm2;
    }

    /**
     * 设置分享
     */
    private void setShare() {
        shareContent = "小伙伴们，我发现了一款很棒的美容整形APP，双眼皮，隆鼻，隆胸十一项整形面面聚到，优惠尺度满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒~，想变美的妹纸们不！要！错！过！ 点击打开APP下载页";
        shareUrl = "http://m.yuemei.com/app/ym.html";
        String sinaText = "小伙伴们，我发现了一款很棒的美容整形APP[太开心]，双眼皮，隆鼻，隆胸十一项整形面面聚到，优惠尺度满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒~，想变美的妹纸们不！要！错！过！[偷乐]APP下载地址：http://m.yuemei.com/app/ym.html（分享自@悦美整形APP）";
        String smsText = "我发现了一款很棒的美容整形APP，整形优惠满意到cry，整形不满意，悦美先行赔付哦，安全感满格哒，快去看看吧~ APP下载地址：http://m.yuemei.com/app/ym.html";
        BaseShareView baseShareView = new BaseShareView(mContext);
        baseShareView.setShareContent(shareContent).ShareAction();

        baseShareView.getShareBoardlistener()
                .setSinaText(sinaText)
                .setSinaThumb(new UMImage(mContext, R.drawable.share_log))
                .setSmsText(smsText).setTencentUrl(shareUrl).setTencentTitle(shareContent).setTencentThumb(new UMImage(mContext, R.drawable.ic_launcher)).setTencentDescription(shareContent).setTencentText(shareContent).getUmShareListener().setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {

                if (!platform.equals(SHARE_MEDIA.SMS)) {
                    Toast.makeText(mContext, " 分享成功啦", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {

            }
        });
    }

    void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        View view = View.inflate(mContext, R.layout.dialog_exit_login, null);
        builder.setView(view);
        builder.setPositiveButton("确定", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Utils.clearUserData();
                initShenHe();
                finish();
            }
        });
        builder.setNegativeButton("取消", new Dialog.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        builder.create().show();
    }


    private void initShenHe() {
        new ShenHeApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {

            @Override
            public void onSuccess(ShenHeData shenHeData) {
                if (shenHeData != null) {
                    Log.e(TAG,"nologinchat == "+shenHeData.getNologinchat());
                    Cfg.saveStr(mContext, SplashActivity.NO_LOGIN_CHAT,shenHeData.getNologinchat());
                    NoLoginBean user = shenHeData.getUser();
                    if (null != user){
                        String userId = user.getUser_id();//游客用户id
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_ID,userId);
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_IMG,user.getImg());
                    }else {
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_ID,"");
                    }

                }
            }
        });
    }

    /*
     * 从服务器获取xml解析并进行比对版本号
     */
    public class CheckVersionTask implements Runnable {

        public void run() {
            try {
                Log.e(TAG, "vserLocInt == " + vserLocInt);
                Log.e(TAG, "vserHttpInt == " + vserHttpInt);
                if (vserLocInt >= vserHttpInt) {
                    Message msg = new Message();
                    msg.what = UPDATA_NONEED;
                    handler.sendMessage(msg);
                } else {
                    Message msg = new Message();
                    msg.what = UPDATA_CLIENT;
                    handler.sendMessage(msg);
                }
            } catch (Exception e) {
                // 待处理
                Message msg = new Message();
                msg.what = GET_UNDATAINFO_ERROR;
                handler.sendMessage(msg);
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATA_NONEED:
                    new DialogPopupwindows(SelfSettingActivity.this, mSildingFinishLayout);
                case UPDATA_CLIENT:
                    if (vserLocInt >= vserHttpInt) {// vserIntLoc1 < vserInt

                    } else {
                        new DialogUpdatePopupwindows(SelfSettingActivity.this, mSildingFinishLayout);
                    }
                    break;
                case GET_UNDATAINFO_ERROR:
                    // 服务器超时
                    Toast.makeText(getApplicationContext(), "获取服务器更新信息失败", Toast.LENGTH_SHORT).show();
                    break;
                case SDCARD_NOMOUNTED:
                    // sdcard不可用
                    Toast.makeText(getApplicationContext(), "SD卡不可用", Toast.LENGTH_SHORT).show();
                    break;
                case DOWN_ERROR:
                    // 下载apk失败
                    Toast.makeText(getApplicationContext(), "下载新版本失败", Toast.LENGTH_SHORT).show();
                    // LoginMain();
                    break;
            }
        }
    };

    ProgressBar pd;
    Button cancelBt;

    /*
     * 从服务器中下载APK
     */
    protected void downLoadApk() {
        final ProgDialog editDialog = new ProgDialog(mContext, R.style.mystyle, R.layout.dialog_progress);
        editDialog.setCanceledOnTouchOutside(false);

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Message msg = new Message();
            msg.what = SDCARD_NOMOUNTED;
            handler.sendMessage(msg);
        } else {
            editDialog.show();
            pd = editDialog.findViewById(R.id.progress_aaaaaa);

            cancelBt = editDialog.findViewById(R.id.cancel_btn_aa);

            cancelBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    editDialog.dismiss();
                    interceptFlag = true;
                }
            });

            new Thread() {
                @Override
                public void run() {
                    try {
                        File file = getFileFromServer(apkUrl, pd);
                        // Log.e("ProgressDialog",
                        // "ProgressDialog==" + pd.toString());
                        sleep(1000);
                        if (!interceptFlag) {
                            installApk(file);
                        }
                        editDialog.dismiss(); // 结束掉进度条对话框
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        Message msg = new Message();
                        msg.what = DOWN_ERROR;
                        handler.sendMessage(msg);
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    // 安装apk
    protected void installApk(File file) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {//判读版本是否在7.0以上
            Uri apkUri = FileProvider.getUriForFile(this, getPackageName() + ".FileProvider", file);//在AndroidManifest中的android:authorities值
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.setDataAndType(apkUri, "application/vnd.android.package-archive");
            startActivity(install);
        } else {

            Intent intent = new Intent();
            // 执行动作
            intent.setAction(Intent.ACTION_VIEW);
            // 执行的数据类型
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            startActivity(intent);
        }

    }

    public static File getFileFromServer(String path, ProgressBar pd) throws Exception {
        // 如果相等的话表示当前的sdcard挂载在手机上并且是可用的
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.setConnectTimeout(5000);

            // Log.e("ProgressDialog", "ProgressDialog2==" + pd.toString());
            // 获取到文件的大小
            pd.setMax(conn.getContentLength());
            pd.getMax();
            Log.e("ProgressDialog", "ProgressDialog==" + conn.getContentLength() + "/pd.getMax()==" + pd.getMax());

            InputStream is = conn.getInputStream();
            File file = new File(Environment.getExternalStorageDirectory(), "updata.apk");
            FileOutputStream fos = new FileOutputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);
            byte[] buffer = new byte[1024];
            int len;
            int total = 0;
            while ((len = bis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
                total += len;
                // 获取当前下载量
                // Log.e("total", "total==" + total);
                pd.setProgress(total);
                // Log.e("Get", "getProgress==" + pd.getProgress());
                if (interceptFlag) {
                    break;
                }
            }
            fos.close();
            bis.close();
            is.close();
            return file;
        } else {
            return null;
        }
    }

    public class DialogUpdatePopupwindows extends PopupWindow {

        public DialogUpdatePopupwindows(final Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.dialog_zuixin_update_version, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);

            showAtLocation(v, Gravity.BOTTOM, 0, 0);
            update();

            Button cancleBt = view.findViewById(R.id.version_dialog_zuinew_cancel);
            cancleBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            Button sureBt = view.findViewById(R.id.version_dialog_zuinew_sure);
            sureBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            interceptFlag = false;
                            downLoadApk();
                            dismiss();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {

                        }
                    });
                }
            });
        }
    }

    //dialog提示
    void showDialogExitEdit3(String title, String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(title);

        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(content);
        cancelBt88.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    public class DialogPopupwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public DialogPopupwindows(final Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.dialog_zuixin_vsersion, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);

            showAtLocation(v, Gravity.BOTTOM, 0, 0);
            update();

            Button lingBt = view.findViewById(R.id.version_dialog_zuinew);
            lingBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);

    }
}
