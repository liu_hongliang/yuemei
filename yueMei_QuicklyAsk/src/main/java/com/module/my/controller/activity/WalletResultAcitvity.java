package com.module.my.controller.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.my.controller.other.ActivityCollector;
import com.quicklyask.activity.R;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletResultAcitvity extends YMBaseActivity {

    @BindView(R.id.wallet_result_title)
    CommonTopBar mWalletResultTitle;
    @BindView(R.id.result_btn)
    Button mTixianBtn;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wallet_result_acitvity;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
        mWalletResultTitle.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityCollector.finishAll();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick(R.id.result_btn)
    public void onViewClicked() {
        finish();
        ActivityCollector.finishAll();
    }
}
