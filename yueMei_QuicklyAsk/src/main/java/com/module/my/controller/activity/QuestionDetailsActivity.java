package com.module.my.controller.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.cookie.store.CookieStore;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.my.controller.other.QuestionDetailsWebViewClient;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyack.emoji.Expressions;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.umeng.socialize.utils.Log;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.HttpUrl;
import okhttp3.Response;


/**
 * 问题详情页
 */
public class QuestionDetailsActivity extends BaseActivity {
    private String TAG = "QuestionDetailsActivity";

    private QuestionDetailsActivity mActivity;
    private String url;
    private String mTitle;
    private RelativeLayout contentWeb;
    private LinearLayout insertExpression;
    private WebView docDetWeb;
    private String userAgent;
    private RelativeLayout queDetail;
    private RelativeLayout queCancel1;
    private Button queSumbit1;
    private EditText queContent1;
    private String questionId;
    private String domain = FinalConstant1.BASE_URL;
    private String replace;
    //表情键盘
    private LinearLayout biaoqingContentLy;
    private ImageButton closeImBt;
    private ViewPager viewPager;
    private ImageView page0;
    private ImageView page1;
    private ArrayList<GridView> grids;
    private GridView gView1;
    private int[] expressionImages;
    private String[] expressionImageNames;
    private int[] expressionImages1;
    private String[] expressionImageNames1;
    private GridView gView2;
    private BaseWebViewClientMessage baseWebViewClientMessage;
    private CommonTopBar mTop;

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_question_details);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = QuestionDetailsActivity.this;

        baseWebViewClientMessage = new BaseWebViewClientMessage(mActivity);
        baseWebViewClientMessage.setBaseWebViewClientCallback(new QuestionDetailsWebViewClient(mActivity));
        initView();
        initWebview();
        LodUrl(url);
    }

    private void initView() {
        Intent it0 = getIntent();
        url = it0.getStringExtra("url");
        mTitle = it0.getStringExtra("title");
        questionId = it0.getStringExtra("questionId");

        replace = Utils.getYuemeiInfo();

        mTop = findViewById(R.id.question_details_top);
        contentWeb = findViewById(R.id.rl_question_details);

        queDetail = findViewById(R.id.que_detail_input1_rly);
        queCancel1 = findViewById(R.id.que_web_cancel1_rly);
        queSumbit1 = findViewById(R.id.que_web_sumbit1_bt);
        queContent1 = findViewById(R.id.que_web_input_content1_et);
        insertExpression = findViewById(R.id.ll_insert_expression);

        //表情键盘
        biaoqingContentLy = findViewById(R.id.biaoqing_shuru_content_ly1);
        closeImBt = findViewById(R.id.colse_biaoqingjian_bt);
        viewPager = findViewById(R.id.viewpager);
        page0 = findViewById(R.id.page0_select);
        page1 = findViewById(R.id.page1_select);

        initExpression();

        queContent1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = queContent1.getText().toString().length();
                if (length > 100) {
                    ViewInject.toast("内容不能大于100字");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = queContent1.getText().toString().length();
                if (length >= 5 && length <= 100) {
                    queSumbit1.setEnabled(true);
                    queSumbit1.setBackgroundResource(R.drawable.btn_submit_orange12x);
                } else {
                    queSumbit1.setEnabled(false);
                    queSumbit1.setBackgroundResource(R.drawable.btn_submit_gray12x);
                }
            }
        });

        /**
         * 表情键盘显示
         */
        insertExpression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                biaoqingContentLy.setVisibility(View.VISIBLE);
            }
        });

        //关闭表情键盘
        closeImBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                biaoqingContentLy.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

    }

    /**
     * 初始化表情键盘
     */
    private void initExpression() {

        // 引入表情
        expressionImages = Expressions.expressionImgs;
        expressionImageNames = Expressions.expressionImgNames;
        expressionImages1 = Expressions.expressionImgs1;
        expressionImageNames1 = Expressions.expressionImgNames1;

        LayoutInflater inflater = LayoutInflater.from(this);
        grids = new ArrayList<>();
        gView1 = (GridView) inflater.inflate(R.layout.grid1, null);

        List<Map<String, Object>> listItems = new ArrayList<>();
        // 生成28个表情
        for (int i = 0; i < 28; i++) {
            Map<String, Object> listItem = new HashMap<>();
            listItem.put("image", expressionImages[i]);
            listItems.add(listItem);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(mActivity, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

        gView1.setAdapter(simpleAdapter);
        gView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if (arg2 == 27) {
                    // 动作按下
                    int action = KeyEvent.ACTION_DOWN;
                    // code:删除，其他code也可以，例如 code = 0
                    int code = KeyEvent.KEYCODE_DEL;
                    KeyEvent event = new KeyEvent(action, code);
                    queContent1.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                } else {
                    Bitmap bitmap = null;
                    bitmap = BitmapFactory.decodeResource(getResources(), expressionImages[arg2 % expressionImages.length]);

                    bitmap = Utils.zoomImage(bitmap, 47, 47);
                    ImageSpan imageSpan = new ImageSpan(mActivity, bitmap);

                    SpannableString spannableString = new SpannableString(expressionImageNames[arg2].substring(1, expressionImageNames[arg2].length() - 1));

                    spannableString.setSpan(imageSpan, 0, expressionImageNames[arg2].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    // 编辑框设置数据
                    queContent1.append(spannableString);

                }

            }
        });
        grids.add(gView1);

        gView2 = (GridView) inflater.inflate(R.layout.grid2, null);
        grids.add(gView2);

        // 填充ViewPager的数据适配器
        PagerAdapter mPagerAdapter = new PagerAdapter() {
            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public int getCount() {
                return grids.size();
            }

            @Override
            public void destroyItem(View container, int position, Object object) {
                ((ViewPager) container).removeView(grids.get(position));
            }

            @Override
            public Object instantiateItem(View container, int position) {
                ((ViewPager) container).addView(grids.get(position));
                return grids.get(position);
            }
        };

        viewPager.setAdapter(mPagerAdapter);
        // viewPager.setAdapter();

        viewPager.setOnPageChangeListener(new GuidePageChangeListener());
    }

    // ** 指引页面改监听器 */
    class GuidePageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // System.out.println("页面滚动" + arg0);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // System.out.println("换页了" + arg0);
        }

        @Override
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));
                    break;
                case 1:
                    page1.setImageDrawable(getResources().getDrawable(R.drawable.page_focused));
                    page0.setImageDrawable(getResources().getDrawable(R.drawable.page_unfocused));

                    List<Map<String, Object>> listItems = new ArrayList<>();

                    // 生成28个表情
                    for (int i = 0; i < 28; i++) {
                        Map<String, Object> listItem = new HashMap<>();
                        listItem.put("image", expressionImages1[i]);
                        listItems.add(listItem);
                    }

                    SimpleAdapter simpleAdapter = new SimpleAdapter(mActivity, listItems, R.layout.singleexpression, new String[]{"image"}, new int[]{R.id.image});

                    gView2.setAdapter(simpleAdapter);
                    // 表情点击
                    gView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                            if (pos == 27) {
                                // 动作按下
                                int action = KeyEvent.ACTION_DOWN;
                                // code:删除，其他code也可以，例如 code = 0
                                int code = KeyEvent.KEYCODE_DEL;
                                KeyEvent event = new KeyEvent(action, code);
                                queContent1.onKeyDown(KeyEvent.KEYCODE_DEL, event); // 抛给系统处理了
                            } else {
                                Bitmap bitmap = null;
                                bitmap = BitmapFactory.decodeResource(getResources(), expressionImages1[pos % expressionImages1.length]);
                                bitmap = Utils.zoomImage(bitmap, 47, 47);

                                ImageSpan imageSpan = new ImageSpan(mActivity, bitmap);
                                SpannableString spannableString = new SpannableString(expressionImageNames1[pos].substring(1, expressionImageNames1[pos].length() - 1));

                                spannableString.setSpan(imageSpan, 0, expressionImageNames1[pos].length() - 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                // 编辑框设置数据
                                queContent1.append(spannableString);

                                // System.out.println("edit的内容 = " +
                                // spannableString);
                            }

                        }
                    });
                    break;
            }
        }
    }


    @SuppressLint("InlinedApi")
    public void initWebview() {
        docDetWeb = new WebView(mActivity);

        docDetWeb.setHorizontalScrollBarEnabled(false);//水平滚动条不显示
        docDetWeb.setVerticalScrollBarEnabled(false); //垂直滚动条不显示
        docDetWeb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        docDetWeb.loadData("", "text/html", "UTF-8");

        docDetWeb.setLongClickable(true);
        docDetWeb.setScrollbarFadingEnabled(true);
        docDetWeb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        docDetWeb.setDrawingCacheEnabled(true);
        docDetWeb.setWebViewClient(baseWebViewClientMessage);
        docDetWeb.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                String[] strs = message.split("\n");

                showDialogExitEdit2("确定", message, result, strs.length);

                return true;
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if ("0".equals(mTitle)) {
                    mTop.setCenterText(title);
                } else {
                    mTop.setCenterText(mTitle);
                }

            }

        });

        //设置 缓存模式
        WebSettings settings = docDetWeb.getSettings();
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);

        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setLoadsImagesAutomatically(true);    //支持自动加载图片
        settings.setLoadWithOverviewMode(true);
        settings.setAllowFileAccess(true);
        settings.setSaveFormData(true);    //设置webview保存表单数据
        settings.setSavePassword(true);    //设置webview保存密码
        settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);    //设置中等像素密度，medium=160dpi
        settings.setSupportZoom(true);    //支持缩放
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存内容
        settings.setJavaScriptEnabled(true);                //支持js
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.supportMultipleWindows();
        settings.setNeedInitialFocus(true);
        // android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        userAgent = settings.getUserAgentString() + "/yuemei.com";
        settings.setUserAgentString(userAgent);

        Log.e(TAG, "contentWeb == " + contentWeb);
        Log.e(TAG, "docDetWeb == " + docDetWeb);
        contentWeb.addView(docDetWeb);
    }


    /**
     * 加载web
     */
    private void LodUrl(String url) {
        baseWebViewClientMessage.startLoading();

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("id", questionId);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url, keyValues);
        if (null != docDetWeb) {
            docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
        }

    }

    /**
     * 问题解答
     */
    public void initBottHttp() {
        queDetail.setVisibility(View.VISIBLE);
        queContent1.requestFocus();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

        /**
         * 回答问题框提交按钮
         */
        queSumbit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFileQue(queContent1.getText().toString());
            }
        });

        /**
         * 回答问题框关闭按钮
         */
        queCancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coleAnswerBox();
            }
        });
    }


    @SuppressLint("HandlerLeak")
    private Handler mHandle = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1 :
                    docDetWeb.reload();
                    break;
            }
        }
    };
    /**
     * 上传解答
     *
     * @param content
     */
    private void postFileQue(String content) {
        baseWebViewClientMessage.startLoading();
        queDetail.setVisibility(View.GONE);     //键盘隐藏
        Map<String, Object> maps = new HashMap<>();
        maps.put("id", questionId);
        maps.put("content", content);

        HttpHeaders headers = SignUtils.buildHttpHeaders(maps);
        HttpParams httpParams = SignUtils.buildHttpParam5(maps);

        CookieConfig.getInstance().setCookie(FinalConstant1.HTTPS, FinalConstant1.BASE_URL,FinalConstant1.BASE_URL);
        CookieStore cookieStore = OkGo.getInstance().getCookieJar().getCookieStore();
        HttpUrl httpUrl = new HttpUrl.Builder().scheme(FinalConstant1.HTTPS).host(FinalConstant1.BASE_URL).build();
        Cookie yuemeiinfo = new Cookie.Builder()
                .name("yuemeiinfo")
                .value(replace)
                .domain(domain)
                .build();
        cookieStore.saveCookie(httpUrl, yuemeiinfo);

        OkGo.post(FinalConstant.ANSWER_MESSAGE).cacheMode(CacheMode.DEFAULT).params(httpParams).headers(headers).execute(new StringCallback() {
            @Override
            public void onSuccess(String result, Call call, Response response) {
                android.util.Log.e(TAG, "result === " + result);
                baseWebViewClientMessage.stopLoading();
                String code = JSONUtil.resolveJson(result, FinalConstant.CODE);
                String message = JSONUtil.resolveJson(result, FinalConstant.MESSAGE);
                ViewInject.toast(message);
                if ("1".equals(code)) {
                    coleAnswerBox();
                    queContent1.setText("");

                    mHandle.sendEmptyMessageDelayed(1,1000);
                }
            }

            @Override
            public void onError(Call call, Response response, Exception e) {
                super.onError(call, response, e);
                Log.e(TAG, "call === " + call);
                Log.e(TAG, "response === " + response);
                Log.e(TAG, "e === " + e);
            }
        });
    }

    /**
     * 关闭解答框
     */
    private void coleAnswerBox() {
        queDetail.setVisibility(View.GONE);
        View view = getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    /**
     * dialog提示
     *
     * @param title
     * @param content
     * @param num
     */
    void showDialogExitEdit2(String title, String content, final JsResult result, int num) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.mystyle);

        // 不需要绑定按键事件
        // 屏蔽keycode等于84之类的按键
        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        // 禁止响应按back键的事件
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();

        result.confirm();       //关闭js的弹窗
        dialog.show();          //显示自己的弹窗

        dialog.getWindow().setContentView(R.layout.dilog_newuser_yizhuce1);


        TextView titleTv77 = dialog.getWindow().findViewById(R.id.dialog_exit_content_tv);
        titleTv77.setText(content);
        titleTv77.setHeight(Utils.sp2px(17) * num + Utils.dip2px(mActivity, 10));

        Button cancelBt88 = dialog.getWindow().findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText(title);
        cancelBt88.setTextColor(0xff1E90FF);
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

    }


}
