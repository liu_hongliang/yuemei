package com.module.my.controller.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.CompoundButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.quicklyask.activity.R;
import com.quicklyask.entity.OrderInfoData;

import java.util.List;

public class ApplyMoneyBackAdapter extends BaseQuickAdapter<OrderInfoData,BaseViewHolder> {

    Context mContext;
    public ApplyMoneyBackAdapter(Context context,int layoutResId, @Nullable List<OrderInfoData> data) {
        super(layoutResId, data);
        mContext=context;
    }
    OrderInfoData mOrderInfoData;

    @Override
    protected void convert(BaseViewHolder helper, final OrderInfoData item) {
        mOrderInfoData=item;
        helper.setText(R.id.money_back_item_title,item.getTitle());
        helper.setText(R.id.money_back_item_status,item.getStatus_title());
        helper.setText(R.id.money_back_item_code,item.getServer_id());
        helper.setChecked(R.id.money_back_item_check,item.isIschecked());
        helper.setOnCheckedChangeListener(R.id.money_back_item_check, new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                item.setIschecked(isChecked);
            }
        });
        if ("1".equals(item.getIs_refund())){
            helper.setVisible(R.id.money_back_item_check,true);
            helper.setVisible(R.id.money_back_item_tip,false);
        }else {
            helper.setVisible(R.id.money_back_item_check,false);
            helper.setVisible(R.id.money_back_item_tip,true);
        }
    }

    //点击item选中CheckBox
    public void setSelectItem(int position) {
        //对当前状态取反
        if (mOrderInfoData.isIschecked()) {
            mOrderInfoData.setIschecked(false);
        } else {
            mOrderInfoData.setIschecked(true);
        }
        notifyItemChanged(position);
    }
}
