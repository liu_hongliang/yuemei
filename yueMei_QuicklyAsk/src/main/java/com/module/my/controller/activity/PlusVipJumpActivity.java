package com.module.my.controller.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.refresh.MyPullRefresh;
import com.module.base.refresh.refresh.RefreshListener;
import com.module.base.view.YMBaseWebViewActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.BaseWebViewReload;
import com.module.my.model.api.MemberUsersureaddressApi;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMMin;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;

public class PlusVipJumpActivity extends YMBaseWebViewActivity {

    @BindView(R.id.plus_vip_jump_top)
    CommonTopBar mTop;                              //头标题
    @BindView(R.id.activity_plus_vip_jump)
    MyPullRefresh mPlusVipJump;
    private BaseWebViewClientMessage clientManager;
    private String TAG = "PlusVipJumpActivity";
    private String mUrl;
    private MemberUsersureaddressApi memberUsersureaddressApi;
    private HashMap<String, Object> hashMap = new HashMap<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plus_vip_jump;
    }

    @Override
    protected void initView() {
        super.initView();

        String mLink = getIntent().getStringExtra("link");
        String[] split6491 = mLink.split("/");
        for (int i = 3; i < split6491.length; i += 2) {
            if (i + 1 <= split6491.length) {
                hashMap.put(split6491[i], split6491[i + 1]);
            }
        }

        mUrl = FinalConstant.baseUrl + FinalConstant.VER+"/" + mLink;
        mPlusVipJump.addView(mWebView);
        loadUrl();

        mPlusVipJump.setRefreshListener(new RefreshListener() {
            @Override
            public void onRefresh() {
                loadUrl();
            }
        });

        //返回按钮回调
        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void initData() {
        memberUsersureaddressApi = new MemberUsersureaddressApi();

        //公共跳转外的跳转
        clientManager = new BaseWebViewClientMessage(mContext);
        clientManager.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String url) {
                try {
                    showWebDetail(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        clientManager.setBaseWebViewReload(new BaseWebViewReload() {
            @Override
            public void reload() {
                loadUrl();
            }
        });

        if (mUrl.contains("/coupons/center/")) {
            mTop.setRightImgVisibility(View.VISIBLE);

            mTop.setRightImageClickListener(new CommonTopBar.ClickCallBack() {
                @Override
                public void onClick(View v) {
                    setShare();
                }
            });
        } else {
            mTop.setRightImgVisibility(View.GONE);
        }
    }

    @Override
    protected boolean ymShouldOverrideUrlLoading(WebView view, String url) {
        WebView.HitTestResult hitTestResult = view.getHitTestResult();
        int hitType = hitTestResult.getType();
        Log.e(TAG, "hitTestResult == " + hitTestResult);
        Log.e(TAG, "hitType == " + hitType);
        Log.e(TAG, "url == " + url);

        if (hitType != WebView.HitTestResult.UNKNOWN_TYPE) {
            if (url.startsWith("type")) {
                clientManager.showWebDetail(url);
            } else {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "0", "0");
            }
            return true;
        }
        return super.ymShouldOverrideUrlLoading(view, url);
    }

    @Override
    protected void onYmReceivedTitle(WebView view, String title) {
        super.onYmReceivedTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            mTop.setCenterText(title);
        } else {
            mTop.setCenterText("PLUS会员权益");
        }
    }

    @Override
    protected void onYmProgressChanged(WebView view, int newProgress) {
        if (newProgress == 100) {
            mPlusVipJump.finishRefresh();
        }
        super.onYmProgressChanged(view, newProgress);
    }

    private void showWebDetail(String urlStr) throws Exception {
        Log.e(TAG, "urlStr == " + urlStr);
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "531":  // 本页面跳转本页面
                hashMap.clear();
                String link531 = obj.getString("link");
                Log.e(TAG, "link531 == " + link531);

                String[] split6491 = link531.split("/");
                for (int i = 3; i < split6491.length; i += 2) {
                    if (i + 1 <= split6491.length) {
                        hashMap.put(split6491[i], split6491[i + 1]);
                    }
                }

                mUrl = FinalConstant.baseUrl + FinalConstant.VER + link531;
                loadUrl();

                break;

            case "64991":
                Log.e("tamade", "64991 hahahah");
                String link64991 = obj.getString("link");
                String url64991 = FinalConstant.baseUrl + FinalConstant.VER + link64991;
                String[] split64991 = link64991.split("/");
                HashMap<String, Object> mSingStr64991 = new HashMap<>();
                mSingStr64991.put("flag", split64991[4]);
                loadUrl(mWebView, url64991, mSingStr64991);
                break;
            case "6493":            //确认收货地址

                memberUsersureaddressApi.addData("address_id", obj.getString("address_id"));
                memberUsersureaddressApi.addData("id", obj.getString("id"));
                memberUsersureaddressApi.getCallBack(mContext, memberUsersureaddressApi.getUsersureaddresstHashMap(), new BaseCallBackListener<ServerData>() {

                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            Intent intent = new Intent();
                            setResult(102, intent);
                            onBackPressed();
                        }
                    }
                });
                break;
        }
    }

    /**
     * 加载url
     */
    private void loadUrl() {

        Log.e(TAG, "mUrl === " + mUrl);
        Log.e(TAG, "hashMap === " + hashMap.toString());
        loadUrl(mUrl, hashMap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        switch (requestCode) {
            case 100:
                if (resultCode == 101) {
                    loadUrl();
                }
                break;
            case 105:
                if (resultCode == 102) {
                    loadUrl();
                }
                break;
        }
    }

    /**
     * 分享设置
     */
    private void setShare() {
        MyUMShareListener myUMShareListener = new MyUMShareListener(mContext);
        boolean installWeiXin = UMShareAPI.get(mContext).isInstall(mContext, SHARE_MEDIA.WEIXIN);

        if (!installWeiXin) {     //微信不存在
            Toast.makeText(mContext, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        } else {
            UMMin umMin = new UMMin("https://m.yuemei.com/");
            umMin.setThumb(new UMImage(mContext, "https://p11.yuemei.com/tag/1537175266b8740.jpg"));
            umMin.setTitle("领券中心");
            umMin.setDescription("海量神券免费领，变美再也不用担心剁手啦");
            umMin.setPath("pages/lottery-active/get-coupon/get-coupon");
            umMin.setUserName("gh_4ce0355fc6ab");
            new ShareAction(mContext).withMedia(umMin).setPlatform(SHARE_MEDIA.WEIXIN).setCallback(myUMShareListener).share();
        }

        myUMShareListener.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                mFunctionManager.showShort("分享成功");
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                mFunctionManager.showShort("分享失败");
            }
        });
    }

    public void loadUrl(WebView webView, String mUrl, HashMap<String, Object> mSingStr) {
        Log.e(TAG, "url111====" + mUrl);
        Log.e(TAG, "mSingStr====" + mSingStr);
        WebSignData addressAndHead = SignUtils.getAddressAndHead(mUrl, mSingStr);
        Log.e(TAG, "url====" + addressAndHead.getUrl());
        Log.e(TAG, "httpHeaders====" + addressAndHead.getHttpHeaders());
        webView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(100, intent);
        super.onBackPressed();
    }
}
