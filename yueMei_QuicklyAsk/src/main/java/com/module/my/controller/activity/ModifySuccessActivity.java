/**
 * 
 */
package com.module.my.controller.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.quicklyask.activity.R;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

/**
 * 修改密码成功
 * 
 * @author Robin
 * 
 */
public class ModifySuccessActivity extends BaseActivity {

	private final String TAG = "ModifySuccessActivity";

	private Context mContext;

	@BindView(id = R.id.suess_smiles_bt, click = true)
	private Button suessBt;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_modify_suess);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = ModifySuccessActivity.this;
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.suess_smiles_bt:
			finish();
			break;
		}
	}
}
