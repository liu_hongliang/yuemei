package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.SaveorderiData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 购买会员
 * Created by 裴成浩 on 2018/9/6.
 */
public class SaveorderiApi implements BaseCallBackApi {
    private String TAG = "SaveorderiApi";
    private HashMap<String, Object> mSaveorderiHashMap;  //传值容器

    public SaveorderiApi() {
        mSaveorderiHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.MEMBER, "saveorder", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                try {
                    if ("1".equals(mData.code)) {
                        SaveorderiData getaddressData = JSONUtil.TransformSingleBean(mData.data, SaveorderiData.class);
                        listener.onSuccess(getaddressData);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }
            }
        });
    }

    public HashMap<String, Object> getSaveorderiHashMap() {
        return mSaveorderiHashMap;
    }

    public void addData(String key, String value) {
        mSaveorderiHashMap.put(key, value);
    }
}
