package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.my.model.bean.TypeProblemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 选择问题类型接口
 * Created by 裴成浩 on 2019/3/18
 */
public class TypeProblemApi implements BaseCallBackApi {
    private String TAG = "TypeProblemApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public TypeProblemApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.FORUM, "asktype", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData == " + mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        List<TypeProblemData> typeProblemData = JSONUtil.jsonToArrayList(mData.data, TypeProblemData.class);
                        listener.onSuccess(typeProblemData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
