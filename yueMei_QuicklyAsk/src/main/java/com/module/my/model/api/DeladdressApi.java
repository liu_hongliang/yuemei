package com.module.my.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;

import java.util.HashMap;
import java.util.Map;

/**
 * 删除收货地址
 * Created by 裴成浩 on 2018/9/6.
 */
public class DeladdressApi implements BaseCallBackApi {
    private String TAG = "DeladdressApi";
    private HashMap<String, Object> mDeladdressApiHashMap;  //传值容器

    public DeladdressApi() {
        mDeladdressApiHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.USERNEW, "deladdress", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getDeladdressHashMap() {
        return mDeladdressApiHashMap;
    }

    public void addData(String key, String value) {
        mDeladdressApiHashMap.put(key, value);
    }
}
