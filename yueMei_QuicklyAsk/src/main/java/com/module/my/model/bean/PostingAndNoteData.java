package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteData {
    private boolean isButton  = false;   //是否是按钮，默认不是按钮
    private PostingAndNoteUpload postingAndNoteUpload;
    private PostingAndNoteButton postingAndNoteButton;

    public boolean isButton() {
        return isButton;
    }

    public void setButton(boolean button) {
        isButton = button;
    }

    public PostingAndNoteUpload getPostingAndNoteUpload() {
        return postingAndNoteUpload;
    }

    public void setPostingAndNoteUpload(PostingAndNoteUpload postingAndNoteUpload) {
        this.postingAndNoteUpload = postingAndNoteUpload;
    }

    public PostingAndNoteButton getPostingAndNoteButton() {
        return postingAndNoteButton;
    }

    public void setPostingAndNoteButton(PostingAndNoteButton postingAndNoteButton) {
        this.postingAndNoteButton = postingAndNoteButton;
    }
}
