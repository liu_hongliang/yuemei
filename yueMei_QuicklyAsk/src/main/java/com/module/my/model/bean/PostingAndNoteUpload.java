package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteUpload {
    private boolean isVideo = false;            //是否是视频 true: 视频，flase:图片。默认是图片
    private PostingAndNoteImage postingAndNoteImage;
    private PostingAndNoteVideo postingAndNoteVideo;
    private FileUploadState fileUploadState;      //上传状态
    private int progress;                       //上传进度

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public PostingAndNoteImage getPostingAndNoteImage() {
        return postingAndNoteImage;
    }

    public void setPostingAndNoteImage(PostingAndNoteImage postingAndNoteImage) {
        this.postingAndNoteImage = postingAndNoteImage;
    }

    public PostingAndNoteVideo getPostingAndNoteVideo() {
        return postingAndNoteVideo;
    }

    public void setPostingAndNoteVideo(PostingAndNoteVideo postingAndNoteVideo) {
        this.postingAndNoteVideo = postingAndNoteVideo;
    }

    public FileUploadState getFileUploadState() {
        return fileUploadState;
    }

    public void setFileUploadState(FileUploadState fileUploadState) {
        this.fileUploadState = fileUploadState;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public enum FileUploadState {
        NOT_UPLOAD,                 //未上传
        UPLOADING,                  //上传中
        UPLOADED_SUCCESSFULLY,      //上传成功
        UPLOAD_FAILED               //上传失败
    }
}
