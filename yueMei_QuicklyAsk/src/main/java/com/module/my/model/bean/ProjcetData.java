package com.module.my.model.bean;

import android.support.annotation.NonNull;

import java.util.List;

public class ProjcetData {

//    private List<ProjcetSXitem> kind;
//    private List<ProjcetSXitem> method;

    private int id;
    private String name;
    private List<ProjcetList> list;
    private String default_select_id;
    private String single_or_many;

    public String getSingle_or_many() {
        return single_or_many;
    }

    public void setSingle_or_many(String single_or_many) {
        this.single_or_many = single_or_many;
    }

    public String getDefault_select_id() {
        return default_select_id;
    }

    public void setDefault_select_id(String default_select_id) {
        this.default_select_id = default_select_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProjcetList> getList() {
        return list;
    }

    public void setList(List<ProjcetList> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
