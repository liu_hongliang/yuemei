package com.module.my.model.bean;

/**
 * 发提问发日记发随聊上传数据实体类
 * Created by 裴成浩 on 2018/8/14.
 */
public class ForumShareData {

    private String title = "";                       //日记本标题
    private String id = "0";                          //日记本id
    private String hosname = "";                     //医院名
    private String docname = "";                     //医生名
    private String cateid = "";                      //标签id串
    private String hosid = "0";                       //医院id
    private String userid = "0";                      //医生id
    private String taoid = "0";                       //淘id
    private String server_id = "0";                   //订单号    为了不改客户端，这块是代表order_id
    private String fee = "0";                         //消费金额
    private String sharetime = "0";                   //体验时间
    private String effect = "0";                      //环境分
    private String pf_doctor = "0";                   //医生分
    private String service = "0";                     //服务分
    private String content = "";                     //内容
    private String visibility = "0";                 //图片/视频仅医生可见  仅医生可见是1,默认是不可见的
    private String askorshare;                       //0：提问，1： 日记 ，  4：随聊
    private String image = "";                       //图片json 【包含字段：width,height,img】
    private String consumer_voucher = "";            //消费凭证json【包含字段：width,height,img】
    private String beforeimage = "";                 //术前照片json【包含字段：width,height,img】
    private String video = "";                       //视频json【包含字段：cover,video_time,width,height,img】
    private String compare = "";                     //对比图json【包含字段：cover,video_time,cover_width,cover_height】
    private String surgeryafterdays = "0";            //术后天数
    private String cover_photo = "";                 //问题 有图则存在，随聊必存在
    private String is_sixin = "0";                    //是否同时发私信，0：不发，1发
    private String vote_tao = "";                     //投票贴tao_ID 逗号拼接
    private String vote_title = "";                   //投票贴选项标题
    private String vote_type = "";                    //投票贴类型（1单选，2多选）
    private String is_push = "1";                     //投票帖是否开始推送0否1是
    private String vote_text = "";                    //普通投票

    public String getVote_text() {
        return vote_text;
    }

    public void setVote_text(String vote_text) {
        this.vote_text = vote_text;
    }

    public String getVote_tao() {
        return vote_tao;
    }

    public void setVote_tao(String vote_tao) {
        this.vote_tao = vote_tao;
    }

    public String getVote_title() {
        return vote_title;
    }

    public void setVote_title(String vote_title) {
        this.vote_title = vote_title;
    }

    public String getVote_type() {
        return vote_type;
    }

    public void setVote_type(String vote_type) {
        this.vote_type = vote_type;
    }

    public String getIs_push() {
        return is_push;
    }

    public void setIs_push(String is_push) {
        this.is_push = is_push;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHosname() {
        return hosname;
    }

    public void setHosname(String hosname) {
        this.hosname = hosname;
    }

    public String getDocname() {
        return docname;
    }

    public void setDocname(String docname) {
        this.docname = docname;
    }

    public String getCateid() {
        return cateid;
    }

    public void setCateid(String cateid) {
        this.cateid = cateid;
    }

    public String getHosid() {
        return hosid;
    }

    public void setHosid(String hosid) {
        this.hosid = hosid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTaoid() {
        return taoid;
    }

    public void setTaoid(String taoid) {
        this.taoid = taoid;
    }

    public String getServer_id() {
        return server_id;
    }

    public void setServer_id(String server_id) {
        this.server_id = server_id;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getSharetime() {
        return sharetime;
    }

    public void setSharetime(String sharetime) {
        this.sharetime = sharetime;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getPf_doctor() {
        return pf_doctor;
    }

    public void setPf_doctor(String pf_doctor) {
        this.pf_doctor = pf_doctor;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getConsumer_voucher() {
        return consumer_voucher;
    }

    public void setConsumer_voucher(String consumer_voucher) {
        this.consumer_voucher = consumer_voucher;
    }

    public String getBeforeimage() {
        return beforeimage;
    }

    public void setBeforeimage(String beforeimage) {
        this.beforeimage = beforeimage;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCompare() {
        return compare;
    }

    public void setCompare(String compare) {
        this.compare = compare;
    }

    public String getSurgeryafterdays() {
        return surgeryafterdays;
    }

    public void setSurgeryafterdays(String surgeryafterdays) {
        this.surgeryafterdays = surgeryafterdays;
    }

    public String getCover_photo() {
        return cover_photo;
    }

    public void setCover_photo(String cover_photo) {
        this.cover_photo = cover_photo;
    }

    public String getIs_sixin() {
        return is_sixin;
    }

    public void setIs_sixin(String is_sixin) {
        this.is_sixin = is_sixin;
    }
}
