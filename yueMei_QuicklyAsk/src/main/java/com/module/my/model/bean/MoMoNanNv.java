package com.module.my.model.bean;

import java.util.List;

/**
 * Created by dwb on 17/3/28.
 */

public class MoMoNanNv {

    private List<HbFenqiItem> girl;
    private List<HbFenqiItem> boy;

    public List<HbFenqiItem> getGirl() {
        return girl;
    }

    public void setGirl(List<HbFenqiItem> girl) {
        this.girl = girl;
    }

    public List<HbFenqiItem> getBoy() {
        return boy;
    }

    public void setBoy(List<HbFenqiItem> boy) {
        this.boy = boy;
    }
}
