package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/9/6.
 */
public class GetaddressData {
    private String name;                //联系人
    private String province;            //省id
    private String city;                //市id
    private String address;             //详细地址
    private String phone;               //手机号
    private String tel;                 //400电话


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
