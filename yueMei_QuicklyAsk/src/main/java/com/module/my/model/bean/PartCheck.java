package com.module.my.model.bean;

/**
 * Created by dwb on 16/11/15.
 */
public class PartCheck {

    private String id;
    private String name;
    private String checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
