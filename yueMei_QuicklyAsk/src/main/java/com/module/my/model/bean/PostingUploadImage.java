package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/14.
 */
public class PostingUploadImage {
    private int width;                  //上传的本地图片宽度
    private int height;                 //上传的本地图片高度
    private String img;              //网络存储路径

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
