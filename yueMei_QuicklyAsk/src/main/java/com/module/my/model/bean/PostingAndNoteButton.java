package com.module.my.model.bean;

/**
 * Created by 裴成浩 on 2018/8/3.
 */
public class PostingAndNoteButton {
    private boolean isVideoButton = false;  //是否是视频按钮

    public boolean isVideoButton() {
        return isVideoButton;
    }

    public void setVideoButton(boolean videoButton) {
        isVideoButton = videoButton;
    }

    public String getContent() {
        if (isVideoButton) {
            return "添加视频";
        } else {
            return "添加图片";
        }
    }

}
