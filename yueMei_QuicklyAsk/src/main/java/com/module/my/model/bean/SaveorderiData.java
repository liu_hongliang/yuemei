package com.module.my.model.bean;

/**
 * 购买会员下单实体类
 * Created by 裴成浩 on 2018/9/6.
 */
public class SaveorderiData {
    private String server_id;
    private String order_id;
    private String is_insure;
    private String is_group;
    private String group_id;
    private String order_time;
    private String money;
    private String is_repayment;
    private String is_repayment_mimo;
    private String price;
    private String member_lijian_money;
    private String service;

    public String getServer_id() {
        return server_id;
    }

    public void setServer_id(String server_id) {
        this.server_id = server_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getIs_insure() {
        return is_insure;
    }

    public void setIs_insure(String is_insure) {
        this.is_insure = is_insure;
    }

    public String getIs_group() {
        return is_group;
    }

    public void setIs_group(String is_group) {
        this.is_group = is_group;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getIs_repayment() {
        return is_repayment;
    }

    public void setIs_repayment(String is_repayment) {
        this.is_repayment = is_repayment;
    }

    public String getIs_repayment_mimo() {
        return is_repayment_mimo;
    }

    public void setIs_repayment_mimo(String is_repayment_mimo) {
        this.is_repayment_mimo = is_repayment_mimo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMember_lijian_money() {
        return member_lijian_money;
    }

    public void setMember_lijian_money(String member_lijian_money) {
        this.member_lijian_money = member_lijian_money;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
