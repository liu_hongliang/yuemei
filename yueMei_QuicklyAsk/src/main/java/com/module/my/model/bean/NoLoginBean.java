package com.module.my.model.bean;

public class NoLoginBean {

    /**
     * id : 1583558994915
     * user_id : 1583558994915
     * nickname : 游客994915
     * addtime : 1583558994
     * from : 0
     * openid : null
     * uvcookie : 867391035524920
     * group_id : 1
     */

    private String id;
    private String user_id;
    private String nickname;
    private String addtime;
    private String from;
    private Object openid;
    private String uvcookie;
    private String img;
    private int group_id;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Object getOpenid() {
        return openid;
    }

    public void setOpenid(Object openid) {
        this.openid = openid;
    }

    public String getUvcookie() {
        return uvcookie;
    }

    public void setUvcookie(String uvcookie) {
        this.uvcookie = uvcookie;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }
}
