package com.module.my.model.bean;

import java.util.List;

/**
 * 用户信息
 * 
 * @author Rubin
 * 
 */
public class UserData {


	/**
	 * _id : xx
	 * img : xx
	 * nickname : xx
	 * sex : xx
	 * birthday : xx
	 * province : xx
	 * city : xx
	 * evaluate : xx
	 * stringegral : xx
	 * level : xx
	 * alipay : xx
	 * real_name : xx
	 * card_id : xx
	 * full_name : xx
	 * mobile : xx
	 * doparts : xx
	 * parts : xx
	 * mynumber : xx
	 * talent : xx
	 * yuemeiinfo : xx
	 * group_id : xx
	 * loginphone : xx
	 * phone : xx
	 * isphone : xx
	 * yqma : xx
	 * sixin : xx
	 * postnum : xx
	 * replynum : xx
	 * is_show : xx
	 * is_signin : xx
	 */
	  private String  _id;  //	用户id
	  private String  img	;//	用户头像
	  private String  nickname;//	用户昵称
	  private String  sex;//	用户性别 1:男;2:女
	  private String  birthday;//	用户生日
	  private String  province;//	用户所在省
	  private String  city;//	用户所在市
	  private String  evaluate;//	用户待评价
	  private String  stringegral;//	用户积分
	  private String  level	;//	用户等级
	  private String  alipay;//	用户支付宝帐号
	  private String  real_name	;//	用户真实姓名
	  private String  card_id	;//	用户省份证
	  private String  full_name	;//	用户全名
	  private String  mobile	;//	用户手机
	private List<Doparts> doparts;

	private List<Doparts>  parts	;//	用户组id，1：超级管理员；2：普通用户
	  private String  mynumber	;//	用户登录帐号
	  private String  talent	;//	用户达人组， 1:悦美达人,2:银牌达人,3:金牌达人,4:管理员 ,8:悦美小编
	  private String  yuemeiinfo	;//	用户加密信息
	  private String  group_id	;//	用户组id，1：普通用户；2：医生；3医院
	  private String  loginphone	;//	用户登录手机
	  private String  phone	;//	用户关联手机
	  private String  isphone	;//	是否是手机号登录
	  private String  yqma	;//	邀请码
	  private String  sixin	;//	是否开通私信
	  private String  postnum	;//	用户发帖数
	  private String  replynum	;//	用户回复数
	  private String  is_show	;//
	  private String  is_signin	;//	是否签到
	 private String signButtonTitle	;//	签到按钮
	 private String sharenum	;//	日记数
	 private String followingnum	;//	关注数
	 private String followingmenum	;//	粉丝数
	 private String browsenum	;//	足迹数
	 private String nopaynum	;//	待支付数
	 private String paynum	;//	代销费数
	 private String evaluationnum;//	待评价数
	 private String nodiarynum	;//	待写日记数
	 private String is_real;//是否实名认证
	 private String is_member;//1是会员
	 private String member_discount;//会员预计可省金额
	 private String user_more;//1是客多多
	private String setuser_desc;
	private String upsetuser_desc;
	private RecoveryReminder recoveryReminder;
	private String is_show_bargain;//砍价是否显示 1显示

	public String getIs_show_bargain() {
		return is_show_bargain;
	}

	public void setIs_show_bargain(String is_show_bargain) {
		this.is_show_bargain = is_show_bargain;
	}

	public List<Doparts> getDoparts() {
		return doparts;
	}
	public void setDoparts(List<Doparts> doparts) {
		this.doparts = doparts;
	}

	public List<Doparts> getParts() {
		return parts;
	}

	public void setParts(List<Doparts> parts) {
		this.parts = parts;
	}
	public String getSignButtonTitle() {
		return signButtonTitle;
	}

	public void setSignButtonTitle(String signButtonTitle) {
		this.signButtonTitle = signButtonTitle;
	}

	public String getSharenum() {
		return sharenum;
	}

	public void setSharenum(String sharenum) {
		this.sharenum = sharenum;
	}

	public String getFollowingnum() {
		return followingnum;
	}

	public void setFollowingnum(String followingnum) {
		this.followingnum = followingnum;
	}

	public String getFollowingmenum() {
		return followingmenum;
	}

	public void setFollowingmenum(String followingmenum) {
		this.followingmenum = followingmenum;
	}

	public String getBrowsenum() {
		return browsenum;
	}

	public void setBrowsenum(String browsenum) {
		this.browsenum = browsenum;
	}

	public String getNopaynum() {
		return nopaynum;
	}

	public void setNopaynum(String nopaynum) {
		this.nopaynum = nopaynum;
	}

	public String getPaynum() {
		return paynum;
	}

	public void setPaynum(String paynum) {
		this.paynum = paynum;
	}


	public String getEvaluationnum() {
		return evaluationnum;
	}

	public void setEvaluationnum(String evaluationnum) {
		this.evaluationnum = evaluationnum;
	}

	public String getNodiarynum() {
		return nodiarynum;
	}

	public void setNodiarynum(String nodiarynum) {
		this.nodiarynum = nodiarynum;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEvaluate() {
		return evaluate;
	}

	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

	public String getStringegral() {
		return stringegral;
	}

	public void setStringegral(String stringegral) {
		this.stringegral = stringegral;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}

	public String getCard_id() {
		return card_id;
	}

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}




	public String getMynumber() {
		return mynumber;
	}

	public void setMynumber(String mynumber) {
		this.mynumber = mynumber;
	}

	public String getTalent() {
		return talent;
	}

	public void setTalent(String talent) {
		this.talent = talent;
	}

	public String getYuemeiinfo() {
		return yuemeiinfo;
	}

	public void setYuemeiinfo(String yuemeiinfo) {
		this.yuemeiinfo = yuemeiinfo;
	}

	public String getGroup_id() {
		return group_id;
	}

	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}

	public String getLoginphone() {
		return loginphone;
	}

	public void setLoginphone(String loginphone) {
		this.loginphone = loginphone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsphone() {
		return isphone;
	}

	public void setIsphone(String isphone) {
		this.isphone = isphone;
	}

	public String getYqma() {
		return yqma;
	}

	public void setYqma(String yqma) {
		this.yqma = yqma;
	}

	public String getSixin() {
		return sixin;
	}

	public void setSixin(String sixin) {
		this.sixin = sixin;
	}

	public String getPostnum() {
		return postnum;
	}

	public void setPostnum(String postnum) {
		this.postnum = postnum;
	}

	public String getReplynum() {
		return replynum;
	}

	public void setReplynum(String replynum) {
		this.replynum = replynum;
	}

	public String getIs_show() {
		return is_show;
	}

	public void setIs_show(String is_show) {
		this.is_show = is_show;
	}

	public String getIs_signin() {
		return is_signin;
	}

	public void setIs_signin(String is_signin) {
		this.is_signin = is_signin;
	}

	public String getIs_real() {
		return is_real;
	}

	public void setIs_real(String is_real) {
		this.is_real = is_real;
	}

	public String getIs_member() {
		return is_member;
	}

	public void setIs_member(String is_member) {
		this.is_member = is_member;
	}

	public String getMember_discount() {
		return member_discount;
	}

	public void setMember_discount(String member_discount) {
		this.member_discount = member_discount;
	}

	public String getUser_more() {
		return user_more;
	}

	public void setUser_more(String user_more) {
		this.user_more = user_more;
	}

	public String getSetuser_desc() {
		return setuser_desc;
	}

	public void setSetuser_desc(String setuser_desc) {
		this.setuser_desc = setuser_desc;
	}

	public String getUpsetuser_desc() {
		return upsetuser_desc;
	}

	public void setUpsetuser_desc(String upsetuser_desc) {
		this.upsetuser_desc = upsetuser_desc;
	}

	public RecoveryReminder getRecoveryReminder() {
		return recoveryReminder;
	}

	public void setRecoveryReminder(RecoveryReminder recoveryReminder) {
		this.recoveryReminder = recoveryReminder;
	}
}
