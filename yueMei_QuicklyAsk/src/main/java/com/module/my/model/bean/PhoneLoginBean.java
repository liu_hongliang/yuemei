package com.module.my.model.bean;

public class PhoneLoginBean {

    /**
     * _id : 88006729
     * img : https://www.yuemei.com/images/weibo/noavatar5_120_120.jpg?0
     * nickname : 悦Mer_1510531802
     * province : 香港
     * city : 香港
     * sex : 2
     */

    private String _id;
    private String img;
    private String nickname;
    private String province;
    private String city;
    private String sex;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
