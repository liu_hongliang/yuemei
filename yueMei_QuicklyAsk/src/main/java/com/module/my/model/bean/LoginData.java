package com.module.my.model.bean;

/**
 * 登录返回数据
 * 
 * @author Rubin
 * 
 */
public class LoginData {

	private String code;
	private String message;
	private UserData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserData getData() {
		return data;
	}

	public void setData(UserData data) {
		this.data = data;
	}


}
