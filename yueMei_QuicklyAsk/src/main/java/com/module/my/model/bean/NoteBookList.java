package com.module.my.model.bean;

import java.util.List;

/**
 * Created by dwb on 16/3/21.
 */
public class NoteBookList {

    private String code;
    private String message;
    private List<NoteBookListData> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NoteBookListData> getData() {
        return data;
    }

    public void setData(List<NoteBookListData> data) {
        this.data = data;
    }
}
