package com.module.my.model.bean;

/**
 * Created by dwb on 17/3/9.
 */

public class HbFenqiItem {

    private int hb_fq_num;
    private String hb_fq_percent;
    private String is_use;


    public int getHb_fq_num() {
        return hb_fq_num;
    }

    public void setHb_fq_num(int hb_fq_num) {
        this.hb_fq_num = hb_fq_num;
    }

    public String getHb_fq_percent() {
        return hb_fq_percent;
    }

    public void setHb_fq_percent(String hb_fq_percent) {
        this.hb_fq_percent = hb_fq_percent;
    }

    public String getIs_use() {
        return is_use;
    }

    public void setIs_use(String is_use) {
        this.is_use = is_use;
    }
}
