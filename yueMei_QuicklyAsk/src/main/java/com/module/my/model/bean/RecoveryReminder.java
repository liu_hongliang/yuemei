package com.module.my.model.bean;

import java.util.HashMap;

public class RecoveryReminder {
    private String show_type;
    private String show_title;
    private String show_button_title;
    private HashMap<String,String> event_params;

    public String getShow_type() {
        return show_type;
    }

    public void setShow_type(String show_type) {
        this.show_type = show_type;
    }

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getShow_button_title() {
        return show_button_title;
    }

    public void setShow_button_title(String show_button_title) {
        this.show_button_title = show_button_title;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
