package com.module.my.model.bean;

public class XiaDan {
	private String code;
	private String message;
	private XiaDanData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public XiaDanData getData() {
		return data;
	}

	public void setData(XiaDanData data) {
		this.data = data;
	}

}
