package com.module.my.model.bean;

public class TaoOrderInfo {
	private String code;
	private String message;
	private TaoOrderInfoData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TaoOrderInfoData getData() {
		return data;
	}

	public void setData(TaoOrderInfoData data) {
		this.data = data;
	}


}
