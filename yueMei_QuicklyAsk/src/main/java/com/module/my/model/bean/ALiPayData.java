package com.module.my.model.bean;

/**
 * Created by dwb on 16/9/21.
 */
public class ALiPayData {

    private String payInfo;

    public String getPayInfo() {
        return payInfo;
    }

    public void setPayInfo(String payInfo) {
        this.payInfo = payInfo;
    }
}
