package com.module.my.model.bean;

import com.module.taodetail.model.bean.HomeTaoData;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/12/11.
 */

public class TaolistData {

    /**
     * code : 1
     * message :
     * data : {"total":"7","list":[{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海芭比眼综合 切开双眼皮+开内眼角+上睑提肌 返现1000比埋线双眼皮更持久","hos_name":"上海华美医疗美容医院","doc_name":"佀同帅","price":"25400","price_discount":"19800","price_range_max":"0","_id":"58811","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"5人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海华美定制park法 显微精准双眼皮 杨亚益/佀同帅主任亲诊比埋线双眼皮更持久","hos_name":"上海华美医疗美容医院","doc_name":"杨亚益","price":"13800","price_discount":"12420","price_range_max":"0","_id":"36867","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"6人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海上睑提肌 美眼专家亲诊 改善眯眯眼 打造神采双眸","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"90519","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":" 上海隐形切开双眼皮 精细比例下定制自然翘睫双眼皮 流畅线条","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"6800","price_discount":"3800","price_range_max":"0","_id":"81295","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"106人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":" 上海开内外眼角 精细弧度开眼角 30%放大双眼 写案例返1000","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"87403","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"纹绣化妆","subtitle":"上海切眉术 华美定制@张朋 30分钟成就气质眉形","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"89225","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海网红眼综合套餐(切开双眼皮+开眼角) 7天拆线","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"12600","price_discount":"6800","price_range_max":"0","_id":"66821","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"39人预订","feeScale":"/次"}],"data":[],"desc":"以下商品可使用满3000减150的医院红包","type":"4"}
     */

    /**
     * total : 7
     * list : [{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海芭比眼综合 切开双眼皮+开内眼角+上睑提肌 返现1000比埋线双眼皮更持久","hos_name":"上海华美医疗美容医院","doc_name":"佀同帅","price":"25400","price_discount":"19800","price_range_max":"0","_id":"58811","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"5人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海华美定制park法 显微精准双眼皮 杨亚益/佀同帅主任亲诊比埋线双眼皮更持久","hos_name":"上海华美医疗美容医院","doc_name":"杨亚益","price":"13800","price_discount":"12420","price_range_max":"0","_id":"36867","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"6人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海上睑提肌 美眼专家亲诊 改善眯眯眼 打造神采双眸","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"90519","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":" 上海隐形切开双眼皮 精细比例下定制自然翘睫双眼皮 流畅线条","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"6800","price_discount":"3800","price_range_max":"0","_id":"81295","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"106人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":" 上海开内外眼角 精细弧度开眼角 30%放大双眼 写案例返1000","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"87403","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1人预订","feeScale":"/次"},{"seckilling":"0","img":"","title":"纹绣化妆","subtitle":"上海切眉术 华美定制@张朋 30分钟成就气质眉形","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"5800","price_discount":"5220","price_range_max":"0","_id":"89225","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次"},{"seckilling":"0","img":"","title":"眼整形","subtitle":"上海网红眼综合套餐(切开双眼皮+开眼角) 7天拆线","hos_name":"上海华美医疗美容医院","doc_name":"张朋","price":"12600","price_discount":"6800","price_range_max":"0","_id":"66821","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"满5000减300,满3000减150","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"39人预订","feeScale":"/次"}]
     * data : []
     * desc : 以下商品可使用满3000减150的医院红包
     * type : 4
     */

    private String total;
    private String desc;
    private String type;
    private List<HomeTaoData> list;
    private List<?> data;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<HomeTaoData> getList() {
        return list;
    }

    public void setList(List<HomeTaoData> list) {
        this.list = list;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

}
