package com.module.my.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/7/19.
 */

public class VideoPlay {

    /**
     * code : 1
     * message : 缺少参数
     * data : {"video":{"url":"/201707/66387c41cd70253ec629252a15f2ee22.mp4","cover":"upload/forum/image/20170719/170719100054_694fc1.png","video_time":"12","url_240":"http://player.yuemei.com/video/335f/201707/66387c41cd70253ec629252a15f2ee22.mp4","url_480":"http://player.yuemei.com/video/6ea2/201707/66387c41cd70253ec629252a15f2ee22.mp4"},"tao":[{"seckilling":"0","img":"http://p34.yuemei.com/tao/2017/0321/200_200/jt170321102249_906c2b.png","title":"测试用淘整形服务","subtitle":"测试用淘整形服务","hos_name":"西安西京医院整形外科","doc_name":"","price":"200","price_discount":"1","price_range_max":"0","_id":"63899","showprice":"1","specialPrice":"0","show_hospital":"1","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"62人预订"},{"seckilling":"0","img":"http://p34.yuemei.com/tao/2017/0710/200_200/jt170710183831_78d32d.png","title":"测试淘","subtitle":"测试淘11测试淘11","hos_name":"悦美特邀","doc_name":"何林","price":"900","price_discount":"1","price_range_max":"0","_id":"13817","showprice":"0","specialPrice":"0","show_hospital":"2","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1972人预订"}]}
     */

    private String code;
    private String message;
    private DataBean data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * video : {"url":"/201707/66387c41cd70253ec629252a15f2ee22.mp4","cover":"upload/forum/image/20170719/170719100054_694fc1.png","video_time":"12","url_240":"http://player.yuemei.com/video/335f/201707/66387c41cd70253ec629252a15f2ee22.mp4","url_480":"http://player.yuemei.com/video/6ea2/201707/66387c41cd70253ec629252a15f2ee22.mp4"}
         * tao : [{"seckilling":"0","img":"http://p34.yuemei.com/tao/2017/0321/200_200/jt170321102249_906c2b.png","title":"测试用淘整形服务","subtitle":"测试用淘整形服务","hos_name":"西安西京医院整形外科","doc_name":"","price":"200","price_discount":"1","price_range_max":"0","_id":"63899","showprice":"1","specialPrice":"0","show_hospital":"1","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"62人预订"},{"seckilling":"0","img":"http://p34.yuemei.com/tao/2017/0710/200_200/jt170710183831_78d32d.png","title":"测试淘","subtitle":"测试淘11测试淘11","hos_name":"悦美特邀","doc_name":"何林","price":"900","price_discount":"1","price_range_max":"0","_id":"13817","showprice":"0","specialPrice":"0","show_hospital":"2","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1972人预订"}]
         */

        private VideoBean video;
        private List<TaoBean> tao;

        public VideoBean getVideo() {
            return video;
        }

        public void setVideo(VideoBean video) {
            this.video = video;
        }

        public List<TaoBean> getTao() {
            return tao;
        }

        public void setTao(List<TaoBean> tao) {
            this.tao = tao;
        }

        public static class VideoBean {
            /**
             * url : /201707/66387c41cd70253ec629252a15f2ee22.mp4
             * cover : upload/forum/image/20170719/170719100054_694fc1.png
             * video_time : 12
             * url_240 : http://player.yuemei.com/video/335f/201707/66387c41cd70253ec629252a15f2ee22.mp4
             * url_480 : http://player.yuemei.com/video/6ea2/201707/66387c41cd70253ec629252a15f2ee22.mp4
             */

            private String url;
            private String cover;
            private String video_time;
            private String url_240;
            private String url_480;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getCover() {
                return cover;
            }

            public void setCover(String cover) {
                this.cover = cover;
            }

            public String getVideo_time() {
                return video_time;
            }

            public void setVideo_time(String video_time) {
                this.video_time = video_time;
            }

            public String getUrl_240() {
                return url_240;
            }

            public void setUrl_240(String url_240) {
                this.url_240 = url_240;
            }

            public String getUrl_480() {
                return url_480;
            }

            public void setUrl_480(String url_480) {
                this.url_480 = url_480;
            }
        }

        public static class TaoBean {
            /**
             * seckilling : 0
             * img : http://p34.yuemei.com/tao/2017/0321/200_200/jt170321102249_906c2b.png
             * title : 测试用淘整形服务
             * subtitle : 测试用淘整形服务
             * hos_name : 西安西京医院整形外科
             * doc_name :
             * price : 200
             * price_discount : 1
             * price_range_max : 0
             * _id : 63899
             * showprice : 1
             * specialPrice : 0
             * show_hospital : 1
             * hos_red_packet :
             * shixiao : 0
             * newp : 0
             * hot : 0
             * mingyi : 0
             * rate : 62人预订
             */

            private String seckilling;
            private String img;
            private String title;
            private String subtitle;
            private String hos_name;
            private String doc_name;
            private String price;
            private String price_discount;
            private String price_range_max;
            private String _id;
            private String showprice;
            private String specialPrice;
            private String show_hospital;
            private String hos_red_packet;
            private String shixiao;
            private String newp;
            private String hot;
            private String mingyi;
            private String rate;

            public String getSeckilling() {
                return seckilling;
            }

            public void setSeckilling(String seckilling) {
                this.seckilling = seckilling;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public String getHos_name() {
                return hos_name;
            }

            public void setHos_name(String hos_name) {
                this.hos_name = hos_name;
            }

            public String getDoc_name() {
                return doc_name;
            }

            public void setDoc_name(String doc_name) {
                this.doc_name = doc_name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getPrice_discount() {
                return price_discount;
            }

            public void setPrice_discount(String price_discount) {
                this.price_discount = price_discount;
            }

            public String getPrice_range_max() {
                return price_range_max;
            }

            public void setPrice_range_max(String price_range_max) {
                this.price_range_max = price_range_max;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getShowprice() {
                return showprice;
            }

            public void setShowprice(String showprice) {
                this.showprice = showprice;
            }

            public String getSpecialPrice() {
                return specialPrice;
            }

            public void setSpecialPrice(String specialPrice) {
                this.specialPrice = specialPrice;
            }

            public String getShow_hospital() {
                return show_hospital;
            }

            public void setShow_hospital(String show_hospital) {
                this.show_hospital = show_hospital;
            }

            public String getHos_red_packet() {
                return hos_red_packet;
            }

            public void setHos_red_packet(String hos_red_packet) {
                this.hos_red_packet = hos_red_packet;
            }

            public String getShixiao() {
                return shixiao;
            }

            public void setShixiao(String shixiao) {
                this.shixiao = shixiao;
            }

            public String getNewp() {
                return newp;
            }

            public void setNewp(String newp) {
                this.newp = newp;
            }

            public String getHot() {
                return hot;
            }

            public void setHot(String hot) {
                this.hot = hot;
            }

            public String getMingyi() {
                return mingyi;
            }

            public void setMingyi(String mingyi) {
                this.mingyi = mingyi;
            }

            public String getRate() {
                return rate;
            }

            public void setRate(String rate) {
                this.rate = rate;
            }
        }
    }
}
