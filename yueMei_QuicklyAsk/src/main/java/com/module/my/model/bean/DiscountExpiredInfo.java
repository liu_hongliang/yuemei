package com.module.my.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.home.model.bean.Coupons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 文 件 名: DiscountExpiredInfo
 * 创 建 人: 原成昊
 * 创建日期: 2020-01-19 11:52
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class DiscountExpiredInfo implements Parcelable {

    /**
     * title : 优惠过期提醒
     * desc : 您有%d元优惠券将在24小时内过期,记得及时使用哦~
     * url : type:eq:6494
     * coupons : []
     * event_params : {"event_name":"userExpireCouponsClick","type":62}
     */

    private String title;
    private String desc;
    private String url;
//    private EventParamsBean event_params;
    private HashMap<String, String> event_params;
    private HashMap<String, String> event_params_show;
    private List<Coupons> coupons;

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public HashMap<String, String> getEvent_params_show() {
        return event_params_show;
    }

    public void setEvent_params_show(HashMap<String, String> event_params_show) {
        this.event_params_show = event_params_show;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

//    public EventParamsBean getEvent_params() {
//        return event_params;
//    }
//
//    public void setEvent_params(EventParamsBean event_params) {
//        this.event_params = event_params;
//    }

    public List<Coupons> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<Coupons> coupons) {
        this.coupons = coupons;
    }

    public static class EventParamsBean {
        /**
         * event_name : userExpireCouponsClick
         * type : 62
         */

        private String event_name;
        private int type;

        public String getEvent_name() {
            return event_name;
        }

        public void setEvent_name(String event_name) {
            this.event_name = event_name;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.desc);
        dest.writeString(this.url);
        dest.writeParcelable((Parcelable) this.event_params, flags);
        dest.writeList(this.coupons);
    }

    public DiscountExpiredInfo() {
    }

    protected DiscountExpiredInfo(Parcel in) {
        this.title = in.readString();
        this.desc = in.readString();
        this.url = in.readString();
        this.event_params = in.readParcelable(EventParamsBean.class.getClassLoader());
        this.coupons = new ArrayList<Coupons>();
        in.readList(this.coupons, Coupons.class.getClassLoader());
    }

    public static final Creator<DiscountExpiredInfo> CREATOR = new Creator<DiscountExpiredInfo>() {
        @Override
        public DiscountExpiredInfo createFromParcel(Parcel source) {
            return new DiscountExpiredInfo(source);
        }

        @Override
        public DiscountExpiredInfo[] newArray(int size) {
            return new DiscountExpiredInfo[size];
        }
    };
}
