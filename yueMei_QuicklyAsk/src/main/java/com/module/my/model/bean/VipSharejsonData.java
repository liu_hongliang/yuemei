package com.module.my.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.commonview.module.bean.ShareWechat;
import com.module.community.model.bean.ShareDetailPictorial;
import com.module.community.model.bean.ShareVipPictorial;

/**
 * Created by 裴成浩 on 2018/9/12.
 */
public class VipSharejsonData implements Parcelable{
    private ShareWechat Wechat;
    private ShareVipPictorial Pictorial;

    protected VipSharejsonData(Parcel in) {
        Wechat = in.readParcelable(ShareWechat.class.getClassLoader());
        Pictorial = in.readParcelable(ShareVipPictorial.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(Wechat, flags);
        dest.writeParcelable(Pictorial, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VipSharejsonData> CREATOR = new Creator<VipSharejsonData>() {
        @Override
        public VipSharejsonData createFromParcel(Parcel in) {
            return new VipSharejsonData(in);
        }

        @Override
        public VipSharejsonData[] newArray(int size) {
            return new VipSharejsonData[size];
        }
    };

    public ShareWechat getWechat() {
        return Wechat;
    }

    public void setWechat(ShareWechat wechat) {
        Wechat = wechat;
    }

    public ShareVipPictorial getPictorial() {
        return Pictorial;
    }

    public void setPictorial(ShareVipPictorial pictorial) {
        Pictorial = pictorial;
    }
}
