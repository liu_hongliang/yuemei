package com.module.my.model.bean;

/**
 * Created by dwb on 16/9/22.
 */
public class AiLiPayCode {

    private String code;
    private String message;
    private AiLiPayCodeData data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AiLiPayCodeData getData() {
        return data;
    }

    public void setData(AiLiPayCodeData data) {
        this.data = data;
    }
}
