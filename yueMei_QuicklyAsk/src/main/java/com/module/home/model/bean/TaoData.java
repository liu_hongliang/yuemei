package com.module.home.model.bean;

public class TaoData {
    private String url;
    private String title;
    private String img;
    private String subtitle;
    private String alltitle;
    private String pay_price;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getAlltitle() {
        return alltitle;
    }

    public void setAlltitle(String alltitle) {
        this.alltitle = alltitle;
    }

    public String getPay_price() {
        return pay_price;
    }

    public void setPay_price(String pay_price) {
        this.pay_price = pay_price;
    }
}
