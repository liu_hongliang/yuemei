package com.module.home.model.bean;

import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/1.
 */

public class TuijPost {

    private String is_fanxian;
    private String picRule;
    private String url;
    private String appmurl;
    private String _id;
    private String q_id;
    private String set_tid;
    private String view_num;
    private String title;
    private String desc;
    private String user_id;
    private String time;
    private String group_id;
    private String user_img;
    private String talent;
    private String user_name;
    private String answer_num;
    private String askorshare;
    private String price;
    private String is_video;
    private List<TuijPostTag> tag;
    private List<?> pic3;
    private List<TuijPostPic> pic;
    private TaoBean tao;
    private String agree_num;
    private String is_follow_user;
    private String bmsid;
    private CashBackBean cashback;

    public String getIs_agree() {
        return is_agree;
    }

    public void setIs_agree(String is_agree) {
        this.is_agree = is_agree;
    }

    private String is_agree;
    private String after_day;

    public String getAfter_day() {
        return after_day;
    }

    public void setAfter_day(String after_day) {
        this.after_day = after_day;
    }

    public String getAgree_num() {
        return agree_num;
    }

    public void setAgree_num(String agree_num) {
        this.agree_num = agree_num;
    }

    public TaoBean getTao() {
        return tao;
    }

    public void setTao(TaoBean tao) {
        this.tao = tao;
    }




    public String getIs_fanxian() {
        return is_fanxian;
    }

    public void setIs_fanxian(String is_fanxian) {
        this.is_fanxian = is_fanxian;
    }

    public String getPicRule() {
        return picRule;
    }

    public void setPicRule(String picRule) {
        this.picRule = picRule;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppmurl() {
        return appmurl;
    }

    public void setAppmurl(String appmurl) {
        this.appmurl = appmurl;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQ_id() {
        return q_id;
    }

    public void setQ_id(String q_id) {
        this.q_id = q_id;
    }

    public String getSet_tid() {
        return set_tid;
    }

    public void setSet_tid(String set_tid) {
        this.set_tid = set_tid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getView_num() {
        return view_num;
    }

    public void setView_num(String view_num) {
        this.view_num = view_num;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAnswer_num() {
        return answer_num;
    }

    public void setAnswer_num(String answer_num) {
        this.answer_num = answer_num;
    }

    public String getAskorshare() {
        return askorshare;
    }

    public void setAskorshare(String askorshare) {
        this.askorshare = askorshare;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getIs_video() {
        return is_video;
    }

    public void setIs_video(String is_video) {
        this.is_video = is_video;
    }

    public List<TuijPostTag> getTag() {
        return tag;
    }

    public void setTag(List<TuijPostTag> tag) {
        this.tag = tag;
    }

    public List<?> getPic3() {
        return pic3;
    }

    public void setPic3(List<?> pic3) {
        this.pic3 = pic3;
    }

    public List<TuijPostPic> getPic() {
        return pic;
    }

    public void setPic(List<TuijPostPic> pic) {
        this.pic = pic;
    }

    public String getIs_follow_user() {
        return is_follow_user;
    }

    public void setIs_follow_user(String is_follow_user) {
        this.is_follow_user = is_follow_user;
    }

    public CashBackBean getCashback() {
        return cashback;
    }

    public void setCashback(CashBackBean cashback) {
        this.cashback = cashback;
    }
    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    @Override
    public String toString() {
        return "TuijPost{" +
                "is_fanxian='" + is_fanxian + '\'' +
                ", picRule='" + picRule + '\'' +
                ", url='" + url + '\'' +
                ", appmurl='" + appmurl + '\'' +
                ", _id='" + _id + '\'' +
                ", q_id='" + q_id + '\'' +
                ", set_tid='" + set_tid + '\'' +
                ", view_num='" + view_num + '\'' +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", user_id='" + user_id + '\'' +
                ", time='" + time + '\'' +
                ", group_id='" + group_id + '\'' +
                ", user_img='" + user_img + '\'' +
                ", talent='" + talent + '\'' +
                ", user_name='" + user_name + '\'' +
                ", answer_num='" + answer_num + '\'' +
                ", askorshare='" + askorshare + '\'' +
                ", price='" + price + '\'' +
                ", is_video='" + is_video + '\'' +
                ", tag=" + tag +
                ", pic3=" + pic3 +
                ", pic=" + pic +
                ", tao=" + tao +
                ", agree_num='" + agree_num + '\'' +
                ", after_day='" + after_day + '\'' +
                '}';
    }
}
