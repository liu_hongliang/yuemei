package com.module.home.model.bean;

public class LanmuTao {

    /**
     * img : https://p34.yuemei.com/tao/2019/0426/jt190426142929_9b0a7a.png
     * title : 埋线提升
     * subtitle : 军大整形医院
     * price : 100
     * url : https://m.yuemei.com/tao/195796/
     */

    private String img;
    private String title;
    private String subtitle;
    private String price;
    private String url;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
