package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/6/18
 */
public class SearchResultBaikeCompare {
    private String id;
    private String name;
    private String img;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
