package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class SearchTaolistData implements Parcelable {

    /**
     * taoTopNum : 1
     * id : 96586
     * title : 注射微整
     * bilateral_title : 注射微整
     * img : https://p24.yuemei.com/tao/2019/0709/200_200/jt190709094750_632c65.jpg
     * price : 780
     * app_url : https://m.yuemei.com/tao/96586/
     * coupons :
     * totalPv : 567
     * labelID : 184
     */

    private String taoTopNum;
    private String id;
    private String title;
    private String bilateral_title;
    private String img;
    private int price;
    private String app_url;
    private String coupons;
    private String totalPv;
    private String labelID;
    private String diaryImg;
    private HashMap<String,String> event_params;
    private int img_resouce;

    public String getTaoTopNum() {
        return taoTopNum;
    }

    public void setTaoTopNum(String taoTopNum) {
        this.taoTopNum = taoTopNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBilateral_title() {
        return bilateral_title;
    }

    public void setBilateral_title(String bilateral_title) {
        this.bilateral_title = bilateral_title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getApp_url() {
        return app_url;
    }

    public void setApp_url(String app_url) {
        this.app_url = app_url;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public String getTotalPv() {
        return totalPv;
    }

    public void setTotalPv(String totalPv) {
        this.totalPv = totalPv;
    }

    public String getLabelID() {
        return labelID;
    }

    public void setLabelID(String labelID) {
        this.labelID = labelID;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public int getImg_resouce() {
        return img_resouce;
    }

    public void setImg_resouce(int img_resouce) {
        this.img_resouce = img_resouce;
    }

    public void setDiaryImg(String diaryImg) {
        this.diaryImg = diaryImg;
    }

    public String getDiaryImg() {
        return diaryImg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.taoTopNum);
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.bilateral_title);
        dest.writeString(this.img);
        dest.writeInt(this.price);
        dest.writeString(this.app_url);
        dest.writeString(this.coupons);
        dest.writeString(this.totalPv);
        dest.writeString(this.labelID);
        dest.writeString(this.diaryImg);
        dest.writeSerializable(this.event_params);
        dest.writeInt(this.img_resouce);
    }

    public SearchTaolistData() {
    }

    protected SearchTaolistData(Parcel in) {
        this.taoTopNum = in.readString();
        this.id = in.readString();
        this.title = in.readString();
        this.bilateral_title = in.readString();
        this.img = in.readString();
        this.price = in.readInt();
        this.app_url = in.readString();
        this.coupons = in.readString();
        this.totalPv = in.readString();
        this.labelID = in.readString();
        this.diaryImg = in.readString();
        this.event_params = (HashMap<String, String>) in.readSerializable();
        this.img_resouce = in.readInt();
    }

    public static final Parcelable.Creator<SearchTaolistData> CREATOR = new Parcelable.Creator<SearchTaolistData>() {
        @Override
        public SearchTaolistData createFromParcel(Parcel source) {
            return new SearchTaolistData(source);
        }

        @Override
        public SearchTaolistData[] newArray(int size) {
            return new SearchTaolistData[size];
        }
    };
}
