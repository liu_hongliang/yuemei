package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Administrator on 2018/1/25.
 */

public class TaoBean implements Parcelable {

    /**
     * id : 85479
     * price : 880
     * title : 面部紧致提升
     */

    private String id;
    private String price;
    private String title;
    private String member_price;
    private String totalAppoint;
    private HashMap<String,String> event_params;

    protected TaoBean(Parcel in) {
        id = in.readString();
        price = in.readString();
        title = in.readString();
        member_price = in.readString();
        totalAppoint = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(price);
        dest.writeString(title);
        dest.writeString(member_price);
        dest.writeString(totalAppoint);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaoBean> CREATOR = new Creator<TaoBean>() {
        @Override
        public TaoBean createFromParcel(Parcel in) {
            return new TaoBean(in);
        }

        @Override
        public TaoBean[] newArray(int size) {
            return new TaoBean[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMember_price() {
        return member_price;
    }

    public void setMember_price(String member_price) {
        this.member_price = member_price;
    }

    public String getTotalAppoint() {
        return totalAppoint;
    }

    public void setTotalAppoint(String totalAppoint) {
        this.totalAppoint = totalAppoint;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
