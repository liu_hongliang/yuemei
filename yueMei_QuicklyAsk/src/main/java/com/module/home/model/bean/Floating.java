package com.module.home.model.bean;

import java.util.HashMap;

public class Floating {
    /**
     * img : https://p11.yuemei.com/tag/1538126251dcd18.jpg
     * url : https://m.yuemei.com/tao_zt/5770.html
     */

    private String img;
    private String url;
    private String id;
    private HashMap<String,String> event_params;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap event_params) {
        this.event_params = event_params;
    }
}
