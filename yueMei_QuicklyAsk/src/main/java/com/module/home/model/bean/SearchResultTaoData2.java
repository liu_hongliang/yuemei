package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.LocationData;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.entity.SearchResultMethod;

import java.util.HashMap;
import java.util.List;

public class SearchResultTaoData2 implements Parcelable {

    private String total;                               //搜索结果数
    private List<SearchTao> list;                     //搜索淘整形返回数据列表
    private SearchTitleList data;                       //推荐头数据
    private String desc;                                //通过优惠券搜索返回字段（列表头部显示）
    private List<SearchResultMethod> search_partChild;   //方法
    private List<SearchResultMethod> search_method;     //方法
    private List<SearchResultBoard> screen_board;       //活动
    private List<SearchResultBoard> screen_active;       //活动
    private List<SearchTao> recomend_list;            //推荐淘列表
    private SearchResultDoctor comparedConsultative;    //对比咨询医生信息
    private List<SearchResultLike> parts;               //你可能喜欢
    private String search_key;                          //任搜索“xxx”
    private String list_tips;                           //显示在列表顶部的提示
    private String recomend_tips;                       //显示在推荐数据顶部的提示
    private SearchResultCompare baikeCompare;           //百科统计
    private List<SearchCompositeData> compositeData;    //综合页数据
    private String type;
    private String isNext = "1";                        //是否加载下一页
    private int is_bilateral;
    private HashMap<String,String> switch_event_params;
    private LocationData city_location;

    public LocationData getCity_location() {
        return city_location;
    }

    public void setCity_location(LocationData city_location) {
        this.city_location = city_location;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<SearchTao> getList() {
        return list;
    }

    public void setList(List<SearchTao> list) {
        this.list = list;
    }

    public SearchTitleList getData() {
        return data;
    }

    public void setData(SearchTitleList data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<SearchResultMethod> getSearch_partChild() {
        return search_partChild;
    }

    public void setSearch_partChild(List<SearchResultMethod> search_partChild) {
        this.search_partChild = search_partChild;
    }

    public List<SearchResultMethod> getSearch_method() {
        return search_method;
    }

    public void setSearch_method(List<SearchResultMethod> search_method) {
        this.search_method = search_method;
    }

    public List<SearchResultBoard> getScreen_board() {
        return screen_board;
    }

    public void setScreen_board(List<SearchResultBoard> screen_board) {
        this.screen_board = screen_board;
    }

    public List<SearchResultBoard> getScreen_active() {
        return screen_active;
    }

    public void setScreen_active(List<SearchResultBoard> screen_active) {
        this.screen_active = screen_active;
    }

    public List<SearchTao> getRecomend_list() {
        return recomend_list;
    }

    public void setRecomend_list(List<SearchTao> recomend_list) {
        this.recomend_list = recomend_list;
    }

    public SearchResultDoctor getComparedConsultative() {
        return comparedConsultative;
    }

    public void setComparedConsultative(SearchResultDoctor comparedConsultative) {
        this.comparedConsultative = comparedConsultative;
    }

    public List<SearchResultLike> getParts() {
        return parts;
    }

    public void setParts(List<SearchResultLike> parts) {
        this.parts = parts;
    }

    public String getSearch_key() {
        return search_key;
    }

    public void setSearch_key(String search_key) {
        this.search_key = search_key;
    }

    public String getList_tips() {
        return list_tips;
    }

    public void setList_tips(String list_tips) {
        this.list_tips = list_tips;
    }

    public String getRecomend_tips() {
        return recomend_tips;
    }

    public void setRecomend_tips(String recomend_tips) {
        this.recomend_tips = recomend_tips;
    }

    public SearchResultCompare getBaikeCompare() {
        return baikeCompare;
    }

    public void setBaikeCompare(SearchResultCompare baikeCompare) {
        this.baikeCompare = baikeCompare;
    }

    public List<SearchCompositeData> getCompositeData() {
        return compositeData;
    }

    public void setCompositeData(List<SearchCompositeData> compositeData) {
        this.compositeData = compositeData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIsNext() {
        return isNext;
    }

    public void setIsNext(String isNext) {
        this.isNext = isNext;
    }

    public int getIs_bilateral() {
        return is_bilateral;
    }

    public void setIs_bilateral(int is_bilateral) {
        this.is_bilateral = is_bilateral;
    }

    public HashMap<String, String> getSwitch_event_params() {
        return switch_event_params;
    }

    public void setSwitch_event_params(HashMap<String, String> switch_event_params) {
        this.switch_event_params = switch_event_params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
