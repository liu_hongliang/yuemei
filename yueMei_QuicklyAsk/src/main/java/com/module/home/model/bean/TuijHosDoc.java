package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.taodetail.model.bean.HomeTaoData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/1.
 */

public class TuijHosDoc implements Parcelable {
    private String img;
    private String name;
    private String button;
    private String url;
    private String yuding;
    private String rate;
    private List<HomeTaoData> tao;
    private HashMap<String,String> event_params;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getYuding() {
        return yuding;
    }

    public void setYuding(String yuding) {
        this.yuding = yuding;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<HomeTaoData> getTao() {
        return tao;
    }

    public void setTao(List<HomeTaoData> tao) {
        this.tao = tao;
    }

    public HashMap<String,String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String,String> event_params) {
        this.event_params = event_params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.img);
        dest.writeString(this.name);
        dest.writeString(this.button);
        dest.writeString(this.url);
        dest.writeString(this.yuding);
        dest.writeString(this.rate);
        dest.writeTypedList(this.tao);
        dest.writeSerializable(this.event_params);
    }

    public TuijHosDoc() {
    }

    protected TuijHosDoc(Parcel in) {
        this.img = in.readString();
        this.name = in.readString();
        this.button = in.readString();
        this.url = in.readString();
        this.yuding = in.readString();
        this.rate = in.readString();
        this.tao = in.createTypedArrayList(HomeTaoData.CREATOR);
        this.event_params = (HashMap) in.readSerializable();
    }

    public static final Creator<TuijHosDoc> CREATOR = new Creator<TuijHosDoc>() {
        @Override
        public TuijHosDoc createFromParcel(Parcel source) {
            return new TuijHosDoc(source);
        }

        @Override
        public TuijHosDoc[] newArray(int size) {
            return new TuijHosDoc[size];
        }
    };
}
