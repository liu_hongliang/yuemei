package com.module.home.model.bean;

public class SearchXSData {

	private String show_key;
	private String search_key;

	public String getShow_key() {
		return show_key;
	}

	public void setShow_key(String show_key) {
		this.show_key = show_key;
	}

	public String getSearch_key() {
		return search_key;
	}

	public void setSearch_key(String search_key) {
		this.search_key = search_key;
	}

}
