package com.module.home.model.bean;

import java.util.List;

public class CategoryBean {

    /**
     * is_show_alert : 1
     * alert_type : 1
     * tao_list : [[{"id":"10313","tao_img":"https://p24.yuemei.com/tao/2018/0104/200_200/jt180104110307_b4035e.jpg","label_title":"吸脂瘦脸","title":"【吸脂瘦脸】北京微创口腔内吸脂瘦脸 吸走婴儿肥  微创打造V脸美人","hospital_name":"北京丽港医疗美容诊所","price":"4560","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/10313/"},{"id":"21935","tao_img":"https://p24.yuemei.com/tao/2017/1128/200_200/jt171128164436_4059e3.jpg","label_title":"面部填充","title":"【面部填充】面部脂肪填充    脂肪填充成活率高    逆龄加V脸  全年仅一次特惠","hospital_name":"北京爱多邦医疗美容诊所","price":"26800","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/21935/"},{"id":"33281","tao_img":"https://p24.yuemei.com/tao/2017/1122/200_200/jt171122142051_38b6e9.jpg","label_title":"鼻整形","title":"【鼻整形】北京自体筋膜隆鼻 名媛圈整形王绍国 无假体 无惧捏揉挤  全年仅一次特惠","hospital_name":"北京爱多邦医疗美容诊所","price":"28000","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/33281/"}],[{"id":"33689","tao_img":"https://p24.yuemei.com/tao/2016/0721/200_200/jt160715133650_2572ea.jpg","label_title":"补水嫩肤","title":"【补水嫩肤】北京韩国水光仪+润百颜+肉毒 给你肌肤喝饱水 年轻有活力 日记返现100","hospital_name":"北京昕颜医疗美容诊所","price":"1980","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/33689/"},{"id":"32201","tao_img":"https://p24.yuemei.com/tao/2018/0816/200_200/jt180816152858_aa3bcd.jpg","label_title":"鼻整形","title":"【鼻整形】北京鼻综合 20余年经验丰富专家亲自手术  效果有保证 写日记返现4000","hospital_name":"北京昕颜医疗美容诊所","price":"14000","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/32201/"},{"id":"32371","tao_img":"https://p24.yuemei.com/tao/2019/0426/200_200/jt190426093306_214959.jpg","label_title":"光子嫩肤","title":"【光子嫩肤】 光子嫩肤 淡斑嫩肤美白|改善肤色瓷肌定制|皇后光子嫩肤赶走暗|收缩毛孔","hospital_name":"北京蕊丽医疗美容诊所","price":"380","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/32371/"}],[{"id":"32585","tao_img":"https://p24.yuemei.com/tao/2019/0427/200_200/jt190427165025_1d5970.jpg","label_title":"自体脂肪填充","title":"【自体脂肪填充】私信立减 面部自体脂肪填充 额头太阳穴苹果肌 脂肪填充 精雕素颜 打造芭比童颜","hospital_name":"北京溪峰聚美仕医疗美容诊所","price":"9901","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/32585/"},{"id":"36513","tao_img":"https://p24.yuemei.com/tao/2018/0816/200_200/jt180816115058_20e0ad.jpg","label_title":"眼整形","title":"【眼整形】北京内切去眼袋 丢掉大眼袋 重获年轻双眸 眼部更有神","hospital_name":"北京昕颜医疗美容诊所","price":"2980","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/36513/"},{"id":"36689","tao_img":"https://p24.yuemei.com/tao/2017/0626/200_200/jt170626171642_a2c420.jpg","label_title":"抗衰老","title":"【抗衰老】北京线雕 蛋白线面部提升 给你紧致肌肤 重现年轻光彩","hospital_name":"北京昕颜医疗美容诊所","price":"5280","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/36689/"}]]
     */

    private String is_show_alert;
    private String alert_type;
    private List<List<TaoListBean>> tao_list;

    public String getIs_show_alert() {
        return is_show_alert;
    }

    public void setIs_show_alert(String is_show_alert) {
        this.is_show_alert = is_show_alert;
    }

    public String getAlert_type() {
        return alert_type;
    }

    public void setAlert_type(String alert_type) {
        this.alert_type = alert_type;
    }

    public List<List<TaoListBean>> getTao_list() {
        return tao_list;
    }

    public void setTao_list(List<List<TaoListBean>> tao_list) {
        this.tao_list = tao_list;
    }

    public static class TaoListBean {
        /**
         * id : 10313
         * tao_img : https://p24.yuemei.com/tao/2018/0104/200_200/jt180104110307_b4035e.jpg
         * label_title : 吸脂瘦脸
         * title : 【吸脂瘦脸】北京微创口腔内吸脂瘦脸 吸走婴儿肥  微创打造V脸美人
         * hospital_name : 北京丽港医疗美容诊所
         * price : 4560
         * icon_label : ["满5000-200","拼团","秒杀"]
         * url : https://m.yuemei.com/tao/10313/
         */

        private String id;
        private String tao_img;
        private String label_title;
        private String title;
        private String hospital_name;
        private String price;
        private String url;
        private List<String> icon_label;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTao_img() {
            return tao_img;
        }

        public void setTao_img(String tao_img) {
            this.tao_img = tao_img;
        }

        public String getLabel_title() {
            return label_title;
        }

        public void setLabel_title(String label_title) {
            this.label_title = label_title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getHospital_name() {
            return hospital_name;
        }

        public void setHospital_name(String hospital_name) {
            this.hospital_name = hospital_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public List<String> getIcon_label() {
            return icon_label;
        }

        public void setIcon_label(List<String> icon_label) {
            this.icon_label = icon_label;
        }
    }
}
