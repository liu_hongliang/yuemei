package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 原成昊
 */
public class RecommentTaoList implements Parcelable {

    /**
     * show_sku_list_position : 10
     * tao_list : [{"id":"1740","tao_img":"https://p24.yuemei.com/tao/2015/0112/20150112114640416573.jpg","label_title":"减肥塑形","title":"【减肥塑形】北京射频溶脂瘦上臂 精细雕塑上臂曲线","hospital_name":"北京丹熙医疗美容诊所","price":"21600","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/1740/"},{"id":"1743","tao_img":"https://p24.yuemei.com/tao/2018/0804/200_200/jt180804125948_4a0a96.jpg","label_title":"减肥塑形","title":"【减肥塑形】北京瘦腿 丹熙院张@魏洁 亲自设计亲自手术 二十年体雕经验 术后隐痕","hospital_name":"北京丹熙医疗美容诊所","price":"36000","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/1743/"},{"id":"2341","tao_img":"https://p24.yuemei.com/tao/2018/0104/200_200/jt180104110534_eab3da.jpg","label_title":"减肥瘦身","title":"【减肥瘦身】北京全方位逐层吸脂 单部位  微创吸脂瘦身","hospital_name":"北京丽港医疗美容诊所","price":"6800","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/2341/"},{"id":"1032","tao_img":"https://p24.yuemei.com/tao/2019/1021/200_200/jt191021173342_6a8b0b.jpg","label_title":"减肥塑形","title":"【减肥塑形】北京吸脂瘦腰腹 甩腰腹赘肉 紧致小蛮腰 瘦腰瘦腹 腰腹减肥 权威专家为您轻松减脂","hospital_name":"北京东方和谐医疗美容诊所","price":"4500","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/1032/"},{"id":"408","tao_img":"https://p24.yuemei.com/tao/2020/0404/200_200/jt200404144501_db504c.png","label_title":"面部填充","title":"【面部填充】北京脂肪填充单部位 重现青春 立体圆润 绽放美丽笑容","hospital_name":"北京丽星翼美医疗美容诊所","price":"2680","icon_label":["满5000-200","拼团","秒杀","满5000减500"],"url":"https://m.yuemei.com/tao/408/"},{"id":"665","tao_img":"https://p24.yuemei.com/tao/2017/1010/200_200/jt171010104802_2a4995.jpg","label_title":"减肥塑形","title":"【减肥塑形】王沛森射频溶脂瘦手臂 拒绝凹凸不平  术后无痕  甩去恼人蝴蝶臂","hospital_name":"北京京韩整形","price":"38500","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/665/"},{"id":"803","tao_img":"https://p24.yuemei.com/tao/2019/0909/200_200/jt190909172822_66410b.jpg","label_title":"面部填充","title":"【面部填充】北京VST脂肪精雕填充泪沟 赶走凹陷细纹 年轻紧致","hospital_name":"北京京韩整形","price":"15620","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/803/"},{"id":"172","tao_img":"https://p24.yuemei.com/tao/2019/1224/200_200/jt191224092356_c470af.jpg","label_title":"注射微整","title":"【注射微整】♥瑞蓝玻尿酸♥填充塑形长效持久♥立体五官/注射卧蚕泪沟/填充凹陷♥","hospital_name":"北京伟力嘉美信医疗美容门诊部","price":"5901","icon_label":["满5000-200","拼团","秒杀","满4999减399"],"url":"https://m.yuemei.com/tao/172/"},{"id":"16729","tao_img":"https://p24.yuemei.com/tao/2019/0310/200_200/jt190310155041_139112.jpg","label_title":"水光针注射美容","title":"【水光针注射美容】调配水光注射 全身肌肤白嫩无瑕  2.5折超值价  手慢无啊！！","hospital_name":"北京普蕊丰亭医疗美容诊所","price":"388","icon_label":["满5000-200","拼团","秒杀"],"url":"https://m.yuemei.com/tao/16729/"}]
     */

    private String show_sku_list_position;
    private List<TaoListBean> tao_list;

    public String getShow_sku_list_position() {
        return show_sku_list_position;
    }

    public void setShow_sku_list_position(String show_sku_list_position) {
        this.show_sku_list_position = show_sku_list_position;
    }

    public List<TaoListBean> getTao_list() {
        return tao_list;
    }

    public void setTao_list(List<TaoListBean> tao_list) {
        this.tao_list = tao_list;
    }

    public static class TaoListBean implements Parcelable {
        /**
         * id : 1740
         * tao_img : https://p24.yuemei.com/tao/2015/0112/20150112114640416573.jpg
         * label_title : 减肥塑形
         * title : 【减肥塑形】北京射频溶脂瘦上臂 精细雕塑上臂曲线
         * hospital_name : 北京丹熙医疗美容诊所
         * price : 21600
         * icon_label : ["满5000-200","拼团","秒杀"]
         * url : https://m.yuemei.com/tao/1740/
         */

        private String id;
        private String tao_img;
        private String label_title;
        private String title;
        private String hospital_name;
        private String price;
        private String url;
        private List<String> icon_label;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTao_img() {
            return tao_img;
        }

        public void setTao_img(String tao_img) {
            this.tao_img = tao_img;
        }

        public String getLabel_title() {
            return label_title;
        }

        public void setLabel_title(String label_title) {
            this.label_title = label_title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getHospital_name() {
            return hospital_name;
        }

        public void setHospital_name(String hospital_name) {
            this.hospital_name = hospital_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public List<String> getIcon_label() {
            return icon_label;
        }

        public void setIcon_label(List<String> icon_label) {
            this.icon_label = icon_label;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.tao_img);
            dest.writeString(this.label_title);
            dest.writeString(this.title);
            dest.writeString(this.hospital_name);
            dest.writeString(this.price);
            dest.writeString(this.url);
            dest.writeStringList(this.icon_label);
        }

        public TaoListBean() {
        }

        protected TaoListBean(Parcel in) {
            this.id = in.readString();
            this.tao_img = in.readString();
            this.label_title = in.readString();
            this.title = in.readString();
            this.hospital_name = in.readString();
            this.price = in.readString();
            this.url = in.readString();
            this.icon_label = in.createStringArrayList();
        }

        public static final Creator<TaoListBean> CREATOR = new Creator<TaoListBean>() {
            @Override
            public TaoListBean createFromParcel(Parcel source) {
                return new TaoListBean(source);
            }

            @Override
            public TaoListBean[] newArray(int size) {
                return new TaoListBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.show_sku_list_position);
        dest.writeList(this.tao_list);
    }

    public RecommentTaoList() {
    }

    protected RecommentTaoList(Parcel in) {
        this.show_sku_list_position = in.readString();
        this.tao_list = new ArrayList<TaoListBean>();
        in.readList(this.tao_list, TaoListBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<RecommentTaoList> CREATOR = new Parcelable.Creator<RecommentTaoList>() {
        @Override
        public RecommentTaoList createFromParcel(Parcel source) {
            return new RecommentTaoList(source);
        }

        @Override
        public RecommentTaoList[] newArray(int size) {
            return new RecommentTaoList[size];
        }
    };
}
