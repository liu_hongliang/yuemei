package com.module.home.model.bean;

import java.util.List;

public class FeedSkuRecommend {
    //要插入的位置
    private int insertPos;

    private String post_id;

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    private List<String> second_parts;

    public List<String> getSecond_parts() {
        return second_parts;
    }

    public void setSecond_parts(List<String> second_parts) {
        this.second_parts = second_parts;
    }

    public int getInsertPos() {
        return insertPos;
    }

    public void setInsertPos(int insertPos) {
        this.insertPos = insertPos;
    }
}
