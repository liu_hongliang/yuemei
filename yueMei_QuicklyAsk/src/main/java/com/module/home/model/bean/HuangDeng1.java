package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by dwb on 16/2/23.
 */
public class HuangDeng1 {

    private String bmsid;
    private String qid;
    private String url;
    private String flag;
    private String img;
    private String img_new;
    private String title;
    private String type;
    private String metro_line;
    private String h_w;
    private String appmurl;
    private HashMap<String, String> event_params;
    private String colorTop;
    private String colorBottom;
    private String metro_type;
    private CountDownArr countdownArr;

    public CountDownArr getCountdownArr() {
        return countdownArr;
    }

    public void setCountdownArr(CountDownArr countdownArr) {
        this.countdownArr = countdownArr;
    }

    public String getMetro_type() {
        return metro_type;
    }

    public void setMetro_type(String metro_type) {
        this.metro_type = metro_type;
    }

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_new() {
        return img_new;
    }

    public void setImg_new(String img_new) {
        this.img_new = img_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetro_line() {
        return metro_line;
    }

    public void setMetro_line(String metro_line) {
        this.metro_line = metro_line;
    }

    public String getH_w() {
        return h_w;
    }

    public void setH_w(String h_w) {
        this.h_w = h_w;
    }

    public String getAppmurl() {
        return appmurl;
    }

    public void setAppmurl(String appmurl) {
        this.appmurl = appmurl;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public String getColorTop() {
        return colorTop;
    }

    public void setColorTop(String colorTop) {
        this.colorTop = colorTop;
    }

    public String getColorBottom() {
        return colorBottom;
    }

    public void setColorBottom(String colorBottom) {
        this.colorBottom = colorBottom;
    }
}