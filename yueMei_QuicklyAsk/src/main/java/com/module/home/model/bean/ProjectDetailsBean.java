package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2019/3/5
 */
public class ProjectDetailsBean implements Parcelable {
    private String twoLabelId;
    private String fourLabelId;
    private String homeSource;
    private int topHeight;

    public ProjectDetailsBean() {
    }

    protected ProjectDetailsBean(Parcel in) {
        twoLabelId = in.readString();
        fourLabelId = in.readString();
        homeSource = in.readString();
        topHeight = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(twoLabelId);
        dest.writeString(fourLabelId);
        dest.writeString(homeSource);
        dest.writeInt(topHeight);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProjectDetailsBean> CREATOR = new Creator<ProjectDetailsBean>() {
        @Override
        public ProjectDetailsBean createFromParcel(Parcel in) {
            return new ProjectDetailsBean(in);
        }

        @Override
        public ProjectDetailsBean[] newArray(int size) {
            return new ProjectDetailsBean[size];
        }
    };

    public String getTwoLabelId() {
        return twoLabelId;
    }

    public void setTwoLabelId(String twoLabelId) {
        this.twoLabelId = twoLabelId;
    }

    public String getFourLabelId() {
        return fourLabelId;
    }

    public void setFourLabelId(String fourLabelId) {
        this.fourLabelId = fourLabelId;
    }

    public String getHomeSource() {
        return homeSource;
    }

    public void setHomeSource(String homeSource) {
        this.homeSource = homeSource;
    }

    public int getTopHeight() {
        return topHeight;
    }

    public void setTopHeight(int topHeight) {
        this.topHeight = topHeight;
    }
}
