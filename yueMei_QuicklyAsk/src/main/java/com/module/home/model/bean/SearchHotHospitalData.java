package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/9/9
 */
public class SearchHotHospitalData {
    private String hospital_url;
    private String hospital_banner;
    private String hospital_name;
    private HashMap<String, String> event_params;


    public String getHospital_url() {
        return hospital_url;
    }

    public void setHospital_url(String hospital_url) {
        this.hospital_url = hospital_url;
    }

    public String getHospital_banner() {
        return hospital_banner;
    }

    public void setHospital_banner(String hospital_banner) {
        this.hospital_banner = hospital_banner;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
