package com.module.home.model.bean;

public class SaozfData {

	private String order_id;
	private String tao_name;
	private String price;
	private String tao_id;
	private String is_insure;
	private String is_repayment;
	private String is_repayment_mimo;


	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getTao_name() {
		return tao_name;
	}

	public void setTao_name(String tao_name) {
		this.tao_name = tao_name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTao_id() {
		return tao_id;
	}

	public void setTao_id(String tao_id) {
		this.tao_id = tao_id;
	}

	public String getIs_insure() {
		return is_insure;
	}

	public void setIs_insure(String is_insure) {
		this.is_insure = is_insure;
	}

	public String getIs_repayment() {
		return is_repayment;
	}

	public void setIs_repayment(String is_repayment) {
		this.is_repayment = is_repayment;
	}

	public String getIs_repayment_mimo() {
		return is_repayment_mimo;
	}

	public void setIs_repayment_mimo(String is_repayment_mimo) {
		this.is_repayment_mimo = is_repayment_mimo;
	}
}
