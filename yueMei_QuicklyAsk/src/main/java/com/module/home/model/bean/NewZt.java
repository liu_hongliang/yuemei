package com.module.home.model.bean;

import java.util.HashMap;
import java.util.List;

public class NewZt {
    private String url;
    private List<TaoData> taodata;
    private NewZtCoupons coupons;

    private HashMap<String,String> event_params;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<TaoData> getTaodata() {
        return taodata;
    }

    public void setTaodata(List<TaoData> taodata) {
        this.taodata = taodata;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }

    public NewZtCoupons getCoupons() {
        return coupons;
    }

    public void setCoupons(NewZtCoupons coupons) {
        this.coupons = coupons;
    }
}
