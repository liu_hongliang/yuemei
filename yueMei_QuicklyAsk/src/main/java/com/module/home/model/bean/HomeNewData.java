package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/19
 */
public class HomeNewData implements Parcelable {
    private List<SearchEntry> searchEntry;
    private List<String> hotSearchWord;
    private List<HuangDeng1> huandeng;
    private List<HuangDeng1> metroBigPromotion;
    private HomeNav nav;
    private List<CardBean> kapian;
    private List<List<HuangDeng1>> metro_top;
    private NewZt new_zt;
    private NewZtBottom new_zt_bottom;
    private List<List<HomeActivityData>> activity;
    private List<HuangDeng1> express;
    private List<HotBean> hot;
    private List<BoardBean> board;
    private ArrayList<Tuijshare> tuijshare;
    private Floating floating;
    private Coupos coupons;
    private HomeButtomFloat buttomFloat;
    private String BigPromotionBgImage;
    private List<NewZtCouponsBean> newZtCoupons;
    private BigPromotionHomeCeilingBean bigPromotionHomeCeiling;

    protected HomeNewData(Parcel in) {
        searchEntry = in.createTypedArrayList(SearchEntry.CREATOR);
        hotSearchWord = in.createStringArrayList();
        tuijshare = in.createTypedArrayList(Tuijshare.CREATOR);
        BigPromotionBgImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(searchEntry);
        dest.writeStringList(hotSearchWord);
        dest.writeTypedList(tuijshare);
        dest.writeString(BigPromotionBgImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeNewData> CREATOR = new Creator<HomeNewData>() {
        @Override
        public HomeNewData createFromParcel(Parcel in) {
            return new HomeNewData(in);
        }

        @Override
        public HomeNewData[] newArray(int size) {
            return new HomeNewData[size];
        }
    };

    public List<SearchEntry> getSearchEntry() {
        return searchEntry;
    }

    public void setSearchEntry(List<SearchEntry> searchEntry) {
        this.searchEntry = searchEntry;
    }

    public List<String> getHotSearchWord() {
        return hotSearchWord;
    }

    public void setHotSearchWord(List<String> hotSearchWord) {
        this.hotSearchWord = hotSearchWord;
    }

    public List<HuangDeng1> getHuandeng() {
        return huandeng;
    }

    public void setHuandeng(List<HuangDeng1> huandeng) {
        this.huandeng = huandeng;
    }

    public List<HuangDeng1> getMetroBigPromotion() {
        return metroBigPromotion;
    }

    public void setMetroBigPromotion(List<HuangDeng1> metroBigPromotion) {
        this.metroBigPromotion = metroBigPromotion;
    }

    public HomeNav getNav() {
        return nav;
    }

    public void setNav(HomeNav nav) {
        this.nav = nav;
    }

    public List<CardBean> getKapian() {
        return kapian;
    }

    public void setKapian(List<CardBean> kapian) {
        this.kapian = kapian;
    }

    public List<List<HuangDeng1>> getMetro_top() {
        return metro_top;
    }

    public void setMetro_top(List<List<HuangDeng1>> metro_top) {
        this.metro_top = metro_top;
    }

    public NewZt getNew_zt() {
        return new_zt;
    }

    public void setNew_zt(NewZt new_zt) {
        this.new_zt = new_zt;
    }

    public NewZtBottom getNew_zt_bottom() {
        return new_zt_bottom;
    }

    public void setNew_zt_bottom(NewZtBottom new_zt_bottom) {
        this.new_zt_bottom = new_zt_bottom;
    }

    public List<List<HomeActivityData>> getActivity() {
        return activity;
    }

    public void setActivity(List<List<HomeActivityData>> activity) {
        this.activity = activity;
    }

    public List<HuangDeng1> getExpress() {
        return express;
    }

    public void setExpress(List<HuangDeng1> express) {
        this.express = express;
    }

    public List<HotBean> getHot() {
        return hot;
    }

    public void setHot(List<HotBean> hot) {
        this.hot = hot;
    }

    public List<BoardBean> getBoard() {
        return board;
    }

    public void setBoard(List<BoardBean> board) {
        this.board = board;
    }

    public ArrayList<Tuijshare> getTuijshare() {
        return tuijshare;
    }

    public void setTuijshare(ArrayList<Tuijshare> tuijshare) {
        this.tuijshare = tuijshare;
    }

    public Floating getFloating() {
        return floating;
    }

    public void setFloating(Floating floating) {
        this.floating = floating;
    }

    public Coupos getCoupons() {
        return coupons;
    }

    public void setCoupons(Coupos coupons) {
        this.coupons = coupons;
    }

    public HomeButtomFloat getButtomFloat() {
        return buttomFloat;
    }

    public void setButtomFloat(HomeButtomFloat buttomFloat) {
        this.buttomFloat = buttomFloat;
    }

    public String getBigPromotionBgImage() {
        return BigPromotionBgImage;
    }

    public void setBigPromotionBgImage(String bigPromotionBgImage) {
        BigPromotionBgImage = bigPromotionBgImage;
    }

    public List<NewZtCouponsBean> getNewZtCoupons() {
        return newZtCoupons;
    }

    public void setNewZtCoupons(List<NewZtCouponsBean> newZtCoupons) {
        this.newZtCoupons = newZtCoupons;
    }

    public BigPromotionHomeCeilingBean getBigPromotionHomeCeiling() {
        return bigPromotionHomeCeiling;
    }

    public void setBigPromotionHomeCeiling(BigPromotionHomeCeilingBean bigPromotionHomeCeiling) {
        this.bigPromotionHomeCeiling = bigPromotionHomeCeiling;
    }

}
