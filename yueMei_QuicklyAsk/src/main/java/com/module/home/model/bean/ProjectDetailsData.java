package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HosListData;
import com.module.taodetail.model.bean.HomeTaoData;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class ProjectDetailsData implements Parcelable {
    private ArrayList<ManualPosition> manualPosition;                           //手工位
    private LabelEncyclopedia labelEncyclopedia;                                //标签百科(仅四级标签)
    private ProjectDetailsBrand brand;                                          //品牌
    private ArrayList<HomeTaoData> taoList;                                     //淘整形列表
    private ArrayList<HosListData> hospitalList;                                //医院列表
    private ArrayList<DocListData> doctorsList;                                 //医生列表
    private ArrayList<BBsListData550> diaryList;                                //日记列表
    private ArrayList<QuestionListData> questionList;                           //问答列表
    private SearchResultDoctor comparedConsultative;                            //对比咨询医生信息
    private RecommentTaoList recommend_tao_list;                                //品类推荐

    protected ProjectDetailsData(Parcel in) {
        labelEncyclopedia = in.readParcelable(LabelEncyclopedia.class.getClassLoader());
        brand = in.readParcelable(ProjectDetailsBrand.class.getClassLoader());
        taoList = in.createTypedArrayList(HomeTaoData.CREATOR);
        hospitalList = in.createTypedArrayList(HosListData.CREATOR);
        doctorsList = in.createTypedArrayList(DocListData.CREATOR);
        diaryList = in.createTypedArrayList(BBsListData550.CREATOR);
        questionList = in.createTypedArrayList(QuestionListData.CREATOR);
        recommend_tao_list = in.readParcelable(RecommentTaoList.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(labelEncyclopedia, flags);
        dest.writeParcelable(brand, flags);
        dest.writeTypedList(taoList);
        dest.writeTypedList(hospitalList);
        dest.writeTypedList(doctorsList);
        dest.writeTypedList(diaryList);
        dest.writeTypedList(questionList);
        dest.writeParcelable(recommend_tao_list, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProjectDetailsData> CREATOR = new Creator<ProjectDetailsData>() {
        @Override
        public ProjectDetailsData createFromParcel(Parcel in) {
            return new ProjectDetailsData(in);
        }

        @Override
        public ProjectDetailsData[] newArray(int size) {
            return new ProjectDetailsData[size];
        }
    };

    public RecommentTaoList getRecommend_tao_list() {
        return recommend_tao_list;
    }

    public void setRecommend_tao_list(RecommentTaoList recommend_tao_list) {
        this.recommend_tao_list = recommend_tao_list;
    }

    public ArrayList<ManualPosition> getManualPosition() {
        return manualPosition;
    }

    public void setManualPosition(ArrayList<ManualPosition> manualPosition) {
        this.manualPosition = manualPosition;
    }

    public LabelEncyclopedia getLabelEncyclopedia() {
        return labelEncyclopedia;
    }

    public void setLabelEncyclopedia(LabelEncyclopedia labelEncyclopedia) {
        this.labelEncyclopedia = labelEncyclopedia;
    }

    public ProjectDetailsBrand getBrand() {
        return brand;
    }

    public void setBrand(ProjectDetailsBrand brand) {
        this.brand = brand;
    }

    public ArrayList<HomeTaoData> getTaoList() {
        return taoList;
    }

    public void setTaoList(ArrayList<HomeTaoData> taoList) {
        this.taoList = taoList;
    }

    public ArrayList<HosListData> getHospitalList() {
        return hospitalList;
    }

    public void setHospitalList(ArrayList<HosListData> hospitalList) {
        this.hospitalList = hospitalList;
    }

    public ArrayList<DocListData> getDoctorsList() {
        return doctorsList;
    }

    public void setDoctorsList(ArrayList<DocListData> doctorsList) {
        this.doctorsList = doctorsList;
    }

    public ArrayList<BBsListData550> getDiaryList() {
        return diaryList;
    }

    public void setDiaryList(ArrayList<BBsListData550> diaryList) {
        this.diaryList = diaryList;
    }

    public ArrayList<QuestionListData> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ArrayList<QuestionListData> questionList) {
        this.questionList = questionList;
    }

    public SearchResultDoctor getComparedConsultative() {
        return comparedConsultative;
    }

    public void setComparedConsultative(SearchResultDoctor comparedConsultative) {
        this.comparedConsultative = comparedConsultative;
    }
}
