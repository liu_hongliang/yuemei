package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/4/13
 */
public class ProjectDetailsMoreUrl {
    private String chosenDiary;
    private String recommendDoctors;
    private String recommendHospital;

    public String getChosenDiary() {
        return chosenDiary;
    }

    public void setChosenDiary(String chosenDiary) {
        this.chosenDiary = chosenDiary;
    }

    public String getRecommendDoctors() {
        return recommendDoctors;
    }

    public void setRecommendDoctors(String recommendDoctors) {
        this.recommendDoctors = recommendDoctors;
    }

    public String getRecommendHospital() {
        return recommendHospital;
    }

    public void setRecommendHospital(String recommendHospital) {
        this.recommendHospital = recommendHospital;
    }
}
