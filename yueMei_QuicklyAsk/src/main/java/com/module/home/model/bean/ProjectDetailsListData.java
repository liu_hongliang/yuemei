package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by 裴成浩 on 2019/2/27
 */
public class ProjectDetailsListData implements Parcelable {
    private String id;
    private String name;
    private boolean isSelected = false;

    public ProjectDetailsListData(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public ProjectDetailsListData(String id, String name,boolean isSelected) {
        this.id = id;
        this.name = name;
        this.isSelected = isSelected;
    }

    protected ProjectDetailsListData(Parcel in) {
        id = in.readString();
        name = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProjectDetailsListData> CREATOR = new Creator<ProjectDetailsListData>() {
        @Override
        public ProjectDetailsListData createFromParcel(Parcel in) {
            return new ProjectDetailsListData(in);
        }

        @Override
        public ProjectDetailsListData[] newArray(int size) {
            return new ProjectDetailsListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
