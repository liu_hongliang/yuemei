package com.module.home.model.bean;


import com.module.taodetail.model.bean.HomeTaoData;

import java.util.List;

/**
 * Created by dwb on 16/2/22.
 */
public class HomeBoard {

    private String id;
    private String title;
    private String desc;
    private List<HomeTaoData> tao;
    private String more_title;
    private String more_link;
    private String more_linkType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<HomeTaoData> getTao() {
        return tao;
    }

    public void setTao(List<HomeTaoData> tao) {
        this.tao = tao;
    }

    public String getMore_title() {
        return more_title;
    }

    public void setMore_title(String more_title) {
        this.more_title = more_title;
    }

    public String getMore_link() {
        return more_link;
    }

    public void setMore_link(String more_link) {
        this.more_link = more_link;
    }

    public String getMore_linkType() {
        return more_linkType;
    }

    public void setMore_linkType(String more_linkType) {
        this.more_linkType = more_linkType;
    }
}
