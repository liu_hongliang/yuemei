package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2017/7/31.
 */

public class Home623 {
    private String code;
    private String message;
    private HomeData623 data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HomeData623 getData() {
        return data;
    }

    public void setData(HomeData623 data) {
        this.data = data;
    }




}
