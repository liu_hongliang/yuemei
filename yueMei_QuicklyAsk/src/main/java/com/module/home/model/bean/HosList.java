/**
 * 
 */
package com.module.home.model.bean;


import com.module.doctor.model.bean.HosListData;

import java.util.List;

/**
 * 医院列表
 * 
 * @author Rubin
 * 
 */
public class HosList {

	private String code;
	private String message;

	private List<HosListData> data;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the data
	 */
	public List<HosListData> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<HosListData> data) {
		this.data = data;
	}

}
