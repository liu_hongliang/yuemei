package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/5/24
 */
public class SearchResultLike {
    private String id;
    private String url;
    private String name;
    private HashMap<String,String> event_params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
