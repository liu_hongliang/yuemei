package com.module.home.model.bean;

/**
 * Created by 裴成浩 on 2019/8/21
 */
public class HomeButtomFloat {
    private String showTime;
    private String title;
    private String url;

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
