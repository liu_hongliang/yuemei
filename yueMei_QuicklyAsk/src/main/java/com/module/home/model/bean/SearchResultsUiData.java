package com.module.home.model.bean;

import com.module.base.view.YMBaseFragment;

/**
 * Created by 裴成浩 on 2019/9/5
 */
public class SearchResultsUiData {
    private String type;                                        //type请求值
    private String title;                                       //标题名
    private YMBaseFragment fragment;                            //fragment页面

    public SearchResultsUiData(String type, String title) {
        this.type = type;
        this.title = title;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public YMBaseFragment getFragment() {
        return fragment;
    }

    public void setFragment(YMBaseFragment fragment) {
        this.fragment = fragment;
    }
}
