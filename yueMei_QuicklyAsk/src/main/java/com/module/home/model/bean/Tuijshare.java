package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.module.community.model.bean.BBsListData550;
import com.module.taodetail.model.bean.HomeTaoData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2017/8/1.
 */

public class Tuijshare implements MultiItemEntity, Parcelable {

    private int itemType = 0;
    private BBsListData550 post;
    private TuijOther other;
    private TuijHosDoc hos_doc;
    private List<FeedSkuRecommendData> feedSkuRecommendData;
    private HashMap<String, String> show_control;

    public List<FeedSkuRecommendData> getFeedSkuRecommendData() {
        return feedSkuRecommendData;
    }

    public void setFeedSkuRecommendData(List<FeedSkuRecommendData> feedSkuRecommendData) {
        this.feedSkuRecommendData = feedSkuRecommendData;
    }

    public Tuijshare() {

    }

    public Tuijshare(BBsListData550 post) {
        this.post = post;
    }


    public BBsListData550 getPost() {
        return post;
    }

    public void setPost(BBsListData550 post) {
        this.post = post;
    }

    public TuijOther getOther() {
        return other;
    }

    public void setOther(TuijOther other) {
        this.other = other;
    }

    public TuijHosDoc getHos_doc() {
        return hos_doc;
    }

    public void setHos_doc(TuijHosDoc hos_doc) {
        this.hos_doc = hos_doc;
    }


    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    @Override
    public int getItemType() {
        return itemType;
    }

    public HashMap<String, String> getShow_control() {
        return show_control;
    }

    public void setShow_control(HashMap<String, String> show_control) {
        this.show_control = show_control;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.itemType);
        dest.writeParcelable(this.post, flags);
        dest.writeParcelable(this.other, flags);
        dest.writeParcelable(this.hos_doc, flags);
        dest.writeSerializable(this.show_control);
        dest.writeTypedList(this.feedSkuRecommendData);
    }

    protected Tuijshare(Parcel in) {
        this.itemType = in.readInt();
        this.post = in.readParcelable(BBsListData550.class.getClassLoader());
        this.other = in.readParcelable(TuijOther.class.getClassLoader());
        this.hos_doc = in.readParcelable(TuijHosDoc.class.getClassLoader());
        this.show_control = (HashMap<String, String>) in.readSerializable();
        this.feedSkuRecommendData = in.createTypedArrayList(FeedSkuRecommendData.CREATOR);
    }

    public static final Parcelable.Creator<Tuijshare> CREATOR = new Parcelable.Creator<Tuijshare>() {
        @Override
        public Tuijshare createFromParcel(Parcel source) {
            return new Tuijshare(source);
        }

        @Override
        public Tuijshare[] newArray(int size) {
            return new Tuijshare[size];
        }
    };
}
