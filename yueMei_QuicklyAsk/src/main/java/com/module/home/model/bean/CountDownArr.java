package com.module.home.model.bean;

/**
 * Created by dwb on 16/2/22.
 */
public class CountDownArr {

    private String title;
    private String titleColor;
    private String timeBgColor;
    private String end_time;
    private String timeColor;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(String titleColor) {
        this.titleColor = titleColor;
    }

    public String getTimeBgColor() {
        return timeBgColor;
    }

    public void setTimeBgColor(String timeBgColor) {
        this.timeBgColor = timeBgColor;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTimeColor() {
        return timeColor;
    }

    public void setTimeColor(String timeColor) {
        this.timeColor = timeColor;
    }
}
