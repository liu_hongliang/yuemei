package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.entity.NewsNumberData;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * 消息数获取
 * Created by 裴成浩 on 2017/10/13.
 */

public class MessagecountApi implements BaseCallBackApi {

    private String TAG = "MessagecountApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.MESSAGE, "messagecount", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {

                if ("1".equals(mData.code)) {

                    try {
                        NewsNumberData newsNumberData = JSONUtil.TransformSingleBean(mData.data, NewsNumberData.class);
                        listener.onSuccess(newsNumberData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
