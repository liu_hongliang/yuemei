package com.module.home.model.bean;

public class SearchXS {

	private String code;
	private String message;
	private SearchXSData data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SearchXSData getData() {
		return data;
	}

	public void setData(SearchXSData data) {
		this.data = data;
	}

}
