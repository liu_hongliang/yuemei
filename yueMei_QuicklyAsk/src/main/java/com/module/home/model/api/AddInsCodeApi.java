package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.entity.JFJY1;
import com.quicklyask.util.JSONUtil;

import java.util.Map;

/**
 * Created by 裴成浩 on 2017/10/12.
 */

public class AddInsCodeApi implements BaseCallBackApi {
    private String TAG = "AddInsCodeApi";

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {

        NetWork.getInstance().call(FinalConstant1.INTEGRALTASK, "writecode", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                if ("1".equals(mData.code)) {
                    try {
                        JFJY1 jfjy1 = JSONUtil.TransformSingleBean(mData.data, JFJY1.class);
                        listener.onSuccess(jfjy1);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
