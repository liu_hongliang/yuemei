package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ComparedBean implements Parcelable {
    private String showSkuListPosition;
    private HashMap<String,String> changeOneEventParams;
    private HashMap<String,String> exposureEventParams;
    private List<List<SearchResultDoctorList>> doctorsList;

    public String getShowSkuListPosition() {
        return showSkuListPosition;
    }

    public void setShowSkuListPosition(String showSkuListPosition) {
        this.showSkuListPosition = showSkuListPosition;
    }

    public HashMap<String,String> getChangeOneEventParams() {
        return changeOneEventParams;
    }

    public void setChangeOneEventParams(HashMap<String,String> changeOneEventParams) {
        this.changeOneEventParams = changeOneEventParams;
    }

    public HashMap<String,String> getExposureEventParams() {
        return exposureEventParams;
    }

    public void setExposureEventParams(HashMap<String,String> exposureEventParams) {
        this.exposureEventParams = exposureEventParams;
    }

    public List<List<SearchResultDoctorList>> getDoctorsList() {
        return doctorsList;
    }

    public void setDoctorsList(List<List<SearchResultDoctorList>> doctorsList) {
        this.doctorsList = doctorsList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.showSkuListPosition);
        dest.writeSerializable(this.changeOneEventParams);
        dest.writeSerializable(this.exposureEventParams);
        dest.writeList(this.doctorsList);
    }

    public ComparedBean() {
    }

    protected ComparedBean(Parcel in) {
        this.showSkuListPosition = in.readString();
        this.changeOneEventParams = (HashMap<String, String>) in.readSerializable();
        this.exposureEventParams = (HashMap<String, String>) in.readSerializable();
        this.doctorsList = new ArrayList<List<SearchResultDoctorList>>();
        in.readList(this.doctorsList, ComparedBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<ComparedBean> CREATOR = new Parcelable.Creator<ComparedBean>() {
        @Override
        public ComparedBean createFromParcel(Parcel source) {
            return new ComparedBean(source);
        }

        @Override
        public ComparedBean[] newArray(int size) {
            return new ComparedBean[size];
        }
    };
}
