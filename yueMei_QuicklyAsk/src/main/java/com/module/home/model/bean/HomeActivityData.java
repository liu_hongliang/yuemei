package com.module.home.model.bean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/20
 */
public class HomeActivityData {
    private String bmsid;
    private String url;
    private String img;
    private String img_new;
    private String title;
    private String color_value;
    private String colour_value;
    private List<HomeActivityImgData> recommend_img;
    private String endTime;
    private String miaosha;
    private String swiper;
    private HashMap<String,String> event_params;

    public String getBmsid() {
        return bmsid;
    }

    public void setBmsid(String bmsid) {
        this.bmsid = bmsid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg_new() {
        return img_new;
    }

    public void setImg_new(String img_new) {
        this.img_new = img_new;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor_value() {
        return color_value;
    }

    public void setColor_value(String color_value) {
        this.color_value = color_value;
    }

    public String getColour_value() {
        return colour_value;
    }

    public void setColour_value(String colour_value) {
        this.colour_value = colour_value;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<HomeActivityImgData> getRecommend_img() {
        return recommend_img;
    }

    public void setRecommend_img(List<HomeActivityImgData> recommend_img) {
        this.recommend_img = recommend_img;
    }

    public String getMiaosha() {
        return miaosha;
    }

    public void setMiaosha(String miaosha) {
        this.miaosha = miaosha;
    }

    public String getSwiper() {
        return swiper;
    }

    public void setSwiper(String swiper) {
        this.swiper = swiper;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
