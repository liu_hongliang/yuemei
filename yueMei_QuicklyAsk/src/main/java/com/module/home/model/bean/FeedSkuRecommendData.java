package com.module.home.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedSkuRecommendData implements Parcelable {

    /**
     * id : 101380
     * price : 38800
     * title : 【下颌角整形】公立医院专家团 下颌角磨骨 限时特惠 私信送红包
     * url : https://m.yuemei.com/tao/101380/
     * price_discount : 8100
     * img : https://p24.yuemei.com/tao/2020/0330/200_200/jt200330162618_aebbe8.jpg
     * hospital_id : 12181
     */

    private String id;
    private String price;
    private String title;
    private String url;
    private String price_discount;
    private String img;
    private String hospital_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.price);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.price_discount);
        dest.writeString(this.img);
        dest.writeString(this.hospital_id);
    }

    public FeedSkuRecommendData() {
    }

    protected FeedSkuRecommendData(Parcel in) {
        this.id = in.readString();
        this.price = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        this.price_discount = in.readString();
        this.img = in.readString();
        this.hospital_id = in.readString();
    }

    public static final Parcelable.Creator<FeedSkuRecommendData> CREATOR = new Parcelable.Creator<FeedSkuRecommendData>() {
        @Override
        public FeedSkuRecommendData createFromParcel(Parcel source) {
            return new FeedSkuRecommendData(source);
        }

        @Override
        public FeedSkuRecommendData[] newArray(int size) {
            return new FeedSkuRecommendData[size];
        }
    };
}
