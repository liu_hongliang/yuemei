package com.module.home.model.bean;

import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/6/19
 */
public class SearchResultCompare {
    private String id;
    private String name;
    private String img;
    private String url;
    private HashMap<String,String> event_params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
