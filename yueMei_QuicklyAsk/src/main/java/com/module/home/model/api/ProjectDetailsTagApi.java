package com.module.home.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.model.bean.ProjectDetailsTag;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/2/26
 */
public class ProjectDetailsTagApi implements BaseCallBackApi {
    private String TAG = "ProjectDetailsTagApi";
    private HashMap<String, Object> mProjectDetailsTagHashMap;  //传值容器

    public ProjectDetailsTagApi() {
        mProjectDetailsTagHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CHANNEL, "getpartalllabel", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, mData.toString());
                try {
                    if ("1".equals(mData.code)) {
                        List<ProjectDetailsTag> makeTagListData = JSONUtil.jsonToArrayList(mData.data, ProjectDetailsTag.class);
                        listener.onSuccess(makeTagListData);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "e == " + e.toString());
                    e.printStackTrace();
                }

            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mProjectDetailsTagHashMap;
    }

    public void addData(String key, String value) {
        mProjectDetailsTagHashMap.put(key, value);
    }
}
