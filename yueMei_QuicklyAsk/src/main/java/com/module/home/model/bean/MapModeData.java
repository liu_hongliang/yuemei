package com.module.home.model.bean;


import com.baidu.mapapi.map.Marker;

/**
 * 文 件 名: MapModeData
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-16 22:42
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class MapModeData {
    String title;
    String score;
    String imgUrl;
    LatLng latLng;
    String hos_id;
    Marker marker;

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public static class LatLng {
        String Lat;
        String Lon;

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLon() {
            return Lon;
        }

        public void setLon(String lon) {
            Lon = lon;
        }
    }
}
