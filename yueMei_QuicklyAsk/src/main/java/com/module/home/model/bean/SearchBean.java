package com.module.home.model.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/4/27.
 */

public class SearchBean {

    /**
     * total : 328
     * list : [{},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2015/1030/200_200/151030142929_9e44fc.jpg","title":"北京面部精细水动力吸脂","subtitle":"面颊部、下颌吸脂 没有双下巴 告别婴儿肥 从此爱上自拍","hos_name":"北京美雅枫医疗美容医院","doc_name":"康虹","price":"30000","price_discount":"18000","price_range_max":"0","_id":"20107","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"2人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2017/1003/200_200/jt171003112141_51845c.jpg","title":"减肥塑形","subtitle":"北京360水动力吸脂","hos_name":"北京美再生医疗美容诊所","doc_name":"李龙","price":"3800","price_discount":"780","price_range_max":"0","_id":"64773","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"23人预订","feeScale":"/部位","is_fanxian":"0"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0310/200_200/jt180310151033_9701f0.jpg","title":"面部填充","subtitle":"北京面部综合套餐 面部精塑 告别\u201c大脸猫\u201d","hos_name":"北京东田丽格医疗美容诊所","doc_name":"杨明强","price":"18000","price_discount":"6800","price_range_max":"0","_id":"4485","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101314_aac2f2.jpg","title":"吸脂瘦身","subtitle":"北京瘦腰腹 吸脂不是目的 体型雕塑是关键","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"32800","price_discount":"26000","price_range_max":"0","_id":"70133","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"167人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101238_6d9f1e.jpg","title":"吸脂瘦身","subtitle":"北京水动力吸脂瘦大腿 体型雕塑我们不一样","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"42000","price_discount":"36000","price_range_max":"0","_id":"70399","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"216人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101526_eee54f.jpg","title":"瘦手臂","subtitle":"北京手臂吸脂玲珑臂再造 名媛圈首选 纯手工雕塑化吸脂","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"22000","price_discount":"16000","price_range_max":"0","_id":"69581","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"174人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101926_ec7624.jpg","title":"减肥塑形","subtitle":"北京纯手工雕塑化吸脂 站在艺术的高度去设计 吸脂体型雕塑是关键","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"5000","price_discount":"4000","price_range_max":"0","_id":"68819","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/部位","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101957_d1123a.jpg","title":"吸脂瘦脸","subtitle":"北京吸脂瘦脸 吸脂名医 全面部手工吸脂精雕细琢 不削骨变V脸","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"26800","price_discount":"23800","price_range_max":"0","_id":"68071","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0303/200_200/jt180303101202_2c9cc2.jpg","title":"吸脂瘦身","subtitle":"北京吸脂瘦后背 美背在于凹凸有致 拯救虎背熊腰","hos_name":"北京丹熙晶都诊所","doc_name":"魏洁","price":"12000","price_discount":"8000","price_range_max":"0","_id":"70403","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"158人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0324/200_200/jt180324155922_a1fef7.jpg","title":"减肥塑形","subtitle":"案例特享10800元 S型美体精雕吸脂术私信送小超声刀体验一次","hos_name":"北京蕾士悦医疗美容诊所","doc_name":"李海兵","price":"19800","price_discount":"10800","price_range_max":"0","_id":"123199","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"满3800减1000,满500减50","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0324/200_200/jt180324140742_eb3ac3.jpg","title":"减肥塑形","subtitle":"水动力吸脂 案例特享价4800元 360°大腿吸脂 私信送小超声刀体验一次","hos_name":"北京蕾士悦医疗美容诊所","doc_name":"李海兵","price":"15800","price_discount":"4800","price_range_max":"0","_id":"123190","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"https://p31.yuemei.com/taobigpromotion/20180416233608_382.png","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"满3800减1000,满500减50","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"1人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0324/200_200/jt180324162136_095874.jpg","title":"减肥塑形","subtitle":"腰腹吸脂 案例特享价9800元 双C小腰精雕术 私信送小超声刀体验一次","hos_name":"北京蕾士悦医疗美容诊所","doc_name":"李海兵","price":"15800","price_discount":"9800","price_range_max":"0","_id":"123205","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"满3800减1000,满500减50","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0324/200_200/jt180324111357_533f53.jpg","title":"减肥塑形","subtitle":"北京水动力吸脂瘦手臂 案例特享2800元 玲珑臂分层吸脂术私信送小超声刀体验一次","hos_name":"北京蕾士悦医疗美容诊所","doc_name":"李海兵","price":"15800","price_discount":"2800","price_range_max":"0","_id":"123184","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"https://p31.yuemei.com/taobigpromotion/20180416233608_382.png","repayment":"最高可享12期分期付款：花呗分期，信用卡分期","hos_red_packet":"满3800减1000,满500减50","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2018/0320/200_200/jt180320104546_a9c0de.jpg","title":"减肥塑形","subtitle":"北京水动力吸脂 单部位水动力吸脂","hos_name":"黛美（北京）医疗美容诊所","doc_name":"程志伟","price":"1999","price_discount":"450","price_range_max":"0","_id":"116725","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"","img66":"https://p31.yuemei.com/taobigpromotion/20180416233608_382.png","repayment":"","hos_red_packet":"满12999减1300,满9999减1000,满6999减700,满4999减500,满1999减200","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"","feeScale":"/次","is_fanxian":"0"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2014/1211/20141211165143428935.jpg","title":"减肥塑形","subtitle":"北京吸脂瘦腰腹 甩腰腹赘肉 紧致小蛮腰","hos_name":"北京东方和谐医疗美容诊所","doc_name":"张准","price":"20000","price_discount":"13900","price_range_max":"0","_id":"1032","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"228人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2016/0714/200_200/jt160713095638_6a9de6.jpg","title":"减肥塑形","subtitle":"北京吸脂瘦后背 定点定量抽取脂肪 肌肤紧致 雕塑完美背部线条 权威专家 愈后痕迹隐藏 皮肤平整","hos_name":"北京东方和谐医疗美容诊所","doc_name":"王自谦","price":"20000","price_discount":"13900","price_range_max":"0","_id":"33003","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"7人预订","feeScale":"/部位","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2016/0718/200_200/jt160714104321_9623f7.jpg","title":"减肥塑形","subtitle":"北京手臂吸脂 采用脂肪移植专利技术 无痕快速 尽显完美玲珑臂","hos_name":"北京东方和谐医疗美容诊所","doc_name":"王自谦","price":"23900","price_discount":"13900","price_range_max":"0","_id":"33297","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"35人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2016/0616/200_200/160616155310_66b8c7.jpg","title":"减肥塑形","subtitle":"北京腰腹吸脂 高清扫描式吸脂技术 吸脂不出血 安全快速 术后平整无痕","hos_name":"北京东方和谐医疗美容诊所","doc_name":"冯斌","price":"30000","price_discount":"20000","price_range_max":"0","_id":"998","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"17人预订","feeScale":"/次","is_fanxian":"1"},{"seckilling":"0","img":"https://p14.yuemei.com/tao/2016/0630/200_200/160630162548_ce145c.jpg","title":"减肥塑形","subtitle":"北京大腿吸脂 逐层逐行吸脂 确保术后平整无痕 权威专家 效果立竿见影 安全保障","hos_name":"北京东方和谐医疗美容诊所","doc_name":"冯斌","price":"30000","price_discount":"20000","price_range_max":"0","_id":"996","showprice":"1","specialPrice":"0","show_hospital":"1","lijian":"0","invitation":"0","baoxian":"免费送50000元 悦美\u2022平安安心整形险","img66":"","repayment":"最高可享12期分期付款：花呗分期","hos_red_packet":"","shixiao":"0","newp":"0","hot":"0","mingyi":"0","rate":"5人预订","feeScale":"/部位","is_fanxian":"1"}]
     * data : []
     * desc :
     * type : 4
     */

    private String total;
    private String desc;
    private String type;
    private List<ListBean> list;
    private List<?> data;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public static class ListBean {
        /**
         * seckilling : 0
         * img : https://p14.yuemei.com/tao/2015/1030/200_200/151030142929_9e44fc.jpg
         * title : 北京面部精细水动力吸脂
         * subtitle : 面颊部、下颌吸脂 没有双下巴 告别婴儿肥 从此爱上自拍
         * hos_name : 北京美雅枫医疗美容医院
         * doc_name : 康虹
         * price : 30000
         * price_discount : 18000
         * price_range_max : 0
         * _id : 20107
         * showprice : 1
         * specialPrice : 0
         * show_hospital : 1
         * lijian : 0
         * invitation : 0
         * baoxian :
         * img66 :
         * repayment : 最高可享12期分期付款：花呗分期
         * hos_red_packet :
         * shixiao : 0
         * newp : 0
         * hot : 0
         * mingyi : 0
         * rate : 2人预订
         * feeScale : /次
         * is_fanxian : 1
         */

        private String seckilling;
        private String img;
        private String title;
        private String subtitle;
        private String hos_name;
        private String doc_name;
        private String price;
        private String price_discount;
        private String price_range_max;
        private String _id;
        private String showprice;
        private String specialPrice;
        private String show_hospital;
        private String lijian;
        private String invitation;
        private String baoxian;
        private String img66;
        private String repayment;
        private String hos_red_packet;
        private String shixiao;
        private String newp;
        private String hot;
        private String mingyi;
        private String rate;
        private String feeScale;
        private String is_fanxian;

        public String getSeckilling() {
            return seckilling;
        }

        public void setSeckilling(String seckilling) {
            this.seckilling = seckilling;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getHos_name() {
            return hos_name;
        }

        public void setHos_name(String hos_name) {
            this.hos_name = hos_name;
        }

        public String getDoc_name() {
            return doc_name;
        }

        public void setDoc_name(String doc_name) {
            this.doc_name = doc_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_discount() {
            return price_discount;
        }

        public void setPrice_discount(String price_discount) {
            this.price_discount = price_discount;
        }

        public String getPrice_range_max() {
            return price_range_max;
        }

        public void setPrice_range_max(String price_range_max) {
            this.price_range_max = price_range_max;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getShowprice() {
            return showprice;
        }

        public void setShowprice(String showprice) {
            this.showprice = showprice;
        }

        public String getSpecialPrice() {
            return specialPrice;
        }

        public void setSpecialPrice(String specialPrice) {
            this.specialPrice = specialPrice;
        }

        public String getShow_hospital() {
            return show_hospital;
        }

        public void setShow_hospital(String show_hospital) {
            this.show_hospital = show_hospital;
        }

        public String getLijian() {
            return lijian;
        }

        public void setLijian(String lijian) {
            this.lijian = lijian;
        }

        public String getInvitation() {
            return invitation;
        }

        public void setInvitation(String invitation) {
            this.invitation = invitation;
        }

        public String getBaoxian() {
            return baoxian;
        }

        public void setBaoxian(String baoxian) {
            this.baoxian = baoxian;
        }

        public String getImg66() {
            return img66;
        }

        public void setImg66(String img66) {
            this.img66 = img66;
        }

        public String getRepayment() {
            return repayment;
        }

        public void setRepayment(String repayment) {
            this.repayment = repayment;
        }

        public String getHos_red_packet() {
            return hos_red_packet;
        }

        public void setHos_red_packet(String hos_red_packet) {
            this.hos_red_packet = hos_red_packet;
        }

        public String getShixiao() {
            return shixiao;
        }

        public void setShixiao(String shixiao) {
            this.shixiao = shixiao;
        }

        public String getNewp() {
            return newp;
        }

        public void setNewp(String newp) {
            this.newp = newp;
        }

        public String getHot() {
            return hot;
        }

        public void setHot(String hot) {
            this.hot = hot;
        }

        public String getMingyi() {
            return mingyi;
        }

        public void setMingyi(String mingyi) {
            this.mingyi = mingyi;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getFeeScale() {
            return feeScale;
        }

        public void setFeeScale(String feeScale) {
            this.feeScale = feeScale;
        }

        public String getIs_fanxian() {
            return is_fanxian;
        }

        public void setIs_fanxian(String is_fanxian) {
            this.is_fanxian = is_fanxian;
        }
    }
}
