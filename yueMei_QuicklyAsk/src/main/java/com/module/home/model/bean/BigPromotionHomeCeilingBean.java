package com.module.home.model.bean;

/**
 * 文 件 名: BigPromotionHomeCeilingBean
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-07 22:50
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class BigPromotionHomeCeilingBean {
        /**
         * bmsid : 507458
         * metro_type : 0
         * qid : 8584
         * url : https://m.yuemei.com/tao_zt/8584.html
         * flag : 0
         * img : https://p11.yuemei.com/tag/1580809651d1a69.png
         * img_new : https://p11.yuemei.com/tag/1580809651d1a69.png
         * title : 悦美
         * type : 0
         * metro_line : 0
         * h_w : 0.10133333333333
         * desc :
         * event_params : {"to_page_type":16,"to_page_id":8584}
         */

        private String bmsid;
        private String metro_type;
        private String qid;
        private String url;
        private String flag;
        private String img;
        private String img_new;
        private String title;
        private String type;
        private String metro_line;
        private String h_w;
        private String desc;
        private EventParamsBean event_params;

        public String getBmsid() {
            return bmsid;
        }

        public void setBmsid(String bmsid) {
            this.bmsid = bmsid;
        }

        public String getMetro_type() {
            return metro_type;
        }

        public void setMetro_type(String metro_type) {
            this.metro_type = metro_type;
        }

        public String getQid() {
            return qid;
        }

        public void setQid(String qid) {
            this.qid = qid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getImg_new() {
            return img_new;
        }

        public void setImg_new(String img_new) {
            this.img_new = img_new;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMetro_line() {
            return metro_line;
        }

        public void setMetro_line(String metro_line) {
            this.metro_line = metro_line;
        }

        public String getH_w() {
            return h_w;
        }

        public void setH_w(String h_w) {
            this.h_w = h_w;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public EventParamsBean getEvent_params() {
            return event_params;
        }

        public void setEvent_params(EventParamsBean event_params) {
            this.event_params = event_params;
        }

        public static class EventParamsBean {
            /**
             * to_page_type : 16
             * to_page_id : 8584
             */

            private int to_page_type;
            private int to_page_id;

            public int getTo_page_type() {
                return to_page_type;
            }

            public void setTo_page_type(int to_page_type) {
                this.to_page_type = to_page_type;
            }

            public int getTo_page_id() {
                return to_page_id;
            }

            public void setTo_page_id(int to_page_id) {
                this.to_page_id = to_page_id;
            }
        }


}
