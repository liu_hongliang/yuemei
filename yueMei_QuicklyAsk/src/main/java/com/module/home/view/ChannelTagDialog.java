package com.module.home.view;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupWindow;

import com.module.base.api.BaseCallBackListener;
import com.module.home.controller.adapter.AllProjectLeftAdapter;
import com.module.home.controller.adapter.AllProjectRightAdapter;
import com.module.home.model.api.ChannelAllpartApi;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.MakeTagListData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 频道页面选择dialog
 *
 * @author 裴成浩
 * @data 2019/10/31
 */
public class ChannelTagDialog extends PopupWindow {

    private Activity mContext;
    private View mView;
    private String id;
    private RecyclerView mLeftRecy;
    private RecyclerView mRightRecy;
    private ChannelAllpartApi channelAllpartApi;
    private AllProjectLeftAdapter leftListAdapter;
    private AllProjectRightAdapter rightListAdapter;
    private List<MakeTagData> mDatas;
    private final String TAG = "ChannelTagDialog";
    private int selectedIndex = 0;      //当前项下标


    public ChannelTagDialog(Activity context, View view, String id) {
        this.mContext = context;
        this.mView = view;
        this.id = id;
        channelAllpartApi = new ChannelAllpartApi();
        initView();
    }

    private void initView() {
        final View view = View.inflate(mContext, R.layout.dialog_all_project, null);
        view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));
        mLeftRecy = view.findViewById(R.id.all_project_left_rl);
        mRightRecy = view.findViewById(R.id.all_project_right_lv);

        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        //7.1以下系统高度直接设置，7.1及7.1以上的系统需要动态设置
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N_MR1) {
            Log.e(TAG, "7.1以下系统");
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        }

        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(false);
        setOutsideTouchable(false);
        setContentView(view);
        update();

        getTagData();
    }


    /**
     * 展开
     */
    public void showPop() {
        int[] location = new int[2];
        mView.getLocationOnScreen(location);
        int rawY = location[1];                                     //当前组件到屏幕顶端的距离

        //根据不同版本显示
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
            // 适配 android 7.0
            showAtLocation(mView, Gravity.NO_GRAVITY, 0, rawY + mView.getHeight());
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                //7.1及以上系统高度动态设置
                setHeight(Utils.getScreenSize(mContext)[1] - rawY - mView.getHeight());
            }
            showAsDropDown(mView);
        }

        setRightView(selectedIndex);
    }

    /**
     * 获取标签的个数
     */
    private void getTagData() {
        Map<String, Object> keyValues = new HashMap<>();
        channelAllpartApi.getCallBack(mContext, keyValues, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {

                // 设置默认选中的
                if (!TextUtils.isEmpty(id) && !"0".equals(id)) {
                    boolean haveSelected = false;
                    for (int i = 0; i < serverData.size(); i++) {
                        MakeTagData makeTagData = serverData.get(i);
                        if (id.equals(makeTagData.getId())) {
                            selectedIndex = i;
                            haveSelected = true;
                            serverData.get(i).setSelected(true);
                            break;
                        }
                    }

                    if (!haveSelected) {
                        serverData.get(0).setSelected(true);
                    }

                } else {
                    serverData.get(0).setSelected(true);
                }

                mDatas = serverData;
                setLeftView();
                setRightView(selectedIndex);
            }
        });
    }

    /**
     * 设置左边listView的数据
     */
    private void setLeftView() {
        //设置布局管理器
        mLeftRecy.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if (mLeftRecy.getItemAnimator() != null) {
            ((DefaultItemAnimator) mLeftRecy.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果
        }

        //设置适配器
        leftListAdapter = new AllProjectLeftAdapter(mContext, mDatas);
        mLeftRecy.setAdapter(leftListAdapter);

        // 设置点击某条的监听
        leftListAdapter.setOnItemClickListener(new AllProjectLeftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                setRightView(pos);
            }
        });

    }

    /**
     * 设置右边listView的数据
     *
     * @param pos
     */
    private void setRightView(int pos) {
        selectedIndex = pos;
        List<MakeTagListData> list = mDatas.get(pos).getList();

        if (rightListAdapter == null) {
            //设置布局管理器
            mRightRecy.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            //设置适配器
            rightListAdapter = new AllProjectRightAdapter(mContext, list,mDatas.get(pos).getId());
            mRightRecy.setAdapter(rightListAdapter);
            rightListAdapter.setItemClickListener(new AllProjectRightAdapter.ItemClickListener() {
                @Override
                public void onItemClick(String level, String id, String name, String selected_id, HashMap<String, String> params) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(level, id, name,params);
                    }
                }
            });
        } else {
            rightListAdapter.setData(list,mDatas.get(pos).getId());
        }
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    private ItemClickListener mItemClickListener;

    public interface ItemClickListener {
        void onItemClick(String level, String id, String name,HashMap<String,String> params);
    }
}
