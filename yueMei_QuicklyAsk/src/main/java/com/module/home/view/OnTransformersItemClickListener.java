package com.module.home.view;

/**
 * @Author: Zaaach
 * @Date: 2019/11/22
 * @Email: zaaach@aliyun.com
 * @Description:
 */
public interface OnTransformersItemClickListener {
    void onItemClick(int position);
}
