package com.module.home.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.TextView;
import com.module.base.view.FunctionManager;
import com.module.home.model.bean.TaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class ScrollerViewpager extends ViewPager {

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 100002)
                setCurrentItem(getCurrentItem() + 1);
        }
    };

    //滚动的页面
    List<TaoData> taodata;

    //滚动的页面数量
    private int count;

    //自动滚动信号
    private final int SCROLL = 100002;

    //创建一个滚动线程
    private Thread thread;

    //间隔时间
    private int SPACE;

    private List<List<TaoData>> Tao;

    private FunctionManager mFunctionManager;

    //目前线程状态
    private boolean NORMAL = true;
    private Context context;
    private boolean isInit = false;

    public ScrollerViewpager(@NonNull Context context) {
        super(context);
    }

    public ScrollerViewpager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 功能：初始化
     *
     * @param taodata  视图组
     * @param space    间隔时间
     * @param listener 监听事件
     */
    public void init(Context context, List<TaoData> taodata, final int space, List<List<TaoData>> mTao, final OnViewpagerChangeListener listener) {
        this.context = context;
        this.taodata = taodata;
        this.count = taodata.size();
        this.SPACE = space;
        this.Tao = mTao;
        mFunctionManager = new FunctionManager(context);

        // 最重要的设置，将viewpager翻转
        setPageTransformer(true, new VerticalPageTransformer());
        // 设置去掉滑到最左或最右时的滑动效果
        setOverScrollMode(OVER_SCROLL_NEVER);

        setAdapter(new MyAdapter());

        if (!isInit) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            NORMAL = true;
                            Thread.sleep(SPACE * 1000);
                            handler.sendEmptyMessage(SCROLL);
                        } catch (InterruptedException e) {
                            try {
                                NORMAL = false;
                                Thread.sleep(Integer.MAX_VALUE);
                            } catch (InterruptedException e1) {
                            }
                        }
                    }
                }
            });
            thread.start();
            isInit = true;
        }
        addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                listener.onChange(position % count);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if ((!NORMAL && state != 1) || (NORMAL && state == 1)) {
                    thread.interrupt();
                }
            }
        });
    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            View view = LayoutInflater.from(context).inflate(R.layout.home_viewpager_item, null, false);
            ImageView iv_sku1 = view.findViewById(R.id.iv_sku1);
            ImageView iv_sku2 = view.findViewById(R.id.iv_sku2);
            TextView tv_title1 = view.findViewById(R.id.tv_title1);
            TextView tv_title2 = view.findViewById(R.id.tv_title2);
            TextView tv_price1 = view.findViewById(R.id.tv_price1);
            TextView tv_price2 = view.findViewById(R.id.tv_price2);
            LinearLayout ll1 = view.findViewById(R.id.ll1);
            LinearLayout ll2 = view.findViewById(R.id.ll2);

            //向上取整 防止返回数据奇数项
            int temp = (int) (position % Math.ceil(count / 2));
            final List<TaoData> list_tao = Tao.get(temp);


            if (TextUtils.isEmpty(list_tao.get(0).getImg())) {
                mFunctionManager.setRoundImageSrc(iv_sku1, R.drawable.sall_null_2x, Utils.dip2px(0));
            } else {
                mFunctionManager.setRoundImageSrc(iv_sku1, list_tao.get(0).getImg(), Utils.dip2px(0));
            }
            if (TextUtils.isEmpty(list_tao.get(0).getAlltitle())) {
                tv_title1.setText("");
            } else {
                tv_title1.setText(list_tao.get(0).getAlltitle());
            }
            if (TextUtils.isEmpty(list_tao.get(0).getPay_price())) {
                tv_price1.setText("");
            } else {
                tv_price1.setText("¥" + list_tao.get(0).getPay_price());
            }
            ll1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    WebUrlTypeUtil.getInstance(context).urlToApp(list_tao.get(0).getUrl());
                }
            });

            if (list_tao.size() == 2) {
                if (TextUtils.isEmpty(list_tao.get(1).getImg())) {
                    mFunctionManager.setRoundImageSrc(iv_sku2, R.drawable.sall_null_2x, Utils.dip2px(0));
                } else {
                    mFunctionManager.setRoundImageSrc(iv_sku2, list_tao.get(1).getImg(), Utils.dip2px(0));
                }
                if (TextUtils.isEmpty(list_tao.get(1).getAlltitle())) {
                    tv_title2.setText("");
                } else {
                    tv_title2.setText(list_tao.get(1).getAlltitle());
                }
                if (TextUtils.isEmpty(list_tao.get(1).getPay_price())) {
                    tv_price2.setText("");
                } else {
                    tv_price2.setText("¥" + list_tao.get(1).getPay_price());
                }

                ll2.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WebUrlTypeUtil.getInstance(context).urlToApp(list_tao.get(1).getUrl());
                    }
                });
            }else {
                ll2.setVisibility(INVISIBLE);
            }
//            if (view.getParent() == container) {
//                container.removeView(view);
//            }
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }
    }

    /**
     * 拆分集合
     *
     * @param <T>
     * @param resList
     * @param count   每个集合的元素个数
     * @return返回拆分后的各个集合
     **/
    public static <T> List<List<T>> split(List<T> resList, int count) {
        if (resList == null || count < 1)
            return null;
        List<List<T>> ret = new ArrayList<List<T>>();
        int size = resList.size();
        if (size <= count) {
            // 数据量不足count指定的大小
            ret.add(resList);
        } else {
            int pre = size / count;
            int last = size % count;
            // 前面pre个集合，每个大小都是count个元素
            for (int i = 0; i < pre; i++) {
                List<T> itemList = new ArrayList<T>();
                for (int j = 0; j < count; j++) {
                    itemList.add(resList.get(i * count + j));
                }
                ret.add(itemList);
            }
            // last的进行处理
            if (last > 0) {
                List<T> itemList = new ArrayList<T>();
                for (int i = 0; i < last; i++) {
                    itemList.add(resList.get(pre * count + i));
                }
                ret.add(itemList);
            }
        }
        return ret;
    }


    private class VerticalPageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View view, float position) {

            if (position < -1) { // [-Infinity,-1)
                // 当前页的上一页
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                view.setAlpha(1);

                // 抵消默认幻灯片过渡
                view.setTranslationX(view.getWidth() * -position);

                //设置从上滑动到Y位置
                float yPosition = position * view.getHeight();
                view.setTranslationY(yPosition);

            } else { // (1,+Infinity]
                // 当前页的下一页
                view.setAlpha(0);
            }
        }
    }


    /**
     * 交换触摸事件的X和Y坐标
     */
    private MotionEvent swapXY(MotionEvent ev) {
        float width = getWidth();
        float height = getHeight();

        float newX = (ev.getY() / height) * width;
        float newY = (ev.getX() / width) * height;

        ev.setLocation(newX, newY);

        return ev;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercepted = super.onInterceptTouchEvent(swapXY(ev));
        swapXY(ev);
        return intercepted; //为所有子视图返回触摸的原始坐标
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(swapXY(ev));
    }

    //通知父控件不拦截当前控件区域事件
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.dispatchTouchEvent(ev);
    }

    public static class ViewPagerScroller extends Scroller {
        private int mDuration = 1500;/*default duration time*/

        /**
         * Set custom duration time.
         * @param duration duration
         */
        public void setScrollDuration(int duration){
            mDuration = duration;
        }

        /**
         * Get duration time.
         * @return duration
         */
        public int getmDuration() {
            return mDuration;
        }

        public ViewPagerScroller(Context context) {
            super(context);
        }

        public ViewPagerScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public ViewPagerScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy,mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            //此处必须重写，网上有些资料里只重写了上面那个，不知道他们的是怎么工作的，我实际测试时行不通的。
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        public void initViewPagerScroll(ViewPager pager){
            try{
                Field field = ViewPager.class.getDeclaredField("mScroller");
                field.setAccessible(true);
                field.set(pager,this);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}






