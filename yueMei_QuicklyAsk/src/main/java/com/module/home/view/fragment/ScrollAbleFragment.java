package com.module.home.view.fragment;/**
 * Created by cpoopc on 2015/8/28.
 */

import android.support.v4.app.Fragment;

import com.cpoopc.scrollablelayoutlib.ScrollableHelper;

/**
 * User: 裴成浩
 * Date: 2017-08-02
 * Time: 11:45
 * Ver.: 6.2.3
 */
public abstract class ScrollAbleFragment extends Fragment implements ScrollableHelper.ScrollableContainer {
}
