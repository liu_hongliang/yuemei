package com.module.home.view;

import android.app.Activity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/7/23
 */
public class SearchInitFlowLayout {
    private final String TAG = "SearchInitFlowLayout";
    private Activity mContext;
    private FlowLayout mFlowLayout;
    private List<String> mSearchWordes;

    public SearchInitFlowLayout(Activity context, FlowLayout flowLayout, List<String> searchWordes) {
        this.mContext = context;
        this.mFlowLayout = flowLayout;
        this.mSearchWordes = searchWordes;
        initView();
    }

    /**
     * 初始化样式
     */
    private void initView() {
        mFlowLayout.removeAllViews();
        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(Utils.dip2px(10), 0, Utils.dip2px(10), 0);
        for (int i = 0; i < mSearchWordes.size(); i++) {

            TextView view = new TextView(mContext);
            view.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            view.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
            view.setText(mSearchWordes.get(i));
            view.setBackgroundResource(R.drawable.shape_bian_yuanjiao_f6f6f6);

            mFlowLayout.addView(view, i, lp);

            view.setTag(i);
            view.setOnClickListener(new onClickView());
        }
    }

    /**
     * 点击按钮回调
     */
    class onClickView implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int selected = (int) v.getTag();
            for (int i = 0; i < mSearchWordes.size(); i++) {
                if (i == selected) {
                    if (clickCallBack != null) {
                        clickCallBack.onClick(v, i, mSearchWordes.get(i));
                    }
                }
            }
        }
    }

    private ClickCallBack clickCallBack;

    public interface ClickCallBack {
        void onClick(View v, int pos, String key);
    }

    public void setClickCallBack(ClickCallBack clickCallBack) {
        this.clickCallBack = clickCallBack;
    }

}
