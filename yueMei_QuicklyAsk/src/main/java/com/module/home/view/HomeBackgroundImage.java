package com.module.home.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.quicklyask.util.Utils;

/**
 * @author 裴成浩
 * @data 2019/10/22
 */
public class HomeBackgroundImage extends AppCompatImageView {

    private Paint mPaint;
    private Path mPath;
    private boolean isDrawImg = true;

    public HomeBackgroundImage(Context context) {
        this(context, null);
    }

    public HomeBackgroundImage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeBackgroundImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true); // 抗锯齿
        mPaint.setColor(Color.WHITE);
        mPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (isDrawImg) {
            mPath.moveTo(0, getHeight() - Utils.dip2px(30));
            mPath.quadTo(getWidth() / 2, getHeight(), getWidth(), getHeight() - Utils.dip2px(30));
            mPath.lineTo(getWidth(), getHeight());
            mPath.lineTo(0, getHeight());
            mPath.close();//设置曲线闭合

            canvas.drawPath(mPath, mPaint);
        }
    }

    public void setDrawImg(boolean drawImg) {
        isDrawImg = drawImg;
        invalidate();
    }
}
