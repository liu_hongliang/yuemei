package com.module.home.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nineoldandroids.animation.ValueAnimator;
import com.quicklyask.activity.R;

/**
 * 文 件 名: BottomSheetLayout
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-11 21:59
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class BottomSheetLayout extends FrameLayout {
    public static final String TAG = "BottomSheetLayout";
    private ValueAnimator valueAnimator;
    private int collapsedHeight = 0;
    private float progress = 0f;
    private boolean startsCollapsed;
    private float scrollTranslationY = 0f;
    private float userTranslationY = 0f;
    private boolean isScrollingUp;
    private View.OnClickListener clickListener;
    private long animationDuration = 300L;
    private OnProgressListener progressListener;
    private TouchToDragListener touchToDragListener = new TouchToDragListener(true);


    public long getAnimationDuration() {
        return animationDuration;
    }

    public void setAnimationDuration(long animationDuration) {
        this.animationDuration = animationDuration;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public boolean isExpended() {
        return progress == 1.0f;
    }


    @Override
    public void setTranslationY(float translationY) {
        userTranslationY = translationY;
//        Log.e(TAG,"userTranslationY =="+userTranslationY);
        super.setTranslationY(scrollTranslationY + userTranslationY);
    }

    public void setProgressListener(OnProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public BottomSheetLayout(@NonNull Context context) {
        this(context, null);
    }

    public BottomSheetLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BottomSheetLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);

    }

    private void initView(AttributeSet attrs) {
        TypedArray a = this.getContext().obtainStyledAttributes(attrs, R.styleable.BottomSheetLayout);
        int collapsedHeight = a.getDimensionPixelSize(R.styleable.BottomSheetLayout_collapsedHeight, 0);
        setCollapsedHeight(collapsedHeight);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.setMinimumHeight(Math.max(this.getMinimumHeight(), collapsedHeight));
        }
        a.recycle();
        valueAnimator = ValueAnimator.ofFloat(0f, 1f);

        setOnTouchListener(touchToDragListener);
        if (getHeight() == 0) {
            addOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    removeOnLayoutChangeListener(this);
                    animate(0f);
                }
            });

        } else {
            animate(0f);
        }

    }

    public final void setCollapsedHeight(int height) {
        collapsedHeight = height;
        if (Build.VERSION.SDK_INT >= 16) {
            setMinimumHeight(Math.max(getMinimumHeight(), collapsedHeight));
        }

    }


    public final void toggle() {
        if (valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }

        long duration;
        if (progress > 0.5f) {
            duration = (long) (animationDuration * progress);
            valueAnimator = ValueAnimator.ofFloat(progress, 0f);
        } else {
            duration = (long) (animationDuration * (1 - progress));
            valueAnimator = ValueAnimator.ofFloat(progress, 1f);
        }

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float progress = (float) animation.getAnimatedValue();
                animate(progress);
            }
        });
        valueAnimator.setDuration(duration);

        valueAnimator.start();
    }

    public void collapse() {
        if (valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }

        valueAnimator = ValueAnimator.ofFloat(progress, 0f);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float progress = (float) animation.getAnimatedValue();
                animate(progress);
            }
        });
        valueAnimator.setDuration((long) (animationDuration * progress));

        valueAnimator.start();
    }


    public void expand() {
        if (valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }

        valueAnimator = ValueAnimator.ofFloat(progress, 1f);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float progress = (float) animation.getAnimatedValue();
                animate(progress);
            }
        });
        valueAnimator.setDuration((long) (animationDuration * (1 - progress)));

        valueAnimator.start();
    }

    private void animate(float progress) {
        this.progress = progress;
        int height = getHeight();
        int distance = height - collapsedHeight;
//        Log.e(TAG,"distance =="+distance);
        scrollTranslationY = (float) distance * (1 - progress);
//        Log.e(TAG,"scrollTranslationY =="+scrollTranslationY);
        super.setTranslationY(scrollTranslationY + userTranslationY);
        if (progressListener != null) {
            progressListener.onProgress(progress);
        }
    }

    private void animateScroll(float firstPos, float touchPos) {
        float distance = touchPos - firstPos;
//        Log.e(TAG,"firstPos =="+firstPos);
//        Log.e(TAG,"touchPos =="+touchPos);
        int height = getHeight();
        int totalDistance = height - collapsedHeight;
        if (!startsCollapsed) {
            isScrollingUp = false;
            progress = Math.max(0f, (float) 1 - distance / totalDistance);
        } else {
            isScrollingUp = true;
            progress = Math.min(1f, -distance / (float) totalDistance);
        }
        progress = Math.max(0f, Math.min(1f, progress));
//        Log.e(TAG,"progress =="+progress);
        animate(progress);
    }


    public void animateScrollEnd() {
        if (valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }

        long duration;
        float progressLimit = isScrollingUp ? 0.2f : 0.8f;
        if (progress > progressLimit) {
            duration = (long) (animationDuration * (1 - progress));
            valueAnimator = ValueAnimator.ofFloat(progress, 1f);
        } else {
            duration = (long) (animationDuration * progress);
            valueAnimator = ValueAnimator.ofFloat(progress, 0f);
        }

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float progress = (float) animation.getAnimatedValue();
                animate(progress);
            }
        });
        valueAnimator.setDuration(duration);

        valueAnimator.start();
    }


//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        return touchToDragListener.onTouch(this, ev);
//    }

    private boolean performChildClick(float eventX, float eventY) {
        return performChildClick(eventX, eventY, this, 0);
    }

    private boolean performChildClick(float eventX, float eventY, ViewGroup viewGroup, int nest) {

        for (int i = viewGroup.getChildCount() - 1; i >= 0; i--) {
            View view = viewGroup.getChildAt(i);
            if (isViewAtLocation(eventX, eventY, view)) {
                if (view instanceof ViewGroup) {
                    boolean performChildClick = performChildClick(eventX - getLeft(), eventY - getTop(), (ViewGroup) view, nest + 1);
                    if (performChildClick) {
                        return true;
                    }
                }
                if (view.performClick()) {
                    return true;
                }
            }
        }

        return performClick();
    }


    private boolean isViewAtLocation(float rawX, float rawY, View view) {
        if (view.getLeft() <= rawX && view.getRight() >= rawX) {
            if (view.getTop() <= rawY && view.getBottom() >= rawY) {
                return true;
            }
        }
        return false;
    }

    private class TouchToDragListener implements OnTouchListener {
        private int CLICK_ACTION_THRESHOLD = 200;
        private float startX;
        private float startY;
        private double startTime;
        private boolean touchToDrag;

        public boolean onTouch(View v, MotionEvent ev) {

            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    if (ev.getPointerCount() == 1) {
                        startX = ev.getRawX();
                        startY = ev.getRawY();
                        startTime = (double) System.currentTimeMillis();
                        startsCollapsed = progress < 0.5;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    float endX = ev.getRawX();
                    float endY = ev.getRawY();
                    if (isAClick(startX, endX, startY, endY, System.currentTimeMillis())) {
                        if (performChildClick(ev.getX(), ev.getY())) {
                            return true;
                        }

                        if (touchToDrag && clickListener != null) {
                            onClick();
                            return true;
                        }
                    }
                    animateScrollEnd();
                    break;
                case MotionEvent.ACTION_MOVE:
                    animateScroll(startY, ev.getRawY());
                    invalidate();
                    break;
            }

            return true;
        }

        private boolean isAClick(float startX, float endX, float startY, float endY, long endTime) {
            float differenceX = Math.abs(startX - endX);
            float differenceY = Math.abs(startY - endY);
            double differenceTime = Math.abs(startTime - (double) endTime);
            return differenceX <= (float) CLICK_ACTION_THRESHOLD && differenceY <= (float) CLICK_ACTION_THRESHOLD && differenceTime <= (double) 400;
        }

        public TouchToDragListener(boolean touchToDrag) {
            this.touchToDrag = touchToDrag;
        }
    }

    private void onClick() {
        if (clickListener != null) {
            clickListener.onClick(this);
        }
    }


    public interface OnProgressListener {
        void onProgress(float var1);
    }
}