package com.module.home.view.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.model.bean.BBsListData550;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.activity.HomeActivity;
import com.module.home.controller.adapter.HomeFragmetDiaryAdapter;
import com.module.home.controller.adapter.HomeStaggeredAdapter;
import com.module.home.model.api.HomeRefeshApi;
import com.module.home.model.api.LoadDataApi;
import com.module.home.model.bean.FeedSkuRecommend;
import com.module.home.model.bean.FeedSkuRecommendData;
import com.module.home.model.bean.ShareListNewData;
import com.module.home.model.bean.TuijHosDoc;
import com.module.home.model.bean.TuijOther;
import com.module.home.model.bean.Tuijshare;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import org.apache.commons.lang.StringUtils;
import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import simplecache.ACache;

/**
 * Created by 裴成浩 on 2019/3/15
 */
public class HomeFragment extends YMBaseFragment {
    @BindView(R.id.home_fragment_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.home_fragment_recycler)
    RecyclerView mRecyclerView;

    private String partId;

    public int mCurPage = 1;                //页数
    private LoadDataApi mLoadDataApi;
    private ArrayList<Tuijshare> mTuiDatas;
    private HomeFragmetDiaryAdapter mHomeFragmentAdapter;

    private final String TAG = "HomeFragment";
    private int mTempPos = -1;
    private HomeStaggeredAdapter homeStaggeredAdapter;
    private String showType = "";
    private boolean firstLoad = false;
    private boolean isFirstAddDecoration = true;
    //日记推荐列表
    private BaseNetWorkCallBackApi getfeedskurecommendApi;
    //记录点击后插入的数据
    private List<String> recommendList = new ArrayList<>();
    //临时记录当前点击条目 tag_ids;
    private List<FeedSkuRecommend> temporaryList = new ArrayList<>();
    private StaggeredGridLayoutManager staggeredGridLayoutManager;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        boolean change = isVisibleToUser != getUserVisibleHint();
        super.setUserVisibleHint(isVisibleToUser);
        if (isResumed() && change) {
            if (getUserVisibleHint()) {
                onVisible();
            } else {
                onInvisible();
            }
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            onInvisible();
        } else {
            onVisible();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !isHidden()) {
            onVisible();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !isHidden()) {
            onInvisible();
        }
    }

    private void onVisible() {
        if (temporaryList != null && temporaryList.size() != 0) {
            List<String> tempList1 = new ArrayList<>();
            tempList1.addAll(temporaryList.get(0).getSecond_parts());
            if (recommendList != null && recommendList.size() != 0) {
                List<String> tempList2 = new ArrayList<>();
                tempList2.addAll(recommendList);
                tempList2.retainAll(tempList1);
                if (tempList2.size() == 0) {
                    //没交集
                    getFeedSkuRecommendData(temporaryList.get(0).getPost_id(), Utils.getUid(mContext), StringUtils.strip(recommendList.toString(), "[]").trim());
                }
            } else {
                getFeedSkuRecommendData(temporaryList.get(0).getPost_id(), Utils.getUid(mContext), StringUtils.strip(recommendList.toString(), "[]").trim());
            }
        }
    }

    private void onInvisible() {

    }


    @Override
    protected int getLayoutId() {
        return R.layout.home_fragment_view;
    }

    public static HomeFragment newInstance(String partId) {
        HomeFragment listFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("partId", partId);
        listFragment.setArguments(bundle);
        return listFragment;
    }

    public static HomeFragment newInstance(String partId, ArrayList<Tuijshare> mDatas, String showType) {
        HomeFragment listFragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("partId", partId);
        bundle.putParcelableArrayList("data", mDatas);
        bundle.putString("showType", showType);
        listFragment.setArguments(bundle);
        return listFragment;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            partId = getArguments().getString("partId");
            mTuiDatas = getArguments().getParcelableArrayList("data");
            showType = getArguments().getString("showType");
        }

        //加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mCurPage = mCurPage + 1;
                loadData();
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                mOnRecyclerViewStateCallBack.recyclerViewStateCallBack(recyclerView, newState);
                int[] first = new int[2];
                staggeredGridLayoutManager.findFirstCompletelyVisibleItemPositions(first);
                if (newState == RecyclerView.SCROLL_STATE_IDLE && (first[0] == 1 || first[1] == 1)) {
                    staggeredGridLayoutManager.invalidateSpanAssignments();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        getfeedskurecommendApi = new BaseNetWorkCallBackApi(FinalConstant1.FORUM, "getfeedskurecommend");
    }

    @Override
    protected void initData(View view) {
        if (!firstLoad) {
            mLoadDataApi = new LoadDataApi(mContext);
            isCheckNet();
            firstLoad = true;
        }
    }

    /**
     * 判断是否有网络
     */
    private void isCheckNet() {
        if (SystemTool.checkNet(mContext)) {       //有网络
            loadData();
        } else {
            String homejson = ACache.get(mContext).getAsString("home_diarylist_json" + partId);
            ShareListNewData shareListNewData = JSONUtil.TransformSingleBean(homejson, ShareListNewData.class);
            List<BBsListData550> datas = shareListNewData.getList();
            dataToview((ArrayList<BBsListData550>) datas);
        }
    }

    /**
     * 联网请求数据
     */
    private void loadData() {
        mLoadDataApi.addData("partId", partId);
        mLoadDataApi.addData("page", mCurPage + "");
        mLoadDataApi.getCallBack(getActivity(), mLoadDataApi.getLoadDataHashMap(), new BaseCallBackListener<ServerData>() {

            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    ACache.get(mContext).put("home_diarylist_json" + partId, serverData.data, 60 * 60 * 24);
                    ShareListNewData shareListNewData = JSONUtil.TransformSingleBean(serverData.data, ShareListNewData.class);
                    if (TextUtils.isEmpty(showType)) {
                        showType = shareListNewData.getShowType();
                    }
                    List<BBsListData550> datas = shareListNewData.getList();
                    if (datas.size() == 0) {
                        mRefresh.finishLoadMoreWithNoMoreData();
                    } else {
                        mRefresh.finishLoadMore();
                    }
                    dataToview((ArrayList<BBsListData550>) datas);
                }
            }
        });
    }

    /**
     * 初始化联网请求数据
     */
    private void dataToview(ArrayList<BBsListData550> datas) {
        if ("2".equals(showType)) { //正常模板
            if (mHomeFragmentAdapter == null) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                RecyclerView.ItemAnimator itemAnimator = mRecyclerView.getItemAnimator();
                //取消局部刷新动画效果
                if (null != itemAnimator) {
                    ((DefaultItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
                }
                mRecyclerView.setLayoutManager(linearLayoutManager);
                if (mTuiDatas != null) {
                    mHomeFragmentAdapter = new HomeFragmetDiaryAdapter(mContext, mTuiDatas, null, partId);
                } else {
                    mHomeFragmentAdapter = new HomeFragmetDiaryAdapter(mContext, datas, partId);
                }
                mRecyclerView.setAdapter(mHomeFragmentAdapter);


                mHomeFragmentAdapter.setOnEventClickListener(new HomeFragmetDiaryAdapter.OnEventClickListener() {
                    @Override
                    public void onDiaryItemClick(View view, int position) {
                        if (Utils.isFastDoubleClick()) {    //快速点击
                            return;
                        }

                        mTempPos = position;
                        Log.e(TAG, "1111mTempPos == " + mTempPos);

                        Tuijshare tuijshare = mHomeFragmentAdapter.getData().get(position);
                        BBsListData550 post = tuijshare.getPost();
                        String url = post.getUrl();
                        String qid = post.getQ_id();
                        String appmurl = post.getAppmurl();
                        Log.e(TAG, "url111 === " + url);
                        Log.e(TAG, "appmurl === " + appmurl);
                        Log.e(TAG, "qid111 === " + qid);
                        //日记列表
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, post.getBmsid(), "1"), post.getEvent_params());
                        //百度统计
                        StatService.onEvent(getActivity(), "066", (position + 1) + "", 1);

                        WebUrlTypeUtil.getInstance(getContext()).urlToApp(appmurl, "1", "0");
                    }

                    @Override
                    public void onSkuItemClick(int position, String tao_id) {
                        Tuijshare tuijshare = mHomeFragmentAdapter.getData().get(position);
                        BBsListData550 post = tuijshare.getPost();
                        HashMap<String, String> event_params = post.getEvent_params();
                        event_params.put("id", tao_id);
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHARETAO, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, post.getBmsid(), "1"), event_params, new ActivityTypeData("1"));
                        Intent intent = new Intent(mContext, TaoDetailActivity.class);
                        intent.putExtra("id", tao_id);
                        intent.putExtra("source", "0");
                        intent.putExtra("objid", "0");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onHosDocClick(View view, int pos) {
                        HashMap<String, String> event_params = mHomeFragmentAdapter.getData().get(pos).getHos_doc().getEvent_params();
                        event_params.put("id", "obj_id");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHAREHOSDOC, "home|share_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, "obj_type", "1"), event_params, new ActivityTypeData("1"));
                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(mHomeFragmentAdapter.getData().get(pos).getHos_doc().getUrl(), "1", "0");
                    }

                    @Override
                    public void onHosDocItemClick(View view, int pos, int itemPos) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }

                        List<HomeTaoData> hosTaos = mHomeFragmentAdapter.getData().get(pos).getHos_doc().getTao();
                        String _id = hosTaos.get(itemPos).get_id();

                        //百度统计
                        StatService.onEvent(getActivity(), "066", (pos + 1) + "-" + itemPos, 1);

                        Intent it2 = new Intent(mContext, TaoDetailActivity.class);
                        it2.putExtra("id", _id);
                        it2.putExtra("source", "1");
                        it2.putExtra("objid", "0");
                        startActivity(it2);
                    }

                    @Override
                    public void onItemMoreClick(int pos, int itemPos) {
                        TuijHosDoc hosDoc = mHomeFragmentAdapter.getData().get(pos).getHos_doc();

                        //百度统计
                        StatService.onEvent(getActivity(), "066", (pos + 1) + "-" + itemPos, 1);

                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(hosDoc.getUrl(), "1", "0");
                    }

                    @Override
                    public void onItemPersonClick(String id, int pos) {
                        BBsListData550 post = mHomeFragmentAdapter.getData().get(pos).getPost();
                        HashMap<String, String> event_params = post.getUserClickData().getEvent_params();
                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(post.getUser_center_url());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_SHARELIST_USER_CLICK, (pos + 1) + "", "0"), event_params, new ActivityTypeData("49"));
                    }

                    @Override
                    public void onOtherItemClick(View view, int pos) {
                        String url = mHomeFragmentAdapter.getData().get(pos).getOther().getUrl();
                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(url, "1", "0");
                    }
                });

            } else {
                mHomeFragmentAdapter.addData(datas);
            }
        } else { //双侧流模板
            if (null == homeStaggeredAdapter) {
                staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
                staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                mRecyclerView.setItemAnimator(null);
                if (mTuiDatas != null) {
                    homeStaggeredAdapter = new HomeStaggeredAdapter(mContext, mTuiDatas, null);
                } else {
                    homeStaggeredAdapter = new HomeStaggeredAdapter(mContext, datas);
                }
                mRecyclerView.setLayoutManager(staggeredGridLayoutManager);
                mRecyclerView.setAdapter(homeStaggeredAdapter);
                homeStaggeredAdapter.setOnItemClickListener(new HomeStaggeredAdapter.OnItemClickListener() {
                    @Override
                    public void onPostItemListener(View view, int position, boolean isOther) {
                        if (Utils.isFastDoubleClick()) {    //快速点击
                            return;
                        }
                        Tuijshare tuijshare = homeStaggeredAdapter.getData().get(position);

                        StaggeredGridLayoutManager.LayoutParams params
                                = (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
                        if (showType.equals("1")
                                && tuijshare.getPost() != null
                                && !TextUtils.isEmpty(tuijshare.getPost().getAskorshare())
                                && tuijshare.getPost().getAskorshare().equals("1")) {
                            FeedSkuRecommend feedSkuRecommend = new FeedSkuRecommend();
                            //判断是否是推荐tab && 类型是日记本
                            if (params.getSpanIndex() % 2 == 0) {
                                Log.i("302", "左列" + position);
                                feedSkuRecommend.setInsertPos(position + 3);
                            } else {
                                Log.i("302", "右列" + position);
                                feedSkuRecommend.setInsertPos(position + 1);

                            }
                            feedSkuRecommend.setSecond_parts(tuijshare.getPost().getSecond_parts());
                            feedSkuRecommend.setPost_id(tuijshare.getPost().get_id());

                            temporaryList.clear();
                            temporaryList.add(feedSkuRecommend);
                        }

                        if (isOther) {
                            TuijOther other = tuijshare.getOther();
                            String url = other.getUrl();
                            WebUrlTypeUtil.getInstance(getContext()).urlToApp(url, "1", "0");
                            HashMap<String, String> showControl = tuijshare.getShow_control();
                            if (showControl != null) {
                                HashMap<String, Object> hashMap = new HashMap<String, Object>(showControl);
                                long timeMillis = System.currentTimeMillis();
                                hashMap.put("trigger_time", (timeMillis / 1000) + "");
                                upLoadParms(hashMap);
                            }
                        } else {
                            BBsListData550 post = tuijshare.getPost();
                            String url = post.getUrl();
                            String qid = post.getQ_id();
                            String appmurl = post.getAppmurl();
                            //日记列表
                            if (mContext instanceof HomeActivity) {
                                Log.e(TAG, "post -> post.getEvent_params() === " + post.getEvent_params());
                                HashMap<String, String> showControl = tuijshare.getShow_control();
                                if (showControl != null) {
                                    HashMap<String, Object> hashMap = new HashMap<String, Object>(showControl);
                                    long timeMillis = System.currentTimeMillis();
                                    hashMap.put("trigger_time", (timeMillis / 1000) + "");
                                    upLoadParms(hashMap);
                                }
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HTMLHOME_TO_POST, (position + 1) + "", post.getBmsid(), "1"), post.getEvent_params());
                            } else {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, post.getBmsid(), "1"), post.getEvent_params());
                            }
                            //百度统计
                            StatService.onEvent(getActivity(), "066", (position + 1) + "", 1);
                            if (!TextUtils.isEmpty(appmurl)) {
                                WebUrlTypeUtil.getInstance(getContext()).urlToApp(appmurl, "1", "0");
                            }
                        }
                    }

                    @Override
                    public void onPostSkuListener(View view, BBsListData550 data, int position) {
                        if (Utils.isFastDoubleClick()) {    //快速点击
                            return;
                        }

                        String tao_id = data.getTao().getId();
                        HashMap<String, String> event_params = data.getEvent_params();
                        event_params.put("id", tao_id);

                        if (mContext instanceof HomeActivity) {
                            Log.e(TAG, "sku -> event_params === " + event_params);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HTMLHOME_TO_TAO, (position + 1) + "", data.getBmsid(), "1"), event_params, new ActivityTypeData("1"));
                        } else {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHARETAO, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, data.getBmsid(), "1"), event_params, new ActivityTypeData("1"));
                        }

                        Intent intent = new Intent(mContext, TaoDetailActivity.class);
                        intent.putExtra("id", tao_id);
                        intent.putExtra("source", "0");
                        intent.putExtra("objid", "0");
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onHosDocItemListener(View view, int pos) {
                        if (Utils.isFastDoubleClick()) {    //快速点击
                            return;
                        }
                        Tuijshare tuijshare = homeStaggeredAdapter.getData().get(pos);
                        TuijHosDoc hos_doc = tuijshare.getHos_doc();
                        HashMap<String, String> event_params = hos_doc.getEvent_params();

                        if (mContext instanceof HomeActivity) {
                            Log.e(TAG, "hos_doc -> event_params === " + event_params);
                            HashMap<String, String> showControl = tuijshare.getShow_control();
                            if (showControl != null) {
                                HashMap<String, Object> hashMap = new HashMap<String, Object>(showControl);
                                long timeMillis = System.currentTimeMillis();
                                hashMap.put("trigger_time", (timeMillis / 1000) + "");
                                upLoadParms(hashMap);
                            }
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HTMLHOME_TO_DOCTOR, (pos + 1) + ""), event_params, new ActivityTypeData("1"));
                        } else {
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHAREHOSDOC, "home|share_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, "obj_type", "1"), event_params, new ActivityTypeData("1"));
                        }

                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(hos_doc.getUrl(), "1", "0");
                    }

                    @Override
                    public void onFeedSkuRecommendItemListener(View view, int pos) {

                    }
                });
                if (isFirstAddDecoration) {
                    isFirstAddDecoration = false;
                    mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                        @Override
                        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                            super.getItemOffsets(outRect, view, parent, state);
                            //判断左右列，方法2（推荐）
                            StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());
                            if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                                //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                                if (params.getSpanIndex() % 2 == 0) {
                                    //左列
                                    outRect.right = Utils.dip2px((int) 2.5);
                                } else {
                                    //右列
                                    outRect.left = Utils.dip2px((int) 2.5);
                                }
                            }
                        }
                    });
                }

            } else {
                homeStaggeredAdapter.addData(datas);
            }
        }
    }


    /**
     * 上传参数
     *
     * @param hashMap
     */
    private void upLoadParms(HashMap<String, Object> hashMap) {
        new HomeRefeshApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "upLoadParms onSuccess");
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            Log.e(TAG, "2222mTempPos == " + mTempPos);
            if (mTempPos >= 0) {
                isFocu(mHomeFragmentAdapter.getData().get(mTempPos).getPost().getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(getActivity(), hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                if (mTempPos >= 0) {
                    mHomeFragmentAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                    mTempPos = -1;
                }
            }
        });
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public void setmOnRecyclerViewStateCallBack(OnRecyclerViewStateCallBack mOnRecyclerViewStateCallBack) {
        this.mOnRecyclerViewStateCallBack = mOnRecyclerViewStateCallBack;
    }

    OnRecyclerViewStateCallBack mOnRecyclerViewStateCallBack;

    public interface OnRecyclerViewStateCallBack {
        void recyclerViewStateCallBack(RecyclerView recyclerView, int newState);
    }

    /**
     * 获取点击日记流推荐列表
     * post_id  日记本id
     * uid      登陆用户id
     * tag_ids  二级标签id  字段是second_parts
     */
    private void getFeedSkuRecommendData(String post_id, String uid, String tag_ids) {
        getfeedskurecommendApi.getHashMap().clear();
        getfeedskurecommendApi.addData("post_id", post_id);
        if (Utils.isLogin()) {
            getfeedskurecommendApi.addData("uid", uid);
        }
        if (!TextUtils.isEmpty(tag_ids)) {
            getfeedskurecommendApi.addData("tag_ids", tag_ids.replaceAll(" ", ""));
        }
        getfeedskurecommendApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    ArrayList<FeedSkuRecommendData> feedSkuRecommendData = JSONUtil.jsonToArrayList(data.data, FeedSkuRecommendData.class);

                    if (feedSkuRecommendData != null && feedSkuRecommendData.size() != 0) {
                        final Tuijshare tuijshare = new Tuijshare();
                        tuijshare.setFeedSkuRecommendData(feedSkuRecommendData);

                        mRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //插入数据
                                homeStaggeredAdapter.addItem(temporaryList.get(0).getInsertPos(), tuijshare);

                                recommendList.addAll(temporaryList.get(0).getSecond_parts());
                                temporaryList.clear();
                            }
                        }, 500);
                    }
                }
            }
        });
    }
}
