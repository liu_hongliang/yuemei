package com.module.home.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.HuangDeng1;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

/**
 * 首页上边metro位
 * Created by 裴成浩 on 2019/9/18
 */
public class MetroTopListView extends LinearLayout {
    private String TAG = "MetroTopListView";
    private final Context mContext;
    private int windowsWight = 0;

    public MetroTopListView(Context context) {
        this(context, null);
    }

    public MetroTopListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MetroTopListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        if (mContext instanceof Activity) {
            windowsWight = Utils.getScreenSize((Activity) mContext)[0];
        }
    }

    public void initView(final List<List<HuangDeng1>> metroTopData) {
        setOrientation(VERTICAL);

        //metroTop 实体类
        if (metroTopData != null && metroTopData.size() > 0) {
            Log.e(TAG, "metroTopData == " + metroTopData.size());
            //先清空
            setVisibility(View.VISIBLE);
            removeAllViews();
            //添加其他item
            for (int i = 0; i < metroTopData.size(); i++) {
                View childView = null;
                //内部是否有分割线
                boolean lineHave = true;

                switch (metroTopData.get(i).size()) {
                    case 1:                                                 //一排一个的(高)
                        childView = View.inflate(mContext, R.layout.metro_all_one, null);
                        ImageView merroTopOne = childView.findViewById(R.id.merro_top_one_high_img);

                        //设置宽高比
                        ViewGroup.LayoutParams paramsOne = merroTopOne.getLayoutParams();
                        paramsOne.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        float h_w = Float.parseFloat(metroTopData.get(i).get(0).getH_w());
                        paramsOne.height = (int) (windowsWight * h_w);
                        merroTopOne.setLayoutParams(paramsOne);

                        Log.e(TAG, "top----> " + metroTopData.get(i).get(0).getImg());
                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(merroTopOne);

                        //判断是否有分割线
                        lineHave = "1".equals(metroTopData.get(i).get(0).getMetro_line());

                        final int metroTopPos1 = i;
                        merroTopOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                baiduTongJi("062", metroTopPos1 + "-" + 1);
                                if (metroTopData.size() > 0) {
                                    HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos1).get(0);
                                    Log.d(TAG, "=========>" + huangDeng1.getUrl());
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                                    YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                }
                            }
                        });

                        break;
                    case 2:                //一排两个的
                        childView = View.inflate(mContext, R.layout.metro_all_two, null);
                        ImageView merroTopTwo1 = childView.findViewById(R.id.merro_two_img1);
                        ImageView merroTopTwo2 = childView.findViewById(R.id.merro_two_img2);
                        View merroTowDivder = childView.findViewById(R.id.merro_top_two_divider);

                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopTwo1);

                        Glide.with(mContext).load(metroTopData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopTwo2);

                        //判断是否有分割线
                        if ("1".equals(metroTopData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroTowDivder.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroTowDivder.setVisibility(View.GONE);
                        }

                        final int metroTopPos2 = i;
                        merroTopTwo1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos2).get(0);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos2 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });

                        merroTopTwo2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos2).get(1);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos2 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });


                        break;
                    case 4:               //一排四个的
                        childView = View.inflate(mContext, R.layout.metro_all_four, null);
                        ImageView merroTopFour1 = childView.findViewById(R.id.merro_four_img1);
                        ImageView merroTopFour2 = childView.findViewById(R.id.merro_four_img2);
                        ImageView merroTopFour3 = childView.findViewById(R.id.merro_four_img3);
                        ImageView merroTopFour4 = childView.findViewById(R.id.merro_four_img4);
                        View merroFourDivder1 = childView.findViewById(R.id.merro_top_four_divider1);
                        View merroFourDivder2 = childView.findViewById(R.id.merro_top_four_divider2);
                        View merroFourDivder3 = childView.findViewById(R.id.merro_top_four_divider3);

                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour1);
                        Glide.with(mContext).load(metroTopData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour2);
                        Glide.with(mContext).load(metroTopData.get(i).get(2).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour3);
                        Glide.with(mContext).load(metroTopData.get(i).get(3).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour4);

                        //判断是否有分割线
                        if ("1".equals(metroTopData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroFourDivder1.setVisibility(View.VISIBLE);
                            merroFourDivder2.setVisibility(View.VISIBLE);
                            merroFourDivder3.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroFourDivder1.setVisibility(View.GONE);
                            merroFourDivder2.setVisibility(View.GONE);
                            merroFourDivder3.setVisibility(View.GONE);
                        }

                        final int metroTopPos4 = i;
                        merroTopFour1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos4).get(0);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });

                        merroTopFour2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos4).get(1);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });
                        merroTopFour3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 = metroTopData.get(metroTopPos4).get(2);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 3);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });

                        merroTopFour4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HuangDeng1 huangDeng1 =metroTopData.get(metroTopPos4).get(3);
                                YmStatistics.getInstance().tongjiApp(huangDeng1.getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 4);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(huangDeng1.getUrl(), "1", "0");
                            }
                        });

                        break;
                }

                if (childView != null) {
                    addView(childView);
                    if (lineHave && i != metroTopData.size() - 1) {   //有分割线(而且不是最后一个)

                        //添加头部的分割线
                        View view = new View(mContext);
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(1));
                        view.setBackgroundColor(Utils.setCustomColor("#efefef"));
                        addView(view, params);
                    }
                }
            }
        } else {
            removeAllViews();
            setVisibility(View.GONE);
        }
    }


    /**
     * 百度统计
     *
     * @param eventId:事件ID,注意要先在mtj.baidu.com中注册此事件ID(产品注册)
     * @param label:自定义事件标签
     */
    private void baiduTongJi(String eventId, String label) {
        StatService.onEvent(mContext, eventId, label, 1);
    }
}
