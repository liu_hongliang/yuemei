package com.module.home.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * 可以禁止滑动的自定义viewPager （频道页用）
 * Created by 裴成浩 on 2019/3/8
 */
public class NoScrollViewPager extends ViewPager {
    private List<Boolean> isCanScrolls = new ArrayList<>();
    private final String TAG = "NoScrollViewPager";
    private int mSelectedPosition = 0;

    public NoScrollViewPager(Context context) {
        this(context, null);
    }

    public NoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 设置其是否能滑动换页
     *
     * @param isCanScroll true 拦截 false：不拦截
     */
    public void setScanScroll(boolean isCanScroll) {
        Log.e(TAG, "mSelectedPosition == " + mSelectedPosition);
        Log.e(TAG, "isCanScrolls == " + isCanScrolls.size());
        isCanScrolls.add(mSelectedPosition, isCanScroll);
    }

    /**
     * 设置子控件个数，并生成是否滑动控制集合（在适配器前加）
     *
     * @param size ：子view个数
     */
    public void setChildCount(int size) {
        Log.e(TAG, "getChildCount() == " + getChildCount());
        for (int i = 0; i < size; i++) {
            isCanScrolls.add(false);
        }

        addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e(TAG, "position == " + position);
                mSelectedPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    float startX = 0;
    float startY = 0;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = ev.getX();
                startY = ev.getY();
                Log.e(TAG, "startX == " + startX);
                Log.e(TAG, "startY == " + startY);
                break;
            case MotionEvent.ACTION_MOVE:
                float moveX = ev.getX();
                float moveY = ev.getY();
                if (Math.abs(moveX - startX) > Math.abs(moveY - startY)) {
                    if (isCanScrolls.get(mSelectedPosition)) {
                        Log.e(TAG, "11111");
                        return true;
                    }
                }
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }
}