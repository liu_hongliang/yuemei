package com.module.home.view;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.commonview.view.NiceImageView;
import com.module.home.model.bean.HuangDeng1;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

import static com.module.commonview.adapter.DiscountExpiredAdapter.getCountTimeByLong;
import static com.module.commonview.adapter.DiscountExpiredAdapter.getSecond;


/**
 * 首页大促模块
 * Created by 裴成浩 on 2019/9/18
 */
public class HomeTopPromote extends LinearLayout {
    private final Context mContext;
    private String TAG = "HomeTopPromote";
    private boolean isBottom;
    private List<HuangDeng1> mDatas;
    private List<HuangDeng1> metroBigPromotion;
    private int windowsWight;
    private View bottomView;
    private CountDownTimer countDownTimer;

    public HomeTopPromote(Context context) {
        this(context, null);
    }

    public HomeTopPromote(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeTopPromote(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        setOrientation(VERTICAL);

    }

    public void initView(boolean isBottom, List<HuangDeng1> data, final int windowsWight) {
        this.isBottom = isBottom;
        this.mDatas = data;
        this.windowsWight = windowsWight;
        if (data.size() > 0) {
            //头部图片
            final HuangDeng1 huangDeng0 = data.get(0);
            View headView = View.inflate(mContext, R.layout.head_view_home_top_promote, null);
            final ImageView imageTop = headView.findViewById(R.id.iv_bg);
            LinearLayout ll_time = headView.findViewById(R.id.ll_time);
            final TextView tv_day = headView.findViewById(R.id.tv_day);
            final TextView tv_hours = headView.findViewById(R.id.tv_hours);
            final TextView tv_minute = headView.findViewById(R.id.tv_minute);
            final TextView tv_second = headView.findViewById(R.id.tv_second);
            final NiceImageView tv_day_bg = headView.findViewById(R.id.tv_day_bg);
            final NiceImageView tv_hours_bg = headView.findViewById(R.id.tv_hours_bg);
            final NiceImageView tv_minute_bg = headView.findViewById(R.id.tv_minute_bg);
            final NiceImageView tv_second_bg = headView.findViewById(R.id.tv_second_bg);
            TextView tv_title1 = headView.findViewById(R.id.tv_title1);
            TextView tv_title2 = headView.findViewById(R.id.tv_title2);
            TextView tv_title3 = headView.findViewById(R.id.tv_title3);
            TextView tv_title4 = headView.findViewById(R.id.tv_title4);
            TextView tv_title5 = headView.findViewById(R.id.tv_title5);
            addView(headView);

            if (huangDeng0.getCountdownArr() != null) {
                if (!TextUtils.isEmpty(huangDeng0.getCountdownArr().getTitle())) {
                    tv_title1.setText(huangDeng0.getCountdownArr().getTitle());
                }
                if (!TextUtils.isEmpty(huangDeng0.getCountdownArr().getTitleColor())) {
                    tv_title1.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTitleColor().trim()));
                    tv_title2.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTitleColor().trim()));
                    tv_title3.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTitleColor().trim()));
                    tv_title4.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTitleColor().trim()));
                    tv_title5.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTitleColor().trim()));
                }
                if (!TextUtils.isEmpty(huangDeng0.getCountdownArr().getTimeColor())) {
                    tv_day.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeColor().trim()));
                    tv_hours.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeColor().trim()));
                    tv_minute.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeColor().trim()));
                    tv_second.setTextColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeColor().trim()));
                }
                if (!TextUtils.isEmpty(huangDeng0.getCountdownArr().getTimeBgColor())) {
                    tv_day_bg.setMaskColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeBgColor().trim()));
                    tv_hours_bg.setMaskColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeBgColor().trim()));
                    tv_minute_bg.setMaskColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeBgColor().trim()));
                    tv_second_bg.setMaskColor(Color.parseColor(huangDeng0.getCountdownArr().getTimeBgColor().trim()));
                }
                ll_time.setVisibility(VISIBLE);
                if (!TextUtils.isEmpty(huangDeng0.getCountdownArr().getEnd_time())) {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                    //倒计时逻辑
                    if (huangDeng0.getCountdownArr().getEnd_time().equals("0")) {
                        countDownTimer = new CountDownTimer((24 * 60 * 60 + 4) * 1000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                tv_day.setText(getSecond(getCountTimeByLong(millisUntilFinished), "0"));
                                tv_hours.setText(getSecond(getCountTimeByLong(millisUntilFinished), "1"));
                                tv_minute.setText(getSecond(getCountTimeByLong(millisUntilFinished), "2"));
                                tv_second.setText(getSecond(getCountTimeByLong(millisUntilFinished), "3"));
                            }

                            @Override
                            public void onFinish() {
                                tv_day.setText("00");
                                tv_hours.setText("00");
                                tv_minute.setText("00");
                                tv_second.setText("00");

                            }
                        }.start();
                    }
                    long timer = Long.parseLong(huangDeng0.getCountdownArr().getEnd_time()) * 1000 - System.currentTimeMillis();
                    if (timer > 0) {
                        countDownTimer = new CountDownTimer(timer, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                tv_day.setText(getSecond(getCountTimeByLong(millisUntilFinished), "0"));
                                tv_hours.setText(getSecond(getCountTimeByLong(millisUntilFinished), "1"));
                                tv_minute.setText(getSecond(getCountTimeByLong(millisUntilFinished), "2"));
                                tv_second.setText(getSecond(getCountTimeByLong(millisUntilFinished), "3"));
                            }

                            @Override
                            public void onFinish() {
                                tv_day.setText("00");
                                tv_hours.setText("00");
                                tv_minute.setText("00");
                                tv_second.setText("00");

                            }
                        }.start();
                    } else {
                        tv_day.setText("00");
                        tv_hours.setText("00");
                        tv_minute.setText("00");
                        tv_second.setText("00");
                    }

                } else {
                    tv_day.setText("00");
                    tv_hours.setText("00");
                    tv_minute.setText("00");
                    tv_second.setText("00");
                }
            } else {
                ll_time.setVisibility(GONE);
            }


            Glide.with(mContext).load(huangDeng0.getImg()).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();

                    int imgWidth = windowsWight - ((MarginLayoutParams) getLayoutParams()).leftMargin - ((MarginLayoutParams) getLayoutParams()).rightMargin;
                    int imgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                    ViewGroup.LayoutParams layoutParams = imageTop.getLayoutParams();
                    layoutParams.width = imgWidth;
                    layoutParams.height = imgHeight;
                    imageTop.setLayoutParams(layoutParams);
                    if (resource.isAnimated()) {
                        resource.setLoopCount(GifDrawable.LOOP_FOREVER);
                        resource.start();
                    }
                    imageTop.setImageDrawable(resource);


                    if (mDatas.size() > 1) {
                        setBottomView(imgHeight);
                    } else {
                        if (onEventClickListener != null) {
                            onEventClickListener.onImageLoadingEndClick(imgHeight);
                        }
                    }
                }

            });

            //设置image
            imageTop.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, huangDeng0);
                    }
                }
            });

        }
    }

    //倒计时销毁的方法
    public void cancelTopPromoteTimers() {
        if (countDownTimer == null) {
            return;
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    /**
     * 底部view设置
     *
     * @param topImgHeight ：头部图片高度
     */
    private void setBottomView(final int topImgHeight) {
        if (isBottom && mDatas.size() >= 6) {
            final HuangDeng1 huangDeng1 = mDatas.get(1);

            bottomView = View.inflate(mContext, R.layout.home_top_promote_view, this);
            final ImageView mOne = bottomView.findViewById(R.id.home_top_promote_one);

            Log.e(TAG, "huangDeng1.getImg()  == " + huangDeng1.getImg());
            Glide.with(mContext).load(huangDeng1.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {

                    mOne.setImageDrawable(resource);

                    int intrinsicWidth = resource.getIntrinsicWidth();
                    int intrinsicHeight = resource.getIntrinsicHeight();
                    MarginLayoutParams parentPl = (MarginLayoutParams) getLayoutParams();
                    MarginLayoutParams bottomPl = (MarginLayoutParams) bottomView.getLayoutParams();
                    int imgWidth = (windowsWight - parentPl.leftMargin - parentPl.rightMargin - bottomPl.leftMargin - bottomPl.rightMargin) / 3;
                    int bottomimgHeight = (imgWidth * intrinsicHeight) / intrinsicWidth;

                    if (onEventClickListener != null) {
                        onEventClickListener.onImageLoadingEndClick(topImgHeight + bottomimgHeight);
                    }

                    ViewGroup.LayoutParams layoutParams = getLayoutParams();
                    layoutParams.height = topImgHeight + bottomimgHeight;
                    setLayoutParams(layoutParams);

                    setBottomImage();
                }

            });

            mOne.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, huangDeng1);
                    }
                }
            });

        }
    }

    private void setBottomImage() {

        final HuangDeng1 huangDeng2 = mDatas.get(2);
        final HuangDeng1 huangDeng3 = mDatas.get(3);
        final HuangDeng1 huangDeng4 = mDatas.get(4);
        final HuangDeng1 huangDeng5 = mDatas.get(5);

        ImageView mTwoOne = bottomView.findViewById(R.id.home_top_promote_two_one);
        ImageView mTwoTwo = bottomView.findViewById(R.id.home_top_promote_two_two);
        ImageView mThreeOne = bottomView.findViewById(R.id.home_top_promote_three_one);
        ImageView mThreeTwo = bottomView.findViewById(R.id.home_top_promote_three_two);

        Glide.with(mContext).load(huangDeng2.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mTwoOne);
        mTwoOne.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng2);
                }
            }
        });

        Glide.with(mContext).load(huangDeng3.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mTwoTwo);
        mTwoTwo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng3);
                }
            }
        });

        Glide.with(mContext).load(huangDeng4.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mThreeOne);
        mThreeOne.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng4);
                }
            }
        });

        Glide.with(mContext).load(huangDeng5.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(2))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mThreeTwo);
        mThreeTwo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onItemClick(v, huangDeng5);
                }
            }
        });
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelTopPromoteTimers();
    }

    public interface OnEventClickListener {
        void onItemClick(View v, HuangDeng1 data);                      //item点击回调

        void onImageLoadingEndClick(int imgHeight);                                  //图片加载回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
