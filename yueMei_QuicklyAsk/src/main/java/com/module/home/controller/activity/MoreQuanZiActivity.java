package com.module.home.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.baidu.mobstat.StatService;
import com.module.community.model.bean.HomeCommunityPartsData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.controller.activity.QuanziDetailActivity;
import com.quicklyask.activity.R;
import com.quicklyask.adpter.BBsHeadPartAdapter_;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 热门圈子更多
 * Created by dwb on 16/3/8.
 */
public class MoreQuanZiActivity extends BaseActivity {

    private final String TAG = "MoreQuanZiActivity";

    private ListView groupGV;

    private BBsHeadPartAdapter_ discAdapter;

    private Context mContext;

    private List<HomeCommunityPartsData> lvGroupData = new ArrayList<>();

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_more_quanzi);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = MoreQuanZiActivity.this;

        groupGV = findViewById(R.id.quanzi_grid_view);

        lvGroupData = getIntent().getParcelableArrayListExtra("listobj");


        if (null != lvGroupData && lvGroupData.size() > 0) {

            discAdapter = new BBsHeadPartAdapter_(mContext, lvGroupData);
            groupGV.setAdapter(discAdapter);
            groupGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BBS_HUATICLICK, (pos + 1) + ""));
                    Intent it = new Intent();
                    it.putExtra("url_name", lvGroupData
                            .get(pos).getUrl_name());
                    it.setClass(mContext, QuanziDetailActivity.class);
                    startActivity(it);
                }
            });
        }
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}
