package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import java.util.List;

/**
 * Created by Administrator on 2018/4/26.
 */

public class TaoAdapter426 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    public static final String TAG="TaoAdapter426";
    private List<HomeTaoData> mHotIssues;
    private Context mContext;
    private HomeTaoData hotIsData;
    private OnItemClickListener mItemClickListener;

    public TaoAdapter426(List<HomeTaoData> hotIssues, Context context) {
        mHotIssues = hotIssues;
        mContext = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tao_new, null, false);
        RecyclerView.ViewHolder holder= new TaoAdapter426.ViewHolder1(view);
        view.setOnClickListener(this);
        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        hotIsData = mHotIssues.get(position);
        ViewHolder1 holder1 = (ViewHolder1) holder;
        holder1.itemView.setTag(position);
        if (hotIsData != null && !TextUtils.isEmpty(hotIsData.getTuijianTitle())) {
            if(position == 0){
                holder1.tuijianTitleTop.setText("未能查到相关服务");
            }else{
                holder1.tuijianTitleTop.setText("没有更多了");
            }
            holder1.taoListCentent.setVisibility(View.GONE);
        } else {
            holder1.taoListCentent.setVisibility(View.VISIBLE);

            try {
                Log.e(TAG, "hotIsData.getImg() == " + hotIsData.getImg());

                Glide.with(mContext)
                        .load(hotIsData.getImg())
                        .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(holder1.mHotPic1);

            } catch (OutOfMemoryError e) {
                // ViewInject.toast("内存溢出3");
            }

            holder1.mHotTitleTV.setText("<" + hotIsData.getTitle() + "> " + hotIsData.getSubtitle());

            boolean ifnull = false;

            if (null != hotIsData.getDoc_name()
                    && hotIsData.getDoc_name().length() > 0) {
                holder1.mHotDocNameTV.setText(hotIsData.getDoc_name());
                ifnull = false;
            } else {
                holder1.mHotDocNameTV.setText("");
                ifnull = true;
            }

            if (null != hotIsData.getHos_name()
                    && hotIsData.getHos_name().length() > 0) {
                if (ifnull) {
                    holder1.mHotHosNameTV.setText(hotIsData.getHos_name());
                } else {
                    holder1.mHotHosNameTV.setText("," + hotIsData.getHos_name());
                }
            } else {
                holder1.mHotHosNameTV.setText(" ");
            }

            holder1.mRateTv.setText(hotIsData.getRate());

            if (null != hotIsData.getNewp()) {
                String newp = hotIsData.getNewp();
                if (newp.equals("1")) {
                    holder1.mNewIv.setVisibility(View.VISIBLE);
                } else {
                    holder1.mNewIv.setVisibility(View.GONE);
                }
            }

            if (null != hotIsData.getHot()) {
                String hot = hotIsData.getHot();
                if (hot.equals("1")) {
                    holder1.mHotIv.setVisibility(View.VISIBLE);
                } else {
                    holder1.mHotIv.setVisibility(View.GONE);
                }
            }

            if (null != hotIsData.getMingyi()) {
                String mingyi = hotIsData.getMingyi();
                if (mingyi.equals("1")) {
                    holder1.mMingyiIv.setVisibility(View.VISIBLE);
                    holder1.mMingyiIv.setVisibility(View.GONE);
                } else {
                }
            }

            String baoxian = hotIsData.getBaoxian();
            if (null != baoxian && baoxian.length() > 0) {
                holder1.baoxianLy.setVisibility(View.VISIBLE);
                holder1.baoxianTv.setText(baoxian);
            } else {
                holder1.baoxianLy.setVisibility(View.GONE);
            }

            if (null != hotIsData.getRepayment() && hotIsData.getRepayment().length() > 0) {
                holder1.fenqiLy.setVisibility(View.VISIBLE);
                holder1.fenqiTv.setText(hotIsData.getRepayment());
            } else {
                holder1.fenqiLy.setVisibility(View.GONE);
            }

            String hosRedPacket = hotIsData.getHos_red_packet();
            if (null != hosRedPacket && hosRedPacket.length() > 0) {
                holder1.hongbaoLy.setVisibility(View.VISIBLE);
                holder1.hongbaoTv.setText(hosRedPacket);
            } else {
                holder1.hongbaoLy.setVisibility(View.GONE);
            }


            String ymPrice = hotIsData.getPrice_discount();
            String member_price = hotIsData.getMember_price();
            int i = Integer.parseInt(member_price);
            if (i >= 0){
                holder1.plusVibilisty.setVisibility(View.VISIBLE);
                holder1.plusPrice.setText("¥"+member_price);
                holder1.mHotPriceTV.setText(ymPrice);

            }else {
                holder1.plusVibilisty.setVisibility(View.GONE);
                holder1.mHotPriceTV.setText(ymPrice);
            }

            holder1.mFeescale.setText(hotIsData.getFeeScale());

            if (null != ymPrice && ymPrice.length() > 0 && Integer.parseInt(ymPrice) > 1000) {
                String is_fanxian = hotIsData.getIs_fanxian();
                if("0".equals(is_fanxian)){
                    holder1.mFanxinTv.setVisibility(View.GONE);
                }else{
                    holder1.mFanxinTv.setVisibility(View.VISIBLE);
                }
            } else {
                holder1.mFanxinTv.setVisibility(View.GONE);
            }

            String limit = hotIsData.getSpecialPrice();//限时特价
            if (limit.equals("1")) {
                holder1.mLimitPic.setVisibility(View.VISIBLE);
            } else {
                holder1.mLimitPic.setVisibility(View.GONE);
            }

            if (hotIsData.getShixiao().equals("1")) {
                holder1.mYishouguangIv.setVisibility(View.VISIBLE);
            } else {
                holder1.mYishouguangIv.setVisibility(View.GONE);
            }

            if (null != hotIsData.getSeckilling()) {
                if (hotIsData.getSeckilling().equals("1")) {
                    holder1.mAllmanFqIv.setVisibility(View.VISIBLE);
                } else {
                    holder1.mAllmanFqIv.setVisibility(View.GONE);
                }
            }

            if (null != hotIsData.getInvitation()
                    && hotIsData.getInvitation().equals("1")) {
                holder1.mTeyaoHIv.setVisibility(View.VISIBLE);
                holder1.mTeyaoIv.setVisibility(View.VISIBLE);
            } else {
                holder1.mTeyaoHIv.setVisibility(View.GONE);
                holder1.mTeyaoIv.setVisibility(View.GONE);
            }

            if (null != hotIsData.getLijian() && hotIsData.getLijian().length() > 0) {

                if (hotIsData.getLijian().equals("0")) {
                    holder1.mButieLy.setVisibility(View.GONE);
                } else {

                    if (null != hotIsData.getImg66() && hotIsData.getImg66().length() > 0) {
                        holder1.mButieLy.setVisibility(View.GONE);
                    } else {
                        holder1.mButieLy.setVisibility(View.VISIBLE);
                        holder1.mButieTv.setText(hotIsData.getLijian());
                    }
                }
            }

            if (null != hotIsData.getImg66() && hotIsData.getImg66().length() > 0) {

                holder1.mCu66Iv.setVisibility(View.VISIBLE);

                Glide.with(mContext)
                        .load(hotIsData.getImg66())
                        .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                        .placeholder(R.drawable.home_other_placeholder)
                        .error(R.drawable.home_other_placeholder)
                        .into(holder1.mCu66Iv);

                holder1.mFanxinTv.setVisibility(View.GONE);
                holder1.mMingyiIv.setVisibility(View.GONE);
                holder1.mButieLy.setVisibility(View.GONE);
                holder1.mTeyaoHIv.setVisibility(View.GONE);
                holder1.mTeyaoIv.setVisibility(View.GONE);
                holder1.mLimitPic.setVisibility(View.GONE);
                holder1.mNewIv.setVisibility(View.GONE);
                holder1.mHotIv.setVisibility(View.GONE);

            } else {
                holder1.mCu66Iv.setVisibility(View.GONE);
            }

            int width = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            int height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            holder1.mTaoListJiageLyss.measure(width, height);
            int width1 = holder1.mTaoListJiageLyss.getMeasuredWidth();

            Log.e(TAG, position + "width1 == " + width1);

            //第二件折扣设置
            Log.e(TAG, "hotIsData.getPromotion()的个数 == " + hotIsData.getPromotion().size());
            ViewGroup.LayoutParams layoutParams = holder1.mHotPicll.getLayoutParams();
            if (hotIsData.getPromotion().size() > 0) {
                holder1.mFlowLayout.setVisibility(View.VISIBLE);
                setTagView(holder1.mFlowLayout, hotIsData.getPromotion());
                layoutParams.height = Utils.dip2px(160);
            } else {
                layoutParams.height = Utils.dip2px(138);
                holder1.mFlowLayout.setVisibility(View.GONE);
            }
            holder1.mHotPicll.setLayoutParams(layoutParams);
        }
    }

    @Override
    public int getItemCount() {
        return mHotIssues.size();
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener!=null){
            mItemClickListener.onItemClick((Integer) v.getTag());
        }
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {
        public LinearLayout taoListCentent;
        public TextView tuijianTitleTop;
        public LinearLayout mHotPicll;
        public ImageView mHotPic1;
        public TextView mHotDocNameTV;
        public LinearLayout mTaoListJiageLyss;
        public TextView mHotTitleTV;
        public TextView mHotHosNameTV;
        public TextView mHotPriceTV;
        public TextView mFeescale;
        public ImageView mLimitPic;

        public ImageView mYishouguangIv;
        public TextView mRateTv;
        public ImageView mNewIv;
        public ImageView mHotIv;
        public ImageView mMingyiIv;

        public ImageView mAllmanFqIv;
        public LinearLayout mButieLy;
        public TextView mButieTv;

        public FlowLayout mFlowLayout;

        public TextView mTeyaoHIv;
        public ImageView mTeyaoIv;

        public ImageView mCu66Iv;

        public TextView mFanxinTv;

        public LinearLayout baoxianLy;
        public TextView baoxianTv;

        public LinearLayout fenqiLy;
        public TextView fenqiTv;

        public LinearLayout hongbaoLy;
        public TextView hongbaoTv;
        public LinearLayout plusVibilisty;
        public TextView plusPrice;
        public ViewHolder1(View itemView) {
            super(itemView);
            tuijianTitleTop = itemView.findViewById(R.id.tuijian_title1);

            taoListCentent = itemView.findViewById(R.id.ll_tao_list);

            mHotPicll = itemView.findViewById(R.id.my_collect_tao_im_ll);

            mHotPic1 =  itemView
                    .findViewById(R.id.my_collect_tao_im_iv);
            mHotTitleTV = itemView
                    .findViewById(R.id.my_collect_tao_name_tv);
            mHotHosNameTV = itemView
                    .findViewById(R.id.my_collect_tao_hosname_iv);
            mHotDocNameTV = itemView
                    .findViewById(R.id.my_collect_tao_docname_iv);
            mTaoListJiageLyss = itemView
                    .findViewById(R.id.tao_list_jiage_lyss);

            mHotPriceTV = itemView
                    .findViewById(R.id.my_collect_tao_price_dis_iv);
            mFeescale = itemView.findViewById(R.id.collect_tao_feescale_iv);

            mLimitPic = itemView
                    .findViewById(R.id.my_collect_limit_iv);

            mYishouguangIv = itemView
                    .findViewById(R.id.tao_yishuouguang_iv);
            mRateTv = itemView
                    .findViewById(R.id.tao_list_rate_tv);
            mNewIv = itemView
                    .findViewById(R.id.tao_list_tag_new_iv);
            mHotIv = itemView
                    .findViewById(R.id.tao_list_tag_hot_iv);
            mMingyiIv = itemView
                    .findViewById(R.id.tao_list_tag_mingyi_iv);

            mAllmanFqIv = itemView
                    .findViewById(R.id.tao_list_tag_allman_iv);

            mButieLy = itemView
                    .findViewById(R.id.tao_butie_ly);
            mButieTv = itemView.findViewById(R.id.tao_butie_tv);

            mFlowLayout = itemView.findViewById(R.id.tao_list_tag_allman_flowLayout);

            mTeyaoHIv = itemView
                    .findViewById(R.id.tao_list_teyao_iv);
            mTeyaoIv = itemView
                    .findViewById(R.id.my_teyao_iv);

            mFanxinTv = itemView.findViewById(R.id.tao_list_tag_fanxian_tv);

            mCu66Iv = itemView.findViewById(R.id.tao_list_cu_66);

            baoxianLy = itemView.findViewById(R.id.taolist_isbaoxian_rly);
            baoxianTv = itemView.findViewById(R.id.taolist_isbaoxian_tv);

            fenqiLy = itemView.findViewById(R.id.taolist_isfenqi_rly);
            fenqiTv = itemView.findViewById(R.id.taolist_isfenqi_tv);

            hongbaoLy = itemView.findViewById(R.id.taolist_ishongbao_rly);
            hongbaoTv = itemView.findViewById(R.id.taolist_ishongbao_tv);
            plusVibilisty = itemView.findViewById(R.id.tao_plus_vibility);
            plusPrice = itemView.findViewById(R.id.tao_plus_price);

        }
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<Promotion> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            for (int i = 0; i < lists.size(); i++) {
                Promotion promotion = (Promotion)lists.get(i);
                String styleType = promotion.getStyle_type();//1正常  2最近浏览
                TextView textView = new TextView(mContext);
                textView.setText(promotion.getTitle());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                if ("2".equals(styleType)){
                    textView.setBackgroundResource(R.drawable.sku_list_nearlook);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.ff7c4f));
                }else {
                    textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                }
                mFlowLayout.addView(textView);
            }
        }
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
