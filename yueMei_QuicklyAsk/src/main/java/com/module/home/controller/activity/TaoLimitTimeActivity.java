package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.view.CommonTopBar;
import com.module.home.controller.adapter.HomePatrAdapter;
import com.module.home.controller.adapter.MyPageAdapter2;
import com.module.other.module.api.InitHotCity;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.MViewPager1;
import com.quicklyask.view.PagerSlidingTabStrip;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 限时淘
 * 
 * @author Rubin
 * 
 */
public class TaoLimitTimeActivity extends FragmentActivity {

	private final String TAG = "TaoLimitTimeActivity";

	private List<TaoPopItemData> lvGroupData = new ArrayList<>();

	private PagerSlidingTabStrip tabs;
	private MViewPager1 pager;
	private MyPageAdapter2 adapter;

	public static String[] partNameData;
	public static String[] partIdData;
	private Handler mHandler;
	private Context mContext;

	private CommonTopBar mTop;// 返回

	private String cityId;
	private int cru = 0;

	PartPopupwindows partPop;
	private LinearLayout titleBar;
	HomePatrAdapter homepartAdater;
	private RelativeLayout xialaRly;
	private int windowsWight;
	GridView partlist;

	private String type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.acty_limittime_tao);
		Intent it = getIntent();
		cityId =Cfg.loadStr(mContext,FinalConstant.DWCITY,"");
		type = it.getStringExtra("type");

		mContext = TaoLimitTimeActivity.this;
		windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);

		initView();

	}


	void initView() {
		tabs = findViewById(R.id.tabs2);
		pager = findViewById(R.id.pager2);
		mTop = findViewById(R.id.limittime_tao_top);

		if (type.equals("1")) {
			mTop.setCenterText("限时特价");
		} else {
			mTop.setCenterText("全民疯抢");
		}

		titleBar = findViewById(R.id.title_bar_rly);
		xialaRly = findViewById(R.id.home_xiala_rly);

		mHandler = getHandler();
		initTab();

		xialaRly.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (partPop.isShowing()) {
					partPop.dismiss();
				} else {
					cru = pager.getCurrentItem();

					homepartAdater = new HomePatrAdapter(TaoLimitTimeActivity.this, lvGroupData, cru);

					partlist.setAdapter(homepartAdater);

					partlist.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) {
							cru = arg2;

							pager.setCurrentItem(cru, false);
							partPop.dismiss();
						}
					});
					partPop.showAsDropDown(titleBar, 0, 0);
				}
			}
		});

	}

	/**
	 * 部位下拉选择
	 * 
	 * @author Rubin
	 * 
	 */
	public class PartPopupwindows extends PopupWindow {

		@SuppressWarnings("deprecation")
		public PartPopupwindows(Context mContext, View v) {

			final View view = View.inflate(mContext,
					R.layout.pop_home_xiala_part, null);

			view.startAnimation(AnimationUtils.loadAnimation(mContext,
					R.anim.fade_ins));

			setWidth(LayoutParams.MATCH_PARENT);
			setHeight(LayoutParams.MATCH_PARENT);
			setBackgroundDrawable(new BitmapDrawable());
			setFocusable(true);
			setOutsideTouchable(true);
			setContentView(view);
			update();

			RelativeLayout shangla = view
					.findViewById(R.id.home_xiala_rly1);

			LinearLayout allly = view
					.findViewById(R.id.hotword_ly);
			LayoutParams params1 = allly.getLayoutParams();
			params1.width = windowsWight;
			allly.setLayoutParams(params1);

			shangla.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

			partlist = view.findViewById(R.id.group_grid_list1);
			LayoutParams params0 = partlist.getLayoutParams();
			params0.width = windowsWight;
			partlist.setLayoutParams(params0);

			cru = pager.getCurrentItem();
			homepartAdater = new HomePatrAdapter(TaoLimitTimeActivity.this,
					lvGroupData, cru);
			partlist.setAdapter(homepartAdater);

			partlist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					cru = arg2;
					pager.setCurrentItem(cru, false);
					dismiss();
				}
			});

		}
	}

	void initTab() {
		lodPart1Data();
	}

	void lodPart1Data() {

		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = null;
				new InitHotCity().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
					@Override
					public void onSuccess(ServerData serverData) {
						if ("1".equals(serverData.code)){
							lvGroupData =JSONUtil.jsonToArrayList(serverData.data,TaoPopItemData.class);
						}
					}
				});

				msg = mHandler.obtainMessage(1);
				msg.sendToTarget();

			}
		}).start();
	}

	private Handler getHandler() {
		return new Handler() {
			@SuppressLint("NewApi")
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (null!=lvGroupData &&lvGroupData.size()>0) {

						int num = lvGroupData.size();
						partNameData = new String[num];
						partIdData = new String[num];

						for (int i = 0; i < num; i++) {
							String partNameStr = lvGroupData.get(i).get_id();
							partIdData[i] = partNameStr;

							// if (partNameStr.equals(cityId)) {
							// cru = i;
							// }
						}

						for (int i = 0; i < num; i++) {
							String parIdStr = lvGroupData.get(i).getName();
							partNameData[i] = parIdStr;
							if (parIdStr.equals(cityId)) {
								cru = i;
							}
						}

						adapter = new MyPageAdapter2(
								getSupportFragmentManager(), partNameData,
								partIdData, type);

						pager.setAdapter(adapter);

						final int pageMargin = (int) TypedValue.applyDimension(
								TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
										.getDisplayMetrics());

						pager.setPageMargin(pageMargin);

						tabs.setViewPager(pager);

						pager.setCurrentItem(cru);

						partPop = new PartPopupwindows(mContext, titleBar);

					}
					break;
				}

			}
		};
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}