package com.module.home.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.other.module.bean.MakeTagData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/20
 */
public class AllProjectLeftAdapter extends RecyclerView.Adapter<AllProjectLeftAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<MakeTagData> mData;
    public static final String REFRESH_DATA = "refresh_data";                   //样式刷新
    private String TAG = "AllProjectLeftAdapter";

    public AllProjectLeftAdapter(Context context, List<MakeTagData> data) {
        this.mContext = context;
        this.mData = data;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_all_project_left, parent, false);
        return new ViewHolder(itemView);        //把这个布局传到ViewHolder中
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * 局部刷新
     *
     * @param viewHolder
     * @param position
     * @param payloads
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position, List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(viewHolder, position);
        } else {
            for (Object payload : payloads) {
                switch ((String) payload) {
                    case REFRESH_DATA:
                        //样式刷新
                        setItemStyle(viewHolder, position);
                        break;
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setItemStyle(holder, position);
//        Log.e("AAAAA", "name -- " + mData.get(position).getName() + ",id ---- " + mData.get(position).getId() + ",是否选中----" + mData.get(position).isSelected());
        holder.tvXiangmu.setText(mData.get(position).getName());
    }

    /**
     * 设置选中和未选中的样式
     *
     * @param holder
     * @param position
     */
    private void setItemStyle(ViewHolder holder, int position) {
        if (position == getSelectedPos()) {
            holder.vSelected.setVisibility(View.VISIBLE);
            holder.tvXiangmu.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
            holder.classIfication.setBackgroundColor(Utils.getLocalColor(mContext, R.color.white));
        } else {
            holder.vSelected.setVisibility(View.GONE);
            holder.tvXiangmu.setTextColor(Utils.getLocalColor(mContext, R.color.zz_gray_22));
            holder.classIfication.setBackgroundColor(Utils.getLocalColor(mContext, R.color.subscribe_item_drag_bg));
        }
    }

    /**
     * 刷新选中和未选中的样式
     *
     * @param pos1
     * @param pos2
     */
    private void setRefresh(int pos1, int pos2) {
        notifyItemChanged(pos1, REFRESH_DATA);
        notifyItemChanged(pos2, REFRESH_DATA);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvXiangmu;
        private LinearLayout classIfication;
        private View vSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            vSelected = itemView.findViewById(R.id.all_project_selected);
            tvXiangmu = itemView.findViewById(R.id.all_project_xiangmu);
            classIfication = itemView.findViewById(R.id.all_project_ification);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int selectedPos = getSelectedPos();
                    if (selectedPos != getLayoutPosition()) {
                        mData.get(selectedPos).setSelected(false);
                        mData.get(getLayoutPosition()).setSelected(true);
                        setRefresh(selectedPos, getLayoutPosition());

                        //刷新右侧数据
                        if (onItemClickListener != null) {
                            onItemClickListener.onItemClick(v, getLayoutPosition());
                        }
                    }
                }
            });

        }
    }

    /**
     * 获取当前选中的下标
     *
     * @return
     */
    public int getSelectedPos() {
        for (int i = 0; i < mData.size(); i++) {
            if (mData.get(i).isSelected()) {
                return i;
            }
        }
        return 0;
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}