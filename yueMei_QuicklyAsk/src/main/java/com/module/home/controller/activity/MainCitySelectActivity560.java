package com.module.home.controller.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mobstat.StatService;
import com.cn.demo.pinyin.AssortView.OnTouchAssortListener;
import com.cn.demo.pinyin.AssortView2;
import com.cn.demo.pinyin.PinyinAdapter591;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.Listener.OnCitySelectListener;
import com.module.commonview.module.api.InitLoadCityApi;
import com.module.commonview.utils.StatisticalManage;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.TabDocAndHosListActivity;
import com.module.doctor.controller.adapter.HotCityAdapter2;
import com.module.doctor.controller.adapter.LocationCityAdapter;
import com.module.doctor.controller.adapter.SurroundingCityAdapter;
import com.module.doctor.model.api.HotCityApi;
import com.module.doctor.model.api.SurroundingCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.doctor.model.bean.MainCityData;
import com.module.doctor.model.bean.RecentVisitCityData;
import com.module.event.CitySearchEvent;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.other.RequestAndResultCode;
import com.module.taodetail.controller.activity.TaoActivity;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Location;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyGridView;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.database.KJDB;
import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.ui.activity.BaseActivity;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * 标题栏城市选择
 *
 * @author Robin
 */
public class MainCitySelectActivity560 extends BaseActivity {

    private final String TAG = "MainCitySelect";

    private Context mContext;
    private ExpandableListView eListView;
    private AssortView2 assortView;
    private PinyinAdapter591 pinAdapter;

    private View headView;
    private String city = "";
    private String cityLocation;
    private RelativeLayout back;

    private MyGridView hotGridlist;
    private HotCityAdapter2 hotcityAdapter;

    //周边城市整体布局
    private LinearLayout ll_surrounding_city;
    //周边城市列表
    private MyGridView gv_periphery_city;
    //定位和最近访问
    private GridView gv_location;
    private TextView tv_city_location;
    private LinearLayout ll_location_button;
    private LinearLayout ll_sucess;
    private LinearLayout ll_fail;

    private TextView title;
    private TextView cityLocTv;
    private TextView tv_looking;


    private String type;
    private String isShowWholeCountry;

    private List<CityDocDataitem> hotcityList;

    private List<MainCityData> citylist = new ArrayList<>();

    // 城市缓存
    private KJDB kjdb;
    private List<MainCityData> citylistCache = new ArrayList<>();
    private RelativeLayout cityAntuo;

    private ImageView iv_close;
    private LinearLayout ll_search;

    private LocationClient locationClient;
    private String provinceLocation;
    private List<RecentVisitCityData> recentVisitCity;
    private LocationCityAdapter locationCityAdapter;

    private static OnCitySelectListener mOnCitySelectListener;

    @Override
    public void setRootView() {
        setContentView(R.layout.acty_city_select_doc);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(CitySearchEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 1:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mContext = MainCitySelectActivity560.this;
        cityLocation = Cfg.loadStr(mContext, FinalConstant.LOCATING_CITY, "失败");
        initView();
        kjdb = KJDB.create(mContext, "yuemeicity");
        Intent it = getIntent();
        type = it.getStringExtra("type");
        isShowWholeCountry = it.getStringExtra("isShowWholeCountry");

        title.setText("当前所选-" + Utils.getCity());

        initViewData();
        cityLocTv.setText(cityLocation);
    }


    void initView() {
        eListView = findViewById(R.id.elist1);
        assortView = findViewById(R.id.assort1);
        back = findViewById(R.id.doc_list_city_back);
        title = findViewById(R.id.doc_list_city_title_tv);

        //EditText相关
        iv_close = findViewById(R.id.iv_close);
        ll_search = findViewById(R.id.ll_search);
        iv_close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCitySelectListener != null) {
                    mOnCitySelectListener.onCitySelectResult("");
                    setOnCitySelectListener(null);
                }
                finish();
            }
        });
        ll_search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CitySearchActivity.invoke(mContext, type);
            }
        });
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mOnCitySelectListener != null) {
                    mOnCitySelectListener.onCitySelectResult("");
                    setOnCitySelectListener(null);
                }
                finish();
            }
        });

    }

    void initViewData() {

        LayoutInflater mInflater = getLayoutInflater();
        headView = mInflater.inflate(R.layout.main_city_select_head_714, null);
        eListView.addHeaderView(headView);
//        eListView.showPinnedHeaderView();
        hotGridlist = headView.findViewById(R.id.group_grid_list1);
        ll_surrounding_city = headView.findViewById(R.id.ll_surrounding_city);
        gv_periphery_city = headView.findViewById(R.id.gv_periphery_city);
        gv_location = headView.findViewById(R.id.gv_location);
        tv_city_location = headView.findViewById(R.id.tv_city_location);
        ll_location_button = headView.findViewById(R.id.ll_location_button);
        ll_sucess = headView.findViewById(R.id.ll_sucess);

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) ll_sucess.getLayoutParams();
        linearParams.width = (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3;
        ll_sucess.setLayoutParams(linearParams);

        ll_fail = headView.findViewById(R.id.ll_fail);
        tv_looking = headView.findViewById(R.id.tv_looking);
        tv_looking.setText(Utils.getCity());
//        setLocationCity();
        ll_location_button.setOnClickListener(new OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_RECENT),hashMap, new ActivityTypeData("1"));
                if (ll_fail.getVisibility() == View.VISIBLE) {
                    if (lacksPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        //未开启
                        final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "请允许悦美获取您当前位置，以帮您查询附近优惠项目", "取消", "确定");
                        yueMeiDialog.setCanceledOnTouchOutside(false);
                        yueMeiDialog.show();
                        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                            @Override
                            public void leftBtnClick() {
                                yueMeiDialog.dismiss();
                            }

                            @Override
                            public void rightBtnClick() {
                                yueMeiDialog.dismiss();
                                JumpSetting();
                            }
                        });
                    } else {
                        //已开启
                        initLocation();
                    }
//                    //版本判断
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        Acp.getInstance(MainCitySelectActivity560.this).request(new AcpOptions.Builder()
//                                        .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)
//                                        .build(),
//                                new AcpListener() {
//                                    @Override
//                                    public void onGranted() {
//                                        if (!"失败".equals(cityLocation)) {
////                                            locatingCity();
//                                            initLocation();
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onDenied(List<String> permissions) {
//                                    }
//                                });
//                    } else {
//
//                        if (null != cityLocation && cityLocation.length() > 0) {
////                            locatingCity();
//                            initLocation();
//                        } else {
//                            ViewInject.toast("正在获取...");
//                        }
//
//                    }
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DWCITY, cityLocation);
                    Utils.getCityOneToHttp(mContext, "1");
                    cityStatistics();
                    if (mOnCitySelectListener != null) {
                        mOnCitySelectListener.onCitySelectResult("1");
                        setOnCitySelectListener(null);
                    }
                    startFinish();
                }
            }
        });
        // 比较当前时间
        int cur1 = (int) System.currentTimeMillis();
        int cur2 = Cfg.loadInt(mContext, "timeCity", 0);
        if ((cur1 - cur2) >= (3600 * 1000 * 24 * 7)) {
            initloadCity();
        } else {
            citylistCache = kjdb.findAll(MainCityData.class);

            if (null != citylistCache && citylistCache.size() > 0) {

                pinAdapter = new PinyinAdapter591(mContext, citylistCache);
                eListView.setAdapter(pinAdapter);

                // 展开所有
                for (int i = 0, length = pinAdapter.getGroupCount(); i < length; i++) {
                    eListView.expandGroup(i);
                }

                // 字母按键回调
                assortView.setOnTouchAssortListener(new OnTouchAssortListener() {

                    View layoutView = LayoutInflater.from(mContext)
                            .inflate(R.layout.alert_dialog_menu_layout,
                                    null);

                    RelativeLayout alRly = (RelativeLayout) layoutView
                            .findViewById(R.id.pop_city_rly);

                    PopupWindow popupWindow;

                    @Override
                    public void onTouchAssortListener(String str) {
                        if (str.equals("定位") || str.equals("热门") || str.equals("周边")) {
//                            eListView.smoothScrollToPosition(0);//滚动
                            eListView.setSelection(0);
                        }
//                        if(str.equals("定位")){
//                            eListView.smoothScrollBy(DensityUtil.dip2px(98),200);
//                        }
                        int index = pinAdapter.getAssort()
                                .getHashList().indexOfKey(str);
                        if (index != -1) {
                            eListView.setSelectedGroup(index);
                        }
                        if (popupWindow != null) {
                            // text.setText(str);
                        } else {
                            popupWindow = new PopupWindow(layoutView,
                                    80, 80, false);
                            popupWindow.showAtLocation(alRly,
                                    Gravity.CENTER, 0, 0);
                        }
                    }

                    @Override
                    public void onTouchAssortUP() {
                        if (popupWindow != null)
                            popupWindow.dismiss();
                        popupWindow = null;
                    }
                });
                //非十二城点击
                eListView.setOnChildClickListener(new OnChildClickListener() {

                    @Override
                    public boolean onChildClick(ExpandableListView arg0,
                                                View arg1, int groupPosition, int childPosition,
                                                long arg4) {
                        int tolal = 0;

                        for (int j = 0; j < groupPosition; j++) {
                            int chidsize1 = pinAdapter.getChildrenCount(j);
                            tolal = tolal + chidsize1;
                        }

                        city = PinyinAdapter591.assortAA.getHashList()
                                .getValueIndex(groupPosition, childPosition);
                        String id = citylistCache.get(childPosition).get_id();
                        HashMap<String,String> hashMap = new HashMap<>();
                        hashMap.put("event_name",FinalEventName.CITY_LIST_CLICK);
                        hashMap.put("id",id);
                        YmStatistics.getInstance().tongjiApp(hashMap);

                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                        Utils.getCityOneToHttp(mContext, "1");
                        if (mOnCitySelectListener != null) {
                            mOnCitySelectListener.onCitySelectResult("1");
                            setOnCitySelectListener(null);
                        }
                        startFinish();
                        Log.e(TAG, "选择的城市是 === " + city);

                        cityStatistics();

                        return true;
                    }
                });
            } else {
                initloadCity();
            }
        }

        loadHotCity();
        loadSurroundingCity();


        //全国
        LinearLayout cityAll = headView.findViewById(R.id.city_all_doc_rly);
        if (TextUtils.isEmpty(isShowWholeCountry) || isShowWholeCountry.equals("1")) {
            cityAll.setVisibility(View.VISIBLE);
        } else {
            cityAll.setVisibility(View.GONE);
        }
        cityAll.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_ENTIRE_COUNTRY),hashMap,new ActivityTypeData("1"));
                city = "全国";
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                Utils.getCityOneToHttp(mContext, "1");

                cityStatistics();

                startFinish();
            }
        });

        cityLocTv = headView.findViewById(R.id.doc_city_select_tv);
        cityAntuo = headView.findViewById(R.id.city_auto_loaction_rly);
        cityAntuo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //版本判断
                if (Build.VERSION.SDK_INT >= 23) {

                    Acp.getInstance(MainCitySelectActivity560.this).request(new AcpOptions.Builder()
                                    .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION)
                                    .build(),
                            new AcpListener() {
                                @Override
                                public void onGranted() {
                                    cityLocTv.setText(cityLocation);

                                    if (!"失败".equals(cityLocation)) {

                                        locatingCity();
                                    }
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                }
                            });
                } else {
                    if (null != cityLocation && cityLocation.length() > 0) {
                        locatingCity();
                    } else {
                        ViewInject.toast("正在获取...");
                    }

                }
            }
        });

    }

    private void setLocationCity() {
        Cfg.saveStr(mContext, "location_city", provinceLocation);
        recentVisitCity = JSONUtil.jsonToArrayList(Cfg.loadStr(mContext, "recent_visit_city", ""), RecentVisitCityData.class);
        if (recentVisitCity.size() < 3) {
            recentVisitCity = recentVisitCity.subList(0, recentVisitCity.size());
        } else {
            recentVisitCity = recentVisitCity.subList(recentVisitCity.size() - 3, recentVisitCity.size());
        }
        Cfg.saveStr(mContext, "recent_visit_city", new Gson().toJson(recentVisitCity));
        //倒序
        Collections.reverse(recentVisitCity);
        if (ll_fail.getVisibility() == View.VISIBLE) {
            gv_location.setNumColumns(1);
            locationCityAdapter = new LocationCityAdapter(mContext, recentVisitCity, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3, "1",provinceLocation);
        } else {
            gv_location.setNumColumns(2);
            locationCityAdapter = new LocationCityAdapter(mContext, recentVisitCity, (DensityUtil.getScreenWidth() - DensityUtil.dip2px(64)) / 3, "0",provinceLocation);
        }
        gv_location.setAdapter(locationCityAdapter);

        gv_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("id","0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_RECENT),hashMap, new ActivityTypeData("1"));

                Cfg.saveStr(mContext, FinalConstant.DWCITY, recentVisitCity.get(position).getName());
                Utils.getCityOneToHttp(mContext, "1");
                cityStatistics();
                if (mOnCitySelectListener != null) {
                    if (!recentVisitCity.get(position).getName().equals("全国")) {
                        mOnCitySelectListener.onCitySelectResult("1");
                        setOnCitySelectListener(null);
                    } else {
                        MyToast.makeTextToast2(mContext, "请选择一个城市", 1000).show();
                        return;
                    }

                }
                startFinish();
            }
        });
    }

    private void loadSurroundingCity() {
        SurroundingCityApi surroundingCityApi = new SurroundingCityApi();
        surroundingCityApi.getHashMap().clear();
        surroundingCityApi.addData("city", Cfg.loadStr(mContext, FinalConstant.DWCITY, ""));
        surroundingCityApi.getCallBack(mContext, surroundingCityApi.getHashMap(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(final List<CityDocDataitem> cityDocDataitem) {
                if (cityDocDataitem != null && cityDocDataitem.size() > 0) {
                    ll_surrounding_city.setVisibility(View.VISIBLE);
                    SurroundingCityAdapter surroundingCityAdapter = new SurroundingCityAdapter(mContext,
                            cityDocDataitem);
                    gv_periphery_city.setAdapter(surroundingCityAdapter);
                } else {
                    ll_surrounding_city.setVisibility(View.GONE);
                }
                //周边城市点击
                gv_periphery_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
                        YmStatistics.getInstance().tongjiApp(cityDocDataitem.get(pos).getEvent_params());
                        city = cityDocDataitem.get(pos).getName();
                        Log.e(TAG, "city == " + city);
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                        Utils.getCityOneToHttp(mContext, "1");
                        cityStatistics();
                        if (mOnCitySelectListener != null) {
                            mOnCitySelectListener.onCitySelectResult("1");
                            setOnCitySelectListener(null);
                        }
                        startFinish();
                    }
                });
            }

        });
    }

    private void startFinish() {
        if ("2".equals(type)) {
            Intent it1 = new Intent();
            it1.putExtra("city", city);
            setResult(4, it1);
        } else if ("4".equals(type)) {
            Intent it1 = new Intent(mContext, TabDocAndHosListActivity.class);
            it1.putExtra("city", city);
            setResult(4, it1);
        } else if ("5".equals(type)) {
            Intent it1 = new Intent(mContext, TaoActivity.class);
            it1.putExtra("city", city);
            setResult(5, it1);
        } else if ("6".equals(type)) {
            Intent it1 = new Intent();
            it1.putExtra("city", city);
            setResult(4, it1);
        } else {
            Intent it1 = new Intent();
            it1.putExtra("city", city);
            setResult(RequestAndResultCode.CITY_RESULT_CODE, it1);
        }

        onBackPressed();
    }

    /**
     * 定位城市
     */
    private void locatingCity() {
        city = cityLocation;
        if ("失败".equals(city)) {
            city = "全国";
        }
        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
        Utils.getCityOneToHttp(mContext, "1");
        cityStatistics();
        if (mOnCitySelectListener != null) {
            mOnCitySelectListener.onCitySelectResult("1");
            setOnCitySelectListener(null);
        }
        startFinish();
    }

    private void cityStatistics() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("area01", "失败".equals(city) ? "全国" : city);
        StatisticalManage.getInstance().growingIO("location_area", hashMap);
    }

    private void initloadCity() {
        new InitLoadCityApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    citylist = JSONUtil.jsonToArrayList(serverData.data, MainCityData.class);
                    if (null != citylist && citylist.size() > 0) {
                        pinAdapter = new PinyinAdapter591(mContext, citylist);
                        eListView.setAdapter(pinAdapter);
                    }

                    // 展开所有
                    for (int i = 0, length = pinAdapter
                            .getGroupCount(); i < length; i++) {
                        eListView.expandGroup(i);
                    }

                    // 字母按键回调
                    assortView.setOnTouchAssortListener(new OnTouchAssortListener() {

                        View layoutView = LayoutInflater
                                .from(mContext)
                                .inflate(
                                        R.layout.alert_dialog_menu_layout,
                                        null);

                        RelativeLayout alRly = (RelativeLayout) layoutView
                                .findViewById(R.id.pop_city_rly);

                        PopupWindow popupWindow;

                        @Override
                        public void onTouchAssortListener(
                                String str) {
                            int index = pinAdapter
                                    .getAssort()
                                    .getHashList()
                                    .indexOfKey(str);
                            if (index != -1) {
                                eListView.setSelectedGroup(index);
                            }
                            if (popupWindow != null) {
                                // text.setText(str);
                            } else {
                                popupWindow = new PopupWindow(
                                        layoutView, 80, 80,
                                        false);
                                popupWindow.showAtLocation(
                                        alRly,
                                        Gravity.CENTER, 0,
                                        0);
                            }
                        }

                        @Override
                        public void onTouchAssortUP() {
                            if (popupWindow != null)
                                popupWindow.dismiss();
                            popupWindow = null;
                        }
                    });

                    eListView.setOnChildClickListener(new OnChildClickListener() {

                        @Override
                        public boolean onChildClick(
                                ExpandableListView arg0,
                                View arg1,
                                int groupPosition,
                                int childPosition, long arg4) {
                            int tolal = 0;

                            for (int j = 0; j < groupPosition; j++) {
                                int chidsize1 = pinAdapter
                                        .getChildrenCount(j);
                                tolal = tolal + chidsize1;
                            }
                            city = PinyinAdapter591.assortAA
                                    .getHashList()
                                    .getValueIndex(
                                            groupPosition,
                                            childPosition);
                            String id = citylist.get(childPosition).get_id();
                            HashMap<String,String> hashMap = new HashMap<>();
                            hashMap.put("id",id);
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CITY_LIST_CLICK),hashMap,new ActivityTypeData("1"));
                            Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                            Utils.getCityOneToHttp(mContext, "1");
                            cityStatistics();
                            startFinish();
                            return true;
                        }
                    });

                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            // 数据保存
                            List<MainCityData> hsdatas = kjdb.findAll(MainCityData.class);

                            if (null != hsdatas && hsdatas.size() > 0) {
                                for (int i = 0; i < hsdatas.size(); i++) {
                                    kjdb.delete(hsdatas.get(i));
                                }
                            }
                            for (int i = 0; i < citylist.size(); i++) {
                                try {
                                    kjdb.save(citylist.get(i));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "Exception====" + e.toString());
                                }
                            }
                            // 保存当前时间
                            int cur = (int) System.currentTimeMillis();
                            Cfg.saveInt(mContext, "timeCity", cur);
                        }
                    }).start();
                }
            }
        });
    }

    void loadHotCity() {
        new HotCityApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitem) {
                hotcityList = cityDocDataitem;
                hotcityAdapter = new HotCityAdapter2(mContext,
                        hotcityList);
                hotGridlist.setAdapter(hotcityAdapter);
                hotGridlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    //十二城点击
                    @Override
                    public void onItemClick(
                            AdapterView<?> arg0,
                            View arg1, int pos,
                            long arg3) {
                        YmStatistics.getInstance().tongjiApp(hotcityList.get(pos).getEvent_params());
                        city = hotcityList.get(pos).getName();
                        Log.e(TAG, "city == " + city);
                        Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                        Utils.getCityOneToHttp(mContext, "1");

                        cityStatistics();
                        if (mOnCitySelectListener != null) {
                            mOnCitySelectListener.onCitySelectResult("1");
                            setOnCitySelectListener(null);
                        }
                        startFinish();
                    }
                });
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
        initLocation();
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public static void setOnCitySelectListener(OnCitySelectListener onCitySelectListener) {
        mOnCitySelectListener = onCitySelectListener;
    }


    /**
     * 初始话百度的api定位
     */
    private void initLocation() {
        try {
            locationClient = new LocationClient(getApplicationContext());
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(new MyBDLocationListener());
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定位结果的响应函数
     *
     * @author
     */
    public class MyBDLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                Location location = new Location();
                location.setAddress(bdLocation.getAddrStr());
                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);
                String latitude = bdLocation.getLatitude() + "";
                String longitude = bdLocation.getLongitude() + "";
                //判断是否定位失败
                if (!"4.9E-324".equals(latitude) && !"4.9E-324".equals(longitude)) {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, latitude);
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, longitude);
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, "0");
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, "0");
//                    MyToast.makeTextToast2(mContext, "定位失败，请查看手机是否开启了定位权限", MyToast.SHOW_TIME).show();
                }

                String ss = bdLocation.getCity();
                if (ss != null && ss.length() > 1) {
                    if (ss.contains("省")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("市")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("自治区")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }
                    ll_fail.setVisibility(View.GONE);
                    ll_sucess.setVisibility(View.VISIBLE);
                    tv_city_location.setText(provinceLocation);
//                    if (Utils.getCity().equals(provinceLocation)) {
//                        ll_sucess.setBackgroundResource(R.drawable.shape_location_ff527f);
//                        tv_city_location.setTextColor(Color.parseColor("#FF527F"));
//                    } else {
//                        ll_sucess.setBackgroundResource(R.drawable.shape_location_f6f6f6);
//                        tv_city_location.setTextColor(Color.parseColor("#333333"));
//                    }
                    locationClient.unRegisterLocationListener(new MyBDLocationListener());

                } else {
                    provinceLocation = "失败";
                    ll_fail.setVisibility(View.VISIBLE);
                    ll_sucess.setVisibility(View.GONE);
                    locationClient.unRegisterLocationListener(new MyBDLocationListener());
                }
                setLocationCity();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断是否缺少权限
     */

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static boolean lacksPermission(Context mContexts, String permission) {
        return ContextCompat.checkSelfPermission(mContexts, permission) == PackageManager.PERMISSION_DENIED;
    }

    private void JumpSetting() {
        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            i.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            i.setData(Uri.fromParts("package", mContext.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            i.setAction(Intent.ACTION_VIEW);
            i.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            i.putExtra("com.android.settings.ApplicationPkgName", mContext.getPackageName());
        }
        startActivity(i);
    }

}
