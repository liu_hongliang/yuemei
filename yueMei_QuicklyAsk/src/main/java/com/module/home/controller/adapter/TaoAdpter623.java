/**
 *
 */
package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView2;
import com.quicklyask.view.FlowLayout;

import java.util.List;

public class TaoAdpter623 extends BaseAdapter {

    private final String TAG = "TaoAdpter623";

    private List<HomeTaoData> mHotIssues;
    private Context mContext;
    private LayoutInflater inflater;

    ViewHolder viewHolder;

    public TaoAdpter623(Context mContext, List<HomeTaoData> mHotIssues) {
        this.mContext = mContext;
        this.mHotIssues = mHotIssues;
        inflater = LayoutInflater.from(mContext);
        Log.e(TAG, "mHotIssues === " + mHotIssues.size());
    }

    public void setDataList(List<HomeTaoData> dataList) {
        this.mHotIssues.addAll(dataList);
    }

    public List<HomeTaoData> getmHotIssues() {
        return mHotIssues;
    }

    static class ViewHolder {
        LinearLayout taoListCentent;
        LinearLayout mPrompt1;
        FrameLayout mPrompt2;
        LinearLayout mHotPicll;
        ImageView mHotPic1;
        TextView mHotDocNameTV;
        LinearLayout mTaoListJiageLyss;
        TextView mHotTitleTV;
        TextView mHotHosNameTV;
        TextView mHotPriceTV;
        TextView mFeescale;
        LinearLayout mTaoPlusVibility;
        TextView mTaoPlusPrice;
        ImageView mLimitPic;

        ImageView mYishouguangIv;
        TextView mRateTv;
        ImageView mNewIv;
        ImageView mHotIv;
        ImageView mMingyiIv;

        ImageView mAllmanFqIv;
        LinearLayout mButieLy;
        TextView mButieTv;

        FlowLayout mFlowLayout;

        TextView mTeyaoHIv;
        ImageView mTeyaoIv;

        ImageView mCu66Iv;

        TextView mFanxinTv;

        LinearLayout baoxianLy;
        TextView baoxianTv;

        LinearLayout fenqiLy;
        TextView fenqiTv;

        LinearLayout hongbaoLy;
        TextView hongbaoTv;

    }

    @Override
    public int getCount() {
        return mHotIssues == null ? 0 : mHotIssues.size();
    }

    @Override
    public Object getItem(int position) {
        return mHotIssues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint({"NewApi", "InlinedApi", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_tao_new, null);
            viewHolder = new ViewHolder();

            viewHolder.mPrompt1 = convertView.findViewById(R.id.list_searh_null_prompt1);
            viewHolder.mPrompt2 = convertView.findViewById(R.id.list_searh_null_prompt2);

            viewHolder.taoListCentent = convertView.findViewById(R.id.ll_tao_list);

            viewHolder.mHotPicll = convertView
                    .findViewById(R.id.my_collect_tao_im_ll);
            viewHolder.mHotPic1 = convertView
                    .findViewById(R.id.my_collect_tao_im_iv);
            viewHolder.mHotTitleTV = convertView
                    .findViewById(R.id.my_collect_tao_name_tv);
            viewHolder.mHotHosNameTV = convertView
                    .findViewById(R.id.my_collect_tao_hosname_iv);
            viewHolder.mHotDocNameTV = convertView
                    .findViewById(R.id.my_collect_tao_docname_iv);
            viewHolder.mTaoListJiageLyss = convertView
                    .findViewById(R.id.tao_list_jiage_lyss);

            viewHolder.mHotPriceTV = convertView
                    .findViewById(R.id.my_collect_tao_price_dis_iv);
            viewHolder.mFeescale = convertView.findViewById(R.id.collect_tao_feescale_iv);

            viewHolder.mTaoPlusVibility = convertView
                    .findViewById(R.id.tao_plus_vibility);
            viewHolder.mTaoPlusPrice = convertView
                    .findViewById(R.id.tao_plus_price);
            viewHolder.mLimitPic = convertView
                    .findViewById(R.id.my_collect_limit_iv);

            viewHolder.mYishouguangIv = convertView
                    .findViewById(R.id.tao_yishuouguang_iv);
            viewHolder.mRateTv = convertView
                    .findViewById(R.id.tao_list_rate_tv);
            viewHolder.mNewIv = convertView
                    .findViewById(R.id.tao_list_tag_new_iv);
            viewHolder.mHotIv = convertView
                    .findViewById(R.id.tao_list_tag_hot_iv);
            viewHolder.mMingyiIv = convertView
                    .findViewById(R.id.tao_list_tag_mingyi_iv);

            viewHolder.mAllmanFqIv = convertView.findViewById(R.id.tao_list_tag_allman_iv);

            viewHolder.mFlowLayout = convertView.findViewById(R.id.tao_list_tag_allman_flowLayout);

            viewHolder.mButieLy = convertView.findViewById(R.id.tao_butie_ly);
            viewHolder.mButieTv = convertView.findViewById(R.id.tao_butie_tv);

            viewHolder.mTeyaoHIv = convertView.findViewById(R.id.tao_list_teyao_iv);
            viewHolder.mTeyaoIv = convertView.findViewById(R.id.my_teyao_iv);

            viewHolder.mFanxinTv = convertView.findViewById(R.id.tao_list_tag_fanxian_tv);

            viewHolder.mCu66Iv = convertView.findViewById(R.id.tao_list_cu_66);

            viewHolder.baoxianLy = convertView.findViewById(R.id.taolist_isbaoxian_rly);
            viewHolder.baoxianTv = convertView.findViewById(R.id.taolist_isbaoxian_tv);

            viewHolder.fenqiLy = convertView.findViewById(R.id.taolist_isfenqi_rly);
            viewHolder.fenqiTv = convertView.findViewById(R.id.taolist_isfenqi_tv);

            viewHolder.hongbaoLy = convertView.findViewById(R.id.taolist_ishongbao_rly);
            viewHolder.hongbaoTv = convertView.findViewById(R.id.taolist_ishongbao_tv);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        HomeTaoData hotIsData = mHotIssues.get(position);
        if (hotIsData != null){
            String hotIsDataId = hotIsData.get_id();
            if (!TextUtils.isEmpty(hotIsDataId)){
                int tao_id = Integer.parseInt(hotIsData.get_id());
                Log.e(TAG, "tao_id == " + tao_id);
                if (tao_id <= 0) {
                    viewHolder.taoListCentent.setVisibility(View.GONE);
                    if (tao_id == 0) {
                        viewHolder.mPrompt1.setVisibility(View.VISIBLE);
                        viewHolder.mPrompt2.setVisibility(View.GONE);
                    } else {
                        viewHolder.mPrompt1.setVisibility(View.GONE);
                        viewHolder.mPrompt2.setVisibility(View.VISIBLE);
                    }

                } else {
                    viewHolder.mPrompt1.setVisibility(View.GONE);
                    viewHolder.mPrompt2.setVisibility(View.GONE);
                    viewHolder.taoListCentent.setVisibility(View.VISIBLE);

                    try {
                        Log.e(TAG, "hotIsData.getImg() == " + hotIsData.getImg());

                        Glide.with(mContext)
                                .load(hotIsData.getImg())
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(viewHolder.mHotPic1);

                    } catch (OutOfMemoryError e) {
                        // ViewInject.toast("内存溢出3");
                    }

                    viewHolder.mHotTitleTV.setText("<" + hotIsData.getTitle() + "> " + hotIsData.getSubtitle());

                    boolean ifnull = false;

                    if (null != hotIsData.getDoc_name()
                            && hotIsData.getDoc_name().length() > 0) {
                        viewHolder.mHotDocNameTV.setText(hotIsData.getDoc_name());
                        ifnull = false;
                    } else {
                        viewHolder.mHotDocNameTV.setText("");
                        ifnull = true;
                    }

                    if (null != hotIsData.getHos_name()
                            && hotIsData.getHos_name().length() > 0) {
                        if (ifnull) {
                            viewHolder.mHotHosNameTV.setText(hotIsData.getHos_name());
                        } else {
                            viewHolder.mHotHosNameTV.setText("," + hotIsData.getHos_name());
                        }
                    } else {
                        viewHolder.mHotHosNameTV.setText(" ");
                    }

                    viewHolder.mRateTv.setText(hotIsData.getRate());

                    if (null != hotIsData.getNewp()) {
                        String newp = hotIsData.getNewp();
                        if (newp.equals("1")) {
                            viewHolder.mNewIv.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.mNewIv.setVisibility(View.GONE);
                        }
                    }

                    if (null != hotIsData.getHot()) {
                        String hot = hotIsData.getHot();
                        if (hot.equals("1")) {
                            viewHolder.mHotIv.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.mHotIv.setVisibility(View.GONE);
                        }
                    }

                    if (null != hotIsData.getMingyi()) {
                        String mingyi = hotIsData.getMingyi();
                        if (mingyi.equals("1")) {
                            viewHolder.mMingyiIv.setVisibility(View.VISIBLE);
                            viewHolder.mMingyiIv.setVisibility(View.GONE);
                        } else {
                        }
                    }

                    String baoxian = hotIsData.getBaoxian();
                    if (null != baoxian && baoxian.length() > 0) {
                        viewHolder.baoxianLy.setVisibility(View.VISIBLE);
                        viewHolder.baoxianTv.setText(baoxian);
                    } else {
                        viewHolder.baoxianLy.setVisibility(View.GONE);
                    }

                    if (null != hotIsData.getRepayment() && hotIsData.getRepayment().length() > 0) {
                        viewHolder.fenqiLy.setVisibility(View.VISIBLE);
                        viewHolder.fenqiTv.setText(hotIsData.getRepayment());
                    } else {
                        viewHolder.fenqiLy.setVisibility(View.GONE);
                    }

                    String hosRedPacket = hotIsData.getHos_red_packet();
                    if (null != hosRedPacket && hosRedPacket.length() > 0) {
                        viewHolder.hongbaoLy.setVisibility(View.VISIBLE);
                        viewHolder.hongbaoTv.setText(hosRedPacket);
                    } else {
                        viewHolder.hongbaoLy.setVisibility(View.GONE);
                    }


                    String ymPrice = hotIsData.getPrice_discount();//悦美价
                    String member_price = hotIsData.getMember_price();//plus会员价
                    int i = Integer.parseInt(member_price);
                    if (i >= 0) {
                        viewHolder.mTaoPlusVibility.setVisibility(View.VISIBLE);
                        viewHolder.mTaoPlusPrice.setText("¥" + member_price);
                        viewHolder.mHotPriceTV.setText(ymPrice);

                    } else {
                        viewHolder.mTaoPlusVibility.setVisibility(View.GONE);
                        viewHolder.mHotPriceTV.setText(ymPrice);
                    }

                    viewHolder.mFeescale.setText(hotIsData.getFeeScale());

                    if (null != ymPrice && ymPrice.length() > 0 && Integer.parseInt(ymPrice) > 1000) {
                        String is_fanxian = hotIsData.getIs_fanxian();
                        if ("0".equals(is_fanxian)) {
                            viewHolder.mFanxinTv.setVisibility(View.GONE);
                        } else {
                            viewHolder.mFanxinTv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        viewHolder.mFanxinTv.setVisibility(View.GONE);
                    }

                    String limit = hotIsData.getSpecialPrice();//限时特价
                    if (limit.equals("1")) {
                        viewHolder.mLimitPic.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mLimitPic.setVisibility(View.GONE);
                    }

                    if (hotIsData.getShixiao().equals("1")) {
                        viewHolder.mYishouguangIv.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mYishouguangIv.setVisibility(View.GONE);
                    }

                    if (null != hotIsData.getSeckilling()) {
                        if (hotIsData.getSeckilling().equals("1")) {
                            viewHolder.mAllmanFqIv.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.mAllmanFqIv.setVisibility(View.GONE);
                        }
                    }

                    if (null != hotIsData.getInvitation()
                            && hotIsData.getInvitation().equals("1")) {
                        viewHolder.mTeyaoHIv.setVisibility(View.VISIBLE);
                        viewHolder.mTeyaoIv.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mTeyaoHIv.setVisibility(View.GONE);
                        viewHolder.mTeyaoIv.setVisibility(View.GONE);
                    }

                    if (null != hotIsData.getLijian() && hotIsData.getLijian().length() > 0) {

                        if (hotIsData.getLijian().equals("0")) {
                            viewHolder.mButieLy.setVisibility(View.GONE);
                        } else {

                            if (null != hotIsData.getImg66() && hotIsData.getImg66().length() > 0) {
                                viewHolder.mButieLy.setVisibility(View.GONE);
                            } else {
                                viewHolder.mButieLy.setVisibility(View.VISIBLE);
                                viewHolder.mButieTv.setText(hotIsData.getLijian());
                            }
                        }
                    }

                    if (null != hotIsData.getImg66() && hotIsData.getImg66().length() > 0) {

                        viewHolder.mCu66Iv.setVisibility(View.VISIBLE);

                        Glide.with(mContext)
                                .load(hotIsData.getImg66())
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(viewHolder.mCu66Iv);

                        viewHolder.mFanxinTv.setVisibility(View.GONE);
                        viewHolder.mMingyiIv.setVisibility(View.GONE);
                        viewHolder.mButieLy.setVisibility(View.GONE);
                        viewHolder.mTeyaoHIv.setVisibility(View.GONE);
                        viewHolder.mTeyaoIv.setVisibility(View.GONE);
                        viewHolder.mLimitPic.setVisibility(View.GONE);
                        viewHolder.mNewIv.setVisibility(View.GONE);
                        viewHolder.mHotIv.setVisibility(View.GONE);

                    } else {
                        viewHolder.mCu66Iv.setVisibility(View.GONE);
                    }

                    int width = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                    int height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                    viewHolder.mTaoListJiageLyss.measure(width, height);
                    int width1 = viewHolder.mTaoListJiageLyss.getMeasuredWidth();

                    Log.e(TAG, position + "width1 == " + width1);

                    //第二件折扣设置
                    Log.e(TAG, "hotIsData.getPromotion()的个数 == " + hotIsData.getPromotion().size());
                    ViewGroup.LayoutParams layoutParams = viewHolder.mHotPicll.getLayoutParams();
                    if (hotIsData.getPromotion().size() > 0) {
                        viewHolder.mFlowLayout.setVisibility(View.VISIBLE);
                        setTagView(viewHolder.mFlowLayout, hotIsData.getPromotion());
                        layoutParams.height = Utils.dip2px(160);
                    } else {
                        layoutParams.height = Utils.dip2px(138);
                        viewHolder.mFlowLayout.setVisibility(View.GONE);
                    }
                    viewHolder.mHotPicll.setLayoutParams(layoutParams);
                }
            }

        }

        return convertView;
    }

    /**
     * 标签设置
     *
     * @param mFlowLayout
     * @param lists
     */
    private void setTagView(FlowLayout mFlowLayout, List<Promotion> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            for (int i = 0; i < lists.size(); i++) {
                Promotion promotion = (Promotion)lists.get(i);
                TextView textView = new TextView(mContext);
                textView.setText(promotion.getTitle());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                mFlowLayout.addView(textView);
            }
        }
    }

    public void add(List<HomeTaoData> infos) {
        mHotIssues.addAll(infos);
    }

    public List<HomeTaoData> getData() {
        return mHotIssues;
    }

    public void clearmHotIssues() {
        mHotIssues.clear();
    }

    public void clears(DropDownListView2 view2) {
        view2.getFooterLayout().setVisibility(View.GONE);
        view2.getHeaderLayout().setVisibility(View.GONE);
        mHotIssues.clear();
        notifyDataSetChanged();
    }
}
