package com.module.home.controller.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class HomeItemHosAdapter extends BaseQuickAdapter<HomeTaoData,BaseViewHolder> {
    public HomeItemHosAdapter(int layoutResId, @Nullable List<HomeTaoData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeTaoData item) {
        Glide.with(mContext).load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.ALL))
                .into((ImageView) helper.getView(R.id.home_item_hos_img));
        helper.setText(R.id.home_item_hos_title,item.getSubtitle())
                .setText(R.id.home_item_hos_price,item.getPrice_discount());

    }
}
