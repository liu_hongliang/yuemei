package com.module.home.controller.adapter;

import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.FeedSkuRecommendData;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import org.xutils.common.util.DensityUtil;

import java.util.List;

public class HomeItemSkuCommendAdapter extends BaseQuickAdapter<FeedSkuRecommendData, BaseViewHolder> {
    public HomeItemSkuCommendAdapter(int layoutResId, @Nullable List<FeedSkuRecommendData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, FeedSkuRecommendData item) {
        LinearLayout container = helper.getView(R.id.ll_root);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        layoutParams.width = (DensityUtil.getScreenWidth() / 2 - Utils.dip2px(5 + 3 + 9 + 9 + 6)) / 2;
        container.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) helper.getView(R.id.item_img).getLayoutParams();
        lp.setMargins(0, DensityUtil.dip2px(10), 0, 0);

        Glide.with(mContext)
                .load(item.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.ALL))
                .override((DensityUtil.getScreenWidth() / 2 - Utils.dip2px(5 + 3 + 9 + 9 + 6)) / 2, (DensityUtil.getScreenWidth() / 2 - Utils.dip2px(5 + 3 + 9 + 9 + 6)) / 2)
                .into((ImageView) helper.getView(R.id.item_img));
        helper.setText(R.id.item_title, item.getTitle())
                .setText(R.id.item_txt_price, item.getPrice_discount());

    }
}
