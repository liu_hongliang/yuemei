package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.HomeActivityData;
import com.module.home.model.bean.HomeActivityImgData;
import com.module.home.model.bean.HomeFourColumnData;
import com.module.home.view.HomeColumnCountDownView;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by 裴成浩 on 2019/9/19
 */
public class HomeFourColumnAdapter extends RecyclerView.Adapter<HomeFourColumnAdapter.ViewHolder> {

    private final String TAG = "HomeFourColumnAdapter";
    private final Activity mContext;
    private final List<HomeFourColumnData> mDatas;
    private final LayoutInflater mInflater;
    private final FunctionManager functionManager;
    private final int windowWidth;
    private final Random mRandom;
    private final int imageW = Utils.dip2px(68);                //图片宽度
    private final int spacing = Utils.dip2px(9);               //item外边距

    public HomeFourColumnAdapter(Activity context, List<List<HomeActivityData>> datas) {
        this.mContext = context;
        this.mDatas = refactoringData(datas);

        functionManager = new FunctionManager(mContext);
        mInflater = LayoutInflater.from(mContext);
        windowWidth = Utils.getScreenSize(mContext)[0];
        mRandom = new Random();
    }


    /**
     * 把一行4个改为一行两个
     *
     * @param datas
     * @return
     */
    private ArrayList<HomeFourColumnData> refactoringData(List<List<HomeActivityData>> datas) {
        ArrayList<HomeActivityData> leftDatas = new ArrayList<>();
        ArrayList<HomeActivityData> rightDatas = new ArrayList<>();

        for (int i = 0; i < datas.size(); i++) {
            List<HomeActivityData> data = datas.get(i);
            if (data.size() >= 2) {
                leftDatas.add(data.get(0));
                rightDatas.add(data.get(1));
            }
        }

        ArrayList<HomeFourColumnData> arrayList = new ArrayList<>();
        int size = leftDatas.size() >= rightDatas.size() ? rightDatas.size() : leftDatas.size();
        for (int i = 0; i < size; i++) {
            HomeFourColumnData homeFourColumnData = new HomeFourColumnData();
            homeFourColumnData.setLeftData(leftDatas.get(i));
            homeFourColumnData.setRightData(rightDatas.get(i));
            arrayList.add(homeFourColumnData);
        }

        return arrayList;
    }

    @NonNull
    @Override
    public HomeFourColumnAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.item_home_four_column_view, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeFourColumnAdapter.ViewHolder viewHolder, int pos) {

        //设置UI间距
        int itemSpacing = (windowWidth - (imageW * 4) - (spacing * 2) - Utils.dip2px(1)) / 3;
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.fourColumnView.getLayoutParams();
        layoutParams.width = (itemSpacing + imageW) * 2;
        if (pos % 2 == 0) {
            layoutParams.leftMargin = spacing;
            layoutParams.rightMargin = itemSpacing / 2;
        } else {
            layoutParams.leftMargin = itemSpacing / 2;
            layoutParams.rightMargin = spacing;
        }
        viewHolder.fourColumnView.setLayoutParams(layoutParams);

        ViewGroup.MarginLayoutParams leftParams = (ViewGroup.MarginLayoutParams) viewHolder.leftView.getLayoutParams();
        ViewGroup.MarginLayoutParams rightParams = (ViewGroup.MarginLayoutParams) viewHolder.rightView.getLayoutParams();
        leftParams.rightMargin = itemSpacing / 2;
        rightParams.leftMargin = itemSpacing / 2;
        viewHolder.leftView.setLayoutParams(leftParams);
        viewHolder.rightView.setLayoutParams(rightParams);

        //设置数据
        HomeFourColumnData data = mDatas.get(pos);
        HomeActivityData leftData = data.getLeftData();
        HomeActivityData rightData = data.getRightData();

        //头部数据设置
        String leftImg = leftData.getImg();
        String rightImg = rightData.getImg();
        String leftEndTime = leftData.getEndTime();
        String miaosha = leftData.getMiaosha();

        if (!TextUtils.isEmpty(leftImg) || !TextUtils.isEmpty(rightImg) || (!TextUtils.isEmpty(leftEndTime) && "1".equals(miaosha))) {
            viewHolder.columnTopView.setVisibility(View.VISIBLE);

            //头部左侧显示隐藏
            if (!TextUtils.isEmpty(leftImg)) {
                viewHolder.leftTopView.setVisibility(View.VISIBLE);
                functionManager.setImageSrc(viewHolder.leftTopImage, leftImg);
            } else {
                viewHolder.leftTopView.setVisibility(View.GONE);
            }

            //头部右侧显示隐藏
            if (!TextUtils.isEmpty(rightImg) || (!TextUtils.isEmpty(leftEndTime) && "1".equals(miaosha))) {
                viewHolder.rightTopView.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(leftEndTime) && "1".equals(miaosha)) {
                    viewHolder.rightTopCountdown.setVisibility(View.VISIBLE);
                    viewHolder.rightTopImage.setVisibility(View.GONE);

                    String startTime = Utils.stampToJavaAndPhpDate(Utils.getSecondTimestamp() + "");
                    String endTime = Utils.stampToJavaAndPhpDate(leftEndTime);
                    long[] timeSubAct = Utils.getTimeSub(startTime, endTime);
                    long[] timesAct = {timeSubAct[1], timeSubAct[2], timeSubAct[3]};
                    viewHolder.rightTopCountdown.setTimes(timesAct);
                    if (!viewHolder.rightTopCountdown.isRun()) {
                        viewHolder.rightTopCountdown.beginRun();
                    }

                } else if (!TextUtils.isEmpty(rightImg)) {
                    viewHolder.rightTopCountdown.setVisibility(View.GONE);
                    viewHolder.rightTopImage.setVisibility(View.VISIBLE);

                    //头部左布局宽度与底部图片宽度一致
                    ViewGroup.LayoutParams leftTopParams = viewHolder.leftTopView.getLayoutParams();
                    leftTopParams.width = imageW;
                    viewHolder.leftTopView.setLayoutParams(leftTopParams);

                    //设置左间距
                    ViewGroup.MarginLayoutParams rightTopParams = (ViewGroup.MarginLayoutParams) viewHolder.rightTopView.getLayoutParams();
                    rightTopParams.leftMargin = itemSpacing;
                    viewHolder.rightTopView.setLayoutParams(rightTopParams);

                    functionManager.setImageSrc(viewHolder.rightTopImage, rightImg);
                } else {
                    viewHolder.rightTopCountdown.setVisibility(View.GONE);
                    viewHolder.rightTopImage.setVisibility(View.GONE);
                }
            } else {
                viewHolder.rightTopView.setVisibility(View.GONE);
            }

        } else {
            viewHolder.columnTopView.setVisibility(View.INVISIBLE);
        }

        //item左view开始设置
        setTitleColor(viewHolder.leftTitle, leftData);
        viewHolder.leftTitle.setText(leftData.getTitle());
        setnNewReportsData(viewHolder.leftimage, leftData);

        //item右view开始设置
        setTitleColor(viewHolder.rightTitle, leftData);
        viewHolder.rightTitle.setText(rightData.getTitle());
        setnNewReportsData(viewHolder.rightimage, rightData);
    }

    /**
     * 设置文字颜色
     *
     * @param textView
     * @param leftData
     */
    private void setTitleColor(TextView textView, HomeActivityData leftData) {
        String rightColor = leftData.getColor_value();
        String rightColour = leftData.getColour_value();
        try {
            if (rightColor.startsWith("#")) {
                textView.setTextColor(Utils.setCustomColor(rightColor));
            } else if (rightColour.startsWith("#")) {
                textView.setTextColor(Utils.setCustomColor(rightColour));
            } else {
                textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            }
        } catch (NumberFormatException e) {
            textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        }
    }

    /**
     * 设置图片滑动
     */
    private void setnNewReportsData(ViewFlipper newFlipper, HomeActivityData data) {
        newFlipper.removeAllViews();
        List<HomeActivityImgData> images = data.getRecommend_img();
        String swiper = data.getSwiper();
        if ("1".equals(swiper)) {
            for (HomeActivityImgData img : images) {
                setImageView(newFlipper, img);
            }
            newFlipper.setFlipInterval(3000);
            newFlipper.startFlipping();
        } else {
            if (images.size() > 0) {
                int index = mRandom.nextInt(images.size());
                setImageView(newFlipper, images.get(index));
            }
        }
    }

    /**
     * 添加滑动图片
     *
     * @param newFlipper
     * @param img
     */
    private void setImageView(ViewFlipper newFlipper, HomeActivityImgData img) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        functionManager.setRoundImageSrc(imageView, img.getImg(), Utils.dip2px(4));
        newFlipper.addView(imageView);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout fourColumnView;

        LinearLayout columnTopView;
        FrameLayout leftTopView;
        ImageView leftTopImage;
        FrameLayout rightTopView;
        HomeColumnCountDownView rightTopCountdown;
        ImageView rightTopImage;

        TextView leftTitle;
        LinearLayout leftView;
        ViewFlipper leftimage;

        TextView rightTitle;
        LinearLayout rightView;
        ViewFlipper rightimage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fourColumnView = itemView.findViewById(R.id.home_four_column_view);

            columnTopView = itemView.findViewById(R.id.four_column_top_view);
            leftTopView = itemView.findViewById(R.id.four_column_left_top);
            leftTopImage = itemView.findViewById(R.id.four_column_left_top_image);
            rightTopView = itemView.findViewById(R.id.four_column_right_top);
            rightTopCountdown = itemView.findViewById(R.id.four_column_right_top_countdown);
            rightTopImage = itemView.findViewById(R.id.four_column_right_top_image);

            leftView = itemView.findViewById(R.id.four_column_left_view);
            leftTitle = itemView.findViewById(R.id.four_column_left_title);
            leftimage = itemView.findViewById(R.id.four_column_left_image);

            rightView = itemView.findViewById(R.id.four_column_right_view);
            rightTitle = itemView.findViewById(R.id.four_column_right_title);
            rightimage = itemView.findViewById(R.id.four_column_right_image);

            //左侧View点击事件
            leftView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivityData leftData = mDatas.get(getLayoutPosition()).getLeftData();
                    String url = leftData.getUrl();
                    HashMap<String, String> event_params = leftData.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(event_params);
                    if (!TextUtils.isEmpty(url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });

            //右侧View点击事件
            rightView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivityData rightData = mDatas.get(getLayoutPosition()).getRightData();
                    String url = rightData.getUrl();
                    HashMap<String, String> event_params = rightData.getEvent_params();
                    YmStatistics.getInstance().tongjiApp(event_params);
                    if (!TextUtils.isEmpty(url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }
}
