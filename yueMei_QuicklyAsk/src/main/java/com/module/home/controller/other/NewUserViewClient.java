package com.module.home.controller.other;

import android.content.Intent;
import android.widget.Toast;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.chatnet.CookieConfig;
import com.module.commonview.view.share.MyUMShareListener;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.home.controller.activity.NewUserActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.Utils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.umeng.socialize.utils.Log;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Response;

/**
 * Created by 裴成浩 on 2019/1/29
 */
public class NewUserViewClient implements BaseWebViewClientCallback {

    private final NewUserActivity mActivity;
    private final Intent intent;
    private String uid;
    private String TAG = "ZhuanTiWebViewClient";

    public NewUserViewClient(NewUserActivity mActivity) {
        this.mActivity = mActivity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        uid = Utils.getUid();
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页

                String id = obj.getString("id");

                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                intent.putExtra("docId", id);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                mActivity.startActivity(intent);
                break;
            case "6":   //问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link);
                intent.putExtra("qid", qid);
                mActivity.startActivity(intent);

                break;

        }
    }

    /**
     * 分享设置
     *
     * @param hashMap
     */
    public void setShare(final String callback_url6361, HashMap<String, String> hashMap) {
        MyUMShareListener myUMShareListener = new MyUMShareListener(mActivity);
        boolean installWeiXin = UMShareAPI.get(mActivity).isInstall(mActivity, SHARE_MEDIA.WEIXIN);

        if (!installWeiXin) {     //微信不存在
            Toast.makeText(mActivity, "请先安装微信", Toast.LENGTH_SHORT).show();
            return;
        } else {

            UMWeb shareWeb = new UMWeb(hashMap.get("tencentUrl"));
            shareWeb.setTitle(hashMap.get("tencentTitle"));
            shareWeb.setThumb(new UMImage(mActivity, hashMap.get("shareImgUrl")));
            shareWeb.setDescription(hashMap.get("tencentTitle"));
            new ShareAction(mActivity).withText(hashMap.get("tencentText")).withMedia(shareWeb).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE).setCallback(myUMShareListener).share();
        }

        myUMShareListener.setOnShareResultClickListener(new MyUMShareListener.OnShareResultClickListener() {
            @Override
            public void onShareResultClick(SHARE_MEDIA platform) {
                Toast.makeText(mActivity, platform + " 分享成功啦", Toast.LENGTH_SHORT).show();
                mActivity.docDetWeb.post(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.docDetWeb.loadUrl("javascript:showCover('succeed')");
                    }
                });

                if (!"".equals(callback_url6361)) {
                    CookieConfig.getInstance().setCookie("https", "user.yuemei.com","user.yuemei.com");
                    OkGo.post(callback_url6361).execute(new StringCallback() {
                        @Override
                        public void onSuccess(String s, Call call, Response response) {
                            Log.d(TAG, "s------------>" + s);
                        }
                    });
                }
            }

            @Override
            public void onShareErrorClick(SHARE_MEDIA platform, Throwable t) {
                Toast.makeText(mActivity, platform + " 分享失败啦", Toast.LENGTH_SHORT).show();

                mActivity.docDetWeb.post(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.docDetWeb.loadUrl("javascript:showCover('fail')");
                    }
                });
            }
        });
    }

}
