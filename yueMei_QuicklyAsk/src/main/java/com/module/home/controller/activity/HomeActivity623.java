package com.module.home.controller.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jcodecraeer.xrecyclerview.AppBarStateChangeListener;
import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.base.view.ui.YMBanner;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.DiariesPostShowData;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.HomeTopView;
import com.module.commonview.view.MainTableButtonView;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebUtil;
import com.module.community.web.WebViewUrlLoading;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.home.controller.adapter.CommunityAdapter;
import com.module.home.controller.adapter.HomeHorizontlaAdapter;
import com.module.home.controller.adapter.HomeSaleListAdapter;
import com.module.home.model.api.HomeApi;
import com.module.home.model.bean.BbsBean;
import com.module.home.model.bean.BoardBean;
import com.module.home.model.bean.CardBean;
import com.module.home.model.bean.Coupos;
import com.module.home.model.bean.Floating;
import com.module.home.model.bean.HomeAskEntry;
import com.module.home.model.bean.HomeButtomFloat;
import com.module.home.model.bean.HomeData623;
import com.module.home.model.bean.HomeNav;
import com.module.home.model.bean.HomePlus;
import com.module.home.model.bean.HotBean;
import com.module.home.model.bean.HuangDeng1;
import com.module.home.model.bean.LanMu2Bean;
import com.module.home.model.bean.LanMu3Bean;
import com.module.home.model.bean.LanmuTao;
import com.module.home.model.bean.NewZt;
import com.module.home.model.bean.NewZtCouponsBean;
import com.module.home.model.bean.TabContent;
import com.module.home.model.bean.TaoData;
import com.module.home.model.bean.Tuijshare;
import com.module.home.view.LoadingProgress;
import com.module.home.view.OnViewpagerChangeListener;
import com.module.home.view.ScrollerViewpager;
import com.module.home.view.fragment.HomeFragment;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.module.taodetail.controller.activity.adapter.TagNavigationAdapter692;
import com.module.taodetail.model.bean.TaoTagItem;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CustomDialog;
import com.quicklyask.view.NewuserConponsPop;
import com.quicklyask.view.PushNewDialog;
import com.quicklyask.view.TimerTextView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.common.SocializeConstants;
import com.youth.banner.listener.OnBannerListener;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.utils.SystemTool;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simplecache.ACache;


/**
 * 首页
 * Created by 裴成浩 on 17/7/28.
 */
public class HomeActivity623 extends FragmentActivity {
    //常用变量
    private final String TAG = "HomeActivity623";
    private HomeActivity623 mContext;
    private ACache mCache;

    //城市
    private String curCity;
    private String city = "全国";

    //标题栏变量
    private RelativeLayout citySelectRly;
    private TextView cityTv;
    private LinearLayout serachRly123;
    private TextView searchEt;
    private RelativeLayout saoerMaRly;
    private TextView homeCartNum;

    //头部幻灯片
    private List<String> urlImageList; //幻灯片路径集合
    private List<String> txtViewpager;  //幻灯片上的文字内容

    // 10个tab位
    private ImageView fiveMetro[] = new ImageView[5];
    private ImageView homeTabBgIv;

    //10个项目类型导航球
    private RecyclerView homeNavigation;
    private RecyclerView homeHorizontalList;

    //metro top 1-6
    private LinearLayout metroTopList;

    private LinearLayout homeFiveMetroOne;
    private LinearLayout homeFiveMetroTwo;
    //新人特价
    private RelativeLayout newuserMetroClick;
    private ScrollerViewpager newuserMetroViewPager;
    private TextView newuserMetroMoney;

    //会员推广位
    private LinearLayout pulsPromoteView;
    private ImageButton pulsPromoteImg;

    //帖子评论入口
    private LinearLayout reviewEntryView;
    private ImageButton reviewEntryImg;

    //热门推荐
    private ImageView ztIv[] = new ImageView[3];
    private LinearLayout zhuantiLy;
    private int mCurZt = 0;

    //metro top 7-9
    private LinearLayout metroBottomList;

    //精选好医
    private LinearLayout hyZtContentLy;
    private ImageView hyZtIv[] = new ImageView[3];
    private LinearLayout hyZtLy;
    private int mCurtjZt = 0;

    //今日特卖
    private RecyclerView saleList;
    private LinearLayout homeSale;

    //日记横滑
    private ArrayList<YMBaseFragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();

    //新用户领礼包
    private RelativeLayout newUserBaoRly;
    private ImageView newUserIv;
    private ImageView mMessageColse;

    private RelativeLayout floatingRly;
    private ImageView floatingIv;
    private ImageView floatingColseIv;

    private LinearLayout homeCouposRly;
    private TimerTextView homeCouposTime;

    //首页json数据实体类
    private HomeData623 homeData;       //首页数据
    private List<HuangDeng1> hdlist = new ArrayList<>();     //幻灯片实体类
    private HomeNav homeNav;                                //10个tab导航球实体类
    private List<TabContent> homeTab = new ArrayList<>();     //10tab 导航集合实体类
    private List<TaoTagItem> homeTag = new ArrayList<>();       //10个项目类型导航球
    private List<CardBean> homeCard = new ArrayList<>();       //10个项目类型导航球
    private List<List<HuangDeng1>> metroTopData = new ArrayList<>();    //metroTop 实体类
    private HomePlus mPlusData;                                 //会员推实体类
    private HomeAskEntry mAskEntry;                              //提问入口数据
    private List<HuangDeng1> ztlist = new ArrayList<>();        //热门推荐实体类
    private List<List<HuangDeng1>> metroBottomData = new ArrayList<>();  //metroBottom 实体类
    private List<HuangDeng1> tuijlist = new ArrayList<>();  //精选好医 实体类
    private List<HotBean> hotData = new ArrayList<>();      //今日特卖 实体类
    private List<BbsBean> bbsData = new ArrayList<>();      //社区标题 实体类
    private List<BoardBean> boardlist = new ArrayList<>();  //日记横向滑动title实体类
    private ArrayList<Tuijshare> tuijshare = new ArrayList<>();  //首页推荐5种类型实体类
    private Floating mFloating;

    private int[] homeCardColor = new int[]{R.color.home_bottom_1, R.color.home_bottom_2, R.color.home_bottom_3, R.color.home_bottom_4};

    //其他
    private SmartRefreshLayout homeRefresh;

    private int windowsWight;           //屏幕的宽
    private LayoutInflater inflater;
    private BaseWebViewClientMessage webViewClientManager;
    private LoadingProgress mDialog;      //加载效果dialog
    private int mPos = 0;               //当前的是第几页
    private FrameLayout home625Conent;
    private Toolbar llHomeToobar;
    private FrameLayout homeTitle;
    private View titleBackground;

    private TabLayout mTabLayout;
    private AppBarLayout mAppBar;
    private ViewPager mViewPage;

    private final int HOME_TIP = 121;
    private final int HOME_TITLE = 122;
    private HomeApi homeApi;
    private int statusbarHeight;        //获取状态栏高度
    private LinearLayout mBbsTital;
    private RecyclerView mBbsList;
    private TextView mBottomTip;
    private NewZt mNewZt;
    private YMBanner mTopBanner;
    private YMBanner mHomeMtroBanner;
    private TextView mTitleTxt;
    private TextView mSubTitleTxt;
    private TextView mPriceTxt;
    private Button mBannerBtn;
    private RelativeLayout bannerContainer;
    private List<LanMu2Bean> lanmu2;
    private List<LanMu3Bean> lanmu3;
    private String couponsUrl;
    private Coupos coupons;
    private String url;
    private HashMap<String, String> event_params;
    private boolean isExpand = true;
    private Gson mGson;
    private BaseNetWorkCallBackApi searchEntryApi;
    private ArrayList<String> titleLists;
    private boolean isLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home625);
        mContext = HomeActivity623.this;
        mCache = ACache.get(mContext);
        inflater = LayoutInflater.from(mContext);
        isLogin = Utils.isLogin();
        webViewClientManager = new BaseWebViewClientMessage(mContext);

        mDialog = new LoadingProgress(mContext);

        QMUIStatusBarHelper.setStatusBarLightMode(mContext);
        // 获取屏幕高宽
        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        windowsWight = metric.widthPixels;

        statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);

        Log.e(TAG, "uid == " + Utils.getUid());
        Log.e(TAG, "友盟SDK版本 == " + SocializeConstants.SDK_VERSION);

        homeApi = new HomeApi();
        searchEntryApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "searchEntry");

        mGson = new Gson();

        //图形初始化
        initView();
        //回调初始化
        initSetOnListener();
        //设置定位城市
        setCity();
        //判断是否联网。来获取本地缓存数据，或者是网络数据
        getCachedData();
    }


    /***
     * 设置定位城市
     */
    private boolean setCity() {
        boolean isRefresh = false;
        curCity = Utils.getCity();

        if (curCity.length() > 0) {
            if (curCity.equals("失败")) {
                city = "全国";
                cityTv.setText(city);

            } else if (!curCity.equals(city)) {
                city = curCity;
                cityTv.setText(city);
                mDialog.startLoading();
                isRefresh = true;
            }
        } else {
            city = "全国";
            cityTv.setText("全国");
        }
        return isRefresh;
    }

    /**
     * 获取本地缓存数据
     */
    private void getCachedData() {
        if (SystemTool.checkNet(mContext)) {
            mDialog.startLoading();
            loadHomeData();     //获取初始化数据
        } else {
            String homejson = mCache.getAsString(FinalConstant.HOMEJSON);
            if (homejson != null) {
                try {
                    homeData = JSONUtil.TransformSingleBean(homejson, HomeData623.class);
                    loadHomeDataToView();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 图形初始化
     */
    private void initView() {
        home625Conent = findViewById(R.id.home625_content);
        //整体变量
        llHomeToobar = findViewById(R.id.ll_home_toobar);
        homeTitle = findViewById(R.id.ll_home_title);
        titleBackground = findViewById(R.id.title_background_color);

        mTabLayout = findViewById(R.id.home_tab_layout);
        mAppBar = findViewById(R.id.home_app_bar);
        mViewPage = findViewById(R.id.home_view_page);

        homeRefresh = findViewById(R.id.home_refresh_layout);

        //标题栏变量
        citySelectRly = findViewById(R.id.tab_doc_city_rly1);
        cityTv = findViewById(R.id.tab_doc_list_city_tv1);
        serachRly123 = findViewById(R.id.search_rly_click1);
        searchEt = findViewById(R.id.home_input_edit1);
        homeCartNum = findViewById(R.id.home_cart_num);
        saoerMaRly = findViewById(R.id.sao_sao_rly1);

        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            homeCartNum.setVisibility(View.VISIBLE);
            homeCartNum.setText(cartNumber);
        } else {
            homeCartNum.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams layoutParams = homeTitle.getLayoutParams();
        layoutParams.height = statusbarHeight + Utils.dip2px(50);
        homeTitle.setLayoutParams(layoutParams);

        ViewGroup.LayoutParams layoutParams1 = llHomeToobar.getLayoutParams();
        layoutParams1.height = statusbarHeight + Utils.dip2px(50);
        llHomeToobar.setLayoutParams(layoutParams1);

        initSearhShowData();     //搜索框按钮点击回调初始化

        //头部幻灯片
        mTopBanner = findViewById(R.id.home_tablayout_top_banner);

        ViewGroup.MarginLayoutParams topBannerParams = (ViewGroup.MarginLayoutParams) mTopBanner.getLayoutParams();
        topBannerParams.topMargin = statusbarHeight + Utils.dip2px(50);
        topBannerParams.height = Utils.dip2px(125);
        mTopBanner.setLayoutParams(topBannerParams);

        homeTabBgIv = findViewById(R.id.home_tab_bg_iv);

        //10个项目类型导航球
        homeNavigation = findViewById(R.id.home_navigation_638);
        homeHorizontalList = findViewById(R.id.home_horizontal_list);

        //metro top 1-6
        metroTopList = findViewById(R.id.metro_top_list);

        homeFiveMetroOne = findViewById(R.id.home_five_metro_one);
        homeFiveMetroTwo = findViewById(R.id.home_five_metro_two);
        fiveMetro[0] = findViewById(R.id.home_five_metro_iv1);
        fiveMetro[1] = findViewById(R.id.home_five_metro_iv2);
        fiveMetro[2] = findViewById(R.id.home_five_metro_iv3);
        fiveMetro[3] = findViewById(R.id.home_five_metro_iv4);
        fiveMetro[4] = findViewById(R.id.home_five_metro_iv5);

        bannerContainer = findViewById(R.id.home_bannerContainer);
        mHomeMtroBanner = findViewById(R.id.home_five_metro_banner);
        mTitleTxt = findViewById(R.id.banner_title);
        mSubTitleTxt = findViewById(R.id.banner_subtitle);
        mPriceTxt = findViewById(R.id.banner_price);
        mBannerBtn = findViewById(R.id.banner_btn);

        //新人特价
        newuserMetroClick = findViewById(R.id.newuser_metro_click);
        newuserMetroViewPager = findViewById(R.id.home_viewpager);
        newuserMetroMoney = findViewById(R.id.newuser_metro_money);

        //会员推广位
        pulsPromoteView = findViewById(R.id.puls_promote_view);
        pulsPromoteImg = findViewById(R.id.puls_promote_img);

        //帖子评论入口
        reviewEntryView = findViewById(R.id.review_entry_view);
        reviewEntryImg = findViewById(R.id.review_entry_img);

        //热门推荐
        ztIv[0] = findViewById(R.id.home_zt_iv1);
        ztIv[1] = findViewById(R.id.home_zt_iv2);
        ztIv[2] = findViewById(R.id.home_zt_iv3);
        zhuantiLy = findViewById(R.id.homepage_zhuanti_content_ly);
        ViewGroup.LayoutParams params1 = zhuantiLy.getLayoutParams();
        params1.height = ((windowsWight * 380) / 750);
        zhuantiLy.setLayoutParams(params1);

        //metro top 7-9
        metroBottomList = findViewById(R.id.metro_bottom_list);

        //精选好医
        hyZtIv[0] = findViewById(R.id.home_haoyi_iv1);
        hyZtIv[1] = findViewById(R.id.home_haoyi_iv2);
        hyZtIv[2] = findViewById(R.id.home_haoyi_iv3);
        hyZtContentLy = findViewById(R.id.homepage_haoyi_content_ly222);
        hyZtLy = findViewById(R.id.homepage_haoyi_content_ly);
        ViewGroup.LayoutParams paramshy = hyZtLy.getLayoutParams();
        paramshy.height = ((windowsWight * 380) / 750);
        hyZtLy.setLayoutParams(paramshy);

        //今日特卖
        saleList = findViewById(R.id.acty_sale_list);
        homeSale = findViewById(R.id.item_home_sale);

        //社区标题
        mBbsTital = findViewById(R.id.item_home_community);
        mBbsList = findViewById(R.id.acty_home_rvlist);

        //底部弹层
        mBottomTip = findViewById(R.id.home_bottom_tip);

        //新人礼包
        newUserBaoRly = findViewById(R.id.home_newlibao_rly);
        newUserIv = findViewById(R.id.new_libao_iv);
        mMessageColse = findViewById(R.id.newlibao_colse_iv);
        //首页浮层
        floatingRly = findViewById(R.id.home_floating_rly);
        floatingIv = findViewById(R.id.folating_iv);
        floatingColseIv = findViewById(R.id.folating_colse_iv);

        //首页优惠券
        homeCouposRly = findViewById(R.id.home_coupos_rly);
        homeCouposTime = findViewById(R.id.home_coupos_time);
        titleBackground.setAlpha(0.0f);

    }


    private final int PUSH_NEW = 111;
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case PUSH_NEW:
                    boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
                    if (!b) {
                        String pushNewPos = Cfg.loadStr(mContext, "pushNewPos", "");
                        if (TextUtils.isEmpty(pushNewPos)) {
                            PushNewDialog pushNewDialog = new PushNewDialog(mContext);
                            pushNewDialog.show();
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEW_SHOW_ALERT, "1", "0", "1"));
                            Cfg.saveStr(mContext, "pushNewPos", "1");
                        }
                    }
                    break;
                case HOME_TIP:
                    if (mBottomTip.getVisibility() == View.VISIBLE) {
                        mBottomTip.setVisibility(View.GONE);
                    }
                    break;
                case HOME_TITLE:
                    int pos = msg.arg1;
                    if (pos < titleLists.size()) {
                        searchEt.setText(titleLists.get(pos));
                        pos++;
                    } else {
                        pos = 0;
                    }
                    Message titleMsg = mHandler.obtainMessage(HOME_TITLE);
                    titleMsg.arg1 = pos;
                    mHandler.sendMessageDelayed(titleMsg, 10 * 1000);
                    break;
            }
        }
    };

    /**
     * 回调初始化
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initSetOnListener() {

        //导航栏点击置顶回调
        MainTableActivity.mainBottomBar.setOnHomeTopClickListener(new MainTableButtonView.OnHomeTopClickListener() {
            @Override
            public void onHomeTopClick(boolean isTabTop, HomeTopView tabHomeTop) {
                Log.e(TAG, "isTabTop == " + isTabTop);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMELOOKDIARY, isTabTop ? "1" : "2"));
                //修改展开或者折叠
                mAppBar.setExpanded(isTabTop, false);
                if (isTabTop) {
                    for (YMBaseFragment fragment : fragmentList) {
                        HomeFragment homeFragment = (HomeFragment) fragment;
                        Log.e(TAG, "homeFragment === " + homeFragment);
                        RecyclerView recyclerView = homeFragment.getRecyclerView();
                        if (recyclerView != null) {
                            recyclerView.scrollToPosition(0);
                        }
                    }
                }
            }
        });

        //展开或者收缩
        mAppBar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.e(TAG, "state == " + state);
                switch (state) {
                    case IDLE:
                        //TabLayout部分露出/全部露出
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(false);
                        break;
                    case COLLAPSED:
                        //TabLayout完全隐藏
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAB_CEILING, "0"));
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(true);
                        break;
                    case EXPANDED:
                        //最大化展开
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(false);
                        break;
                }
            }
        });

        //滑动监听
        mAppBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (i != 0 && mBottomTip.getVisibility() == View.VISIBLE) {
                    mHandler.removeMessages(HOME_TIP);
                    mBottomTip.setVisibility(View.GONE);
                }
            }
        });

        //城市按钮点击
        citySelectRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent addIntent = new Intent(mContext, MainCitySelectActivity560.class);
                addIntent.putExtra("curcity", city);
                addIntent.putExtra("type", "2");
                if (null != MainTableActivity.mContext) {
                    MainTableActivity.mContext.startActivityForResult(addIntent, 1004);
                } else {
                    startActivityForResult(addIntent, 4);
                }

                Utils.tongjiApp(mContext, "home_city", "home", "home", "1");
            }
        });


        //搜索框点击按钮
        serachRly123.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }

                TCAgent.onEvent(mContext, "首页搜索", "首页搜索");
                Utils.tongjiApp(mContext, "home_search", "home", "home", "1");

                Intent it = new Intent(mContext, SearchAllActivity668.class);

                String key = searchEt.getText().toString();
                if (!TextUtils.isEmpty(key)) {
                    it.putExtra(SearchAllActivity668.KEYS, key);
                } else {
                    it.putExtra(SearchAllActivity668.KEYS, "");
                }
                it.putExtra(SearchAllActivity668.TYPE, "1");
                startActivity(it);
            }
        });

        //购物车点击
        saoerMaRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

        //下拉刷新
        homeRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mDialog.startLoading();
                loadHomeData();
                initSearhShowData();
            }
        });

        newUserIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", "6025");
                hashMap.put("to_page_type", "16");
                hashMap.put("to_page_id", "6025");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "yes"), hashMap);
                WebUrlTypeUtil.getInstance(mContext).urlToApp("https://m.yuemei.com/tao_zt/6025.html", "0", "0");
            }
        });
        mMessageColse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", "6025");
                hashMap.put("to_page_type", "16");
                hashMap.put("to_page_id", "6025");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "no"), hashMap);
                Cfg.saveStr(mContext, FinalConstant.NEWUSER_CLOSE, "close");
                newUserControl(false);
            }
        });

    }

    /**
     * 首页数据加载
     */
    private void loadHomeData() {
        Map<String, Object> maps = new HashMap<>();
        homeApi.getCallBack(mContext, maps, new BaseCallBackListener<HomeData623>() {

            @Override
            public void onSuccess(HomeData623 homeData623) {
                Log.e(TAG, "homeData623 === " + homeData623);
                //刷新隐藏
                homeRefresh.finishRefresh();
                homeData = homeData623;
                loadHomeDataToView();
                mCache.put(FinalConstant.HOMEJSON, homeApi.getmHomeData(), 60 * 60 * 24);
            }

        });
    }

    /**
     * 搜索框显示加载。
     */
    void initSearhShowData() {
        searchEntryApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                titleLists = new Gson().fromJson(data.data, new TypeToken<List<String>>() {
                }.getType());
                if (titleList.size() > 0) {
                    Message msg = mHandler.obtainMessage(HOME_TITLE);
                    msg.arg1 = 0;
                    mHandler.sendMessage(msg);
                } else {
                    searchEt.setText("搜索你感兴趣的项目");
                }
            }
        });
    }


    /**
     * 首页数据解析
     */
    void loadHomeDataToView() {
        if (hdlist != null) {
            hdlist.clear();
        }
        homeNav = null;
        if (homeTab != null) {
            homeTab.clear();
        }
        if (homeTag != null) {
            homeTag.clear();
        }
        if (metroTopData != null) {
            metroTopData.clear();
        }
        if (ztlist != null) {
            ztlist.clear();
        }

        if (metroBottomData != null) {
            metroBottomData.clear();
        }
        if (tuijlist != null) {
            tuijlist.clear();
        }
        if (hotData != null) {
            hotData.clear();
        }
        if (bbsData != null) {
            bbsData.clear();
        }
        if (boardlist != null) {
            boardlist.clear();
        }
        if (tuijshare != null) {
            tuijshare.clear();
        }

        if (homeCard != null) {
            homeCard.clear();
        }

        homeNav = homeData.getHomeNav();                //10个tab导航球实体类

        mNewZt = homeData.getNew_zt();

        mDialog.stopLoading();

        //幻灯片
        homeSlide();

        //10个导航球
        setTenTab();

        //栏目5卡
        setColumnFiveData();

        //5个手工位
        setFiveMetro();

        //metroTop 1-6
        metroTop();


        //新人专享
        newuserMetro();
        //会员推广位
        pulsPromote();

        //帖子评论入口
        reviewEntry();

        //热门推荐
        hotRecommended();

        //metroBottom 7-9
        metroBottom();

        //医生推荐
        doctorRecommended();

        //社区标题
        communityTitle();

        //今日特卖
        saleDay();

        //首页调用日记
        diaryList();

        //首页悬浮
        setHomeFloat();

        showHomeCouponsPop();
    }

    private void showHomeCouponsPop() {
        List<NewZtCouponsBean> newZtCoupons = homeData.getNewZtCoupons();
        if (CollectionUtils.isNotEmpty(newZtCoupons)) {
            String newZtcoupons = Cfg.loadStr(mContext, "new_ztcoupons", "");
            if (TextUtils.isEmpty(newZtcoupons)) {
                Cfg.saveStr(mContext, "new_ztcoupons", "1");

            } else {
                String ad_imgurl = Cfg.loadStr(mContext, "ad_imgurl", "");
                if (TextUtils.isEmpty(ad_imgurl)) {
                    NewuserConponsPop newuserConponsPop = new NewuserConponsPop(mContext, newZtCoupons);
                    newuserConponsPop.showAtLocation(home625Conent, Gravity.CENTER, 0, 0);
                }
            }

        }
    }

    private int pos = 0;

    private void setFiveMetro() {
        if (lanmu2 != null) lanmu2.clear();
        if (lanmu3 != null) lanmu3.clear();
        lanmu2 = homeData.getLanmu2();
        lanmu3 = homeData.getLanmu3();
        if (lanmu2 != null && lanmu2.size() > 0) {
            homeFiveMetroOne.setVisibility(View.VISIBLE);
            for (int i = 0; i < lanmu2.size(); i++) {
                if (i == 0) {
                    final List<LanmuTao> lanmuTaoList = lanmu2.get(0).getTao();
                    Glide.with(mContext)
                            .load(lanmu2.get(i).getImg())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.LEFT))
                            .into(fiveMetro[i]);
                    if (lanmuTaoList != null && !lanmuTaoList.isEmpty()) {
                        bannerContainer.setVisibility(View.VISIBLE);
                        ArrayList<String> imgurl = new ArrayList<>();
                        for (int j = 0; j < lanmuTaoList.size(); j++) {
                            imgurl.add(lanmuTaoList.get(j).getImg());
                        }
                        mHomeMtroBanner.setImagesData(imgurl);
                        mTitleTxt.setText(lanmuTaoList.get(0).getTitle());
                        mSubTitleTxt.setText(lanmuTaoList.get(0).getSubtitle());
                        mPriceTxt.setText(lanmuTaoList.get(0).getPrice());
                        bannerContainer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(lanmuTaoList.get(pos).getUrl());
                            }
                        });
                        mHomeMtroBanner.setOnBannerListener(new OnBannerListener() {
                            @Override
                            public void OnBannerClick(int position) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(lanmuTaoList.get(pos).getUrl());
                            }
                        });
                        mBannerBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(lanmuTaoList.get(pos).getUrl());
                            }
                        });
                        mHomeMtroBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int i, float v, int i1) {

                            }

                            @Override
                            public void onPageSelected(int i) {
                                pos = i;
                                mTitleTxt.setText(lanmuTaoList.get(i).getTitle());
                                mSubTitleTxt.setText(lanmuTaoList.get(i).getSubtitle());
                                mPriceTxt.setText(lanmuTaoList.get(i).getPrice());
                            }

                            @Override
                            public void onPageScrollStateChanged(int i) {

                            }
                        });


                    } else {
                        bannerContainer.setVisibility(View.GONE);
                    }

                } else if (i == 1) {
                    Glide.with(mContext)
                            .load(lanmu2.get(i).getImg())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.RIGHT))
                            .into(fiveMetro[i]);
                }

                final String url = lanmu2.get(i).getUrl();
                final HashMap<String, String> event_params = lanmu2.get(i).getEvent_params();
                final int finalI = i;
                fiveMetro[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_COLUMN2, (finalI + 1) + ""), event_params);

                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                });
            }
        } else {
            homeFiveMetroOne.setVisibility(View.GONE);
        }

        if (lanmu3 != null && lanmu3.size() > 0) {
            homeFiveMetroTwo.setVisibility(View.VISIBLE);
            for (int i = 0; i < lanmu3.size(); i++) {
                if (i == 0) {
                    Glide.with(mContext)
                            .load(lanmu3.get(i).getImg())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.LEFT))
                            .into(fiveMetro[i + 2]);
                } else if (i == 2) {
                    Glide.with(mContext)
                            .load(lanmu3.get(i).getImg())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.RIGHT))
                            .into(fiveMetro[i + 2]);
                } else {
                    Glide.with(mContext).load(lanmu3.get(i).getImg()).into(fiveMetro[i + 2]);
                }
                final String url = lanmu3.get(i).getUrl();
                final HashMap<String, String> event_params = lanmu3.get(i).getEvent_params();
                final int finalI = i;
                fiveMetro[i + 2].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_COLUMN3, (finalI + 1) + ""), event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                });
            }
        } else {
            homeFiveMetroTwo.setVisibility(View.GONE);
        }
    }

    private void newuserMetro() {
        if (mNewZt != null) {
            List<TaoData> taodata = mNewZt.getTaodata();
            if (taodata != null && taodata.size() > 0) {
                newuserMetroClick.setVisibility(View.VISIBLE);
                newuserMetroViewPager.init(mContext, mNewZt.getTaodata(), 4, ScrollerViewpager.split(taodata,2) ,new OnViewpagerChangeListener() {
                    @Override
                    public void onChange(int currentPage) {

                    }
                });
                setViewPagerScrollSpeed(newuserMetroViewPager);
                newuserMetroClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mNewZt.getUrl());
                    }
                });

                newuserMetroMoney.setText(mNewZt.getCoupons().getMoney());
            } else {
                newuserMetroClick.setVisibility(View.GONE);
            }

        }
    }


    /**
     * 设置ViewPager切换速度
     */
    private void setViewPagerScrollSpeed(ViewPager pager) {
        ScrollerViewpager.ViewPagerScroller scroller = new ScrollerViewpager.ViewPagerScroller(mContext);
        scroller.setScrollDuration(1500);
        scroller.initViewPagerScroll(pager);//这个是设置切换过渡时间为2秒
    }


    private void setHomeFloat() {
        //弹层
        final HomeButtomFloat buttomFloat = homeData.getButtomFloat();
        if (buttomFloat != null) {
            Log.e(TAG, "buttomFloat.getTitle() == " + buttomFloat.getTitle());
            if (!TextUtils.isEmpty(buttomFloat.getTitle())) {
                setBottomTipShow(buttomFloat);
            } else {
                mBottomTip.setVisibility(View.GONE);
            }
        } else {
            mBottomTip.setVisibility(View.GONE);
        }

        //大促指定浮窗->新人红包浮窗->优惠券倒计时浮层
        mFloating = homeData.getFloating();              //首页悬浮
        String img = mFloating.getImg();
        url = mFloating.getUrl();
        String id = mFloating.getId();
        event_params = mFloating.getEvent_params();
        //大促浮层
        if (!TextUtils.isEmpty(img) && !TextUtils.isEmpty(url)) {
            String isClose = Cfg.loadStr(mContext, FinalConstant.DACU_FLOAT_CLOAS, "");
            if (TextUtils.isEmpty(isClose)) {
                dacuFloatingControl(true, img, event_params);
                newUserControl(false);
                couponsFloatControl(false, null);
            } else {
                dacuFloatingControl(false, img, event_params);
            }

        } else {
            //新人红包
            dacuFloatingControl(false, "", new HashMap<String, String>());
            String is_show = Cfg.loadStr(mContext, FinalConstant.ISSHOW, "1");
            if ("1".equals(is_show)) {
                String isClose = Cfg.loadStr(mContext, FinalConstant.NEWUSER_CLOSE, "");
                if (TextUtils.isEmpty(isClose)) {
                    newUserControl(true);
                    couponsFloatControl(false, null);
                } else {
                    newUserControl(false);
                }

            } else {
                //优惠券倒计时浮层
                newUserControl(false);
                coupons = homeData.getCoupons();
                String coupons_id = coupons.getCoupons_id();
                String end_time = coupons.getEnd_time();
                couponsUrl = coupons.getUrl();
                String endTimeAct = Utils.stampToPhpDate(end_time);
                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTime = simpleDateFormat.format(date);
                long[] timeSubCurr = Utils.getTimeSub(currentTime, endTimeAct);
                long[] timesAct = {timeSubCurr[0], timeSubCurr[1], timeSubCurr[2], timeSubCurr[3]};

                int couponsId = Integer.parseInt(coupons_id);
                if (couponsId > 0) {
                    if (timeSubCurr[0] == 0) {
                        couponsFloatControl(true, timesAct);
                    }
                } else {
                    couponsFloatControl(false, null);
                }
            }

        }
        floatingRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "1", "0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "yes"), event_params);
            }
        });
        floatingColseIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingRly.setVisibility(View.GONE);
                Cfg.saveStr(mContext, FinalConstant.DACU_FLOAT_CLOAS, "close");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "no"), event_params);
            }
        });
        homeCouposRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMECOUPONS, "click"), new ActivityTypeData("1"));
                WebUrlTypeUtil.getInstance(mContext).urlToApp(couponsUrl, "1", "0");
            }
        });

    }


    /**
     * 新人红包显示或隐藏
     *
     * @param isShow true 显示 flase 隐藏
     */
    private void newUserControl(boolean isShow) {
        if (isShow) {
            newUserBaoRly.setVisibility(View.VISIBLE);

            Glide.with(mContext)
                    .load(R.drawable.newuser_rightredback)
                    .asGif()
                    .into(newUserIv);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("id", "6025");
            hashMap.put("to_page_type", "16");
            hashMap.put("to_page_id", "6025");
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "show"), hashMap);
        } else {
            newUserBaoRly.setVisibility(View.GONE);
        }

    }

    /**
     * 大促浮层显示或隐藏
     *
     * @param isShow true 显示 flase 隐藏
     */
    private void dacuFloatingControl(boolean isShow, String url, HashMap<String, String> event_params) {
        if (isShow) {
            floatingRly.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(url).into(floatingIv);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "show"), event_params);
        } else {
            floatingRly.setVisibility(View.GONE);
        }
    }

    /**
     * 优惠券倒计时浮层
     *
     * @param isShow   true 显示
     * @param timesAct 时间
     */

    private void couponsFloatControl(boolean isShow, long[] timesAct) {
        if (isShow) {
            homeCouposRly.setVisibility(View.VISIBLE);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMECOUPONS, "show"), new ActivityTypeData("1"));
            homeCouposTime.setTimes(timesAct);
            homeCouposTime.setCopywriting("");
            if (!homeCouposTime.isRun()) {
                homeCouposTime.beginRun();
            }
            homeCouposTime.setTimeCallBack(new TimerTextView.TimeCallBack() {
                @Override
                public void timeCallBack() {
                    couponsFloatControl(false, null);
                }
            });
        } else {
            if (homeCouposTime.isRun()) {
                homeCouposTime.stopRun();
            }
            homeCouposRly.setVisibility(View.GONE);
        }

    }

    /**
     * 幻灯片配置
     */
    private void homeSlide() {
        hdlist = homeData.getHuandeng();                //幻灯片实体类
        urlImageList = null;
        txtViewpager = null;
        if (hdlist == null) return;
        int hdsize = hdlist.size();
        urlImageList = new ArrayList<>();
        txtViewpager = new ArrayList<>();

        for (int i = 0; i < hdsize; i++) {
            Log.e(TAG, "urlImageList == " + urlImageList);
            Log.e(TAG, "hdlist == " + hdlist);
            Log.e(TAG, "hdlist.get(i) == " + hdlist.get(i));
            Log.e(TAG, "hdlist.get(i) == " + hdlist.get(i).getImg());
            urlImageList.add(hdlist.get(i).getImg());
            txtViewpager.add(i + "1");
        }

        if (null != hdlist && hdlist.size() > 0) {
            mTopBanner.setImagesData(urlImageList);
            mTopBanner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int curIndex) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }

                    //自己的统计
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|banner|" + (curIndex + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, hdlist.get(curIndex).getBmsid(), "1"), hdlist.get(curIndex).getEvent_params());

                    //百度统计
                    baiduTongJi("060", "首页焦点图");

                    //GrowingIO统计
                    Integer curIndex11 = curIndex + 1;
                    if (curIndex11.toString().length() == 1) {
                        StatisticalManage.getInstance().growingIO("Focus_graph0" + curIndex + 1);
                    } else {
                        StatisticalManage.getInstance().growingIO("Focus_graph" + curIndex + 1);
                    }

                    String ss = hdlist.get(curIndex).getUrl();

                    Log.e(TAG, "url=====" + ss);
                    Log.e(TAG, "type=====" + hdlist.get(curIndex).getType());

                    if ("1".equals(hdlist.get(curIndex).getType())) {// 帖子

                        String id = hdlist.get(curIndex).getQid();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "帖子 id:" + id, kv);

                        String url = hdlist.get(curIndex).getUrl();
                        Intent it2 = new Intent(mContext, DiariesAndPostsActivity.class);
                        it2.putExtra("url", url);
                        it2.putExtra("qid", id);
                        startActivity(it2);
                    } else if (hdlist.get(curIndex)
                            .getType().equals("1000")) {// 专题
                        String url = hdlist.get(
                                curIndex).getUrl();
                        String title = hdlist.get(
                                curIndex).getTitle();
                        String ztid = hdlist.get(
                                curIndex).getQid();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "快速专题 id:" + ztid, kv);

                        Utils.ztrecordHttp(mContext, "FrontBanner" + ztid);

                        Intent it1 = new Intent();
                        it1.setClass(mContext, ZhuanTiWebActivity.class);
                        it1.putExtra("url", url);
                        it1.putExtra("title", title);
                        it1.putExtra("ztid", ztid);
                        startActivity(it1);

                    } else if ("999".equals(hdlist.get(curIndex).getType())) {// 悦美页
                        String url = hdlist.get(
                                curIndex).getUrl();
                        String sharePic = hdlist.get(
                                curIndex).getImg();
                        String shareTitle = hdlist.get(
                                curIndex).getTitle();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "999 url:" + url, kv);

                        Log.e(TAG, "url=====" + url);

                        Intent it1 = new Intent();
                        it1.setClass(
                                mContext,
                                SlidePicTitieWebActivity.class);
                        it1.putExtra("url", url);
                        it1.putExtra("shareTitle",
                                shareTitle);
                        it1.putExtra("sharePic",
                                sharePic);
                        startActivity(it1);


                    } else if ("418".equals(hdlist.get(curIndex)
                            .getType())) {// 淘整形

                        String id = hdlist.get(curIndex).getQid();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "淘整形 id:" + id, kv);

                        Intent it1 = new Intent();
                        it1.putExtra("id", id);
                        it1.putExtra("source", "3");
                        it1.putExtra("objid", id);
                        it1.setClass(mContext, TaoDetailActivity.class);
                        startActivity(it1);
                    } else if ("5701".equals(hdlist.get(curIndex)
                            .getType())) {//专家医生

                        String docid = hdlist
                                .get(curIndex).getQid();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "医生主页 id:" + docid, kv);

                        Intent it0 = new Intent();
                        it0.putExtra("docId", docid);
                        it0.putExtra("docName", "");
                        it0.putExtra("partId", "0");
                        it0.setClass(
                                mContext,
                                DoctorDetailsActivity592.class);
                        startActivity(it0);

                    } else if ("6074".equals(hdlist.get(curIndex).getType())) {

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "直播", kv);

                        Intent it = new Intent();
                        it.setClass(mContext, WebUrlTitleActivity.class);
                        it.putExtra("link", "/live/list/");
                        it.putExtra("title", "直播列表");
                        startActivity(it);

                    } else if ("6061".equals(hdlist.get(curIndex)
                            .getType())) {//看直播

                        String relatedId = hdlist
                                .get(curIndex).getQid();
                        String flag = hdlist.get(curIndex).getFlag();

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "直播 id:" + relatedId, kv);

                        try {

                            String fake_token = Cfg.loadStr(mContext, "hj_token", "");

                            if (!"0".equals(Utils.getUid())) {

                                if (fake_token.length() > 0) {
                                    Bundle bundle = new Bundle();
                                    bundle.putInt("type", Integer.parseInt(flag));
                                    bundle.putString("sn", "");
                                    bundle.putString("liveId", relatedId);
                                    bundle.putString("replayId", relatedId);
                                    bundle.putString("background", "");
                                    bundle.putString("channel", "");
                                    bundle.putString("usign", "");

                                }

                            } else {
                                Utils.jumpLogin(mContext);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (hdlist.get(curIndex).getType().equals("0") || hdlist.get(curIndex).getType().equals("") || null == hdlist.get(curIndex).getType()) {

                        Map kv = new HashMap();
                        kv.put("位置", curIndex);
                        kv.put("地区", city);
                        TCAgent.onEvent(mContext, "首页焦点图", "url:" + hdlist.get(curIndex).getUrl(), kv);

                        WebUrlTypeUtil.getInstance(mContext).urlToApp(hdlist.get(curIndex).getUrl(), "1", hdlist.get(curIndex).getQid());

                    } else if ("6491".equals(hdlist.get(curIndex).getType())) {
                        String url6491 = FinalConstant1.HTTPS + FinalConstant1.SYMBOL1 + FinalConstant1.BASE_URL + hdlist.get(curIndex).getUrl();
                        WebUtil.getInstance().startWebActivity(mContext, url6491);
                    }
                }
            });

        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    /**
     * 10个导航球
     */
    private void setTenTab() {
        final HomeNav homeNav = homeData.getNav();
        final List<TaoTagItem> homeTag = homeNav.getTag();                     //10个项目类型导航球

        //背景设置
        final String hTabBg = homeNav.getBgImage();
        if (!TextUtils.isEmpty(hTabBg)) {
            Glide.with(mContext).load(hTabBg).into(homeTabBgIv);
        } else {
            homeTabBgIv.setBackgroundResource(R.color.white);
        }

        //项目类型导航球
        if (null != homeTag && homeTag.size() > 0) {
            homeNavigation.setVisibility(View.VISIBLE);

            //适配数据
            ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 5);
            gridLayoutManager.setScrollEnable(false);
            homeNavigation.setLayoutManager(gridLayoutManager);

            TagNavigationAdapter692 adapter = new TagNavigationAdapter692(mContext, homeTag, Utils.dip2px(37));
            homeNavigation.setAdapter(adapter);
            adapter.setOnEventClickListener(new TagNavigationAdapter692.OnEventClickListener() {
                @Override
                public void onItemClick(int pos, TaoTagItem taoTagItem) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_NAV, (pos + 6) + "", "home", "1"));

                    String one_id = taoTagItem.getOne_id();
                    String two_id = taoTagItem.getTwo_id();
                    String three_id = taoTagItem.getThree_id();
                    String homesource = taoTagItem.getHomeSource();
                    String isAll = taoTagItem.getIsAll();

                    if ("1".equals(isAll)) {
                        Intent intent = new Intent(mContext, AllProjectActivity.class);
                        intent.putExtra("home_source",homesource);
                        startActivity(intent);
                    } else {
                        switch (taoTagItem.getLevel()) {
                            case "2":
                                //部位
                                Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                                intent2.putExtra("id", one_id);
                                intent2.putExtra("title", taoTagItem.getChannel_title());
                                intent2.putExtra("home_source", homesource);
                                startActivity(intent2);
                                break;
                            case "3":
                                //二级
                                Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString(ChannelTwoActivity.TITLE, taoTagItem.getChannel_title());
                                bundle.putString(ChannelTwoActivity.TWO_ID, two_id);
                                bundle.putString(ChannelTwoActivity.HOME_SOURCE, homesource);
                                intent3.putExtra("data", bundle);
                                startActivity(intent3);
                                break;
                            case "4":
                                //四级
                                Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                                intent4.putExtra("id", three_id);
                                intent4.putExtra("title", taoTagItem.getChannel_title());
                                intent4.putExtra("home_source", homesource);
                                startActivity(intent4);
                                break;
                        }
                    }

                }
            });

        } else {
            homeNavigation.setVisibility(View.GONE);
        }
    }

    /**
     * 栏目5卡
     */
    private void setColumnFiveData() {
        //横向导航
        final List<CardBean> homeCard = homeData.getKapian();
        if (homeCard != null && homeCard.size() > 0) {
            homeHorizontalList.setVisibility(View.VISIBLE);
            for (int i = 0; i < homeCard.size(); i++) {
                homeCard.get(i).setColor(ContextCompat.getColor(mContext, homeCardColor[i % homeCardColor.length]));
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
            HomeHorizontlaAdapter homeHorizontlaAdapter = new HomeHorizontlaAdapter(R.layout.home_horizontal_list_item, homeCard);
            homeHorizontalList.setFocusable(false);//解决卡顿
            homeHorizontalList.setLayoutManager(linearLayoutManager);
            homeHorizontalList.setAdapter(homeHorizontlaAdapter);
            homeHorizontlaAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                    //百度统计
                    Utils.baiduTongJi(mContext, "061", homeCard.get(position).getTitle());

                    String type = homeCard.get(position).getType();
                    String qid = homeCard.get(position).getQid();
                    final String url = homeCard.get(position).getUrl();
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("id", type);
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_CARD, (position + 1) + ""), hashMap);

                    Log.e(TAG, "type == " + type);
                    Log.e(TAG, "url == " + url);
                    if (!TextUtils.isEmpty(url) && url.startsWith("http")) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    } else {
                        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(qid)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:link:eq:" + url + ":and:id:eq:" + qid);
                        } else if (!TextUtils.isEmpty(url)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:link:eq:" + url);
                        } else if (!TextUtils.isEmpty(qid)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:id:eq:" + qid);
                        }
                    }
                }
            });
        } else {
            homeHorizontalList.setVisibility(View.GONE);
        }
    }

    /**
     * metroTop数据配置
     */
    private void metroTop() {
        metroTopData = homeData.getMetro_top();         //metroTop 实体类
        Log.e(TAG, "metroTopData == " + metroTopData.size());
        if (null != metroTopData && metroTopData.size() > 0) {
            //先清空
            metroTopList.setVisibility(View.VISIBLE);
            metroTopList.removeAllViews();
            //添加其他item
            for (int i = 0; i < metroTopData.size(); i++) {
                View childView = null;
                //内部是否有分割线
                boolean lineHave = true;

                switch (metroTopData.get(i).size()) {
                    case 1:                                                 //一排一个的(高)

                        childView = inflater.inflate(R.layout.metro_all_one, metroTopList, false);
                        ImageView merroTopOne = childView.findViewById(R.id.merro_top_one_high_img);

                        //设置宽高比
                        ViewGroup.LayoutParams paramsOne = merroTopOne.getLayoutParams();
                        paramsOne.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        float h_w = Float.parseFloat(metroTopData.get(i).get(0).getH_w());
                        paramsOne.height = (int) (windowsWight * h_w);
                        merroTopOne.setLayoutParams(paramsOne);

                        Log.e(TAG, "top----> " + metroTopData.get(i).get(0).getImg());
                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(merroTopOne);

                        //判断是否有分割线
                        lineHave = "1".equals(metroTopData.get(i).get(0).getMetro_line());

                        final int metroTopPos1 = i;
                        merroTopOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                baiduTongJi("062", metroTopPos1 + "-" + 1);
                                if (null != metroTopData && metroTopData.size() > 0)
                                    Log.d(TAG, "=========>" + metroTopData.get(metroTopPos1).get(0).getUrl());
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos1).get(0).getUrl(), "1", "0");
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos1 + 1) + "-" + 1), metroTopData.get(metroTopPos1).get(0).getEvent_params());
                            }
                        });

                        break;
                    case 2:                //一排两个的
                        childView = inflater.inflate(R.layout.metro_all_two, metroTopList, false);
                        ImageView merroTopTwo1 = childView.findViewById(R.id.merro_two_img1);
                        ImageView merroTopTwo2 = childView.findViewById(R.id.merro_two_img2);
                        View merroTowDivder = childView.findViewById(R.id.merro_top_two_divider);

                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopTwo1);

                        Glide.with(mContext).load(metroTopData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopTwo2);


                        //判断是否有分割线
                        if ("1".equals(metroTopData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroTowDivder.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroTowDivder.setVisibility(View.GONE);
                        }

                        final int metroTopPos2 = i;
                        merroTopTwo1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos2 + 1) + "-" + 1), metroTopData.get(metroTopPos2).get(0).getEvent_params());
                                baiduTongJi("062", metroTopPos2 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos2).get(0).getUrl(), "1", "0");

                            }
                        });

                        merroTopTwo2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos2 + 1) + "-" + 2), metroTopData.get(metroTopPos2).get(1).getEvent_params());
                                baiduTongJi("062", metroTopPos2 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos2).get(1).getUrl(), "1", "0");
                            }
                        });


                        break;
                    case 4:               //一排四个的
                        childView = inflater.inflate(R.layout.metro_all_four, metroTopList, false);
                        ImageView merroTopFour1 = childView.findViewById(R.id.merro_four_img1);
                        ImageView merroTopFour2 = childView.findViewById(R.id.merro_four_img2);
                        ImageView merroTopFour3 = childView.findViewById(R.id.merro_four_img3);
                        ImageView merroTopFour4 = childView.findViewById(R.id.merro_four_img4);
                        View merroFourDivder1 = childView.findViewById(R.id.merro_top_four_divider1);
                        View merroFourDivder2 = childView.findViewById(R.id.merro_top_four_divider2);
                        View merroFourDivder3 = childView.findViewById(R.id.merro_top_four_divider3);

                        Glide.with(mContext).load(metroTopData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour1);
                        Glide.with(mContext).load(metroTopData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour2);
                        Glide.with(mContext).load(metroTopData.get(i).get(2).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour3);
                        Glide.with(mContext).load(metroTopData.get(i).get(3).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroTopFour4);

                        //判断是否有分割线
                        if ("1".equals(metroTopData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroFourDivder1.setVisibility(View.VISIBLE);
                            merroFourDivder2.setVisibility(View.VISIBLE);
                            merroFourDivder3.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroFourDivder1.setVisibility(View.GONE);
                            merroFourDivder2.setVisibility(View.GONE);
                            merroFourDivder3.setVisibility(View.GONE);
                        }

                        final int metroTopPos4 = i;
                        merroTopFour1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos4 + 1) + "-" + 1), metroTopData.get(metroTopPos4).get(0).getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos4).get(0).getUrl(), "1", "0");
                            }
                        });

                        merroTopFour2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos4 + 1) + "-" + 2), metroTopData.get(metroTopPos4).get(1).getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos4).get(1).getUrl(), "1", "0");
                            }
                        });
                        merroTopFour3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos4 + 1) + "-" + 3), metroTopData.get(metroTopPos4).get(2).getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 3);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos4).get(2).getUrl(), "1", "0");
                            }
                        });

                        merroTopFour4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_METRO_TOP, (metroTopPos4 + 1) + "-" + 4), metroTopData.get(metroTopPos4).get(3).getEvent_params());
                                baiduTongJi("062", metroTopPos4 + "-" + 4);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroTopData.get(metroTopPos4).get(3).getUrl(), "1", "0");
                            }
                        });

                        break;
                }

                if (childView != null) {
                    metroTopList.addView(childView);
                    if (lineHave && i != metroTopData.size() - 1) {   //有分割线(而且不是最后一个)
                        addDivider1(1, "#efefef", metroTopList);
                    }
                }
            }
        } else {
            metroTopList.removeAllViews();
            metroTopList.setVisibility(View.GONE);
        }
    }

    /**
     * 会员推广位
     */
    private void pulsPromote() {
        mPlusData = homeData.getPlus();                 //会员推实体类
        if (mPlusData != null) {
            if (!TextUtils.isEmpty(mPlusData.getImg())) {
                pulsPromoteView.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(mPlusData.getImg())
                        .placeholder(R.drawable.home_other_placeholder2)
                        .error(R.drawable.home_other_placeholder2)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();

                                ViewGroup.LayoutParams layoutParams = pulsPromoteImg.getLayoutParams();
                                layoutParams.height = (windowsWight * intrinsicHeight) / intrinsicWidth;

                                pulsPromoteImg.setImageDrawable(resource);
                            }
                        });
                //会员点击
                pulsPromoteImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Utils.isLoginAndBind(mContext)) {
                            Log.e(TAG, "mPlusData.getUrl() == " + mPlusData.getUrl());
                            String urlStr = "type:eq:" + mPlusData.getType() + ":and:link:eq:" + mPlusData.getUrl();
                            Utils.tongjiApp(mContext, "home_plus", "1", "0", "9");
                            webViewClientManager.showWebDetail(urlStr);
                        }
                    }
                });

            } else {
                pulsPromoteView.setVisibility(View.GONE);
            }
        } else {
            pulsPromoteView.setVisibility(View.GONE);
        }
    }

    /**
     * 帖子评论入口
     */
    private void reviewEntry() {
        mAskEntry = homeData.getAsk_entry();            //提问入口
        if (mAskEntry != null) {
            if (!TextUtils.isEmpty(mAskEntry.getImg())) {
                reviewEntryView.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(mAskEntry.getImg())
                        .placeholder(R.drawable.home_other_placeholder2)
                        .error(R.drawable.home_other_placeholder2)
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();

                                ViewGroup.LayoutParams layoutParams = reviewEntryImg.getLayoutParams();
                                layoutParams.height = (windowsWight * intrinsicHeight) / intrinsicWidth;

                                reviewEntryImg.setImageDrawable(resource);
                            }
                        });
                //帖子评论点击
                reviewEntryImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Utils.isLoginAndBind(mContext)) {
                            if (!TextUtils.isEmpty(mAskEntry.getUrl())) {
                                Log.e(TAG, "mAskEntry.getUrl() == " + mAskEntry.getUrl());

                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.ASK_ENTRY, "home"));

                                webViewClientManager.showWebDetail(mAskEntry.getUrl());
                            }
                        }
                    }
                });

            } else {
                reviewEntryView.setVisibility(View.GONE);
            }
        } else {
            reviewEntryView.setVisibility(View.GONE);
        }
    }

    /**
     * 热门推荐
     */
    private void hotRecommended() {
        ztlist = homeData.getZt();                      //热门推荐 实体类
        Log.e(TAG, "ztlist == " + ztlist.size());
        if (null != ztlist && ztlist.size() > 0) {
            zhuantiLy.setVisibility(View.VISIBLE);

            for (int i = 0; i < ztlist.size(); i++) {

                Log.e(TAG, "ztIv[i] == " + ztIv[i]);

                final int j = i;
                if (i == 0) {

                    Glide.with(mContext).load(ztlist.get(i).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(ztIv[i]);
                } else {

                    Glide.with(mContext).load(ztlist.get(i).getImg()).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(ztIv[i]);

                }

                ztIv[i].setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        mCurZt = j;

                        Utils.tongjiApp(mContext, "home_3k_top", (mCurZt + 1) + "", "home", "1");
                        baiduTongJi("063", (mCurZt + 1) + "");

                        Map kv = new HashMap();
                        kv.put("位置", mCurZt);
                        kv.put("地区", city);

                        if (ztlist.get(mCurZt).getType().equals("1")) {// 帖子
                            String id = ztlist.get(mCurZt).getQid();
                            String url = ztlist.get(mCurZt).getUrl();
                            String appmurl = ztlist.get(mCurZt).getAppmurl();
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "1", "0");

                            TCAgent.onEvent(mContext, "首页热门专题", "帖子 id:" + id, kv);
                        } else if (ztlist.get(mCurZt).getType().equals("1000")) {// 专题
                            String url = ztlist.get(mCurZt).getUrl();
                            String title = ztlist.get(mCurZt).getTitle();
                            String ztid = ztlist.get(mCurZt).getQid();

                            Utils.ztrecordHttp(mContext, "FrontTopic" + ztid);
                            TCAgent.onEvent(mContext, "首页热门专题", "快速专题 id:" + ztid, kv);

                            Intent it1 = new Intent();
                            it1.setClass(mContext, ZhuanTiWebActivity.class);
                            it1.putExtra("url", url);
                            it1.putExtra("title", title);
                            it1.putExtra("ztid", ztid);
                            startActivity(it1);
                        } else if (ztlist.get(mCurZt).getType().equals("999")) {// 悦美页
                            String url = ztlist.get(mCurZt).getUrl();
                            String sharePic = ztlist.get(mCurZt).getImg();
                            String shareTitle = ztlist.get(mCurZt).getTitle();

                            TCAgent.onEvent(mContext, "首页热门专题", "999 url:" + url, kv);

                            Intent it1 = new Intent();
                            it1.setClass(mContext, SlidePicTitieWebActivity.class);
                            it1.putExtra("url", url);
                            it1.putExtra("shareTitle", shareTitle);
                            it1.putExtra("sharePic", sharePic);
                            startActivity(it1);
                        } else if (ztlist.get(mCurZt).getType().equals("418")) {// 淘整形
                            String id = ztlist.get(mCurZt).getQid();

                            TCAgent.onEvent(mContext, "首页热门专题", "淘整形 id:" + id, kv);

                            Intent it1 = new Intent();
                            it1.putExtra("id", id);
                            it1.putExtra("source", "1");
                            it1.putExtra("objid", "0");
                            it1.setClass(mContext, TaoDetailActivity.class);
                            startActivity(it1);
                        } else if (ztlist.get(mCurZt).getType().equals("5701")) {
                            String docId = ztlist.get(mCurZt).getQid();

                            TCAgent.onEvent(mContext, "首页热门专题", "医生主页 id:" + docId, kv);

                            Intent it0 = new Intent();
                            it0.putExtra("docId", docId);
                            it0.putExtra("docName", "");
                            it0.putExtra("partId", "0");
                            it0.setClass(mContext, DoctorDetailsActivity592.class);
                            startActivity(it0);
                        } else if (ztlist.get(mCurZt).getType().equals("6061")) {//看直播

                            String relatedId = ztlist.get(mCurZt).getQid();
                            String flag = ztlist.get(mCurZt).getFlag();

                            TCAgent.onEvent(mContext, "首页热门专题", "直播 id:" + relatedId, kv);

                            try {
                                String fake_token = Cfg.loadStr(mContext, "hj_token", "");

                                if (!"0".equals(Utils.getUid())) {

                                    if (fake_token.length() > 0) {
                                        Bundle bundle = new Bundle();
                                        bundle.putInt("type", Integer.parseInt(flag));
                                        bundle.putString("sn", "");
                                        bundle.putString("liveId", relatedId);
                                        bundle.putString("replayId", relatedId);
                                        bundle.putString("background", "");
                                        bundle.putString("channel", "");
                                        bundle.putString("usign", "");
                                    }
                                } else {
                                    Utils.jumpLogin(mContext);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (ztlist.get(mCurZt).getType().equals("0") || ztlist.get(mCurZt).getType().equals("") || null == ztlist.get(mCurZt).getType()) {

                            TCAgent.onEvent(mContext, "首页热门专题", "url:" + ztlist.get(mCurZt).getUrl(), kv);

                            WebUrlTypeUtil.getInstance(mContext).urlToApp(ztlist.get(mCurZt).getUrl(), "1", ztlist.get(mCurZt).getQid());

                        }
                    }
                });
            }
        } else {
            zhuantiLy.setVisibility(View.GONE);
        }
    }

    /**
     * metroBottom数据配置
     */
    private void metroBottom() {
        metroBottomData = homeData.getMetro_buttom();   //metroBottom 实体类
        if (null != metroBottomData && metroBottomData.size() > 0) {
            //先清空
            metroBottomList.removeAllViews();

            //添加其他item
            for (int i = 0; i < metroBottomData.size(); i++) {
                View childView = null;

                //内部是否有分割线
                boolean lineHave = true;

                switch (metroBottomData.get(i).size()) {
                    case 1:                //一排一个的(高)
                        childView = inflater.inflate(R.layout.metro_all_one, metroBottomList, false);
                        ImageView merroBottomOne = childView.findViewById(R.id.merro_top_one_high_img);

                        //设置宽高比
                        ViewGroup.LayoutParams paramsOne = merroBottomOne.getLayoutParams();
                        paramsOne.width = ViewGroup.LayoutParams.MATCH_PARENT;
                        float h_w = Float.parseFloat(metroBottomData.get(i).get(0).getH_w());
                        paramsOne.height = (int) (windowsWight * h_w);
                        merroBottomOne.setLayoutParams(paramsOne);

                        Log.e(TAG, "metroBottomData.get(i).get(0).getImg() == " + metroBottomData.get(i).get(0).getImg());
                        Glide.with(mContext).load(metroBottomData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(merroBottomOne);


                        //判断是否有分割线
                        lineHave = "1".equals(metroBottomData.get(i).get(0).getMetro_line());

                        final int metroBottomPos1 = i;
                        merroBottomOne.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos1 + "-" + 1, "home", "1");
                                baiduTongJi("064", metroBottomPos1 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos1).get(0).getUrl(), "1", "0");
                            }
                        });

                        break;
                    case 2:                //一排两个的
                        childView = inflater.inflate(R.layout.metro_all_two, metroBottomList, false);
                        ImageView merroBottomTwo1 = childView.findViewById(R.id.merro_two_img1);
                        ImageView merroBottomTwo2 = childView.findViewById(R.id.merro_two_img2);
                        View merroTowDivder = childView.findViewById(R.id.merro_top_two_divider);

                        Glide.with(mContext).load(metroBottomData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomTwo1);

                        Glide.with(mContext).load(metroBottomData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomTwo2);

                        //判断是否有分割线
                        if ("1".equals(metroBottomData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroTowDivder.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroTowDivder.setVisibility(View.GONE);
                        }

                        final int metroBottomPos2 = i;
                        merroBottomTwo1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos2 + "-" + 1, "home", "1");
                                baiduTongJi("064", metroBottomPos2 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos2).get(0).getUrl(), "1", "0");
                            }
                        });

                        merroBottomTwo2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos2 + "-" + 2, "home", "1");
                                baiduTongJi("064", metroBottomPos2 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos2).get(1).getUrl(), "1", "0");
                            }
                        });

                        break;
                    case 4:               //一排四个的
                        childView = inflater.inflate(R.layout.metro_all_four, metroBottomList, false);
                        ImageView merroBottomFour1 = childView.findViewById(R.id.merro_four_img1);
                        ImageView merroBottomFour2 = childView.findViewById(R.id.merro_four_img2);
                        ImageView merroBottomFour3 = childView.findViewById(R.id.merro_four_img3);
                        ImageView merroBottomFour4 = childView.findViewById(R.id.merro_four_img4);
                        View merroFourDivder1 = childView.findViewById(R.id.merro_top_four_divider1);
                        View merroFourDivder2 = childView.findViewById(R.id.merro_top_four_divider2);
                        View merroFourDivder3 = childView.findViewById(R.id.merro_top_four_divider3);


                        Glide.with(mContext).load(metroBottomData.get(i).get(0).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomFour1);
                        Glide.with(mContext).load(metroBottomData.get(i).get(1).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomFour2);
                        Glide.with(mContext).load(metroBottomData.get(i).get(2).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomFour3);
                        Glide.with(mContext).load(metroBottomData.get(i).get(3).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(merroBottomFour4);

                        //判断是否有分割线
                        if ("1".equals(metroBottomData.get(i).get(0).getMetro_line())) {
                            lineHave = true;
                            merroFourDivder1.setVisibility(View.VISIBLE);
                            merroFourDivder2.setVisibility(View.VISIBLE);
                            merroFourDivder3.setVisibility(View.VISIBLE);
                        } else {
                            lineHave = false;
                            merroFourDivder1.setVisibility(View.GONE);
                            merroFourDivder2.setVisibility(View.GONE);
                            merroFourDivder3.setVisibility(View.GONE);
                        }

                        final int metroBottomPos4 = i;
                        merroBottomFour1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos4 + "-" + 1, "home", "1");
                                baiduTongJi("064", metroBottomPos4 + "-" + 1);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos4).get(0).getUrl(), "1", "0");
                            }
                        });

                        merroBottomFour2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos4 + "-" + 2, "home", "1");
                                baiduTongJi("064", metroBottomPos4 + "-" + 2);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos4).get(1).getUrl(), "1", "0");
                            }
                        });
                        merroBottomFour3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos4 + "-" + 3, "home", "1");
                                baiduTongJi("064", metroBottomPos4 + "-" + 3);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos4).get(2).getUrl(), "1", "0");
                            }
                        });

                        merroBottomFour4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Utils.tongjiApp(mContext, "home_metro_bottom", metroBottomPos4 + "-" + 4, "home", "1");
                                baiduTongJi("064", metroBottomPos4 + "-" + 4);
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(metroBottomData.get(metroBottomPos4).get(3).getUrl(), "1", "0");
                            }
                        });

                        break;
                }

                if (childView != null) {
                    metroBottomList.addView(childView);
                    if (lineHave && i != metroBottomData.size() - 1) {   //有分割线(而且不是最后一个)
                        addDivider1(1, "#efefef", metroBottomList);
                    }
                }
            }
        } else {
            metroBottomList.removeAllViews();
        }
    }

    /**
     * 医生推荐
     */
    private void doctorRecommended() {
        tuijlist = homeData.getTuij();                  //精选好医 实体类
        Log.e("66666", "doctorRecommended == " + tuijlist.size());
        if (null != tuijlist && tuijlist.size() > 0) {
            hyZtLy.setVisibility(View.VISIBLE);
            hyZtContentLy.setVisibility(View.VISIBLE);

            for (int i = 0; i < tuijlist.size(); i++) {
                final int j = i;
                if (i == 0) {

                    Glide.with(mContext).load(tuijlist.get(i).getImg()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(hyZtIv[i]);

                } else {

                    Glide.with(mContext).load(tuijlist.get(i).getImg()).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(hyZtIv[i]);
                }

                hyZtIv[i].setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (Utils.isFastDoubleClick()) {
                            return;
                        }
                        mCurtjZt = j;

                        Utils.tongjiApp(mContext, "home_3k_bottom", (mCurtjZt + 1) + "", "home", "1");
                        baiduTongJi("065", (mCurtjZt + 1) + "");

                        Map kv = new HashMap();
                        kv.put("位置", mCurtjZt);
                        kv.put("地区", city);

                        if (tuijlist.get(mCurtjZt).getType().equals("1")) {// 帖子

                            String id = tuijlist.get(mCurtjZt).getQid();
                            String url = tuijlist.get(mCurtjZt).getUrl();

                            TCAgent.onEvent(mContext, "首页精选好医", "帖子 id:" + id, kv);

                            Intent it2 = new Intent();
                            it2.putExtra("url", url);
                            it2.putExtra("qid", id);
                            it2.setClass(mContext, DiariesAndPostsActivity.class);
                            startActivity(it2);
                        } else if (tuijlist.get(mCurtjZt).getType().equals("1000")) {// 专题
                            String url = tuijlist.get(mCurtjZt).getUrl();
                            String title = tuijlist.get(mCurtjZt).getTitle();
                            String ztid = tuijlist.get(mCurtjZt).getQid();

                            TCAgent.onEvent(mContext, "首页精选好医", "快速专题 id:" + ztid, kv);
                            Utils.ztrecordHttp(mContext, "FrontTopic" + ztid);

                            Intent it1 = new Intent();
                            it1.setClass(mContext, ZhuanTiWebActivity.class);
                            it1.putExtra("url", url);
                            it1.putExtra("title", title);
                            it1.putExtra("ztid", ztid);
                            startActivity(it1);
                        } else if (tuijlist.get(mCurtjZt).getType().equals("999")) {// 悦美页
                            String url = tuijlist.get(mCurtjZt).getUrl();
                            String sharePic = tuijlist.get(mCurtjZt).getImg();
                            String shareTitle = tuijlist.get(mCurtjZt).getTitle();


                            TCAgent.onEvent(mContext, "首页精选好医", "999 url:" + url, kv);

                            Intent it1 = new Intent();
                            it1.setClass(mContext, SlidePicTitieWebActivity.class);
                            it1.putExtra("url", url);
                            it1.putExtra("shareTitle", shareTitle);
                            it1.putExtra("sharePic", sharePic);
                            startActivity(it1);
                        } else if (tuijlist.get(mCurtjZt).getType().equals("418")) {// 淘整形
                            String id = tuijlist.get(mCurtjZt).getQid();

                            TCAgent.onEvent(mContext, "首页精选好医", "淘整形 id:" + id, kv);

                            Intent it1 = new Intent();
                            it1.putExtra("id", id);
                            it1.putExtra("source", "1");
                            it1.putExtra("objid", "0");
                            it1.setClass(mContext, TaoDetailActivity.class);
                            startActivity(it1);
                        } else if (tuijlist.get(mCurtjZt).getType().equals("5701")) {
                            String docId = tuijlist.get(mCurtjZt).getQid();

                            TCAgent.onEvent(mContext, "首页精选好医", "医生主页 id:" + docId, kv);

                            Intent it0 = new Intent();
                            it0.putExtra("docId", docId);
                            it0.putExtra("docName", "");
                            it0.putExtra("partId", "0");
                            it0.setClass(mContext, DoctorDetailsActivity592.class);
                            startActivity(it0);
                        } else if (tuijlist.get(mCurtjZt).getType().equals("6061")) {//看直播

                            String relatedId = tuijlist.get(mCurtjZt).getQid();
                            String flag = tuijlist.get(mCurtjZt).getFlag();

                            TCAgent.onEvent(mContext, "首页精选好医", "直播 id:" + relatedId, kv);

                            try {

                                String uid = Utils.getUid();
                                String fake_token = Cfg.loadStr(mContext, "hj_token", "");

                                if (Utils.isLogin()) {
                                    if (Utils.isBind()) {
                                        if (fake_token.length() > 0) {
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("type", Integer.parseInt(flag));
                                            bundle.putString("sn", "");
                                            bundle.putString("liveId", relatedId);
                                            bundle.putString("replayId", relatedId);
                                            bundle.putString("background", "");
                                            bundle.putString("channel", "");
                                            bundle.putString("usign", "");

                                        }
                                    } else {
                                        Utils.jumpBindingPhone(mContext);

                                    }

                                } else {
                                    Utils.jumpLogin(mContext);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (tuijlist.get(mCurtjZt).getType().equals("0") || tuijlist.get(mCurtjZt).getType().equals("") || null == tuijlist.get(mCurtjZt).getType()) {

                            TCAgent.onEvent(mContext, "首页精选好医", "url:" + tuijlist.get(mCurtjZt).getUrl(), kv);

                            WebUrlTypeUtil.getInstance(mContext).urlToApp(tuijlist.get(mCurtjZt).getUrl(), "1", tuijlist.get(mCurtjZt).getQid());

                        }
                    }
                });
            }

        } else {
            hyZtLy.setVisibility(View.GONE);
            hyZtContentLy.setVisibility(View.GONE);
        }
    }

    private void communityTitle() {
        bbsData = homeData.getBbsdata();                 //社区标题 实体类
        if (null != bbsData && bbsData.size() > 0) {
            mBbsTital.setVisibility(View.VISIBLE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

            CommunityAdapter communityAdapter = new CommunityAdapter(mContext, R.layout.acty_home_community_item, bbsData);
            communityAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(bbsData.get(position).getUrl(), "1", "0");
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_BBS, (position + 1) + ""), bbsData.get(position).getEvent_params());
                }
            });
            mBbsList.setLayoutManager(linearLayoutManager);
            mBbsList.setAdapter(communityAdapter);

        } else {
            mBbsTital.setVisibility(View.GONE);
        }
    }

    /**
     * 今日特卖
     */
    private void saleDay() {
        List<HotBean> hotData = homeData.getHot();                    //今日特卖 实体类
        if (hotData.size() > 0) {
            homeSale.setVisibility(View.VISIBLE);

            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            HomeSaleListAdapter homeSaleListAdapter = new HomeSaleListAdapter(mContext, hotData);
            saleList.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.setCustomColor("#efefef")));
            saleList.setLayoutManager(scrollLinearLayoutManager);
            saleList.setAdapter(homeSaleListAdapter);

            //item点击事件
            homeSaleListAdapter.setOnEventClickListener(new HomeSaleListAdapter.OnEventClickListener() {
                @Override
                public void onItemClick(View v, int pos, HotBean data) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|hotsku|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, data.getBmsid(), "1"), data.getEvent_params());
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(data.getUrl(), "1", "0");
                }
            });

        } else {
            homeSale.setVisibility(View.GONE);
        }
    }

    /**
     * 横滑日记列表
     */
    private void diaryList() {
        HomeFragment homeFragment = null;
        tuijshare = homeData.getTuijshare();            //首页推荐5种类型实体类
        boardlist = homeData.getBoard();                //日记横向滑动title实体类
        String showType = homeData.getShowType();       // 1 双侧流 2正常
        titleList.clear();
        fragmentList.clear();

        for (int i = 0; i < boardlist.size(); i++) {
            BoardBean data = boardlist.get(i);
            titleList.add(data.getTitle());
            String parIdStr = data.getId();
            if (i == 0) {
                homeFragment = HomeFragment.newInstance(parIdStr, tuijshare, showType);
                setHomeFragmentListener(homeFragment);
                fragmentList.add(homeFragment);
            } else {
                HomeFragment homeFragment1 = HomeFragment.newInstance(parIdStr);
                setHomeFragmentListener(homeFragment1);
                fragmentList.add(homeFragment1);
            }
        }

        YMTabLayoutAdapter ymTabLayoutAdapter = new YMTabLayoutAdapter(getSupportFragmentManager(), titleList, fragmentList);
        mViewPage.setAdapter(ymTabLayoutAdapter);
        mTabLayout.setupWithViewPager(mViewPage);
        YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(13), Utils.dip2px(13));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mPos = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setHomeFragmentListener(HomeFragment homeFragment) {
        if (null != homeFragment) {
            homeFragment.setmOnRecyclerViewStateCallBack(new HomeFragment.OnRecyclerViewStateCallBack() {
                @Override
                public void recyclerViewStateCallBack(RecyclerView recyclerView, int newState) {
                    Log.e(TAG, "newState == " + newState);
                    if (homeCouposRly.getVisibility() == View.VISIBLE) {
                        switch (newState) {
                            case RecyclerView.SCROLL_STATE_IDLE:
                                isExpand = true;

                                //滑入动画
                                ObjectAnimator translationXAnimatorIn = ObjectAnimator.ofFloat(homeCouposRly, "translationX", 120f, 0);
                                ObjectAnimator alphaAnimatorIn = ObjectAnimator.ofFloat(homeCouposRly, "alpha", 0.5f, 1f);
                                AnimatorSet animatorSetIn = new AnimatorSet();
                                animatorSetIn.playTogether(translationXAnimatorIn, alphaAnimatorIn);
                                animatorSetIn.setDuration(500);
                                animatorSetIn.start();
                                break;
                            case RecyclerView.SCROLL_STATE_DRAGGING:
                            case RecyclerView.SCROLL_STATE_SETTLING:
                                if (isExpand) {
                                    isExpand = false;

                                    ObjectAnimator translationXAnimatorOut = ObjectAnimator.ofFloat(homeCouposRly, "translationX", 0f, 120f);
                                    ObjectAnimator alphaAnimatorOut = ObjectAnimator.ofFloat(homeCouposRly, "alpha", 1f, 0.5f);
                                    AnimatorSet animatorSetOut = new AnimatorSet();
                                    animatorSetOut.playTogether(translationXAnimatorOut, alphaAnimatorOut);
                                    animatorSetOut.setDuration(500);
                                    animatorSetOut.start();

                                }
                                break;
                        }
                    }

                }
            });
        }
    }


    /**
     * 添加分割线
     *
     * @param high：分割线高度（要传过来的是  px）
     * @param color：分割线颜色
     * @param container：添加分割线的容器
     */
    private void addDivider1(int high, String color, ViewGroup container) {
        //添加头部的分割线
        View view = new View(mContext);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, high);
        view.setBackgroundColor(Color.parseColor(color));
        container.addView(view, params);
    }

    /**
     * 百度统计
     *
     * @param eventId:事件ID,注意要先在mtj.baidu.com中注册此事件ID(产品注册)
     * @param label:自定义事件标签
     */
    private void baiduTongJi(String eventId, String label) {
        StatService.onEvent(mContext, eventId, label, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 4 && resultCode == 4) {
            String typeCurCity = Cfg.loadStr(mContext, FinalConstant.DWCITY, "全国");
            Log.e(TAG, "curCity === " + curCity);
            Log.e(TAG, "typeCurCity === " + typeCurCity);
            if (!typeCurCity.equals(curCity)) {
                initSearhShowData();
            }

        } else if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            Log.e(TAG, "fragmentList === " + fragmentList.size());
            Log.e(TAG, "mPos === " + mPos);
            if (fragmentList.size() != 0) {
                fragmentList.get(mPos).onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    /**
     * 返回键回调方法
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(getParent(), R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
        if (isLogin != Utils.isLogin()) {
            loadHomeData();
            isLogin = Utils.isLogin();
        }

        mHandler.sendEmptyMessage(PUSH_NEW);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (setCity()) {
            loadHomeData();     //获取初始化数据
        }
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            homeCartNum.setVisibility(View.VISIBLE);
            homeCartNum.setText(cartNumber);
        } else {
            homeCartNum.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 设置底部提示显示隐藏
     *
     * @param buttomFloat
     */
    private void setBottomTipShow(HomeButtomFloat buttomFloat) {
        String today = Utils.getDay();
        String srt = Cfg.loadStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, "");
        if (!TextUtils.isEmpty(srt)) {
            DiariesPostShowData diariesPostShowData = mGson.fromJson(srt, DiariesPostShowData.class);
            String data = diariesPostShowData.getData();
            int number = diariesPostShowData.getNumber();
            if (!data.equals(today) || number < 3) {
                setBottomTipData(buttomFloat);
                String toJson;
                if (!data.equals(today)) {
                    toJson = mGson.toJson(new DiariesPostShowData(today, 1));
                } else {
                    number++;
                    toJson = mGson.toJson(new DiariesPostShowData(today, number));
                }
                Cfg.saveStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, toJson);
            } else {
                mBottomTip.setVisibility(View.GONE);
            }
        } else {
            setBottomTipData(buttomFloat);
            String toJson = mGson.toJson(new DiariesPostShowData(today, 1));
            Cfg.saveStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, toJson);
        }
    }

    /**
     * 设置底部提示数据
     *
     * @param buttomFloat
     */
    private void setBottomTipData(final HomeButtomFloat buttomFloat) {
        mBottomTip.setVisibility(View.VISIBLE);
        mBottomTip.setText(buttomFloat.getTitle());
        mBottomTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = buttomFloat.getUrl();
                if (!TextUtils.isEmpty(url)) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(buttomFloat.getUrl());
                }
            }
        });
        mHandler.sendEmptyMessageDelayed(HOME_TIP, Integer.parseInt(buttomFloat.getShowTime()) * 1000);
    }
}
