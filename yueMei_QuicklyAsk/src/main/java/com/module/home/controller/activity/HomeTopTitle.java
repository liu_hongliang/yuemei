package com.module.home.controller.activity;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.module.home.model.bean.SearchEntry;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import java.util.ArrayList;
import java.util.List;

/**
 * 首页Title
 * Created by 裴成浩 on 2019/9/17
 */
public class HomeTopTitle extends FrameLayout {

    private final Context mContext;

    private View viewBackground;

//    private ImageView viewBackground2;

    private AdertAdv.OtherBackgroundImgBean data;

    //标题栏
    FrameLayout homeTitle;
    View titleBackground;
    TextView cityTv;
    ImageView taoZxDiquIv1;
    LinearLayout cityHomeLy1;
    RelativeLayout citySelectRly;
    LinearLayout searchClick;
    ViewFlipper mFlipper;
    ImageView inputImage;
    LinearLayout serachRly123;
    ImageView saoSaoIv1;
    TextView homeCartNum;
    RelativeLayout saoerMaRly;

    private List<String> titleLists = new ArrayList<>();       //标题栏文案集合
    private List<String> showTitleLists = new ArrayList<>();       //标题栏文案集合
    private String TAG = "HomeTopTitle";
    private final String defaultTitle = "搜索你感兴趣的项目";

    public HomeTopTitle(Context context) {
        this(context, null);
    }

    public HomeTopTitle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeTopTitle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);

        if (data != null && data.getImg() != null) {
            viewBackground = new View(mContext);
            //设置背景默认透明
            viewBackground.setAlpha(0);
            viewBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color._f5));
            addView(viewBackground, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//            ImageView viewBackground2 = new ImageView(mContext);
//            Glide.with(mContext).load(data.getImg())
//                    .centerCrop()
//                    .into(viewBackground2);
//            addView(viewBackground2, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            viewBackground = new View(mContext);
            //设置背景默认透明
            viewBackground.setAlpha(0);
            viewBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color.red_ff527f));
            addView(viewBackground, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        View topView = View.inflate(mContext, R.layout.home_top_title_view, this);
        homeTitle = topView.findViewById(R.id.ll_home_title);
        titleBackground = topView.findViewById(R.id.title_background_color);
        cityTv = topView.findViewById(R.id.tab_doc_list_city_tv1);
        taoZxDiquIv1 = topView.findViewById(R.id.tao_zx_diqu_iv1);
        cityHomeLy1 = topView.findViewById(R.id.city_home_ly1);
        citySelectRly = topView.findViewById(R.id.tab_doc_city_rly1);
        searchClick = topView.findViewById(R.id.home_input_edit1_click);
        mFlipper = topView.findViewById(R.id.home_input_edit1);
        inputImage = topView.findViewById(R.id.home_input_image);
        serachRly123 = topView.findViewById(R.id.search_rly_click1);
        saoSaoIv1 = topView.findViewById(R.id.sao_sao_iv1);
        homeCartNum = topView.findViewById(R.id.home_cart_num);
        saoerMaRly = topView.findViewById(R.id.sao_sao_rly1);
        //城市点击事件
        citySelectRly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCityClick(v, getCity());
                }
            }
        });

        //搜索框点击
        searchClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    String titleText = getTitleText();
                    if (defaultTitle.equals(titleText)) {
                        titleText = "";
                    }
                    onEventClickListener.onTitleClick(v, titleText, mFlipper.getDisplayedChild());
                }
            }
        });

        //购物车点击
        saoerMaRly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCartClick(v);
                }
            }
        });

        setCartNum();
    }


    /***
     * 设置定位城市
     */
    public boolean setCity() {
        boolean isRefresh = false;
        if (Utils.getCity().length() > 0) {
            if (Utils.getCity().equals("失败")) {
                cityTv.setText("全国");

            } else if (!Utils.getCity().equals(getCity())) {
                cityTv.setText(Utils.getCity());
                isRefresh = true;
            }
        } else {
            cityTv.setText("全国");
        }
        return isRefresh;
    }

    /**
     * 获取选中城市
     *
     * @return
     */
    private String getCity() {
        return cityTv.getText().toString();
    }

    /**
     * 头部渐变色
     *
     * @param alpha
     */
    private int[] topImages = new int[]{R.drawable.home_top_search, R.drawable.home_top_search2};
    int topImagesPos = 0;

    public void setTopAlpha(float alpha) {
        if (inputImage.getVisibility() == VISIBLE) {
            if (alpha == 1) {
                if (topImagesPos == 0) {
                    topImagesPos = 1;
                    Glide.with(mContext).load(topImages[topImagesPos]).into(inputImage);
                }
            } else {
                if (topImagesPos == 1) {
                    topImagesPos = 0;
                    Glide.with(mContext).load(topImages[topImagesPos]).into(inputImage);
                }
            }
        }
        if (data != null && alpha == 1) {
            saoSaoIv1.setImageResource(R.drawable.shop_home_car_white);
        }else {
            saoSaoIv1.setImageResource(R.drawable.shop_home_car_black);
        }
        viewBackground.setAlpha(alpha);
        ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) citySelectRly.getLayoutParams();
        int leftMargin = layoutParams.leftMargin;
        int rightMargin = layoutParams.rightMargin;
        int measuredWidth = citySelectRly.getMeasuredWidth();
        int measuredHeight = citySelectRly.getMeasuredHeight();

        float translationX = (measuredWidth + leftMargin) * alpha;
        citySelectRly.layout((int) (leftMargin - translationX), 0, (int) (measuredWidth + leftMargin - translationX), measuredHeight);

        ViewGroup.MarginLayoutParams layoutParams1 = (MarginLayoutParams) searchClick.getLayoutParams();
        int leftMargin1 = layoutParams1.leftMargin;
        int measuredWidth1 = searchClick.getMeasuredWidth();
        int measuredHeight1 = searchClick.getMeasuredHeight();
        searchClick.layout((int) (measuredWidth + leftMargin + rightMargin + leftMargin1 - translationX), 0, leftMargin + measuredWidth + rightMargin + leftMargin1 + measuredWidth1, measuredHeight1);
    }

    /**
     * 设置搜索框文案
     *
     * @param datas
     */
    public void setTitleText(List<SearchEntry> datas) {
        titleLists.clear();
        showTitleLists.clear();
        for (SearchEntry data : datas) {
            titleLists.add(data.getSearch_title());
            showTitleLists.add(data.getShow_title());
        }
        mFlipper.removeAllViews();
        if (titleLists.size() == 0) {
            titleLists.add(defaultTitle);
        }
        for (final String data : showTitleLists) {
            TextView textView = new TextView(mContext);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            textView.setTextColor(Utils.getLocalColor(mContext, R.color._88));
            textView.setText(data);
            textView.setLines(1);
            textView.setGravity(Gravity.CENTER);
            mFlipper.addView(textView);
        }

        if (titleLists.size() > 1) {
            mFlipper.setFlipInterval(1000 * 10);
            mFlipper.startFlipping();
        }
    }

    /**
     * 获取搜索框文案
     */
    private String getTitleText() {
        if (titleLists.size() > 0) {
            return titleLists.get(mFlipper.getDisplayedChild());
        } else {
            return "";
        }
    }

    /**
     * 设置购物车数量
     */
    public void setCartNum() {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            homeCartNum.setVisibility(View.VISIBLE);
            homeCartNum.setText(cartNumber);
        } else {
            homeCartNum.setVisibility(View.GONE);
        }
    }

    public interface OnEventClickListener {
        void onCityClick(View v, String city);                      //城市点击回调

        void onTitleClick(View v, String key, int pos);              //搜索框点击回调

        void onCartClick(View v);                                   //购物车点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
