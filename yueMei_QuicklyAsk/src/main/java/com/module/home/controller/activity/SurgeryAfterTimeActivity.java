package com.module.home.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.my.controller.activity.WriteNoteActivity;
import com.quicklyask.activity.R;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SurgeryAfterTimeActivity extends BaseActivity {

    private String TAG = "SurgeryAfterTime";

    @BindView(id = R.id.after_time_back)
    private RelativeLayout afterTimeBack;
    @BindView(id = R.id.surgery_determine_rly)
    private RelativeLayout surgeryDetermine;
    @BindView(id = R.id.surgery_after_et)
    private EditText surgeryAfter;
    private String beforeMaxDay;
    private Context mContext;


    @Override
    public void setRootView() {
        setContentView(R.layout.activity_surgery_after_time);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = SurgeryAfterTimeActivity.this;
        initView();
    }

    private void initView() {
        String afterDay = getIntent().getStringExtra("afterDay");
        beforeMaxDay = getIntent().getStringExtra("beforemaxday");
        if (afterDay != null && !"".equals(afterDay)) {
            surgeryAfter.setText(afterDay);
            surgeryAfter.setSelection(surgeryAfter.length());
        }

        // 1秒后自动弹出软键盘
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                //软键盘显示
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        }, 1000);


        //返回
        afterTimeBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("tiem", surgeryAfter.getText().toString());
                setResult(WriteNoteActivity.POSTOPERATIVE_TIME, intent);
                finish();
                overridePendingTransition(0, R.anim.activity_left_to_right_close);
            }
        });

        //确定
        surgeryDetermine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String surAfterStr = surgeryAfter.getText().toString();
                Log.e(TAG, "beforeMaxDay== " + beforeMaxDay);
                Log.e(TAG, "surAfterStr== " + surAfterStr);

                if(surAfterStr.length() == 0){
                    surAfterStr = "0";
                }

                if (Integer.parseInt(surAfterStr) < 9999) {

                    if (!"0".equals(beforeMaxDay)) {
                        if (Integer.parseInt(surAfterStr) < Integer.parseInt(beforeMaxDay)) {
                            showDialogExitEdit("术后时间不能小于"+ beforeMaxDay +"天，请重新填写");
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("tiem", surAfterStr);
                            setResult(WriteNoteActivity.POSTOPERATIVE_TIME, intent);
                            finish();
                            overridePendingTransition(0, R.anim.activity_left_to_right_close);
                        }
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("tiem", surAfterStr);
                        setResult(WriteNoteActivity.POSTOPERATIVE_TIME, intent);
                        finish();
                        overridePendingTransition(0, R.anim.activity_left_to_right_close);
                    }

                } else {
                    showDialogExitEdit("术后天数不得大于9999");
                }

            }
        });

    }

    private void showDialogExitEdit(String content) {
        final EditExitDialog editDialog = new EditExitDialog(mContext,
                R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        TextView trueText = editDialog.findViewById(R.id.dialog_exit_content_tv);
        trueText.setText(content);
        Button trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setText("确定");
        trueBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });

    }

    /**
     * 监听Back键按下事件
     * 注意:
     * 返回值表示:是否能完全处理该事件
     * 在此处返回false,所以会继续传播该事件.
     * 在具体项目中此处的返回值视情况而定.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            Intent intent = new Intent();
            intent.putExtra("tiem", surgeryAfter.getText().toString());
            setResult(WriteNoteActivity.POSTOPERATIVE_TIME, intent);
            finish();
            overridePendingTransition(0, R.anim.activity_left_to_right_close);

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }


}
