package com.module.home.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.home.fragment.ProjectDetailsFragment;
import com.module.home.model.api.ProjectDetailsTagApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsTag;
import com.module.home.view.NoScrollViewPager;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 项目详情页面
 *
 * @author 裴成浩
 */
public class ProjectDetailsActivity extends YMBaseActivity {

    @BindView(R.id.project_details_search_top)
    FrameLayout mSearchTop;
    @BindView(R.id.project_details_back)
    ImageView mDetailsBack;
    @BindView(R.id.project_details_tab_container)
    FrameLayout mTabContainer;

    TabLayout mTabLayout;
    public NoScrollViewPager mViewPage;

    private final String TAG = "ProjectDetailsActivity";
    private List<YMBaseFragment> mFragmentList = new ArrayList<>();
    private List<String> mPageTitleList = new ArrayList<>();
    private ProjectDetailsTagApi projectDetailsTagApi;
    public String oneLabelId;
    private String twoLabelId;
    private String fourLabelId;
    private String homeSource;
    public List<ProjectDetailsTag> tagDatas = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_project_details;
    }

    @Override
    protected void initView() {
        Bundle bundle = getIntent().getBundleExtra("data");
        oneLabelId = bundle.getString("one_id");
        twoLabelId = bundle.getString("two_labe");
        fourLabelId = bundle.getString("four_labe");
        homeSource = bundle.getString("home_source");

        Log.e(TAG, "twoLabelId == " + twoLabelId);
        Log.e(TAG, "fourLabelId == " + fourLabelId);
        Log.e(TAG, "homeSource == " + homeSource);

        //返回按钮
        mDetailsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //搜索页面
        mSearchTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Utils.tongjiApp(mContext, "bbs_search", "forum", "", "0");
                Intent it2 = new Intent(mContext, SearchAllActivity668.class);
                it2.putExtra(SearchAllActivity668.TYPE, "4");
                startActivity(it2);
            }
        });
    }

    @Override
    protected void initData() {
        projectDetailsTagApi = new ProjectDetailsTagApi();
        getTagData();
    }

    private void getTagData() {
        Log.e(TAG, "parentLabelID == " + twoLabelId);
        Log.e(TAG, "labelID == " + fourLabelId);
        projectDetailsTagApi.addData("parentLabelID", twoLabelId);
        projectDetailsTagApi.addData("labelID", fourLabelId);
        projectDetailsTagApi.getCallBack(mContext, projectDetailsTagApi.getHashMap(), new BaseCallBackListener<List<ProjectDetailsTag>>() {

            @Override
            public void onSuccess(List<ProjectDetailsTag> datas) {
                tagDatas = datas;
                int selectedPos = 0;
                if (datas.size() > 1) {
                    mPageTitleList.clear();
                    mFragmentList.clear();

                    View tabView = View.inflate(mContext, R.layout.project_details_tab, mTabContainer);

                    mTabLayout = tabView.findViewById(R.id.project_details_tab_latoyt);
                    mViewPage = tabView.findViewById(R.id.project_details_view_page);

                    for (int i = 0; i < datas.size(); i++) {
                        ProjectDetailsTag data = datas.get(i);
                        mPageTitleList.add(data.getName());
                        //设置默认选中
                        String parentId = data.getId();
                        if (parentId.equals(twoLabelId)) {
                            selectedPos = i;
                        } else {
                            for (int j = 0; j < data.getList().size(); j++) {
                                if (data.getList().get(j).getId().equals(fourLabelId)) {
                                    selectedPos = i;
                                    data.getList().get(j).setSelected(true);
                                    break;
                                }
                            }
                        }

                        Log.e(TAG, "11111data.getId() == " + data.getId());
                        Log.e(TAG, "22222fourLabelId == " + fourLabelId);
                        ProjectDetailsBean detailsBean = new ProjectDetailsBean();
                        detailsBean.setTwoLabelId(data.getId());
                        detailsBean.setFourLabelId("0");
                        detailsBean.setHomeSource(homeSource);
                        mFragmentList.add(ProjectDetailsFragment.newInstance(data.getList(), detailsBean, i));
                    }

                    mViewPage.setChildCount(mFragmentList.size());

                    YMTabLayoutAdapter mPagerAdapter = new YMTabLayoutAdapter(getSupportFragmentManager(), mPageTitleList, mFragmentList);
                    mViewPage.setAdapter(mPagerAdapter);
                    mTabLayout.setupWithViewPager(mViewPage);

                    YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(13), Utils.dip2px(13));

                    TabLayout.Tab tabAt = mTabLayout.getTabAt(selectedPos);
                    if (tabAt != null) {
                        tabAt.select();
                    }
                } else {
                    ProjectDetailsTag data = null;
                    ProjectDetailsBean detailsBean = new ProjectDetailsBean();
                    if (datas.size() == 1) {
                        data = datas.get(0);
                        detailsBean.setTwoLabelId(data.getId());
                        detailsBean.setFourLabelId("0");
                    } else {
                        detailsBean.setTwoLabelId(twoLabelId);
                        detailsBean.setFourLabelId(fourLabelId);
                    }
                    detailsBean.setHomeSource(homeSource);

                    setActivityFragment(R.id.project_details_tab_container, ProjectDetailsFragment.newInstance(data == null ? null : data.getList(), detailsBean, 0));
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTabLayout != null) {
                mFragmentList.get(mTabLayout.getSelectedTabPosition()).onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }
}
