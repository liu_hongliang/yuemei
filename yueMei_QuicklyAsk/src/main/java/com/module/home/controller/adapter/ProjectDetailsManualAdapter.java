package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.base.view.ui.HomeBanner;
import com.module.base.view.ui.YMBanner;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.model.bean.BbsBean;
import com.module.home.model.bean.HotBean;
import com.module.home.model.bean.ManualPosition;
import com.module.home.model.bean.ManualPositionBtnBoard;
import com.module.home.model.bean.ManualPositionTag;
import com.module.home.model.bean.ProjectFocuMapData;
import com.module.home.model.bean.RecommendDoctors;
import com.module.home.model.bean.RecommendHospital;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/4/3
 */
public class ProjectDetailsManualAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private ArrayList<ManualPosition> mDatas;
    private LayoutInflater mInflater;
    private final int mPos;
    private final HashMap<String, String> more_event_params;

    private final int ITEM_TYPE_ONE = 1;                            //标签1
    private final int ITEM_TYPE_TWO = 2;                            //标签2
    private final int ITEM_TYPE_THREE = 3;                          //标签3
    private final int ITEM_TYPE_FOCUS = 4;                          //轮播图
    private final int ITEM_TYPE_METRO = 5;                          //metro手工位
    private final int ITEM_TYPE_HOTTAO = 6;                         //超值特卖
    private final int ITEM_TYPE_ACTIVITY = 7;                       //活动上三卡
    private final int ITEM_TYPE_CHOSEN = 8;                         //教你避坑
    private final int ITEM_TYPE_DOCTORS = 9;                        //医生列表
    private final int ITEM_TYPE_HOSPITAL = 10;                      //医院列表
    private final FunctionManager mFunctionManager;
    private String TAG = "ProjectDetailsManualAdapter";

    public ProjectDetailsManualAdapter(Activity context, ArrayList<ManualPosition> manualPosition, int pos, HashMap<String, String> more_event_params) {
        this.mContext = context;
        this.mDatas = manualPosition;
        this.mPos = pos;
        this.more_event_params = more_event_params;
        mInflater = LayoutInflater.from(context);
        mFunctionManager = new FunctionManager(mContext);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public int getItemViewType(int position) {

        ManualPosition data = mDatas.get(position);
        ManualPositionTag listOne = data.getListOne();
        ManualPositionTag listTwo = data.getListTwo();
        ManualPositionTag listThree = data.getListThree();
        List<ProjectFocuMapData> focusImage = data.getFocusImage();
        List<List<ProjectFocuMapData>> metro = data.getMetro();
        List<HotBean> hotTao = data.getHotTao();
        List<ProjectFocuMapData> activity = data.getActivity();
        List<BbsBean> chosenDiary = data.getChosenDiary();
        List<RecommendDoctors> recommendDoctors = data.getRecommendDoctors();

        if (listOne != null) {
            return ITEM_TYPE_ONE;
        } else if (listTwo != null) {
            return ITEM_TYPE_TWO;
        } else if (listThree != null) {
            return ITEM_TYPE_THREE;
        } else if (focusImage != null) {
            return ITEM_TYPE_FOCUS;
        } else if (metro != null) {
            return ITEM_TYPE_METRO;
        } else if (hotTao != null) {
            return ITEM_TYPE_HOTTAO;
        } else if (activity != null) {
            return ITEM_TYPE_ACTIVITY;
        } else if (chosenDiary != null) {
            return ITEM_TYPE_CHOSEN;
        } else if (recommendDoctors != null) {
            return ITEM_TYPE_DOCTORS;
        } else {
            return ITEM_TYPE_HOSPITAL;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ONE:
                return new OneViewHolder(mInflater.inflate(R.layout.item_project_top_one, parent, false));
            case ITEM_TYPE_TWO:
                return new TwoViewHolder(mInflater.inflate(R.layout.item_project_top_two, parent, false));
            case ITEM_TYPE_THREE:
                return new ThreeViewHolder(mInflater.inflate(R.layout.item_project_top_three, parent, false));
            case ITEM_TYPE_FOCUS:
                return new FocusViewHolder(mInflater.inflate(R.layout.item_project_top_bannner, parent, false));
            case ITEM_TYPE_METRO:
                return new MetroViewHolder(mInflater.inflate(R.layout.item_project_top_metro, parent, false));
            case ITEM_TYPE_HOTTAO:
                return new HottaoViewHolder(mInflater.inflate(R.layout.item_project_top_sale, parent, false));
            case ITEM_TYPE_ACTIVITY:
                return new ActivityViewHolder(mInflater.inflate(R.layout.item_project_top_card, parent, false));
            case ITEM_TYPE_CHOSEN:
                return new ChosenViewHolder(mInflater.inflate(R.layout.item_project_top_community, parent, false));
            case ITEM_TYPE_DOCTORS:
                return new DoctorsViewHolder(mInflater.inflate(R.layout.item_project_top_doctor, parent, false));
            case ITEM_TYPE_HOSPITAL:
            default:
                return new HospitalViewHolder(mInflater.inflate(R.layout.item_project_top_hospital, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OneViewHolder) {
            setOneView((OneViewHolder) holder, position);
        } else if (holder instanceof TwoViewHolder) {
            setTwoView((TwoViewHolder) holder, position);
        } else if (holder instanceof ThreeViewHolder) {
            setThreeView((ThreeViewHolder) holder, position);
        } else if (holder instanceof FocusViewHolder) {
            setFocusView((FocusViewHolder) holder, position);
        } else if (holder instanceof MetroViewHolder) {
            setMetroView((MetroViewHolder) holder, position);
        } else if (holder instanceof HottaoViewHolder) {
            setHottaoView((HottaoViewHolder) holder, position);
        } else if (holder instanceof ActivityViewHolder) {
            setActivityView((ActivityViewHolder) holder, position);
        } else if (holder instanceof ChosenViewHolder) {
            setChosenView((ChosenViewHolder) holder, position);
        } else if (holder instanceof DoctorsViewHolder) {
            setDoctorsView((DoctorsViewHolder) holder, position);
        } else if (holder instanceof HospitalViewHolder) {
            setHospitalView((HospitalViewHolder) holder, position);
        }
    }


    /**
     * 设置标签
     *
     * @param holder
     * @param position
     */
    private void setOneView(OneViewHolder holder, int position) {
        ManualPositionTag listOne = mDatas.get(position).getListOne();
        Log.e(TAG, "listOne == " + listOne);
        ManualPositionBtnBoard leftBtnBoard = listOne.getLeft_btn_board();

        //设置背景渐变色
        setGradientBackground(holder.oneBack, listOne.getBack_color(), listOne.getBack_img());

        int spanCount = 0;
        //设置左侧是否存在
        if (leftBtnBoard != null) {
            holder.oneLeftView.setVisibility(View.VISIBLE);
            holder.leftText.setText(leftBtnBoard.getTitle());
            mFunctionManager.setImageSrc(holder.leftImage, leftBtnBoard.getBack_img());
            spanCount = 2;
        } else {
            spanCount = 3;
            holder.oneLeftView.setVisibility(View.GONE);
        }

        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, spanCount);
        gridLayoutManager.setScrollEnable(false);
        holder.recyclerView.setLayoutManager(gridLayoutManager);

        holder.recyclerView.setPadding(Utils.dip2px(10), 0, Utils.dip2px(10), 0);
        int parentWidth = 0;
        if (spanCount == 2) {
            parentWidth = (Utils.getScreenSize(mContext)[0] - Utils.dip2px(30)) / 3 * 2 - holder.recyclerView.getPaddingLeft() - holder.recyclerView.getPaddingRight();
        } else {
            parentWidth = Utils.getScreenSize(mContext)[0] - Utils.dip2px(30) - holder.recyclerView.getPaddingLeft() - holder.recyclerView.getPaddingRight();
        }

        ManualPositionTagAdapter manualPositionTagAdapter = new ManualPositionTagAdapter(mContext, listOne.getBoard_list(), spanCount, parentWidth);
        holder.recyclerView.setAdapter(manualPositionTagAdapter);

        manualPositionTagAdapter.setOnItemClickListener(new ManualPositionTagAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, ManualPositionBtnBoard data) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position, data);
                }
            }
        });

    }

    /**
     * 设置渐变背景
     *
     * @param imageView:      要设置渐变色组件
     * @param backColor：渐变色颜色
     * @param backImage：背景图
     */
    private void setGradientBackground(ImageView imageView, List<String> backColor, String backImage) {
        if (!TextUtils.isEmpty(backImage)) {
            mFunctionManager.setPlaceholderImageSrc(imageView, backImage);
        } else {
            try {
                if (backColor.size() >= 2) {
                    String colorLeft = backColor.get(0);
                    String colorRight = backColor.get(1);
                    if (colorLeft.startsWith("#") && colorRight.startsWith("#")) {
                        GradientDrawable aDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Utils.setCustomColor(colorLeft), Utils.setCustomColor(colorRight)});
                        aDrawable.setCornerRadius(Utils.dip2px(6));
                        imageView.setBackground(aDrawable);
                    } else {
                        if (colorLeft.startsWith("#")) {
                            imageView.setBackgroundColor(Utils.setCustomColor(colorLeft));
                        } else if (colorRight.startsWith("#")) {
                            imageView.setBackgroundColor(Utils.setCustomColor(colorRight));
                        } else {
                            imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_eye_shap));
                        }
                    }
                } else {
                    imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_eye_shap));
                }
            } catch (NumberFormatException e) {
                imageView.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_eye_shap));
            }
        }
    }


    /**
     * 标签二
     *
     * @param holder
     * @param position
     */
    private void setTwoView(TwoViewHolder holder, int position) {
        ManualPositionTag listTwo = mDatas.get(position).getListTwo();
        int moreShowNum = Integer.parseInt(listTwo.getMore_show_num());
        if (listTwo.getBoard_list().size() > moreShowNum) {
            holder.twoText.setVisibility(View.VISIBLE);
        } else {
            holder.twoText.setVisibility(View.GONE);
        }

        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 3);
        gridLayoutManager.setScrollEnable(false);
        holder.twoRecycler.setLayoutManager(gridLayoutManager);

        int paddingLeft = holder.twoRecycler.getPaddingLeft();
        int paddingRight = holder.twoRecycler.getPaddingRight();
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.twoRecycler.getLayoutParams();
        int parentMargin = paddingLeft + paddingRight + params.leftMargin + params.rightMargin;

        ManualPositionTwoAdapter manualPositionTwoAdapter = new ManualPositionTwoAdapter(mContext, listTwo, parentMargin);
        holder.twoRecycler.setAdapter(manualPositionTwoAdapter);

        manualPositionTwoAdapter.setOnItemClickListener(new ManualPositionTwoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, ManualPositionBtnBoard data) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position, data);
                }
            }
        });
    }

    /**
     * 标签三
     *
     * @param holder
     * @param position
     */
    private void setThreeView(ThreeViewHolder holder, int position) {
        ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 4);
        gridLayoutManager.setScrollEnable(false);
        holder.mRecycler.setLayoutManager(gridLayoutManager);

        ManualPositionThreeAdapter manualPositionThreeAdapter = new ManualPositionThreeAdapter(mContext, mDatas.get(position).getListThree().getBoard_list());
        holder.mRecycler.setAdapter(manualPositionThreeAdapter);

        manualPositionThreeAdapter.setOnItemClickListener(new ManualPositionThreeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, ManualPositionBtnBoard data) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(view, position, data);
                }
            }
        });
    }

    /**
     * 轮播图
     *
     * @param holder
     * @param position
     */
    private void setFocusView(FocusViewHolder holder, int position) {
        final List<ProjectFocuMapData> focusImage = mDatas.get(position).getFocusImage();

        if (holder.mFragmentBanner.getUrlImageList().size() == 0) {
            if (focusImage.size() != 0) {
                ArrayList<String> images = new ArrayList<>();
                for (ProjectFocuMapData img : focusImage) {
                    images.add(img.getImg());
                }
                holder.mFragmentBanner.setImagesData(images);
                holder.mFragmentBanner.setVisibility(View.VISIBLE);
            } else {
                holder.mFragmentBanner.setVisibility(View.GONE);
            }
        }
    }

    /**
     * metro手工位
     *
     * @param holder
     * @param position
     */
    private void setMetroView(MetroViewHolder holder, int position) {
        List<List<ProjectFocuMapData>> metro = mDatas.get(position).getMetro();

        ScrollLayoutManager scrollLinear = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinear.setScrollEnable(false);
        holder.mMetroRecycler.setLayoutManager(scrollLinear);
        ProjectDetailsMetrosAdapter detailsMetroAdapter = new ProjectDetailsMetrosAdapter(mContext, metro, mPos);
        holder.mMetroRecycler.setAdapter(detailsMetroAdapter);
    }

    /**
     * 超值特卖
     *
     * @param holder
     * @param position
     */
    private void setHottaoView(HottaoViewHolder holder, int position) {
        List<HotBean> hotTao = mDatas.get(position).getHotTao();

        String title = mDatas.get(position).getRecommendTitle();
        if (!TextUtils.isEmpty(title)) {
            holder.saleTtle.setText(title);
        }

        ScrollLayoutManager scrollLinear = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinear.setScrollEnable(false);
        holder.mSaleRecycler.setLayoutManager(scrollLinear);
        ProjectDetailsSalesAdapter detailsSalesAdapter = new ProjectDetailsSalesAdapter(mContext, hotTao);
        holder.mSaleRecycler.setAdapter(detailsSalesAdapter);
    }

    /**
     * 活动上三卡
     *
     * @param holder
     * @param position
     */
    private void setActivityView(ActivityViewHolder holder, int position) {
        List<ProjectFocuMapData> activity = mDatas.get(position).getActivity();
        holder.mFragmentActivity.setLayoutManager(new GridLayoutManager(mContext, 3, LinearLayoutManager.VERTICAL, false));
        ProjectDetailsActivityAdapter detailsActivityAdapter = new ProjectDetailsActivityAdapter(mContext, activity);
        holder.mFragmentActivity.setAdapter(detailsActivityAdapter);
    }

    /**
     * 教你避坑
     *
     * @param holder
     * @param position
     */
    private void setChosenView(ChosenViewHolder holder, int position) {
        List<BbsBean> chosenDiary = mDatas.get(position).getChosenDiary();

        String title = mDatas.get(position).getRecommendTitle();
        if (!TextUtils.isEmpty(title)) {
            holder.mCommunityTitle.setText(title);
        }

        holder.mCommunityRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ProjectDetailsCommunityAdapter detailsSalesAdapter = new ProjectDetailsCommunityAdapter(mContext, chosenDiary);
        holder.mCommunityRecycler.setAdapter(detailsSalesAdapter);
    }

    /**
     * 医生列表
     *
     * @param holder
     * @param position
     */
    private void setDoctorsView(DoctorsViewHolder holder, int position) {
        List<RecommendDoctors> recommendDoctors = mDatas.get(position).getRecommendDoctors();

        String title = mDatas.get(position).getRecommendTitle();
        if (!TextUtils.isEmpty(title)) {
            holder.mDoctorTitle.setText(title);
        }

        holder.mDoctorRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ProjectDetailsDoctorAdapter detailsDoctorAdapter = new ProjectDetailsDoctorAdapter(mContext, recommendDoctors);
        holder.mDoctorRecycler.setAdapter(detailsDoctorAdapter);
    }

    /**
     * 医院列表
     *
     * @param holder
     * @param position
     */
    private void setHospitalView(HospitalViewHolder holder, int position) {
        List<RecommendHospital> recommendHospital = mDatas.get(position).getRecommendHospital();

        String title = mDatas.get(position).getRecommendTitle();
        if (!TextUtils.isEmpty(title)) {
            holder.mHospitalTitle.setText(title);
        }

        holder.mHospitalRecycler.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        ProjectDetailsHospitalAdapter detailsDoctorAdapter = new ProjectDetailsHospitalAdapter(mContext, recommendHospital);
        holder.mHospitalRecycler.setAdapter(detailsDoctorAdapter);
    }


    /**
     * 标签样式1
     */
    public class OneViewHolder extends RecyclerView.ViewHolder {
        ImageView oneBack;
        LinearLayout oneLeftView;
        ImageView leftImage;
        TextView leftText;
        RecyclerView recyclerView;

        public OneViewHolder(@NonNull View itemView) {
            super(itemView);
            oneBack = itemView.findViewById(R.id.item_project_top_one_back);
            oneLeftView = itemView.findViewById(R.id.item_project_top_one_left);
            leftImage = itemView.findViewById(R.id.item_project_top_one_image);
            leftText = itemView.findViewById(R.id.item_project_top_one_text);
            recyclerView = itemView.findViewById(R.id.item_project_top_one_list);

            //增加点击事件
            oneLeftView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition(), mDatas.get(getLayoutPosition()).getListOne().getLeft_btn_board());
                    }
                }
            });
        }
    }

    /**
     * 标签样式2
     */
    public class TwoViewHolder extends RecyclerView.ViewHolder {
        RecyclerView twoRecycler;
        TextView twoText;

        public TwoViewHolder(@NonNull View itemView) {
            super(itemView);
            twoRecycler = itemView.findViewById(R.id.item_project_top_two_recycler);
            twoText = itemView.findViewById(R.id.item_project_top_two_text);

            //更多点击
            twoText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YmStatistics.getInstance().tongjiApp(more_event_params);
                    if (twoRecycler.getAdapter() instanceof ManualPositionTwoAdapter) {
                        ManualPositionTwoAdapter adapter = (ManualPositionTwoAdapter) twoRecycler.getAdapter();
                        adapter.setAn(!adapter.isAn());

                        Drawable drawable;
                        if (adapter.isAn()) {
                            drawable = Utils.getLocalDrawable(mContext, R.drawable.channl_close);
                            twoText.setText("收起");
                        } else {
                            drawable = Utils.getLocalDrawable(mContext, R.drawable.channl_an);
                            twoText.setText("更多");
                        }

                        drawable.setBounds(0, 0, Utils.dip2px(12), Utils.dip2px(12));
                        twoText.setCompoundDrawables(null, null, drawable, null);
                        twoText.setCompoundDrawablePadding(Utils.dip2px(5));

                        adapter.notifyDataSetChanged();
                    }
                }
            });

        }
    }

    /**
     * 标签样式3
     */
    public class ThreeViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView mRecycler;

        public ThreeViewHolder(@NonNull View itemView) {
            super(itemView);
            mRecycler = itemView.findViewById(R.id.item_project_top_three_recycler);
        }
    }

    /**
     * 轮播图
     */
    public class FocusViewHolder extends RecyclerView.ViewHolder {
        HomeBanner mFragmentBanner;

        public FocusViewHolder(@NonNull View itemView) {
            super(itemView);
            mFragmentBanner = itemView.findViewById(R.id.project_details_fragment_banner);

            mFragmentBanner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    List<ProjectFocuMapData> focusImage = mDatas.get(getLayoutPosition()).getFocusImage();

                    if (!TextUtils.isEmpty(focusImage.get(position).getUrl())) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.FOCUS_POSITION, (position + 1) + "", focusImage.get(position).getQid(), "96"), focusImage.get(position).getEvent_params());
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(focusImage.get(position).getUrl(), "0", "0");
                    }
                }
            });
        }
    }

    /**
     * metro手工位
     */
    public class MetroViewHolder extends RecyclerView.ViewHolder {

        private final RecyclerView mMetroRecycler;

        public MetroViewHolder(@NonNull View itemView) {
            super(itemView);
            mMetroRecycler = itemView.findViewById(R.id.item_project_top_metro_recycler);
        }
    }

    /**
     * 超值特卖
     */
    public class HottaoViewHolder extends RecyclerView.ViewHolder {

        private final TextView saleTtle;
        private final RecyclerView mSaleRecycler;

        public HottaoViewHolder(@NonNull View itemView) {
            super(itemView);
            saleTtle = itemView.findViewById(R.id.item_project_top_sale_title);
            mSaleRecycler = itemView.findViewById(R.id.item_project_top_sale_recycler);
        }
    }

    /**
     * 活动上三卡
     */
    public class ActivityViewHolder extends RecyclerView.ViewHolder {

        RecyclerView mFragmentActivity;                                 //活动专场

        public ActivityViewHolder(@NonNull View itemView) {
            super(itemView);
            mFragmentActivity = itemView.findViewById(R.id.item_project_top_activity);
        }
    }

    /**
     * 教你闭坑
     */
    public class ChosenViewHolder extends RecyclerView.ViewHolder {

        private final LinearLayout mCommunityTitleClick;
        private TextView mCommunityTitle;
        private final RecyclerView mCommunityRecycler;

        public ChosenViewHolder(@NonNull View itemView) {
            super(itemView);
            mCommunityTitleClick = itemView.findViewById(R.id.item_project_top_community_title_click);
            mCommunityTitle = itemView.findViewById(R.id.item_project_top_community_title);
            mCommunityRecycler = itemView.findViewById(R.id.item_project_top_community_recycler);

            mCommunityTitleClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String moreUrl = mDatas.get(getLayoutPosition()).getMoreUrl();
                    if (!TextUtils.isEmpty(moreUrl)) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, moreUrl);
                    }
                }
            });
        }
    }

    /**
     * 医生列表
     */
    public class DoctorsViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout mDoctorTitleClick;
        private final TextView mDoctorTitle;
        private final RecyclerView mDoctorRecycler;

        public DoctorsViewHolder(@NonNull View itemView) {
            super(itemView);
            mDoctorTitleClick = itemView.findViewById(R.id.item_project_top_doctor_title_click);
            mDoctorTitle = itemView.findViewById(R.id.item_project_top_doctor_title);
            mDoctorRecycler = itemView.findViewById(R.id.item_project_top_doctor_recycler);

            mDoctorTitleClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String moreUrl = mDatas.get(getLayoutPosition()).getMoreUrl();
                    if (!TextUtils.isEmpty(moreUrl)) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, moreUrl);
                    }
                }
            });
        }
    }

    /**
     * 医院列表
     */
    public class HospitalViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout mHospitalTitleClick;
        private final TextView mHospitalTitle;
        private final RecyclerView mHospitalRecycler;

        public HospitalViewHolder(@NonNull View itemView) {
            super(itemView);
            mHospitalTitleClick = itemView.findViewById(R.id.item_project_top_hospital_title_click);
            mHospitalTitle = itemView.findViewById(R.id.item_project_top_hospital_title);
            mHospitalRecycler = itemView.findViewById(R.id.item_project_top_hospital_recycler);

            mHospitalTitleClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String moreUrl = mDatas.get(getLayoutPosition()).getMoreUrl();
                    if (!TextUtils.isEmpty(moreUrl)) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, moreUrl);
                    }
                }
            });
        }
    }

    public void refreshData(ArrayList<ManualPosition> listDatas) {
        this.mDatas = listDatas;
        notifyDataSetChanged();
    }


    private OnItemClickListener onItemClickListener;

    //点击事件
    public interface OnItemClickListener {
        void onItemClick(View view, int position, ManualPositionBtnBoard data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
