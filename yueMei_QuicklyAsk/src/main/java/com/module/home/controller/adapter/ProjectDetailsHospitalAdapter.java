package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.module.base.view.FunctionManager;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.RecommendHospital;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.HashMap;
import java.util.List;

/**
 * 频道首页医院列表适配器
 * Created by 裴成浩 on 2019/4/3
 */
public class ProjectDetailsHospitalAdapter extends RecyclerView.Adapter<ProjectDetailsHospitalAdapter.ViewHolder> {

    private final Context mContext;
    private final List<RecommendHospital> mDatas;
    private final FunctionManager mFunctionManager;

    public ProjectDetailsHospitalAdapter(Context context, List<RecommendHospital> datas) {
        this.mContext = context;
        this.mDatas = datas;
        mFunctionManager = new FunctionManager(mContext);
    }

    @NonNull
    @Override
    public ProjectDetailsHospitalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_hospital, parent, false);
        return new ProjectDetailsHospitalAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProjectDetailsHospitalAdapter.ViewHolder viewHolder, int position) {

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) viewHolder.mHospital.getLayoutParams();
        if (position == 0) {
            params.leftMargin = Utils.dip2px(20);
            params.rightMargin = Utils.dip2px(5);
        } else if (position == mDatas.size() - 1) {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(20);
        } else {
            params.leftMargin = Utils.dip2px(5);
            params.rightMargin = Utils.dip2px(5);
        }
        viewHolder.mHospital.setLayoutParams(params);

        RecommendHospital data = mDatas.get(position);

        mFunctionManager.setCircleImageSrc(viewHolder.mHospitalImage, data.getImg());
        viewHolder.mHospitalTitle.setText(data.getTitle() + "\n");
        viewHolder.mHospitalPeople.setText(data.getOrder_num() + "人预约过");
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView mHospital;
        ImageView mHospitalImage;
        TextView mHospitalTitle;
        TextView mHospitalPeople;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mHospital = itemView.findViewById(R.id.item_project_details_hospital);
            mHospitalImage = itemView.findViewById(R.id.item_project_details_hospital_img);
            mHospitalTitle = itemView.findViewById(R.id.item_project_details_hospital_title);
            mHospitalPeople = itemView.findViewById(R.id.item_project_details_hospital_people);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RecommendHospital data = mDatas.get(getLayoutPosition());
                    String url = data.getUrl();
                    HashMap<String, String> event_params = data.getEvent_params();
                    if (!TextUtils.isEmpty(url)) {
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CHANNEL_HOSPITAL, (getLayoutPosition() + 1) + ""),event_params);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
                }
            });
        }
    }
}
