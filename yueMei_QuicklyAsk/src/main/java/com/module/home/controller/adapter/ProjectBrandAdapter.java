package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.ProjectBrandList;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2019/2/27
 */
public class ProjectBrandAdapter extends RecyclerView.Adapter<ProjectBrandAdapter.ViewHolder> {

    private final String TAG = "ProjectBrandAdapter";
    private Activity mContext;
    private ArrayList<ProjectBrandList> mDatas;
    private final BaseWebViewClientMessage webViewClientManager;

    public ProjectBrandAdapter(Activity context, ArrayList<ProjectBrandList> datas) {
        this.mContext = context;
        this.mDatas = datas;
        webViewClientManager = new BaseWebViewClientMessage(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_project_details_brand, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.projectDetailsBrand.getLayoutParams();
        if (position == 0) {
            layoutParams.leftMargin = Utils.dip2px(20);
            layoutParams.rightMargin = Utils.dip2px(3.5f);
        } else if (position == mDatas.size() - 1) {
            layoutParams.leftMargin = Utils.dip2px(3.5f);
            layoutParams.rightMargin = Utils.dip2px(20);
        } else {
            layoutParams.leftMargin = Utils.dip2px(3.5f);
            layoutParams.rightMargin = Utils.dip2px(3.5f);
        }

        ProjectBrandList projectBrandList = mDatas.get(position);
        Glide.with(mContext).load(projectBrandList.getBrand_logo()).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.projectBrandImage);
        Log.e(TAG, "projectBrandList.getBrand_name() == " + projectBrandList.getBrand_name());
        viewHolder.projectBrandTitle.setText(projectBrandList.getBrand_name());
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView projectDetailsBrand;
        ImageView projectBrandImage;
        TextView projectBrandTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            projectDetailsBrand = itemView.findViewById(R.id.item_project_details_brand);
            projectBrandImage = itemView.findViewById(R.id.item_project_details_brand_image);
            projectBrandTitle = itemView.findViewById(R.id.item_project_details_brand_title);

            //item点击事件
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProjectBrandList list = mDatas.get(getLayoutPosition());
                    Log.e(TAG, "list.getUrl() == " + list.getUrl());
                    Log.e(TAG, "list.getEvent_params() == " + list.getEvent_params());
                    if (!TextUtils.isEmpty(list.getUrl())) {
                        YmStatistics.getInstance().tongjiApp(list.getEvent_params());
                        webViewClientManager.showWebDetail(list.getUrl());
                    }
                }
            });
        }
    }

}
