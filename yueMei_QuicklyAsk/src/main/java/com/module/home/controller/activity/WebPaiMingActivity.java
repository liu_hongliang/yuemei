package com.module.home.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.baidu.mobstat.StatService;
import com.module.commonview.PageJumpManager;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.commonview.view.webclient.BaseWebViewClientMessage;
import com.module.commonview.view.webclient.WebViewTypeOutside;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.ParserPagramsForWebUrl;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.BaoxianPopWindow;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.HashMap;

/**
 * 排名页面
 * Created by dwb on 17/1/16.
 */
public class WebPaiMingActivity extends BaseActivity {

    private final String TAG = "WebPaiMingActivity";

    @BindView(id = R.id.wan_beautifu_linearlayout3, click = true)
    private LinearLayout contentWeb;

    @BindView(id = R.id.basic_web_top)
    private CommonTopBar mTop;// 返回

    private WebView docDetWeb;

    private Activity mContex;

    public JSONObject obj_http;

    private String url;
    private String title;

    private int bTheight;
    private int windowsH;
    private int statusBarHeight;
    private int windowsW;

    @BindView(id = R.id.title_bar_rly)
    private RelativeLayout biaoView;

    @BindView(id = R.id.all_content)
    private LinearLayout contentLy;

    private BaoxianPopWindow baoxianPop;
    private PageJumpManager pageJumpManager;
    private BaseWebViewClientMessage baseWebViewClientMessage;


    @Override
    public void setRootView() {
        setContentView(R.layout.acty_peifu_basic_web);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex = WebPaiMingActivity.this;

        pageJumpManager = new PageJumpManager(mContex);
        windowsH = Cfg.loadInt(mContex, FinalConstant.WINDOWS_H, 0);
        windowsW = Cfg.loadInt(mContex, FinalConstant.WINDOWS_W, 0);

        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        biaoView.measure(w, h);
        bTheight = biaoView.getMeasuredHeight();

        Rect rectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        statusBarHeight = rectangle.top;


        Intent it = getIntent();
        url = it.getStringExtra("link");
        title = it.getStringExtra("title");

        url = FinalConstant.baseUrl + FinalConstant.VER + url;

        mTop.setCenterText(title);

        setWebViewClientMessage();

        initWebview();
        LodUrl1(url);

        mTop.setLeftViewClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    /**
     * 设置type值跳转
     */
    private void setWebViewClientMessage() {
        baseWebViewClientMessage = new BaseWebViewClientMessage(mContex);
        baseWebViewClientMessage.setTypeOutside(true);
        baseWebViewClientMessage.setWebViewTypeOutside(new WebViewTypeOutside() {
            @Override
            public void typeOutside(WebView view, String url) {
                if (url.startsWith("type")) {
                    baseWebViewClientMessage.showWebDetail(url);
                } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/rank/")) {
                    LodUrl1(url);
                } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/choosecity/")) {
                    LodUrl1(url);
                } else {
                    WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "0", "0");
                }
            }
        });

        baseWebViewClientMessage.setBaseWebViewClientCallback(new BaseWebViewClientCallback() {
            @Override
            public void otherJump(String urlStr) {
                showWebDetail(urlStr);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }


    public void initWebview() {
        docDetWeb = new WebView(mContex);
        docDetWeb.getSettings().setJavaScriptEnabled(true);
        docDetWeb.getSettings().setUseWideViewPort(true);
        docDetWeb.setWebViewClient(new MyWebViewClientMessage());
        docDetWeb.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) docDetWeb.getLayoutParams();
        linearParams.height = windowsH - bTheight - statusBarHeight * 2 - 60;
        linearParams.weight = windowsW;
        docDetWeb.setLayoutParams(linearParams);


        contentWeb.addView(docDetWeb);
    }

    protected void OnReceiveData(String str) {

    }

    public void webReload() {
        if (docDetWeb != null) {
            baseWebViewClientMessage.startLoading();
            docDetWeb.reload();
        }
    }

    /**
     * 处理webview里面的按钮
     *
     * @param urlStr
     */
    public void showWebDetail(String urlStr) {
        Log.e(TAG, urlStr);
        try {
            ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
            parserWebUrl.parserPagrms(urlStr);
            JSONObject obj = parserWebUrl.jsonObject;
            obj_http = obj;

            if (obj.getString("type").equals("1")) {// 医生详情页
                try {
                    String id = obj.getString("id");

                    Intent it = new Intent();
                    it.setClass(mContex, DoctorDetailsActivity592.class);
                    it.putExtra("docId", id);
                    it.putExtra("docName", "");
                    it.putExtra("partId", "");
                    startActivity(it);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (obj.getString("type").equals("6")) {// 问答详情
                String link = obj.getString("link");
                String qid = obj.getString("id");

                Intent it = new Intent();
                it.setClass(mContex, DiariesAndPostsActivity.class);
                it.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER
                        + link);
                it.putExtra("qid", qid);
                startActivity(it);
            }

            if (obj.getString("type").equals("6156")) {// 排行榜弹窗
                String boardtype = obj.getString("boardtype");
                String urlss = FinalConstant.PAIHANGBANG_GUIZE;
                HashMap<String, Object> urlMap = new HashMap<>();
                urlMap.put("boardtype",boardtype);
                baoxianPop = new BaoxianPopWindow(mContex, urlss,urlMap);
                baoxianPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * 加载web
     */
    public void LodUrl1(String urlstr) {
        baseWebViewClientMessage.startLoading();

        WebSignData addressAndHead = SignUtils.getAddressAndHead(urlstr);
        docDetWeb.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
    }

    public class MyWebViewClientMessage extends WebViewClient {
        @SuppressWarnings("deprecation")
        public void onExceededDatabaseQuota(String url,
                                            String databaseIdentifier, long currentQuota,
                                            long estimatedSize, long totalUsedQuota,
                                            WebStorage.QuotaUpdater quotaUpdater) {
            quotaUpdater.updateQuota(estimatedSize * 2);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.startsWith("type")) {
                baseWebViewClientMessage.showWebDetail(url);
            } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/rank/")) {
                LodUrl1(url);
            } else if (url.startsWith(FinalConstant.baseUrl + FinalConstant.VER + "/rank/choosecity/")) {
                LodUrl1(url);
            } else {
                WebUrlTypeUtil.getInstance(mContex).urlToApp(url, "0", "0");
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void toProjectDetailActivity(String projectId) {

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            OnReceiveData("");
            baseWebViewClientMessage.stopLoading();
            super.onPageFinished(view, url);
        }

        @Override
        public void onReceivedSslError(WebView view,
                                       SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}