package com.module.home.controller.other;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.module.commonview.view.SkuVideoChatDialog;

public class SkuVideoChatJSCallBack {

    private SkuVideoChatDialog mVideoChatDialog;

    public SkuVideoChatJSCallBack(SkuVideoChatDialog skuVideoChatDialog) {
        mVideoChatDialog = skuVideoChatDialog;
    }

    @JavascriptInterface
    public void close() {
        mVideoChatDialog.dismiss();
    }
}
