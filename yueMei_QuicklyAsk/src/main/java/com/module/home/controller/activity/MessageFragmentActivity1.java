package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;
import com.module.MainTableActivity;
import com.module.SplashActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.ChatActivity;
import com.module.commonview.module.bean.ChatListBean;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.controller.adapter.SimpleFragmentPagerAdapter;
import com.module.home.fragment.MessageWebViewFragment;
import com.module.home.fragment.XiaoxiFragment;
import com.module.home.model.api.ClearMessageApi;
import com.module.home.model.api.MessagecountApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.NoLoginBean;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.netWork.ServerData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.NewsNumberData;
import com.quicklyask.util.AppShortCutUtil;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.CustomDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;
import com.taobao.accs.utl.UT;
import com.umeng.analytics.MobclickAgent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 新消息界面
 * <p>
 * Created by pch on 17/10/17.
 */
public class MessageFragmentActivity1 extends FragmentActivity {

    private List<String> mPageTitleList = new ArrayList<>();
    private List<Fragment> mFragmentList = new ArrayList<>();
    public List<Integer> mBadgeCountList = new ArrayList<>();

    private SimpleFragmentPagerAdapter mPagerAdapter;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private RelativeLayout mFrameParent;

    private String TAG = "MessageFragment";
    private MessageFragmentActivity1 mContext;
    private int mZongNum;
    private int mySixinNum;
    private int mPinglunNum;
    private int mZanNum;
    private int mGuanzhuNum;
    private int mTongzhiNum;
    private String uid;
    public XiaoxiFragment xiaoxiFragment;
    private FrameLayout mMessageTip;
    private RelativeLayout mTongzhiMessage;
    private RelativeLayout mTongzhiCloseRel;
    private Button mTongzhiOpenBtn;
    private int mVersionCode;
    public static final int MESSAGE_TIP = 100;
    private Handler mHandler = new MyHandler(this);
    private TextView mTitleText;
    private LinearLayout clearMessageClick;
    private TextView clearMessagetext;
    private boolean isDacuBackground = false;//大促背景

    private static class MyHandler extends Handler {
        private final WeakReference<Activity> mActivity;

        public MyHandler(Activity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MessageFragmentActivity1 theyActivity = (MessageFragmentActivity1) mActivity.get();
            if (theyActivity == null) {
                return;
            }
            switch (msg.what) {
                case MESSAGE_TIP:
                    theyActivity.mMessageTip.setVisibility(View.GONE);
                    break;
            }
        }
    }

    public MessageFragmentActivity1() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_fragment1);
        mContext = MessageFragmentActivity1.this;
        uid = Utils.getUid();
        mVersionCode = Cfg.loadInt(mContext, "versionCode", 0);

        initView();
        getMessageNum();
        initFragments();
        setUpTabBadge(MainTableActivity.acquiescencePage);
        MainTableActivity.acquiescencePage = 0;
        initEvent();

        if (MainTableActivity.mBroadcastReceiver != null) {
            MainTableActivity.mBroadcastReceiver.setMessageFragmentActivity1(mContext);
        }

        if (mActivityCallBack != null) {
            mActivityCallBack.activityCallBack(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
        boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
        if (!b) {
            mTongzhiMessage.setVisibility(View.VISIBLE);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT, "1", "0", "1"));
            String tongzhi = Cfg.loadStr(mContext, "tongzhi", "");
            if (TextUtils.isEmpty(tongzhi)) {
                Cfg.saveStr(mContext, "tongzhi", "1");
                final YueMeiDialog yueMeiDialog = new YueMeiDialog(mContext, "检测到您没有打开通知权限，是否去打开?", "取消", "确定");
                yueMeiDialog.setCanceledOnTouchOutside(false);
                yueMeiDialog.show();
                yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
                    @Override
                    public void leftBtnClick() {
                        yueMeiDialog.dismiss();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT, "no", "0", "1"));
                    }

                    @Override
                    public void rightBtnClick() {
                        yueMeiDialog.dismiss();
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.MESSAGE_SHOW_ALERT, "yes", "0", "1"));
                        Intent localIntent = new Intent();
                        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (Build.VERSION.SDK_INT >= 9) {
                            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                            localIntent.setData(Uri.fromParts("package", mContext.getPackageName(), null));
                        } else if (Build.VERSION.SDK_INT <= 8) {
                            localIntent.setAction(Intent.ACTION_VIEW);

                            localIntent.setClassName("com.android.settings",
                                    "com.android.settings.InstalledAppDetails");

                            localIntent.putExtra("com.android.settings.ApplicationPkgName",
                                    mContext.getPackageName());
                        }
                        startActivity(localIntent);

                    }
                });
            }

        } else {
            mTongzhiMessage.setVisibility(View.GONE);
        }
        refreshMessages();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "onRestart");
        if (!Utils.isLogin() && !Utils.noLoginChat()){
            MainTableActivity.mainBottomBar.setCheckedPos(4);
        }else {
            if (!Utils.isLogin()){
                setUpTabBadge(0);
            }
        }

    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Util.isOnMainThread()) {
            Glide.with(getApplicationContext()).pauseRequests();
        }
    }

    /**
     * 初始化ui
     */
    private void initView() {
        mFrameParent = findViewById(R.id.layout_pantent);
        mTitleText = findViewById(R.id.message_title);
        clearMessageClick = findViewById(R.id.message_clear_click);
        clearMessagetext = findViewById(R.id.message_clear_text);
        mTabLayout = findViewById(R.id.tab_layout);
        mViewPager = findViewById(R.id.view_pager);
        mMessageTip = findViewById(R.id.message_tip);
        mTongzhiMessage = findViewById(R.id.myprofile_upsetuser_desc_rl);
        mTongzhiCloseRel = findViewById(R.id.message_close_click);
        mTongzhiOpenBtn = findViewById(R.id.message_opne_btn);
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mFrameParent.getLayoutParams();
        params.height = statusbarHeight + Utils.dip2px(50);
        params.setMargins(0, 0, 0, statusbarHeight / 2);
        mFrameParent.setLayoutParams(params);
        ImageView dacuBackgroundImg = findViewById(R.id.dacu_backgroud);
        AdertAdv.OtherBackgroundImgBean data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "other_background_data", ""), AdertAdv.OtherBackgroundImgBean.class);
        if (data != null && data.getImg() != null) {
            isDacuBackground = true;
            dacuBackgroundImg.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(data.getImg()).into(dacuBackgroundImg);
            mTitleText.setTextColor(Color.WHITE);
            clearMessagetext.setTextColor(Color.WHITE);
        } else {
            QMUIStatusBarHelper.setStatusBarLightMode(mContext);
            dacuBackgroundImg.setVisibility(View.GONE);
        }
        ViewGroup.LayoutParams layoutParams = dacuBackgroundImg.getLayoutParams();
        params.height = statusbarHeight + Utils.dip2px(50);
        dacuBackgroundImg.setLayoutParams(layoutParams);
        if (mVersionCode == 6410) {
            String messageTip = Cfg.loadStr(mContext, "messageTip", "");
            if (messageTip.length() == 0) {
                mMessageTip.setVisibility(View.VISIBLE);
                mHandler.sendEmptyMessageDelayed(MESSAGE_TIP, 4000);
                Cfg.saveStr(mContext, "messageTip", "1");
            }
        }
        mTongzhiCloseRel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTongzhiMessage.setVisibility(View.GONE);
            }
        });
        mTongzhiOpenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent localIntent = new Intent();
                localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (Build.VERSION.SDK_INT >= 9) {
                    localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    localIntent.setData(Uri.fromParts("package", mContext.getPackageName(), null));
                } else if (Build.VERSION.SDK_INT <= 8) {
                    localIntent.setAction(Intent.ACTION_VIEW);

                    localIntent.setClassName("com.android.settings",
                            "com.android.settings.InstalledAppDetails");

                    localIntent.putExtra("com.android.settings.ApplicationPkgName",
                            mContext.getPackageName());
                }
                startActivity(localIntent);
            }
        });
        clearMessageClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearMessagetext.setText("标记中...");
                if (!Utils.isNetworkAvailable(mContext)) {
                    MyToast.makeTextToast2(mContext, "清除未成功，网路中断请稍后再试", MyToast.SHOW_TIME).show();
                    clearMessagetext.setText("标为已读");
                }
                int zong_num = Cfg.loadInt(mContext, FinalConstant.ZONG_ID, 0);
                if (zong_num == 0) {
                    MyToast.makeTextToast2(mContext, "暂无未读消息", MyToast.SHOW_TIME).show();
                    clearMessagetext.setText("标为已读");
                    return;
                }
                new ClearMessageApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData s) {
                        if ("1".equals(s.code)) {
                            Cfg.saveInt(mContext, FinalConstant.ZONG_ID, 0);
                            Cfg.saveInt(mContext, FinalConstant.SIXIN_ID, 0);
                            Cfg.saveInt(mContext, FinalConstant.PINGLUN_ID, 0);
                            Cfg.saveInt(mContext, FinalConstant.ZAN_ID, 0);
                            Cfg.saveInt(mContext, FinalConstant.GUANZHU_ID, 0);
                            Cfg.saveInt(mContext, FinalConstant.NOTICE_ID, 0);
                            MainTableActivity.mainBottomBar.setMessageNum(0);
                            mBadgeCountList.set(0, 0);
                            mBadgeCountList.set(1, 0);
                            mBadgeCountList.set(2, 0);
                            mBadgeCountList.set(3, 0);
                            mBadgeCountList.set(4, 0);
                            setUpTabBadge(0);
                            if (xiaoxiFragment.messageAdapter != null) {
                                xiaoxiFragment.messageAdapter.clearNoread();
                                xiaoxiFragment.messageAdapter.notifyDataSetChanged();
                            }
                            clearMessagetext.setText("标为已读");

                        } else {
                            MyToast.makeTextToast2(mContext, s.message, MyToast.SHOW_TIME).show();
                        }
                    }
                });
            }

        });
    }

    /**
     * 获取消息数
     */
    private void getMessageNum() {
        mZongNum = Cfg.loadInt(mContext, FinalConstant.ZONG_ID, 0);  //总
        mySixinNum = Cfg.loadInt(mContext, FinalConstant.SIXIN_ID, 0);        //私信
        if (Utils.isLogin()){
            mPinglunNum = Cfg.loadInt(mContext, FinalConstant.PINGLUN_ID, 0);           //评论
            mTongzhiNum = Cfg.loadInt(mContext, FinalConstant.NOTICE_ID, 0);         //通知数
            mZanNum = Cfg.loadInt(mContext, FinalConstant.ZAN_ID, 0);         //赞
            mGuanzhuNum = Cfg.loadInt(mContext, FinalConstant.GUANZHU_ID, 0);         //关注
        }
        Log.e(TAG, "mZongNum ==> " + mZongNum);
        Log.e(TAG, "mySixinNum ==> " + mySixinNum);
        Log.e(TAG, "mPinglunNum ==> " + mPinglunNum);
        Log.e(TAG, "mZanNum ==> " + mZanNum);
        Log.e(TAG, "mGuanzhuNum ==> " + mGuanzhuNum);
        Log.e(TAG, "mTongzhiNum ==> " + mTongzhiNum);

    }




    /**
     * 初始化Fragment
     */
    private void initFragments() {
        mPageTitleList.add("私信");
        mPageTitleList.add("评论");
        mPageTitleList.add("赞");
        mPageTitleList.add("粉丝");
        mPageTitleList.add("通知");

        //消息数初始化
        Log.e(TAG, "mySixinNum == " + mySixinNum);
        Log.e(TAG, "mPinglunNum == " + mPinglunNum);
        Log.e(TAG, "mZanNum == " + mZanNum);
        Log.e(TAG, "mGuanzhuNum == " + mGuanzhuNum);
        Log.e(TAG, "mTongzhiNum == " + mTongzhiNum);

        mBadgeCountList.add(mySixinNum);
        mBadgeCountList.add(mPinglunNum);
        mBadgeCountList.add(mZanNum);
        mBadgeCountList.add(mGuanzhuNum);
        mBadgeCountList.add(mTongzhiNum);


        //fragment初始化
        xiaoxiFragment = XiaoxiFragment.getInstance();
        mFragmentList.add(xiaoxiFragment);
        mFragmentList.add(MessageWebViewFragment.newInstance(mContext, FinalConstant.MESSAGEURL, "1"));
        mFragmentList.add(MessageWebViewFragment.newInstance(mContext, FinalConstant.MESSAGEURL, "3"));
        mFragmentList.add(MessageWebViewFragment.newInstance(mContext, FinalConstant.MESSAGEURL, "4"));
        mFragmentList.add(MessageWebViewFragment.newInstance(mContext, FinalConstant.MESSAGEURL, "2"));

        //初始化
        mPagerAdapter = new SimpleFragmentPagerAdapter(mContext, getSupportFragmentManager(), mFragmentList, mPageTitleList, mBadgeCountList,isDacuBackground);
        mViewPager.setAdapter(mPagerAdapter);
        if (isDacuBackground){
            mTabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(mContext,R.color.white));
        }
        mTabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * 设置Tablayout上的标题的角标
     *
     * @param pos
     */
    public void setUpTabBadge(int pos) {
        for (int i = 0; i < mFragmentList.size(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);

            // 更新Badge前,先remove原来的customView,否则Badge无法更新
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(customView);
                }
            }

            // 更新CustomView
            tab.setCustomView(mPagerAdapter.getTabItemView(i));
        }

        // 需加上以下代码,不然会出现更新Tab角标后,选中的Tab字体颜色不是选中状态的颜色
        mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);

        //初始化颜色
        changeTabSelect(mTabLayout.getTabAt(pos));
    }

    public void setUpTabBadge2(int pos) {
        for (int i = 0; i < mFragmentList.size(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);

            // 更新Badge前,先remove原来的customView,否则Badge无法更新
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(customView);
                }
            }

            // 更新CustomView
            tab.setCustomView(mPagerAdapter.getTabItemView(i));
        }

        // 需加上以下代码,不然会出现更新Tab角标后,选中的Tab字体颜色不是选中状态的颜色
        mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);


    }

    /**
     * TabLayout回调监听
     */
    private void initEvent() {
        //TabLayout的配置
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabSelected.....");
                switch (tab.getPosition()) {
                    case 1:
                        if (Utils.isLoginAndBind(mContext)){
                            Cfg.saveInt(mContext, FinalConstant.PINGLUN_ID, 0);
                            mBadgeCountList.set(1, 0);
                            setUpTabBadge(1);
                        }
                        break;
                    case 2:
                        if (Utils.isLoginAndBind(mContext)){
                            Cfg.saveInt(mContext, FinalConstant.ZAN_ID, 0);
                            mBadgeCountList.set(2, 0);
                            setUpTabBadge(2);
                        }
                        break;
                    case 3:
                        if (Utils.isLoginAndBind(mContext)){
                            Cfg.saveInt(mContext, FinalConstant.GUANZHU_ID, 0);
                            mBadgeCountList.set(3, 0);
                            setUpTabBadge(3);
                        }
                        break;
                    case 4:
                        if (Utils.isLoginAndBind(mContext)){
                            Cfg.saveInt(mContext, FinalConstant.NOTICE_ID, 0);
                            mBadgeCountList.set(4, 0);
                            setUpTabBadge(4);
                        }
                        break;
                    default:
                        changeTabSelect(tab);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabUnselected.....");
                changeTabNormal(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.e(TAG, "onTabReselected.....");
            }
        });

    }

    /**
     * 联网刷新未读数
     */
    public void refreshMessages() {
        MessagecountApi messagecountApi = new MessagecountApi();
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);

        messagecountApi.getCallBack(mContext, maps, new BaseCallBackListener<NewsNumberData>() {

            @Override
            public void onSuccess(NewsNumberData newsNumberData) {
                String ask_num = newsNumberData.getAsk_num();//问题未读消息数
                String zong_num = newsNumberData.getZong_num();

                String share_num = newsNumberData.getShare_num();//评论未读消息数
                String notice_num = newsNumberData.getNotice_num();//通知未读消息数
                String sixin_num = newsNumberData.getSixin_num();//私信未读消息数
                String agree_num = newsNumberData.getAgree_num();//赞未读消息数
                String follow_num = newsNumberData.getFollow_num();//关注未读消息数
                Log.e(TAG, "zong_num == " + zong_num);
                Log.e(TAG, "share_num == " + share_num);
                Log.e(TAG, "notice_num == " + notice_num);
                Log.e(TAG, "sixin_num == " + sixin_num);
                Log.e(TAG, "agree_num == " + agree_num);
                Log.e(TAG, "follow_num == " + follow_num);

                int zongNum = Integer.parseInt(zong_num);
                int siXinNum = Integer.parseInt(sixin_num);
                int pingLunNum = Integer.parseInt(share_num);
                int zanNum = Integer.parseInt(agree_num);
                int guanZhuNum = Integer.parseInt(follow_num);
                int noticeNum = Integer.parseInt(notice_num);
                Cfg.saveInt(mContext, FinalConstant.ZONG_ID, zongNum);
                Cfg.saveInt(mContext, FinalConstant.SIXIN_ID, siXinNum);
                Cfg.saveInt(mContext, FinalConstant.PINGLUN_ID, pingLunNum);
                Cfg.saveInt(mContext, FinalConstant.ZAN_ID, zanNum);
                Cfg.saveInt(mContext, FinalConstant.GUANZHU_ID, guanZhuNum);
                Cfg.saveInt(mContext, FinalConstant.NOTICE_ID, noticeNum);

                getMessageNum();
                refreshUnreadMessage();
            }

        });
    }

    int mPos = 0;

    /**
     * 刷新未读消息数
     */
    public void refreshUnreadMessage() {

        int selectedTabPosition = mTabLayout.getSelectedTabPosition();

        if (xiaoxiFragment != null) {
            ChatActivity.setIdCallBack(new ChatActivity.IdCallBack() {
                @Override
                public void idCallBack(String chatId) {
                    if (xiaoxiFragment.messageAdapter != null) {
                        List<ChatListBean.ListBean> chatList = xiaoxiFragment.messageAdapter.getmChatList();
                        for (int i = 0; i < chatList.size(); i++) {
                            String id = chatList.get(i).getId();
                            if (chatId.equals(id)) {
                                mPos = i;
                                break;
                            }
                        }
                        xiaoxiFragment.messageAdapter.setNoread(mPos, 0 + "");
                        xiaoxiFragment.messageAdapter.notifyDataSetChanged();
                    }
                }
            });

        }

        switch (selectedTabPosition) {
            case 0:
                mBadgeCountList.set(0, mySixinNum);
                setUpTabBadge(0);
                break;
            case 1:
                mBadgeCountList.set(1, mPinglunNum);
                setUpTabBadge(1);
                break;
            case 2:
                mBadgeCountList.set(2, mZanNum);
                setUpTabBadge(2);
                break;
            case 3:
                mBadgeCountList.set(3, mGuanzhuNum);
                setUpTabBadge(3);
                break;
            case 4:
                mBadgeCountList.set(4, mTongzhiNum);
                setUpTabBadge(4);
                break;
        }

        homeNumHXListen();
    }

    /**
     * 设置桌面消息数和消息界面消息数
     */
    private void homeNumHXListen() {
        //桌面显示的消息数
        int zzNum = Cfg.loadInt(mContext, FinalConstant.SIXIN_ID, 0)
                + Cfg.loadInt(mContext, FinalConstant.PINGLUN_ID, 0)
                + Cfg.loadInt(mContext, FinalConstant.ZAN_ID, 0)
                + Cfg.loadInt(mContext, FinalConstant.GUANZHU_ID, 0)
                + Cfg.loadInt(mContext, FinalConstant.NOTICE_ID, 0);

        AppShortCutUtil.addNumShortCut(mContext, MainTableActivity.class, true, zzNum + "", true);
        if (Utils.isLogin() && Utils.isBind()) {
            MainTableActivity.mainBottomBar.setMessageNum(zzNum);
        }
    }

    /**
     * 选中消息数样式设置
     *
     * @param tab
     */
    private void changeTabSelect(TabLayout.Tab tab) {
        int position = tab.getPosition();
        mViewPager.setCurrentItem(position);
        View view = tab.getCustomView();
        if (view == null){
            return;
        }
        TextView txt_title = view.findViewById(R.id.textview);
        if (isDacuBackground){
            txt_title.setTextColor(Color.WHITE);
            txt_title.setTypeface(Typeface.DEFAULT_BOLD);
        }else {
            txt_title.setTextColor(ContextCompat.getColor(mContext,R.color.red_ff5c77));
        }


    }


    /**
     * 取消选中消息数样式设置
     *
     * @param tab
     */
    private void changeTabNormal(TabLayout.Tab tab) {
        View view = tab.getCustomView();
        if (view == null){
            return;
        }
        TextView txt_title = view.findViewById(R.id.textview);
        if (isDacuBackground){
            txt_title.setTextColor(Color.WHITE);
            txt_title.setTypeface(Typeface.DEFAULT);
        }else {
            txt_title.setTextColor(Color.BLACK);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(getParent(), R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.base_slide_right_out);
    }

    private static ActivityCallBack mActivityCallBack;

    public static void setmActivityCallBack(ActivityCallBack activityCallBack) {
        mActivityCallBack = activityCallBack;
    }

    public interface ActivityCallBack {
        void activityCallBack(MessageFragmentActivity1 messageFragmentActivity1);
    }


}
