package com.module.home.controller.activity;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.module.home.model.bean.SearchEntry;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class SearchTopView extends FrameLayout {

    private  Context mContext;


    //标题栏
    FrameLayout homeTitle;
    View titleBackground;
    TextView cityTv;
    ImageView taoZxDiquIv1;
    LinearLayout cityHomeLy1;
    RelativeLayout citySelectRly;
    LinearLayout searchClick;
    ViewFlipper mFlipper;
    ImageView inputImage;
    LinearLayout serachRly123;
    ImageView saoSaoIv1;
    TextView homeCartNum;
    RelativeLayout saoerMaRly;

    private List<String> titleLists = new ArrayList<>();       //标题栏文案集合
    private List<String> showTitleLists = new ArrayList<>();       //标题栏文案集合
    private String TAG = "SearchTopView";
    private final String defaultTitle = "搜索你感兴趣的项目";
    public SearchTopView(@NonNull Context context) {
        this(context,null);
    }

    public SearchTopView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public SearchTopView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.mContext = context;
        initView();
    }

    private void initView() {
        setFocusable(true);
        setFocusableInTouchMode(true);

        View topView = View.inflate(mContext, R.layout.search_top_view, this);
        homeTitle = topView.findViewById(R.id.ll_home_title);
        titleBackground = topView.findViewById(R.id.title_background_color);
        cityTv = topView.findViewById(R.id.tab_doc_list_city_tv1);
        taoZxDiquIv1 = topView.findViewById(R.id.tao_zx_diqu_iv1);
        cityHomeLy1 = topView.findViewById(R.id.city_home_ly1);
        citySelectRly = topView.findViewById(R.id.tab_doc_city_rly1);
        searchClick = topView.findViewById(R.id.home_input_edit1_click);
        mFlipper = topView.findViewById(R.id.home_input_edit1);
        inputImage = topView.findViewById(R.id.home_input_image);
        serachRly123 = topView.findViewById(R.id.search_rly_click1);
        saoSaoIv1 = topView.findViewById(R.id.sao_sao_iv1);
        homeCartNum = topView.findViewById(R.id.home_cart_num);
        saoerMaRly = topView.findViewById(R.id.sao_sao_rly1);


        //城市点击事件
        citySelectRly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCityClick(v, getCity());
                }
            }
        });

        //搜索框点击
        searchClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    String titleText = getTitleText();
                    if (defaultTitle.equals(titleText)) {
                        titleText = "";
                    }
                    onEventClickListener.onTitleClick(v, titleText, mFlipper.getDisplayedChild());
                }
            }
        });

        //购物车点击
        saoerMaRly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEventClickListener != null) {
                    onEventClickListener.onCartClick(v);
                }
            }
        });

        setCartNum();
    }

    public void setColor(String type){
        if(type.equals("0")){
            cityTv.setTextColor(Color.BLACK);
            taoZxDiquIv1.setBackgroundResource(R.drawable.jiantou_down_balck_2x2);
            saoSaoIv1.setImageResource(R.drawable.shop_car_black);
        }else{
            cityTv.setTextColor(Color.WHITE);
            taoZxDiquIv1.setBackgroundResource(R.drawable.shop_home_city_bottom);
            saoSaoIv1.setImageResource(R.drawable.shop_home_car_black);
        }

    }

    /***
     * 设置定位城市
     */
    public boolean setCity() {
        boolean isRefresh = false;
        if (Utils.getCity().length() > 0) {
            if (Utils.getCity().equals("失败")) {
                cityTv.setText("全国");

            } else if (!Utils.getCity().equals(getCity())) {
                cityTv.setText(Utils.getCity());
                isRefresh = true;
            }
        } else {
            cityTv.setText("全国");
        }
        return isRefresh;
    }

    /**
     * 获取选中城市
     *
     * @return
     */
    private String getCity() {
        return cityTv.getText().toString();
    }

    /**
     * 设置搜索框文案
     *
     * @param datas
     */
    public void setTitleText(List<SearchEntry> datas) {
        titleLists.clear();
        showTitleLists.clear();
        for (SearchEntry data : datas) {
            titleLists.add(data.getSearch_title());
            showTitleLists.add(data.getShow_title());
        }
        mFlipper.removeAllViews();
        if (titleLists.size() == 0) {
            titleLists.add(defaultTitle);
        }
        for (final String data : showTitleLists) {
            TextView textView = new TextView(mContext);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            textView.setTextColor(Utils.getLocalColor(mContext, R.color._88));
            textView.setText(data);
            textView.setLines(1);
            textView.setGravity(Gravity.CENTER);
            mFlipper.addView(textView);
        }

        if (titleLists.size() > 1) {
            mFlipper.setFlipInterval(1000 * 10);
            mFlipper.startFlipping();
        }
    }

    /**
     * 获取搜索框文案
     */
    private String getTitleText() {
        if (titleLists.size() > 0) {
            return titleLists.get(mFlipper.getDisplayedChild());
        } else {
            return "";
        }
    }

    /**
     * 设置购物车数量
     */
    public void setCartNum() {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            homeCartNum.setVisibility(View.VISIBLE);
            homeCartNum.setText(cartNumber);
        } else {
            homeCartNum.setVisibility(View.GONE);
        }
    }

    public interface OnEventClickListener {
        void onCityClick(View v, String city);                      //城市点击回调

        void onTitleClick(View v, String key, int pos);              //搜索框点击回调

        void onCartClick(View v);                                   //购物车点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
