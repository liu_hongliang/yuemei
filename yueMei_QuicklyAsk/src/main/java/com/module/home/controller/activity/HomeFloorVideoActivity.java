package com.module.home.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.module.base.view.YMBaseActivity;
import com.quicklyask.activity.R;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.NothingVideoView;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.cache.CacheFactory;
import com.shuyu.gsyvideoplayer.cache.ProxyCacheManager;
import com.shuyu.gsyvideoplayer.listener.VideoAllCallBack;
import com.shuyu.gsyvideoplayer.player.PlayerFactory;
import com.shuyu.gsyvideoplayer.player.SystemPlayerManager;
import com.shuyu.gsyvideoplayer.utils.GSYVideoType;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
import org.xutils.common.util.DensityUtil;
import java.util.HashMap;

/**
 * 文 件 名: HomeFloorVideoActivity
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-14 00:01
 * 邮   箱: 188897876@qq.com
 * 修改备注：首页二楼视频页面
 */

public class HomeFloorVideoActivity extends YMBaseActivity {
    private NothingVideoView videoView;
    private ImageView iv_cover;
    private ImageView iv_close;
    private FrameLayout fl_video;
    private String path;
    private String jumpPath;
    private String coverImg;
    private String h_w;
    private boolean isPlay, isPause;
    private OrientationUtils orientationUtils;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home_floor_video;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void initView() {

        path = getIntent().getExtras().getString("VIDEO_PATH");
        jumpPath = getIntent().getExtras().getString("JUMP_PATH");
        coverImg = getIntent().getExtras().getString("COVER_IMG");
        h_w = getIntent().getExtras().getString("H_W");
        videoView = findViewById(R.id.videoView);
        iv_cover = findViewById(R.id.iv_cover);
        iv_close = findViewById(R.id.iv_close);
        fl_video = findViewById(R.id.fl_video);

        //外部辅助的旋转，帮助全屏
        orientationUtils = new OrientationUtils(this, videoView);
        //初始化不打开外部的旋转
        orientationUtils.setEnable(false);


        if (!TextUtils.isEmpty(coverImg) && !TextUtils.isEmpty(h_w)) {
            Glide.with(mContext).load(coverImg).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    ViewGroup.LayoutParams params = iv_cover.getLayoutParams();
                    params.width = DensityUtil.getScreenWidth();
                    params.height = (int) (DensityUtil.getScreenWidth() * Float.parseFloat(h_w));
                    iv_cover.setLayoutParams(params);
                    iv_cover.setImageDrawable(resource);
                }
            });
        } else {
            ViewGroup.LayoutParams params = iv_cover.getLayoutParams();
            params.width = DensityUtil.getScreenWidth();
            params.height = DensityUtil.getScreenHeight();
            iv_cover.setLayoutParams(params);
            iv_cover.setImageBitmap(getNetVideoBitmap(path));
        }

        if (!TextUtils.isEmpty(h_w)) {
            videoView.getGSYVideoManager().setCurrentVideoWidth(DensityUtil.getScreenWidth());
            videoView.getGSYVideoManager().setCurrentVideoHeight((int) (DensityUtil.getScreenWidth() * Float.parseFloat(h_w)));
        } else {
            videoView.getGSYVideoManager().setCurrentVideoWidth(DensityUtil.getScreenWidth());
//            videoView.getGSYVideoManager().setCurrentVideoHeight(DensityUtil.getScreenHeight());
        }

        if (!TextUtils.isEmpty(path)) {
            startVideo(path,videoView);
        }
        videoView.setVideoAllCallBack(new VideoAllCallBack() {
            @Override
            public void onStartPrepared(String url, Object... objects) {
                iv_cover.animate().alpha(1).setDuration(200);
            }

            @Override
            public void onPrepared(String url, Object... objects) {
                //开始播放回调
                //开始播放了才能旋转和全屏
                orientationUtils.setEnable(true);
                isPlay = true;
                iv_cover.animate().alpha(0).setDuration(200);
            }

            @Override
            public void onClickStartIcon(String url, Object... objects) {

            }

            @Override
            public void onClickStartError(String url, Object... objects) {

            }

            @Override
            public void onClickStop(String url, Object... objects) {

            }

            @Override
            public void onClickStopFullscreen(String url, Object... objects) {

            }

            @Override
            public void onClickResume(String url, Object... objects) {

            }

            @Override
            public void onClickResumeFullscreen(String url, Object... objects) {

            }

            @Override
            public void onClickSeekbar(String url, Object... objects) {

            }

            @Override
            public void onClickSeekbarFullscreen(String url, Object... objects) {

            }

            @Override
            public void onAutoComplete(String url, Object... objects) {
                iv_cover.animate().alpha(1).setDuration(200);
                videoView.startPlayLogic();
            }

            @Override
            public void onEnterFullscreen(String url, Object... objects) {

            }

            @Override
            public void onQuitFullscreen(String url, Object... objects) {
                if (orientationUtils != null) {
                    orientationUtils.backToProtVideo();
                }
            }

            @Override
            public void onQuitSmallWidget(String url, Object... objects) {

            }

            @Override
            public void onEnterSmallWidget(String url, Object... objects) {

            }

            @Override
            public void onTouchScreenSeekVolume(String url, Object... objects) {

            }

            @Override
            public void onTouchScreenSeekPosition(String url, Object... objects) {

            }

            @Override
            public void onTouchScreenSeekLight(String url, Object... objects) {

            }

            @Override
            public void onPlayError(String url, Object... objects) {

            }

            @Override
            public void onClickStartThumb(String url, Object... objects) {

            }

            @Override
            public void onClickBlank(String url, Object... objects) {

            }

            @Override
            public void onClickBlankFullscreen(String url, Object... objects) {

            }
        });
        iv_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(jumpPath);
                finish();
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void startVideo(String video_url, NothingVideoView video) {
        GSYVideoType.setShowType(GSYVideoType.SCREEN_TYPE_DEFAULT);
        PlayerFactory.setPlayManager(SystemPlayerManager.class);
        CacheFactory.setCacheManager(ProxyCacheManager.class);
        GSYVideoType.setRenderType(GSYVideoType.SUFRACE);
        video.getCurrentPlayer().setUp(video_url, true, "");
        video.getCurrentPlayer().startPlayLogic();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
        isPause = true;

    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.onResume();
        isPause = false;

    }

    @Override
    public void onBackPressed() {
        if (orientationUtils != null) {
            orientationUtils.backToProtVideo();
        }
        if (GSYVideoManager.backFromWindowFull(this)) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isPlay) {
            videoView.getCurrentPlayer().release();
        }
        if (orientationUtils != null){
            orientationUtils.releaseListener();
        }
        GSYVideoManager.releaseAllVideos();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //如果旋转了就全屏
        if (isPlay && !isPause) {
            if (newConfig.orientation == ActivityInfo.SCREEN_ORIENTATION_USER) {
                if (!videoView.getCurrentPlayer().isIfCurrentIsFullscreen()) {
                    videoView.getCurrentPlayer().startWindowFullscreen(HomeFloorVideoActivity.this, true, true);
                }
            } else {
                if (videoView.getCurrentPlayer().isIfCurrentIsFullscreen()) {
                    GSYVideoManager.backFromWindowFull(this);
                }
                if (orientationUtils != null) {
                    orientationUtils.setEnable(true);
                }
            }
        }

    }

    //    获取网络视频第一帧
    public Bitmap getNetVideoBitmap(String videoUrl) {
        Bitmap bitmap = null;

        android.media.MediaMetadataRetriever mmr = new android.media.MediaMetadataRetriever();
        try {
            //根据url获取缩略图
            mmr.setDataSource(videoUrl, new HashMap());
            //获得第一帧图片
            bitmap = mmr.getFrameAtTime();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } finally {
            mmr.release();
        }
        return bitmap;
    }



    public static void start(Context context, String destVideoPath, String jumpPath, String coverImg, String h_w) {
        Intent intent = new Intent(context, HomeFloorVideoActivity.class);
        intent.putExtra("VIDEO_PATH", destVideoPath);
        intent.putExtra("JUMP_PATH", jumpPath);
        intent.putExtra("COVER_IMG", coverImg);
        intent.putExtra("H_W", h_w);
        context.startActivity(intent);
    }
}
