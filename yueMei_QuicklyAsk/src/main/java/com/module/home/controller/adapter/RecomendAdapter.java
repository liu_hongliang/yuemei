package com.module.home.controller.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.commonview.module.bean.ChatListBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.util.List;

public class RecomendAdapter extends BaseQuickAdapter<ChatListBean.RecomendBean.ListBean,BaseViewHolder> {
    public RecomendAdapter(int layoutResId, @Nullable List<ChatListBean.RecomendBean.ListBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ChatListBean.RecomendBean.ListBean item) {
        Glide.with(mContext).load(item.getUserImg())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ease_default_avatar)
                .error(R.drawable.ease_default_avatar)
                .into((ImageView) helper.getView(R.id.chat_list_img));
        helper.setText(R.id.chat_list_title,item.getHos_name())
                .setText(R.id.chat_list_zixun,item.getChatUserNum()+"人咨询");

    }
}
