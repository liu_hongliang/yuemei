package com.module.home.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.baidu.mobstat.StatService;
import com.module.SearchProjectActivity;
import com.module.api.LodPart1DataApi;
import com.module.base.api.BaseCallBackListener;
import com.module.home.controller.adapter.MyAdapter;
import com.module.home.view.LoadingProgress;
import com.module.other.activity.ProjectGridFragment;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.CustomDialog;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;
import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 桌面 项目首页
 * 
 * @author Rubin
 * 
 */
public class TabProjectActivity extends FragmentActivity {

	private final String TAG = "TabProjectActivity";

	private ListView mlist;
	private ProjectGridFragment mFragment;
	public static int mPosition;
	private List<TaoPopItemData> lvGroupData = new ArrayList<TaoPopItemData>();
	public String[] strName;
	private Handler mHandler;
	private MyAdapter adapter;
	private EditText searchEdit;

	public LoadingProgress mDalog;
	private TabProjectActivity mContext;

	private LinearLayout nowifiLy;
	private Button refreshBt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_acty_project);
		mContext = TabProjectActivity.this;
		nowifiLy = findViewById(R.id.no_wifi_ly1);
		refreshBt = findViewById(R.id.refresh_bt);

		mDalog = new LoadingProgress(mContext);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		findView();

		MobclickAgent.onResume(this);
		StatService.onResume(this);
		TCAgent.onResume(this);

		if (SystemTool.checkNet(mContext)) {
			nowifiLy.setVisibility(View.GONE);
		} else {
			nowifiLy.setVisibility(View.VISIBLE);
		}

		refreshBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mDalog.startLoading();
				new CountDownTimer(2000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

					@Override
					public void onTick(long millisUntilFinished) {

					}

					@Override
					public void onFinish() {
						if (SystemTool.checkNet(mContext)) {
							mDalog.stopLoading();
							nowifiLy.setVisibility(View.GONE);
							findView();
						} else {
							mDalog.stopLoading();
							nowifiLy.setVisibility(View.VISIBLE);
							ViewInject.toast(getResources().getString(
									R.string.no_wifi_tips));
						}
					}
				}.start();
			}
		});

	}

	void findView() {
		mlist = findViewById(R.id.tab_project_listview);
		searchEdit = findViewById(R.id.project_input_edit);
		searchEdit.clearFocus();
		searchEdit.setCursorVisible(false);
		searchEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent it = new Intent();
				it.setClass(TabProjectActivity.this,
						SearchProjectActivity.class);
				startActivity(it);
			}
		});
		mHandler = getHandler();
		lodPart1Data();
		mPosition = 0;
		mlist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// 拿到当前位置
				mPosition = pos;
				adapter = new MyAdapter(TabProjectActivity.this, strName);
				mlist.setAdapter(adapter);
				mlist.setSelection(pos);
				// 即使刷新adapter
				adapter.notifyDataSetChanged();
				mFragment = new ProjectGridFragment();
				FragmentTransaction fragmentTransaction = getSupportFragmentManager()
						.beginTransaction();
				fragmentTransaction.replace(R.id.fragment_container, mFragment);
				// 通过bundle传值给MyFragment
				Bundle bundle = new Bundle();
				bundle.putString("id", lvGroupData.get(pos).get_id());
				mFragment.setArguments(bundle);
				fragmentTransaction.commitAllowingStateLoss();
			}
		});
	}

	void lodPart1Data() {

		new Thread(new Runnable() {
			@Override
			public void run() {
				new LodPart1DataApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
					@Override
					public void onSuccess(ServerData serverData) {
						if ("1".equals(serverData.code)){
							lvGroupData= JSONUtil.jsonToArrayList(serverData.data,TaoPopItemData.class);
							Message message = mHandler.obtainMessage(1);
							message.sendToTarget();
						}
					}
				});
				//lvGroupData = HttpData.loadHomeTopcData1();



			}
		}).start();
	}

	private Handler getHandler() {
		return new Handler() {
			@SuppressLint("NewApi")
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 1:
					if (lvGroupData != null) {
						// lvGroupData.remove(0);
						int num = lvGroupData.size();
						strName = new String[num];

						for (int i = 0; i < num; i++) {
							String partNameStr = lvGroupData.get(i).getName();
							strName[i] = partNameStr;
						}

						adapter = new MyAdapter(TabProjectActivity.this,
								strName);
						mlist.setAdapter(adapter);

						// 创建MyFragment对象
						mFragment = new ProjectGridFragment();
						FragmentTransaction fragmentTransaction = getSupportFragmentManager()
								.beginTransaction();
						fragmentTransaction.replace(R.id.fragment_container,
								mFragment);
						// 通过bundle传值给MyFragment
						Bundle bundle = new Bundle();
						bundle.putString("id", lvGroupData.get(mPosition)
								.get_id());
						mFragment.setArguments(bundle);
						fragmentTransaction.commitAllowingStateLoss();
					} else {
						ViewInject.toast(getResources().getString(
								R.string.no_wifi_tips));
					}
					break;
				}
			}
		};
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			// ViewInject.create().getExitDialog(this);
			final CustomDialog dialog = new CustomDialog(this, R.style.mystyle,
					R.layout.customdialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		StatService.onPause(this);
		TCAgent.onPause(this);
	}
}
