package com.module.home.controller.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.adapter.DiaryRecyclerAdapter;
import com.module.commonview.module.bean.DiariesDeleteData;
import com.module.commonview.module.bean.DiariesReportLikeData;
import com.module.home.model.bean.HotBean;
import com.quicklyask.activity.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/18
 */
public class HomeSaleListAdapter extends RecyclerView.Adapter<HomeSaleListAdapter.ViewHolder> {

    private Activity mContext;
    private List<HotBean> mDatas;
    private final LayoutInflater mInflater;

    public HomeSaleListAdapter(Activity context, List<HotBean> hotData) {
        this.mContext = context;
        this.mDatas = hotData;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public HomeSaleListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.home_today_sale, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeSaleListAdapter.ViewHolder viewHolder, int i) {
        HotBean hotBean = mDatas.get(i);
        String imgUrl = hotBean.getImg();
        if (null != imgUrl && imgUrl.length() > 0) {

            Glide.with(mContext).load(imgUrl).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(viewHolder.salePosters);
        }
        viewHolder.saletitle.setText(hotBean.getLable());
        viewHolder.saleContent.setText(hotBean.getTitle());
        viewHolder.salePrice.setText(hotBean.getPrice());
        String price_x = hotBean.getPrice_x();
        if ("0".equals(price_x)) {
            viewHolder.saleOriginalDiscount.setVisibility(View.GONE);
        } else {
            viewHolder.saleOriginalDiscount.setVisibility(View.VISIBLE);
            DecimalFormat fnum = new DecimalFormat("##0.0");
            viewHolder.saleOriginalDiscount.setText(fnum.format(Float.parseFloat(hotBean.getPrice()) / Float.parseFloat(price_x) * 10) + "折");
        }
        String member_price = hotBean.getMember_price();
        int vipPrice = Integer.parseInt(member_price);
        if (vipPrice >= 0) {
            viewHolder.plusVisibity.setVisibility(View.VISIBLE);
            viewHolder.plusPrice.setText("¥" + member_price);
        } else {
            viewHolder.plusVisibity.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView salePosters;
        TextView saletitle;
        TextView saleContent;
        TextView salePrice;
        TextView saleOriginalDiscount;
        LinearLayout plusVisibity;
        TextView plusPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            salePosters = itemView.findViewById(R.id.iv_sale_posters);
            saletitle = itemView.findViewById(R.id.tv_sale_title);
            saleContent = itemView.findViewById(R.id.tv_sale_content);
            salePrice = itemView.findViewById(R.id.tv_sale_price);
            saleOriginalDiscount = itemView.findViewById(R.id.tv_sale_discount);
            plusVisibity = itemView.findViewById(R.id.tao_plus_vibility);
            plusPrice = itemView.findViewById(R.id.tao_plus_price);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onEventClickListener != null){
                        onEventClickListener.onItemClick(v,getLayoutPosition(),mDatas.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    public interface OnEventClickListener {
        void onItemClick(View v, int pos,HotBean data);                      //item点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
