package com.module.home.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.PageJumpManager;
import com.module.commonview.module.api.DaiJinJuanApi;
import com.module.doctor.model.api.SaoPayApi;
import com.module.home.model.bean.SaozfData;
import com.module.my.controller.activity.LoginBackActivity;
import com.module.my.view.orderpay.OrdePaySaoActivity;
import com.module.my.view.orderpay.SaoOrderMessageActivity;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.zbar.lib.CameraManager;
import com.zbar.lib.CameraPreview;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.kymjs.kjframe.utils.CipherUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ScanCaptureAct extends Activity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private CameraManager mCameraManager;
    private PageJumpManager pageJumpManager;

    private TextView scanResult;
    private FrameLayout scanPreview;
    private Button scanRestart;
    private RelativeLayout scanContainer;
    private RelativeLayout scanCropView;
    private ImageView scanLine;

    private Rect mCropRect = null;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private ImageScanner mImageScanner = null;

    private ScanCaptureAct mContex;
    private String uid;

    private final int BACK = 888;
    private String price;
    private String doc_assistant_id;
    private String doctor_id;
    private String sign;
    private String signEq;

    private RelativeLayout back;

    static {
        System.loadLibrary("iconv");
    }

    private String TAG = "ScanCaptureAct";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_zbar_scan_capture);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mContex = ScanCaptureAct.this;
        uid = Utils.getUid();
        pageJumpManager = new PageJumpManager(mContex);
        findViewById();
        addEvents();
        initViews();
    }

    private void findViewById() {
        scanPreview = findViewById(R.id.capture_preview);
        scanResult = findViewById(R.id.capture_scan_result);
        scanRestart = findViewById(R.id.capture_restart_scan);
        scanContainer = findViewById(R.id.capture_container);
        scanCropView = findViewById(R.id.capture_crop_view);
        scanLine = findViewById(R.id.capture_scan_line);

        back = findViewById(R.id.set_back);
        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void addEvents() {
        scanRestart.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (barcodeScanned) {
                    barcodeScanned = false;
                    scanResult.setText("Scanning...");
                    mCamera.setPreviewCallback(previewCb);
                    mCamera.startPreview();
                    previewing = true;
                    mCamera.autoFocus(autoFocusCB);
                }
            }
        });
    }

    private void initViews() {
        mImageScanner = new ImageScanner();
        mImageScanner.setConfig(0, Config.X_DENSITY, 3);
        mImageScanner.setConfig(0, Config.Y_DENSITY, 3);

        autoFocusHandler = new Handler();
        mCameraManager = new CameraManager(this);
        try {
            mCameraManager.openDriver();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //调整扫描框大小,自适应屏幕
        Display display = this.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();

        RelativeLayout.LayoutParams linearParams = (RelativeLayout.LayoutParams) scanCropView.getLayoutParams();
        linearParams.height = (int) (width * 0.8);
        linearParams.width = (int) (width * 0.8);
        scanCropView.setLayoutParams(linearParams);
        //**

        mCamera = mCameraManager.getCamera();
        mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
        scanPreview.addView(mPreview);

        TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.85f);
        animation.setDuration(5000);
        animation.setRepeatCount(-1);
        animation.setRepeatMode(Animation.REVERSE);
        scanLine.startAnimation(animation);
    }

    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.lock();
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing) mCamera.autoFocus(autoFocusCB);
        }
    };

    PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Size size = camera.getParameters().getPreviewSize();

            // 这里需要将获取的data翻转一下，因为相机默认拿的的横屏的数据
            byte[] rotatedData = new byte[data.length];
            for (int y = 0; y < size.height; y++) {
                for (int x = 0; x < size.width; x++)
                    rotatedData[x * size.height + size.height - y - 1] = data[x + y * size.width];
            }

            // 宽高也要调整
            int tmp = size.width;
            size.width = size.height;
            size.height = tmp;

            initCrop();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(rotatedData);
            barcode.setCrop(mCropRect.left, mCropRect.top, mCropRect.width(), mCropRect.height());

            int result = mImageScanner.scanImage(barcode);
            String resultStr = null;

            if (result != 0) {
                SymbolSet syms = mImageScanner.getResults();
                for (Symbol sym : syms) {
                    resultStr = sym.getData();
                }
            }

            if (!TextUtils.isEmpty(resultStr)) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                releaseCamera();
                barcodeScanned = true;

                toSkip(resultStr);

            }
        }
    };

    AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };


    void toSkip(String result) {
        Log.e(TAG, "result == " + result);
        String sid = "";
        if (result.length() > 0) {
            if (result.contains("user.yuemei.com/userpay/pay/")) {// 扫描支付
                saoPay(result);
            } else if (result.startsWith("user.yuemei.com/userpay/index/")) {
                toPay(result);
            } else if (result.contains("user.yuemei.com/coupons/saomalingquan/")) {// 扫二维码领取
                sid = getStringNum(result);
                uid = Utils.getUid();
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        getDaijinjuanData(sid);
                    } else {
                        Utils.jumpBindingPhone(mContex);
                    }

                } else {
                    Utils.jumpLogin(mContex);
                }

            } else {
                WebUrlTypeUtil.getInstance(mContex).urlToApp(result, "0", "0");
                finish();
            }
        }
    }

    void saoPay(String result) {

        String orderid = getStringNum(result);
        Map<String, Object> maps = new HashMap<>();
        maps.put("order_id", orderid);
        new SaoPayApi().getCallBack(mContex, maps, new BaseCallBackListener<SaozfData>() {
            @Override
            public void onSuccess(SaozfData saozfData) {
                String price = saozfData.getPrice();
                String order_id = saozfData.getOrder_id();
                String title = saozfData.getTao_name();

                Intent it2 = new Intent();
                it2.putExtra("price", price);
                it2.putExtra("server_id", "0");
                it2.putExtra("order_id", order_id);
                it2.putExtra("tao_title", title);
                it2.putExtra("order_time", "30m");
                it2.setClass(mContex, OrdePaySaoActivity.class);
                startActivity(it2);

                // Cfg.saveStr(mContex, "server_id", server_id);
                Cfg.saveStr(mContex, "order_id", order_id);
                Cfg.saveStr(mContex, "taotitle", title);
                Cfg.saveStr(mContex, "price", price);

                finish();

            }
        });
    }


    /**
     * 跳转支付界面
     */
    void toPay(String ree) {

        String ss = ree.replace("http://user.yuemei.com/userpay/index/?", "");

        String[] group = ss.split("&");

        int si = group.length;
        for (int i = 0; i < si; i++) {
            if (group[i].contains("price")) {
                price = group[i].replace("price=", "");
            } else if (group[i].contains("doc_assistant_id")) {
                doc_assistant_id = group[i].replace("doc_assistant_id=", "");
            } else if (group[i].contains("doctor_id")) {
                doctor_id = group[i].replace("doctor_id=", "");
            } else if (group[i].contains("sign")) {
                sign = group[i].replace("sign=", "");
            }

        }

        signEq = CipherUtils.md5(price + doc_assistant_id + doctor_id + "yuemei");

        String uid = Utils.getUid();

        if (Utils.isLogin()) {
            if (sign.equals(signEq)) {

                Intent it = new Intent();
                it.putExtra("price", price);
                it.putExtra("doc_assistant_id", doc_assistant_id);
                it.putExtra("doctor_id", doctor_id);
                it.setClass(getApplicationContext(), SaoOrderMessageActivity.class);
                startActivity(it);

                finish();
            } else {
                MyToast.makeTextToast(getApplicationContext(), "非法操作", 1000).show();
            }
        } else {
            Intent it3 = new Intent();
            it3.setClass(getApplicationContext(), LoginBackActivity.class);
            startActivityForResult(it3, BACK);
        }
    }


    public String getStringNum(String zifuchuang) {
        //https://m.yuemei.com/tao_zt/5407.html?u=9100078
        String aStr = zifuchuang;
        if (zifuchuang.contains("https")) {
            aStr = aStr.replace("https://", "");
        } else {
            aStr = aStr.replace("http://", "");
        }
        String[] aSz = aStr.split("/");

        if (null != aSz[2] && aSz[2].length() > 0) {
            if (aSz[2].contains(".html")) {
                String replace = aSz[2].replace(".html", "");
                if (replace.contains("?u=")) { //5407?u=9100078
                    String[] split = replace.split("\\?u=");
                    return split[0];
                } else {
                    return replace;
                }
            } else {
                return aSz[2];
            }

        } else {
            return 0 + "";
        }

    }


    void getDaijinjuanData(String id) {
        Map<String, Object> maps = new HashMap<>();
        maps.put("uid", uid);
        maps.put("id", id);
        new DaiJinJuanApi().getCallBack(mContex, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "serverData === " + serverData.data);
                if ("1".equals(serverData.code)) {
                    showDialogLingquSuecss();
                } else {
                    MyToast.makeTextToast(mContex, serverData.message, 1000).show();
                }
            }
        });
    }

    Button trueBt;

    void showDialogLingquSuecss() {
        final EditExitDialog editDialog = new EditExitDialog(mContex, R.style.mystyle, R.layout.dialog_lingqu_daijinjuan1);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();

        trueBt = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                editDialog.dismiss();
                finish();
            }
        });
    }

    /**
     * 初始化截取的矩形区域
     */
    private void initCrop() {
        int cameraWidth = mCameraManager.getCameraResolution().y;
        int cameraHeight = mCameraManager.getCameraResolution().x;

        /** 获取布局中扫描框的位置信息 */
        int[] location = new int[2];
        scanCropView.getLocationInWindow(location);

        int cropLeft = location[0];
        int cropTop = location[1] - getStatusBarHeight();

        int cropWidth = scanCropView.getWidth();
        int cropHeight = scanCropView.getHeight();

        /** 获取布局容器的宽高 */
        int containerWidth = scanContainer.getWidth();
        int containerHeight = scanContainer.getHeight();

        /** 计算最终截取的矩形的左上角顶点x坐标 */
        int x = cropLeft * cameraWidth / containerWidth;
        /** 计算最终截取的矩形的左上角顶点y坐标 */
        int y = cropTop * cameraHeight / containerHeight;

        /** 计算最终截取的矩形的宽度 */
        int width = cropWidth * cameraWidth / containerWidth;
        /** 计算最终截取的矩形的高度 */
        int height = cropHeight * cameraHeight / containerHeight;

        /** 生成最终的截取的矩形 */
        mCropRect = new Rect(x, y, width + x, height + y);
    }

    private int getStatusBarHeight() {
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            int x = Integer.parseInt(field.get(obj).toString());
            return getResources().getDimensionPixelSize(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }
}
