package com.module.home.controller.adapter;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.home.model.bean.ManualPositionBtnBoard;
import com.module.home.model.bean.ManualPositionTag;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

/**
 * @author 裴成浩
 * @data 2019/11/7
 */
public class ManualPositionTwoAdapter extends RecyclerView.Adapter<ManualPositionTwoAdapter.ViewHolder> {

    private final Activity mContext;
    private final LayoutInflater mInflater;
    private final int moreShowNum;
    private final List<ManualPositionBtnBoard> boardList;
    private boolean isAn = false;           //是否是展开的
    private int parentMargin = 0;
    private String TAG = "ManualPositionTwoAdapter";

    public ManualPositionTwoAdapter(Activity context, ManualPositionTag listTwo, int parentMargin) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        moreShowNum = Integer.parseInt(listTwo.getMore_show_num());
        boardList = listTwo.getBoard_list();
        this.parentMargin = parentMargin;
    }

    @NonNull
    @Override
    public ManualPositionTwoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.project_list_item_txt2, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ManualPositionTwoAdapter.ViewHolder viewHolder, int i) {
        ManualPositionBtnBoard data = boardList.get(i);
        List<String> back_color = data.getBack_color();

        setGradientBackground(viewHolder.itemTxt, back_color);
        viewHolder.itemTxt.setText(data.getTitle());

        setItemSize(viewHolder, i);
    }

    /**
     * 设置item的Size
     *
     * @param viewHolder
     * @param i
     */
    private void setItemSize(@NonNull ViewHolder viewHolder, int i) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.itemTxt.getLayoutParams();

        viewHolder.itemTxt.setPadding(0, 0, 0, 0);
        int itemWidth = (Utils.getScreenSize(mContext)[0] - parentMargin - ((layoutParams.leftMargin + layoutParams.rightMargin) * 3)) / 3;
        layoutParams.width = itemWidth;
        layoutParams.height = itemWidth * 40 / 109;
    }

    @Override
    public int getItemCount() {
        int size = boardList.size();
        if (size > moreShowNum) {
            return isAn ? size : moreShowNum;
        }
        return size;
    }


    /**
     * 设置渐变背景
     *
     * @param view             :要设置渐变色组件
     * @param back_color：渐变色颜色
     */
    private void setGradientBackground(View view, List<String> back_color) {
        try {
            if (back_color.size() >= 2) {
                String colorLeft = back_color.get(0);
                String colorRight = back_color.get(1);
                if (colorLeft.startsWith("#") && colorRight.startsWith("#")) {
                    GradientDrawable aDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Utils.setCustomColor(colorLeft), Utils.setCustomColor(colorRight)});
                    aDrawable.setCornerRadius(Utils.dip2px(6));
                    view.setBackground(aDrawable);
                } else {
                    if (colorLeft.startsWith("#")) {
                        view.setBackgroundColor(Utils.setCustomColor(colorLeft));
                    } else if (colorRight.startsWith("#")) {
                        view.setBackgroundColor(Utils.setCustomColor(colorRight));
                    } else {
                        view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_txt_shap2));
                    }
                }
            } else {
                view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_txt_shap2));
            }
        } catch (NumberFormatException e) {
            view.setBackground(Utils.getLocalDrawable(mContext, R.drawable.project_txt_shap2));
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTxt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemTxt = itemView.findViewById(R.id.project_list_item_txt2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition(),boardList.get(getLayoutPosition()));
                    }
                }
            });
        }
    }

    /**
     * 设置是否展开
     *
     * @param an
     */
    public void setAn(boolean an) {
        isAn = an;
    }

    public boolean isAn() {
        return isAn;
    }

    private OnItemClickListener onItemClickListener;

    //点击事件
    public interface OnItemClickListener {
        void onItemClick(View view, int position, ManualPositionBtnBoard data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
