package com.module.home.controller.activity;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.module.MyApplication;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.web.WebUtil;
import com.module.my.model.api.InitCode1Api;
import com.module.my.model.api.JPushBindApi;
import com.module.my.model.api.SendEMSApi;
import com.module.my.model.bean.NewUserData;
import com.module.my.model.bean.UserData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.module.other.netWork.netWork.WebSignData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.tendcloud.appcpa.TalkingDataAppCpa;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;

public class JavaScriptCallBack {

    private String TAG = "JavaScriptCallBack";
    Context mContext;
    WebView mWebView;
    String mUrl;

    public JavaScriptCallBack(Context context, WebView webView, String url) {
        Log.e(TAG, "JavaScriptCallBack.............");
        mContext = context;
        mWebView = webView;
        mUrl = url;//https://user.yuemei.com/home/taozt/id/6025/u/0/
    }

    @JavascriptInterface
    public void sendPhoneCode(String phone) {
        Log.e(TAG, "sendPhoneCode.............");
        Map<String, Object> maps = new HashMap<>();
        if (!TextUtils.isEmpty(phone)) {
            maps.put("phone", phone);
        }
        maps.put("flag", "1");

        new SendEMSApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mWebView.loadUrl("javascript:countDown()");
                } else {
                    ViewInject.toast(serverData.message);
                }
            }
        });
    }

    @JavascriptInterface
    public void goLogin(String data) {
        Log.e("JavaScriptCallBack", "goLogin");
        Log.e(TAG, "goLogin.............");
        try {
            NewUserData newUserData = JSONUtil.TransformSingleBean(data, NewUserData.class);

            Map<String, Object> params = new HashMap<>();
            String phone = newUserData.getPhone();
            String code = newUserData.getCode();
            String referrer = newUserData.getReferrer();
            String referrer_id = newUserData.getReferrer_id();
            if (!TextUtils.isEmpty(phone)) {
                params.put("phone", phone);
            }
            if (!TextUtils.isEmpty(code)) {
                params.put("code", code);
            }
            if (!TextUtils.isEmpty(referrer)){
                params.put("referrer",referrer);
            }
            if (!TextUtils.isEmpty(referrer_id)){
                params.put("referrer_id",referrer_id);
            }

            new InitCode1Api().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
                @Override
                public void onSuccess(ServerData s) {
                    if ("1".equals(s.code)) {
                        UserData userData = JSONUtil.TransformLogin(s.data);
                        String id = userData.get_id();
                        getUserInfoLogin(id);

                    } else {
                        ViewInject.toast(s.message);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void htmlBackReFresh(){

    }





    public void LodUrl(String url) {
        Log.e(TAG, "LodUrl.............");
        WebSignData addressAndHead = SignUtils.getAddressAndHead(url);
        mWebView.loadUrl(addressAndHead.getUrl(), addressAndHead.getHttpHeaders());
//        mWebView.loadUrl(url);
    }



    private void getUserInfoLogin(String uid) {
        Log.e(TAG, "getUserInfoLogin.............");
        BBsDetailUserInfoApi userInfoApi = new BBsDetailUserInfoApi();
        Map<String, Object> params = new HashMap<>();
        params.put("id", uid);
        userInfoApi.getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)){
                    UserData userData = JSONUtil.TransformSingleBean(serverData.data, UserData.class);
                    String id = userData.get_id();
                    TalkingDataAppCpa.onLogin(id);
                    String img = userData.getImg();
                    String nickName = userData.getNickname();
                    String province = userData.getProvince();
                    String city = userData.getCity();
                    String sex = userData.getSex();
                    String birthday = userData.getBirthday();
                    String yuemeiinfo = userData.getYuemeiinfo();
                    String group_id = userData.getGroup_id();
                    String phoneStr = userData.getPhone();
                    String isphone = userData.getIsphone();
                    String loginPhone = userData.getLoginphone();
                    String is_show = userData.getIs_show();
                    String real_name = userData.getReal_name();
                    Cfg.saveStr(mContext, "real_name", real_name);
                    Utils.setUid(id);
                    Cfg.saveStr(mContext, FinalConstant.HOME_PERSON_UID, id);
                    Cfg.saveStr(mContext, FinalConstant.UHEADIMG, img);
                    Cfg.saveStr(mContext, FinalConstant.UNAME, nickName);
                    Cfg.saveStr(mContext, FinalConstant.UPROVINCE, province);
                    Cfg.saveStr(mContext, FinalConstant.UCITY, city);
                    Cfg.saveStr(mContext, FinalConstant.USEX, sex);
                    Cfg.saveStr(mContext, FinalConstant.UBIRTHDAY, birthday);
                    Utils.setYuemeiInfo(yuemeiinfo);
                    Cfg.saveStr(mContext, FinalConstant.GROUP_ID, group_id);

                    Log.d("MainTableActivity", "=======initWebSocket");
                    IMManager.getInstance(mContext).getIMNetInstance().connWebSocket(FinalConstant.baseService);
                    String registrationID = JPushInterface.getRegistrationID(mContext);
                    String mCity = Cfg.loadStr(mContext, "city_dingwei", "");
                    String mProvince = Cfg.loadStr(mContext, FinalConstant.DWPROVINCE, "");
                    HashMap<String, Object> maps = new HashMap<>();
                    String version = android.os.Build.VERSION.RELEASE;// String
                    String mtype = android.os.Build.MODEL; // 手机型号
                    String mtyb = android.os.Build.BRAND;// 手机品牌
                    String brand = mtyb + "_" + mtype;
                    maps.put("reg_id", registrationID);
                    maps.put("location_city", mCity);
                    maps.put("location_Area", mProvince);
                    maps.put("brand", brand);
                    maps.put("system", version);
                    new JPushBindApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData serverData) {
                            Log.e(TAG, "message====" + serverData.message);
                        }
                    });
                    if (phoneStr.equals("0")) {
                        Cfg.saveStr(mContext, FinalConstant.UPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.UPHONE, phoneStr);
                    }

                    if ("".equals(loginPhone) || "0".equals(loginPhone)) {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.ULOGINPHONE, loginPhone);
                    }

                    if ("".equals(isphone) || "0".equals(isphone)) {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, "");
                    } else {
                        Cfg.saveStr(mContext, FinalConstant.USERISPHONE, isphone);
                    }

                    if (Utils.getNewOrOldUser()) {              //如果是新用户
                        Utils.saveNewOrOldUser("1");        //改为老用户
                    }
                    LodUrl(mUrl);
                }else {
                    Toast.makeText(mContext,serverData.message,Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
}
