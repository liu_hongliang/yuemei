package com.module.home.controller.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;
import com.jcodecraeer.xrecyclerview.AppBarStateChangeListener;
import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.refresh.smart.YMRefreshHeader2;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.base.view.ui.GlideImageLoader;
import com.module.commonview.module.bean.DiariesPostShowData;
import com.module.commonview.view.HomeTopView;
import com.module.commonview.view.MainTableButtonView;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.HomeEvent;
import com.module.event.VoteMsgEvent;
import com.module.home.fragment.HomeHomeTopFragment;
import com.module.home.model.bean.BoardBean;
import com.module.home.model.bean.Coupos;
import com.module.home.model.bean.Floating;
import com.module.home.model.bean.HomeButtomFloat;
import com.module.home.model.bean.HomeNewData;
import com.module.home.model.bean.NewZtCouponsBean;
import com.module.home.model.bean.SearchEntry;
import com.module.home.model.bean.Tuijshare;
import com.module.home.view.HomeBackgroundImage;
import com.module.home.view.LoadingProgress;
import com.module.home.view.fragment.HomeFragment;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.CustomDialog;
import com.quicklyask.view.NewuserConponsPop;
import com.quicklyask.view.PushNewDialog;
import com.quicklyask.view.TimerTextView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.OnTwoLevelListener;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.header.TwoLevelHeader;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.youth.banner.Banner;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import org.apache.commons.collections.CollectionUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.utils.SystemTool;
import org.xutils.common.util.DensityUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import simplecache.ACache;

/**
 * 新首页
 */
public class HomeActivity extends FragmentActivity {

    @BindView(R.id.activity_home)
    FrameLayout activityHomeView;                                                 //首页父容器
    //标题栏
    @BindView(R.id.home_top_background)
    public HomeBackgroundImage mTopBackground;                                  //滑动头部的搜索栏背景
    @BindView(R.id.ll_home_toobar)
    Toolbar llHomeToobar;                                                       //滑动头部的搜索栏占位
    @BindView(R.id.home_top_title)
    public HomeTopTitle mTopTitle;                                              //滑动头部的搜索栏
    @BindView(R.id.home_top_fragment)
    FrameLayout mTopFragment;                                                   //滑动头部的模块
    //页面框架
    @BindView(R.id.home_tab_layout)
    TabLayout mTabLayout;                     //tabLayout
    @BindView(R.id.home_app_bar)
    AppBarLayout mAppBar;
    @BindView(R.id.home_view_page)
    ViewPager mViewPage;                     //Fragment布局
    @BindView(R.id.home_coordinator_layout)
    CoordinatorLayout homeCoordinatorLayout;
    @BindView(R.id.home_refresh_layout)
    SmartRefreshLayout homeRefresh;       //刷新组件

    //代金券部分
    @BindView(R.id.new_libao_iv)
    ImageView newUserIv;
    @BindView(R.id.newlibao_colse_iv)
    ImageView mMessageColse;
    @BindView(R.id.home_newlibao_rly)
    RelativeLayout newUserBaoRly;
    @BindView(R.id.folating_iv)
    ImageView floatingIv;
    @BindView(R.id.folating_colse_iv)
    ImageView floatingColseIv;
    @BindView(R.id.home_floating_rly)
    RelativeLayout floatingRly;
    @BindView(R.id.home_coupos_time)
    TimerTextView homeCouposTime;
    @BindView(R.id.home_coupos_rly)
    LinearLayout homeCouposRly;
    @BindView(R.id.rl_home_activity)
    RelativeLayout rlHomeActivity;
    @BindView(R.id.home_bottom_tip)
    TextView mBottomTip;
    @BindView(R.id.recovery_title_container)
    LinearLayout mRecoveryTitleContainer;
    @BindView(R.id.recovery_title)
    TextView mRecoveryTitle;
    @BindView(R.id.banner_tab_top)
    Banner mBannerTabTop;

    @BindView(R.id.header_two)
    TwoLevelHeader mTwoLevelHeader;
    //二楼加载图
    @BindView(R.id.iv_floor_loading)
    ImageView iv_floor_loading;
    //二楼内容布局
    @BindView(R.id.fl_floor_content)
    FrameLayout fl_floor_content;
    //二楼占位图
    @BindView(R.id.iv_floor_placeholder)
    ImageView iv_floor_placeholder;
    //二楼关闭按钮
    @BindView(R.id.iv_floor_close)
    ImageView iv_floor_close;
    @BindView(R.id.header_one)
    YMRefreshHeader2 header;

    @BindView(R.id.home_recycler_refesh)
    LinearLayout mHomeRecyclerRefesh;
    @BindView(R.id.iv_home_recycler_refesh)
    ImageView mHomeRecyclerRefeshIv;

    private String TAG = "HomeActivity";
    private HomeActivity mContext;
    private ACache mCache;                          //缓存组件
    private LoadingProgress mDialog;                //等待组件
    private BaseNetWorkCallBackApi homeApi;         //首页数据获取
    private Gson mGson;
    private boolean isLogin = Utils.isLogin();              //是否登录
    private HomeNewData mHomeData;                          //首页数据
    private int titleHeight;                                //状态栏高度
    private int mFragmentSelectedPos = 0;                   //当前选中的信息流
    private boolean isExpand = true;
    private HomeHomeTopFragment homeHomeTopFragment;        //首页头部滑动

    private final int PUSH_NEW = 111;
    private final int HOME_TIP = 121;
    private final int RECOVERY_SHOW = 122;
    private final int RECOVERY_CANCEL = 123;
    public static String isTwoLevel = "0";
    private CountDownTimer countDownTimer;
    private boolean isShowCoupsTimeView = false;
    private boolean isShowNewUserView = false;
    private boolean isShowFloatView = false;
    private AdertAdv.SecondFloorBean data;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case PUSH_NEW:
                    boolean b = NotificationManagerCompat.from(mContext).areNotificationsEnabled();
                    if (!b) {
                        String pushNewPos = Cfg.loadStr(mContext, "pushNewPos", "");
                        if (TextUtils.isEmpty(pushNewPos)) {
                            PushNewDialog pushNewDialog = new PushNewDialog(mContext);
                            pushNewDialog.show();
                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEW_SHOW_ALERT, "1", "0", "1"));
                            Cfg.saveStr(mContext, "pushNewPos", "1");
                        }
                    }
                    break;
                case HOME_TIP:
                    if (mBottomTip.getVisibility() == View.VISIBLE) {
                        mBottomTip.setVisibility(View.GONE);
                    }
                    break;
                case RECOVERY_SHOW:
                    mRecoveryTitleContainer.setVisibility(View.VISIBLE);
                    ObjectAnimator animator = ObjectAnimator.ofFloat(mRecoveryTitleContainer, "translationY", 200, 0);
                    animator.setDuration(500);
                    animator.start();

                    mHandler.sendEmptyMessageDelayed(RECOVERY_CANCEL, 5000);
                    break;
                case RECOVERY_CANCEL:
                    ObjectAnimator animator2 = ObjectAnimator.ofFloat(mRecoveryTitleContainer, "translationY", 0, 200);
                    animator2.setDuration(500);
                    animator2.start();
                    animator2.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mRecoveryTitleContainer.setVisibility(View.GONE);
                        }

                    });

                    break;
            }
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(VoteMsgEvent msgEvent) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mContext = HomeActivity.this;
        ButterKnife.bind(mContext);
        QMUIStatusBarHelper.setStatusBarLightMode(mContext);

        mCache = ACache.get(mContext);
        mDialog = new LoadingProgress(mContext);

        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        homeApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME_NEW, "homeIndex");
        mGson = new Gson();

        titleHeight = statusbarHeight + Utils.dip2px(50);

        initView();

        //判断是否联网。来获取本地缓存数据，或者是网络数据
        getCachedData();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveMsg(String refresh) {
        loadHomeData();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
        mHandler.sendEmptyMessage(PUSH_NEW);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (mTopTitle.setCity() || (isLogin != Utils.isLogin())) {
            //获取初始化数据
            loadHomeData();
            isLogin = Utils.isLogin();
        }

        //设置购物车数量
        mTopTitle.setCartNum();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (fragmentList.size() != 0) {
                fragmentList.get(mFragmentSelectedPos).onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    /**
     * 初始化UI数据
     */
    private void initView() {
        //设置定位城市
        mTopTitle.setCity();
        //回调初始化
        initSetOnListener();
        //设置搜索栏高度
        ViewGroup.LayoutParams layoutParams = mTopTitle.getLayoutParams();
        ViewGroup.LayoutParams layoutParams1 = llHomeToobar.getLayoutParams();
        layoutParams.height = titleHeight;
        layoutParams1.height = titleHeight;
        mTopTitle.setLayoutParams(layoutParams);
        llHomeToobar.setLayoutParams(layoutParams1);
        //模块上外边距
        ViewGroup.MarginLayoutParams layoutParams2 = (ViewGroup.MarginLayoutParams) mTopFragment.getLayoutParams();
        layoutParams2.topMargin = titleHeight;
        mTopFragment.setLayoutParams(layoutParams2);
//        EventBus.getDefault().register(this);
    }

    /**
     * 获取本地缓存数据
     */
    private void getCachedData() {
        if (SystemTool.checkNet(mContext)) {
            loadHomeData();     //获取初始化数据
        } else {
            String homejson = mCache.getAsString(FinalConstant.HOME_NEW_JSON);
            if (homejson != null) {
                try {
                    mHomeData = JSONUtil.TransformSingleBean(homejson, HomeNewData.class);
                    loadHomeDataToView();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 回调初始化
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initSetOnListener() {
        homeRefresh.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadHomeData();
            }

            @Override
            public void onHeaderMoving(RefreshHeader header, boolean isDragging, float percent, int offset, int headerHeight, int maxDragHeight) {
                if (isTwoLevel.equals("1")) {
                    iv_floor_loading.setTranslationY(Math.min(offset - iv_floor_loading.getHeight(), homeRefresh.getLayout().getHeight() - iv_floor_loading.getHeight()));
                    iv_floor_placeholder.setTranslationY(Math.min(offset - iv_floor_placeholder.getHeight(), homeRefresh.getLayout().getHeight() - iv_floor_placeholder.getHeight()));
                }
            }

            @Override
            public void onStateChanged(@NonNull RefreshLayout refreshLayout, @NonNull RefreshState oldState, @NonNull RefreshState newState) {
                if (isTwoLevel.equals("1")) {
                    if (oldState == RefreshState.TwoLevel) {
                        fl_floor_content.animate().alpha(0).setDuration(100);
                    }
                }
            }
        });

        //监听回调
        mTopTitle.setOnEventClickListener(new HomeTopTitle.OnEventClickListener() {
            @Override
            public void onCityClick(View v, String city) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Intent intent = new Intent(mContext, MainCitySelectActivity560.class);
                intent.putExtra("curcity", city);
                intent.putExtra("type", "2");
                if (MainTableActivity.mContext != null) {
                    MainTableActivity.mContext.startActivityForResult(intent, 1004);
                } else {
                    startActivity(intent);
                }
                Utils.tongjiApp(mContext, "home_city", "home", "home", "1");
            }

            @Override
            public void onTitleClick(View v, String key, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                TCAgent.onEvent(mContext, "首页搜索", "首页搜索");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.INDEX_NEW_SEARCH, (pos + 1) + ""), new ActivityTypeData("1"));

                Intent it = new Intent(mContext, SearchAllActivity668.class);
                if (!TextUtils.isEmpty(key)) {
                    it.putExtra(SearchAllActivity668.KEYS, key);
                    List<SearchEntry> searchEntryList = mHomeData.getSearchEntry();
                    SearchEntry searchEntry = searchEntryList.get(pos);
                    it.putExtra(SearchAllActivity668.TARGET, searchEntry);
                } else {
                    it.putExtra(SearchAllActivity668.KEYS, "");
                }
                it.putExtra(SearchAllActivity668.TYPE, "1");
                startActivity(it);
            }

            @Override
            public void onCartClick(View v) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                startActivity(new Intent(mContext, ShoppingCartActivity.class));

            }
        });

        //导航栏点击置顶回调
        MainTableActivity.mainBottomBar.setOnHomeTopClickListener(new MainTableButtonView.OnHomeTopClickListener() {
            @Override
            public void onHomeTopClick(boolean isTabTop, HomeTopView tabHomeTop) {
                Log.e(TAG, "isTabTop == " + isTabTop);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMELOOKDIARY, isTabTop ? "1" : "2"));

                //修改展开或者折叠
                mAppBar.setExpanded(isTabTop, true);

                if (isTabTop) {
                    for (YMBaseFragment fragment : fragmentList) {
                        HomeFragment homeFragment = (HomeFragment) fragment;
                        RecyclerView recyclerView = homeFragment.getRecyclerView();
                        if (recyclerView != null) {
                            recyclerView.scrollToPosition(0);
                        }
                    }
                }
            }
        });

        //展开或者收缩
        mAppBar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, AppBarStateChangeListener.State state) {
                Log.e(TAG, "state == " + state);
                switch (state) {
                    case IDLE:
                        //TabLayout部分露出/全部露出
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(false);
                        mHomeRecyclerRefesh.setVisibility(View.GONE);
                        mBannerTabTop.setVisibility(View.GONE);
                        mBannerTabTop.stopAutoPlay();
                        break;
                    case COLLAPSED:
                        //TabLayout完全隐藏
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAB_CEILING, "0"));
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(true);
                        mHomeRecyclerRefesh.setVisibility(View.VISIBLE);
                        if (mHomeData.getBigPromotionHomeCeiling() != null && mHomeData.getBigPromotionHomeCeiling().getImg() != null) {
                            mBannerTabTop.setVisibility(View.VISIBLE);
                            mBannerTabTop.startAutoPlay();
                        }
                        break;
                    case EXPANDED:
                        //最大化展开
                        MainTableActivity.mainBottomBar.tabHomeTop.isTopHome(false);
                        mHomeRecyclerRefesh.setVisibility(View.GONE);
                        mBannerTabTop.setVisibility(View.GONE);
                        mBannerTabTop.stopAutoPlay();
                        break;
                }
            }
        });

        //滑动监听
        mAppBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                int abs = Math.abs(i);
                if (homeHomeTopFragment != null && homeHomeTopFragment.mTopBanner != null) {
                    ViewGroup.MarginLayoutParams mBannerLp = (ViewGroup.MarginLayoutParams) homeHomeTopFragment.mTopBanner.getLayoutParams();
                    int height = mTopTitle.getMeasuredHeight() + mBannerLp.height + mBannerLp.topMargin;

                    //热词搜索高度
                    LinearLayout mTopHotWord = homeHomeTopFragment.mTopHotWord;
                    if (mTopHotWord.getVisibility() == View.VISIBLE) {
                        ViewGroup.MarginLayoutParams mHotWordLp = (ViewGroup.MarginLayoutParams) mTopHotWord.getLayoutParams();
                        height += mHotWordLp.height + mHotWordLp.topMargin;
                    }
                    if (height > 0) {
                        if (abs <= height) {
                            mTopTitle.setTopAlpha((float) abs / height);
                        } else {
                            mTopTitle.setTopAlpha(1.0f);
                        }
                    }
                } else {
                    mTopTitle.setTopAlpha(0);
                }
                if (i != 0 && mBottomTip.getVisibility() == View.VISIBLE) {
                    mHandler.removeMessages(HOME_TIP);
                    mBottomTip.setVisibility(View.GONE);
                }
            }
        });

        //代金券点击
        newUserIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", "6025");
                hashMap.put("to_page_type", "16");
                hashMap.put("to_page_id", "6025");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "yes"), hashMap);
                WebUrlTypeUtil.getInstance(mContext).urlToApp("https://m.yuemei.com/tao_zt/6025.html", "0", "0");
            }
        });

        //代金券关闭点击
        mMessageColse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", "6025");
                hashMap.put("to_page_type", "16");
                hashMap.put("to_page_id", "6025");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "no"), hashMap);
                Cfg.saveStr(mContext, FinalConstant.NEWUSER_CLOSE, "close");
                newUserControl(false);
            }
        });

        //刷新
        mHomeRecyclerRefesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post("refresh");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_INDEX_REFRESH), new ActivityTypeData("1"));
            }
        });
    }

    /**
     * 是不是二楼 用哪个头
     * isTwoLevel  0没有二楼 1有二楼
     */
    private void setHeader() {
        if (!TextUtils.isEmpty(Cfg.loadStr(mContext, "home_secondFloor_data", "")) && !(Cfg.loadStr(mContext, "home_secondFloor_data", "")).equals("null")) {
            data = JSONUtil.TransformSingleBean(Cfg.loadStr(mContext, "home_secondFloor_data", ""), AdertAdv.SecondFloorBean.class);
        }
        if (data != null && data.getLoadingImg() != null) {
            isTwoLevel = "1";
            header.setState();
            header.setVisibility(View.VISIBLE);
            iv_floor_loading.setVisibility(View.VISIBLE);
            fl_floor_content.setVisibility(View.VISIBLE);
            iv_floor_placeholder.setVisibility(View.VISIBLE);
            iv_floor_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                    closeFloor();
                }
            });
            Glide.with(mContext).load(data.getLoadingImg().getImg()).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    if (resource.isAnimated()) {
                        resource.setLoopCount(GifDrawable.LOOP_FOREVER);
                        resource.start();
                    }
                    iv_floor_loading.setImageDrawable(resource);
                }
            });
            Glide.with(mContext).load(data.getPlaceholderImg().getImg()).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    if (resource.isAnimated()) {
                        resource.setLoopCount(GifDrawable.LOOP_FOREVER);
                        resource.start();
                    }
                    iv_floor_placeholder.setImageDrawable(resource);
                }
            });
            //刷新界限
//            mTwoLevelHeader.setRefreshRage((float)(DensityUtil.getScreenHeight()*0.15/DensityUtil.dip2px(80)));
            homeRefresh.setHeaderTriggerRate(0);
            //二楼界限
            mTwoLevelHeader.setFloorRage((float) (getRealHeight() * 0.3 / DensityUtil.dip2px(80)));
            //最大滑动界限
            mTwoLevelHeader.setMaxRage((float) (getRealHeight() * 0.4 / DensityUtil.dip2px(80)));
            mTwoLevelHeader.setEnableTwoLevel(true);
            mTwoLevelHeader.setEnablePullToCloseTwoLevel(false);
            mTwoLevelHeader.setOnTwoLevelListener(new OnTwoLevelListener() {
                @Override
                public boolean onTwoLevel(@NonNull final RefreshLayout refreshLayout) {
                    if (homeCouposRly.getVisibility() == View.VISIBLE) {
                        homeCouposRly.setVisibility(View.GONE);
                        isShowCoupsTimeView = true;
                    }
                    if (newUserBaoRly.getVisibility() == View.VISIBLE) {
                        newUserBaoRly.setVisibility(View.GONE);
                        isShowNewUserView = true;
                    }
                    if (floatingRly.getVisibility() == View.VISIBLE) {
                        floatingRly.setVisibility(View.GONE);
                        isShowFloatView = true;
                    }


                    YmStatistics.getInstance().tongjiApp(data.getLoadData().getEvent_params());
                    EventBus.getDefault().post(new HomeEvent(1));
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                    countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            String link = data.getLoadData().getUrl();
                            if (!TextUtils.isEmpty(link)) {
                                if (link.length() >= 4 && (link.substring(link.length() - 4, link.length())).equalsIgnoreCase(".mp4")) {
//                                    跳转视频页
                                    HomeFloorVideoActivity.start(mContext, link, data.getLoadData().getLocation_url(), data.getLoadData().getImg(), data.getLoadData().getH_w());
//                                HomeFloorVideoActivity.start(mContext, "https://player.yuemei.com/video/6ea2/201901/c77f1ffd0eb9a8b0d85cdcc21c4a2ab9.mp4", data.getLoadData().getUrl(), "", data.getLoadData().getH_w());
                                } else {
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(link);
                                }
                                refreshLayout.getLayout().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        closeFloor();
                                    }
                                }, 200);
                            }
                        }
                    };
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fl_floor_content.animate().alpha(1).setDuration(100);
                            countDownTimer.start();
                        }
                    }, 1000);

                    return true;//true 将会展开二楼状态 false 关闭刷新
                }
            });
        } else {
            isTwoLevel = "0";
            header.setState();
            header.setVisibility(View.VISIBLE);
            iv_floor_loading.setVisibility(View.GONE);
            fl_floor_content.setVisibility(View.GONE);
            iv_floor_close.setVisibility(View.GONE);
            mTwoLevelHeader.setEnableTwoLevel(false);
        }
    }

    private void closeFloor() {
        fl_floor_content.animate().alpha(0).setDuration(100);
        mTwoLevelHeader.finishTwoLevel();
        EventBus.getDefault().post(new HomeEvent(2));
        if (isShowCoupsTimeView) {
            homeCouposRly.setVisibility(View.VISIBLE);
            isShowCoupsTimeView = false;
        }
        if (isShowNewUserView) {
            newUserBaoRly.setVisibility(View.VISIBLE);
            isShowNewUserView = false;
        }
        if (isShowFloatView) {
            floatingRly.setVisibility(View.VISIBLE);
            isShowFloatView = false;
        }
    }


    /**
     * 首页数据加载
     */
    private void loadHomeData() {
        mDialog.startLoading();
        homeApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    homeRefresh.finishRefresh();
                    mDialog.stopLoading();
                    mHomeData = JSONUtil.TransformSingleBean(data.data, HomeNewData.class);
                    loadHomeDataToView();
                    mCache.put(FinalConstant.HOME_NEW_JSON, data.data, 60 * 60 * 24);
                }
            }
        });
    }

    /**
     * 首页数据解析
     */
    private void loadHomeDataToView() {
        //设置头部标题
        mTopTitle.setTitleText(mHomeData.getSearchEntry());

        //首页滑动
        homeHomeTopFragment = HomeHomeTopFragment.newInstance(mHomeData);
        setActivityFragment(R.id.home_top_fragment, homeHomeTopFragment);

        //首页调用日记
        diaryList();

        //首页悬浮
        setHomeFloat(mHomeData.getButtomFloat());

        //新人优惠弹层
        showHomeCouponsPop();

        //有没有二楼标志
        setHeader();

        //tab上banner
        initTabLayoutTopBanner();
    }

    private void initTabLayoutTopBanner() {
        if (mHomeData.getBigPromotionHomeCeiling() != null && mHomeData.getBigPromotionHomeCeiling().getImg() != null) {
            ArrayList<String> images = new ArrayList<>();
            ArrayList<String> titles = new ArrayList<>();
//            for (int i = 0; i < ballBeans.size(); i++) {
            images.add(mHomeData.getBigPromotionHomeCeiling().getImg());
            titles.add(mHomeData.getBigPromotionHomeCeiling().getUrl());
//            }
            mBannerTabTop.setBannerStyle(0);
            mBannerTabTop.setImages(images);
            mBannerTabTop.setBannerTitles(titles);
            mBannerTabTop.setBannerAnimation(Transformer.Default);
            mBannerTabTop.isAutoPlay(true);
            mBannerTabTop.setDelayTime(4000);
            mBannerTabTop.setImageLoader((new GlideImageLoader(mContext, Utils.dip2px(0))));
            mBannerTabTop.start();

            mBannerTabTop.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(mHomeData.getBigPromotionHomeCeiling().getUrl());
                }
            });
        }
    }


    /**
     * 横滑日记列表
     */
    private ArrayList<YMBaseFragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();

    private void diaryList() {
        HomeFragment homeFragment = null;
        ArrayList<Tuijshare> tuijshare = mHomeData.getTuijshare();            //首页推荐5种类型实体类
        List<BoardBean> boardlist = mHomeData.getBoard();                //日记横向滑动title实体类
        titleList.clear();
        fragmentList.clear();

        for (int i = 0; i < boardlist.size(); i++) {
            BoardBean data = boardlist.get(i);
            Log.e(TAG, "data ==" + data);
            if (data != null) {
                titleList.add(data.getTitle());
                String parIdStr = data.getId();
                if (i == 0) {
                    homeFragment = HomeFragment.newInstance(parIdStr, tuijshare, "1");
                    setHomeFragmentListener(homeFragment);
                    fragmentList.add(homeFragment);
                } else {
                    HomeFragment homeFragment1 = HomeFragment.newInstance(parIdStr);
                    setHomeFragmentListener(homeFragment1);
                    fragmentList.add(homeFragment1);
                }
            }
        }

        YMTabLayoutAdapter ymTabLayoutAdapter = new YMTabLayoutAdapter(getSupportFragmentManager(), titleList, fragmentList);
        mViewPage.setAdapter(ymTabLayoutAdapter);
        mTabLayout.setupWithViewPager(mViewPage);
        YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(13), Utils.dip2px(13));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mFragmentSelectedPos = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    /**
     * 首页悬浮
     */
    private String couponsUrl;

    private void setHomeFloat(HomeButtomFloat buttomFloat) {
        //弹层
        if (buttomFloat != null && !TextUtils.isEmpty(buttomFloat.getTitle())) {
            Log.e(TAG, "buttomFloat.getTitle() == " + buttomFloat.getTitle());
            setBottomTipShow(buttomFloat);

        } else {
            mBottomTip.setVisibility(View.GONE);
            showRecoverTip(1000);
        }

        //大促指定浮窗->新人红包浮窗->优惠券倒计时浮层
        Floating mFloating = mHomeData.getFloating();              //首页悬浮
        String img = mFloating.getImg();
        final String url = mFloating.getUrl();
        final HashMap<String, String> event_params = mFloating.getEvent_params();
        //大促浮层
        if (!TextUtils.isEmpty(img) && !TextUtils.isEmpty(url)) {
            String isClose = Cfg.loadStr(mContext, FinalConstant.DACU_FLOAT_CLOAS, "");
            if (TextUtils.isEmpty(isClose)) {
                dacuFloatingControl(true, img, event_params);
                newUserControl(false);
                couponsFloatControl(false, null);
            } else {
                dacuFloatingControl(false, img, event_params);
            }

        } else {
            //新人红包
            dacuFloatingControl(false, "", new HashMap<String, String>());
            String is_show = Cfg.loadStr(mContext, FinalConstant.ISSHOW, "1");
            if ("1".equals(is_show)) {
                String isClose = Cfg.loadStr(mContext, FinalConstant.NEWUSER_CLOSE, "");
                if (TextUtils.isEmpty(isClose)) {
                    if (!Utils.isLogin()) {
                        newUserControl(true);
                    }
                    couponsFloatControl(false, null);
                } else {
                    newUserControl(false);
                }

            } else {
                //优惠券倒计时浮层
                newUserControl(false);
                Coupos coupons = mHomeData.getCoupons();
                String coupons_id = coupons.getCoupons_id();
                String end_time = coupons.getEnd_time();
                couponsUrl = coupons.getUrl();
                String endTimeAct = Utils.stampToPhpDate(end_time);
                Date date = new Date();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentTime = simpleDateFormat.format(date);
                long[] timeSubCurr = Utils.getTimeSub(currentTime, endTimeAct);
                long[] timesAct = {timeSubCurr[0], timeSubCurr[1], timeSubCurr[2], timeSubCurr[3]};

                int couponsId = Integer.parseInt(coupons_id);
                if (couponsId > 0) {
                    if (timeSubCurr[0] == 0) {
                        couponsFloatControl(true, timesAct);
                    }
                } else {
                    couponsFloatControl(false, null);
                }
            }

        }
        floatingRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "1", "0");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "yes"), event_params);
            }
        });
        floatingColseIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingRly.setVisibility(View.GONE);
                Cfg.saveStr(mContext, FinalConstant.DACU_FLOAT_CLOAS, "close");
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "no"), event_params);
            }
        });
        homeCouposRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(couponsUrl)) {
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMECOUPONS, "click"), new ActivityTypeData("1"));
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(couponsUrl, "1", "0");
                }
            }
        });

    }

    /**
     * 新人优惠弹层
     */
    private void showHomeCouponsPop() {
        List<NewZtCouponsBean> newZtCoupons = mHomeData.getNewZtCoupons();
        if (CollectionUtils.isNotEmpty(newZtCoupons)) {
            String newZtcoupons = Cfg.loadStr(mContext, "new_ztcoupons", "");
            if (TextUtils.isEmpty(newZtcoupons)) {
                Cfg.saveStr(mContext, "new_ztcoupons", "1");
            } else {
                String ad_imgurl = Cfg.loadStr(mContext, "ad_imgurl", "");
                if (TextUtils.isEmpty(ad_imgurl)) {
                    NewuserConponsPop newuserConponsPop = new NewuserConponsPop(mContext, newZtCoupons);
                    newuserConponsPop.showAtLocation(activityHomeView, Gravity.CENTER, 0, 0);
                }
            }

        }
    }


    /**
     * 日历弹窗
     *
     * @param time
     */
    private void showRecoverTip(int time) {

        String homeShowTitle = Cfg.loadStr(mContext, "show_title", "");
        String showTitle = Cfg.loadStr(mContext, "home_show_title", "");
        String adImgurl = Cfg.loadStr(mContext, "ad_imgurl", "");
        if (!TextUtils.isEmpty(adImgurl)) {
            if (!TextUtils.isEmpty(homeShowTitle)) {
                if ("show".equals(homeShowTitle)) {
                    mRecoveryTitle.setText(showTitle);
                    mHandler.sendEmptyMessageDelayed(RECOVERY_SHOW, time + 2000);
                }
            }
        } else {
            if (!TextUtils.isEmpty(homeShowTitle)) {
                if ("show".equals(homeShowTitle)) {
                    mRecoveryTitle.setText(showTitle);
                    mHandler.sendEmptyMessageDelayed(RECOVERY_SHOW, time);
                }
            }
        }


    }

    /**
     * 新人红包显示或隐藏
     *
     * @param isShow true 显示 flase 隐藏
     */
    private void newUserControl(boolean isShow) {
        if (isShow) {
            newUserBaoRly.setVisibility(View.VISIBLE);

            Glide.with(mContext)
                    .load(R.drawable.newuser_rightredback)
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(newUserIv);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("id", "6025");
            hashMap.put("to_page_type", "16");
            hashMap.put("to_page_id", "6025");
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "show"), hashMap);
        } else {
            newUserBaoRly.setVisibility(View.GONE);
        }

    }

    /**
     * 大促浮层显示或隐藏
     *
     * @param isShow true 显示 flase 隐藏
     */
    private void dacuFloatingControl(boolean isShow, String url, HashMap<String, String> event_params) {
        if (isShow) {
            floatingRly.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(url).into(floatingIv);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLE, "show"), event_params);
        } else {
            floatingRly.setVisibility(View.GONE);
        }
    }

    /**
     * 优惠券倒计时浮层
     *
     * @param isShow   true 显示
     * @param timesAct 时间
     */

    private void couponsFloatControl(boolean isShow, long[] timesAct) {
        if (isShow) {
            homeCouposRly.setVisibility(View.VISIBLE);
            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMECOUPONS, "show"), new ActivityTypeData("1"));
            homeCouposTime.setTimes(timesAct);
            homeCouposTime.setCopywriting("");
            if (!homeCouposTime.isRun()) {
                homeCouposTime.beginRun();
            }
            homeCouposTime.setTimeCallBack(new TimerTextView.TimeCallBack() {
                @Override
                public void timeCallBack() {
                    couponsFloatControl(false, null);
                }
            });
        } else {
            if (homeCouposTime.isRun()) {
                homeCouposTime.stopRun();
            }
            homeCouposRly.setVisibility(View.GONE);
        }
    }

    /**
     * 设置底部提示显示隐藏
     *
     * @param buttomFloat
     */
    private void setBottomTipShow(HomeButtomFloat buttomFloat) {
        String today = Utils.getDay();
        String srt = Cfg.loadStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, "");
        if (!TextUtils.isEmpty(srt)) {
            DiariesPostShowData diariesPostShowData = mGson.fromJson(srt, DiariesPostShowData.class);
            String data = diariesPostShowData.getData();
            int number = diariesPostShowData.getNumber();
            if (!data.equals(today) || number < 3) {
                setBottomTipData(buttomFloat);
                String toJson;
                if (!data.equals(today)) {
                    toJson = mGson.toJson(new DiariesPostShowData(today, 1));
                } else {
                    number++;
                    toJson = mGson.toJson(new DiariesPostShowData(today, number));
                }
                Cfg.saveStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, toJson);
                //日历弹窗优先级
                String showTime = buttomFloat.getShowTime();
                if (!TextUtils.isEmpty(showTime)) {
                    int time = Integer.parseInt(showTime);
                    showRecoverTip((time * 1000) + 1000);
                }
            } else {
                mBottomTip.setVisibility(View.GONE);
                showRecoverTip(1000);
            }
        } else {
            setBottomTipData(buttomFloat);
            String toJson = mGson.toJson(new DiariesPostShowData(today, 1));
            Cfg.saveStr(mContext, FinalConstant.HOME_BOTOOM_TIP_SHOW, toJson);
            showRecoverTip(1000);
        }
    }

    /**
     * 设置底部提示数据
     *
     * @param buttomFloat
     */
    private void setBottomTipData(final HomeButtomFloat buttomFloat) {
        mBottomTip.setVisibility(View.VISIBLE);
        mBottomTip.setText(buttomFloat.getTitle());
        mBottomTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = buttomFloat.getUrl();
                if (!TextUtils.isEmpty(url)) {
                    WebUrlTypeUtil.getInstance(mContext).urlToApp(buttomFloat.getUrl());
                }
            }
        });
        String showTime = buttomFloat.getShowTime();
        if (!TextUtils.isEmpty(showTime)) {
            mHandler.sendEmptyMessageDelayed(HOME_TIP, Integer.parseInt(showTime) * 1000);
        }

    }

    private void setHomeFragmentListener(HomeFragment homeFragment) {
        if (null != homeFragment) {
            homeFragment.setmOnRecyclerViewStateCallBack(new HomeFragment.OnRecyclerViewStateCallBack() {
                @Override
                public void recyclerViewStateCallBack(RecyclerView recyclerView, int newState) {
                    Log.e(TAG, "newState == " + newState);
                    if (homeCouposRly.getVisibility() == View.VISIBLE) {
                        switch (newState) {
                            case RecyclerView.SCROLL_STATE_IDLE:
                                isExpand = true;
                                //滑入动画
                                ObjectAnimator translationXAnimatorIn = ObjectAnimator.ofFloat(homeCouposRly, "translationX", Utils.dip2px(40), 0);
                                ObjectAnimator alphaAnimatorIn = ObjectAnimator.ofFloat(homeCouposRly, "alpha", 0.5f, 1f);
                                AnimatorSet animatorSetIn = new AnimatorSet();
                                animatorSetIn.playTogether(translationXAnimatorIn, alphaAnimatorIn);
                                animatorSetIn.setDuration(500);
                                animatorSetIn.start();
                                break;
                            case RecyclerView.SCROLL_STATE_DRAGGING:
                            case RecyclerView.SCROLL_STATE_SETTLING:
                                if (isExpand) {
                                    isExpand = false;
                                    ObjectAnimator translationXAnimatorOut = ObjectAnimator.ofFloat(homeCouposRly, "translationX", 0f, Utils.dip2px(40));
                                    ObjectAnimator alphaAnimatorOut = ObjectAnimator.ofFloat(homeCouposRly, "alpha", 1f, 0.5f);
                                    AnimatorSet animatorSetOut = new AnimatorSet();
                                    animatorSetOut.playTogether(translationXAnimatorOut, alphaAnimatorOut);
                                    animatorSetOut.setDuration(500);
                                    animatorSetOut.start();
                                }
                                break;
                        }
                    }
                    switch (newState) {
                        case RecyclerView.SCROLL_STATE_IDLE:
                            mHomeRecyclerRefeshIv.setImageResource(R.drawable.home_refesh_img2);
                            //滑入动画
                            ObjectAnimator translationYAnimatorIn = ObjectAnimator.ofFloat(mHomeRecyclerRefesh, "translationY", Utils.dip2px(28), 0);
                            ObjectAnimator alphaAnimatorIn = ObjectAnimator.ofFloat(mHomeRecyclerRefesh, "alpha", 0.5f, 1f);
                            AnimatorSet animatorSetIn = new AnimatorSet();
                            animatorSetIn.playTogether(translationYAnimatorIn, alphaAnimatorIn);
                            animatorSetIn.setDuration(200);
                            animatorSetIn.start();
                            break;
                        case RecyclerView.SCROLL_STATE_DRAGGING:
                        case RecyclerView.SCROLL_STATE_SETTLING:
                            mHomeRecyclerRefeshIv.setImageResource(R.drawable.home_refesh_img1);
                            ObjectAnimator translationYAnimatorOut = ObjectAnimator.ofFloat(mHomeRecyclerRefesh, "translationY", 0f, Utils.dip2px(28));
                            ObjectAnimator alphaAnimatorOut = ObjectAnimator.ofFloat(mHomeRecyclerRefesh, "alpha", 1f, 0.5f);
                            AnimatorSet animatorSetOut = new AnimatorSet();
                            animatorSetOut.playTogether(translationYAnimatorOut, alphaAnimatorOut);
                            animatorSetOut.setDuration(200);
                            animatorSetOut.start();
                            break;
                    }
                }
            });
        }
    }

    /**
     * 设置布局的fragment
     */
    private void setActivityFragment(int id, Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(id, fragment);
        transaction.commitAllowingStateLoss();
    }

    /**
     * 返回键回调方法
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            final CustomDialog dialog = new CustomDialog(getParent(), R.style.mystyle, R.layout.customdialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);
    }

    //整块屏幕高度
    @SuppressLint("NewApi")
    public int getRealHeight() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(dm);
        int dwidth = dm.widthPixels;
        int dheight = dm.heightPixels;
        return dheight;
    }
}
