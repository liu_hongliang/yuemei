package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.community.model.bean.TaoListData;
import com.module.doctor.model.bean.HosListData;
import com.module.doctor.view.StaScoreBar;
import com.module.home.model.bean.SearchResultTaoData;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.taodetail.model.bean.SkuLabelLevel;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/28
 */
public class ProjectHosMapModeAdapter extends RecyclerView.Adapter<ProjectHosMapModeAdapter.ViewHolder> {

    private Context mContext;
    private List<HosListData> mData;

    public ProjectHosMapModeAdapter(Context context, List<HosListData> hosLists) {
        this.mContext = context;
        this.mData = hosLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.hos_list_item_view1_map_mode, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        HosListData doctorData = mData.get(position);

        // 填充布局
        viewHolder.docNameTV.setText(doctorData.getHos_name());

        Glide.with(mContext).load(doctorData.getImg()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.ic_headpic_img).error(R.drawable.ic_headpic_img).into(viewHolder.docHeadPic);

        String kind = doctorData.getKind();

        if ("1".equals(kind)) {
            viewHolder.docYueIv.setVisibility(View.VISIBLE);
            viewHolder.docPeiIv.setVisibility(View.GONE);
        } else {
            viewHolder.docYueIv.setVisibility(View.GONE);
            viewHolder.docPeiIv.setVisibility(View.VISIBLE);
        }

        if (null != doctorData.getDistance()) {
            if (doctorData.getDistance().length() > 0) {
                viewHolder.juliTv.setText(doctorData.getDistance());
            }
        }

        if (doctorData.getComment_bili() != null && doctorData.getComment_bili().length() > 0) {
            String startNum = doctorData.getComment_bili();

            if (startNum.length() > 0) {
                int num = Integer.parseInt(startNum);
//                viewHolder.ratBar.setMax(100);
                viewHolder.ratBar.setProgressBar(num);
                if (num > 0) {
                    viewHolder.scotreTv.setText(doctorData.getComment_score());
                } else {
                    viewHolder.scotreTv.setText("暂无评价");
                }
            }
        }

        //预订人数
        if (null != doctorData.getComment_people() && doctorData.getComment_people().length() > 0 && !doctorData.getComment_people().equals("0")) {
            viewHolder.docCommnetNumTV.setVisibility(View.VISIBLE);
            viewHolder.docCommnetNumTV.setText(doctorData.getComment_people() + "人预定");
        } else {
            viewHolder.docCommnetNumTV.setVisibility(View.GONE);
        }

        viewHolder.docDiZhiTv.setText(doctorData.getAddress());

        //日记数
        if (!TextUtils.isEmpty(doctorData.getDiaryTotal())) {
            viewHolder.myudingDiary.setText(doctorData.getDiaryTotal() + "日记");
            viewHolder.myudingDiary.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingDiary.setVisibility(View.GONE);
        }

        //中间横线
        if (viewHolder.docCommnetNumTV.getVisibility() == View.VISIBLE && viewHolder.myudingDiary.getVisibility() == View.VISIBLE) {
            viewHolder.myudingLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingLine.setVisibility(View.GONE);
        }

        //top
        SkuLabelLevel hospital_top = doctorData.getHospital_top();
        if (!TextUtils.isEmpty(hospital_top.getDesc())) {
            viewHolder.hocTopTag.setText(hospital_top.getDesc());
            viewHolder.hocTopTagContainer.setVisibility(View.VISIBLE);
        } else {
            viewHolder.hocTopTagContainer.setVisibility(View.GONE);
        }

        //推荐项目
        if (!TextUtils.isEmpty(doctorData.getTagname())) {
            viewHolder.myudingProjectContainer.setVisibility(View.VISIBLE);
            viewHolder.myudingProject.setText(doctorData.getTagname());
        } else {
            viewHolder.myudingProjectContainer.setVisibility(View.GONE);
        }

        //推荐项目预定人数
        String totalAppoint = doctorData.getTotalAppoint();
        if (!TextUtils.isEmpty(totalAppoint) && !"0".equals(totalAppoint)) {
            viewHolder.myudingReservation.setVisibility(View.VISIBLE);
            viewHolder.myudingReservation.setText(totalAppoint + "次预定");
        } else {
            viewHolder.myudingReservation.setVisibility(View.GONE);
        }

        //推荐项目日记数
        String diaryTotal = doctorData.getTagDiaryTotal();
        if (!TextUtils.isEmpty(diaryTotal) && !"0".equals(diaryTotal)) {
            viewHolder.myudingProjectDiary.setVisibility(View.VISIBLE);
            viewHolder.myudingProjectDiary.setText(diaryTotal + "篇日记");
        } else {
            viewHolder.myudingProjectDiary.setVisibility(View.GONE);
        }

        //中间线显示隐藏
        if (viewHolder.myudingReservation.getVisibility() == View.VISIBLE && viewHolder.myudingProjectDiary.getVisibility() == View.VISIBLE) {
            viewHolder.myudingProjectLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingProjectLine.setVisibility(View.GONE);
        }

        int taosize = doctorData.getTao().size();
        if (taosize > 0) {

            if (taosize == 1) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.GONE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());

                String member_price = doctorData.getTao().get(0).getMember_price();
                int i = Integer.parseInt(member_price);
                if (i >= 0) {
                    viewHolder.docTao1PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setVisibility(View.GONE);
                    viewHolder.docTao1PlusPrice.setText("¥" + member_price);
                } else {
                    viewHolder.docTao1PlusVibility.setVisibility(View.GONE);
                    viewHolder.docTao1PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setText("¥" + doctorData.getTao().get(0).getPrice_discount());
                }
                String iscu = doctorData.getTao().get(0).getDacu66_id();
                if (iscu.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

            } else if (taosize == 2) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao2NameTv.setText(doctorData.getTao().get(1).getTitle());

                String member_price = doctorData.getTao().get(0).getMember_price();
                String member_price2 = doctorData.getTao().get(1).getMember_price();

                int i = Integer.parseInt(member_price);
                int j = Integer.parseInt(member_price2);
                if (i >= 0) {
                    viewHolder.docTao1PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setVisibility(View.GONE);
                    viewHolder.docTao1PlusPrice.setText("¥" + member_price);
                } else {
                    viewHolder.docTao1PlusVibility.setVisibility(View.GONE);
                    viewHolder.docTao1PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setText("¥" + doctorData.getTao().get(0).getPrice_discount());
                }
                if (j >= 0) {
                    viewHolder.docTao2PlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao2PriceTv.setVisibility(View.GONE);
                    viewHolder.docTao2PlusPrice.setText("¥" + member_price2);
                } else {
                    viewHolder.docTao2PlusVibility.setVisibility(View.GONE);
                    viewHolder.docTao2PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao2PriceTv.setText("¥" + doctorData.getTao().get(1).getPrice_discount());
                }
                String iscu1 = doctorData.getTao().get(0).getDacu66_id();
                if (iscu1.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

                String iscu2 = doctorData.getTao().get(1).getDacu66_id();
                if ("1".equals(iscu2)) {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }
            }
        } else {
            viewHolder.docTao1Content.setVisibility(View.GONE);
            viewHolder.docTao2Content.setVisibility(View.GONE);
        }

//        服务设置
        if (!TextUtils.isEmpty(doctorData.getLook())) {
            viewHolder.docPlusService.setText(doctorData.getLook());
            viewHolder.docPlusService.setVisibility(View.VISIBLE);
        } else {
            viewHolder.docPlusService.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView docHeadPic;
        ImageView docYueIv;
        ImageView docPeiIv;
        TextView docNameTV;

        StaScoreBar ratBar;
        TextView scotreTv;
        TextView docCommnetNumTV;
        TextView docDiZhiTv;
        TextView juliTv;

        TextView docTao1NameTv;
        TextView docTao2NameTv;
        TextView docTao1PriceTv;
        TextView docTao2PriceTv;

        LinearLayout docTao1Content;
        LinearLayout docTao2Content;
        LinearLayout docTao1PlusVibility;
        LinearLayout docTao2PlusVibility;
        TextView docTao1PlusPrice;
        TextView docTao2PlusPrice;
        ImageView docTao1Iv;
        ImageView docTao2Iv;

        View myudingLine;
        TextView myudingDiary;
        LinearLayout myudingProjectContainer;
        TextView myudingProject;
        TextView myudingReservation;
        TextView myudingProjectDiary;
        View myudingProjectLine;
        TextView docPlusService;
        LinearLayout hocTopTagContainer;
        TextView hocTopTag;

        int flag;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            flag = getLayoutPosition();

            docHeadPic = itemView.findViewById(R.id.doc_list_head_image_iv);
            docYueIv = itemView.findViewById(R.id.doc_list_item_cooperation_iv);
            docPeiIv = itemView.findViewById(R.id.doc_list_item_pei_iv);
            docNameTV = itemView.findViewById(R.id.doc_list_item_name_tv);
            ratBar = itemView.findViewById(R.id.room_ratingbar);
            scotreTv = itemView.findViewById(R.id.comment_score_list_tv);
            docCommnetNumTV = itemView.findViewById(R.id.comment_num_hos_list_tv);

            juliTv = itemView.findViewById(R.id.doc_list_item_juli_tv);

            docDiZhiTv = itemView.findViewById(R.id.doc_list_item_shanchang_tv);

            docTao1NameTv = itemView.findViewById(R.id.doc_list_tao1_name_tv);
            docTao2NameTv = itemView.findViewById(R.id.doc_list_tao2_name_tv);
            docTao1PriceTv = itemView.findViewById(R.id.doc_list_tao1_jg_tv);
            docTao2PriceTv = itemView.findViewById(R.id.doc_list_tao2_jg_tv);

            docTao1Content = itemView.findViewById(R.id.doc_list_tao1_ly);
            docTao2Content = itemView.findViewById(R.id.doc_list_tao2_ly);


            docTao1PlusVibility = itemView.findViewById(R.id.hos_list_plus_vibiliyt);
            docTao2PlusVibility = itemView.findViewById(R.id.hos_list_plus_vibiliyt2);
            docTao1PlusPrice = itemView.findViewById(R.id.hos_plus_price);
            docTao2PlusPrice = itemView.findViewById(R.id.hos_plus_price2);

            docTao1Iv = itemView.findViewById(R.id.hos_list_tao_iv1);
            docTao2Iv = itemView.findViewById(R.id.hos_list_tao_iv2);

            myudingLine = itemView.findViewById(R.id.comment_num_hos_line);
            myudingDiary = itemView.findViewById(R.id.comment_num_hos_diary);
            myudingProjectContainer = itemView.findViewById(R.id.hos_list_item_project_container);
            myudingProject = itemView.findViewById(R.id.hos_list_item_project);
            myudingReservation = itemView.findViewById(R.id.hos_list_item_project_reservation);
            myudingProjectLine = itemView.findViewById(R.id.hos_list_item_project_line);
            myudingProjectDiary = itemView.findViewById(R.id.hos_list_item_project_diary);
            docPlusService = itemView.findViewById(R.id.hos_list_more_tao_tv);
            hocTopTagContainer = itemView.findViewById(R.id.hos_list_top_tag_container);
            hocTopTag = itemView.findViewById(R.id.hos_list_top_tag);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, mData.get(getLayoutPosition()), getLayoutPosition());
                    }
                }
            });

        }
    }

    /**
     * 刷新数据
     *
     * @param
     */
    public void refreshData(List<HosListData> infos) {
        mData = infos;
        notifyDataSetChanged();
    }

    public void addData(List<HosListData> infos) {
        int size = mData.size();
        mData.addAll(infos);
        notifyItemRangeInserted(size, mData.size() - size);
    }

    public List<HosListData> getData() {
        return mData;
    }

    public interface OnEventClickListener {
        void onItemClick(View v, HosListData data, int pos);   //点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
