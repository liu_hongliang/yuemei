package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.home.model.bean.ProjectHotztData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.xutils.image.ImageOptions;

import java.util.ArrayList;
import java.util.List;

public class ProjectHotztAdapter extends BaseAdapter {

	private final String TAG = "ProjectHotztAdapter";

	private List<ProjectHotztData> mHotIssues = new ArrayList<ProjectHotztData>();
	private Context mContext;
	private LayoutInflater inflater;
	private ProjectHotztData hotIsData;
	ViewHolder viewHolder;

//	private BitmapUtils bitmapUtils;
//	private BitmapDisplayConfig bitmapConfig;
	private Animation ain;
	private Drawable pic;

	ImageOptions imageOptions;

	private int windowsWight;

	public ProjectHotztAdapter(Context mContext,
			List<ProjectHotztData> mHotIssues) {
		this.mContext = mContext;
		this.mHotIssues = mHotIssues;
		inflater = LayoutInflater.from(mContext);

		windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);

		pic = mContext.getResources().getDrawable(R.drawable.radius_gray80);
		ain = AnimationUtils.loadAnimation(mContext, R.anim.push_in);
//		bitmapConfig = new BitmapDisplayConfig();
//		bitmapConfig.setLoadingDrawable(pic);
//		bitmapConfig.setLoadFailedDrawable(pic);
//		bitmapConfig.setAnimation(ain);
//		bitmapConfig.setBitmapConfig(Bitmap.Config.RGB_565);
//		bitmapUtils = BitmapHelp.getBitmapUtils(mContext);

		imageOptions = new ImageOptions.Builder()
//				.setSize(DensityUtil.dip2px(120), DensityUtil.dip2px(120))
//				.setRadius(DensityUtil.dip2px(5))
				// 如果ImageView的大小不是定义为wrap_content, 不要crop.
				.setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
				// 加载中或错误图片的ScaleType
				//.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
				.setImageScaleType(ImageView.ScaleType.FIT_XY)
				.setLoadingDrawableId(R.drawable.radius_gray80)
				.setFailureDrawableId(R.drawable.radius_gray80)
				.build();

	}

	static class ViewHolder {
		public ImageView mHotPic1;
		public TextView mHotNew;
	}

	@Override
	public int getCount() {
		return mHotIssues.size();
	}

	@Override
	public Object getItem(int position) {
		return mHotIssues.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint({ "NewApi", "InlinedApi" })
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {

			convertView = inflater.inflate(R.layout.item_home_project_zhuanti,
					null);
			viewHolder = new ViewHolder();

			viewHolder.mHotPic1 = convertView
					.findViewById(R.id.home_project_item_zhuanti_iv);
			viewHolder.mHotNew = convertView
					.findViewById(R.id.home_project_item_zhuanti_tv);

			LayoutParams params0 = viewHolder.mHotPic1.getLayoutParams();
			params0.height = (windowsWight - 30) * 374 / 690;
			viewHolder.mHotPic1.setLayoutParams(params0);

			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		hotIsData = mHotIssues.get(position);

//		bitmapUtils.display(viewHolder.mHotPic1, hotIsData.getImg(),
//				bitmapConfig);
		Glide.with(mContext).load(hotIsData.getImg())
				.transform(new GlideRoundTransform(mContext, Utils.dip2px(5)))
				.into(viewHolder.mHotPic1);


		String isnew = hotIsData.getIsnew();
		if (isnew.equals("1")) {
			viewHolder.mHotNew.setVisibility(View.VISIBLE);
		} else {
			viewHolder.mHotNew.setVisibility(View.GONE);
		}

		return convertView;
	}

	public void add(List<ProjectHotztData> infos) {
		mHotIssues.addAll(infos);
	}

}