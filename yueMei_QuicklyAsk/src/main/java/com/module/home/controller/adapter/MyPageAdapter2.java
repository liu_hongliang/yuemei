package com.module.home.controller.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.module.other.activity.LimitTaoFragment;

public class MyPageAdapter2 extends FragmentStatePagerAdapter {

	private final String TAG = "MyPageAdapter2";

	private String[] title;
	private String[] partId;
	private String type;

	public MyPageAdapter2(FragmentManager fm, String[] title, String[] partId,
			String type) {
		super(fm);
		this.title = title;
		this.partId = partId;
		this.type = type;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return title[position];
	}

	@Override
	public int getCount() {
		return title.length;
	}

	@Override
	public Fragment getItem(int position) {
		LimitTaoFragment f1 = LimitTaoFragment.newInstance(position, partId,
				type);
		return f1;
	}
}
