package com.module.home.controller.other;

import android.content.Intent;

import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.view.webclient.BaseWebViewClientCallback;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.home.controller.activity.WebUrlTitleActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.ParserPagramsForWebUrl;

import org.json.JSONObject;

/**
 * Created by 裴成浩 on 2018/1/18.
 */

public class UrlTitleWebViewClient implements BaseWebViewClientCallback {

    private Intent intent;
    private WebUrlTitleActivity mActivity;
    private String TAG = "UrlTitleWebViewClient";

    public UrlTitleWebViewClient(WebUrlTitleActivity activity) {
        this.mActivity = activity;
        intent = new Intent();
    }

    @Override
    public void otherJump(String urlStr) throws Exception {
        showWebDetail(urlStr);
    }

    private void showWebDetail(String urlStr) throws Exception {
        ParserPagramsForWebUrl parserWebUrl = new ParserPagramsForWebUrl();
        parserWebUrl.parserPagrms(urlStr);
        JSONObject obj = parserWebUrl.jsonObject;

        String mType = obj.getString("type");
        switch (mType) {
            case "1":// 医生详情页
                String id1 = obj.getString("id");
                intent.putExtra("docId", id1);
                intent.putExtra("docName", "");
                intent.putExtra("partId", "");
                intent.setClass(mActivity, DoctorDetailsActivity592.class);
                mActivity.startActivity(intent);
                break;

            case "6":// 问答详情

                String link6 = obj.getString("link");
                String qid6 = obj.getString("id");

                intent.putExtra("url", FinalConstant.baseUrl + FinalConstant.VER + link6);
                intent.putExtra("qid", qid6);
                intent.setClass(mActivity, DiariesAndPostsActivity.class);
                mActivity.startActivity(intent);

                break;

        }
    }


}
