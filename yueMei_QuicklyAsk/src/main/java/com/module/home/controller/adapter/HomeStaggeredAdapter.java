package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.BBsListTag;
import com.module.community.model.bean.Img310;
import com.module.community.model.bean.TagRank;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.model.bean.FeedSkuRecommendData;
import com.module.home.model.bean.TaoBean;
import com.module.home.model.bean.TuijHosDoc;
import com.module.home.model.bean.TuijOther;
import com.module.home.model.bean.Tuijshare;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.MyToast;
import com.zfdang.multiple_images_selector.YMGridLayoutManager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Handler;

public class HomeStaggeredAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "HomeStaggeredAdapter";
    private Activity mContext;
    private List<Tuijshare> mDatas = new ArrayList<>();

    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TOW = 2;
    private final int ITEM_TYPE_THTEE = 3;
    //日记本推荐类型
    private final int ITEM_TYPE_FOUR = 4;
    private LayoutInflater mInflater;
    private int windowsWight;
    private int itemWidth;
    private LinkedHashMap<Integer, Integer> imgHeights = new LinkedHashMap();
    private ZanOrJuBaoApi mZanOrJuBaoApi;


    public HomeStaggeredAdapter(Activity context, List<BBsListData550> datas) {
        this(context, null, datas);
    }

    public HomeStaggeredAdapter(Activity context, List<Tuijshare> tuiDatas, List<BBsListData550> bbsDatas) {
        this.mContext = context;
        if (tuiDatas != null) {
            this.mDatas = tuiDatas;
        } else {
            for (BBsListData550 data : bbsDatas) {
                mDatas.add(new Tuijshare(data));
            }
        }
        mInflater = LayoutInflater.from(context);
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        itemWidth = (windowsWight - Utils.dip2px(15)) / 2;
        mZanOrJuBaoApi = new ZanOrJuBaoApi();
    }

    @Override
    public int getItemViewType(int position) {
        Log.e(TAG, "mDatas == " + mDatas);
        Log.e(TAG, "position == " + position);
        Tuijshare tuijshare = mDatas.get(position);
        TuijHosDoc hosDoc = tuijshare.getHos_doc();
        TuijOther other = tuijshare.getOther();
        if (hosDoc != null) {
            return ITEM_TYPE_TOW;
        } else if (null != other) {
            return ITEM_TYPE_THTEE;
        } else if (tuijshare.getFeedSkuRecommendData() != null) {
            return ITEM_TYPE_FOUR;
        } else {
            return ITEM_TYPE_ONE;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ONE:
                return new PostViewHolder(mInflater.inflate(R.layout.home_staggered_common, parent, false));
            case ITEM_TYPE_TOW:
                return new HosDocViewHolder(mInflater.inflate(R.layout.home_staggered_hos, parent, false));
            case ITEM_TYPE_THTEE:
                return new OtherViewHolder(mInflater.inflate(R.layout.home_staggered_other, parent, false));
            case ITEM_TYPE_FOUR:
                return new FeedSkuRecommendViewHolder(mInflater.inflate(R.layout.home_staggered_sku_commend, parent, false));
            default:
                return new PostViewHolder(mInflater.inflate(R.layout.home_staggered_common, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof PostViewHolder) {
            setTypePost1((PostViewHolder) viewHolder, mDatas.get(position).getPost(), position);
        } else if (viewHolder instanceof HosDocViewHolder) {
            setTypeHosDoc((HosDocViewHolder) viewHolder, mDatas.get(position).getHos_doc(), position);
        } else if (viewHolder instanceof OtherViewHolder) {
            setTypeOther((OtherViewHolder) viewHolder, mDatas.get(position).getOther(), position);
        } else if (viewHolder instanceof FeedSkuRecommendViewHolder) {
            setTypeSkuCommend((FeedSkuRecommendViewHolder) viewHolder, mDatas.get(position).getFeedSkuRecommendData(), position);
        }
    }

    private void setTypePost1(final PostViewHolder postViewHolder, final BBsListData550 post, final int position) {
        setImage(postViewHolder, position, post);

        String is_video = post.getIs_video();
        if (!TextUtils.isEmpty(is_video) && "1".equals(is_video)) {
            postViewHolder.homeVideo.setVisibility(View.VISIBLE);
        } else {
            postViewHolder.homeVideo.setVisibility(View.GONE);
        }
        String title = post.getTitle();
        if (!TextUtils.isEmpty(title)) {
            postViewHolder.homeTitle.setVisibility(View.VISIBLE);
            postViewHolder.homeTitle.setText(title);
        } else {
            postViewHolder.homeTitle.setVisibility(View.GONE);
        }

        String user_name = post.getUser_name();
        if (!TextUtils.isEmpty(user_name)) {
            postViewHolder.homeUserVisorgone.setVisibility(View.VISIBLE);
            postViewHolder.homeUserName.setText(user_name);
            String agree_num = post.getAgree_num();
            if (!"0".equals(agree_num) && !TextUtils.isEmpty(agree_num)) {
                postViewHolder.homeLikeNum.setVisibility(View.VISIBLE);
                postViewHolder.homeLike.setVisibility(View.VISIBLE);
                postViewHolder.homeLikeNum.setText(agree_num);
            } else {
                postViewHolder.homeLikeNum.setVisibility(View.GONE);
                postViewHolder.homeLike.setVisibility(View.GONE);
            }

            Glide.with(mContext)
                    .load(post.getUser_img())
                    .dontAnimate()
                    .transform(new GlideCircleTransform(mContext))
                    .into(postViewHolder.homeUserImg);
        }

        TaoBean tao = post.getTao();
        String titleTao = tao.getTitle();
        if (!TextUtils.isEmpty(titleTao)) {
            postViewHolder.homeSkuVisorgone.setVisibility(View.VISIBLE);
            postViewHolder.homeSkuName.setText(titleTao);
            postViewHolder.homeTag.setVisibility(View.GONE);
            postViewHolder.homeSkuPrice.setText("¥" + tao.getPrice());
            postViewHolder.homeSkuVisorgone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onPostSkuListener(v, mDatas.get(position).getPost(), position);
                    }
                }
            });
        } else {
            postViewHolder.homeSkuVisorgone.setVisibility(View.GONE);
            List<BBsListTag> postTag = post.getTag();
            if (null != postTag && postTag.size() > 0) {
                postViewHolder.homeTag.setVisibility(View.VISIBLE);
                StringBuilder tagTxt = new StringBuilder();
                for (int i = 0; i < postTag.size(); i++) {
                    tagTxt.append("#" + postTag.get(i).getName());
                }
                postViewHolder.homeTag.setText(tagTxt);
            } else {
                postViewHolder.homeTag.setVisibility(View.GONE);
            }
        }


        //设置奖牌
        TagRank tag_rank = post.getTag_rank();
        if (tag_rank != null) {
            String level = tag_rank.getRanking();
            if (!TextUtils.isEmpty(tag_rank.getTag()) && !"0".equals(level)) {
                postViewHolder.tao_list_top_tag_container.setVisibility(View.VISIBLE);
                postViewHolder.doc_list_item_level.setText("NO." + level);
                postViewHolder.tao_list_top_tag.setText(tag_rank.getTag());
            } else {
                postViewHolder.tao_list_top_tag_container.setVisibility(View.GONE);
            }
        }

        String askorshare = post.getAskorshare();
        if (!TextUtils.isEmpty(askorshare)) {
            postViewHolder.homeBBsTypeFl.setVisibility(View.VISIBLE);
            postViewHolder.homeBbsTypeText.setVisibility(View.VISIBLE);
            switch (askorshare) {
                case "0"://问答
                    postViewHolder.homeBbsTypeText.setText("问答");
                    postViewHolder.homeBbsTypeText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_bbs_type_ask));
                    postViewHolder.homeBBsTypeImg.setBackgroundResource(R.drawable.home_ask_background);
                    break;
                case "10"://招募
                    postViewHolder.homeBbsTypeText.setText("招募");
                    postViewHolder.homeBbsTypeText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_bbs_type_recruit));
                    postViewHolder.homeBBsTypeImg.setBackgroundResource(R.drawable.home_recruit_background);
                    break;
                case "13"://投票贴
                    postViewHolder.homeBbsTypeText.setText("投票");
                    postViewHolder.homeBbsTypeText.setBackground(ContextCompat.getDrawable(mContext, R.drawable.home_bbs_type_vote));
                    postViewHolder.homeBBsTypeImg.setBackgroundResource(R.drawable.home_vote_background);
                    break;
                default:
                    postViewHolder.homeBBsTypeFl.setVisibility(View.GONE);
                    postViewHolder.homeBbsTypeText.setVisibility(View.GONE);
                    break;
            }
        } else {
            postViewHolder.homeBBsTypeFl.setVisibility(View.GONE);
            postViewHolder.homeBbsTypeText.setVisibility(View.GONE);
        }
        postViewHolder.homeLikeClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    String is_agree = post.getIs_agree();
                    String agree_num = post.getAgree_num();
                    if (!TextUtils.isEmpty(agree_num)) {
                        int agreeNum = Integer.parseInt(agree_num);
                        if ("0".equals(is_agree)) {
                            postViewHolder.homeLike.setImageResource(R.drawable.diary_list_recycler_praise_selected);
                            agreeNum++;
                            mDatas.get(position).getPost().setIs_agree("1");
                        } else {
                            postViewHolder.homeLike.setImageResource(R.drawable.diary_list_suction_bottom_like);
                            agreeNum--;
                            mDatas.get(position).getPost().setIs_agree("0");
                        }

                        postViewHolder.homeLikeNum.setText(agreeNum + "");
                        mDatas.get(position).getPost().setAgree_num(agreeNum + "");
                        if (agreeNum > 0) {
                            postViewHolder.homeLikeNum.setVisibility(View.VISIBLE);
                        } else {
                            postViewHolder.homeLikeNum.setVisibility(View.GONE);
                        }
                    }

                    mZanOrJuBaoApi.addData("id", post.getQ_id());
                    mZanOrJuBaoApi.addData("flag", "1");
                    mZanOrJuBaoApi.addData("puid", post.getUser_id());
                    mZanOrJuBaoApi.getCallBack(mContext, mZanOrJuBaoApi.getHashMap(), new BaseCallBackListener<ServerData>() {
                        @Override
                        public void onSuccess(ServerData data) {
                            Log.e(TAG, "点赞成功==>" + data.code);
                            if (!data.isOtherCode) {
                                MyToast.makeTextToast2(mContext, data.message, MyToast.SHOW_TIME).show();

                            }
                        }
                    });
                }
            }
        });
        postViewHolder.homeStaggeredContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onPostItemListener(v, position, false);
                }
            }
        });

    }

    private void setTypeHosDoc(HosDocViewHolder hosDocViewHolder, final TuijHosDoc hos_doc, final int position) {
        Glide.with(mContext)
                .load(hos_doc.getImg())
                .dontAnimate()
                .transform(new GlideCircleTransform(mContext))
                .into(hosDocViewHolder.homeHosImg);
        hosDocViewHolder.homeHosName.setText(hos_doc.getName());
        hosDocViewHolder.homeHosOrding.setText(hos_doc.getYuding());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        List<HomeTaoData> tao = hos_doc.getTao();
        List<HomeTaoData> taoData = new ArrayList<>();
        for (int i = 0; i < tao.size(); i++) {
            if (i < 3) {
                taoData.add(tao.get(i));
            }
        }
        HomeItemHosAdapter homeItemHosAdapter = new HomeItemHosAdapter(R.layout.home_item_hos, taoData);
        hosDocViewHolder.homeHosList.setLayoutManager(linearLayoutManager);
        hosDocViewHolder.homeHosList.setAdapter(homeItemHosAdapter);
        homeItemHosAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                HomeTaoData taoData1 = hos_doc.getTao().get(position);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HTMLHOME_TO_TAO, (position + 1) + "", taoData1.getBmsid(), "1"), taoData1.getEvent_params(), new ActivityTypeData("1"));

                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                intent.putExtra("id", taoData1.get_id());
                intent.putExtra("source", "1");
                intent.putExtra("objid", "0");
                mContext.startActivity(intent);

            }
        });
    }

    private void setTypeOther(OtherViewHolder postViewHolder, TuijOther other, final int position) {
        Glide.with(mContext)
                .load(other.getImg())
                .dontAnimate()
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .into(postViewHolder.homeImage);
        postViewHolder.homeTitle.setText(other.getTitle());
        postViewHolder.homeStaggeredContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onPostItemListener(v, position, true);
                }
            }
        });
    }

    private void setTypeSkuCommend(FeedSkuRecommendViewHolder skuRecommendViewHolder, final List<FeedSkuRecommendData> feedSkuRecommendData, final int position) {
        YMGridLayoutManager gridLayoutManager = new YMGridLayoutManager(mContext, 2);
        HomeItemSkuCommendAdapter homeItemSkuCommendAdapter = new HomeItemSkuCommendAdapter(R.layout.item_home_sku_commend, feedSkuRecommendData);
        skuRecommendViewHolder.rv_sku_commend.setLayoutManager(gridLayoutManager);
        skuRecommendViewHolder.rv_sku_commend.setAdapter(homeItemSkuCommendAdapter);
        homeItemSkuCommendAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                WebUrlTypeUtil.getInstance(mContext).urlToApp(feedSkuRecommendData.get(position).getUrl());
            }
        });
    }

    /**
     * 设置图片
     *
     * @param holder
     * @param position
     * @param post
     */
    private void setImage(@NonNull final PostViewHolder holder, final int position, BBsListData550 post) {
        if (imgHeights.containsKey(position)) {
            int imgHeight = imgHeights.get(position);

            ViewGroup.LayoutParams params = holder.homeImage.getLayoutParams();
            params.width = itemWidth;
            params.height = imgHeight;

            holder.homeImage.setLayoutParams(params);

            Glide.with(mContext).load(post.getImg310().getImg())
                    .dontAnimate()
                    .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.homeImage);

        } else {
            Img310 img310 = post.getImg310();
            String imgImg = img310.getImg();

            int intrinsicWidth;
            try {
                intrinsicWidth = Integer.parseInt(img310.getWidth());
            } catch (NumberFormatException e) {
                intrinsicWidth = 0;
            }
            int intrinsicHeight;
            try {
                intrinsicHeight = Integer.parseInt(img310.getHeight());
            } catch (NumberFormatException e) {
                intrinsicHeight = 0;
            }

            if (intrinsicWidth != 0 && intrinsicHeight != 0) {
                int imgHeight = (itemWidth * intrinsicHeight) / intrinsicWidth;

                ViewGroup.LayoutParams params = holder.homeImage.getLayoutParams();
                params.width = itemWidth;
                params.height = imgHeight;

                Log.e(TAG, "111width == " + intrinsicWidth);
                Log.e(TAG, "111height == " + intrinsicHeight);
                holder.homeImage.setLayoutParams(params);

                imgHeights.put(position, imgHeight);

                Glide.with(mContext)
                        .load(imgImg)
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(holder.homeImage);

            } else {

                Glide.with(mContext)
                        .load(imgImg)
                        .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(4), GlidePartRoundTransform.CornerType.TOP))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                int intrinsicWidth = resource.getIntrinsicWidth();
                                int intrinsicHeight = resource.getIntrinsicHeight();
                                Log.e(TAG, "intrinsicWidth == " + intrinsicWidth);
                                Log.e(TAG, "intrinsicHeight == " + intrinsicHeight);

                                int imgHeight = (itemWidth * intrinsicHeight) / intrinsicWidth;

                                Log.e(TAG, "imgWidth == " + itemWidth);
                                Log.e(TAG, "imgHeight == " + imgHeight);


                                ViewGroup.LayoutParams params = holder.homeImage.getLayoutParams();
                                params.width = itemWidth;
                                params.height = imgHeight;
                                holder.homeImage.setLayoutParams(params);

                                imgHeights.put(position, imgHeight);

                                holder.homeImage.setImageDrawable(resource);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                                int imageHeight = 0;
                                ViewGroup.LayoutParams params = holder.homeImage.getLayoutParams();
                                params.width = itemWidth;
                                params.height = imageHeight;
                                holder.homeImage.setLayoutParams(params);
                                imgHeights.put(position, imageHeight);

                            }
                        });

            }
        }
        //距离
        if (post.getHospital_distance() != null) {
            if (TextUtils.isEmpty(post.getHospital_distance().getDistance())
                    && TextUtils.isEmpty(post.getHospital_distance().getBusiness_district())) {
                holder.ll_diance.setVisibility(View.GONE);
            } else {
                holder.tv_diance.setText(post.getHospital_distance().getBusiness_district() + "·距您" + post.getHospital_distance().getDistance());
                holder.ll_diance.setVisibility(View.VISIBLE);
            }
        } else {
            holder.ll_diance.setVisibility(View.GONE);
        }
    }


    class PostViewHolder extends RecyclerView.ViewHolder {

        private final FrameLayout homeStaggeredContainer;
        private final FrameLayout homeImgContainer;
        private LinearLayout ll_diance;
        private TextView tv_diance;
        private final FrameLayout homeBBsTypeFl;
        private final ImageView homeBBsTypeImg;
        private final ImageView homeImage;
        private final ImageView homeVideo;
        private final ImageView homeUserImg;
        private final ImageView homeLike;
        private final TextView homeTitle;
        private final TextView homeBbsTypeText;
        private final TextView homeUserName;
        private final TextView homeLikeNum;
        private final TextView homeSkuName;
        private final TextView homeSkuPrice;
        private final LinearLayout homeSkuVisorgone;
        private final RelativeLayout homeUserVisorgone;
        private final LinearLayout homeLikeClick;
        private final TextView homeTag;
        private final TextView homeHotSku;
        private LinearLayout tao_list_top_tag_container;
        private TextView doc_list_item_level;
        private TextView tao_list_top_tag;


        PostViewHolder(@NonNull View itemView) {
            super(itemView);
            homeStaggeredContainer = itemView.findViewById(R.id.home_staggered_container);
            homeImgContainer = itemView.findViewById(R.id.home_item_img_container);
            ll_diance = itemView.findViewById(R.id.ll_diance);
            tv_diance = itemView.findViewById(R.id.tv_diance);
            homeBBsTypeFl = itemView.findViewById(R.id.home_bbs_type_fl);
            homeBBsTypeImg = itemView.findViewById(R.id.home_bbs_type_img);
            homeImage = itemView.findViewById(R.id.home_item_img);
            homeVideo = itemView.findViewById(R.id.home_item_video);
            homeTitle = itemView.findViewById(R.id.home_item_title);
            homeBbsTypeText = itemView.findViewById(R.id.home_bbs_type_text);
            homeUserImg = itemView.findViewById(R.id.home_item_userimg);
            homeUserName = itemView.findViewById(R.id.home_item_username);
            homeLikeClick = itemView.findViewById(R.id.home_like_click);
            homeLike = itemView.findViewById(R.id.home_item_like);
            homeLikeNum = itemView.findViewById(R.id.home_item_likenum);
            homeUserVisorgone = itemView.findViewById(R.id.home_user_visorgone);
            homeTag = itemView.findViewById(R.id.home_item_tag);
            homeHotSku = itemView.findViewById(R.id.home_hot_sku);
            homeSkuVisorgone = itemView.findViewById(R.id.home_item_sku_visorgone);
            homeSkuName = itemView.findViewById(R.id.home_item_skuname);
            homeSkuPrice = itemView.findViewById(R.id.home_item_skuprice);

            tao_list_top_tag_container = itemView.findViewById(R.id.tao_list_top_tag_container);
            doc_list_item_level = itemView.findViewById(R.id.doc_list_item_level);
            tao_list_top_tag = itemView.findViewById(R.id.tao_list_top_tag);
        }
    }

    class OtherViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout homeStaggeredContainer;
        private final FrameLayout homeImgContainer;
        private final ImageView homeImage;
        private final ImageView homeVideo;
        private final TextView homeTitle;

        public OtherViewHolder(@NonNull View itemView) {
            super(itemView);
            homeStaggeredContainer = itemView.findViewById(R.id.home_staggered_container);
            homeImgContainer = itemView.findViewById(R.id.home_item_img_container);
            homeImage = itemView.findViewById(R.id.home_item_img);
            homeVideo = itemView.findViewById(R.id.home_item_video);
            homeTitle = itemView.findViewById(R.id.home_item_title);
        }
    }

    class HosDocViewHolder extends RecyclerView.ViewHolder {
        private final ImageView homeHosImg;
        private final TextView homeHosName;
        private final TextView homeHosOrding;
        //        private final TextView homeHosGoodReputation;
        private final RecyclerView homeHosList;

        HosDocViewHolder(@NonNull View itemView) {
            super(itemView);
            homeHosImg = itemView.findViewById(R.id.home_staggered_hos_img);
            homeHosName = itemView.findViewById(R.id.home_staggered_hos_name);
            homeHosOrding = itemView.findViewById(R.id.home_staggered_hos_ording);
//            homeHosGoodReputation=itemView.findViewById(R.id.home_staggered_hos_goodreputation);
            homeHosList = itemView.findViewById(R.id.home_staggered_list);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onHosDocItemListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    class FeedSkuRecommendViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView rv_sku_commend;

        FeedSkuRecommendViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_sku_commend = itemView.findViewById(R.id.home_staggered_list);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onFeedSkuRecommendItemListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (mDatas != null) {
            size = mDatas.size();
        }
        return size;
    }

    //添加单条数据
    public void addItem(int position, Tuijshare data) {
        mDatas.add(position, data);
        notifyItemInserted(position);
        //局部刷新
        notifyItemRangeChanged(position, mDatas.size() - position);
    }

    /**
     * 添加数据
     *
     * @param datas
     */
    public void addData(ArrayList<BBsListData550> datas) {
        for (BBsListData550 data : datas) {
            mDatas.add(new Tuijshare(data));
        }

        notifyItemRangeChanged(mDatas.size() - datas.size(), datas.size());
    }

    public List<Tuijshare> getData() {
        return mDatas;
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        //普通item点击 是否是其他类型的点击
        void onPostItemListener(View view, int pos, boolean isOther);

        //普通sku
        void onPostSkuListener(View view, BBsListData550 data, int pos);

        //医生医院item点击
        void onHosDocItemListener(View view, int pos);

        //日记本推荐item点击
        void onFeedSkuRecommendItemListener(View view, int pos);
    }
}
