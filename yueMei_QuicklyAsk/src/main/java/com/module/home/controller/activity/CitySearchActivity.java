package com.module.home.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.Listener.OnCitySelectListener;
import com.module.doctor.controller.adapter.CitySearchAdapter;
import com.module.doctor.model.api.SearchCityApi;
import com.module.doctor.model.bean.CityDocDataitem;
import com.module.event.CitySearchEvent;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;


/**
 * 文 件 名: CitySearchActivity
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-08 00:08
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class CitySearchActivity extends YMBaseActivity {
    private ImageView iv_close;
    private LinearLayout ll_search;
    private EditText search_edit;
    private ImageView search_title_colse;
    private ListView lv;
    private List<CityDocDataitem> mCityList;
    private String type;
    private static OnCitySelectListener mOnCitySelectListener;
    private boolean isOnlyOneCity = false;
    private String onlyOneCityString;

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(CitySearchEvent msgEvent) {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_city_search;
    }

    @Override
    protected void initView() {
        type = getIntent().getStringExtra("type");
        //EditText相关
        iv_close = findViewById(R.id.iv_close);
        ll_search = findViewById(R.id.ll_search);
        search_edit = findViewById(R.id.search_edit);
        search_title_colse = findViewById(R.id.search_title_colse);
        lv = findViewById(R.id.lv);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnCitySelectListener != null) {
                    mOnCitySelectListener.onCitySelectResult("");
                    setOnCitySelectListener(null);
                }
                finish();
            }
        });
        Utils.showSoftInputFromWindow(mContext, search_edit);
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    search_title_colse.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                } else {
                    search_title_colse.setVisibility(View.VISIBLE);
                    sendSearchKey(search_edit.getText().toString().trim());
                }
            }
        });
        //清空搜索列表
        search_title_colse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mContext instanceof Activity) {
//                    Utils.hideSoftKeyboard((Activity) mContext);
//                }
                search_edit.setText("");
                lv.setVisibility(View.GONE);
            }
        });

        search_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“搜索”键*/
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (TextUtils.isEmpty(search_edit.getText().toString().trim())) {
                        MyToast.makeTextToast2(mContext, "请输入", 1000).show();
                        return false;
                    }
                    //隐藏软键盘
                    if (mContext instanceof Activity) {
                        Utils.hideSoftKeyboard((Activity) mContext);
                    }

                    if(isOnlyOneCity){
                        if (!TextUtils.isEmpty(type) && type.equals("6")) {
                            Cfg.saveStr(mContext, FinalConstant.DWCITY, onlyOneCityString);
                            Utils.getCityOneToHttp(mContext, "1");
                            EventBus.getDefault().post(new CitySearchEvent(1));
                            finish();
                            if (mOnCitySelectListener != null) {
                                mOnCitySelectListener.onCitySelectResult("1");
                                setOnCitySelectListener(null);
                            }
                        } else {
                            Cfg.saveStr(mContext, FinalConstant.DWCITY, onlyOneCityString);
                            Utils.getCityOneToHttp(mContext, "1");
                            EventBus.getDefault().post(new CitySearchEvent(1));
                            finish();
                        }
                    }
//                    sendSearchKey(search_edit.getText().toString().trim());

                    return true;
                }
                return false;
            }
        });
    }

    public static void setOnCitySelectListener(OnCitySelectListener onCitySelectListener) {
        mOnCitySelectListener = onCitySelectListener;
    }

    private void sendSearchKey(String edit) {
        SearchCityApi searchCityApi = new SearchCityApi();
        searchCityApi.getHashMap().clear();
        searchCityApi.addData("search_key", edit);
        searchCityApi.getCallBack(mContext, searchCityApi.getHashMap(), new BaseCallBackListener<List<CityDocDataitem>>() {
            @Override
            public void onSuccess(List<CityDocDataitem> cityDocDataitems) {
                if (cityDocDataitems != null && cityDocDataitems.size() > 0) {
                    if (cityDocDataitems.size() == 1) {
                        isOnlyOneCity = true;
                        onlyOneCityString = cityDocDataitems.get(0).getName();
                    } else {
                        isOnlyOneCity = false;
                    }
                    lv.setVisibility(View.VISIBLE);
                    mCityList = cityDocDataitems;
                    CitySearchAdapter citySearchAdapter = new CitySearchAdapter(mContext, mCityList);
                    lv.setAdapter(citySearchAdapter);
                } else {
                    lv.setVisibility(View.GONE);
                    MyToast.makeTextToast2(mContext, "抱歉,未找到相关城市,请重试", 1000).show();
                }
            }
        });
    }

    @Override
    protected void initData() {
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!TextUtils.isEmpty(type) && type.equals("6")) {
                    Cfg.saveStr(mContext, FinalConstant.DWCITY, mCityList.get(position).getName());
                    Utils.getCityOneToHttp(mContext, "1");
                    EventBus.getDefault().post(new CitySearchEvent(1));
                    finish();
                    if (mOnCitySelectListener != null) {
                        mOnCitySelectListener.onCitySelectResult("1");
                        setOnCitySelectListener(null);
                    }
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DWCITY, mCityList.get(position).getName());
                    Utils.getCityOneToHttp(mContext, "1");
                    EventBus.getDefault().post(new CitySearchEvent(1));
                    finish();
                }
            }
        });
    }

    public static void invoke(Context context, String type) {
        Intent intent = new Intent(context, CitySearchActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
