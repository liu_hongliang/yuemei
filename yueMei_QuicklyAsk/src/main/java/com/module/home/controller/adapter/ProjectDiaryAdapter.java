package com.module.home.controller.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.community.model.bean.BBsListData550;
import com.module.home.model.bean.CashBackBean;
import com.module.home.model.bean.TaoBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;

import org.kymjs.aframe.ui.ViewInject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 裴成浩 on 2019/2/28
 */
public class ProjectDiaryAdapter extends RecyclerView.Adapter<ProjectDiaryAdapter.ViewHolder> {

    private Activity mContext;
    private List<BBsListData550> mDatas;
    private final int windowsWight;
    private final String TAG = "ProjectDiaryAdapter";

    public ProjectDiaryAdapter(Activity context, List<BBsListData550> datas) {
        this.mContext = context;
        this.mDatas = datas;
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_home_diaryother, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        final BBsListData550 hotIsData = mDatas.get(position);

        Glide.with(mContext).load(hotIsData.getUser_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsHeadPic);

        viewHolder.mBBsName.setText(hotIsData.getUser_name());
        viewHolder.mBBsTime.setText(hotIsData.getTime()+"   共更新"+hotIsData.getShareNum()+"篇");
        viewHolder.mBBsTitle.setText(hotIsData.getTitle());
        Log.e(TAG, "hotIsData.get_id() === " + hotIsData.get_id());
        Log.e(TAG, "hotIsData.getTitle() === " + hotIsData.getTitle());
        Log.e(TAG, "hotIsData.getAnswer_num() === " + hotIsData.getAnswer_num());



        CashBackBean cashback = hotIsData.getCashback();
        if(cashback != null){
            if ("1".equals(cashback.getIs_cashback())) {
                viewHolder.mFanxianLine.setVisibility(View.VISIBLE);
                viewHolder.mFanxianContent.setVisibility(View.VISIBLE);
                viewHolder.mFanxian.setText(hotIsData.getCashback().getCashback_complete());
            } else {
                viewHolder.mFanxianLine.setVisibility(View.GONE);
                viewHolder.mFanxianContent.setVisibility(View.GONE);
            }
        }else{
            viewHolder.mFanxianLine.setVisibility(View.GONE);
            viewHolder.mFanxianContent.setVisibility(View.GONE);
        }


        String picRule = hotIsData.getPicRule();
        if ("1".equals(picRule)) {
            viewHolder.mBBsPicRuleIv1.setVisibility(View.VISIBLE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.VISIBLE);
            if ("0".equals(hotIsData.getAfter_day())) {
                viewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mContext, 13), 0, Utils.dip2px(mContext, 12), 0);
                viewHolder.mBBsPicRuleIv2.setText("After");
            } else {
                viewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mContext, 8), 0, Utils.dip2px(mContext, 7), 0);
                viewHolder.mBBsPicRuleIv2.setText("After" + hotIsData.getAfter_day() + "天");
            }
        } else {
            viewHolder.mBBsPicRuleIv1.setVisibility(View.GONE);
            viewHolder.mBBsPicRuleIv2.setVisibility(View.GONE);
        }


        // 标签
        if (hotIsData.getTao() != null) {
            //关联SKU，
            TaoBean taoBean = hotIsData.getTao();
            if (!"0".equals(hotIsData.getTao().getId())) {

                String totalAppoint = taoBean.getTotalAppoint();
                String member_price = taoBean.getMember_price();
                int i = Integer.parseInt(member_price);
                if (!"0".equals(totalAppoint)) {
                    //且SKU有预订数
                    viewHolder.tagLy.setVisibility(View.GONE);
                    viewHolder.mSkuClick.setVisibility(View.VISIBLE);
                    viewHolder.mSkuClick2.setVisibility(View.GONE);
                    viewHolder.mSkuName.setText(taoBean.getTitle());
                    viewHolder.mSkuPrice.setText(taoBean.getPrice());
                    viewHolder.mSkuOrding.setText(totalAppoint + "人已预订");
                    if (i >= 0) {
                        viewHolder.mSkuPlus1Visorgone.setVisibility(View.VISIBLE);
                        viewHolder.mSkuPlus1Price.setText(member_price);
                    } else {
                        viewHolder.mSkuPlus1Visorgone.setVisibility(View.GONE);
                    }
                    viewHolder.mSkuClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }

                            Intent intent = new Intent(mContext, TaoDetailActivity.class);
                            intent.putExtra("id", hotIsData.getTao().getId());
                            intent.putExtra("source", "0");
                            intent.putExtra("objid", "0");
                            mContext.startActivity(intent);
                        }
                    });

                } else {
                    viewHolder.mSkuClick.setVisibility(View.GONE);
                    viewHolder.mSkuClick2.setVisibility(View.VISIBLE);
                    viewHolder.mSkuName2.setText(taoBean.getTitle());
                    viewHolder.mSkuPrice2.setText(taoBean.getPrice());
                    if (i >= 0) {
                        viewHolder.mSkuPlus2Visorgone.setVisibility(View.VISIBLE);
                        viewHolder.mSkuPlus2Price.setText(member_price);
                    } else {
                        viewHolder.mSkuPlus2Visorgone.setVisibility(View.GONE);
                    }
                    viewHolder.mSkuClick2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, TaoDetailActivity.class);
                            intent.putExtra("id", hotIsData.getTao().getId());
                            intent.putExtra("source", "0");
                            intent.putExtra("objid", "0");
                            mContext.startActivity(intent);
                        }
                    });
                }

            } else {
                viewHolder.mSkuClick.setVisibility(View.GONE);
                viewHolder.mSkuClick2.setVisibility(View.GONE);
                //         标签
                if (null != hotIsData.getTag()) {
                    if (hotIsData.getTag().size() > 0) {
                        viewHolder.tagLy.setVisibility(View.VISIBLE);
                        int tagsize = hotIsData.getTag().size();
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < tagsize; i++) {
                            stringBuilder.append("#" + hotIsData.getTag().get(i).getName());
                        }
                        viewHolder.mTag.setText(stringBuilder);
                    } else {
                        viewHolder.tagLy.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.tagLy.setVisibility(View.GONE);
                }

            }
        }

        // 图片
        if (null != hotIsData.getPic()) {

            if (hotIsData.getPic().size() > 0) {
                if (hotIsData.getPic().size() == 1) {//单张图片的时候

                    viewHolder.mDuozhangLy.setVisibility(View.GONE);
                    viewHolder.mSingalLy.setVisibility(View.VISIBLE);
                    viewHolder.tagLy.setVisibility(View.GONE);
                    viewHolder.mSkuClick.setVisibility(View.GONE);
                    viewHolder.mSkuClick2.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = viewHolder.mSingalLy.getLayoutParams();
                    params.height = ((windowsWight - 40) * 340 / 710);
                    viewHolder.mSingalLy.setLayoutParams(params);
                    //视频播放按钮
                    if ("1".equals(hotIsData.getIs_video())) {
                        viewHolder.mSigngaVideo.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mSigngaVideo.setVisibility(View.GONE);

                    }
                    try {
                        Glide.with(mContext).load(hotIsData.getPic().get(0).getImg())
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(viewHolder.mSingImg);
                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }

                } else if (hotIsData.getPic().size() > 1) {//两张对比图

                    viewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    viewHolder.mSingalLy.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = viewHolder.mDuozhangLy.getLayoutParams();
                    params.height = ((windowsWight - 40) / 2);
                    viewHolder.mDuozhangLy.setLayoutParams(params);

                    if ("1".equals(hotIsData.getIs_video())) {
                        viewHolder.mIdentification1.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.mIdentification1.setVisibility(View.GONE);
                    }

                    try {
                        String img1 = hotIsData.getPic().get(0).getImg();
                        String img2 = hotIsData.getPic().get(1).getImg();

                        if (!TextUtils.isEmpty(img1)) {
                            Glide.with(mContext).load(img1).transform(new GlideRoundTransform(mContext, Utils.dip2px(3))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsIv1);
                        }

                        if (!TextUtils.isEmpty(img2)) {
                            Glide.with(mContext).load(img2).transform(new GlideRoundTransform(mContext, Utils.dip2px(3))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(viewHolder.mBBsIv2);
                        }


                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }
                }
            } else {
                viewHolder.mDuozhangLy.setVisibility(View.GONE);
                viewHolder.mSingalLy.setVisibility(View.GONE);
            }

        } else {
            viewHolder.mDuozhangLy.setVisibility(View.GONE);
            viewHolder.mSingalLy.setVisibility(View.GONE);
        }

        final String isFollowUser = hotIsData.getIs_follow_user();
        Log.e(TAG, "isFollowUser == " + isFollowUser);
        switch (isFollowUser) {
            case "0":          //未关注
                setFocusView(viewHolder, R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                break;
            case "1":          //已关注
                setFocusView(viewHolder, R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                break;
            case "2":          //互相关注
                setFocusView(viewHolder, R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                break;
        }

        viewHolder.mFans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        switch (isFollowUser) {
                            case "0":          //未关注
                                FocusAndCancel(viewHolder, hotIsData.getUser_id(), isFollowUser, position);
                                break;
                            case "1":          //已关注
                            case "2":          //互相关注
                                showDialogExitEdit(viewHolder, hotIsData.getUser_id(), isFollowUser, position);
                                break;
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);

                    }


                } else {
                    Utils.jumpLogin(mContext);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mBBsHeadHeadpic;
        ImageView mBBsHeadPic;
        TextView mBBsName;
        TextView mBBsTime;
        TextView mBBsTitle;

        LinearLayout mFans;
        ImageView mImgageFans;
        TextView mCenterFans;

        FrameLayout mSingalLy;
        ImageView mSingImg;
        ImageView mSigngaVideo;

        RelativeLayout mDuozhangLy;
        ImageView mBBsIv1;
        ImageView mBBsIv2;
        int flag;
        TextView mBBsPicRuleIv1;
        TextView mBBsPicRuleIv2;
        TextView mTag;
        LinearLayout tagLy;
        RelativeLayout mSkuClick;
        TextView mSkuName;
        TextView mSkuPrice;
        TextView mSkuOrding;
        RelativeLayout mSkuClick2;
        TextView mSkuName2;
        TextView mSkuPrice2;
        TextView mSkuPlusPrice;
        LinearLayout mSkuPlus1Visorgone;
        LinearLayout mSkuPlus2Visorgone;
        TextView mSkuPlus1Price;
        TextView mSkuPlus2Price;
        View mFanxianLine;
        LinearLayout mFanxianContent;
        TextView mFanxian;
        ImageView mIdentification1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            flag = getLayoutPosition();

            mBBsHeadHeadpic = itemView.findViewById(R.id.diary_list_headpic_ly);
            mBBsHeadPic = itemView.findViewById(R.id.bbs_list_head_image_iv);
            mBBsName = itemView.findViewById(R.id.bbs_list_name_tv);
            mBBsTime = itemView.findViewById(R.id.bbs_list_time_tv);
            mBBsTitle = itemView.findViewById(R.id.bbs_list_title_tv);

            mFans = itemView.findViewById(R.id.diary_fans);
            mImgageFans = itemView.findViewById(R.id.diary_imgage_fans);
            mCenterFans = itemView.findViewById(R.id.diary_center_fans);

            mDuozhangLy = itemView.findViewById(R.id.bbs_list_duozhang_ly);
            mBBsIv1 = itemView.findViewById(R.id.bbs_list_duozhang_iv1);
            mBBsIv2 = itemView.findViewById(R.id.bbs_list_duozhang_iv2);


            mSingalLy = itemView.findViewById(R.id.bbs_list_pic_danzhang_ly);
            mSingImg = itemView.findViewById(R.id.bbs_list_pic_img);
            mSigngaVideo = itemView.findViewById(R.id.bbs_list_video);
            mSkuOrding = itemView.findViewById(R.id.bbs_list_sku_ording);

            mBBsPicRuleIv1 = itemView.findViewById(R.id.bbs_list_picrule1);
            mBBsPicRuleIv2 = itemView.findViewById(R.id.bbs_list_picrule2);


            mTag = itemView.findViewById(R.id.bbs_list_tag_tv);
            tagLy = itemView.findViewById(R.id.tag_ly_550);
            mSkuClick = itemView.findViewById(R.id.bbs_item_sku_click);
            mSkuName = itemView.findViewById(R.id.bbs_list_sku_name);
            mSkuPrice = itemView.findViewById(R.id.bbs_list_sku_price);
            mSkuClick2 = itemView.findViewById(R.id.bbs_item_sku_click2);
            mSkuName2 = itemView.findViewById(R.id.bbs_list_sku_name2);
            mSkuPrice2 = itemView.findViewById(R.id.bbs_list_sku_price2);
            mSkuPlusPrice = itemView.findViewById(R.id.plus_price);

            mSkuPlus1Visorgone = itemView.findViewById(R.id.sku_plus1_visorgone);
            mSkuPlus2Visorgone = itemView.findViewById(R.id.sku_plus2_visorgone);
            mSkuPlus1Price = itemView.findViewById(R.id.sku_plus_price);
            mSkuPlus2Price = itemView.findViewById(R.id.sku_plus_price2);

            mIdentification1 = itemView.findViewById(R.id.iv_video_identification1);
            mFanxianLine = itemView.findViewById(R.id.diary_fanxian_line);
            mFanxianContent = itemView.findViewById(R.id.diary_fanxian_content);
            mFanxian = itemView.findViewById(R.id.diary_fanxian);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemPersonClickListener != null) {
                        onItemPersonClickListener.onItemClick(getLayoutPosition());
                    }
                }
            });

            mFanxianContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String link = FinalConstant.baseUrl + FinalConstant.VER + "/forum/postinfo/id/939555/";
                    Intent intent = new Intent();
                    intent.setClass(mContext, DiariesAndPostsActivity.class);
                    intent.putExtra("url", link);
                    intent.putExtra("qid", "939555");
                    mContext.startActivity(intent);
                }
            });

            Log.e(TAG, "mDatas === " + mDatas.size());

            //跳转到个人中心页
            mBBsHeadHeadpic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final BBsListData550 hotIsData = mDatas.get(getLayoutPosition());
                    if (!TextUtils.isEmpty(hotIsData.getUser_id()) && onItemPersonClickListener != null) {
                        onItemPersonClickListener.onItemPersonClick(hotIsData.getUser_id(), getLayoutPosition());
                    }
                }
            });
        }
    }

    private void showDialogExitEdit(final ViewHolder viewHolder, final String id, final String mFolowing, final int position) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(viewHolder, id, mFolowing, position);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }


    /**
     * 关注和取消关注
     */
    private void FocusAndCancel(final ViewHolder viewHolder, String id, final String mFolowing, final int position) {
        Log.e(TAG, "id === " + id);
        Log.e(TAG, "mFolowing === " + mFolowing);
        Log.e(TAG, "position === " + position);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mDatas.get(position).setIs_follow_user("0");
                                setFocusView(viewHolder, R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                                break;
                            case "1":          //已关注
                                mDatas.get(position).setIs_follow_user("1");
                                setFocusView(viewHolder, R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                                break;
                            case "2":          //互相关注
                                mDatas.get(position).setIs_follow_user("2");
                                setFocusView(viewHolder, R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                                break;
                        }

                        notifyDataSetChanged();
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void setEachFollowing(int mTempPos, String folowing) {
        switch (folowing) {
            case "0":          //未关注
                mDatas.get(mTempPos).setIs_follow_user("0");
                break;
            case "1":          //已关注
                mDatas.get(mTempPos).setIs_follow_user("1");
                break;
            case "2":          //互相关注
                mDatas.get(mTempPos).setIs_follow_user("2");
                break;
        }
        notifyDataSetChanged();
    }

    /**
     * 修改关注样式
     *
     * @param drawable1
     * @param drawable
     * @param color
     * @param text
     */
    private void setFocusView(ViewHolder viewHolder, int drawable1, int drawable, String color, String text) {

        viewHolder.mImgageFans.setBackground(ContextCompat.getDrawable(mContext, drawable1));
        viewHolder.mFans.setBackground(ContextCompat.getDrawable(mContext, drawable));
        viewHolder.mCenterFans.setTextColor(Color.parseColor(color));
        viewHolder.mCenterFans.setText(text);
    }


    public void addData(List<BBsListData550> infos) {
        int size = mDatas.size();
        mDatas.addAll(infos);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }

    public List<BBsListData550> getDatas() {
        return mDatas;
    }

    //跳转到个人中心页面
    private OnItemPersonClickListener onItemPersonClickListener;

    public interface OnItemPersonClickListener {
        void onItemClick(int pos);

        void onItemPersonClick(String id, int pos);
    }

    public void setOnItemPersonClickListener(OnItemPersonClickListener onItemPersonClickListener) {
        this.onItemPersonClickListener = onItemPersonClickListener;
    }

    public List<BBsListData550> getmHotIssues() {
        return mDatas;
    }

}
