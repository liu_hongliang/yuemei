package com.module.home.controller.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.home.model.bean.CardBean;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.List;

public class HomeHorizontlaAdapter extends BaseQuickAdapter<CardBean, BaseViewHolder> {
    private List<CardBean> data;

    public HomeHorizontlaAdapter(int layoutResId, @Nullable List<CardBean> data) {
        super(layoutResId, data);
        this.data = data;
    }

    @Override
    protected void convert(BaseViewHolder helper, CardBean item) {
        RelativeLayout listItem = helper.getView(R.id.home_horizontal_list_item);
        int position = helper.getAdapterPosition();

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) listItem.getLayoutParams();
        if (position == 0) {
            layoutParams.leftMargin = Utils.dip2px(15);
        } else if (position == (data.size() - 1)) {
            layoutParams.rightMargin = Utils.dip2px(15);
        }else{
            layoutParams.leftMargin = Utils.dip2px(3);
            layoutParams.rightMargin = Utils.dip2px(3);
        }
        listItem.setLayoutParams(layoutParams);

        helper.setText(R.id.home_horizontal_title, item.getTitle())
                .setText(R.id.home_horizontal_desc, item.getDesc());

        //设置右下角图片
        String imgUrl = item.getImg();
        Log.e(TAG, "imgUrl == " + imgUrl);
        if (!TextUtils.isEmpty(imgUrl)) {
            Glide.with(mContext).load(imgUrl).into((ImageView) helper.getView(R.id.home_horizontal_img));
        }
        String isCheckin = item.getIsCheckin();
        if (null != isCheckin) {
            if ("0".equals(isCheckin)) {
                helper.setVisible(R.id.home_sign_container, true);
                Glide.with(mContext).load(R.drawable.home_sign).asGif().into((ImageView) helper.getView(R.id.home_sign_img));
            } else {
                helper.setVisible(R.id.home_sign_container, false);
            }
        } else {
            helper.setVisible(R.id.home_sign_container, false);
        }
    }
}
