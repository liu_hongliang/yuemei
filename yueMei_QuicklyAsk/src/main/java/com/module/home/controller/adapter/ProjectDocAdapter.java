package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.DocListDataTaoList;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/2/28
 */
public class ProjectDocAdapter extends RecyclerView.Adapter<ProjectDocAdapter.ViewHolder> {

    private Activity mContext;
    private List<DocListData> mDatas;

    public ProjectDocAdapter(Activity context, List<DocListData> datas) {
        this.mContext = context;
        this.mDatas = datas;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.doc_list_item_view, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        DocListData doctorData = mDatas.get(position);

        Glide.with(mContext)
                .load(doctorData.getImg())
                .transform(new GlideCircleTransform(mContext))
                .placeholder(R.drawable.ic_headpic_img)
                .error(R.drawable.ic_headpic_img)
                .into(viewHolder.docHeadPic);


        viewHolder.docTitlelTV.setText(doctorData.getTitle());

        viewHolder.docNameTV.setText(doctorData.getUsername());
        if (null != doctorData.getHspital_name() && doctorData.getHspital_name().length() > 0) {
            viewHolder.docHosptialTV.setVisibility(View.VISIBLE);
            viewHolder.docHosptialTV.setText(doctorData.getHspital_name());
        } else {
            viewHolder.docHosptialTV.setVisibility(View.GONE);
        }

        //距离
        if (!TextUtils.isEmpty(doctorData.getDistance())) {
            viewHolder.juliTv.setText(doctorData.getDistance());
        }

        if (doctorData.getComment_bili() != null && doctorData.getComment_bili().length() > 0) {
            String startNum = doctorData.getComment_bili();

            if (startNum.length() > 0) {
                int num = Integer.parseInt(startNum);
                viewHolder.ratBar.setMax(100);
                viewHolder.ratBar.setProgress(num);

                if (num > 0) {
                    viewHolder.mscoreTv.setText(doctorData.getComment_score());
                } else {
                    viewHolder.mscoreTv.setText("暂无评价");
                }
            }
        }

        //预订人数
        if (null != doctorData.getSku_order_num() && !doctorData.getSku_order_num().equals("0")) {
            viewHolder.myudingTv.setVisibility(View.VISIBLE);
            viewHolder.myudingTv.setText(doctorData.getSku_order_num() + "人预订");
        } else {
            viewHolder.myudingTv.setVisibility(View.GONE);
        }

        //日记数
        if (!TextUtils.isEmpty(doctorData.getDiaryTotal())) {
            viewHolder.myudingDiary.setText(doctorData.getDiaryTotal() + "日记");
            viewHolder.myudingDiary.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingDiary.setVisibility(View.GONE);
        }

        //中间横线
        if (viewHolder.myudingTv.getVisibility() == View.VISIBLE && viewHolder.myudingDiary.getVisibility() == View.VISIBLE) {
            viewHolder.myudingLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingLine.setVisibility(View.GONE);
        }

        //推荐项目
        if (!TextUtils.isEmpty(doctorData.getTagname())) {
            viewHolder.myudingContainer.setVisibility(View.VISIBLE);
            viewHolder.myudingProject.setText(doctorData.getTagname());
        } else {
            viewHolder.myudingContainer.setVisibility(View.GONE);
        }

        //推荐项目预定人数
        String totalAppoint = doctorData.getTotalAppoint();
        if (!TextUtils.isEmpty(totalAppoint) && !"0".equals(totalAppoint)) {
            viewHolder.myudingReservation.setVisibility(View.VISIBLE);
            viewHolder.myudingReservation.setText(totalAppoint + "次预定");
        } else {
            viewHolder.myudingReservation.setVisibility(View.GONE);
        }

        //推荐项目日记数
        String diaryTotal = doctorData.getTagDiaryTotal();
        if (!TextUtils.isEmpty(diaryTotal) && !"0".equals(diaryTotal)) {
            viewHolder.myudingProjectDiary.setVisibility(View.VISIBLE);
            viewHolder.myudingProjectDiary.setText(diaryTotal + "篇日记");
        } else {
            viewHolder.myudingProjectDiary.setVisibility(View.GONE);
        }

        //中间线显示隐藏
        if (viewHolder.myudingReservation.getVisibility() == View.VISIBLE && viewHolder.myudingProjectDiary.getVisibility() == View.VISIBLE) {
            viewHolder.myudingProjectLine.setVisibility(View.VISIBLE);
        } else {
            viewHolder.myudingProjectLine.setVisibility(View.GONE);
        }

        if (null != doctorData.getTalent() && !doctorData.getTalent().equals("0")) {
            viewHolder.mYuemeiIv.setVisibility(View.VISIBLE);

            if (doctorData.getTalent().equals("1")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("2")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("3")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("4")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.talent_list);
            } else if (doctorData.getTalent().equals("5")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            } else if (doctorData.getTalent().equals("6")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.teyao);
            } else if (doctorData.getTalent().equals("7")) {
                viewHolder.mYuemeiIv.setBackgroundResource(R.drawable.renzheng_list);
            }

        } else {
            viewHolder.mYuemeiIv.setVisibility(View.GONE);
        }

        List<DocListDataTaoList> tao = doctorData.getTao();
        if (tao != null && tao.size() > 0) {
            if (tao.size() == 1) {
                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.GONE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                String member_price = doctorData.getTao().get(0).getMember_price();
                int i = Integer.parseInt(member_price);
                if (i >= 0) {
                    viewHolder.docPlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setVisibility(View.GONE);
                    viewHolder.docPlusPrice.setText("¥" + member_price);
                } else {
                    viewHolder.docPlusVibility.setVisibility(View.GONE);
                    viewHolder.docTao1PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setText("¥" + doctorData.getTao().get(0).getPrice_discount());
                }
                String iscu = doctorData.getTao().get(0).getDacu66_id();
                if (iscu.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao);
                }

            } else {
                tao.size();

                viewHolder.docTao1Content.setVisibility(View.VISIBLE);
                viewHolder.docTao2Content.setVisibility(View.VISIBLE);

                viewHolder.docTao1NameTv.setText(doctorData.getTao().get(0).getTitle());
                viewHolder.docTao2NameTv.setText(doctorData.getTao().get(1).getTitle());

                String member_price = doctorData.getTao().get(0).getMember_price();
                String member_price2 = doctorData.getTao().get(1).getMember_price();
                int i = Integer.parseInt(member_price);
                int j = Integer.parseInt(member_price2);
                if (i >= 0) {
                    viewHolder.docPlusVibility.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setVisibility(View.GONE);
                    viewHolder.docPlusPrice.setText("¥" + member_price);
                } else {
                    viewHolder.docPlusVibility.setVisibility(View.GONE);
                    viewHolder.docTao1PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao1PriceTv.setText("¥" + doctorData.getTao().get(0).getPrice_discount());
                }
                if (j >= 0) {
                    viewHolder.docPlusVibility2.setVisibility(View.VISIBLE);
                    viewHolder.docTao2PriceTv.setVisibility(View.GONE);
                    viewHolder.docPlusPrice2.setText("¥" + member_price2);
                } else {
                    viewHolder.docPlusVibility2.setVisibility(View.GONE);
                    viewHolder.docTao2PriceTv.setVisibility(View.VISIBLE);
                    viewHolder.docTao2PriceTv.setText("¥" + doctorData.getTao().get(1).getPrice_discount());
                }
                String iscu1 = doctorData.getTao().get(0).getDacu66_id();
                if (iscu1.equals("1")) {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao1Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }

                String iscu2 = doctorData.getTao().get(1).getDacu66_id();
                if (iscu2.equals("1")) {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.cu_tips_2x);
                } else {
                    viewHolder.docTao2Iv.setBackgroundResource(R.drawable.doc_list_tao_shopping);
                }
            }

        } else {
            viewHolder.docTao1Content.setVisibility(View.GONE);
            viewHolder.docTao2Content.setVisibility(View.GONE);
        }

//        服务设置
        if (!TextUtils.isEmpty(doctorData.getLook())) {
            viewHolder.docPlusService.setText(doctorData.getLook());
            viewHolder.docPlusService.setVisibility(View.VISIBLE);
        } else {
            viewHolder.docPlusService.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView docHeadPic;
        ImageView docYueIv;
        ImageView docPeiIv;
        TextView docNameTV;
        TextView docHosptialTV;
        TextView juliTv;
        TextView docTitlelTV;
        RatingBar ratBar;
        TextView mscoreTv;
        TextView myudingTv;
        ImageView mYuemeiIv;
        View myudingLine;
        TextView myudingDiary;
        LinearLayout myudingContainer;
        TextView myudingProject;
        View myudingProjectLine;
        TextView myudingReservation;
        TextView myudingProjectDiary;

        TextView docTao1NameTv;
        TextView docTao2NameTv;
        ImageView docTao1Iv;
        ImageView docTao2Iv;
        TextView docTao1PriceTv;
        TextView docTao2PriceTv;
        LinearLayout docTao1Content;
        LinearLayout docTao2Content;
        LinearLayout docPlusVibility;
        LinearLayout docPlusVibility2;
        TextView docPlusPrice;
        TextView docPlusPrice2;
        TextView docPlusService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            docHeadPic = itemView.findViewById(R.id.doc_list_head_image_iv);
            docYueIv = itemView.findViewById(R.id.doc_list_item_cooperation_iv);
            docPeiIv = itemView.findViewById(R.id.doc_list_item_pei_iv);
            docNameTV = itemView.findViewById(R.id.doc_list_item_name_tv);
            docTitlelTV = itemView.findViewById(R.id.doc_list_item_title);
            docHosptialTV = itemView.findViewById(R.id.doc_list_item_hspital_name);
            juliTv = itemView.findViewById(R.id.doc_list_item_juli_tv);


            docTao1NameTv = itemView.findViewById(R.id.doc_list_tao1_name_tv);
            docTao2NameTv = itemView.findViewById(R.id.doc_list_tao2_name_tv);
            docTao1PriceTv = itemView.findViewById(R.id.doc_list_tao1_jg_tv);
            docTao2PriceTv = itemView.findViewById(R.id.doc_list_tao2_jg_tv);

            docTao1Content = itemView.findViewById(R.id.doc_list_tao1_ly);
            docTao2Content = itemView.findViewById(R.id.doc_list_tao2_ly);

            docPlusVibility = itemView.findViewById(R.id.doc_list_plus_vibiliyt);
            docPlusVibility2 = itemView.findViewById(R.id.doc_list_plus_vibiliyt2);
            docPlusPrice = itemView.findViewById(R.id.doc_plus_price);
            docPlusPrice2 = itemView.findViewById(R.id.doc_plus_price2);
            docPlusService = itemView.findViewById(R.id.doc_list_more_tao_tv);

            docTao1Iv = itemView.findViewById(R.id.doc_list_tao_iv1);
            docTao2Iv = itemView.findViewById(R.id.doc_list_tao_iv2);

            mYuemeiIv = itemView.findViewById(R.id.doc_yuemei_renzheng_iv);

            ratBar = itemView.findViewById(R.id.room_ratingbar);
            mscoreTv = itemView.findViewById(R.id.comment_score_list_tv);
            myudingTv = itemView.findViewById(R.id.comment_num_hos_list_tv);
            myudingLine = itemView.findViewById(R.id.comment_num_doc_line);
            myudingDiary = itemView.findViewById(R.id.comment_num_doc_diary);
            myudingContainer = itemView.findViewById(R.id.doc_list_item_project_container);
            myudingProject = itemView.findViewById(R.id.doc_list_item_project);
            myudingReservation = itemView.findViewById(R.id.doc_list_item_project_reservation);
            myudingProjectLine = itemView.findViewById(R.id.doc_list_item_project_line);
            myudingProjectDiary = itemView.findViewById(R.id.doc_list_item_project_diary);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    /**
     * 刷新数据
     *
     * @param
     */
    public void refreshData(List<DocListData> infos) {
        mDatas = infos;
        notifyDataSetChanged();
    }


    public void addData(List<DocListData> infos) {
        int size = mDatas.size();
        mDatas.addAll(infos);
        notifyItemRangeInserted(size, mDatas.size() - size);
    }

    public List<DocListData> getDatas() {
        return mDatas;
    }

    public interface OnEventClickListener {
        void onItemClick(View v, int pos);   //点击回调
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
