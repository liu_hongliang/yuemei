package com.module.home.controller.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.CategoryAdapter;
import com.module.commonview.utils.DialogUtils;
import com.module.commonview.view.CommonTopBar;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.home.fragment.ChannelBottomFragment;
import com.module.home.model.api.ChannelCategoryApi;
import com.module.home.model.bean.CategoryBean;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.activity.ShoppingCartActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

public class ChannelFourActivity extends YMBaseActivity {

    @BindView(R.id.four_channel_top)
    CommonTopBar mTop;
    public String id;
    public ChannelBottomFragment channelBottomFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_channel_four;
    }

    @Override
    protected void initView() {
        id = getIntent().getStringExtra("id");
        String title = getIntent().getStringExtra("title");
        String homeSource = getIntent().getStringExtra("home_source");
        mTop.setCenterText(title);

        //要传入的值
        ProjectDetailsBean detailsBean = new ProjectDetailsBean();
        detailsBean.setTwoLabelId("");
        detailsBean.setFourLabelId(id);
        detailsBean.setHomeSource(homeSource);

        channelBottomFragment = ChannelBottomFragment.newInstance(detailsBean);
        setActivityFragment(R.id.four_channel_fragment, channelBottomFragment);


        View view = View.inflate(mContext, R.layout.channel_shopping_cart_view, null);
        RelativeLayout cartView = view.findViewById(R.id.channel_shopping_cart_view);
        TextView num = view.findViewById(R.id.channel_shopping_cart_num);
        setCartNum(num);
        mTop.setCustomContainer(view, Utils.dip2px(30),Utils.dip2px(35));
        //跳转购物车页面
        cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("id", id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SHOPPING_CART_CLICK),hashMap,new ActivityTypeData("96"));
                startActivity(new Intent(mContext, ShoppingCartActivity.class));
            }
        });

        //品类页推荐弹窗
        categoryDialog();
    }

    private void categoryDialog() {
        ChannelCategoryApi channelCategoryApi = new ChannelCategoryApi();
        Map<String, Object> hashMap = new HashMap();
        hashMap.put("board_id", id);
        channelCategoryApi.getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    final CategoryBean categoryBean = JSONUtil.TransformSingleBean(serverData.data, CategoryBean.class);
                    //显示弹窗
                    if (categoryBean != null && categoryBean.getIs_show_alert().equals("1")) {
                        DialogUtils.showCategoryDialog(mContext, categoryBean, new DialogUtils.CallBack3() {
                            @Override
                            public void onItemClick(int pos, CategoryAdapter categoryAdapter) {
                                Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                                it1.putExtra("id", categoryBean.getTao_list().get(categoryAdapter.getmCount()).get(pos).getId());
                                it1.putExtra("source", "0");
                                it1.putExtra("objid", "0");
                                mContext.startActivity(it1);
                            }

                            @Override
                            public void onDismiss() {

                            }

                            @Override
                            public void onSee(CategoryAdapter categoryAdapter, TextView tv_see) {
                                if (categoryAdapter.getmCount() == 2) {
                                    categoryAdapter.updata(categoryBean.getTao_list().get(0), 0);
                                } else {
                                    categoryAdapter.updata(categoryBean.getTao_list().get(categoryAdapter.getmCount() + 1), categoryAdapter.getmCount() + 1);
                                }
                                if(categoryAdapter.getmCount() == 2){
                                    tv_see.setText("重新查看");
                                }else{
                                    tv_see.setText("换一批");
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    @Override
    protected void initData() {

    }


    /**
     * 设置购物车数量
     */
    public void setCartNum(TextView num) {
        //购物车数量
        String cartNumber = Cfg.loadStr(mContext, FinalConstant.CART_NUMBER, "0");
        if (!TextUtils.isEmpty(cartNumber) && !"0".equals(cartNumber)) {
            num.setVisibility(View.VISIBLE);
            num.setText(cartNumber);
        } else {
            num.setVisibility(View.GONE);
        }
    }
}
