package com.module.home.controller.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.module.bean.ChatListBean;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    List<ChatListBean.ListBean> mChatList;
    Context mContext;
    private LayoutInflater mInflater;
    public MessageAdapter(List<ChatListBean.ListBean> mChatList, Context mContext) {
        this.mChatList = mChatList;
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = mInflater.inflate(R.layout.item_huanxin_kefu, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ChatListBean.ListBean listBean = mChatList.get(i);
        String id = listBean.getId();           //聊天对象Id
        String message = listBean.getMessage(); //内容
        String timeSet = listBean.getTimeSet(); //时间显示
        String noread = listBean.getNoread();   //未读消息数

        String dateTime = listBean.getDateTime();   //加载下一页要传的
        String fromName = listBean.getFromName();   // 聊天对象是医院用的
        String hos_id = listBean.getHos_id();       //医院客服id
        String hos_name = listBean.getHos_name();   // 聊天对象是用户用的
        String group_id = listBean.getGroup_id();   //客服组id
        String userImg = listBean.getUserImg(); //用户头像
        String label_color = listBean.getLabel_color();
        String label_icon = listBean.getLabel_icon();
        String label_title = listBean.getLabel_title();
        //头像
        Glide.with(mContext).load(userImg).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.ease_default_avatar).error(R.drawable.ease_default_avatar).into(viewHolder.headImg);
        //设置未读消息数
        if (!TextUtils.isEmpty(noread)){
            if (Integer.parseInt(noread) > 0) {
                viewHolder.messageNum.setVisibility(View.VISIBLE);
                viewHolder.messageNum.setText(noread);
            } else {
                viewHolder.messageNum.setVisibility(View.GONE);
            }
        }else {
            viewHolder.messageNum.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(label_icon)){
            viewHolder.messageIcon.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(label_icon).into(viewHolder.messageIcon);
        }else {
            viewHolder.messageIcon.setVisibility(View.GONE);
        }
        String groupId = Cfg.loadStr(mContext, FinalConstant.GROUP_ID, "");
        String userMore = Cfg.loadStr(mContext, FinalConstant.USER_MORE, "");
        if (!"1".equals(groupId) || "1".equals(userMore)) { //医院
            viewHolder.messageTitle.setText(fromName);
        }else {
            viewHolder.messageTitle.setText(hos_name);
        }


        if (!TextUtils.isEmpty(label_color)){
            viewHolder.messageContent.setTextColor(Color.parseColor(label_color));
        }else {
            viewHolder.messageContent.setTextColor(ContextCompat.getColor(mContext,R.color._99));
        }
        //消息内容
        if (!TextUtils.isEmpty(label_title)){
            viewHolder.messageContent.setText(label_title);
        }else {
            viewHolder.messageContent.setText(message);
        }


        //消息时间
        viewHolder.messageTime.setText(timeSet);

    }

    @Override
    public int getItemCount() {
        return mChatList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private  ImageView headImg;
        private  TextView messageNum;
        private  TextView messageTitle;
        private  TextView messageTime;
        private  TextView messageContent;
        private  ImageView messageIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            headImg = itemView.findViewById(R.id.item_inform_iv);
            messageNum = itemView.findViewById(R.id.item_inform_tv_);
            messageTitle = itemView.findViewById(R.id.kefu_title);
            messageTime = itemView.findViewById(R.id.kefu_time);
            messageContent = itemView.findViewById(R.id.kefu_lastmessage);
            messageIcon = itemView.findViewById(R.id.kefu_icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemCallBackListener.onItemClick(v, getLayoutPosition());
                }
            });
        }
    }



    /**
     * 设置最新数据
     *
     * @param pos
     * @param message
     */
    public void setNewContent(int pos, String message) {
        try {
            mChatList.get(pos).setMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置最新数据时间
     *
     * @param pos
     * @param time
     */
    public void setNewTime(int pos, String time) {
        mChatList.get(pos).setTimeSet(time);
    }

    /**
     * 设置最新的消息数
     *
     * @param pos
     * @param noread
     */
    public void setNoread(int pos, String noread) {
        if (CollectionUtils.isNotEmpty(mChatList)){
            mChatList.get(pos).setNoread(noread);
        }
    }

    public void clearNoread(){
        if (CollectionUtils.isNotEmpty(mChatList)){
            for (int i = 0; i <mChatList.size() ; i++) {
                mChatList.get(i).setNoread("0");
            }
        }
    }


    public List<ChatListBean.ListBean> getmChatList() {
        return mChatList;
    }

    public void setmChatList(List<ChatListBean.ListBean> mChatList) {
        this.mChatList.addAll(mChatList);
        notifyDataSetChanged();
    }
    public void setmChatList2(List<ChatListBean.ListBean> mChatList) {
        this.mChatList=mChatList;
        notifyDataSetChanged();
    }


    private ItemCallBackListener itemCallBackListener;

    public interface ItemCallBackListener {
        void onItemClick(View v, int pos);
    }

    public void setOnItemCallBackListener(ItemCallBackListener itemCallBackListener) {
        this.itemCallBackListener = itemCallBackListener;
    }
}
