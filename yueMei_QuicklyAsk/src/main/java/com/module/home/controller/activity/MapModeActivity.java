package com.module.home.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ZoomControls;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.TextureMapView;
import com.baidu.mapapi.model.LatLng;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.adapter.ProjectRightListAdapter;
import com.module.commonview.module.api.LoadTwoTreeListApi;
import com.module.commonview.module.bean.TaoScreenTitleData;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseProjectPopupwindows;
import com.module.commonview.view.SortScreenPopwin;
import com.module.community.controller.adapter.TaoListMapModeAdapter;
import com.module.community.model.bean.SearchAboutData;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.api.FilterDataApi;
import com.module.doctor.model.api.SearchApi;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HosListData;
import com.module.home.controller.adapter.MapModeViewPagerAdapter;
import com.module.home.controller.adapter.ProjectDocMapModeAdapter;
import com.module.home.controller.adapter.ProjectHosMapModeAdapter;
import com.module.home.controller.adapter.SearchKeywordsAdapter;
import com.module.home.model.bean.MapModeData;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.home.model.bean.SearchTao;
import com.module.home.view.BottomSheetLayout;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.other.module.bean.MakeTagData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.LocationData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData4;
import com.quicklyask.entity.SearchResultData5;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 文 件 名: MapModeActivity
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-08 00:08
 * 邮   箱: 188897876@qq.com
 * 修改备注：地图模式页面
 */

public class MapModeActivity extends YMBaseActivity implements View.OnClickListener {
    @BindView(R.id.ll_title)
    LinearLayout ll_title;
    @BindView(R.id.ll_sort)
    LinearLayout ll_sort;
    @BindView(R.id.project_part_pop_rly1)//项目
            RelativeLayout project_part_pop_rly1;
    @BindView(R.id.project_part_pop_tv)
    TextView project_part_pop_tv;
    @BindView(R.id.project_part_pop_iv)
    ImageView project_part_pop_iv;

    @BindView(R.id.project_diqu_pop_rly)//地区
            RelativeLayout project_diqu_pop_rly;
    @BindView(R.id.project_diqu_pop_tv)
    TextView project_diqu_pop_tv;
    @BindView(R.id.project_diqu_pop_iv)
    ImageView project_diqu_pop_iv;

    @BindView(R.id.project_sort_pop_rly)//智能排序
            RelativeLayout project_sort_pop_rly;
    @BindView(R.id.project_sort_pop_tv)
    TextView project_sort_pop_tv;
    @BindView(R.id.project_sort_pop_iv)
    ImageView project_sort_pop_iv;

    @BindView(R.id.project_kind_pop_rly)//筛选
            RelativeLayout project_kind_pop_rly;
    @BindView(R.id.project_kind_pop_tv)
    TextView project_kind_pop_tv;
    @BindView(R.id.project_kind_pop_iv)
    ImageView project_kind_pop_iv;

    @BindView(R.id.iv_close)
    ImageView iv_close;
    @BindView(R.id.search_title_click)
    LinearLayout search_title_click;
    @BindView(R.id.search_title_edit)
    EditText search_title_edit;
    @BindView(R.id.search_title_colse)
    ImageView search_title_colse;
    @BindView(R.id.mapView)
    TextureMapView mMapView;//为了解决pop里 mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND) 地图黑屏问题
    @BindView(R.id.vp)
    ViewPager vp;//横向列表
    @BindView(R.id.tv_count)
    TextView tvCount;      //加载的数量
    @BindView(R.id.rv_vertical)
    RecyclerView rvVertical;  //竖向列表
    @BindView(R.id.bottom_sheet_layout)
    BottomSheetLayout bottom_sheet_layout;
    @BindView(R.id.refresh_more)
    YMLoadMore refreshMore; //加载更多
    @BindView(R.id.refresh)
    SmartRefreshLayout refresh; //刷新控件
    @BindView(R.id.ll_content)
    LinearLayout llContent;     //内容区域
    @BindView(R.id.search_keywords_recycler)
    RecyclerView searchKeywordsRecycler;  //联系词列表

    //城市选择
    private BaseCityPopwindows cityPop;
    //项目选择
    private BaseProjectPopupwindows projectPop;
    //筛选
    private SortScreenPopwin kindPop;
    //筛选数据
    private ArrayList<ProjcetList> kindStr = new ArrayList<>();
    //项目详情数据
    private List<MakeTagData> mData = new ArrayList<>();

    // 筛选
    private String curPid = "0";
    private String partId = "0";
    private String mCity = "全国";

    private BaiduMap mBaiduMap;
    private LocationClient mLocationClient;
    private SearchApi mSearchApi;
    private SearchKeywordsAdapter mKeyAdater;
    private String type;
    private String sort;
    private String key;
    private int page = 1;
    private BaseNetWorkCallBackApi lodHotIssueDataApi;
    private List<SearchTao> skuList;
    private TaoListMapModeAdapter taoListAdapter;
    private SearchResultTaoData2 mSearchResultTaoData;
    private List<DocListData> docList;
    private SearchResultData4 searchResultData4;
    private ProjectDocMapModeAdapter projectDocAdapter;
    private SearchResultData5 searchResultData5;
    private List<HosListData> hosList;
    private ProjectHosMapModeAdapter projectHosAdapter;
    private TextView tv_name, tv_name2;
    private TextView tv_score, tv_score2;
    private ImageView iv_marker, iv_marker2;
    private View popView, popView2;
    private MapModeViewPagerAdapter mapModeViewPagerAdapter;
    private ArrayList<MapModeData> mapList;
    //记录医院id
    private ArrayList<String> hosIdList;
    //判断是否是手动设置的
    private boolean isSetContent = false;
    //当前选中的Marker
    private Marker curMarker;
    //当前选中的覆盖物下标
    private int markerPos = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_map_mode;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) ll_title.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        type = getIntent().getStringExtra("type");
        key = getIntent().getStringExtra("key");

        if (!TextUtils.isEmpty(type) && type.equals("4")) {
            ll_sort.setVisibility(View.VISIBLE);
        } else {
            ll_sort.setVisibility(View.GONE);
        }

        search_title_edit.setText(key);
        mapList = new ArrayList<>();
        hosIdList = new ArrayList<>();
        setPopData();
        setMapView();
        initListen();
        loadingData();
    }

    private void initListen() {
        setMultiOnClickListener(tvCount, iv_close, search_title_colse, project_part_pop_rly1, project_diqu_pop_rly, project_sort_pop_rly, project_kind_pop_rly);
        refresh.setEnableLoadMore(true);
        refresh.setEnableRefresh(false);
        refresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        bottom_sheet_layout.setProgressListener(new BottomSheetLayout.OnProgressListener() {
            @Override
            public void onProgress(float var1) {

            }
        });


        search_title_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    search_title_colse.setVisibility(View.GONE);
                    searchKeywordsRecycler.setVisibility(View.GONE);
                } else {
                    search_title_colse.setVisibility(View.VISIBLE);
                }
                if (!TextUtils.isEmpty(key)) {
                    if (isSetContent) {
                        isSetContent = false;

                    } else {
                        sendSearchKey(search_title_edit.getText().toString().trim());
                    }
                } else {
                    searchKeywordsRecycler.setVisibility(View.GONE);
                }
            }
        });
        search_title_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“搜索”键*/
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (TextUtils.isEmpty(search_title_edit.getText().toString().trim())) {
                        MyToast.makeTextToast2(mContext, "请输入", 1000).show();
                        return false;
                    }
                    //隐藏软键盘
                    if (mContext instanceof Activity) {
                        Utils.hideSoftKeyboard((Activity) mContext);
                    }
                    search_title_edit.clearFocus();
                    key = search_title_edit.getText().toString().trim();
                    searchKeywordsRecycler.setVisibility(View.GONE);
                    clearProjectPop();
                    refresh();
                    return true;
                }
                return false;
            }
        });

        search_title_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !TextUtils.isEmpty(key)) {
                    //如果存在联想词：显示联想词页面
//                    isSetContent = false;
                    sendSearchKey(search_title_edit.getText().toString().trim());
                }
            }
        });

        //城市点击回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                mCity = city;
                project_diqu_pop_tv.setText(mCity);
                Cfg.saveStr(MapModeActivity.this, FinalConstant.DWCITY, mCity);
//                //改变城市可能没有悦美自营 所有刷新一下
//                loadSXData();
                refresh();
                cityPop.dismiss();
                initpop();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (type.equals("1")) {
                    //医生
                    setCenterPoint(mapModeViewPagerAdapter.getDocList().get(i).getHospital_id());
                    setCurrentMarker(mapModeViewPagerAdapter.getDocList().get(i).getHospital_id());
                } else if (type.equals("5")) {
                    //医院
                    setCenterPoint(mapModeViewPagerAdapter.getHosList().get(i).getHos_id());
                    setCurrentMarker(mapModeViewPagerAdapter.getHosList().get(i).getHos_id());
                } else {
                    //sku
                    setCenterPoint(mapModeViewPagerAdapter.getSkuList().get(i).getTao().getHospital_id());
                    setCurrentMarker(mapModeViewPagerAdapter.getSkuList().get(i).getTao().getHospital_id());
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                for (int i = 0; i < mapList.size(); i++) {
                    if (mapList.get(i).getLatLng().getLat().equals(marker.getPosition().latitude + "")
                            && mapList.get(i).getLatLng().getLon().equals(marker.getPosition().longitude + "")) {
                        markerPos = i;
                        setVpCur(mapList.get(i).getHos_id());
                        setCurrentMarker(mapList.get(i).getHos_id());
                    }
                }
                return true;
            }
        });
    }

    private void clearProjectPop() {
        projectPop.setOneid("0");
        projectPop.setTwoid("0");
        projectPop.setThreeid("0");
        project_part_pop_tv.setText("全部项目");
        loadPart();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_count:
                if (bottom_sheet_layout.isExpended()) {
                    bottom_sheet_layout.collapse();
                } else {
                    bottom_sheet_layout.expand();
                }
                break;
            case R.id.iv_close:
                finish();
                break;
            case R.id.search_title_colse:
                if (mContext instanceof Activity) {
                    Utils.hideSoftKeyboard((Activity) mContext);
                }
                search_title_edit.setText("");
                break;
            case R.id.project_part_pop_rly1:
                if (projectPop.isShowing()) {
                    projectPop.dismiss();
                } else {
                    projectPop.showPop();
                }
                initpop();
                break;
            case R.id.project_diqu_pop_rly:
                if (cityPop.isShowing()) {
                    cityPop.dismiss();
                } else {
                    cityPop.showPop();
                }
                initpop();
                break;
            case R.id.project_kind_pop_rly:
                if (kindPop.isShowing()) {
                    kindPop.dismiss();
                } else {
                    kindPop.showPop();
                }
                initpop();
                break;
            default:
                break;
        }
    }

    private void initpop() {
        if (projectPop != null && projectPop.isShowing()) {
            project_part_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_part_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_part_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_part_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
        if (kindPop != null && kindPop.isShowing()) {
            project_kind_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_kind_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_kind_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_kind_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
        if (cityPop != null && cityPop.isShowing()) {
            project_diqu_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_diqu_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            cityPop.initViewData();
            project_diqu_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_diqu_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
    }

    /**
     * 设置智能排序和筛选所需要的数据
     */
    private void setPopData() {
        loadCity();             //城市
        loadPart();             //全部项目
        loadSXData();           //获取筛选列表数据
    }

    /**
     * 筛选数据请求
     */
    private void loadSXData() {
        FilterDataApi filterDataApi = new FilterDataApi();
        filterDataApi.getHashMap().clear();
        filterDataApi.getHashMap().put("partId", curPid);
        filterDataApi.getCallBack(mContext, filterDataApi.getHashMap(), new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> lvkindData) {
                if (lvkindData != null && lvkindData.size() != 0) {
                    kindPop = new SortScreenPopwin(MapModeActivity.this, project_part_pop_rly1, lvkindData);

                    kindPop.setOnButtonClickListener(new SortScreenPopwin.OnButtonClickListener() {
                        @Override
                        public void onResetListener(View view) {
                            if (kindPop != null) {
                                kindStr.clear();
                                kindPop.resetData();
                            }
                        }

                        @Override
                        public void onSureListener(View view, ArrayList data) {
                            if (kindPop != null) {
                                kindStr = kindPop.getSelectedData();
                                refresh();
                                kindPop.dismiss();
                            }
                        }
                    });

                    kindPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            initpop();
                        }
                    });
                }
            }
        });
    }

    private void loadCity() {
        mCity = Cfg.loadStr(MapModeActivity.this, FinalConstant.DWCITY, "");
        if (mCity.length() > 0) {
            if (mCity.equals("失败")) {
                mCity = "全国";
            } else {
            }
        } else {
            mCity = "全国";
        }
        project_diqu_pop_tv.setText(mCity);
        cityPop = new BaseCityPopwindows(MapModeActivity.this, project_part_pop_rly1, false, "2");
    }

    private void loadPart() {
        loadTwoTreeList();      //获取项目列表数据
    }

    private void loadTwoTreeList() {
        Map<String, Object> maps = new HashMap<>();
        new LoadTwoTreeListApi().getCallBack(mContext, maps, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {
                if (serverData != null && serverData.size() != 0) {
                    mData = serverData;

                    projectPop = new BaseProjectPopupwindows(mContext, project_part_pop_rly1, serverData, "");
                    projectPop.setmServerData(serverData);

                    projectPop.setOneid("");
                    projectPop.setTwoid("");
                    projectPop.setThreeid("");

                    projectPop.setLeftView();
                    projectPop.setRightView(projectPop.getmPos());

                    setProPopTitle();

                    //滚动到相应位置
                    projectPop.getmLeftRecy().scrollToPosition(projectPop.getmPos());
                    ProjectRightListAdapter rightListAdapter = projectPop.getRightListAdapter();
                    int selectedPos = 0;
                    if (rightListAdapter != null) {
                        selectedPos = rightListAdapter.getmSelectedPos();
                    }
                    projectPop.getRightList().setSelection(selectedPos);

                    projectPop.setOnItemSelectedClickListener(new BaseProjectPopupwindows.OnItemSelectedClickListener() {
                        @Override
                        public void onItemSelectedClick(String id, String name) {
                            search_title_edit.setText("");
                            partId = id;
                            key = name;
                            if (!TextUtils.isEmpty(name)) {
                                project_part_pop_tv.setText(name);
                            }
                            refresh();
                        }
                    });

                    //筛选关闭
                    projectPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            initpop();
                        }
                    });
                }
            }
        });
    }

    private void setProPopTitle() {
        //设置所选项目id和和title1
        if (projectPop.getmTwoPos() > 0) {
            if (projectPop.getmThreePos() > 0) {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getList().get(projectPop.getmThreePos()).getName();
                project_part_pop_tv.setText(name);
            } else {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getName();
                project_part_pop_tv.setText(name);
            }

        } else {
            if ("0".equals("0")) {
                project_part_pop_tv.setText("全部项目");
            } else {
                String name = mData.get(projectPop.getmPos()).getName();
                project_part_pop_tv.setText(name);
            }
            partId = "0";
        }
    }

    private void sendSearchKey(final String trim) {
        mSearchApi = new SearchApi();
        Map<String, Object> maps = new HashMap<>();
        maps.put("key", Utils.unicodeEncode(trim));

        mSearchApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                ArrayList<SearchAboutData> searchAboutDatas = JSONUtil.TransformSearchAboutData(serverData.data);

                //是否有联想词
                if (searchAboutDatas != null && searchAboutDatas.size() > 0 && !TextUtils.isEmpty(search_title_edit.getText().toString())) {
                    //如果存在联想词：显示联想词页面
                    searchKeywordsRecycler.setVisibility(View.VISIBLE);

                    if (mKeyAdater == null) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        searchKeywordsRecycler.setLayoutManager(linearLayoutManager);
                        mKeyAdater = new SearchKeywordsAdapter(mContext, searchAboutDatas);
                        searchKeywordsRecycler.setAdapter(mKeyAdater);
                        mKeyAdater.setOnEventClickListener(new SearchKeywordsAdapter.OnEventClickListener() {
                            @Override
                            public void onItemViewClick(View v, String keys, HashMap<String, String> event_params) {
                                //隐藏软键盘
                                Utils.hideSoftKeyboard(mContext);
                                clearProjectPop();
                                isSetContent = true;
                                search_title_edit.setText(keys);
                                search_title_edit.clearFocus();
                                searchKeywordsRecycler.setVisibility(View.GONE);
                                key = keys;
                                refresh();
                            }
                        });
                    } else {
                        mKeyAdater.replaceData(searchAboutDatas);
                    }

                } else {
                    //如果不存在联想词：显示初始化页面
                    searchKeywordsRecycler.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setHorRv() {
        if (type.equals("1")) {
            //医生
            mapModeViewPagerAdapter = new MapModeViewPagerAdapter(getSupportFragmentManager(), 1, null, projectDocAdapter.getDatas(), null, "");
        } else if (type.equals("5")) {
            //医院
            mapModeViewPagerAdapter = new MapModeViewPagerAdapter(getSupportFragmentManager(), 2, null, null, projectHosAdapter.getData(), searchResultData5.getSearch_hit_board_id());
        } else {
            //sku
            mapModeViewPagerAdapter = new MapModeViewPagerAdapter(getSupportFragmentManager(), 0, taoListAdapter.getActiveData(), null, null, "");
        }
        vp.setAdapter(mapModeViewPagerAdapter);
        vp.setPageMargin(30);
        vp.setCurrentItem(0);
    }

    private void setCurrentMarker(String hos_id) {
        iv_marker.setBackgroundResource(R.drawable.hos_big_map);
        iv_marker2.setBackgroundResource(R.drawable.hos_small_map);
        for (int i = 0; i < mapList.size(); i++) {
            tv_name.setText(mapList.get(i).getTitle());
            tv_name2.setText(mapList.get(i).getTitle());
            if (TextUtils.isEmpty(mapList.get(i).getScore())) {
                tv_score.setVisibility(View.GONE);
                tv_score2.setVisibility(View.GONE);
            } else {
                tv_score.setVisibility(View.VISIBLE);
                tv_score.setText(mapList.get(i).getScore() + "分");
                tv_score2.setVisibility(View.VISIBLE);
                tv_score2.setText(mapList.get(i).getScore() + "分");
            }
            BitmapDescriptor descriptor1 = BitmapDescriptorFactory.fromView(popView);
            BitmapDescriptor descriptor2 = BitmapDescriptorFactory.fromView(popView2);
            if (mapList.get(i).getHos_id().equals(hos_id)) {
                markerPos = i;
                mapList.get(i).getMarker().setIcon(descriptor1);
                mapList.get(i).getMarker().setToTop();
            } else {
                mapList.get(i).getMarker().setIcon(descriptor2);
            }
        }
    }

    private void loadingData() {
        lodHotIssueDataApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "index613");
        lodHotIssueDataApi.getHashMap().clear();

        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(key));
        lodHotIssueDataApi.addData("type", type);
        lodHotIssueDataApi.addData("page", page + "");
//        lodHotIssueDataApi.addData("partId", partId);
        //筛选
        for (ProjcetList data : kindStr) {
            lodHotIssueDataApi.addData(data.getPostName(), data.getPostVal());
        }
        //普通模式 0普通  1地图
        lodHotIssueDataApi.addData("search_mode", "1");
        lodHotIssueDataApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData.code.equals("1")) {
                    if (type.equals("1")) {
                        //医生
                        searchResultData4 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData4.class);
                        docList = searchResultData4.getList();
                        setVerDocRecycleview();
                    } else if (type.equals("5")) {
                        //医院
                        searchResultData5 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData5.class);
                        hosList = searchResultData5.getList();
                        setVerHosRecycleview();
                    } else {
                        //sku
                        mSearchResultTaoData = JSONUtil.TransformSingleBean(serverData.data, SearchResultTaoData2.class);
                        skuList = mSearchResultTaoData.getList();
                        //设置竖向sku列表
                        setVerSKURecycleview();
                    }
                    setHorRv();
                    if (mapList != null && mapList.size() != 0) {
                        //当前覆盖物在最上层 不知道为啥 有的还是不对
                        mapList.get(markerPos).getMarker().setToTop();
                    }
                }
            }
        });
    }

    private void setHosMapMarker() {
        for (int i = 0; i < projectHosAdapter.getData().size(); i++) {
            if (projectHosAdapter.getData().get(i) != null) {
                LocationData locationData = projectHosAdapter.getData().get(i).getLocation();
                if (locationData != null
                        && !TextUtils.isEmpty(locationData.getLat())
                        && !TextUtils.isEmpty(locationData.getLon())
                        && !hosIdList.contains(projectHosAdapter.getData().get(i).getHos_id())) {
                    LatLng point = new LatLng(Double.parseDouble(locationData.getLat()), Double.parseDouble(locationData.getLon()));
                    tv_name2.setText(projectHosAdapter.getData().get(i).getHos_name());
                    tv_score2.setText(projectHosAdapter.getData().get(i).getComment_score() + "分");
                    iv_marker2.setBackgroundResource(R.drawable.hos_small_map);
                    BitmapDescriptor descriptor = BitmapDescriptorFactory.fromView(popView2);
                    //创建OverlayOptions属性
                    OverlayOptions option = new MarkerOptions()
                            .position(point)
                            .perspective(true)
                            .animateType(MarkerOptions.MarkerAnimateType.grow)
                            .icon(descriptor);

                    Marker curMarker = (Marker) mBaiduMap.addOverlay(option);

                    MapModeData.LatLng l = new MapModeData.LatLng();
                    l.setLat(locationData.getLat());
                    l.setLon(locationData.getLon());

                    hosIdList.add(projectHosAdapter.getData().get(i).getHos_id());

                    setMapMode(projectHosAdapter.getData().get(i).getHos_id(),
                            projectHosAdapter.getData().get(i).getHos_name(),
                            projectHosAdapter.getData().get(i).getComment_score(),
                            projectHosAdapter.getData().get(i).getImg(),
                            curMarker, l);
                }
            }
        }
        if (page == 1) {
            if (hosList != null && hosList.size() != 0) {
                setCurrentMarker(hosList.get(0).getHos_id());
                setCenterPoint(hosList.get(0).getHos_id());
            }
        }
    }

    private void setDocMapMarker() {
        for (int i = 0; i < projectDocAdapter.getDatas().size(); i++) {
            if (projectDocAdapter.getDatas().get(i) != null) {
                LocationData locationData = projectDocAdapter.getDatas().get(i).getLocation();
                if (locationData != null
                        && !TextUtils.isEmpty(locationData.getLat())
                        && !TextUtils.isEmpty(locationData.getLon())
                        && !hosIdList.contains(projectDocAdapter.getDatas().get(i).getHospital_id())) {
                    LatLng point = new LatLng(Double.parseDouble(locationData.getLat()), Double.parseDouble(locationData.getLon()));
                    tv_name2.setText(projectDocAdapter.getDatas().get(i).getHspital_name());
                    tv_score2.setText(projectDocAdapter.getDatas().get(i).getHospital_effect() + "分");
                    iv_marker2.setBackgroundResource(R.drawable.doc_small_map);
                    BitmapDescriptor descriptor = BitmapDescriptorFactory.fromView(popView2);
                    //创建OverlayOptions属性
                    OverlayOptions option = new MarkerOptions()
                            .position(point)
                            .animateType(MarkerOptions.MarkerAnimateType.grow)
                            .perspective(true)
                            .icon(descriptor);

                    Marker curMarker = (Marker) mBaiduMap.addOverlay(option);

                    MapModeData.LatLng l = new MapModeData.LatLng();
                    l.setLat(locationData.getLat());
                    l.setLon(locationData.getLon());

                    setMapMode(projectDocAdapter.getDatas().get(i).getHospital_id(),
                            projectDocAdapter.getDatas().get(i).getHspital_name(),
                            projectDocAdapter.getDatas().get(i).getHospital_effect(),
                            projectDocAdapter.getDatas().get(i).getImg(),
                            curMarker, l);
                }
            }
        }
        if (page == 1) {
            if (docList != null && docList.size() != 0) {
                setCurrentMarker(docList.get(0).getHospital_id());
                setCenterPoint(docList.get(0).getHospital_id());
            }
        }
    }

    private void setVerHosRecycleview() {
        final String searchHitBoardId = searchResultData5.getSearch_hit_board_id();
        //刷新隐藏
        refresh.finishRefresh();
        if (hosList.size() == 0) {
            refresh.finishLoadMoreWithNoMoreData();
        } else {
            refresh.finishLoadMore();
        }

        if (page == 1 && hosList.size() == 0) {
            refresh.setVisibility(View.GONE);
            MyToast.makeTextToast2(mContext, "没有找到您要的内容", 1000).show();
            vp.setVisibility(View.GONE);
            bottom_sheet_layout.setVisibility(View.GONE);
            if (searchResultData5 != null
                    && searchResultData5.getCity_location() != null
                    && !TextUtils.isEmpty(searchResultData5.getCity_location().getLat())
                    && !TextUtils.isEmpty(searchResultData5.getCity_location().getLon())) {
                setCenterPoint(Double.parseDouble(searchResultData5.getCity_location().getLat()), Double.parseDouble(searchResultData5.getCity_location().getLon()));
            }
            mBaiduMap.clear();
        } else {
            vp.setVisibility(View.VISIBLE);
            bottom_sheet_layout.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.VISIBLE);
        }
        if (projectHosAdapter == null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvVertical.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(10), Utils.getLocalColor(mContext, R.color._f6)));
            rvVertical.setLayoutManager(linearLayoutManager);
            projectHosAdapter = new ProjectHosMapModeAdapter(mContext, hosList);
            rvVertical.setAdapter(projectHosAdapter);
            projectHosAdapter.setOnEventClickListener(new ProjectHosMapModeAdapter.OnEventClickListener() {
                @Override
                public void onItemClick(View v, HosListData data, int pos) {
                    String hosid = data.getHos_id();
                    Intent it = new Intent(mContext, HosDetailActivity.class);
                    it.putExtra("hosid", hosid);
                    if (!TextUtils.isEmpty(searchHitBoardId)) {
                        it.putExtra("search_hit_board_id", searchHitBoardId);
                    }
                    mContext.startActivity(it);
                }
            });
        } else {
            if (page == 1) {
                //刷新数据
                projectHosAdapter.refreshData(hosList);
            } else {
                //加载更多
                projectHosAdapter.addData(hosList);
            }
        }
        if (projectHosAdapter.getData() != null) {
            tvCount.setText("已展示" + projectHosAdapter.getData().size() + "个结果");
        }
        if (projectHosAdapter.getData() != null && projectHosAdapter.getData().size() >= 80) {
            refresh.finishLoadMoreWithNoMoreData();
        }
        setHosMapMarker();
        page++;
    }

    private void setVerDocRecycleview() {
        //刷新隐藏
        refresh.finishRefresh();
        if (docList.size() == 0) {
            refresh.finishLoadMoreWithNoMoreData();
        } else {
            refresh.finishLoadMore();
        }
        if (page == 1 && docList.size() == 0) {
            refresh.setVisibility(View.GONE);
            MyToast.makeTextToast2(mContext, "没有找到您要的内容", 1000).show();
            if (searchResultData4 != null
                    && searchResultData4.getCity_location() != null
                    && !TextUtils.isEmpty(searchResultData4.getCity_location().getLat())
                    && !TextUtils.isEmpty(searchResultData4.getCity_location().getLon())) {
                setCenterPoint(Double.parseDouble(searchResultData4.getCity_location().getLat()), Double.parseDouble(searchResultData4.getCity_location().getLon()));
            }
            vp.setVisibility(View.GONE);
            bottom_sheet_layout.setVisibility(View.GONE);
            mBaiduMap.clear();
        } else {
            vp.setVisibility(View.VISIBLE);
            bottom_sheet_layout.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.VISIBLE);
        }
        if (projectDocAdapter == null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvVertical.setLayoutManager(linearLayoutManager);
            projectDocAdapter = new ProjectDocMapModeAdapter(mContext, docList);
            rvVertical.setAdapter(projectDocAdapter);
            projectDocAdapter.setOnEventClickListener(new ProjectDocMapModeAdapter.OnEventClickListener() {
                @Override
                public void onItemClick(View v, int pos) {
                    DocListData docListData = projectDocAdapter.getDatas().get(pos);
                    String docId = docListData.getUser_id();
                    String docName = docListData.getUsername();
                    Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                    intent.putExtra("docId", docId);
                    intent.putExtra("docName", docName);
                    intent.putExtra("partId", "");
                    mContext.startActivity(intent);
                }
            });
        } else {
            if (page == 1) {
                //刷新数据
                projectDocAdapter.refreshData(docList);
            } else {
                //加载更多
                projectDocAdapter.addData(docList);
            }

        }
        if (projectDocAdapter.getDatas() != null) {
            tvCount.setText("已展示" + projectDocAdapter.getDatas().size() + "个结果");
        }
        if (projectDocAdapter.getDatas() != null && projectDocAdapter.getDatas().size() >= 80) {
            refresh.finishLoadMoreWithNoMoreData();
        }
        setDocMapMarker();
        page++;
    }

    private void setSkuMapMarker() {
        for (int i = 0; i < taoListAdapter.getActiveData().size(); i++) {
            if (taoListAdapter.getActiveData().get(i) != null && taoListAdapter.getActiveData().get(i).getTao() != null) {
                LocationData locationData = taoListAdapter.getActiveData().get(i).getTao().getLocation();
                if (locationData != null
                        && !TextUtils.isEmpty(locationData.getLat())
                        && !TextUtils.isEmpty(locationData.getLon())
                        && !hosIdList.contains(taoListAdapter.getActiveData().get(i).getTao().getHospital_id())) {
                    LatLng point = new LatLng(Double.parseDouble(locationData.getLat()), Double.parseDouble(locationData.getLon()));
                    tv_name2.setText(taoListAdapter.getActiveData().get(i).getTao().getHos_name());
                    tv_score2.setText(taoListAdapter.getActiveData().get(i).getTao().getHos_effect() + "分");
                    iv_marker2.setBackgroundResource(R.drawable.sku_small_map);
                    BitmapDescriptor descriptor = BitmapDescriptorFactory.fromView(popView2);
                    //创建OverlayOptions属性
                    OverlayOptions option = new MarkerOptions()
                            .position(point)
                            .animateType(MarkerOptions.MarkerAnimateType.grow)
                            .perspective(true)
                            .icon(descriptor);
                    Marker curMarker = (Marker) mBaiduMap.addOverlay(option);

                    MapModeData.LatLng l = new MapModeData.LatLng();
                    l.setLat(locationData.getLat());
                    l.setLon(locationData.getLon());

                    hosIdList.add(taoListAdapter.getActiveData().get(i).getTao().getHospital_id());
                    setMapMode(taoListAdapter.getActiveData().get(i).getTao().getHospital_id(),
                            taoListAdapter.getActiveData().get(i).getTao().getHos_name(),
                            taoListAdapter.getActiveData().get(i).getTao().getHos_effect(),
                            taoListAdapter.getActiveData().get(i).getTao().getImg(),
                            curMarker, l);
                }
            }
        }
        if (page == 1) {
            if (skuList != null && skuList.size() != 0) {
                setCurrentMarker(skuList.get(0).getTao().getHospital_id());
                setCenterPoint(skuList.get(0).getTao().getHospital_id());
            }
        }
    }

    private void setMapMode(String hos_id, String title, String score, String imgUrl, Marker marker, MapModeData.LatLng latLng) {
        MapModeData mapModeData = new MapModeData();
        mapModeData.setHos_id(hos_id);
        mapModeData.setTitle(title);
        mapModeData.setScore(score);
        mapModeData.setImgUrl(imgUrl);
        mapModeData.setMarker(marker);
        mapModeData.setLatLng(latLng);
        mapList.add(mapModeData);
    }

    private void setVerSKURecycleview() {
        //刷新隐藏
        refresh.finishRefresh();
        if (skuList.size() == 0) {
            refresh.finishLoadMoreWithNoMoreData();
        } else {
            refresh.finishLoadMore();
        }
        if (page == 1 && skuList.size() == 0) {
            refresh.setVisibility(View.GONE);
            MyToast.makeTextToast2(mContext, "没有找到您要的内容", 1000).show();
            vp.setVisibility(View.GONE);
            bottom_sheet_layout.setVisibility(View.GONE);
            if (mSearchResultTaoData != null
                    && mSearchResultTaoData.getCity_location() != null
                    && !TextUtils.isEmpty(mSearchResultTaoData.getCity_location().getLat())
                    && !TextUtils.isEmpty(mSearchResultTaoData.getCity_location().getLon())) {
                setCenterPoint(Double.parseDouble(mSearchResultTaoData.getCity_location().getLat()), Double.parseDouble(mSearchResultTaoData.getCity_location().getLon()));
            }
            mBaiduMap.clear();
        } else {
            vp.setVisibility(View.VISIBLE);
            bottom_sheet_layout.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.VISIBLE);
        }
        if (taoListAdapter == null) {
            //淘整形列表设置
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rvVertical.setLayoutManager(linearLayoutManager);
            DefaultItemAnimator itemAnimator = (DefaultItemAnimator) rvVertical.getItemAnimator();
            if (itemAnimator != null) {
                itemAnimator.setSupportsChangeAnimations(false);
            }
            taoListAdapter = new TaoListMapModeAdapter(mContext, mSearchResultTaoData.getList());
            rvVertical.setAdapter(taoListAdapter);
            taoListAdapter.setOnItemClickListener(new TaoListMapModeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(HomeTaoData taoList, int pos) {
                    Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                    it1.putExtra("id", taoList.get_id());
                    it1.putExtra("source", "2");
                    it1.putExtra("objid", "0");
                    startActivity(it1);
                }
            });
        } else {
            if (page == 1) {
                //刷新数据
                taoListAdapter.refreshData(mSearchResultTaoData.getList());
            } else {
                //加载更多
                taoListAdapter.addData(mSearchResultTaoData.getList());
            }
        }
        tvCount.setText("已展示" + taoListAdapter.getActiveData().size() + "个结果");
        if (taoListAdapter.getActiveData() != null && taoListAdapter.getActiveData().size() >= 80) {
            refresh.finishLoadMoreWithNoMoreData();
        }
        //设置覆盖物
        setSkuMapMarker();
        page++;
    }

    private void setMapView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        popView = inflater.inflate(R.layout.item_pop_map_mode1, null);
        tv_name = popView.findViewById(R.id.tv_name);
        tv_score = popView.findViewById(R.id.tv_score);
        iv_marker = popView.findViewById(R.id.iv_marker);

        LayoutInflater inflater2 = LayoutInflater.from(mContext);
        popView2 = inflater2.inflate(R.layout.item_pop_map_mode2, null);
        tv_name2 = popView2.findViewById(R.id.tv_name);
        tv_score2 = popView2.findViewById(R.id.tv_score);
        iv_marker2 = popView2.findViewById(R.id.iv_marker);

        mBaiduMap = mMapView.getMap();
        //地图缩放按钮
        mMapView.showZoomControls(false);
        //移除logo
        View child = mMapView.getChildAt(1);
        if (child != null && (child instanceof ImageView || child instanceof ZoomControls)) {
            child.setVisibility(View.INVISIBLE);
        }
        //开启定位图层
        mBaiduMap.setMyLocationEnabled(true);
        //普通地图
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        //打开室内图，默认为关闭状态
        mBaiduMap.setIndoorEnable(true);
        //关闭指南针
        mBaiduMap.getUiSettings().setCompassEnabled(false);
        //关闭比例尺
        mMapView.showScaleControl(false);
        initLocation();
    }


    //按医院id
    private void setVpCur(String hosid) {
        if (type.equals("1")) {
            //医生
            for (int i = 0; i < mapModeViewPagerAdapter.getDocList().size(); i++) {
                if (mapModeViewPagerAdapter.getDocList().get(i).getHospital_id().equals(hosid)) {
                    vp.setCurrentItem(i);
                    break;
                }
            }
        } else if (type.equals("5")) {
            //医院
            for (int i = 0; i < mapModeViewPagerAdapter.getHosList().size(); i++) {
                if (mapModeViewPagerAdapter.getHosList().get(i).getHos_id().equals(hosid)) {
                    vp.setCurrentItem(i);
                    break;
                }
            }
        } else {
            //sku
            for (int i = 0; i < mapModeViewPagerAdapter.getSkuList().size(); i++) {
                if (mapModeViewPagerAdapter.getSkuList().get(i).getTao().getHospital_id().equals(hosid)) {
                    vp.setCurrentItem(i);
                    break;
                }
            }
        }
    }

    private void initLocation() {
        //定位初始化
        mLocationClient = new LocationClient(this);
        //通过LocationClientOption设置LocationClient相关参数
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true); // 打开gps
        option.setCoorType("bd09ll"); // 设置坐标类型
        option.setScanSpan(1000);
        //设置locationClientOption
        mLocationClient.setLocOption(option);
        //注册LocationListener监听器
        MyLocationListener myLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(myLocationListener);
        //开启地图定位图层
        mLocationClient.start();
    }


    @Override
    protected void initData() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        removeAllMarker();
        mBaiduMap.clear();
        mLocationClient.stop();
        mBaiduMap.setMyLocationEnabled(false);
        mMapView.onDestroy();
        mMapView = null;

        if (projectPop != null) {
            projectPop.dismiss();
        }
        if (cityPop != null) {
            cityPop.dismiss();
        }
        if (kindPop != null) {
            kindPop.dismiss();
        }
    }

    public void refresh() {
        taoListAdapter = null;
        projectDocAdapter = null;
        projectHosAdapter = null;
        markerPos = 0;
        hosIdList.clear();
        mapList.clear();
        mBaiduMap.clear();
        page = 1;
        loadingData();
    }

    public static void invoke(Context context, String type, String sort, String key, TaoScreenTitleData screenTitleData) {
        Intent intent = new Intent(context, MapModeActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("sort", sort);
        intent.putExtra("key", key);
        intent.putExtra("screenTitleData", screenTitleData);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    public class MyLocationListener extends BDAbstractLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location) {
            //mapView 销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }
            MyLocationData locData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    // 此处设置开发者获取到的方向信息，顺时针0-360
                    .direction(location.getDirection()).latitude(location.getLatitude())
                    .longitude(location.getLongitude()).build();
            mBaiduMap.setMyLocationData(locData);
        }
    }

    public void setCenterPoint(double a, double b) {
        LatLng centerPoint = new LatLng(a, b);
        //缩放级别 3-21
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.zoom(14.0f);
        builder.target(centerPoint);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    public void setCenterPoint(String hosId) {
        for (int i = 0; i < mapList.size(); i++) {
            if (hosId.equals(mapList.get(i).getHos_id())) {
                LatLng centerPoint = new LatLng(Double.parseDouble(mapList.get(i).getLatLng().getLat())
                        , Double.parseDouble(mapList.get(i).getLatLng().getLon()));
                //缩放级别 3-21
                MapStatus.Builder builder = new MapStatus.Builder();
                builder.zoom(14.0f);
                builder.target(centerPoint);
                mBaiduMap.setMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
            }
        }
    }

    public void removeAllMarker() {
        for (int i = 0; i < mapList.size(); i++) {
            mapList.get(i).getMarker().remove();
            mapList.get(i).getMarker().getIcon().recycle();
        }
    }
}
