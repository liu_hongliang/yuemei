package com.module.home.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.module.api.FocusAndCancelApi;
import com.module.commonview.module.api.ZanOrJuBaoApi;
import com.module.commonview.module.bean.FocusAndCancelData;
import com.module.community.model.bean.BBsListData550;
import com.module.community.model.bean.VideoBean;
import com.module.home.model.bean.TaoBean;
import com.module.home.model.bean.TuijHosDoc;
import com.module.home.model.bean.TuijOther;
import com.module.home.model.bean.Tuijshare;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiVideoView;

import org.kymjs.aframe.ui.ViewInject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 裴成浩 on 2019/3/15
 */
public class HomeFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "HomeFragmentAdapter";
    private Activity mContext;
    private List<Tuijshare> mDatas = new ArrayList<>();

    private final int ITEM_TYPE_ONE = 1;
    private final int ITEM_TYPE_TOW = 2;
    private final int ITEM_TYPE_THREE = 3;
    private LayoutInflater mInflater;
    private int windowsWight;
    private PostViewHolder postViewHolder;
    private ZanOrJuBaoApi mZanOrJuBaoApi;

    public HomeFragmentAdapter(Activity context, List<BBsListData550> datas) {
        this(context, null, datas);
    }

    public HomeFragmentAdapter(Activity context, List<Tuijshare> tuiDatas, List<BBsListData550> bbsDatas) {
        this.mContext = context;
        if (tuiDatas != null) {
            this.mDatas = tuiDatas;
        } else {
            for (BBsListData550 data : bbsDatas) {
                mDatas.add(new Tuijshare(data));
            }
        }
        mInflater = LayoutInflater.from(context);
        windowsWight = Cfg.loadInt(mContext, FinalConstant.WINDOWS_W, 0);
        mZanOrJuBaoApi = new ZanOrJuBaoApi();
    }

    @Override
    public int getItemViewType(int position) {
        Log.e(TAG, "mDatas == " + mDatas);

        Tuijshare tuijshare = mDatas.get(position);
        BBsListData550 post = tuijshare.getPost();
        TuijHosDoc hosDoc = tuijshare.getHos_doc();
        TuijOther other = tuijshare.getOther();

        if (post != null) {
            return ITEM_TYPE_ONE;
        } else if (hosDoc != null) {
            return ITEM_TYPE_TOW;
        } else if (other != null) {
            return ITEM_TYPE_THREE;
        } else {
            return ITEM_TYPE_ONE;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_ONE:
                return new PostViewHolder(mInflater.inflate(R.layout.item_home_diaryother, parent, false));
            case ITEM_TYPE_TOW:
                return new HosDocViewHolder(mInflater.inflate(R.layout.item_home_diary_hosdoc, parent, false));
            case ITEM_TYPE_THREE:
                return new OtherViewHolder(mInflater.inflate(R.layout.item_home_diary_other, parent, false));
            default:
                return new PostViewHolder(mInflater.inflate(R.layout.item_home_diaryother, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof PostViewHolder) {
            postViewHolder = (PostViewHolder) viewHolder;
            setTypePost1(postViewHolder, mDatas.get(position).getPost(), position);
        } else if (viewHolder instanceof HosDocViewHolder) {
            setTypeHosDoc((HosDocViewHolder) viewHolder, mDatas.get(position).getHos_doc(), position);
        } else if (viewHolder instanceof OtherViewHolder) {
            setTypeOther((OtherViewHolder) viewHolder, mDatas.get(position).getOther());
        }
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (mDatas != null) {
            size = mDatas.size();
        }
        return size;
    }

    /**
     * 设置Post类型的数据
     *
     * @param postViewHolder
     * @param postData       :第一页第一种类型的数据
     */
    private void setTypePost1(final PostViewHolder postViewHolder, final BBsListData550 postData, final int position) {

        Glide.with(mContext).load(postData.getUser_img()).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(postViewHolder.mBBsHeadPic);

        postViewHolder.mBBsName.setText(postData.getUser_name());
        postViewHolder.mBBsTime.setText(postData.getTime()+"   共更新"+postData.getShareNum()+"篇");
        postViewHolder.mBBsTitle.setText(postData.getTitle());
        if (null != postData.getTalent() && !"0".equals(postData.getTalent())) {
            postViewHolder.mBBsTalent.setVisibility(View.VISIBLE);
            if ("10".equals(postData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = postViewHolder.mBBsTalent.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                postViewHolder.mBBsTalent.setLayoutParams(layoutParams);
                postViewHolder.mBBsTalent.setBackgroundResource(R.drawable.yuemei_officia_listl);
            } else if ("11".equals(postData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = postViewHolder.mBBsTalent.getLayoutParams();
                layoutParams.width = Utils.dip2px(15);
                layoutParams.height = Utils.dip2px(15);
                postViewHolder.mBBsTalent.setLayoutParams(layoutParams);
                postViewHolder.mBBsTalent.setBackgroundResource(R.drawable.talent_list);
            } else if ("12".equals(postData.getTalent()) || "13".equals(postData.getTalent())) {
                ViewGroup.LayoutParams layoutParams = postViewHolder.mBBsTalent.getLayoutParams();
                layoutParams.width = Utils.dip2px(46);
                layoutParams.height = Utils.dip2px(16);
                postViewHolder.mBBsTalent.setLayoutParams(layoutParams);
                postViewHolder.mBBsTalent.setBackgroundResource(R.drawable.renzheng_list);
            }
        } else {
            postViewHolder.mBBsTalent.setVisibility(View.GONE);
        }

        String picRule = postData.getPicRule();
        Log.e(TAG, "picRule == " + picRule);
        if (picRule.equals("1")) {
            postViewHolder.mBBsPicRuleIv1.setVisibility(View.VISIBLE);
            postViewHolder.mBBsPicRuleIv2.setVisibility(View.VISIBLE);
            if ("0".equals(postData.getAfter_day())) {
                postViewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mContext, 13), 0, Utils.dip2px(mContext, 12), 0);
                postViewHolder.mBBsPicRuleIv2.setText("After");
            } else {
                postViewHolder.mBBsPicRuleIv2.setPadding(Utils.dip2px(mContext, 8), 0, Utils.dip2px(mContext, 7), 0);
                postViewHolder.mBBsPicRuleIv2.setText("After" + postData.getAfter_day() + "天");
            }

        } else {
            postViewHolder.mBBsPicRuleIv1.setVisibility(View.GONE);
            postViewHolder.mBBsPicRuleIv2.setVisibility(View.GONE);
        }

        if (postData.getTao() != null) {
            //关联SKU，
            TaoBean taoBean = postData.getTao();
            if (!"0".equals(postData.getTao().getId())) {

                String totalAppoint = taoBean.getTotalAppoint();
                String member_price = taoBean.getMember_price();
                int i = Integer.parseInt(member_price);
                if (!"0".equals(totalAppoint)){
                    //且SKU有预订数
                    postViewHolder.mSkuClick.setVisibility(View.VISIBLE);
                    postViewHolder.mSkuClick2.setVisibility(View.GONE);
                    postViewHolder.mSkuName.setText(taoBean.getTitle());
                    postViewHolder.mSkuPrice.setText(taoBean.getPrice());
                    postViewHolder.mSkuOrding.setText(totalAppoint+"人已预订");
                    if (i >= 0){
                        postViewHolder.mSkuPlus1Visorgone.setVisibility(View.VISIBLE);
                        postViewHolder.mSkuPlus1Price.setText(member_price);
                    }else {
                        postViewHolder.mSkuPlus1Visorgone.setVisibility(View.GONE);
                    }
                    postViewHolder.mSkuClick.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isFastDoubleClick()) {
                                return;
                            }

                            if (onEventClickListener != null) {
                                onEventClickListener.onSkuItemClick(position, postData.getTao().getId());
                            }
                        }
                    });

                }else {
                    postViewHolder.mSkuClick.setVisibility(View.GONE);
                    postViewHolder.mSkuClick2.setVisibility(View.VISIBLE);
                    postViewHolder.mSkuName2.setText(taoBean.getTitle());
                    postViewHolder.mSkuPrice2.setText(taoBean.getPrice());
                    if (i >= 0){
                        postViewHolder.mSkuPlus2Visorgone.setVisibility(View.VISIBLE);
                        postViewHolder.mSkuPlus2Price.setText(member_price);
                    }else {
                        postViewHolder.mSkuPlus2Visorgone.setVisibility(View.GONE);
                    }
                    postViewHolder.mSkuClick2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onEventClickListener != null) {
                                onEventClickListener.onSkuItemClick(position, postData.getTao().getId());
                            }
                        }
                    });
                }

            } else {
                postViewHolder.mSkuClick.setVisibility(View.GONE);
                postViewHolder.mSkuClick2.setVisibility(View.GONE);
                //         标签
                if (null != postData.getTag()) {
                    if (postData.getTag().size() > 0) {
                        postViewHolder.tagLy.setVisibility(View.VISIBLE);
                        int tagsize = postData.getTag().size();
                        StringBuilder stringBuilder = new StringBuilder();
                        for (int i = 0; i < tagsize; i++) {
                            stringBuilder.append("#" + postData.getTag().get(i).getName());
                        }
                        postViewHolder.mTag.setText(stringBuilder);
                    } else {
                        postViewHolder.tagLy.setVisibility(View.GONE);
                    }
                } else {
                    postViewHolder.tagLy.setVisibility(View.GONE);
                }
            }
        }

        // 图片
        if (null != postData.getPic()) {

            if (postData.getPic().size() > 0) {
                if (postData.getPic().size() == 1) {//单张图片的时候

                    postViewHolder.mDuozhangLy.setVisibility(View.GONE);
                    postViewHolder.mSingalLy.setVisibility(View.VISIBLE);
                    postViewHolder.tagLy.setVisibility(View.GONE);
                    postViewHolder.mSkuClick.setVisibility(View.GONE);
                    postViewHolder.mSkuClick2.setVisibility(View.GONE);

//                    ViewGroup.LayoutParams params = postViewHolder.mSingalLy.getLayoutParams();
//                    params.height = ((windowsWight - 40) * 340 / 710);
//                    postViewHolder.mSingalLy.setLayoutParams(params);

                    Log.e(TAG, "图片url == " + postData.getPic().get(0).getImg());
                    postViewHolder.mSingalLy.removeAllViews();
                    //视频播放按钮
                    if ("1".equals(postData.getIs_video())) {
                        final YueMeiVideoView yueMeiVideoView = new YueMeiVideoView(mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                        yueMeiVideoView.setLayoutParams(layoutParams);
                        VideoBean video = postData.getVideo();
                        String url="https://player.yuemei.com/video/6ea2/201906/5bb2ac1e7d68adf4766d63788ca4924c.mp4";
                        String imgUrl="https://p11.yuemei.com/bms/img/20190729/190729143945_15833d.jpg";
                        yueMeiVideoView.setVideoParameter(imgUrl,url);
//                        if (Utils.getAPNType(mContext) == 1){
//                            postViewHolder.mYmVideoView.videoStartPlayer();
//                        }
                        postViewHolder.mSingalLy.addView(yueMeiVideoView);
                    } else {
                        ImageView imageView = new ImageView(mContext);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
                        imageView.setLayoutParams(layoutParams);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        postViewHolder.mSingalLy.addView(imageView);
                        Glide.with(mContext).load(postData.getPic().get(0).getImg())
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(imageView);
                    }



                } else if (postData.getPic().size() > 1) {//两张对比图

                    postViewHolder.mDuozhangLy.setVisibility(View.VISIBLE);
                    postViewHolder.mSingalLy.setVisibility(View.GONE);

                    ViewGroup.LayoutParams params = postViewHolder.mDuozhangLy.getLayoutParams();
                    params.height = (windowsWight - Utils.dip2px(31)) / 2;
                    postViewHolder.mDuozhangLy.setLayoutParams(params);


                    //视频播放按钮
                    if ("1".equals(postData.getIs_video())) {
                        postViewHolder.mIdentification1.setVisibility(View.VISIBLE);
                    } else {
                        postViewHolder.mIdentification1.setVisibility(View.GONE);
                    }

                    try {
                        String img1 = postData.getPic().get(0).getImg();
                        String img2 = postData.getPic().get(1).getImg();

                        Glide.with(mContext).load(img1)
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(postViewHolder.mBBsIv1);

                        Glide.with(mContext)
                                .load(img2)
                                .transform(new GlideRoundTransform(mContext, Utils.dip2px(3)))
                                .placeholder(R.drawable.home_other_placeholder)
                                .error(R.drawable.home_other_placeholder)
                                .into(postViewHolder.mBBsIv2);

                    } catch (OutOfMemoryError e) {
                        ViewInject.toast("内存不足");
                    }
                }
            } else {
                postViewHolder.mDuozhangLy.setVisibility(View.GONE);
                postViewHolder.mSingalLy.setVisibility(View.GONE);
            }
        }

        final String isFollowUser = postData.getIs_follow_user();
        Log.e(TAG, isFollowUser);
        switch (isFollowUser) {
            case "0":          //未关注
                setFocusView(R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                break;
            case "1":          //已关注
                setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                break;
            case "2":          //互相关注
                setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                break;
        }

        postViewHolder.mFans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLogin()) {
                    if (Utils.isBind()) {
                        switch (isFollowUser) {
                            case "0":          //未关注
                                FocusAndCancel(postData.getUser_id(), isFollowUser, position);
                                break;
                            case "1":          //已关注
                            case "2":          //互相关注
                                showDialogExitEdit(postData.getUser_id(), isFollowUser, position);
                                break;
                        }
                    } else {
                        Utils.jumpBindingPhone(mContext);
                    }
                } else {
                    Utils.jumpLogin(mContext);
                }
            }
        });

    }

    private void showDialogExitEdit(final String id, final String mFolowing, final int position) {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dialog_edit_exit2);
        editDialog.setCanceledOnTouchOutside(false);
        editDialog.show();
        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("确定取消关注？");
        titleTv88.setHeight(50);
        Button cancelBt88 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt88.setText("确认");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                FocusAndCancel(id, mFolowing, position);
                editDialog.dismiss();
            }
        });

        Button trueBt1188 = editDialog.findViewById(R.id.confirm_btn1_edit);
        trueBt1188.setText("取消");
        trueBt1188.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    /**
     * 关注和取消关注
     */
    private void FocusAndCancel(String id, final String mFolowing, final int position) {
        Log.e(TAG, "id === " + id);
        Log.e(TAG, "mFolowing === " + mFolowing);
        Log.e(TAG, "position === " + position);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new FocusAndCancelApi().getCallBack(mContext, hashMap, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    try {
                        FocusAndCancelData focusAndCancelData = JSONUtil.TransformSingleBean(serverData.data, FocusAndCancelData.class);

                        switch (focusAndCancelData.getIs_following()) {
                            case "0":          //未关注
                                mDatas.get(position).getPost().setIs_follow_user("0");
                                setFocusView(R.drawable.focus_plus_sign1, R.drawable.shape_bian_tuoyuan3_ff5c77, "#ff5c77", "关注");
                                break;
                            case "1":          //已关注
                                mDatas.get(position).getPost().setIs_follow_user("1");
                                setFocusView(R.drawable.focus_plus_sign_yes, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "已关注");
                                break;
                            case "2":          //互相关注
                                mDatas.get(position).getPost().setIs_follow_user("2");
                                setFocusView(R.drawable.each_focus, R.drawable.shape_tuiyuan3_cdcdcd, "#999999", "互相关注");
                                break;
                        }

                        notifyDataSetChanged();
                        Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 修改关注样式
     *
     * @param drawable1
     * @param drawable
     * @param color
     * @param text
     */
    private void setFocusView(int drawable1, int drawable, String color, String text) {

        postViewHolder.mImgageFans.setBackground(ContextCompat.getDrawable(mContext, drawable1));
        postViewHolder.mFans.setBackground(ContextCompat.getDrawable(mContext, drawable));
        postViewHolder.mCenterFans.setTextColor(Color.parseColor(color));
        postViewHolder.mCenterFans.setText(text);

//        ViewGroup.LayoutParams layoutParams1 = postViewHolder.mFans.getLayoutParams();
//        switch (text.length()) {
//            case 2:
//                layoutParams1.width = Utils.dip2px(mContext, 68);
//                break;
//            case 3:
//                layoutParams1.width = Utils.dip2px(mContext, 78);
//                break;
//            case 4:
//                layoutParams1.width = Utils.dip2px(mContext, 90);
//                break;
//        }
//
//        postViewHolder.mFans.setLayoutParams(layoutParams1);
    }

    /**
     * 设置HosDoc类型的数据
     *
     * @param hosDocViewHolder
     * @param hosDoc           :第二种类型的数据
     * @param position
     */
    private void setTypeHosDoc(HosDocViewHolder hosDocViewHolder, TuijHosDoc hosDoc, final int position) {
        String imgUrl = hosDoc.getImg();
        String name = hosDoc.getName();
        String button = hosDoc.getButton();
        String rate = hosDoc.getRate();
        String url = hosDoc.getUrl();
        String yuding = hosDoc.getYuding();
        List<HomeTaoData> taoData = hosDoc.getTao();

        Glide.with(mContext).load(imgUrl).transform(new GlideCircleTransform(mContext)).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(hosDocViewHolder.diaryHosdocImg);


        hosDocViewHolder.diaryHosdocTitle.setText(name);
        hosDocViewHolder.diaryHosdocExamine.setText(button);
        hosDocViewHolder.diaryHosdocReservation.setText(yuding);
        hosDocViewHolder.diaryHosdocPraise.setText(rate);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        hosDocViewHolder.diaryHosdocList.setLayoutManager(layoutManager);

        HomeHosDocAdapter homeHosDocAdapter = new HomeHosDocAdapter(mContext, taoData);
        hosDocViewHolder.diaryHosdocList.setAdapter(homeHosDocAdapter);
        //adapter item点击事件
        homeHosDocAdapter.setOnItemClickListener(new HomeHosDocAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (onEventClickListener != null) {
                    onEventClickListener.onHosDocItemClick(view, position, pos);
                }
            }
        });

        //查看更多点击事件
        homeHosDocAdapter.setOnItemClickMoreListener(new HomeHosDocAdapter.OnItemClickMoreListener() {
            @Override
            public void onItemClickMore(int pos) {
                Log.e(TAG, "pos == " + pos);
                if (onEventClickListener != null) {
                    onEventClickListener.onItemMoreClick(position, pos);
                }
            }
        });
    }

    /**
     * 设置Other类型的数据
     *
     * @param otherViewHolder
     * @param other:第三种类型数据
     */

    private void setTypeOther(OtherViewHolder otherViewHolder, TuijOther other) {
        String title = other.getTitle();
        String desc = other.getDesc();
        otherViewHolder.otherTitle.setText(title);

        if (desc != null && desc.length() > 0) {
            otherViewHolder.otherSubtitle.setVisibility(View.VISIBLE);
            otherViewHolder.otherSubtitle.setText(other.getDesc());
        } else {
            otherViewHolder.otherSubtitle.setVisibility(View.GONE);
        }

        Glide.with(mContext).load(other.getImg()).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder2).error(R.drawable.home_other_placeholder2).into(otherViewHolder.otherImg);

    }

    class PostViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mBBsHeadHeadpic;
        ImageView mBBsHeadPic;

        TextView mBBsName;
        TextView mBBsTime;
        TextView mBBsTitle;
        ImageView mBBsTalent;

        LinearLayout mFans;
        ImageView mImgageFans;
        TextView mCenterFans;

        LinearLayout mSingalLy;

        RelativeLayout mDuozhangLy;
        ImageView mBBsIv1;
        ImageView mBBsIv2;
        TextView mBBsPicRuleIv1;
        TextView mBBsPicRuleIv2;

        TextView mTag;
        LinearLayout tagLy;
        RelativeLayout mSkuClick;
        TextView mSkuName;
        TextView mSkuPrice;
        TextView mSkuOrding;
        RelativeLayout mSkuClick2;
        TextView mSkuName2;
        TextView mSkuPrice2;
        LinearLayout mSkuPlus1Visorgone;
        LinearLayout mSkuPlus2Visorgone;
        TextView mSkuPlus1Price;
        TextView mSkuPlus2Price;
        ImageView mIdentification1;
//        ImageView mIdentification2;

        View mFanxianLine;
        LinearLayout mFanxianContent;
        TextView mFanxian;

        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            mBBsHeadHeadpic = itemView.findViewById(R.id.diary_list_headpic_ly);
            mBBsHeadPic = itemView.findViewById(R.id.bbs_list_head_image_iv);
            mBBsName = itemView.findViewById(R.id.bbs_list_name_tv);
            mBBsTime = itemView.findViewById(R.id.bbs_list_time_tv);
            mBBsTitle = itemView.findViewById(R.id.bbs_list_title_tv);
            mBBsTalent = itemView.findViewById(R.id.bbs_list_talent_iv);
            mFans = itemView.findViewById(R.id.diary_fans);
            mImgageFans = itemView.findViewById(R.id.diary_imgage_fans);
            mCenterFans = itemView.findViewById(R.id.diary_center_fans);

            mDuozhangLy = itemView.findViewById(R.id.bbs_list_duozhang_ly);
            mBBsIv1 = itemView.findViewById(R.id.bbs_list_duozhang_iv1);
            mBBsIv2 = itemView.findViewById(R.id.bbs_list_duozhang_iv2);


            mSingalLy = itemView.findViewById(R.id.bbs_list_pic_danzhang_ly);

            mBBsPicRuleIv1 = itemView.findViewById(R.id.bbs_list_picrule1);
            mBBsPicRuleIv2 = itemView.findViewById(R.id.bbs_list_picrule2);


            mTag = itemView.findViewById(R.id.bbs_list_tag_tv);
            tagLy = itemView.findViewById(R.id.tag_ly_550);
            mSkuClick = itemView.findViewById(R.id.bbs_item_sku_click);
            mSkuName = itemView.findViewById(R.id.bbs_list_sku_name);
            mSkuPrice = itemView.findViewById(R.id.bbs_list_sku_price);
            mSkuOrding = itemView.findViewById(R.id.bbs_list_sku_ording);
            mSkuClick2 = itemView.findViewById(R.id.bbs_item_sku_click2);
            mSkuName2 = itemView.findViewById(R.id.bbs_list_sku_name2);
            mSkuPrice2 = itemView.findViewById(R.id.bbs_list_sku_price2);
            mSkuPlus1Visorgone = itemView.findViewById(R.id.sku_plus1_visorgone);
            mSkuPlus2Visorgone = itemView.findViewById(R.id.sku_plus2_visorgone);
            mSkuPlus1Price = itemView.findViewById(R.id.sku_plus_price);
            mSkuPlus2Price = itemView.findViewById(R.id.sku_plus_price2);
//            mDesc = itemView.findViewById(R.id.bbs_list_desc_tv);
//            descLy = itemView.findViewById(R.id.tagdesc_ly_550);

            mIdentification1 = itemView.findViewById(R.id.iv_video_identification1);
//            mIdentification2 = itemView.findViewById(R.id.iv_video_identification2);
            mFanxianLine = itemView.findViewById(R.id.diary_fanxian_line);
            mFanxianContent = itemView.findViewById(R.id.diary_fanxian_content);
            mFanxian = itemView.findViewById(R.id.diary_fanxian);

            //item点击
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onDiaryItemClick(v, getLayoutPosition());
                    }
                }
            });

            //跳转到个人中心页
            mBBsHeadHeadpic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onItemPersonClick(mDatas.get(getLayoutPosition()).getPost().getUser_id(), getLayoutPosition());
                    }
                }
            });
        }
    }

    class HosDocViewHolder extends RecyclerView.ViewHolder {
        LinearLayout diaryHosdocMore;
        ImageView diaryHosdocImg;
        TextView diaryHosdocTitle;
        TextView diaryHosdocReservation;
        TextView diaryHosdocPraise;
        TextView diaryHosdocExamine;
        RecyclerView diaryHosdocList;

        public HosDocViewHolder(@NonNull View itemView) {
            super(itemView);
            diaryHosdocMore = itemView.findViewById(R.id.diary_hosdoc_more);
            diaryHosdocImg = itemView.findViewById(R.id.diary_hosdoc_img);
            diaryHosdocTitle = itemView.findViewById(R.id.diary_hosdoc_title);
            diaryHosdocReservation = itemView.findViewById(R.id.diary_hosdoc_reservation);
            diaryHosdocPraise = itemView.findViewById(R.id.diary_hosdoc_praise);
            diaryHosdocExamine = itemView.findViewById(R.id.diary_hosdoc_examine);
            diaryHosdocList = itemView.findViewById(R.id.diary_hosdoc_list);

            //医生或医院的点击事件
            diaryHosdocMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onHosDocClick(view, getLayoutPosition());
                    }
                }
            });
        }
    }

    class OtherViewHolder extends RecyclerView.ViewHolder {
        TextView otherTitle;
        TextView otherSubtitle;
        ImageView otherImg;

        public OtherViewHolder(@NonNull View itemView) {
            super(itemView);
            otherTitle = itemView.findViewById(R.id.diary_other_title);
            otherSubtitle = itemView.findViewById(R.id.diary_other_subtitle);
            otherImg = itemView.findViewById(R.id.diary_other_img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onOtherItemClick(v, getLayoutPosition());
                    }
                }
            });
        }
    }

    /**
     * 添加数据
     *
     * @param datas
     */
    public void addData(ArrayList<BBsListData550> datas) {
        for (BBsListData550 data : datas) {
            mDatas.add(new Tuijshare(data));
        }

        notifyItemRangeChanged(mDatas.size() - datas.size(), datas.size());
    }

    public List<Tuijshare> getData() {
        return mDatas;
    }

    public interface OnEventClickListener {
        //查看日记事件回调
        void onDiaryItemClick(View view, int pos);

        //查看sku事件回调
        void onSkuItemClick(int position, String tao_id);

        //查看医生或医院点击事件回调
        void onHosDocClick(View view, int pos);

        //RecyclerView item点击事件回调
        void onHosDocItemClick(View view, int pos, int itemPos);

        //查看更多的点击事件
        void onItemMoreClick(int pos, int itemPos);

        //跳转到个人中心页面
        void onItemPersonClick(String id, int pos);

        //第三种类型时间点击
        void onOtherItemClick(View view, int pos);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

    public void setEachFollowing(int mTempPos, String folowing) {
        switch (folowing) {
            case "0":          //未关注
                mDatas.get(mTempPos).getPost().setIs_follow_user("0");
                break;
            case "1":          //已关注
                mDatas.get(mTempPos).getPost().setIs_follow_user("1");
                break;
            case "2":          //互相关注
                mDatas.get(mTempPos).getPost().setIs_follow_user("2");
                break;
        }
        notifyItemChanged(mTempPos);
    }
}
