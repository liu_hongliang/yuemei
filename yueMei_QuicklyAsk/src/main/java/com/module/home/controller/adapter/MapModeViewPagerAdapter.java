package com.module.home.controller.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.model.bean.HosListData;
import com.module.home.fragment.MapModeFragment1;
import com.module.home.fragment.MapModeFragment2;
import com.module.home.fragment.MapModeFragment3;
import com.module.home.model.bean.SearchResultTaoData2;
import com.module.home.model.bean.SearchTao;
import com.quicklyask.entity.SearchResultData4;
import com.quicklyask.entity.SearchResultData5;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: MapModeViewPagerAdapter
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-12 10:30
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class MapModeViewPagerAdapter extends FragmentPagerAdapter {
    private int mType;
    private List<SearchTao> skuList;
    private List<DocListData> docList;
    private List<HosListData> hosList;
    private String hit_board_id;

    private SearchResultTaoData2 mSearchResultTaoData;
    private SearchResultData4 mSearchResultData4;
    private SearchResultData5 mSearchResultData5;

    public MapModeViewPagerAdapter(FragmentManager fm, int type, List<SearchTao> mSkuList, List<DocListData> mDocList, List<HosListData> mHosList,String mHit_board_id) {
        super(fm);
        this.mType = type;
        this.skuList = mSkuList;
        this.docList = mDocList;
        this.hosList = mHosList;
        this.hit_board_id = mHit_board_id;
    }

    public MapModeViewPagerAdapter(FragmentManager fm, int type, SearchResultTaoData2 searchResultTaoData, SearchResultData4 searchResultData4, SearchResultData5 searchResultData5) {
        super(fm);
        this.mType = type;
        this.mSearchResultTaoData = searchResultTaoData;
        this.mSearchResultData4 = searchResultData4;
        this.mSearchResultData5 = searchResultData5;
    }


    @Override
    public int getCount() {
        switch (mType) {
            case 0:
                //sku
                return skuList.size();
            case 1:
                //医生
                return docList.size();
            case 2:
                //医院
                return hosList.size();
        }
        return -1;
    }

    @Override
    public Fragment getItem(int position) {
        switch (mType) {
            case 0:
                //sku
                return MapModeFragment1.newInstance(skuList.get(position).getTao());
            case 1:
                //医生
                return MapModeFragment2.newInstance(docList.get(position));
            case 2:
                //医院
                return MapModeFragment3.newInstance(hosList.get(position),hit_board_id);
            default:
                break;
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        switch (mType) {
            case 0:
                //sku
                return skuList.get(position).hashCode();
            case 1:
                //医生
                return docList.get(position).hashCode();
            case 2:
                //医院
                return hosList.get(position).hashCode();
        }
        return 0;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public List<SearchTao> getSkuList() {
        return skuList;
    }

    public List<DocListData> getDocList() {
        return docList;
    }

    public List<HosListData> getHosList() {
        return hosList;
    }
}