package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;
import org.xutils.common.util.DensityUtil;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 文 件 名: MapModeFragment1
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-12 10:34
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class MapModeFragment1 extends YMBaseFragment {

    @BindView(R.id.tao_list_image)
    ImageView taoListImage;
    @BindView(R.id.tao_list_image_video_tag)
    ImageView taoListImageVideoTag;
    @BindView(R.id.tao_list_title)
    TextView taoListTitle;
    @BindView(R.id.tao_list_price_num)
    TextView taoListPriceNum;
    @BindView(R.id.tao_list_docname)
    TextView taoListDocname;
    @BindView(R.id.tao_list_hosname)
    TextView taoListHosname;
    @BindView(R.id.tao_list_price_click)
    LinearLayout taoListPriceClick;
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.tv_manjian)
    TextView tv_manjian;
    @BindView(R.id.tv_miaosha)
    TextView tv_miaosha;
    Unbinder unbinder;
    private HomeTaoData mTao;

    public static MapModeFragment1 newInstance(HomeTaoData tao) {
        MapModeFragment1 fragment = new MapModeFragment1();
        Bundle bundle = new Bundle();
        bundle.putParcelable("datas", tao);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item1_map_mode;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mTao = getArguments().getParcelable("datas");
        }
        if (mTao != null) {
            Glide.with(mContext).load(mTao.getImg()).transform(new GlideRoundTransform(mContext, DensityUtil.dip2px(3))).into(taoListImage);
            if (!mTao.getIs_have_video().equals("1")) {
                taoListImageVideoTag.setVisibility(View.GONE);
            } else {
                taoListImageVideoTag.setVisibility(View.VISIBLE);
            }
            taoListTitle.setText(mTao.getSubtitle());
            taoListPriceNum.setText(mTao.getPrice_discount());
            taoListDocname.setText(mTao.getDoc_name());
            taoListHosname.setText(mTao.getHos_name());
            if (TextUtils.isEmpty(mTao.getBilateral_coupons())) {
                tv_manjian.setVisibility(View.GONE);
            } else {
                tv_manjian.setText(mTao.getBilateral_coupons());
                tv_manjian.setVisibility(View.VISIBLE);
            }


            if (!TextUtils.isEmpty(mTao.getSale_type())) {
                if (mTao.getSale_type().equals("2")) {
                    tv_miaosha.setText("秒杀");
                    tv_miaosha.setVisibility(View.VISIBLE);
                } else if (mTao.getSale_type().equals("4")) {
                    tv_miaosha.setText("拼团");
                    tv_miaosha.setVisibility(View.VISIBLE);
                }else{
                    tv_miaosha.setVisibility(View.GONE);
                }
            } else {
                tv_miaosha.setVisibility(View.GONE);
            }

            //设置标签
//            List<Promotion> promotion = skuList.get(mPos).getTao().getPromotion();
//            if (CollectionUtils.isNotEmpty(promotion)) {
//                taoListFlowLayout.setVisibility(View.VISIBLE);
//                setTagView(taoListFlowLayout, promotion);
//            } else {
//                taoListFlowLayout.setVisibility(View.INVISIBLE);
//            }
            ll_root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                    it1.putExtra("id", mTao.get_id());
                    it1.putExtra("source", "2");
                    it1.putExtra("objid", "0");
                    getArguments().clear();
                    startActivity(it1);
                }
            });
        }
    }

    private void setTagView(FlowLayout mFlowLayout, List<Promotion> lists) {
        if (mFlowLayout.getChildCount() != lists.size()) {
            mFlowLayout.removeAllViews();
            mFlowLayout.setMaxLine(1);
            for (int i = 0; i < lists.size(); i++) {
                Promotion promotion = (Promotion) lists.get(i);
                String styleType = promotion.getStyle_type();//1正常  2最近浏览
                TextView textView = new TextView(mContext);
                textView.setText(promotion.getTitle());
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                if ("2".equals(styleType)) {
                    textView.setBackgroundResource(R.drawable.sku_list_nearlook);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.ff7c4f));
                } else {
                    textView.setBackgroundResource(R.drawable.shopping_cart_sku_tab_background);
                    textView.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
                }
                mFlowLayout.addView(textView);


            }
        }
    }

    @Override
    protected void initData(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
