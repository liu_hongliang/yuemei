package com.module.home.fragment;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.ScrollStaggeredGridLayoutManager;
import com.module.community.controller.adapter.CommunityStagFragmentAdapter;
import com.module.community.model.bean.CommunityStaggeredListData;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.model.bean.SearchResultBBsData;
import com.module.my.controller.activity.TypeProblemActivity;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;

/**
 * 搜索帖子页面
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsBBsFragment extends YMBaseFragment {

    @BindView(R.id.search_results_bbs_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_bbs_recycler)
    LoadMoreRecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView bbsNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;
    private String TAG = "SearchResultsBBsFragment";
    private String mKey;
    private String mType;
    private int mPage = 1;
    private CommunityStagFragmentAdapter mProjectDiaryAdapter;
    private Gson mGson;
    private boolean isNoMore = true;
    private ScrollStaggeredGridLayoutManager scrollLinearLayoutManager;

    public static SearchResultsBBsFragment newInstance(String key,String type) {
        SearchResultsBBsFragment fragment = new SearchResultsBBsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_bbs_view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mType = getArguments().getString("type");

        View mHeadView = mInflater.inflate(R.layout.search_results_top_tip, null);
        LinearLayout mTopTip = mHeadView.findViewById(R.id.search_results_top_tip);
        TextView mTopTitle = mHeadView.findViewById(R.id.search_results_top_title);
        mTopTitle.setText("关于“" + mKey + "”");
        mRecycler.addHeaderView(mHeadView);

        //头部点击
        mTopTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isLoginAndBind(mContext)) {
                    mFunctionManager.goToActivity(TypeProblemActivity.class);
                }
            }
        });

        //下拉刷新
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mProjectDiaryAdapter = null;
                mPage = 1;
                Log.e(TAG, "1111");
                loadingData();
            }
        });

        //上拉加载更多
        mRecycler.setLoadMoreListener(new LoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (isNoMore) {
                    loadingData();
                }
            }
        });

        mRecycler.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);

                //判断左右列，方法2（推荐）
                StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());

                int viewLayoutPosition = params.getViewLayoutPosition();

                Log.e(TAG, "params.getSpanIndex()== " + params.getSpanIndex());
                Log.e(TAG, "viewLayoutPosition== " + viewLayoutPosition);
                if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                    //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                    if (viewLayoutPosition != 0) {
                        if (params.getSpanIndex() % 2 == 0) {
                            //左列
                            outRect.right = Utils.dip2px((int)2.5);
                        } else {
                            //右列
                            outRect.left = Utils.dip2px((int)2.5);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        mGson = new Gson();

        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));             //筛选词
        lodHotIssueDataApi.addData("type", mType);            //搜索数据类型
        lodHotIssueDataApi.addData("page", mPage + "");     //页码
        Log.e(TAG, "mKey == " + mKey);
        if (onEventClickListener != null) {
//                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
            onEventClickListener.onSearchParaClick(mGson.toJson(lodHotIssueDataApi.getHashMap()));
        }
        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultBBsData searchResultBBsData = JSONUtil.TransformSingleBean(serverData.data, SearchResultBBsData.class);
                        List<CommunityStaggeredListData> lvBBslistData = searchResultBBsData.getList();

                        //刷新隐藏
                        mRefresh.finishRefresh();

                        if (lvBBslistData.size() < 20) {
                            isNoMore = false;
                            mRecycler.setNoMore(true);
                        } else {
                            mRecycler.loadMoreComplete();
                        }

                        if (mPage == 1 && lvBBslistData.size() == 0) {
                            bbsNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            bbsNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);

                            if (mProjectDiaryAdapter == null) {

                                scrollLinearLayoutManager = new ScrollStaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
                                scrollLinearLayoutManager.setScrollEnable(true);

                                scrollLinearLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                                mRecycler.setLayoutManager(scrollLinearLayoutManager);

                                mProjectDiaryAdapter = new CommunityStagFragmentAdapter(mContext, lvBBslistData);
                                mRecycler.setAdapter(mProjectDiaryAdapter);

                                ((DefaultItemAnimator) mRecycler.getItemAnimator()).setSupportsChangeAnimations(false);

                                //回到顶部
                                mRecycler.setBackTopListener(new LoadMoreRecyclerView.BackTopListener() {
                                    @Override
                                    public void onBackTop() {

                                        int[] firstVisibleItem = scrollLinearLayoutManager.findFirstVisibleItemPositions(null);
                                        if (firstVisibleItem[1] == 2) {
                                            if (mProjectDiaryAdapter != null) {
                                                scrollLinearLayoutManager.invalidateSpanAssignments();
                                            }
                                        }
                                    }
                                });

                                mProjectDiaryAdapter.setOnItemCallBackListener(new CommunityStagFragmentAdapter.ItemCallBackListener() {
                                    @Override
                                    public void onItemClick(View v, int pos) {
                                        if (pos != mProjectDiaryAdapter.getItemCount() && pos - 1 >= 0) {
                                            CommunityStaggeredListData listData = mProjectDiaryAdapter.getmData().get(pos - 1);
                                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH, "postlist|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE), listData.getEvent_params(), new ActivityTypeData("41"));

                                            WebUrlTypeUtil.getInstance(mContext).urlToApp(listData.getUrl());
                                        }
                                    }
                                });
                            } else {
                                mProjectDiaryAdapter.addData(lvBBslistData);
                            }
                        }

                        mPage++;
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }else{
                    bbsNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}