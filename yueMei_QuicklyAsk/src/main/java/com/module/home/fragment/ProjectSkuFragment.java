package com.module.home.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.bean.ProjectDetailBoard;
import com.module.commonview.module.bean.TaoScreenTitleData;
import com.module.commonview.view.TaoScreenTitleView;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.model.bean.TaoListDataType;
import com.module.community.model.bean.TaoListDoctors;
import com.module.community.model.bean.TaoListDoctorsCompared;
import com.module.community.model.bean.TaoListDoctorsEnum;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.FilterDataApi;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.controller.activity.ChannelTwoActivity;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.module.home.model.bean.RecommentTaoList;
import com.module.home.model.bean.SearchResultDoctor;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.view.adapter.SearhBoardRecyclerAdapter;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/2/22
 */
public class ProjectSkuFragment extends YMBaseFragment {

    private final String TAG = "ProjectSkuFragment";
    @BindView(R.id.project_sku_screen)
    TaoScreenTitleView mSkuScreen;
    @BindView(R.id.project_activity_recycler)
    RecyclerView mActivityRecycler;
    @BindView(R.id.project_sku_rec)
    RecyclerView mSkuRecycler;
    @BindView(R.id.project_sku_refresh)
    SmartRefreshLayout mSkuRefresh;
    @BindView(R.id.fragment_project_details_not)
    NestedScrollView mNotData;

    private ProjectDetailsApi mProjectDetailsApi;
    private FilterDataApi filterDataApi;
    private ArrayList<HomeTaoData> mFragmentData;
    private ArrayList<HomeTaoData> mFragmentDataCache;
    private SearchResultDoctor mDoctors;
    private SearchResultDoctor mDoctorsCache;
    private RecommentTaoList mRecommentTaoList;
    private RecommentTaoList mRecommentTaoListCache;
    private TaoListAdapter mProjectSkuAdapter;
    private int mPage = 1;
    private ProjectDetailsBean detailsBean;
    private BaseNetWorkCallBackApi getScreenBoard;
    private SearchResultBoard searchActivity;                       //活动
    private LinearLayoutManager linearLayoutManager;
    private int lastVisibleItemPosition;


    @SuppressLint("HandlerLeak")
    public static ProjectSkuFragment newInstance(ArrayList<HomeTaoData> datas, SearchResultDoctor doctors, RecommentTaoList recommentTaoList, ProjectDetailsBean bean) {
        ProjectSkuFragment fragment = new ProjectSkuFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("datas", datas);
        bundle.putParcelable("doctors", doctors);
        bundle.putParcelable("bean", bean);
        bundle.putParcelable("recommentTaoList", recommentTaoList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @SuppressLint("HandlerLeak")
    public static ProjectSkuFragment newInstance(ArrayList<HomeTaoData> datas, SearchResultDoctor doctors, ProjectDetailsBean bean) {
        ProjectSkuFragment fragment = new ProjectSkuFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("datas", datas);
        bundle.putParcelable("doctors", doctors);
        bundle.putParcelable("bean", bean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_details_sku;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mFragmentData = getArguments().getParcelableArrayList("datas");
            mFragmentDataCache = mFragmentData;
            mDoctors = getArguments().getParcelable("doctors");
            mDoctorsCache = mDoctors;
            detailsBean = getArguments().getParcelable("bean");
            mRecommentTaoList = getArguments().getParcelable("recommentTaoList");
            mRecommentTaoListCache = mRecommentTaoList;
        }

        mSkuScreen.initView(false, "1");
        mSkuScreen.setOnEventClickListener(new TaoScreenTitleView.OnEventClickListener() {

            @Override
            public void onScreenClick() {
                FragmentActivity activity = getActivity();
                if (getActivity() instanceof ChannelPartsActivity) {
                    ChannelPartsActivity channelPartsActivity = (ChannelPartsActivity) activity;
                    if (channelPartsActivity != null) {
                        channelPartsActivity.channelBottomFragment.getProjectDetailsData();
                    }
                } else if (getActivity() instanceof ChannelFourActivity) {
                    ChannelFourActivity channelFourActivity = (ChannelFourActivity) activity;
                    if (channelFourActivity != null) {
                        channelFourActivity.channelBottomFragment.getProjectDetailsData();
                    }
                }
                refresh();
            }

            @Override
            public void onChangeClick(boolean isChange) {

            }
        });


        mNotData.setPadding(0, Utils.dip2px(100), 0, 0);

        //上拉加载更多
        mSkuRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mSkuRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                getProjectDetailsData();
            }
        });

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mSkuRecycler.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.getLocalColor(mContext, R.color.subscribe_item_drag_bg)));
        mSkuRecycler.setLayoutManager(linearLayoutManager);

        //检测recylerview的滚动事件
        mSkuRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int lastItemPosition = linearLayoutManager.findLastVisibleItemPosition();
                    if (lastItemPosition > lastVisibleItemPosition) {
                        lastVisibleItemPosition = lastItemPosition;
                    }
                }
            }
        });
    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        filterDataApi = new FilterDataApi();
        getScreenBoard = new BaseNetWorkCallBackApi(FinalConstant1.BOARD, "getScreenBoard");
        if (mFragmentData.size() == 0) {
            getProjectDetailsData();
        } else {
            mPage++;
            setRecyclerData(mFragmentData, mDoctors, mRecommentTaoList);
        }

        setSortData();
        loadKindData();
        lodBoardData();
    }

    @Override
    public void onPause() {
        saveData();
        super.onPause();
    }

    /**
     * 获取页面数据
     */
    private void getProjectDetailsData() {
        TaoScreenTitleData taoScreenTitleData = mSkuScreen.getTaoScreenTitleData();

        Log.e(TAG, "homeSource == " + detailsBean.getHomeSource());
        Log.e(TAG, "parentLabelID == " + detailsBean.getTwoLabelId());
        Log.e(TAG, "labelID == " + detailsBean.getFourLabelId());
        Log.e(TAG, "page == " + mPage);

        mProjectDetailsApi.getProjectDetailsHashMap().clear();
        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "1");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("page", mPage + "");
        mProjectDetailsApi.addData("sort", taoScreenTitleData.getSort());

        //筛选
        for (ProjcetList data : taoScreenTitleData.getKindStr()) {
            Log.e(TAG, data.getPostName() + ":" + data.getPostVal());
            mProjectDetailsApi.addData(data.getPostName(), data.getPostVal());
        }

        //活动
        if (searchActivity != null) {
            mProjectDetailsApi.addData(searchActivity.getPostName(), searchActivity.getPostVal());
        }

        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(ProjectDetailsData data) {
                mPage++;
                if (data.getTaoList().size() == 0) {
                    mSkuRefresh.finishLoadMoreWithNoMoreData();
                } else {
                    mSkuRefresh.finishLoadMore();
                }

                if (mProjectSkuAdapter == null) {
                    if (data.getTaoList().size() != 0) {
                        mSkuRecycler.setVisibility(View.VISIBLE);
                        mNotData.setVisibility(View.GONE);

                        mFragmentData = data.getTaoList();
                        mDoctors = data.getComparedConsultative();
                        mRecommentTaoList = data.getRecommend_tao_list();

                        if (data.getRecommend_tao_list() != null
                                && !TextUtils.isEmpty(data.getRecommend_tao_list().getShow_sku_list_position())
                                && data.getRecommend_tao_list().getTao_list() != null
                                && data.getRecommend_tao_list().getTao_list().size() != 0) {
                            setRecyclerData(mFragmentData, mDoctors, data.getRecommend_tao_list());
                        } else {
                            setRecyclerData(mFragmentData, mDoctors, null);
                        }
                    } else {
                        mSkuRecycler.setVisibility(View.GONE);
                        mNotData.setVisibility(View.VISIBLE);
                    }

                } else {
                    mProjectSkuAdapter.addData(data.getTaoList());
                }
            }
        });
    }

    /**
     * 设置列表数据
     */
    private void setRecyclerData(ArrayList<HomeTaoData> fragmentData, SearchResultDoctor mDoctors, RecommentTaoList recommentTaoList) {
        mProjectSkuAdapter = new TaoListAdapter(mContext, fragmentData, mDoctors, recommentTaoList);
        mSkuRecycler.setAdapter(mProjectSkuAdapter);

        mProjectSkuAdapter.setOnItemClickListener(new TaoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TaoListDataType taoData, int pos) {
                HomeTaoData data = taoData.getTao();
                if (TextUtils.isEmpty(data.getTuijianTitle())) {
                    HashMap<String, String> event_params = data.getEvent_params();
                    ActivityTypeData activityTypeData = null;
                    if (getActivity() instanceof ChannelTwoActivity || getActivity() instanceof ChannelFourActivity || getActivity() instanceof ChannelPartsActivity) {
                        activityTypeData = new ActivityTypeData("97");
                    }
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "channel|taolist_" + Utils.getCity() + "_" + getTagId() + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE + ""), event_params, activityTypeData);
                    String id = data.get_id();
                    Intent it1 = new Intent(mContext, TaoDetailActivity.class);
                    it1.putExtra("id", id);
                    it1.putExtra("source", "96");
                    it1.putExtra("objid", "0");
                    mContext.startActivity(it1);
                }
            }
        });

        //咨询统计
        mProjectSkuAdapter.setOnDoctorChatlickListener(new TaoListAdapter.OnDoctorChatlickListener() {
            @Override
            public void onDoctorChatClick(HashMap<String, String> event_params) {
                YmStatistics.getInstance().tongjiApp(event_params);
            }

            @Override
            public void onChangeClick(TaoListDoctorsCompared compared, int changePos) {
                TaoListDoctorsEnum doctorsEnum = compared.getDoctorsEnum();
                HashMap<String, String> changeOneEventParams = compared.getChangeOneEventParams();
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.COMPARED_CHANGE_ONE_CLICK, (changePos + 1) + "", (doctorsEnum == TaoListDoctorsEnum.TOP ? "0" : "1")), changeOneEventParams);
            }
        });
    }

    /**
     * 获取活动数据
     */
    private void lodBoardData() {
        getScreenBoard.addData("flag", "97");
        getScreenBoard.addData("board_id", "97");
        getScreenBoard.startCallBack(new BaseCallBackListener<ServerData>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    ProjectDetailBoard projectDetailBoard = JSONUtil.TransformSingleBean(data.data, ProjectDetailBoard.class);
                    if (projectDetailBoard != null) {
                        ArrayList<SearchResultBoard> resultBoards = projectDetailBoard.getScreen_board();
                        if (CollectionUtils.isNotEmpty(resultBoards)) {
                            if (getActivity() != null) {
                                setHeadView(resultBoards);
                            }
                        }
                    }

                }
            }
        });
    }

    /**
     * 设置头布局
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setHeadView(ArrayList<SearchResultBoard> resultBoards) {

        //活动列表设置
        LinearLayoutManager activityLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        ((DefaultItemAnimator) Objects.requireNonNull(mActivityRecycler.getItemAnimator())).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)
        SearhBoardRecyclerAdapter mBoardRecyclerAdapter = new SearhBoardRecyclerAdapter(mContext, resultBoards);
        mActivityRecycler.setLayoutManager(activityLinearLayoutManager);
        mActivityRecycler.setAdapter(mBoardRecyclerAdapter);
        mBoardRecyclerAdapter.setOnEventClickListener(new SearhBoardRecyclerAdapter.OnEventClickListener() {
            @Override
            public void onItemClick(SearchResultBoard data) {
                if (data != null) {
                    YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                    searchActivity = data;
                    mPage = 1;
                    mProjectSkuAdapter = null;
                    getProjectDetailsData();
                } else {
                    setRecyclerData(mFragmentDataCache, mDoctorsCache, mRecommentTaoListCache);
                }
            }
        });
    }


    /**
     * 获取选中标签
     *
     * @return
     */
    private String getTagId() {
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId()) && !"0".equals(detailsBean.getFourLabelId())) {
            return detailsBean.getFourLabelId();
        } else {
            return detailsBean.getTwoLabelId();
        }
    }

    /**
     * 四级标签点击，刷新数据
     *
     * @param taoList
     * @param bean
     */
    public void setRefreshData(ArrayList<HomeTaoData> taoList, SearchResultDoctor doctors, ProjectDetailsBean bean) {
        if (mProjectDetailsApi != null) {
            detailsBean = bean;

            mFragmentData = taoList;
            mDoctors = doctors;

            refresh();
        }
    }

    /**
     * 筛选刷新点击
     */
    private void refresh() {
        saveData();
        mPage = 1;
        mProjectSkuAdapter = null;
        getProjectDetailsData();
    }


    /**
     * 排序数据
     * 排序优化
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("4");
        a2.setName("销量最高");
        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("3");
        a3.setName("价格最低");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("2");
        a4.setName("价格最高");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("7");
        a5.setName("离我最近");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("8");
        a6.setName("评分最高");
        TaoPopItemData a7 = new TaoPopItemData();
        a7.set_id("9");
        a7.setName("最新上架");
        TaoPopItemData a8 = new TaoPopItemData();
        a8.set_id("5");
        a8.setName("案例最多");
//        TaoPopItemData a1 = new TaoPopItemData();
//        a1.set_id("1");
//        a1.setName("综合");
//        TaoPopItemData a2 = new TaoPopItemData();
//        a2.set_id("3");
//        a2.setName("价格从低到高");
//        TaoPopItemData a3 = new TaoPopItemData();
//        a3.set_id("2");
//        a3.setName("价格从高到低");
//        TaoPopItemData a6 = new TaoPopItemData();
//        a6.set_id("7");
//        a6.setName("离我最近");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a3);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a6);
        lvSortData.add(a7);
        lvSortData.add(a8);
        mSkuScreen.setSortData(lvSortData);
    }


    /**
     * 获取筛选的数据
     */
    private void loadKindData() {
        filterDataApi.getCallBack(mContext, filterDataApi.getHashMap(), new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> data) {
                if (mSkuScreen != null) {
                    mSkuScreen.setScreeData(data);
                }
            }

        });
    }

    /**
     * 保存曝光数据
     */
    private void saveData() {
        try {
            List<HashMap<String, String>> doctors = new ArrayList<>();
            if (mProjectSkuAdapter != null) {
                List<HomeTaoData> datas = mProjectSkuAdapter.getData();
                int size = lastVisibleItemPosition < datas.size() ? lastVisibleItemPosition : datas.size();
                for (int i = 0; i < size; i++) {
                    List<TaoListDoctors> comparChatDatas = mProjectSkuAdapter.getComparChatData();

                    for (int j = 0; j < comparChatDatas.size(); j++) {
                        TaoListDoctorsCompared compared = comparChatDatas.get(j).getCompared();
                        int doctorSelectPos = Integer.parseInt(compared.getShowSkuListPosition());
                        if (doctorSelectPos >= 0 && i == doctorSelectPos) {
                            doctors.add(compared.getExposureEventParams());
                        }
                    }
                }
            }

            for (HashMap<String, String> doc : doctors) {
                if (doc != null) {
                    Log.e(TAG, "doc == " + doc.toString());
                    YmStatistics.getInstance().tongjiApp(doc);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

