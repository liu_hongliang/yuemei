package com.module.home.fragment;

import com.module.my.model.bean.MyFansData;

import java.util.List;

/**
 * Created by 裴成浩 on 2019/9/5
 */
public class SearchResultUserData {
    private List<MyFansData> list;

    public List<MyFansData> getList() {
        return list;
    }

    public void setList(List<MyFansData> list) {
        this.list = list;
    }
}
