package com.module.home.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.ProjectUserAdapter;
import com.module.my.model.bean.MyFansData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.List;

import butterknife.BindView;

/**
 * 搜索用户页
 * Created by 裴成浩 on 2019/9/5
 */
public class SearchResultsUserFragment extends YMBaseFragment {

    @BindView(R.id.search_results_user_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_user_recycler)
    RecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView userNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;

    private String TAG = "SearchResultsUserFragment";

    private int mPage = 1;
    private String mKey;
    private String mType;
    private ProjectUserAdapter projectUserAdapter;
    private Gson mGson;

    public static SearchResultsUserFragment newInstance(String key, String type) {
        SearchResultsUserFragment fragment = new SearchResultsUserFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_user_view;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mType = getArguments().getString("type");

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                projectUserAdapter = null;
                mPage = 1;
                loadingData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        mGson = new Gson();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);
        loadingData();
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));             //筛选词
        lodHotIssueDataApi.addData("type", mType);            //搜索数据类型
        lodHotIssueDataApi.addData("page", mPage + "");     //页码
        if (onEventClickListener != null) {
//                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
            onEventClickListener.onSearchParaClick(mGson.toJson(lodHotIssueDataApi.getHashMap()));
        }
        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultUserData searchResultUserData = JSONUtil.TransformSingleBean(serverData.data, SearchResultUserData.class);
                        List<MyFansData> listDatas = searchResultUserData.getList();

                        //刷新隐藏
                        mRefresh.finishRefresh();
                        if (listDatas.size() == 0) {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            mRefresh.finishLoadMore();
                        }

                        if (mPage == 1 && listDatas.size() == 0) {
                            userNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            userNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);

                            if (projectUserAdapter == null) {
                                projectUserAdapter = new ProjectUserAdapter(mContext, listDatas);
                                mRecycler.setAdapter(projectUserAdapter);
                            } else {
                                projectUserAdapter.addData(listDatas);
                            }
                        }

                        mPage++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    userNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
