package com.module.home.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.module.MainActivity;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.ui.HomeBanner;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.ScrollGridLayoutManager;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.model.bean.HistorySearchWords;
import com.module.community.other.MyRecyclerViewDivider;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.home.controller.activity.AllProjectActivity;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.controller.activity.ChannelTwoActivity;
import com.module.home.controller.activity.HomeActivity;
import com.module.home.controller.activity.HomeTopTitle;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.HomeFourColumnAdapter;
import com.module.home.controller.adapter.HomeHorizontlaAdapter;
import com.module.home.controller.adapter.HomeSaleListAdapter;
import com.module.home.controller.adapter.NavAdapterViewHolder;
import com.module.home.controller.other.HomeInitFlowLayout;
import com.module.home.model.bean.CardBean;
import com.module.home.model.bean.HomeActivityData;
import com.module.home.model.bean.HomeNav;
import com.module.home.model.bean.HomeNewData;
import com.module.home.model.bean.HotBean;
import com.module.home.model.bean.HuangDeng1;
import com.module.home.model.bean.NewZt;
import com.module.home.model.bean.NewZtBottom;
import com.module.home.model.bean.TaoData;
import com.module.home.view.AbstractHolder;
import com.module.home.view.Holder;
import com.module.home.view.HomeBackgroundImage;
import com.module.home.view.HomeTopPromote;
import com.module.home.view.IndicatorView;
import com.module.home.view.MetroTopListView;
import com.module.home.view.OnViewpagerChangeListener;
import com.module.home.view.PageMenuLayout;
import com.module.home.view.PageMenuViewHolderCreator;
import com.module.home.view.ScrollerViewpager;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.taodetail.model.bean.TaoTagItem;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.FlowLayout;
import com.tendcloud.tenddata.TCAgent;
import com.youth.banner.listener.OnBannerListener;

import org.apache.commons.collections.CollectionUtils;
import org.kymjs.aframe.database.KJDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 首页头部首页栏
 * Created by 裴成浩 on 2019/9/17
 */
public class HomeHomeTopFragment extends YMBaseFragment {

    @BindView(R.id.home_tablayout_top_hot_word)
    public LinearLayout mTopHotWord;                         //头部热词容器
    @BindView(R.id.home_tablayout_top_flow_layout)
    FlowLayout mTopFlowLayout;                              //头部热词列表

    @BindView(R.id.home_tablayout_top_banner)
    public HomeBanner mTopBanner;                           //头部幻灯片

    @BindView(R.id.home_banner_bottom_image1)
    ImageView mBannerBottomImage1;                         //幻灯片下图片1
    @BindView(R.id.home_banner_bottom_title1)
    TextView mBannerBottomtitle1;                          //幻灯片下文案1
    @BindView(R.id.home_banner_bottom_image2)
    ImageView mBannerBottomImage2;                         //幻灯片下图片2
    @BindView(R.id.home_banner_bottom_title2)
    TextView mBannerBottomtitle2;                          //幻灯片下文案2
    @BindView(R.id.home_banner_bottom_image3)
    ImageView mBannerBottomImage3;                         //幻灯片下图片3
    @BindView(R.id.home_banner_bottom_title3)
    TextView mBannerBottomtitle3;                          //幻灯片下文案3
    @BindView(R.id.home_banner_bottom_image4)
    ImageView mBannerBottomImage4;                         //幻灯片下图片4
    @BindView(R.id.home_banner_bottom_title4)
    TextView mBannerBottomtitle4;                          //幻灯片下文案4

    @BindView(R.id.home_tablayout_top_promote)
    HomeTopPromote mTopPromote;                             //头部大促模块
    @BindView(R.id.home_banner_bottom_view)
    LinearLayout mBannerBottom;                             //幻灯片底部文案

    @BindView(R.id.home_tab_bg_iv)
    ImageView homeTabBgIv;                                 //10个项目导航球的背景

    @BindView(R.id.home_navigation_638)
    LinearLayout homeNavigation;                           //10个项目导航球
    @BindView(R.id.page_menu_layout)
    PageMenuLayout<TaoTagItem> mPageMenuLayout;
    @BindView(R.id.main_home_entrance_indicator)
    IndicatorView mIndicatorView;

    @BindView(R.id.home_horizontal_list)
    RecyclerView homeHorizontalList;                        //栏目5卡

    @BindView(R.id.metro_top_list)
    MetroTopListView metroTopList;                          //metro位 1-6

    //新人专享
    @BindView(R.id.newuser_metro_image)
    ImageView newuserMetroImage;
    @BindView(R.id.newuser_metro_money)
    TextView newuserMetroMoney;
    @BindView(R.id.home_viewpager)
    ScrollerViewpager newuserMetroViewPager;
    @BindView(R.id.newuser_metro_click)
    RelativeLayout newuserMetroClick;

    //一行4卡
    @BindView(R.id.home_four_column_list)
    RecyclerView mFourColumnList;

    //快报
    @BindView(R.id.home_new_reports_view)
    LinearLayout newReportsView;
    @BindView(R.id.home_new_reports_flipper)
    ViewFlipper newFlipper;

    //新人专享  快报之下
    @BindView(R.id.newuser_metro_image2)
    ImageView newuserMetroImage2;
    @BindView(R.id.newuser_metro_money2)
    TextView newuserMetroMone2;
    @BindView(R.id.home_viewpager2)
    ScrollerViewpager newuserMetroViewPager2;
    @BindView(R.id.newuser_metro_click2)
    RelativeLayout newuserMetroClick2;

    //今日特卖
    @BindView(R.id.item_home_sale)
    LinearLayout homeSale;
    @BindView(R.id.acty_sale_list)
    RecyclerView mSaleList;

    private String TAG = "HomeHomeTopFragment";

    //导航球下的横滑卡右下角随机颜色
    private int[] homeCardColor = new int[]{R.color.home_bottom_1, R.color.home_bottom_2, R.color.home_bottom_3, R.color.home_bottom_4};
    private HomeNewData mData;
    private HomeBackgroundImage mTopBackground;            //背景颜色
    private boolean isFirst = true;

    public static HomeHomeTopFragment newInstance(HomeNewData data) {
        HomeHomeTopFragment homeHomeTopFragment = new HomeHomeTopFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        homeHomeTopFragment.setArguments(bundle);
        return homeHomeTopFragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.home_top_tablayout;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mData = getArguments().getParcelable("data");
        }

        //设置banner高度
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTopBanner.getLayoutParams();
        layoutParams.height = (windowsWight - layoutParams.leftMargin - layoutParams.rightMargin) * 117 / 351;
        mTopBanner.setLayoutParams(layoutParams);

        //设置大促UI
        setTopPromote();
    }


    @Override
    protected void initData(View view) {
        //热搜词
        hotWord();

        //幻灯片
        homeSlide();

        //10个导航球
        setTenTab();

        //栏目5卡
        setColumnFiveData();

        if (mData != null){
            //metro位初始化
            metroTopList.initView(mData.getMetro_top());
        }

        //新人专享
        newuserMetro();

        //一行四卡
        setFourColumnListData();

        //快报
        setnNewReportsData();

        //新人专享 之下快报
        newuserMetro2();

        //今日特卖
        saleDay();
    }


    @Override
    public void onStart() {
        super.onStart();
        //开始轮播
        mTopBanner.startAutoPlay();
    }

    @Override
    public void onStop() {
        super.onStop();
        //结束轮播
        mTopBanner.stopAutoPlay();
    }

    /**
     * 热词
     */
    private void hotWord() {
        if (mData == null)return;
        List<String> hotSearchWord = mData.getHotSearchWord();
        if (hotSearchWord != null && hotSearchWord.size() > 0) {
            mTopHotWord.setVisibility(View.VISIBLE);
            mTopFlowLayout.removeAllViews();

            //历史记录第一条添加
            List<HistorySearchWords> searchWords = KJDB.create(mContext, FinalConstant.YUEMEIWORDS).findAll(HistorySearchWords.class);
            mTopFlowLayout.setMaxLine(1);
            if (searchWords.size() > 0) {
                String hwords = searchWords.get(searchWords.size() - 1).getHwords();
                if (Utils.isGB2312(hwords) && hwords.length() <= 8) {
                    boolean isHaveWord = false;
                    for (String word : hotSearchWord) {
                        if (hwords.equals(word)) {
                            isHaveWord = true;
                            break;
                        }
                    }

                    if (!isHaveWord) {
                        hotSearchWord.add(1, hwords);
                    }
                }

            }

            HomeInitFlowLayout mHomeInitFlowLayout = new HomeInitFlowLayout(mContext, mTopFlowLayout, hotSearchWord);

            //点击事件
            mHomeInitFlowLayout.setClickCallBack(new HomeInitFlowLayout.ClickCallBack() {
                @Override
                public void onClick(View v, int pos, String key) {
                    Intent it = new Intent(mContext, SearchAllActivity668.class);
                    it.putExtra(SearchAllActivity668.KEYS, key);
                    it.putExtra(SearchAllActivity668.RESULTS, true);
                    startActivity(it);
                }
            });
        } else {
            mTopHotWord.setVisibility(View.GONE);
        }
    }

    /**
     * 幻灯片
     */
    private void homeSlide() {
        if (null == mData)return;
        final List<HuangDeng1> hdlist = mData.getHuandeng();                //幻灯片实体类
        if (hdlist == null) {
            return;
        }

        final int hdsize = hdlist.size();
        ArrayList<String> urlImageList = new ArrayList<>();
        ArrayList<String> txtViewpager = new ArrayList<>();

        for (int i = 0; i < hdsize; i++) {
            urlImageList.add(hdlist.get(i).getImg());
            txtViewpager.add(i + "1");
        }

        if (hdlist.size() > 0) {
            mTopBanner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int curIndex) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    //自己的统计
                    Log.e(TAG, "hdlist.get(curIndex).getEvent_params() == " + hdlist.get(curIndex).getEvent_params().toString());

                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS), hdlist.get(curIndex).getEvent_params());
                    //百度统计
                    Utils.baiduTongJi(mContext, "060", "首页焦点图");
                    //GrowingIO统计
                    int curIndex11 = curIndex + 1;
                    if (Integer.toString(curIndex11).length() == 1) {
                        StatisticalManage.getInstance().growingIO("Focus_graph0" + curIndex + 1);
                    } else {
                        StatisticalManage.getInstance().growingIO("Focus_graph" + curIndex + 1);
                    }

                    String url = hdlist.get(curIndex).getUrl();
                    if (!TextUtils.isEmpty(url)) {
                        Log.e(TAG, "url=====" + url);
                        Map<String, String> kv = new HashMap<>();
                        kv.put("位置", curIndex + "");
                        kv.put("地区", Utils.getCity());
                        TCAgent.onEvent(mContext, "首页焦点图", "url:" + url, kv);

                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "1", hdlist.get(curIndex).getQid());
                    }
                }
            });
        }

        if (isFirst) {
            //banner滑动监听
            mTopBanner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    Log.e(TAG, "onPageSelected--->i === " + i);
                    HuangDeng1 huangDeng = hdlist.get(i);
                    setGradientBackground(huangDeng.getColorTop().trim(), huangDeng.getColorBottom().trim());
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
            isFirst = false;
        }

        mTopBanner.setImagesData(urlImageList);
    }


    /**
     * 10个导航球
     */
    private void setTenTab() {
        if (null == mData)return;
        final HomeNav homeNav = mData.getNav();
        if (homeNav != null) {
            final List<TaoTagItem> homeTag = homeNav.getTag();                     //10个项目类型导航球
            //背景设置
            final String hTabBg = homeNav.getBgImage();
            if (!TextUtils.isEmpty(hTabBg)) {
                Glide.with(mContext).load(hTabBg).into(homeTabBgIv);
            } else {
                homeTabBgIv.setBackgroundResource(R.color.white);
            }


            //项目类型导航球
            if (null != homeTag && homeTag.size() > 0) {
                homeNavigation.setVisibility(View.VISIBLE);
                mPageMenuLayout.setPageDatas(homeTag, new PageMenuViewHolderCreator() {
                    @Override
                    public AbstractHolder createHolder(View itemView) {
                        return new AbstractHolder<TaoTagItem>(itemView) {
                            LinearLayout tagView;
                            ImageView tagImg;
                            TextView tagTitle;
                            @Override
                            protected void initView(View itemView) {
                                tagView = itemView.findViewById(R.id.tag_view);
                                tagImg = itemView.findViewById(R.id.tag_img);
                                tagTitle = itemView.findViewById(R.id.tag_title);
                            }

                            @Override
                            public void bindView(RecyclerView.ViewHolder holder, TaoTagItem data, final int position) {
                                Glide.with(mContext)
                                        .load(data.getImg())
                                        .into(tagImg);
                                tagTitle.setText(data.getTitle());
                                holder.itemView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        TaoTagItem taoTagItem = homeTag.get(position);
                                        YmStatistics.getInstance().tongjiApp(taoTagItem.getEvent_params());

                                        String one_id = taoTagItem.getOne_id();
                                        String two_id = taoTagItem.getTwo_id();
                                        String three_id = taoTagItem.getThree_id();
                                        String homesource = taoTagItem.getHomeSource();
                                        String isAll = taoTagItem.getIsAll();

                                        if ("1".equals(isAll)) {
                                            Intent intent = new Intent(mContext, AllProjectActivity.class);
                                            intent.putExtra("home_source", homesource);
                                            startActivity(intent);
                                        } else {
                                            switch (taoTagItem.getLevel()) {
                                                case "2":
                                                    //部位
                                                    Intent intent2 = new Intent(mContext, ChannelPartsActivity.class);
                                                    intent2.putExtra("id", one_id);
                                                    intent2.putExtra("title", taoTagItem.getChannel_title());
                                                    intent2.putExtra("home_source", homesource);
                                                    startActivity(intent2);
                                                    break;
                                                case "3":
                                                    //二级
                                                    Intent intent3 = new Intent(mContext, ChannelTwoActivity.class);
                                                    Bundle bundle = new Bundle();
                                                    bundle.putString(ChannelTwoActivity.TITLE, taoTagItem.getChannel_title());
                                                    bundle.putString(ChannelTwoActivity.SELECTED_ID, one_id);
                                                    bundle.putString(ChannelTwoActivity.TWO_ID, two_id);
                                                    bundle.putString(ChannelTwoActivity.HOME_SOURCE, homesource);
                                                    intent3.putExtra("data", bundle);
                                                    startActivity(intent3);
                                                    break;
                                                case "4":
                                                    //四级
                                                    Intent intent4 = new Intent(mContext, ChannelFourActivity.class);
                                                    intent4.putExtra("id", three_id);
                                                    intent4.putExtra("title", taoTagItem.getChannel_title());
                                                    intent4.putExtra("home_source", homesource);
                                                    startActivity(intent4);
                                                    break;
                                            }
                                        }
                                    }
                                });
                            }
                        };
                    }

                    @Override
                    public int getLayoutId() {
                        return R.layout.item_tao_tag;
                    }
                });
                mIndicatorView.setIndicatorCount(mPageMenuLayout.getPageCount());
                mPageMenuLayout.setOnPageListener(new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        mIndicatorView.setCurrentIndicator(position);
                    }
                });

            } else {
                homeNavigation.setVisibility(View.GONE);
            }
        }

    }


    /**
     * 栏目5卡
     */
    private void setColumnFiveData() {
        if (null == mData)return;
        //横向导航
        final List<CardBean> homeCard = mData.getKapian();
        if (homeCard != null && homeCard.size() > 0) {
            homeHorizontalList.setVisibility(View.VISIBLE);
            for (int i = 0; i < homeCard.size(); i++) {
                homeCard.get(i).setColor(ContextCompat.getColor(mContext, homeCardColor[i % homeCardColor.length]));
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
            HomeHorizontlaAdapter homeHorizontlaAdapter = new HomeHorizontlaAdapter(R.layout.home_horizontal_list_item, homeCard);
            homeHorizontalList.setFocusable(false);//解决卡顿
            homeHorizontalList.setLayoutManager(linearLayoutManager);
            homeHorizontalList.setAdapter(homeHorizontlaAdapter);
            homeHorizontlaAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    CardBean cardBean = homeCard.get(position);
                    //百度统计
                    Utils.baiduTongJi(mContext, "061", cardBean.getTitle());

                    String type = cardBean.getType();
                    String qid = cardBean.getQid();
                    final String url = cardBean.getUrl();
                    YmStatistics.getInstance().tongjiApp(cardBean.getEvent_params());

                    Log.e(TAG, "type == " + type);
                    Log.e(TAG, "url == " + url);
                    if (!TextUtils.isEmpty(url) && url.startsWith("http")) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    } else {
                        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(qid)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:link:eq:" + url + ":and:id:eq:" + qid);
                        } else if (!TextUtils.isEmpty(url)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:link:eq:" + url);
                        } else if (!TextUtils.isEmpty(qid)) {
                            WebViewUrlLoading.getInstance().showWebDetail(mContext, "type:eq:" + type + ":and:id:eq:" + qid);
                        }
                    }
                }
            });
        } else {
            homeHorizontalList.setVisibility(View.GONE);
        }
    }

    /**
     * 新人专享
     */
    private void newuserMetro() {
        if (null == mData)return;
        final NewZt mNewZt = mData.getNew_zt();
        if (mNewZt != null) {
            List<TaoData> taodata = mNewZt.getTaodata();
            if (taodata != null && taodata.size() > 0) {
                newuserMetroClick.setVisibility(View.VISIBLE);
                newuserMetroViewPager.init(mContext, mNewZt.getTaodata(), 4, ScrollerViewpager.split(taodata, 2), new OnViewpagerChangeListener() {
                    @Override
                    public void onChange(int currentPage) {

                    }
                });
                setViewPagerScrollSpeed(newuserMetroViewPager);
                newuserMetroClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> eventParams = mNewZt.getEvent_params();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mNewZt.getUrl());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_NEWZT, "1", "0"), eventParams, new ActivityTypeData("1"));

                    }
                });

                newuserMetroMoney.setText(mNewZt.getCoupons().getMoney());
            } else {
                newuserMetroClick.setVisibility(View.GONE);
            }
        } else {
            newuserMetroClick.setVisibility(View.GONE);
        }
    }

    /**
     * 新人专享2
     */
    private void newuserMetro2() {
        if (null == mData)return;
        final NewZtBottom mNewZtBottom = mData.getNew_zt_bottom();
        if (mNewZtBottom != null) {
            List<TaoData> taodata = mNewZtBottom.getTaodata();
            if (taodata != null && taodata.size() > 0) {
                newuserMetroClick2.setVisibility(View.VISIBLE);
                newuserMetroViewPager2.init(mContext, mNewZtBottom.getTaodata(), 4, ScrollerViewpager.split(taodata, 2), new OnViewpagerChangeListener() {
                    @Override
                    public void onChange(int currentPage) {

                    }
                });
                setViewPagerScrollSpeed(newuserMetroViewPager2);
                newuserMetroClick2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> eventParams = mNewZtBottom.getEvent_params();
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mNewZtBottom.getUrl());
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOME_NEWZT, "1", "0"), eventParams, new ActivityTypeData("1"));

                    }
                });

                newuserMetroMone2.setText(mNewZtBottom.getCoupons().getMoney());
            } else {
                newuserMetroClick2.setVisibility(View.GONE);
            }
        } else {
            newuserMetroClick2.setVisibility(View.GONE);
        }
    }

    /**
     * 设置ViewPager切换速度
     */
    private void setViewPagerScrollSpeed(ViewPager pager) {
        ScrollerViewpager.ViewPagerScroller scroller = new ScrollerViewpager.ViewPagerScroller(getActivity());
        scroller.setScrollDuration(1500);
        scroller.initViewPagerScroll(pager);//这个是设置切换过渡时间为2秒
    }

    /**
     * 设置一行4卡数据
     */
    private void setFourColumnListData() {
        if (null == mData)return;
        List<List<HomeActivityData>> activity = mData.getActivity();
        if (activity != null && activity.size() > 0) {
            mFourColumnList.setVisibility(View.VISIBLE);
            ScrollGridLayoutManager gridLayoutManager = new ScrollGridLayoutManager(mContext, 2);
            gridLayoutManager.setScrollEnable(false);
            HomeFourColumnAdapter homeFourColumnAdapter = new HomeFourColumnAdapter(mContext, activity);
            mFourColumnList.setLayoutManager(gridLayoutManager);
            mFourColumnList.setAdapter(homeFourColumnAdapter);
        } else {
            mFourColumnList.setVisibility(View.GONE);
        }

    }

    /**
     * 设置快报数据
     */
    private void setnNewReportsData() {
        if (null == mData)return;
        newFlipper.removeAllViews();
        List<HuangDeng1> express = mData.getExpress();
        if (express != null && express.size() > 0) {
            newReportsView.setVisibility(View.VISIBLE);
            for (final HuangDeng1 data : express) {
                View view = getLayoutInflater().inflate(R.layout.home_new_reports_flipper, null);
                TextView tv_type = view.findViewById(R.id.tv_type);
                TextView tv_title = view.findViewById(R.id.tv_title);
                tv_title.setText(data.getTitle());
                if (!TextUtils.isEmpty(data.getMetro_type())) {
                    switch (data.getMetro_type()) {
                        case "1":
                            tv_type.setText("科普");
                            break;
                        case "2":
                            tv_type.setText("八卦");
                            break;
                        case "3":
                            tv_type.setText("热点");
                            break;
                        case "4":
                            tv_type.setText("投票");
                            break;
                    }
                }
                newFlipper.addView(view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = data.getUrl();
                        HashMap<String, String> event_params = data.getEvent_params();
                        YmStatistics.getInstance().tongjiApp(event_params);
                        if (!TextUtils.isEmpty(url)) {
                            WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                        }
                    }
                });


//                TextView textView = new TextView(mContext);
//                textView.setLines(1);
//                textView.setEllipsize(TextUtils.TruncateAt.END);
//                textView.setGravity(Gravity.CENTER_VERTICAL);
//                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
//                textView.setTextColor(Utils.getLocalColor(mContext, R.color._33));
//                textView.setText(data.getTitle());
//
//                newFlipper.addView(textView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//                //点击事件
//                textView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        String url = data.getUrl();
//                        HashMap<String, String> event_params = data.getEvent_params();
//                        YmStatistics.getInstance().tongjiApp(event_params);
//                        if (!TextUtils.isEmpty(url)) {
//                            WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
//                        }
//                    }
//                });
            }
            newFlipper.setFlipInterval(3000);
            newFlipper.startFlipping();
        } else {
            newReportsView.setVisibility(View.GONE);
        }
    }

    /**
     * 今日特卖
     */
    private void saleDay() {
        if (null == mData)return;
        List<HotBean> hotData = mData.getHot();                    //今日特卖 实体类
        if (hotData != null && hotData.size() > 0) {
            homeSale.setVisibility(View.VISIBLE);

            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            HomeSaleListAdapter homeSaleListAdapter = new HomeSaleListAdapter(mContext, hotData);
            mSaleList.addItemDecoration(new MyRecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL, Utils.dip2px(1), Utils.setCustomColor("#efefef")));
            mSaleList.setLayoutManager(scrollLinearLayoutManager);
            mSaleList.setAdapter(homeSaleListAdapter);

            //item点击事件
            homeSaleListAdapter.setOnEventClickListener(new HomeSaleListAdapter.OnEventClickListener() {
                @Override
                public void onItemClick(View v, int pos, HotBean data) {
                    Log.e(TAG, "data.getEvent_params() == " + data.getEvent_params().toString());
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|hotsku|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, data.getBmsid(), "1"), data.getEvent_params());
                    YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                    String url = data.getUrl();
                    if (!TextUtils.isEmpty(url)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url, "1", "0");
                    }
                }
            });

        } else {
            homeSale.setVisibility(View.GONE);
        }
    }


    /**
     * 设置大促UI
     */
    private void setTopPromote() {
        if (mData != null) {
            List<HuangDeng1> metroBigPromotion = mData.getMetroBigPromotion();
            if (metroBigPromotion != null) {
                //设置大促高度
                if (metroBigPromotion.size() >= 2) {
                    mTopPromote.setVisibility(View.VISIBLE);
                    mTopPromote.initView(true, metroBigPromotion, windowsWight);
                    mTopPromote.setOnEventClickListener(new HomeTopPromote.OnEventClickListener() {
                        @Override
                        public void onItemClick(View v, HuangDeng1 data) {
                            String url = data.getUrl();
                            YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                            if (!TextUtils.isEmpty(url)) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                            }
                        }

                        @Override
                        public void onImageLoadingEndClick(int imgHeight) {
                            //设置背景颜色
                            setBackgroundColor(imgHeight);
                        }

                    });
                } else if (metroBigPromotion.size() == 1) {
                    mTopPromote.setVisibility(View.VISIBLE);
                    mTopPromote.initView(false, metroBigPromotion, windowsWight);
                    mTopPromote.setOnEventClickListener(new HomeTopPromote.OnEventClickListener() {
                        @Override
                        public void onItemClick(View v, HuangDeng1 data) {
                            String url = data.getUrl();
                            YmStatistics.getInstance().tongjiApp(data.getEvent_params());
                            if (!TextUtils.isEmpty(url)) {
                                WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                            }
                        }

                        @Override
                        public void onImageLoadingEndClick(int imgHeight) {
                            //设置背景颜色
                            setBackgroundColor(imgHeight);
                        }

                    });
                } else {
                    mTopPromote.initView(false, metroBigPromotion, windowsWight);
                    mTopPromote.setVisibility(View.GONE);
                    //设置背景颜色
                    setBackgroundColor(0);
                }
            } else {
                mTopPromote.setVisibility(View.GONE);
                //设置背景颜色
                setBackgroundColor(0);
            }
        }
    }


    /**
     * 设置背景颜色
     */
    private void setBackgroundColor(int imgHeight) {
        if (mContext instanceof HomeActivity) {
            HomeActivity homeActivity = (HomeActivity) this.mContext;
            HomeTopTitle mTopTitle = homeActivity.mTopTitle;
            mTopBackground = homeActivity.mTopBackground;

            int titleHeight = mTopTitle.getMeasuredHeight();
            if (mTopHotWord == null){
                return;
            }
            ViewGroup.MarginLayoutParams hotWordLp = (ViewGroup.MarginLayoutParams) mTopHotWord.getLayoutParams();
            ViewGroup.MarginLayoutParams bannerLp = (ViewGroup.MarginLayoutParams) mTopBanner.getLayoutParams();
            ViewGroup.MarginLayoutParams bottomLp = (ViewGroup.MarginLayoutParams) mBannerBottom.getLayoutParams();
            ViewGroup.MarginLayoutParams promoteLp = (ViewGroup.MarginLayoutParams) mTopPromote.getLayoutParams();

            int hotWordHeight = hotWordLp.height;
            int hotWordMargin = hotWordLp.topMargin;
            int bannerHeight = bannerLp.height;
            int bannerMargin = bannerLp.topMargin;
            int bottomHeight = bottomLp.height;
            int bottomMargin = bottomLp.topMargin;
            int promoteMargin = promoteLp.topMargin;

//            Log.e(TAG, "imgHeight === " + imgHeight);
//            Log.e(TAG, "hotWordHeight === " + hotWordHeight);
//            Log.e(TAG, "hotWordMargin === " + hotWordMargin);
//            Log.e(TAG, "titleHeight === " + titleHeight);
//            Log.e(TAG, "bannerHeight === " + bannerHeight);
//            Log.e(TAG, "bannerMargin === " + bannerMargin);
//            Log.e(TAG, "bottomHeight === " + bottomHeight);
//            Log.e(TAG, "bottomMargin === " + bottomMargin);
//            Log.e(TAG, "promoteMargin === " + promoteMargin);
//            Log.e(TAG, "imgHeight === " + imgHeight);

            //标题栏+banner高度+底部文案高度
            int height = titleHeight + bannerHeight + bottomHeight + bannerMargin + bottomMargin;

            //热词搜索存在+高度
            if (mTopHotWord.getVisibility() == View.VISIBLE) {
                height += hotWordHeight + hotWordMargin;
            }

            if (mTopPromote.getVisibility() == View.VISIBLE) {
                //大促存在+高度 + 底部padding>>>10dp
                height += imgHeight + promoteMargin;
                if (mData.getMetroBigPromotion().size() >= 2) {
                    height += Utils.dip2px(10);
                }
                setBannerBottomUI(true);
                mTopBackground.setDrawImg(false);
            } else {
                height -= bannerHeight / 5 + bottomHeight + bottomMargin;
                setBannerBottomUI(false);
                mTopBackground.setDrawImg(true);
            }

            ViewGroup.LayoutParams layoutParams3 = mTopBackground.getLayoutParams();
            layoutParams3.height = height;
            mTopBackground.setLayoutParams(layoutParams3);

            List<HuangDeng1> hdlist = mData.getHuandeng();
            if (hdlist != null && hdlist.size() > 0) {
                HuangDeng1 huangDeng = hdlist.get(0);
                setGradientBackground(huangDeng.getColorTop().trim(), huangDeng.getColorBottom().trim());
            }

            //大促背景图是否展示
            List<HuangDeng1> metroBigPromotion = mData.getMetroBigPromotion();
            String bigPromotionBgImage = mData.getBigPromotionBgImage();
            Log.e(TAG, "bigPromotionBgImage === " + bigPromotionBgImage);
            if (CollectionUtils.isNotEmpty(metroBigPromotion) && !TextUtils.isEmpty(bigPromotionBgImage)) {
                mFunctionManager.setImageSrc(mTopBackground, bigPromotionBgImage);
            } else {
                mFunctionManager.setImageSrc(mTopBackground, "");
            }
        }
    }


    /**
     * 设置渐变背景
     *
     * @param colorTop
     * @param colorBottom
     */
    private void setGradientBackground(String colorTop, String colorBottom) {
        if (mTopBackground != null) {
            try {
                if (colorTop.startsWith("#") && colorBottom.startsWith("#")) {
                    GradientDrawable aDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Utils.setCustomColor(colorTop), Utils.setCustomColor(colorBottom)});
                    mTopBackground.setBackground(aDrawable);
                } else {
                    if (colorTop.startsWith("#")) {
                        mTopBackground.setBackgroundColor(Utils.setCustomColor(colorTop));
                    } else if (colorBottom.startsWith("#")) {
                        mTopBackground.setBackgroundColor(Utils.setCustomColor(colorBottom));
                    } else {
                        mTopBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color.white));
                    }
                }
            } catch (NumberFormatException e) {
                mTopBackground.setBackgroundColor(Utils.getLocalColor(mContext, R.color.white));
            }
        }
    }

    /**
     * 设置幻灯片下模块风格
     *
     * @param isPromote :是否是大促
     */
    private void setBannerBottomUI(boolean isPromote) {
        if (isPromote) {
            mBannerBottomImage1.setImageResource(R.drawable.banner_bottom_view_left_white);
            mBannerBottomImage2.setImageResource(R.drawable.banner_bottom_view_left_white);
            mBannerBottomImage3.setImageResource(R.drawable.banner_bottom_view_left_white);
            mBannerBottomImage4.setImageResource(R.drawable.banner_bottom_view_left_white);
            mBannerBottomtitle1.setTextColor(Utils.setCustomColor("#ffffffff"));
            mBannerBottomtitle2.setTextColor(Utils.setCustomColor("#ffffffff"));
            mBannerBottomtitle3.setTextColor(Utils.setCustomColor("#ffffffff"));
            mBannerBottomtitle4.setTextColor(Utils.setCustomColor("#ffffffff"));
        } else {
            mBannerBottomImage1.setImageResource(R.drawable.banner_bottom_view_left_black);
            mBannerBottomImage2.setImageResource(R.drawable.banner_bottom_view_left_black);
            mBannerBottomImage3.setImageResource(R.drawable.banner_bottom_view_left_black);
            mBannerBottomImage4.setImageResource(R.drawable.banner_bottom_view_left_black);
            mBannerBottomtitle1.setTextColor(Utils.setCustomColor("#ff999999"));
            mBannerBottomtitle2.setTextColor(Utils.setCustomColor("#ff999999"));
            mBannerBottomtitle3.setTextColor(Utils.setCustomColor("#ff999999"));
            mBannerBottomtitle4.setTextColor(Utils.setCustomColor("#ff999999"));
        }
    }
}
