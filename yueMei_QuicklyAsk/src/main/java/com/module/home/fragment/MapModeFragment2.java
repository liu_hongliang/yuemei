package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.module.base.view.YMBaseFragment;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.model.bean.DocListData;
import com.module.doctor.view.StaScoreBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 文 件 名: MapModeFragment1
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-12 10:34
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class MapModeFragment2 extends YMBaseFragment {
    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_doctor_type)
    TextView tvDoctorType;
    @BindView(R.id.iv_doctor_type)
    ImageView ivDoctorType;
    @BindView(R.id.tv_case)
    TextView tvCase;
    @BindView(R.id.tv_hosname)
    TextView tvHosname;
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.other_illustrated_room_ratingbar)
    StaScoreBar otherIllustratedRoomRatingbar;
    private DocListData mDocData;
    Unbinder unbinder;

    public static MapModeFragment2 newInstance(DocListData docdata) {
        MapModeFragment2 fragment = new MapModeFragment2();
        Bundle bundle = new Bundle();
        bundle.putParcelable("datas", docdata);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item2_map_mode;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mDocData = getArguments().getParcelable("datas");
        }
        Glide.with(mContext).load(mDocData.getImg()).transform(new GlideCircleTransform(mContext)).into(ivIcon);
        tvName.setText(mDocData.getUsername());
        tvDoctorType.setText(mDocData.getTitle());
        if (null != mDocData.getTalent() && !mDocData.getTalent().equals("0")) {
            ivDoctorType.setVisibility(View.VISIBLE);
            if (mDocData.getTalent().equals("1")) {
                ivDoctorType.setBackgroundResource(R.drawable.talent_list);
            } else if (mDocData.getTalent().equals("2")) {
                ivDoctorType.setBackgroundResource(R.drawable.talent_list);
            } else if (mDocData.getTalent().equals("3")) {
                ivDoctorType.setBackgroundResource(R.drawable.talent_list);
            } else if (mDocData.getTalent().equals("4")) {
                ivDoctorType.setBackgroundResource(R.drawable.talent_list);
            } else if (mDocData.getTalent().equals("5")) {
                ivDoctorType.setBackgroundResource(R.drawable.renzheng_doc);
            } else if (mDocData.getTalent().equals("6")) {
                ivDoctorType.setBackgroundResource(R.drawable.teyao_doc);
            } else if (mDocData.getTalent().equals("7")) {
                ivDoctorType.setBackgroundResource(R.drawable.renzheng_doc);
            }
        } else {
            ivDoctorType.setVisibility(View.GONE);
        }
        //推荐项目预定人数
        String totalAppoint = mDocData.getDiaryTotal();
        if (!TextUtils.isEmpty(totalAppoint) && !"0".equals(totalAppoint)) {
            tvCase.setVisibility(View.VISIBLE);
            tvCase.setText(totalAppoint + "案例");
        } else {
            tvCase.setVisibility(View.GONE);
        }
        if (null != mDocData.getHspital_name() && mDocData.getHspital_name().length() > 0) {
            tvHosname.setVisibility(View.VISIBLE);
            tvHosname.setText(mDocData.getHspital_name());
        } else {
            tvHosname.setVisibility(View.GONE);
        }
        int rateSale = Integer.parseInt(mDocData.getComment_bili());
        if (rateSale != 0) {
            otherIllustratedRoomRatingbar.setVisibility(View.VISIBLE);
            otherIllustratedRoomRatingbar.setProgressBar(rateSale);    //评分
        } else {
            otherIllustratedRoomRatingbar.setVisibility(View.INVISIBLE);
        }

        ll_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                intent.putExtra("docId", mDocData.getUser_id());
                intent.putExtra("docName", mDocData.getUsername());
                intent.putExtra("partId", "");
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    protected void initData(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
