package com.module.home.fragment;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.base.view.YMTabLayoutAdapter;
import com.module.base.view.YMTabLayoutIndicator;
import com.module.commonview.view.TaoScreenTitleView;
import com.module.home.controller.activity.ChannelFourActivity;
import com.module.home.controller.activity.ChannelPartsActivity;
import com.module.home.model.api.ProjectDetailsApi;
import com.module.home.model.bean.ProjectDetailsBean;
import com.module.home.model.bean.ProjectDetailsData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author 裴成浩
 * @data 2019/10/31
 */
public class ChannelBottomFragment extends YMBaseFragment {
    public static final String TAG = "ChannelBottomFragment";
    @BindView(R.id.channel_appbar)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.fragment_bottom_channel_tab)
    TabLayout mTabLayout;
    @BindView(R.id.fragment_bottom_channel_pager)
    ViewPager mViewPager;

    private List<String> mPageTitleList = new ArrayList<>();
    private List<YMBaseFragment> mFragmentList = new ArrayList<>();

    private ProjectDetailsApi mProjectDetailsApi;
    private ProjectDetailsBean detailsBean;

    public static ChannelBottomFragment newInstance(ProjectDetailsBean bean) {
        ChannelBottomFragment channelBottomFragment = new ChannelBottomFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("bean", bean);
        channelBottomFragment.setArguments(bundle);
        return channelBottomFragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_bottom_channel_view;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            detailsBean = getArguments().getParcelable("bean");
        }

        mPageTitleList.add("商品");
        mPageTitleList.add("医院");
        mPageTitleList.add("医生");
        mPageTitleList.add("日记");
        mPageTitleList.add("问答");


        YMTabLayoutAdapter mPagerAdapter = new YMTabLayoutAdapter(getChildFragmentManager(), mPageTitleList, mFragmentList);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(17), Utils.dip2px(17));

    }

    @Override
    protected void initData(View view) {
        mProjectDetailsApi = new ProjectDetailsApi();
        getProjectDetailsData();
    }

    /**
     * 获取页面数据
     */
    public void getProjectDetailsData() {

        Log.e(TAG, "homeSource == " + detailsBean.getHomeSource());
        Log.e(TAG, "parentLabelID == " + detailsBean.getTwoLabelId());

        mProjectDetailsApi.addData("parentLabelID", detailsBean.getTwoLabelId());
        if (!TextUtils.isEmpty(detailsBean.getFourLabelId())) {
            Log.e(TAG, "labelID == " + detailsBean.getFourLabelId());
            mProjectDetailsApi.addData("labelID", detailsBean.getFourLabelId());
        }
        mProjectDetailsApi.addData("listType", "1");
        mProjectDetailsApi.addData("homeSource", detailsBean.getHomeSource());
        mProjectDetailsApi.addData("page", "1");
        mProjectDetailsApi.getCallBack(mContext, mProjectDetailsApi.getProjectDetailsHashMap(), new BaseCallBackListener<ProjectDetailsData>() {
            @Override
            public void onSuccess(final ProjectDetailsData data) {
                if (getActivity() != null) {
                    setTopData(data);
                    //TabLayout适配
                    setTabLayout(data);
                }
            }
        });
    }

    ChannelFourScrollFrament channelFourScrollFrament;
    ChannelPartsScrollFrament channelPartsScrollFrament;

    private void setTopData(ProjectDetailsData data) {
        if (getActivity() instanceof ChannelFourActivity) {
            if (channelFourScrollFrament == null) {
                channelFourScrollFrament = ChannelFourScrollFrament.newInstance(data);
                setActivityFragment(R.id.fragment_bottom_channel_top, channelFourScrollFrament);
            } else {
                channelFourScrollFrament.setData(data);
            }
        } else if (getActivity() instanceof ChannelPartsActivity) {
            if (channelPartsScrollFrament == null) {
                channelPartsScrollFrament = ChannelPartsScrollFrament.newInstance(data, detailsBean);
                setActivityFragment(R.id.fragment_bottom_channel_top, channelPartsScrollFrament);
            } else {
                channelPartsScrollFrament.setData(data, detailsBean);
            }
        }
    }

    /**
     * TabLayout适配
     *
     * @param data
     */
    private void setTabLayout(final ProjectDetailsData data) {

        if (mFragmentList.size() == 0) {
            ProjectSkuFragment projectSkuFragment = ProjectSkuFragment.newInstance(data.getTaoList(), data.getComparedConsultative(), data.getRecommend_tao_list(), detailsBean);
            ProjectHospitalFragment projectHospitalFragment = ProjectHospitalFragment.newInstance(data.getHospitalList(), detailsBean);
            ProjectDoctorFragment projectDoctorFragment = ProjectDoctorFragment.newInstance(data.getDoctorsList(), detailsBean);
            mFragmentList.add(projectSkuFragment);
            mFragmentList.add(projectHospitalFragment);
            mFragmentList.add(projectDoctorFragment);
            mFragmentList.add(ProjectDiaryFragment.newInstance(data.getDiaryList(), detailsBean));
            mFragmentList.add(ProjectAnswerFragment.newInstance(data.getQuestionList(), detailsBean));

            YMTabLayoutAdapter mPagerAdapter = new YMTabLayoutAdapter(getChildFragmentManager(), mPageTitleList, mFragmentList);
            mViewPager.setAdapter(mPagerAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
            ProjectSkuFragment skuFragment = (ProjectSkuFragment) mPagerAdapter.getItem(0);
            if (skuFragment.mSkuScreen != null) {

                int[] location = new int[2];
                skuFragment.mSkuScreen.getLocationOnScreen(location);
                Log.e(TAG, "mSkuScreen ===" + location[1]);
                projectSkuFragment.mSkuScreen.setRawHeight(location[1]);
                projectSkuFragment.mSkuScreen.setClickListener(new TaoScreenTitleView.ClickListener() {
                    @Override
                    public void onClickListener() {
                        mAppBarLayout.setExpanded(false, true);
                    }
                });
            }

            YMTabLayoutIndicator.newInstance().reflex(mTabLayout, Utils.dip2px(17), Utils.dip2px(17));
        } else {
            for (YMBaseFragment fragment : mFragmentList) {
                if (fragment instanceof ProjectSkuFragment) {

                    ProjectSkuFragment projectSkuFragment = (ProjectSkuFragment) fragment;
                    projectSkuFragment.setRefreshData(data.getTaoList(), data.getComparedConsultative(), detailsBean);

                } else if (fragment instanceof ProjectHospitalFragment) {

                    ProjectHospitalFragment projectHosFragment = (ProjectHospitalFragment) fragment;
                    projectHosFragment.setRefreshData(data.getHospitalList(), detailsBean);

                } else if (fragment instanceof ProjectDoctorFragment) {

                    ProjectDoctorFragment projectDocFragment = (ProjectDoctorFragment) fragment;
                    projectDocFragment.setRefreshData(data.getDoctorsList(), detailsBean);

                } else if (fragment instanceof ProjectDiaryFragment) {

                    ProjectDiaryFragment projectDiaryFragment = (ProjectDiaryFragment) fragment;
                    projectDiaryFragment.setRefreshData(data.getDiaryList(), detailsBean);
                }
            }

        }
    }

    public void setDetailsBean(ProjectDetailsBean detailsBean) {
        this.detailsBean = detailsBean;
        getProjectDetailsData();
    }

}