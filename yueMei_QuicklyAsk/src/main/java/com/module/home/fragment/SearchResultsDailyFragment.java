package com.module.home.fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.baidu.mobstat.StatService;
import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.module.api.IsFocuApi;
import com.module.commonview.module.bean.IsFocuData;
import com.module.community.model.bean.BBsListData550;
import com.module.home.controller.adapter.HomeStaggeredAdapter;
import com.module.home.controller.adapter.ProjectDiaryAdapter;
import com.module.home.model.bean.TuijOther;
import com.module.home.model.bean.Tuijshare;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData23;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 相关日记页面
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsDailyFragment extends YMBaseFragment {

    @BindView(R.id.search_results_daily_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_daily_recycler)
    RecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView dailyNotView;

    private BaseNetWorkCallBackApi lodHotIssueDataApi;
    private HomeStaggeredAdapter homeStaggeredAdapter;

    private String TAG = "SearchResultsDailyFragment";

    private int mPage = 1;
    private String mKey;
    private String mType;
    private ProjectDiaryAdapter projectDiaryAdapter;
    private int mTempPos = -1;
    private Gson mGson;
    private String hospital_id;

    public static SearchResultsDailyFragment newInstance(String key,String type,String hospital_id) {
        SearchResultsDailyFragment fragment = new SearchResultsDailyFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        bundle.putString("hospital_id", hospital_id);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.search_results_daily_view;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mKey = getArguments().getString("key");
            mType = getArguments().getString("type");
            hospital_id = getArguments().getString("hospital_id");
        }
        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                projectDiaryAdapter = null;
                mPage = 1;
                loadingData();
            }
        });


    }



    @Override
    protected void initData(View view) {
        if (TextUtils.isEmpty(hospital_id)) {
            lodHotIssueDataApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "index613");
        } else {
            lodHotIssueDataApi = new BaseNetWorkCallBackApi(FinalConstant1.HOME, "hossearch");
        }

        mGson = new Gson();

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//        mRecycler.setLayoutManager(linearLayoutManager);
        loadingData();
    }



    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));             //筛选词
        lodHotIssueDataApi.addData("type", mType);            //搜索数据类型
        lodHotIssueDataApi.addData("page", mPage + "");     //页码

        if (!TextUtils.isEmpty(hospital_id)) {
            lodHotIssueDataApi.addData("id", hospital_id);
        }
        if (onEventClickListener != null) {
//                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
            onEventClickListener.onSearchParaClick(mGson.toJson(lodHotIssueDataApi.getHashMap()));
        }
        lodHotIssueDataApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultData23 searchResultData23 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData23.class);
                        List<BBsListData550> lvBBslistData = searchResultData23.getList();

                        //刷新隐藏
                        mRefresh.finishRefresh();
                        if (lvBBslistData.size() == 0) {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            mRefresh.finishLoadMore();
                        }

                        if (mPage == 1 && lvBBslistData.size() == 0) {
                            dailyNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            dailyNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);

                            if (homeStaggeredAdapter == null) {

                                final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
//                                staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                                mRecycler.setItemAnimator(null);
                                homeStaggeredAdapter = new HomeStaggeredAdapter(mContext, lvBBslistData);
                                mRecycler.setLayoutManager(staggeredGridLayoutManager);
                                mRecycler.setAdapter(homeStaggeredAdapter);
                                homeStaggeredAdapter.setOnItemClickListener(new HomeStaggeredAdapter.OnItemClickListener() {
                                    @Override
                                    public void onPostItemListener(View view, int position,boolean isOther) {


                                        if (Utils.isFastDoubleClick()) {    //快速点击
                                            return;
                                        }
                                        Tuijshare tuijshare = homeStaggeredAdapter.getData().get(position);
                                        if (isOther){
                                            TuijOther other = tuijshare.getOther();
                                            String url = other.getUrl();
                                            WebUrlTypeUtil.getInstance(getContext()).urlToApp(url, "1", "0");
                                        }else {
                                            BBsListData550 post = tuijshare.getPost();
                                            String url = post.getUrl();
                                            String qid = post.getQ_id();
                                            String appmurl = post.getAppmurl();
                                            Log.e(TAG, "url111 === " + url);
                                            Log.e(TAG, "appmurl === " + appmurl);
                                            Log.e(TAG, "qid111 === " + qid);
                                            //日记列表
//                                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.BMS, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, post.getBmsid(),"1"), post.getEvent_params());
                                            //百度统计
                                            StatService.onEvent(getActivity(), "066", (position + 1) + "", 1);

                                            WebUrlTypeUtil.getInstance(getContext()).urlToApp(appmurl, "1", "0");
                                        }
                                    }

                                    @Override
                                    public void onPostSkuListener(View view, BBsListData550 data, int pos) {
                                        if (Utils.isFastDoubleClick()) {    //快速点击
                                            return;
                                        }
                                        String tao_id = data.getTao().getId();
                                        HashMap<String, String> event_params = data.getEvent_params();
                                        event_params.put("id",tao_id);
//                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHARETAO, "home|share_" + partId + "|" + (position + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, post.getBmsid(), "1"),event_params,new ActivityTypeData("1"));
                                        Intent intent = new Intent(mContext, TaoDetailActivity.class);
                                        intent.putExtra("id",tao_id);
                                        intent.putExtra("source", "0");
                                        intent.putExtra("objid", "0");
                                        mContext.startActivity(intent);
                                    }

                                    @Override
                                    public void onHosDocItemListener(View view, int pos) {
                                        if (Utils.isFastDoubleClick()) {    //快速点击
                                            return;
                                        }
                                        HashMap<String, String> event_params = homeStaggeredAdapter.getData().get(pos).getHos_doc().getEvent_params();
                                        event_params.put("id","obj_id");
//                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.HOMESHAREHOSDOC, "home|share_" + partId + "|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE, "obj_type", "1"),event_params,new ActivityTypeData("1"));
                                        WebUrlTypeUtil.getInstance(getActivity()).urlToApp(homeStaggeredAdapter.getData().get(pos).getHos_doc().getUrl(), "1", "0");

                                    }

                                    @Override
                                    public void onFeedSkuRecommendItemListener(View view, int pos) {

                                    }

                                });

//                                projectDiaryAdapter = new ProjectDiaryAdapter(mContext, lvBBslistData);
//                                mRecycler.setAdapter(projectDiaryAdapter);
//                                projectDiaryAdapter.setOnItemPersonClickListener(new ProjectDiaryAdapter.OnItemPersonClickListener() {
//                                    @Override
//                                    public void onItemClick(int pos) {
//                                        mTempPos = pos;
//                                        String appmurl = projectDiaryAdapter.getDatas().get(pos).getAppmurl();
//                                        HashMap<String, String> event_params = projectDiaryAdapter.getDatas().get(pos).getEvent_params();
//                                        if (!TextUtils.isEmpty(appmurl)) {
//                                            YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH, "diarylist|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE), event_params, new ActivityTypeData("40"));
//
//                                            WebUrlTypeUtil.getInstance(mContext).urlToApp(appmurl, "0", "0");
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onItemPersonClick(String id, int pos) {
//                                        mTempPos = pos;
//                                        Intent intent = new Intent(mContext, PersonCenterActivity641.class);
//                                        intent.putExtra("id", id);
//                                        startActivityForResult(intent, 18);
//                                    }
//                                });

                                mRecycler.addItemDecoration(new RecyclerView.ItemDecoration() {
                                    @Override
                                    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                                        super.getItemOffsets(outRect, view, parent, state);
                                        //判断左右列，方法2（推荐）
                                        StaggeredGridLayoutManager.LayoutParams params = ((StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams());
                                        if (params.getSpanIndex() != GridLayoutManager.LayoutParams.INVALID_SPAN_ID) {
                                            //getSpanIndex方法不管控件高度如何，始终都是左右左右返回index
                                            if (params.getSpanIndex() % 2 == 0) {
                                                //左列
//                                              outRect.left = Utils.dip2px(5);
                                                outRect.right = Utils.dip2px((int) 2.5);
                                            } else {
                                                //右列
                                                outRect.left = Utils.dip2px((int) 2.5);
//                                              outRect.right = Utils.dip2px(5);
                                            }
                                        }
                                    }
                                });


                            } else {
//                                projectDiaryAdapter.addData(lvBBslistData);
                                homeStaggeredAdapter.addData((ArrayList<BBsListData550>) lvBBslistData);
                            }
                        }

                        mPage++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    dailyNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "requestCode === " + requestCode);
        Log.e(TAG, "resultCode === " + resultCode);
        if (requestCode == 10 || requestCode == 18 && resultCode == 100) {
            if (mTempPos >= 0) {
                isFocu(projectDiaryAdapter.getmHotIssues().get(mTempPos).getUser_id());
            }
        }
    }

    /**
     * 判断是否关注
     */
    private void isFocu(String id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("objid", id);
        hashMap.put("type", "6");
        new IsFocuApi().getCallBack(mContext, hashMap, new BaseCallBackListener<IsFocuData>() {
            @Override
            public void onSuccess(IsFocuData isFocuData) {
                projectDiaryAdapter.setEachFollowing(mTempPos, isFocuData.getFolowing());
                mTempPos = -1;
            }
        });
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}