package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.module.base.view.YMBaseFragment;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.doctor.model.bean.HosListData;
import com.module.doctor.view.StaScoreBar;
import com.module.other.netWork.imageLoaderUtil.GlideCircleTransform;
import com.quicklyask.activity.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 文 件 名: MapModeFragment1
 * 创 建 人: 原成昊
 * 创建日期: 2020-03-12 10:34
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class MapModeFragment3 extends YMBaseFragment {

    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.iv_flag)
    ImageView ivFlag;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_doctor_type)
    ImageView ivDoctorType;
    @BindView(R.id.tv_yuyue)
    TextView tvYuyue;
    @BindView(R.id.tv_case)
    TextView tvCase;
    @BindView(R.id.tv_hosname)
    TextView tvHosname;
    @BindView(R.id.tv_location_name)
    TextView tvLocationName;
    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.other_illustrated_room_ratingbar)
    StaScoreBar otherIllustratedRoomRatingbar;
    private String mSearchHitBoardId;
    private HosListData mHosData;
    Unbinder unbinder;

    public static MapModeFragment3 newInstance(HosListData hosData, String searchHitBoardId) {
        MapModeFragment3 fragment = new MapModeFragment3();
        Bundle bundle = new Bundle();
        bundle.putParcelable("datas", hosData);
        bundle.putString("searchHitBoardId", searchHitBoardId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.item3_map_mode;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            mHosData = getArguments().getParcelable("datas");
            mSearchHitBoardId = getArguments().getString("searchHitBoardId");
        }
        Glide.with(mContext).load(mHosData.getImg()).transform(new GlideCircleTransform(mContext)).into(ivIcon);
        tvName.setText(mHosData.getHos_name());

        if(TextUtils.isEmpty(mHosData.getHospital_promotion_icon())){
            ivDoctorType.setVisibility(View.INVISIBLE);
        }else{
            ivDoctorType.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(mHosData.getHospital_promotion_icon()).into(ivDoctorType);
        }
//        if ("1".equals(hosList.get(mPos).getHospital_promotion_icon())) {
//            ivDoctorType.setBackgroundResource(R.drawable.gongli_list);
//        } else {
//            ivDoctorType.setBackgroundResource(R.drawable.minying_2x);
//        }
        //预订人数
        if (null != mHosData.getTotalAppoint() && mHosData.getTotalAppoint().length() > 0 && !mHosData.getTotalAppoint().equals("0")) {
            tvYuyue.setVisibility(View.VISIBLE);
            tvYuyue.setText(mHosData.getTotalAppoint() + "预约");
        } else {
            tvYuyue.setVisibility(View.GONE);
        }
        //日记数
        if (!TextUtils.isEmpty(mHosData.getDiaryTotal())) {
            tvCase.setText(mHosData.getDiaryTotal() + "案例");
            tvCase.setVisibility(View.VISIBLE);
        } else {
            tvCase.setVisibility(View.GONE);
        }
        tvHosname.setText(mHosData.getType());
        tvLocationName.setText(mHosData.getAddress());
        int rateSale = Integer.parseInt(mHosData.getComment_bili());
        if (rateSale != 0) {
            otherIllustratedRoomRatingbar.setVisibility(View.VISIBLE);
            otherIllustratedRoomRatingbar.setProgressBar(rateSale);    //评分
        } else {
            otherIllustratedRoomRatingbar.setVisibility(View.INVISIBLE);
        }
        ll_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext, HosDetailActivity.class);
                it.putExtra("hosid", mHosData.getHos_id());
                if (!TextUtils.isEmpty(mSearchHitBoardId)) {
                    it.putExtra("search_hit_board_id", mSearchHitBoardId);
                }
                mContext.startActivity(it);
            }
        });
    }

    @Override
    protected void initData(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
