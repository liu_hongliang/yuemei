package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.PopupWindow;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.ScreenTitleView;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.doctor.model.bean.DocListData;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.ProjectDocAdapter;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData4;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsDocFragment extends YMBaseFragment {

    @BindView(R.id.search_results_doc_screen)
    ScreenTitleView mScreen;
    @BindView(R.id.search_results_doc_refresh)
    public SmartRefreshLayout mRefresh;
    @BindView(R.id.search_results_doc_recycler)
    RecyclerView mRecycler;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView docNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;
    private String TAG = "SearchResultsDocFragment";
    private String mKey;
    private String mType;
    private int mPage = 1;
    private BaseCityPopwindows cityPop;
    private BaseSortPopupwindows sortPop;
    private String mSort = "1";                                 //排序的选中id
    private ProjectDocAdapter projectDocAdapter;
    private Gson mGson;

    public static SearchResultsDocFragment newInstance(String key,String type) {
        SearchResultsDocFragment fragment = new SearchResultsDocFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        boolean change = isVisibleToUser != getUserVisibleHint();
        super.setUserVisibleHint(isVisibleToUser);
        if (isResumed() && change) {
            if (getUserVisibleHint()) {
                onVisible();
            } else {
                onInvisible();
            }
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            onInvisible();
        } else {
            onVisible();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !isHidden()) {
            onVisible();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !isHidden()) {
            onInvisible();
        }
    }

    private void onVisible() {
        mScreen.setCityTitle(Utils.getCity());
        refresh();
    }

    private void onInvisible() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_doc_view;
    }

    @Override
    protected void initView(View view) {
        mKey = getArguments().getString("key");
        mType = getArguments().getString("type");

        mScreen.initView(false);
        mScreen.setCityTitle(Utils.getCity());
        mScreen.setOnEventClickListener(new ScreenTitleView.OnEventClickListener1() {
            @Override
            public void onCityClick() {
                if (cityPop != null) {
                    if (cityPop.isShowing()) {
                        cityPop.dismiss();
                    } else {
                        cityPop.showPop();
                    }
                    cityPop.initViewData();
                    mScreen.initCityView(cityPop.isShowing());
                }
            }

            @Override
            public void onSortClick() {
                if (sortPop != null) {
                    if (sortPop.isShowing()) {
                        sortPop.dismiss();
                    } else {
                        sortPop.showPop();
                    }
                    mScreen.initSortView(sortPop.isShowing());
                }
            }
        });

        //上拉加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });

    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();

        mGson = new Gson();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);

        cityPop = new BaseCityPopwindows(mContext, mScreen);
        setSortData();
        loadingData();

        //城市回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                Cfg.saveStr(mContext, FinalConstant.DWCITY, city);
                mScreen.setCityTitle(city);
                if (cityPop != null) {
                    cityPop.dismiss();
                    mScreen.initCityView(cityPop.isShowing());
                }
                mRecycler.scrollToPosition(0);
                refresh();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initCityView(false);
            }
        });
    }

    /**
     * 加载数据
     */
    private void loadingData() {
        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));
        lodHotIssueDataApi.addData("type",mType);
        lodHotIssueDataApi.addData("page", mPage + "");
        lodHotIssueDataApi.addData("sort", mSort);
        if (onEventClickListener != null) {
//                    onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
            onEventClickListener.onSearchParaClick(mGson.toJson(lodHotIssueDataApi.getHashMap()));
        }
        lodHotIssueDataApi.getCallBack(mContext, lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if ("1".equals(serverData.code)) {
                    try {
                        SearchResultData4 searchResultData4 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData4.class);
                        List<DocListData> lvHotIssueData = searchResultData4.getList();

                        //刷新隐藏
                        mRefresh.finishRefresh();
                        if (lvHotIssueData.size() == 0) {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        } else {
                            mRefresh.finishLoadMore();
                        }

                        if (mPage == 1 && lvHotIssueData.size() == 0) {
                            docNotView.setVisibility(View.VISIBLE);
                            mRefresh.setVisibility(View.GONE);
                        } else {
                            docNotView.setVisibility(View.GONE);
                            mRefresh.setVisibility(View.VISIBLE);
                        }
                            if (projectDocAdapter == null) {
                                projectDocAdapter = new ProjectDocAdapter(mContext, lvHotIssueData);
                                mRecycler.setAdapter(projectDocAdapter);
                                projectDocAdapter.setOnEventClickListener(new ProjectDocAdapter.OnEventClickListener() {
                                    @Override
                                    public void onItemClick(View v, int pos) {
                                        DocListData docListData = projectDocAdapter.getDatas().get(pos);
                                        HashMap<String, String> event_params = docListData.getEvent_params();
                                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.SEARCH, "doctorlist|" + (pos + 1) + "|" + FinalConstant1.YUEMEI_MARKET + "|" + FinalConstant1.YUEMEI_DEVICE), event_params, new ActivityTypeData("43"));
                                        String docId = docListData.getUser_id();
                                        String docName = docListData.getUsername();
                                        Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                                        intent.putExtra("docId", docId);
                                        intent.putExtra("docName", docName);
                                        intent.putExtra("partId", "");
                                        mContext.startActivity(intent);
                                    }
                                });
                            } else {
                                if (mPage == 1) {
                                    //刷新数据
                                    projectDocAdapter.refreshData(lvHotIssueData);
                                } else {
                                    //加载更多
                                    projectDocAdapter.addData(lvHotIssueData);
                                }
                            }
                        mPage++;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    docNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 排序数据
     */
    private void setSortData() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("4");
        a2.setName("预约数多");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("5");
        a4.setName("案例数多");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("8");
        a5.setName("评分高");

        List<TaoPopItemData> lvSortData = new ArrayList<>();
        lvSortData.add(a1);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a2);
        sortPop = new BaseSortPopupwindows(mContext, mScreen, lvSortData);

        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                if (sortPop != null) {
                    sortPop.dismiss();
                    mSort = sortId;
                    mScreen.initSortView(sortPop.isShowing());
                    mScreen.setSortTitle(sortName);
                }
                mRecycler.scrollToPosition(0);
                refresh();
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mScreen.initSortView(false);
            }
        });
    }

    /**
     * 刷新
     */
    private void refresh() {
//        projectDocAdapter = null;
        mPage = 1;
        loadingData();
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
