package com.module.home.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.module.MainActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.model.bean.HistorySearchWords;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.CitySearchEvent;
import com.module.event.SearchEvent;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.SearchInitAdapter;
import com.module.home.model.bean.SearchCompositeData;
import com.module.home.view.SearchInitFlowLayout;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResult;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.database.KJDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 初始化时的页面
 * Created by 裴成浩 on 2019/4/16
 */
public class SearchInitFragment extends YMBaseFragment {

    @BindView(R.id.search_init_record)
    LinearLayout mRecord;
    @BindView(R.id.search_init_record_remove)
    ImageView mRecordRemove;
    @BindView(R.id.search_init_record_recycler)
    FlowLayout mRecordRecycler;
    @BindView(R.id.search_init_bottom)
    RecyclerView mBottomList;

    private String type;
    private String hospital_id;
    private KJDB mKjdb;
    private String TAG = "SearchInitFragment";
    private BaseNetWorkCallBackApi searchindexApi;

    public static SearchInitFragment newInstance(String type, String hospital_id) {
        SearchInitFragment fragment = new SearchInitFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        bundle.putString("hospital_id", hospital_id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_init_view;
    }

    @Override
    protected void initView(View view) {
        //设置入口
        if (getArguments() != null) {
            type = getArguments().getString("type");
            hospital_id = getArguments().getString("hospital_id");
        }
        mKjdb = KJDB.create(mContext, FinalConstant.YUEMEIWORDS);
        setHistoryData(mKjdb.findAll(HistorySearchWords.class));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        boolean change = isVisibleToUser != getUserVisibleHint();
        super.setUserVisibleHint(isVisibleToUser);
        if (isResumed() && change) {
            if (getUserVisibleHint()) {
                onVisible();
            } else {
                onInvisible();
            }
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            onInvisible();
        } else {
            onVisible();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !isHidden()) {
            onVisible();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !isHidden()) {
            onInvisible();
        }
    }

    private void onVisible() {
        EventBus.getDefault().post(new SearchEvent(0));
    }

    private void onInvisible() {
        EventBus.getDefault().post(new SearchEvent(1));
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(SearchEvent msgEvent) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void initData(View view) {
        searchindexApi = new BaseNetWorkCallBackApi(FinalConstant1.SEARCH, "searchindex");
        lodGroupData();
    }

    /**
     * 加载初始化数据
     */
    private void lodGroupData() {
        if (!TextUtils.isEmpty(type)) {
            searchindexApi.addData("type", type);
        }
        if (!TextUtils.isEmpty(hospital_id)) {
            searchindexApi.addData("hospital_id", hospital_id);
        }
        searchindexApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                Log.e(TAG, "s === " + data.toString());
                if ("1".equals(data.code)) {
                    ArrayList<SearchCompositeData> mGroupData = JSONUtil.jsonToArrayList(data.data, SearchCompositeData.class);
                    if (getActivity() != null) {
                        Log.e(TAG, "mGroupData === " + mGroupData.size());
                        setHotData(mGroupData);
                    }
                }
            }
        });
    }

    /**
     * 设置历史记录
     *
     * @param hsdatas
     */
    private void setHistoryData(List<HistorySearchWords> hsdatas) {
        Log.e(TAG, "hsdatas == " + hsdatas);
        Log.e(TAG, "hsdatas == " + hsdatas.size());
        ArrayList<String> hsdatas1 = new ArrayList<>();
        if (hsdatas.size() > 0) {
            mRecord.setVisibility(View.VISIBLE);
            for (int i = 0; i < (hsdatas.size() > 16 ? 16 : hsdatas.size()); i++) {
                hsdatas1.add(hsdatas.get((hsdatas.size() - 1) - i).getHwords());
            }

            Log.e(TAG, "hsdatas1 == " + hsdatas1.size());
            mRecordRecycler.setMaxLine(2);
            SearchInitFlowLayout mSearchInitFlowLayout = new SearchInitFlowLayout(mContext, mRecordRecycler, hsdatas1);

            //历史记录点击事件
            mSearchInitFlowLayout.setClickCallBack(new SearchInitFlowLayout.ClickCallBack() {
                @Override
                public void onClick(View v, int pos, String key) {
                    // 先隐藏键盘
                    Utils.hideSoftKeyboard(mContext);
                    if (onEventClickListener != null) {

                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("to_page_type", "0");
                        hashMap.put("to_page_id", "0");
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.RECENT_SEARCH, (pos + 1) + ""), hashMap, new ActivityTypeData("73"));
                        onEventClickListener.onHistoryClick(v, key);
                    }
                }
            });

            //清除历史记录
            mRecordRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<HistorySearchWords> hsdatas = mKjdb.findAll(HistorySearchWords.class);
                    for (int i = 0; i < hsdatas.size(); i++) {
                        mKjdb.delete(hsdatas.get(i));
                    }
                    mRecord.setVisibility(View.GONE);
                }
            });

        } else {
            mRecord.setVisibility(View.GONE);
        }
    }

    /**
     * 设置热门推荐
     */
    private void setHotData(ArrayList<SearchCompositeData> mGroupData) {
        ScrollLayoutManager linearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setScrollEnable(false);
        mBottomList.setLayoutManager(linearLayoutManager);

        SearchInitAdapter searchInitAdapter = new SearchInitAdapter(mContext, mGroupData);
        mBottomList.setAdapter(searchInitAdapter);
        searchInitAdapter.setOnEventClickListener(new SearchInitAdapter.OnEventClickListener() {
            @Override
            public void onHotClick(View v, String key) {
                if (onEventClickListener != null) {
                    onEventClickListener.onHotClick(v, key);
                }
            }
        });

    }

    //item点击回调
    public interface OnEventClickListener {
        void onHistoryClick(View v, String key);

        void onHotClick(View v, String key);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
