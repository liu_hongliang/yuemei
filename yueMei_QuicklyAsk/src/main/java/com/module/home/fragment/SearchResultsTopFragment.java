package com.module.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.view.YMBaseFragment;
import com.module.doctor.controller.activity.DoctorDetailsActivity592;
import com.module.doctor.controller.activity.HosDetailActivity;
import com.module.home.controller.adapter.SearchResultsTopAdapter;
import com.module.home.model.bean.SearchTitleData;
import com.module.home.model.bean.SearchTitleDataData;
import com.quicklyask.activity.R;

import java.util.List;

import butterknife.BindView;

/**
 * Created by 裴成浩 on 2019/4/17
 */
public class SearchResultsTopFragment extends YMBaseFragment {

    @BindView(R.id.search_results_top_view)
    public LinearLayout mTopView;
    @BindView(R.id.search_results_top_top)
    LinearLayout mTop;
    @BindView(R.id.search_results_top_top_title)
    TextView mTopTitle;
    @BindView(R.id.search_results_top_top_look)
    TextView mTopLook;
    @BindView(R.id.search_results_top_recycler)
    RecyclerView mRecycler;

    private String TAG = "SearchResultsTopFragment";
    private String mType;
    private String mKey;

    public static SearchResultsTopFragment newInstance(SearchTitleData data, String key) {
        SearchResultsTopFragment fragment = new SearchResultsTopFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", data);
        bundle.putString("key", key);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_top_view;
    }

    @Override
    protected void initView(View view) {
        if (getArguments() != null) {
            SearchTitleData mData = getArguments().getParcelable("data");
            mKey = getArguments().getString("key");
            if (mData != null) {
                List<SearchTitleDataData> list = mData.getData().getList();
                mType = mData.getData().getType();

                //判断医生或医院数量是否超过2个
                if (list.size() >= 2) {
                    mTop.setVisibility(View.VISIBLE);
                    if ("doctor".equals(mType)) {
                        mTopTitle.setText("医生");
                    } else {
                        mTopTitle.setText("医院");
                    }

                    //点击事件
                    mTopLook.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onEventClickListener != null) {
                                onEventClickListener.onLookClick(mType);
                            }
                        }
                    });

                } else {
                    mTop.setVisibility(View.GONE);
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                mRecycler.setLayoutManager(linearLayoutManager);
                SearchResultsTopAdapter searchResultsTopAdapter = new SearchResultsTopAdapter(mContext, list, mType, mKey);
                mRecycler.setAdapter(searchResultsTopAdapter);

                Log.e(TAG, "mType == " + mType);
                searchResultsTopAdapter.setOnEventClickListener(new SearchResultsTopAdapter.OnEventClickListener() {
                    @Override
                    public void onItemClick(SearchTitleDataData data, int position) {
                        Log.e(TAG, "");
                        if ("doctor".equals(data.getType())) {
                            Intent intent = new Intent(mContext, DoctorDetailsActivity592.class);
                            intent.putExtra("docId", data.getId());
                            intent.putExtra("docName", data.getName());
                            intent.putExtra("partId", "");
                            startActivity(intent);
                        } else if ("hospital".equals(data.getType())) {
                            Intent intent = new Intent(mContext, HosDetailActivity.class);
                            intent.putExtra("hosid", data.getId());
                            startActivity(intent);
                        }

                    }
                });
            }
        }

    }

    @Override
    protected void initData(View view) {

    }

    public interface OnEventClickListener {
        void onLookClick(String type);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
