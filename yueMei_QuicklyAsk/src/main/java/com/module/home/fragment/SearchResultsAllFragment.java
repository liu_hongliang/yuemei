package com.module.home.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseFragment;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.LodHotIssueDataApi;
import com.module.home.controller.activity.SearchAllActivity668;
import com.module.home.controller.adapter.SearchResultsAllAdapter;
import com.module.home.model.bean.SearchCompositeData;
import com.module.home.model.bean.SearchResultTaoData;
import com.module.other.netWork.SignUtils;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

/**
 * 搜索综合页
 * Created by 裴成浩 on 2019/7/23
 */
public class SearchResultsAllFragment extends YMBaseFragment {

    @BindView(R.id.results_all_recycler_refresh)
    SmartRefreshLayout mRefresh;
    @BindView(R.id.results_all_card_tao_list)
    RecyclerView mList;

    //没有数据View
    @BindView(R.id.search_results_not_view)
    NestedScrollView allNotView;

    private LodHotIssueDataApi lodHotIssueDataApi;
    private String TAG = "SearchResultsAllFragment";
    private int mPage = 1;
    private Gson mGson;
    private String mKey;
    private String mType;
    private SearchResultsAllAdapter mResultsAllAdapter;

    public static SearchResultsAllFragment newInstance(String key, String type) {
        SearchResultsAllFragment fragment = new SearchResultsAllFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        boolean change = isVisibleToUser != getUserVisibleHint();
        super.setUserVisibleHint(isVisibleToUser);
        if (isResumed() && change) {
            if (getUserVisibleHint()) {
                onVisible();
            } else {
                onInvisible();
            }
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            onInvisible();
        } else {
            onVisible();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !isHidden()) {
            onVisible();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !isHidden()) {
            onInvisible();
        }
    }

    private void onVisible() {
        mPage = 1;
        mResultsAllAdapter = null;
        loadingData();
    }

    private void onInvisible() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_results_all_view;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void initView(View view) {

        if(getArguments() != null){
            mKey = getArguments().getString("key");
            mType = getArguments().getString("type");
        }

        //综合页设置
        LinearLayoutManager tagLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mList.setLayoutManager(tagLinearLayoutManager);
        ((DefaultItemAnimator) Objects.requireNonNull(mList.getItemAnimator())).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)

        //刷新加载更多
        mRefresh.setEnableFooterFollowWhenLoadFinished(true);
        mRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPage = 1;
                mResultsAllAdapter = null;
                loadingData();
            }
        });
    }

    @Override
    protected void initData(View view) {
        lodHotIssueDataApi = new LodHotIssueDataApi();
        mGson = new Gson();

        loadingData();
    }


    private void loadingData() {

        lodHotIssueDataApi.getHashMap().clear();
        lodHotIssueDataApi.addData("key", Utils.unicodeEncode(mKey));
        lodHotIssueDataApi.addData("type", mType);
        lodHotIssueDataApi.addData("page", mPage + "");
        if (onEventClickListener != null) {
//                        onEventClickListener.onSearchParaClick(mGson.toJson(SignUtils.buildHttpParamMap(lodHotIssueDataApi.getHashMap())));
            onEventClickListener.onSearchParaClick(mGson.toJson(lodHotIssueDataApi.getHashMap()));
        }
        lodHotIssueDataApi.getCallBack(getActivity(), lodHotIssueDataApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(final ServerData serverData) {
                Log.e(TAG, "JSON1:==" + serverData.data);
                if (getActivity() != null) {
                    setResultData(serverData);

                }
            }
        });
    }

    private void setResultData(ServerData serverData) {
        if ("1".equals(serverData.code)) {
            SearchResultTaoData searchResultTaoData = JSONUtil.TransformSingleBean(serverData.data, SearchResultTaoData.class);
            List<SearchCompositeData> compositeDatas = searchResultTaoData.getCompositeData();

            //刷新加载样式结束
            mRefresh.finishRefresh();
            if (compositeDatas != null && compositeDatas.size() > 0) {

                if ("1".equals(searchResultTaoData.getIsNext())) {
                    mRefresh.finishLoadMore();
                } else {
                    mRefresh.finishLoadMoreWithNoMoreData();
                }

                if (mResultsAllAdapter == null) {
                    mResultsAllAdapter = new SearchResultsAllAdapter(mContext, compositeDatas);
                    mList.setAdapter(mResultsAllAdapter);

                    //点击回调
                    mResultsAllAdapter.setOnEventClickListener(new SearchResultsAllAdapter.OnEventClickListener() {
                        @Override
                        public void onSearchKeyClick(String key) {
                            if (onEventClickListener != null) {
                                onEventClickListener.onSearchKeyClick(key);
                            }
                        }

                        @Override
                        public void onSearchMoreClick(String type, HashMap<String, String> event_params) {
                            if (onEventClickListener != null) {
                                Log.e(TAG, "event_params == " + event_params);
                                YmStatistics.getInstance().tongjiApp(event_params);
                                onEventClickListener.onSearchMoreClick(type);
                            }
                        }

                    });
                } else {
                    mResultsAllAdapter.addData(compositeDatas);
                }
                //页码+1
                mPage++;

            } else {
                if (mPage == 1) {
                    allNotView.setVisibility(View.VISIBLE);
                    mRefresh.setVisibility(View.GONE);
                } else {
                    allNotView.setVisibility(View.GONE);
                    mRefresh.setVisibility(View.VISIBLE);
                    mRefresh.finishLoadMoreWithNoMoreData();
                }
            }
        } else {
            allNotView.setVisibility(View.VISIBLE);
            mRefresh.setVisibility(View.GONE);
        }
    }

    public interface OnEventClickListener {
        void onSearchParaClick(String para);

        void onSearchKeyClick(String key);

        void onSearchMoreClick(String type);
    }

    private OnEventClickListener onEventClickListener;

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
