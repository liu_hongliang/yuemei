package com.module.shopping.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.my.model.api.TiXianCodeApi;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;

import java.util.HashMap;

/**
 * 输入验证码的
 * Created by 裴成浩 on 2018/12/20
 */
public class VerificationCodeDialog extends Dialog implements android.view.View.OnClickListener {
    private ImageButton mCancel;
    private VerificationCodeView mCodeEt;
    private TextView mObtainButton;
    private LinearLayout mCodeButtonClick;
    private TextView mCodeAgainButton;
    private Context mContext;
    private String TAG = "VerificationCodeDialog";
    private String codeContent = "";

    public VerificationCodeDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 指定布局
        setContentView(R.layout.verification_code_dialog);

        // 根据id在布局中找到控件对象
        mCancel = findViewById(R.id.verification_code_cancel);
        mCodeEt = findViewById(R.id.verification_code_et);
        mObtainButton = findViewById(R.id.verification_obtain_code_button);
        mCodeButtonClick = findViewById(R.id.verification_again_code_button_click);
        mCodeAgainButton = findViewById(R.id.verification_again_code_button);

        setCanceledOnTouchOutside(false);

        initView();
    }

    private void initView() {
        mCancel.setOnClickListener(this);
        mObtainButton.setOnClickListener(this);
        mCodeAgainButton.setOnClickListener(this);

        //验证码
        mCodeEt.setOnCodeFinishListener(new VerificationCodeView.OnCodeFinishListener() {
            @Override
            public void onComplete(String content) {
                codeContent = content;
                Log.e(TAG, "content == " + content);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verification_code_cancel:                 //关闭
                dismiss();
                break;
            case R.id.verification_again_code_button:           //重新获取验证码
            case R.id.verification_obtain_code_button:          //获取验证码
                getCode();
                break;
        }
    }

    /**
     * 获取验证码
     */
    private void getCode() {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put("code", codeContent);
        new TiXianCodeApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData s) {
                if ("1".equals(s.code)) {
                    new CountDownTimer(120000, 1000) {// 两个参数，前一个指倒计时的总时间，后一个指多长时间倒数一下。

                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTick(long millisUntilFinished) {
                            mObtainButton.setVisibility(View.VISIBLE);
                            mCodeButtonClick.setVisibility(View.GONE);
                            mObtainButton.setText("(" + millisUntilFinished / 1000 + ")重新获取");
                        }

                        @Override
                        public void onFinish() {
                            mObtainButton.setVisibility(View.GONE);
                            mCodeButtonClick.setVisibility(View.VISIBLE);
                        }
                    }.start();
                }
            }
        });
    }

    /**
     * 获取输入的验证码
     *
     * @return
     */
    public String getCodeContent() {
        return codeContent;
    }
}
