package com.module.shopping.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.quicklyask.activity.R;

/**
 * 颜值币提示弹层
 * Created by 裴成浩 on 2019/7/15
 */
public class MakeSureCoinTipDialog extends Dialog {

    private Context mContext;
    private String title;

    public MakeSureCoinTipDialog(Context context, String title) {
        super(context, R.style.MyDialog1);
        this.mContext = context;
        this.title = title;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.make_sure_coin_tip_dialog, null, false);

        onClickListener(view);
        setContentView(view);           //这行一定要写在前面

        setCancelable(false);           //点击外部是否可以dismiss
        setCanceledOnTouchOutside(true);
        Window window = this.getWindow();
        window.setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
    }


    /**
     * 监听设置
     *
     * @param view
     */
    private void onClickListener(View view) {
        ImageView mClose = view.findViewById(R.id.make_sure_coin_tip_close);
        TextView mTitle = view.findViewById(R.id.make_sure_coin_tip_title);
        TextView mBtn = view.findViewById(R.id.make_sure_coin_tip_btn);

        mTitle.setText(title);

        //确定关闭
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });

        //关闭
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downDialog();
            }
        });
    }


    /**
     * 关闭
     */
    private void downDialog() {
        dismiss();
    }
}
