package com.module.shopping.model.bean;

import com.module.taodetail.model.bean.HomeTaoData;

/**
 * 购物车医院下的SKU详情页面
 * Created by 裴成浩 on 2018/11/21.
 */
public class ShoppingCartSkuData {
    private String cart_id;                     //购物车Id
    private String tao_id;                      //taoid
    private String price;                       //加入时的价格
    private String number;                      //数量
    private String hos_id;                      //医院id
    private String selected;                    //是否选中 1是
    private ShopCarTaoData tao;                    //陶整形数据

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getTao_id() {
        return tao_id;
    }

    public void setTao_id(String tao_id) {
        this.tao_id = tao_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getHos_id() {
        return hos_id;
    }

    public void setHos_id(String hos_id) {
        this.hos_id = hos_id;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public ShopCarTaoData getTao() {
        return tao;
    }

    public void setTao(ShopCarTaoData tao) {
        this.tao = tao;
    }
}
