package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.ShoppingCartData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 删除购物车数据
 * Created by 裴成浩 on 2018/12/3
 */
public class ShoppingCartDeleteApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartApi";
    private HashMap<String, Object> mShoppingCartDeleteHashMap;  //传值容器

    public ShoppingCartDeleteApi() {
        mShoppingCartDeleteHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "delinvalidcart", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData = " + mData.toString());
                if ("1".equals(mData.code)) {
                    try {
                        List<ShoppingCartData> shoppingCartData = JSONUtil.jsonToArrayList(mData.data, ShoppingCartData.class);
                        listener.onSuccess(shoppingCartData);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public HashMap<String, Object> getmShoppingCartDeleteHashMap() {
        return mShoppingCartDeleteHashMap;
    }

    public void addData(String key, String value) {
        mShoppingCartDeleteHashMap.put(key, value);
    }
}
