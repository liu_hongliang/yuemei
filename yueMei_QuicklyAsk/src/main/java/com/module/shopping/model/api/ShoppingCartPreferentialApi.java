package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.ShoppingCartPreferentialData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订金劵获取
 * Created by 裴成浩 on 2018/12/10
 */
public class ShoppingCartPreferentialApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartPreferentialApi";
    private HashMap<String, Object> mHashMap;  //传值容器

    public ShoppingCartPreferentialApi() {
        mHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "getcouponsbycart", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                Log.e(TAG, "mData = " + mData.toString());
                List<ShoppingCartPreferentialData> data = null;
                if ("1".equals(mData.code)) {
                    try {
                       data = JSONUtil.jsonToArrayList(mData.data, ShoppingCartPreferentialData.class);
                    } catch (Exception e) {
                        Log.e(TAG, "e == " + e.toString());
                        e.printStackTrace();
                    }
                    listener.onSuccess(data);
                }
            }
        });
    }

    public HashMap<String, Object> getHashMap() {
        return mHashMap;
    }

    public void addData(String key, String value) {
        mHashMap.put(key, value);
    }
}
