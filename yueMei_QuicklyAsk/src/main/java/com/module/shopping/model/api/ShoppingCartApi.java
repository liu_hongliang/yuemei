package com.module.shopping.model.api;

import android.content.Context;
import android.util.Log;

import com.module.base.api.BaseCallBackApi;
import com.module.base.api.BaseCallBackListener;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.NetWork;
import com.module.other.netWork.netWork.ServerCallback;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.bean.ShoppingCartData;
import com.quicklyask.util.JSONUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取购物车信息
 * Created by 裴成浩 on 2018/12/3
 */
public class ShoppingCartApi implements BaseCallBackApi {
    private String TAG = "ShoppingCartApi";
    private HashMap<String, Object> mShoppingCartHashMap;  //传值容器

    public ShoppingCartApi() {
        mShoppingCartHashMap = new HashMap<>();
    }

    @Override
    public void getCallBack(Context context, Map<String, Object> maps, final BaseCallBackListener listener) {
        NetWork.getInstance().call(FinalConstant1.CART, "getcart", maps, new ServerCallback() {
            @Override
            public void onServerCallback(ServerData mData) {
                    listener.onSuccess(mData);
            }
        });
    }

    public HashMap<String, Object> getmShoppingCartHashMap() {
        return mShoppingCartHashMap;
    }

    public void addData(String key, String value) {
        mShoppingCartHashMap.put(key, value);
    }
}
