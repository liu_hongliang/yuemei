package com.module.shopping.model.bean;

/**
 * 购物车颜值币数据
 * Created by 裴成浩 on 2019/7/12
 */
public class MakeSureOrderCoinData {
    private String isShowDeduction;                 //是否显示抵扣开关 0: 不显示 1: 显示
    private String isCanUse;                        //是否可以使用 0: 不可开启使用 1: 可以开启使用
    private String deductionDesc;                   //抵扣文案
    private String notUesNotice;                    //不能使用抵扣弹层说明(如果存在则显示提示)
    private String canDeductionAmount;              //可以使用抵扣颜值币数量
    private String deductionMoney;                  //抵扣金额
    private String yanCoinUesDesc;                  //抵扣使用说明（弹层文案）

    public String getIsShowDeduction() {
        return isShowDeduction;
    }

    public void setIsShowDeduction(String isShowDeduction) {
        this.isShowDeduction = isShowDeduction;
    }

    public String getIsCanUse() {
        return isCanUse;
    }

    public void setIsCanUse(String isCanUse) {
        this.isCanUse = isCanUse;
    }

    public String getDeductionDesc() {
        return deductionDesc;
    }

    public void setDeductionDesc(String deductionDesc) {
        this.deductionDesc = deductionDesc;
    }

    public String getNotUesNotice() {
        return notUesNotice;
    }

    public void setNotUesNotice(String notUesNotice) {
        this.notUesNotice = notUesNotice;
    }

    public String getCanDeductionAmount() {
        return canDeductionAmount;
    }

    public void setCanDeductionAmount(String canDeductionAmount) {
        this.canDeductionAmount = canDeductionAmount;
    }

    public String getDeductionMoney() {
        return deductionMoney;
    }

    public void setDeductionMoney(String deductionMoney) {
        this.deductionMoney = deductionMoney;
    }

    public String getYanCoinUesDesc() {
        return yanCoinUesDesc;
    }

    public void setYanCoinUesDesc(String yanCoinUesDesc) {
        this.yanCoinUesDesc = yanCoinUesDesc;
    }
}
