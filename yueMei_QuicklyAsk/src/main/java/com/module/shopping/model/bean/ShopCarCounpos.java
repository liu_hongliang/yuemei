package com.module.shopping.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class ShopCarCounpos implements Parcelable {
    private String money;
    private String use_text;
    private String is_get;
    private String use_url;
    private String receive_url;

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getUse_text() {
        return use_text;
    }

    public void setUse_text(String use_text) {
        this.use_text = use_text;
    }

    public String getIs_get() {
        return is_get;
    }

    public void setIs_get(String is_get) {
        this.is_get = is_get;
    }

    public String getUse_url() {
        return use_url;
    }

    public void setUse_url(String use_url) {
        this.use_url = use_url;
    }

    public String getReceive_url() {
        return receive_url;
    }

    public void setReceive_url(String receive_url) {
        this.receive_url = receive_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.money);
        dest.writeString(this.use_text);
        dest.writeString(this.is_get);
        dest.writeString(this.use_url);
        dest.writeString(this.receive_url);
    }

    public ShopCarCounpos() {
    }

    protected ShopCarCounpos(Parcel in) {
        this.money = in.readString();
        this.use_text = in.readString();
        this.is_get = in.readString();
        this.use_url = in.readString();
        this.receive_url = in.readString();
    }

    public static final Parcelable.Creator<ShopCarCounpos> CREATOR = new Parcelable.Creator<ShopCarCounpos>() {
        @Override
        public ShopCarCounpos createFromParcel(Parcel source) {
            return new ShopCarCounpos(source);
        }

        @Override
        public ShopCarCounpos[] newArray(int size) {
            return new ShopCarCounpos[size];
        }
    };
}
