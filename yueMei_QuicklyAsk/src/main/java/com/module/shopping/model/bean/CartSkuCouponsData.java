package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/3
 */
public class CartSkuCouponsData {
    private String money;                           //价格
    private String card_id;                         //购物车id
    private String coupons_id;                      //优惠劵id
    private String lowest_consumption;              //优惠券最低使用限制
    private String title;                           //显示标题
    private String search_title;                    //凑单
    private String search_url;                      //凑单链接

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getCoupons_id() {
        return coupons_id;
    }

    public void setCoupons_id(String coupons_id) {
        this.coupons_id = coupons_id;
    }

    public String getLowest_consumption() {
        return lowest_consumption;
    }

    public void setLowest_consumption(String lowest_consumption) {
        this.lowest_consumption = lowest_consumption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearch_title() {
        return search_title;
    }

    public void setSearch_title(String search_title) {
        this.search_title = search_title;
    }

    public String getSearch_url() {
        return search_url;
    }

    public void setSearch_url(String search_url) {
        this.search_url = search_url;
    }
}
