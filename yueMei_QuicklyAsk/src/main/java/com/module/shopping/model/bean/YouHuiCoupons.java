package com.module.shopping.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 裴成浩 on 2018/12/13
 */
public class YouHuiCoupons implements Parcelable {
    private String card_id;
    private String lowest_consumption;
    private String title;
    private String money;
    private String couponsType;
    private String mankeyong;
    private String shiyongtiaojian;
    private String time;

    public YouHuiCoupons() {
    }

    protected YouHuiCoupons(Parcel in) {
        card_id = in.readString();
        lowest_consumption = in.readString();
        title = in.readString();
        money = in.readString();
        couponsType = in.readString();
        mankeyong = in.readString();
        shiyongtiaojian = in.readString();
        time = in.readString();
    }

    public static final Creator<YouHuiCoupons> CREATOR = new Creator<YouHuiCoupons>() {
        @Override
        public YouHuiCoupons createFromParcel(Parcel in) {
            return new YouHuiCoupons(in);
        }

        @Override
        public YouHuiCoupons[] newArray(int size) {
            return new YouHuiCoupons[size];
        }
    };

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getLowest_consumption() {
        return lowest_consumption;
    }

    public void setLowest_consumption(String lowest_consumption) {
        this.lowest_consumption = lowest_consumption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCouponsType() {
        return couponsType;
    }

    public void setCouponsType(String couponsType) {
        this.couponsType = couponsType;
    }

    public String getMankeyong() {
        return mankeyong;
    }

    public void setMankeyong(String mankeyong) {
        this.mankeyong = mankeyong;
    }

    public String getShiyongtiaojian() {
        return shiyongtiaojian;
    }

    public void setShiyongtiaojian(String shiyongtiaojian) {
        this.shiyongtiaojian = shiyongtiaojian;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(card_id);
        parcel.writeString(lowest_consumption);
        parcel.writeString(title);
        parcel.writeString(money);
        parcel.writeString(couponsType);
        parcel.writeString(mankeyong);
        parcel.writeString(shiyongtiaojian);
        parcel.writeString(time);
    }
}
