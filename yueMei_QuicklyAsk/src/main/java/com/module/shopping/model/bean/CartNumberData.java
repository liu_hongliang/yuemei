package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/7
 */
public class CartNumberData {
    private int hosPos;             //医院下标
    private int skuPos;             //SKU下标
    private String cart_id;         //购物车SKU的id
    private String flag;            //1加，2减，直接修改
    private int value = -1;            //直接修改的值

    public int getHosPos() {
        return hosPos;
    }

    public void setHosPos(int hosPos) {
        this.hosPos = hosPos;
    }

    public int getSkuPos() {
        return skuPos;
    }

    public void setSkuPos(int skuPos) {
        this.skuPos = skuPos;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
