package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/10
 */
public class ShoppingCartPreferentialData {
    private String card_id;                         //购物车id
    private String lowest_consumption;              //最低消费
    private String title;                           //标题
    private String money;                           //金额
    private String couponsType;                     //数量
    private String mankeyong;                       //满可用
    private String shiyongtiaojian;                 //使用条件
    private String time;                            //时间
    private String end_time;                            //结束时间


    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getLowest_consumption() {
        return lowest_consumption;
    }

    public void setLowest_consumption(String lowest_consumption) {
        this.lowest_consumption = lowest_consumption;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCouponsType() {
        return couponsType;
    }

    public void setCouponsType(String couponsType) {
        this.couponsType = couponsType;
    }

    public String getMankeyong() {
        return mankeyong;
    }

    public void setMankeyong(String mankeyong) {
        this.mankeyong = mankeyong;
    }

    public String getShiyongtiaojian() {
        return shiyongtiaojian;
    }

    public void setShiyongtiaojian(String shiyongtiaojian) {
        this.shiyongtiaojian = shiyongtiaojian;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
