package com.module.shopping.model.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ShopCarBean implements Parcelable {
    private List<ShoppingCartData> tao_list;
    private ShopCarCounpos coupons;

    public List<ShoppingCartData> getTao_list() {
        return tao_list;
    }

    public void setTao_list(List<ShoppingCartData> tao_list) {
        this.tao_list = tao_list;
    }

    public ShopCarCounpos getCoupons() {
        return coupons;
    }

    public void setCoupons(ShopCarCounpos coupons) {
        this.coupons = coupons;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.tao_list);
        dest.writeParcelable(this.coupons, flags);
    }

    public ShopCarBean() {
    }

    protected ShopCarBean(Parcel in) {
        this.tao_list = new ArrayList<ShoppingCartData>();
        in.readList(this.tao_list, ShoppingCartData.class.getClassLoader());
        this.coupons = in.readParcelable(ShopCarCounpos.class.getClassLoader());
    }

    public static final Parcelable.Creator<ShopCarBean> CREATOR = new Parcelable.Creator<ShopCarBean>() {
        @Override
        public ShopCarBean createFromParcel(Parcel source) {
            return new ShopCarBean(source);
        }

        @Override
        public ShopCarBean[] newArray(int size) {
            return new ShopCarBean[size];
        }
    };
}
