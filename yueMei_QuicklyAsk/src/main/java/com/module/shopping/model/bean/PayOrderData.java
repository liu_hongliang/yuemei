package com.module.shopping.model.bean;

/**
 * Created by 裴成浩 on 2018/12/19
 */
public class PayOrderData {
    private String server_id;					//服务码
    private String order_id;					//订单号
    private String is_insure;					//是否参保 1是
    private String is_group;					//是否是拼团 1是
    private String group_id;					//团id
    private String order_time;					//订单时间
    private String money;						//需要支付金额
    private String is_repayment;				//是否支持信用卡分期支付
    private String is_repayment_mimo;			//是否支持米么支付
    private String total_price;                 //订单gvm
    private String number;                      //订单数量

    public String getServer_id() {
        return server_id;
    }

    public void setServer_id(String server_id) {
        this.server_id = server_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getIs_insure() {
        return is_insure;
    }

    public void setIs_insure(String is_insure) {
        this.is_insure = is_insure;
    }

    public String getIs_group() {
        return is_group;
    }

    public void setIs_group(String is_group) {
        this.is_group = is_group;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getIs_repayment() {
        return is_repayment;
    }

    public void setIs_repayment(String is_repayment) {
        this.is_repayment = is_repayment;
    }

    public String getIs_repayment_mimo() {
        return is_repayment_mimo;
    }

    public void setIs_repayment_mimo(String is_repayment_mimo) {
        this.is_repayment_mimo = is_repayment_mimo;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
