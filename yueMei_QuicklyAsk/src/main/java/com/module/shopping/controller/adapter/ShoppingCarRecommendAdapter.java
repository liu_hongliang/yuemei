package com.module.shopping.controller.adapter;

import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.FunctionManager;
import com.module.commonview.module.api.SkuAddcarApi;
import com.module.other.netWork.imageLoaderUtil.GlidePartRoundTransform;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.model.api.CartSkuNumberApi;
import com.module.shopping.model.bean.CartSkuNumberData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.module.taodetail.model.bean.Promotion;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;

import org.apache.commons.collections.CollectionUtils;
import org.greenrobot.eventbus.EventBus;
import org.xutils.view.annotation.Event;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;

public class ShoppingCarRecommendAdapter extends BaseQuickAdapter<HomeTaoData, BaseViewHolder> {
    public ShoppingCarRecommendAdapter(int layoutResId, @Nullable List<HomeTaoData> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, final HomeTaoData tao) {
        Glide.with(mContext).load(tao.getImg())
                .bitmapTransform(new GlidePartRoundTransform(mContext, Utils.dip2px(5), GlidePartRoundTransform.CornerType.TOP))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into((ImageView) helper.getView(R.id.staggered_common_img));
        String bilateralCoupons = tao.getBilateral_coupons();
        String bilateralDesc = tao.getHospital_top().getBilateral_desc();
        String level = tao.getHospital_top().getLevel();
        List<Promotion> promotion = tao.getPromotion();
        if (CollectionUtils.isNotEmpty(promotion)) {
            for (int i = 0; i < promotion.size(); i++) {
                Promotion promotion1 = (Promotion) promotion.get(i);
                String styleType = promotion1.getStyle_type();
                if ("2".equals(styleType) && TextUtils.isEmpty(bilateralDesc)) {
                    helper.setGone(R.id.item_nearlook, true);
                    break;
                } else {
                    helper.setGone(R.id.item_nearlook, false);
                }
            }
        } else {
            String isUserBrowse = tao.getIs_user_browse();
            if ("1".equals(isUserBrowse)){
                helper.setGone(R.id.item_nearlook, true);
            }else {
                helper.setGone(R.id.item_nearlook, false);
            }
        }
        if (!TextUtils.isEmpty(bilateralDesc) && !"0".equals(level)) {
            helper.setGone(R.id.tao_staggered_tagcontianer, true);
            helper.setGone(R.id.item_coupons, false);
            helper.setText(R.id.tao_staggered_levle,"N0."+level);
            helper.setText(R.id.tao_staggered_tagtitle, bilateralDesc);
        } else {
            helper.setGone(R.id.tao_staggered_tagcontianer, false);
            if (!TextUtils.isEmpty(bilateralCoupons)) {
                helper.setGone(R.id.item_coupons, true);
                helper.setText(R.id.item_coupons, bilateralCoupons);
            } else {
                helper.setGone(R.id.item_coupons, false);
            }
        }
        String highlightTitle = tao.getHighlight_title();
        if (!TextUtils.isEmpty(highlightTitle)) {
            try {
                Log.e(TAG, "highlightTitleOrgin ==" + highlightTitle);
                String htmlTitle = URLDecoder.decode(highlightTitle.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "utf-8");
                Log.e(TAG, "highlightTitle ==" + htmlTitle);
                //设置标题
                helper.setText(R.id.staggered_common_title, Html.fromHtml(htmlTitle));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
//                    holder.mCommonTxt.setText("<" + tao.getTitle() + "> " + tao.getSubtitle());
//        String appSlideLogo = tao.getApp_slide_logo();
//        if (!TextUtils.isEmpty(appSlideLogo)) {
//            helper.setGone(R.id.staggered_promotton_img, true);
//            Glide.with(mContext).load(appSlideLogo).into((ImageView) helper.getView(R.id.staggered_promotton_img));
//        } else {
//            helper.setGone(R.id.staggered_promotton_img, false);
//        }
        helper.setGone(R.id.staggered_shopping_car,true);
        ImageView shoppingCarImg = helper.getView(R.id.staggered_shopping_car);
        shoppingCarImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> maps2 = new HashMap<>();
//                maps2.put("source", source);
//                maps2.put("objid", objid);
//                if (!TextUtils.isEmpty(mU)) {
//                    maps2.put("u", mU);

                maps2.put("tao_id", tao.getId());
                maps2.put("referer", "tao/tao");

                new SkuAddcarApi().getCallBack(mContext, maps2, new BaseCallBackListener<ServerData>() {
                    @Override
                    public void onSuccess(ServerData serverData) {
                        if ("1".equals(serverData.code)) {
                            CartSkuNumberApi cartSkuNumberApi = new CartSkuNumberApi();
                            cartSkuNumberApi.getCallBack(mContext, cartSkuNumberApi.getmCartSkuNumberHashMap(), new BaseCallBackListener<CartSkuNumberData>() {
                                @Override
                                public void onSuccess(CartSkuNumberData data) {
                                    if (Integer.parseInt(data.getCart_number()) >= 100) {
                                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, "99+");
                                    } else {
                                        Cfg.saveStr(mContext, FinalConstant.CART_NUMBER, data.getCart_number());
                                    }
                                    EventBus.getDefault().post("refresh");
                                }
                            });
                        }
                        new FunctionManager(mContext).showShort(serverData.message);
                    }
                });
            }
        });
        helper.setText(R.id.staggered_common_price, tao.getPrice_discount());
        String doc_name = tao.getDoc_name();
        String hos_name = tao.getHos_name();
        helper.setText(R.id.staggered_common_docname, doc_name + "  " + hos_name);
        helper.setText(R.id.staggered_common_distance, tao.getDistance());

    }


}
