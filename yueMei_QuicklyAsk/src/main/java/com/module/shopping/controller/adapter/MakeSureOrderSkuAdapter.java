package com.module.shopping.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.module.commonview.view.NumberAddSubView;
import com.module.shopping.model.bean.InsuranceCheckedData;
import com.module.shopping.model.bean.MakeSureOrderSkuNumberTao;
import com.module.taodetail.model.bean.HomeTaoInsureData;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.MyToast;

import java.util.List;

/**
 * Created by 裴成浩 on 2018/12/17
 */
public class MakeSureOrderSkuAdapter extends RecyclerView.Adapter<MakeSureOrderSkuAdapter.ViewHolder> {

    private Activity mContext;
    private List<MakeSureOrderSkuNumberTao> mDatas;
    private String mBuyForCart;
    private LayoutInflater mInflater;
    private String TAG = "MakeSureOrderSkuAdapter";

    public MakeSureOrderSkuAdapter(Activity context, String buyForCart, List<MakeSureOrderSkuNumberTao> datas) {
        this.mContext = context;
        this.mBuyForCart = buyForCart;
        this.mDatas = datas;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public MakeSureOrderSkuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View inflate = mInflater.inflate(R.layout.make_sure_order_sku_view, parent, false);
        return new MakeSureOrderSkuAdapter.ViewHolder(inflate);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MakeSureOrderSkuAdapter.ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.mSkuView.getLayoutParams();
        if (pos != 0 && pos != mDatas.size() - 1) {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(5);
        }
        viewHolder.mSkuView.setLayoutParams(layoutParams);

        MakeSureOrderSkuNumberTao taoData = mDatas.get(pos);

        int priceDiscount = Integer.parseInt(taoData.getPrice_discount());      //医院价格
        int memberPrice = Integer.parseInt(taoData.getMember_price());          //会员价

        int skuNumber = Integer.parseInt(taoData.getNumber());                  //购买数量
        int minSkuNumber = Integer.parseInt(taoData.getMin_number());       //最少购买数量
        int maxSkuNumber = Integer.parseInt(taoData.getMax_number());        //最多购买数量
        Log.e(TAG, "skuNumber == " + skuNumber);
        Log.e(TAG, "startSkuNumber == " + minSkuNumber);
        Log.e(TAG, "maxSkuNumber == " + maxSkuNumber);

        //图片加载
        Glide.with(mContext).load(taoData.getImg()).into(viewHolder.mSkuImg);
        //SKU描述
        viewHolder.mSkuName.setText(taoData.getTitle());
        //医生
        viewHolder.mSkuDoc.setText(taoData.getDoc_name());
        //购买数量
        if (isBuyForCart()) {
            viewHolder.mNumber.setVisibility(View.VISIBLE);
            viewHolder.mNumberAddSub.setVisibility(View.GONE);
            viewHolder.mNumber.setText("×" + skuNumber);
        } else {
            viewHolder.mNumber.setVisibility(View.GONE);
            viewHolder.mNumberAddSub.setVisibility(View.VISIBLE);
            viewHolder.mNumberAddSub.setValue(skuNumber);
            viewHolder.mNumberAddSub.setMinValue(minSkuNumber);
            viewHolder.mNumberAddSub.setMaxValue(maxSkuNumber);
        }

        //判断会员价是否小于悦美价
        boolean isVip = memberPrice > 0 && memberPrice < priceDiscount;
        setPrice(isVip, taoData, viewHolder);

        //保险
        HomeTaoInsureData baoxian = taoData.getInsure();
        if ("1".equals(baoxian.getInsure_pay_money())) {
            viewHolder.mInsurance.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mInsurance.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void setPrice(boolean isVip, MakeSureOrderSkuNumberTao taoData, ViewHolder viewHolder) {

        //价格
        TextView price = new TextView(mContext);
        price.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
        price.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        price.setPadding(0,Utils.dip2px(3),0,0);
        price.setText("¥" + taoData.getPrice_discount());

        //单位
        TextView unit = new TextView(mContext);
        unit.setTextColor(Utils.getLocalColor(mContext, R.color.red_ff4965));
        unit.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
        unit.setPadding(0,Utils.dip2px(6),0,0);
        unit.setText(taoData.getFeeScale());

        viewHolder.mFlowLayout.addView(price);
        viewHolder.mFlowLayout.addView(unit);

        if (isVip) {
            TextView vipPrice = new TextView(mContext);
            vipPrice.setTextColor(Utils.getLocalColor(mContext, R.color._33));
            vipPrice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

            Drawable nav_up = Utils.getLocalDrawable(mContext, R.drawable.plus_biaos2);
            nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());
            vipPrice.setCompoundDrawables(null, null, nav_up, null);
            vipPrice.setCompoundDrawablePadding(Utils.dip2px(3));
            vipPrice.setPadding(0,Utils.dip2px(1),0,0);
            vipPrice.setText("¥" + taoData.getMember_price());

            viewHolder.mFlowLayout.addView(vipPrice);
        }
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mSkuView;          //item容器
        ImageView mSkuImg;              //SKU图片
        TextView mSkuName;              //SKU描述
        TextView mSkuDoc;               //SKU医生
        FlowLayout mFlowLayout;               //购买数量
        TextView mNumber;               //购买数量
        NumberAddSubView mNumberAddSub; //购买数量加减器
        LinearLayout mInsurance;        //保险容器
        Switch mOrderSwitch;            //是否购买保险开关

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mSkuView = itemView.findViewById(R.id.make_sure_order_sku_view);
            mSkuImg = itemView.findViewById(R.id.make_sure_order_sku_img);
            mSkuName = itemView.findViewById(R.id.make_sure_order_sku_name);
            mSkuDoc = itemView.findViewById(R.id.make_sure_order_sku_doc);
            mFlowLayout = itemView.findViewById(R.id.make_sure_order_sku_vip_price);
            mNumber = itemView.findViewById(R.id.make_sure_order_sku_number);
            mNumberAddSub = itemView.findViewById(R.id.make_sure_order_sku_add_sub);
            mInsurance = itemView.findViewById(R.id.make_sure_order_sku_insurance);
            mOrderSwitch = itemView.findViewById(R.id.make_sure_order_switch);

            mOrderSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (onEventClickListener != null) {
                        InsuranceCheckedData data = new InsuranceCheckedData();
                        data.setSkuPos(getLayoutPosition());
                        data.setPrice(Float.parseFloat(mDatas.get(getLayoutPosition()).getInsure().getInsure_pay_money()));
                        if (isChecked) {
                            //打开的
                            data.setChecked(true);
                            mDatas.get(getLayoutPosition()).getInsure().setBaoxianOpen(true);
                        } else {
                            data.setChecked(false);
                            mDatas.get(getLayoutPosition()).getInsure().setBaoxianOpen(false);
                        }

                        onEventClickListener.onInsuranceClick(data);
                    }
                }
            });

            //不是购物车过来的
            if (!isBuyForCart()) {
                //数量加减
                mNumberAddSub.setOnButtonClickListenter(new NumberAddSubView.OnButtonClickListenter() {
                    @Override
                    public void onButtonAddClick(int value, boolean b) {
                        if (b) {
                            if (onEventClickListener != null) {
                                mDatas.get(getLayoutPosition()).setNumber(value + "");
                                onEventClickListener.onNumberClick(value);
                            }
                        } else {
                            int number = Integer.parseInt(mDatas.get(getLayoutPosition()).getMax_number());                    //最高限购
                            MyToast.makeTextToast2(mContext, "不能再增加啦，最高限购" + number + "件", MyToast.SHOW_TIME).show();
                        }
                    }

                    @Override
                    public void onTextViewClick(int value) {
                        if (onEventClickListener != null) {
                            mDatas.get(getLayoutPosition()).setNumber(value + "");
                            onEventClickListener.onNumberClick(value);
                        }
                    }

                    @Override
                    public void onButtonSubClick(int value, boolean b) {
                        if (b) {
                            if (onEventClickListener != null) {
                                mDatas.get(getLayoutPosition()).setNumber(value + "");
                                onEventClickListener.onNumberClick(value);
                            }
                        } else {
                            int startNumber = Integer.parseInt(mDatas.get(getLayoutPosition()).getStart_number());
                            if (startNumber >= 1) {
                                MyToast.makeTextToast2(mContext, "不能再减少啦，最低" + startNumber + "件起预订", MyToast.SHOW_TIME).show();
                            } else {
                                MyToast.makeTextToast2(mContext, "不能再减少啦", MyToast.SHOW_TIME).show();
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * 获取医院下所有开通保险的总金额
     */
    public float getHosInsurance() {
        float price = 0;
        for (MakeSureOrderSkuNumberTao data : mDatas) {
            HomeTaoInsureData insure = data.getInsure();
            if (insure.isBaoxianOpen()) {
                price += Float.parseFloat(insure.getInsure_pay_money());
            }
        }
        return price;
    }


    /**
     * 判断是否是从购物车购买的
     *
     * @return ：1购物车
     */
    private boolean isBuyForCart() {
        return "1".equals(mBuyForCart);
    }

    //回调
    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onInsuranceClick(InsuranceCheckedData data);

        void onNumberClick(int value);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }
}
