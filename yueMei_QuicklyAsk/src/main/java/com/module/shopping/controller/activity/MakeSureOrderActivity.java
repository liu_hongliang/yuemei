package com.module.shopping.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.module.base.api.BaseCallBackListener;
import com.module.base.api.BaseNetWorkCallBackApi;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.SpeltActivity;
import com.module.commonview.utils.StatisticalManage;
import com.module.commonview.view.PayOrderDetailsDialog;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.my.controller.activity.InSureActivity;
import com.module.my.controller.activity.OrderPreferentialActivity639;
import com.module.my.model.api.InsuranceMessageApi;
import com.module.my.model.bean.BaoXianData;
import com.module.my.view.orderpay.OrderMethodActivity594;
import com.module.my.view.orderpay.OrderPhoneModifyActivity;
import com.module.my.view.orderpay.OrderZhiFuStatus1Activity;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.adapter.MakeSureOrderAdapter;
import com.module.shopping.model.api.MakeSureOrderApi;
import com.module.shopping.model.api.PayOrderApi;
import com.module.shopping.model.bean.BalancePaymentData;
import com.module.shopping.model.bean.DingjinCouponsRank;
import com.module.shopping.model.bean.InsuranceCheckedData;
import com.module.shopping.model.bean.MakeSureOrderCoinData;
import com.module.shopping.model.bean.MakeSureOrderData;
import com.module.shopping.model.bean.MakeSureOrderSkuNumberData;
import com.module.shopping.model.bean.MakeSureOrderSkuNumberTao;
import com.module.shopping.model.bean.MakeSureOrderTaomember;
import com.module.shopping.model.bean.PayOrderData;
import com.module.shopping.model.bean.PayOrderTao;
import com.module.shopping.model.bean.PayOrderTaoSku;
import com.module.shopping.model.bean.YouHuiCoupons;
import com.module.shopping.view.MakeSureCoinTipDialog;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.view.WalletSecurityCodePop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 确认订单页
 *
 * @author 裴成浩
 */
public class MakeSureOrderActivity extends YMBaseActivity {

    @BindView(R.id.activity_make_sure_order)
    LinearLayout mMakeSureOrder;                    //父容器
    @BindView(R.id.make_sure_order_phone_click)
    LinearLayout mOrderPhoneClick;                  //电话点击
    @BindView(R.id.make_sure_order_phone)
    TextView mOrderPhone;                           //电话
    @BindView(R.id.make_sure_order_recycler)
    RecyclerView mOrderRecycler;                    //订单列表

    @BindView(R.id.make_sure_order_insurance)
    LinearLayout mOrderInsurance;                   //保险容器
    @BindView(R.id.make_sure_order_insurance_price)
    TextView mOrderInsurancePrice;                  //保险价格
    @BindView(R.id.make_sure_order_insurance_information)
    TextView mOrderInformation;                      //保险信息

    @BindView(R.id.make_sure_order_open_vip)
    LinearLayout mOpenVip;                          //是否开通会员容器
    @BindView(R.id.make_sure_order_vip_sup)
    TextView mOrderVipSup;                          //本单立减金额
    @BindView(R.id.make_sure_order_vip_sup_price)
    TextView mOrderVipSupPrice;                     //会员立减金额
    @BindView(R.id.make_sure_order_vip_price)
    TextView mOrderVipPrice;                        //会员价格and单位
    @BindView(R.id.make_sure_order_vip_switch)
    Switch mVipSwitch;                              //是否开通会员按钮
    @BindView(R.id.make_sure_order_vip_sub_price_container)
    RelativeLayout mVipSubPriceContainer;            //会员立减价格容器
    @BindView(R.id.make_sure_order_vip_sub_price)
    TextView mVipSubPrice;                          //会员立减价格
    @BindView(R.id.make_sure_order_platform_securities)
    RelativeLayout mPlatformSecurities;             //平台订金劵容器
    @BindView(R.id.make_sure_order_platform_securities_price)
    TextView mPlatformSecuritiesPrice;              //平台订金劵
    @BindView(R.id.make_sure_order_coin_container)
    RelativeLayout mCoinContainer;                  //颜值币抵扣文案
    @BindView(R.id.make_sure_order_coin_title)
    TextView mCoinTitle;                            //颜值币抵扣文案
    @BindView(R.id.make_sure_order_coin_tip)
    ImageView mCoinTip;                            //颜值币抵扣提示
    @BindView(R.id.make_sure_order_coin_switch)
    Switch mCoinSwitch;                             //颜值币开关
    @BindView(R.id.make_sure_order_deposit_price)
    TextView mDepositPrice;                         //订金总价
    @BindView(R.id.make_sure_order_wallet_click)
    LinearLayout mWalletClick;                        //钱包容器
    @BindView(R.id.make_sure_order_wallet_balance)
    TextView mWalletBalance;                        //钱包余额
    @BindView(R.id.make_sure_order_deduction_amount)
    TextView mDeductionAmount;                      //抵扣金额
    @BindView(R.id.make_sure_order_deduction_switch)
    Switch mDeductionSwitch;                        //钱包开关
    @BindView(R.id.make_sure_order_order_price)
    TextView mOrderPrice;                           //订单金额
    @BindView(R.id.make_sure_order_pay_order)
    Button mPayOrder;                               //去支付按钮
    @BindView(R.id.ll_order_details)
    LinearLayout mPayOrderDeatils;                  //支付明细浮层

    public static final int REQUEST_CODE1 = 1;                //手机号回调
    public static final int REQUEST_CODE2 = 2;                //医院尾款红包回调
    public static final int REQUEST_CODE4 = 4;                //保险信息

    private String taoId;
    private String number;
    private String source;
    private String objid;
    private String groupId;
    private String isGroup;
    private String buyForCart = "0";                //购物车过来的是1，SKU直接购买为0
    private String mU = "0";
    private String TAG = "MakeSureOrderActivity";
    private MakeSureOrderApi makeSureOrderApi;
    private BaseNetWorkCallBackApi makeSureOrderCoinApi;
    private InsuranceMessageApi insuranceMessageApi;
    private MakeSureOrderAdapter makeSureOrderAdapter;
    private PayOrderApi payOrderApi;

    //已经选择的订金劵
    private YouHuiCoupons dingjinNull = new YouHuiCoupons();                              //空的订金优惠
    private YouHuiCoupons youHuiCoupons = dingjinNull;
    //选则保险的SKU列表
    private HashMap<String, InsuranceCheckedData> mInsuranceHashMap = new HashMap<>();

    //加载完成的数据
    private MakeSureOrderData mData;
    //加载完成的保险数据
    private BaoXianData mBaoXianData;
    //颜值币数据
    private MakeSureOrderCoinData coinData;
    private Gson mGson;
    private WalletSecurityCodePop securityCodePop;
    private final String NO_SECURITIES_TITLE = "暂无可用订金劵";
    private final String NUMBER_SECURITIES_TITLE = "张订金劵可用";
    private MakeSureCoinTipDialog coinTipDialog;
    private final String MONEY = "[money]";
    private boolean userIsMember;               //当前用户是否是会员
    private PayOrderDetailsDialog mPayOrderDetailsDialog;//支付明细弹层
    private float price1, price2, price3, price4, price5, price6;
    private String orderMoney;
    private String mBargainOrderId;
    private HashMap<String, String> mBargainData;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_make_sure_order;
    }

    @Override
    protected void initView() {
        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null) {
            taoId = bundle.getString("tao_id");
            number = bundle.getString("number");
            source = bundle.getString("source");
            objid = bundle.getString("objid");
            groupId = bundle.getString("group_id", "");
            isGroup = bundle.getString("is_group");
            buyForCart = bundle.getString("buy_for_cart");
            mU = bundle.getString("u", "0");
            mBargainOrderId = bundle.getString("bargain_order_id");
        }

        Log.e(TAG, "taoId == " + taoId);
        Log.e(TAG, "taoId == " + taoId);
        Log.e(TAG, "number == " + number);
        Log.e(TAG, "source == " + source);
        Log.e(TAG, "objid == " + objid);
        Log.e(TAG, "groupId == " + groupId);
        Log.e(TAG, "isGroup == " + isGroup);
        Log.e(TAG, "buyForCart == " + buyForCart);
        //设置是否选择购买vip会员
        mVipSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                if (makeSureOrderAdapter != null) {
                    makeSureOrderAdapter.openOrDownVip(isVipOrder());

                    //操作会员开通关闭时设置数据
                    if (mData != null) {
                        mData.getTaomember().setUser_is_member(isCheck ? "1" : "0");
                    }

                    //判断立减88是否可用
                    if (isFirstSingle()) {
                        mVipSubPriceContainer.setVisibility(View.VISIBLE);
                        mVipSubPrice.setText("-¥" + mData.getTaomember().getFrist_lijian_price() + "订金");
                    } else {
                        mVipSubPriceContainer.setVisibility(View.GONE);
                    }

                    isFrist = true;
                    loadingCarCoinData();
                }
            }
        });

        //钱包余额开关
        mDeductionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                float walletBalance = Float.parseFloat(mData.getUserWalletBalance());
                if (isCheck) {
                    mWalletBalance.setText("(¥" + walletBalance + ")");
                } else {
                    mWalletBalance.setText("(可用余额¥" + walletBalance + ")");
                }
                setDingJinNum();
            }
        });
        //设置点击事件
        setMultiOnClickListener(mOrderPhoneClick, mPayOrder, mOrderInsurance, mPlatformSecurities, mPayOrderDeatils);
    }

    @Override
    protected void initData() {
        makeSureOrderApi = new MakeSureOrderApi();
        makeSureOrderCoinApi = new BaseNetWorkCallBackApi(FinalConstant1.CART, "getyancoindeduction");
        insuranceMessageApi = new InsuranceMessageApi();
        payOrderApi = new PayOrderApi();
        securityCodePop = new WalletSecurityCodePop(mContext);

        mGson = new Gson();

        ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        scrollLinearLayoutManager.setScrollEnable(false);
        mOrderRecycler.setLayoutManager(scrollLinearLayoutManager);

        loadingData();
        getInsuranceData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (Utils.isFastDoubleClick()) {
            return;
        }
        switch (v.getId()) {
            case R.id.make_sure_order_phone_click:              //更改手机号
                Intent it = new Intent(mContext, OrderPhoneModifyActivity.class);
                startActivityForResult(it, REQUEST_CODE1);
                break;
            case R.id.make_sure_order_pay_order:               //去支付
                if (mWalletClick.getVisibility() != View.GONE && mDeductionSwitch.isChecked()) {
                    getCode();
                } else {
                    payOrder();
                }
                break;
            case R.id.make_sure_order_insurance:              //保险点击事件
                //跳转到保险信息页面
                Intent it88 = new Intent(mContext, InSureActivity.class);
                if (mBaoXianData != null) {
                    it88.putExtra("insure_name", mBaoXianData.getName());
                    it88.putExtra("insure_phone", mBaoXianData.getPhone());
                    it88.putExtra("insure_card", mBaoXianData.getIdcard());
                    it88.putExtra("insure_sex", mBaoXianData.getSex());
                } else {
                    mContext.startActivityForResult(it88, REQUEST_CODE4);
                }
                break;
            case R.id.make_sure_order_platform_securities:    //平台劵抵扣
                ArrayList<YouHuiCoupons> usemember;
                if (mVipSwitch.isChecked()) {
                    usemember = mData.getDepositCoupons().getUsemember();
                } else {
                    usemember = mData.getDepositCoupons().getNousemember();
                }

                Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
                intent.putExtra("youhui_coupon", youHuiCoupons);                     //选中的
                intent.putParcelableArrayListExtra("youhui_coupons", usemember);     //列表
                startActivityForResult(intent, REQUEST_CODE2);

                break;
            case R.id.ll_order_details:
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.PAYMENT_DETAIL_CLICK, "1"), new ActivityTypeData("86"));
                //初始化价格明细弹窗
                mPayOrderDetailsDialog = new PayOrderDetailsDialog(mContext, price1, price2, mDeductionSwitch.isChecked() ? price3 : 0, price4, price5, price6, orderMoney, new PayOrderDetailsDialog.PayListener() {
                    @Override
                    public void onPay(View v) {
                        if (mWalletClick.getVisibility() != View.GONE && mDeductionSwitch.isChecked()) {
                            getCode();
                        } else {
                            payOrder();
                        }
                    }
                });
                if (mPayOrderDetailsDialog != null) {
                    mPayOrderDetailsDialog.show();
                }
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE1:                 //更改手机号的回调
                if (data != null) {
                    String spj = data.getStringExtra("phone");
                    mData.setLoginphone(spj);
                    mOrderPhone.setText(getOmitPhone(spj));
                }
                break;
            case REQUEST_CODE2:                 //使用尾款券，订金劵的回调
                int hos_pos = data.getIntExtra("hos_pos", -1);
                YouHuiCoupons wkqCoupon = data.getParcelableExtra("youhui_coupon");
                if (hos_pos >= 0) {
                    //尾款红包
                    makeSureOrderAdapter.setWkYouhui(hos_pos, wkqCoupon);
                } else {
                    //订金劵
                    youHuiCoupons = wkqCoupon;
                    Log.e(TAG, "youHuiCoupons == " + youHuiCoupons);
                    Log.e(TAG, "wkqCoupon == " + youHuiCoupons);

                    loadingCarCoinData();
                }
                break;
            case REQUEST_CODE4:                 //保险信息回调
                String baoXianName = data.getStringExtra("name");
                String baoXianPhone = data.getStringExtra("phone");
                String baoXianIdcard = data.getStringExtra("card");
                String baoXianSex = data.getStringExtra("sex");

                if (!TextUtils.isEmpty(baoXianName) && !TextUtils.isEmpty(baoXianPhone) && !TextUtils.isEmpty(baoXianIdcard) && !TextUtils.isEmpty(baoXianSex)) {
                    mBaoXianData.setName(baoXianName);
                    mBaoXianData.setIdcard(baoXianIdcard);
                    mBaoXianData.setPhone(baoXianPhone);
                    mBaoXianData.setSex(baoXianSex);

                    mOrderInformation.setText("被保险人：" + baoXianName + " (" + baoXianPhone.substring(0, 3) + "****" + baoXianPhone.substring(7, baoXianPhone.length()) + ")");
                }
                break;
        }

    }

    /**
     * 获取订单数据
     */
    private void loadingData() {
        if (!TextUtils.isEmpty(taoId)) {
            makeSureOrderApi.addData("tao_id", taoId);
        }
        if (!TextUtils.isEmpty(number)) {
            makeSureOrderApi.addData("number", number);
        }
        if (!TextUtils.isEmpty(source)) {
            makeSureOrderApi.addData("source", source);
        }
        if (!TextUtils.isEmpty(objid)) {
            makeSureOrderApi.addData("objid", objid);
        }
        if (!TextUtils.isEmpty(isGroup)) {
            makeSureOrderApi.addData("is_group", isGroup);
        }
        if (!TextUtils.isEmpty(mU)) {
            makeSureOrderApi.addData("u", mU);
        }
        if (!TextUtils.isEmpty(mBargainOrderId)) {
            makeSureOrderApi.addData("bargain_order_id", mBargainOrderId);
        }

        makeSureOrderApi.getCallBack(mContext, makeSureOrderApi.getHashMap(), new BaseCallBackListener<MakeSureOrderData>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(MakeSureOrderData data) {
                mMakeSureOrder.setVisibility(View.VISIBLE);
                mData = data;
                userIsMember = "1".equals(mData.getTaomember().getUser_is_member());
                mBargainData = data.getBargain_data();
                //手机号
                mOrderPhone.setText(getOmitPhone(mData.getLoginphone()));

                //判断是否是会员
                MakeSureOrderTaomember taomember = mData.getTaomember();
                float memberlijian = Float.parseFloat(taomember.getMemberlijian());             //本单立省
                float fristLijianPrice = Float.parseFloat(taomember.getFrist_lijian_price());   //本单立减

                if ("0".equals(mData.getTaomember().getUser_is_member())) {
                    //如果不是会员
                    if (memberlijian > 150) {
                        //立减>150，开通按钮显示
                        mOpenVip.setVisibility(View.VISIBLE);
                        mOrderVipPrice.setText("¥" + taomember.getBuy_plus_price() + "/年");               //会员价格

                        //开通会员容器下的立减
                        mOrderVipSup.setText("本单立省¥" + memberlijian + "元");

                        //开通会员时下边的立减
                        if (fristLijianPrice > 0) {
                            mOrderVipSupPrice.setVisibility(View.VISIBLE);
                            mOrderVipSupPrice.setText("开通即可用立减" + fristLijianPrice + "元");
                        } else {
                            mOrderVipSupPrice.setVisibility(View.GONE);
                        }

                    } else {
                        //<=150开通按钮隐藏
                        mOpenVip.setVisibility(View.GONE);
                    }

                } else {
                    //如果是会员，开通按钮隐藏
                    mOpenVip.setVisibility(View.GONE);
                }

                //判断立减88是否可用
                if (isFirstSingle()) {
                    mVipSubPriceContainer.setVisibility(View.VISIBLE);
                    mVipSubPrice.setText("-¥" + taomember.getFrist_lijian_price() + "订金");
                } else {
                    mVipSubPriceContainer.setVisibility(View.GONE);
                }

                //加载是适配器
                makeSureOrderAdapter = new MakeSureOrderAdapter(mContext, isVipOrder(), isGroupSku(), buyForCart, mData.getData());
                mOrderRecycler.setAdapter(makeSureOrderAdapter);

                //设置平台最优惠订金劵
                setDepositPreferential();
                //颜值币数据加载
                loadingCarCoinData();

                //回调
                makeSureOrderAdapter.setOnEventClickListener(new MakeSureOrderAdapter.OnEventClickListener() {
                    @Override
                    public void onInsuranceClick(InsuranceCheckedData data) {
                        mInsuranceHashMap.put(data.getTaoid(), data);
                        setDingJinNum();
                    }

                    @Override
                    public void onNumberClick(int value) {
                        if (!buyForCart()) {
                            isFrist = true;
                            number = value + "";
                            loadingData();
                        }
                    }

                    @Override
                    public void onBalancePaymentClick(BalancePaymentData data) {
                        if (data.getWk_coupons().size() != 0) {
                            Log.e(TAG, "data.getHosPos() == " + data.getHosPos());
                            Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
                            intent.putExtra("youhui_coupon", data.getCoupons());                            //选中的
                            intent.putExtra("hos_pos", data.getHosPos());                                   //当前医院下标
                            intent.putParcelableArrayListExtra("youhui_coupons", data.getWk_coupons());     //列表
                            startActivityForResult(intent, REQUEST_CODE2);
                        }
                    }
                });
            }
        });
    }

    /**
     * 获取颜值币数据
     */
    private void loadingCarCoinData() {
        makeSureOrderCoinApi.getHashMap().clear();
        if (!TextUtils.isEmpty(number)) {
            makeSureOrderCoinApi.addData("number", number);              //SKU数量（直接购买使用）
        }
        if (!TextUtils.isEmpty(taoId)) {
            makeSureOrderCoinApi.addData("tao_id", taoId);              //淘整形ID（直接购买使用）
        }

        if (!TextUtils.isEmpty(isGroup)) {
            makeSureOrderCoinApi.addData("is_group", isGroup);            //是否是拼团订单
        }

        //是否开启会员
        if (!userIsMember) {
            if (mVipSwitch.isChecked()) {
                makeSureOrderCoinApi.addData("isMember", "1");
            } else {
                makeSureOrderCoinApi.addData("isMember", "0");
            }
        }

        //是否使用优惠劵ID
        Log.e(TAG, "youHuiCoupons.getCard_id() === " + youHuiCoupons.getCard_id());
        if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
            makeSureOrderCoinApi.addData("couponsID", youHuiCoupons.getCard_id());
        }

        if (!TextUtils.isEmpty(mBargainOrderId)) {
            makeSureOrderCoinApi.addData("bargain_order_id", mBargainOrderId);
        }

        makeSureOrderCoinApi.startCallBack(new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData data) {
                if ("1".equals(data.code)) {
                    coinData = JSONUtil.TransformSingleBean(data.data, MakeSureOrderCoinData.class);

                    //初始化钱包
                    initWallet();

                    //最终金额设置
                    setDingJinNum();
                }
            }
        });
    }

    //是否是初始化钱包
    private boolean isInitWallet = true;

    /**
     * 设置钱包
     */
    @SuppressLint("SetTextI18n")
    private void initWallet() {
        //默认钱包是打开的还是关闭的
        float walletBalance = Float.parseFloat(mData.getUserWalletBalance());
        if (walletBalance > 0) {
            mWalletClick.setVisibility(View.VISIBLE);

            //订金劵价格
            float dingjinDeposit = 0;
            if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
                dingjinDeposit = Float.parseFloat(youHuiCoupons.getMoney());
            }

            Log.e(TAG, "dingjinDeposit == " + dingjinDeposit);
            if (getDeposit() - dingjinDeposit >= 0) {
                if (isInitWallet) {
                    mDeductionSwitch.setChecked(true);
                    isInitWallet = false;
                }
                mWalletBalance.setText("(¥" + walletBalance + ")");
            } else {
                if (isInitWallet) {
                    mDeductionSwitch.setChecked(false);
                    isInitWallet = false;
                }
                mWalletBalance.setText("(可用余额¥" + walletBalance + ")");
            }
        } else {
            mWalletClick.setVisibility(View.GONE);
        }
    }

    /**
     * 获取保险信息
     */
    private void getInsuranceData() {

        insuranceMessageApi.getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    Log.e(TAG, "serverData == " + serverData.toString());
                    try {
                        if (!TextUtils.isEmpty(serverData.data)) {
                            mBaoXianData = JSONUtil.TransformSingleBean(serverData.data, BaoXianData.class);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * 支付订单
     */
    private void payOrder() {
        payOrder("");
    }

    private void payOrder(String code) {
        //手机号
        payOrderApi.addData("phone", mData.getLoginphone());

        //是否投保  1 是
        if (mOrderInsurance.getVisibility() == View.VISIBLE) {
            payOrderApi.addData("insure_name", mBaoXianData.getName());                         //保险人姓名
            payOrderApi.addData("insure_phone", mBaoXianData.getPhone());                       //保险人手机
            payOrderApi.addData("insure_card_id", mBaoXianData.getIdcard());                    //保险保险人
            payOrderApi.addData("insure_sex", mBaoXianData.getSex());                           //保险人性别
        } else {
            payOrderApi.addData("insure_name", "");                          //保险人姓名
            payOrderApi.addData("insure_phone", "");                         //保险人手机
            payOrderApi.addData("insure_card_id", "0");                       //保险保险人
            payOrderApi.addData("insure_sex", "");                           //保险人性别
        }

        //订金优惠券id
        if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
            payOrderApi.addData("deposit_coupons_card_id", youHuiCoupons.getCard_id());
            Log.e(TAG, "youHuiCoupons.getCard_id() === " + youHuiCoupons.getCard_id());
        } else {
            payOrderApi.addData("deposit_coupons_card_id", "0");
        }

        //是否是拼团订单
        if (isGroupSku()) {
            //是拼团购买
            payOrderApi.addData("is_group", "1");
            if (!TextUtils.isEmpty(groupId)) {
                //团员
                payOrderApi.addData("group_id", groupId);
            } else {
                //团长开团
                payOrderApi.addData("group_id", "0");
            }
        } else {
            //不是拼团购买
            payOrderApi.addData("is_group", "0");
            payOrderApi.addData("group_id", "0");
        }

        //是否有立减88 有传1 没有传0
        if (isFirstSingle()) {
            payOrderApi.addData("is_have_first_reduce", "1");
        } else {
            payOrderApi.addData("is_have_first_reduce", "0");
        }

        //淘整形字符串
        String payOrderTao = getPayOrderTao();
        Log.e(TAG, "payOrderTao == " + payOrderTao);
        payOrderApi.addData("tao_json", payOrderTao);

        //是否使用钱包付款1是
        if (Float.parseFloat(mData.getUserWalletBalance()) > 0 && mDeductionSwitch.isChecked()) {
            payOrderApi.addData("use_wallet", "1");
            payOrderApi.addData("code", code);
        } else {
            payOrderApi.addData("use_wallet", "0");
        }

        //是否购买会员
        if (mOpenVip.getVisibility() == View.VISIBLE && mVipSwitch.isChecked()) {
            payOrderApi.addData("buy_plus", "1");
        } else {
            payOrderApi.addData("buy_plus", "0");
        }

        //是否是购物车下单 1是
        payOrderApi.addData("buyForCart", buyForCart);

        //是否使用颜值币抵扣
        payOrderApi.addData("yanCoinDeductionRes", mCoinSwitch.isChecked() ? "1" : "0");
        if (null != mBargainData && !mBargainData.isEmpty()) {
            Log.e(TAG, "mBargainData ==" + mBargainData.toString());
            for (Map.Entry<String, String> map : mBargainData.entrySet()) {
                payOrderApi.addData(map.getKey(), map.getValue());
            }

        }
        Log.e(TAG, "payOrderApi.getHashMap() == " + payOrderApi.getHashMap().toString());

        payOrderApi.getCallBack(mContext, payOrderApi.getHashMap(), new BaseCallBackListener<PayOrderData>() {
            @Override
            public void onSuccess(PayOrderData data) {
                Utils.getCartNumber(mContext);

                String groupId = data.getGroup_id();                        //拼团id
                String isGroup = data.getIs_group();                        //是否是拼团 1是
                String isInsure = data.getIs_insure();                      //是否参保 1是
                String is_repayment = data.getIs_repayment();                //是否支持信用卡分期支付
                String money_ = data.getMoney();                             //需要支付金额
                String order_id = data.getOrder_id();                        //订单号
                String order_time = data.getOrder_time();                    //订单时间
                String server_id = data.getServer_id();                      //服务码
                String is_repayment_mimo = data.getIs_repayment_mimo();
                String total_price = data.getTotal_price();
                String number = data.getNumber();


                Log.e(TAG, "total_price == " + total_price);
                Log.e(TAG, "number == " + number);
                //打点统计
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Time", Utils.getTime());
                hashMap.put("XDO1_jine", money_);
                StatisticalManage.getInstance().growingIO("Unpaid_orders", hashMap);

                mFunctionManager.saveStr(order_id + "quantity", number);
                mFunctionManager.saveStr(order_id + FinalConstant.GMV, total_price);


                // 下单成功后跳转
                if (Float.parseFloat(money_) <= 0) {
                    //订单金额小于0
                    if (!TextUtils.isEmpty(groupId) && "1".equals(isGroup)) { //拼团
                        Intent intent = new Intent(mContext, SpeltActivity.class);
                        intent.putExtra("group_id", groupId);
                        intent.putExtra("order_id", order_id);
                        intent.putExtra("type", "2");
                        startActivity(intent);
                    } else {                                     //不是拼团
                        Bundle bundle = new Bundle();
                        bundle.putString("payType", "2");
                        bundle.putString("pay_type", "1");
                        bundle.putString("server_id", server_id);
                        bundle.putString("order_id", order_id);
                        bundle.putString("sku_type", "1");
                        bundle.putString("is_repayment", is_repayment);
                        bundle.putString("is_repayment_mimo", is_repayment_mimo);
                        bundle.putString("price", money_);
                        mFunctionManager.goToActivity(OrderZhiFuStatus1Activity.class, bundle);
                    }
                } else {
                    //订单金额大于0
                    Bundle bundle = new Bundle();
                    bundle.putString("price", money_);
                    bundle.putString("server_id", server_id);
                    bundle.putString("order_id", order_id);
                    bundle.putString("order_time", order_time);
                    bundle.putString("type", "1");
                    bundle.putString("sku_type", "1");
                    bundle.putString("is_repayment", is_repayment);
                    bundle.putString("is_repayment_mimo", is_repayment_mimo);
                    bundle.putString("weikuan", "0");
                    bundle.putString("is_group", isGroup);
                    bundle.putString("group_id", groupId);
                    mFunctionManager.goToActivity(OrderMethodActivity594.class, bundle);
                }

                onBackPressed();
            }
        });
    }

    /**
     * 保险、平台订金劵、钱包等发生变化时的变化
     */
    @SuppressLint("SetTextI18n")
    private void setDingJinNum() {
        //设置保险金额
        setInsurancePrice();
        //颜值币抵扣
        setCoinPrice();
        //设置平台订金劵金额
        setDepositPreferential();
        //设置订金金额
        setDeposit();
        //设置钱包余额
        setWalletBalance();
        //设置订单金额
        setOrderPrice();
    }

    /**
     * 设置保险金额
     */
    @SuppressLint("SetTextI18n")
    private void setInsurancePrice() {
        float insurancePrice = 0;
        for (InsuranceCheckedData data : mInsuranceHashMap.values()) {
            insurancePrice += data.getPrice();
        }

        if (insurancePrice > 0) {
            mOrderInsurance.setVisibility(View.VISIBLE);
            mOrderInsurancePrice.setText("¥" + insurancePrice);
        }
    }

    /**
     * 设置颜值币抵扣
     */
    private void setCoinPrice() {
        //抵扣文案
        Log.e(TAG, "coinData == " + coinData);
        if (coinData != null && "1".equals(coinData.getIsShowDeduction())) {
            mCoinContainer.setVisibility(View.VISIBLE);
            String desc = coinData.getDeductionDesc();
            if (desc.contains(MONEY)) {
                Log.e(TAG, "desc === " + desc);
                String deductionMoney = "¥" + coinData.getDeductionMoney();
                Log.e(TAG, "deductionMoney === " + deductionMoney);

                String descText = desc.replace(MONEY, deductionMoney);
                Log.e(TAG, "descText === " + descText);

                String[] split = desc.split(MONEY);
                Log.e(TAG, "split[0] === " + split[0]);
                int start = split[0].length() - 1;
                int end = descText.length();
                Log.e(TAG, "start === " + start);
                Log.e(TAG, "end === " + end);

                SpannableString spannableString = new SpannableString(descText);
                spannableString.setSpan(new ForegroundColorSpan(Utils.getLocalColor(mContext, R.color.red_ff5c77)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                mCoinTitle.setText(spannableString);
            } else {
                mCoinTitle.setText(desc);
            }
            //颜值币抵扣提示文案
            if (!TextUtils.isEmpty(coinData.getYanCoinUesDesc())) {
                mCoinTip.setVisibility(View.VISIBLE);
                mCoinTip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String coinUesDesc = coinData.getYanCoinUesDesc();
                        if (!TextUtils.isEmpty(coinUesDesc)) {
                            if (coinTipDialog == null) {
                                coinTipDialog = new MakeSureCoinTipDialog(mContext, coinUesDesc);
                            }
                            coinTipDialog.show();
                        }
                    }
                });

            } else {
                mCoinTip.setVisibility(View.GONE);
            }

            //是否显示抵扣按钮
            mCoinSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                    if ("1".equals(coinData.getIsCanUse())) {
                        setDingJinNum();
                    } else {
                        if (!TextUtils.isEmpty(coinData.getNotUesNotice())) {
                            mFunctionManager.showShort(coinData.getNotUesNotice());
                        }
                        mCoinSwitch.setChecked(false);
                    }

                }
            });
        } else {
            mCoinContainer.setVisibility(View.GONE);
        }
    }

    /**
     * 设置平台订金劵
     */
    private boolean isFrist = true;   //是否是第一次筛选优惠劵，开通或者关闭会员优惠劵是不一样的，所以要重新筛选

    @SuppressLint("SetTextI18n")
    private void setDepositPreferential() {
        if (isFrist) {
            //第一次筛选优惠劵
            isFrist = false;
            ArrayList<YouHuiCoupons> dingjinList = new ArrayList<>();
            if (mVipSwitch.isChecked()) {
                //开通会员
                makeSureOrderAdapter.openOrDownVip(true);
                if (mData != null) {
                    dingjinList = mData.getDepositCoupons().getUsemember();
                }
            } else {
                //关闭会员
                makeSureOrderAdapter.openOrDownVip(false);
                if (mData != null) {
                    dingjinList = mData.getDepositCoupons().getNousemember();
                }
            }

            //平台定金劵
            if (dingjinList.size() > 0) {
                if (getDeposit() != 0) {
                    mPlatformSecurities.setVisibility(View.VISIBLE);
                    Collections.sort(dingjinList, new DingjinCouponsRank());
                    youHuiCoupons = dingjinList.get(0);
                    mPlatformSecuritiesPrice.setText("抵扣¥" + youHuiCoupons.getMoney());
                } else {
                    youHuiCoupons = dingjinNull;
                    mPlatformSecuritiesPrice.setText(dingjinList.size() + NUMBER_SECURITIES_TITLE);
                }
            } else {
                mPlatformSecurities.setVisibility(View.GONE);
                youHuiCoupons = dingjinNull;
                mPlatformSecuritiesPrice.setText(NO_SECURITIES_TITLE);
            }
        } else {
            //非第一次筛选优惠劵
            ArrayList<YouHuiCoupons> dingjinList = getDingjinList();
            if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
                if (getDeposit() != 0) {
                    mPlatformSecuritiesPrice.setText("抵扣¥" + youHuiCoupons.getMoney());
                } else {
                    mPlatformSecuritiesPrice.setText(dingjinList.size() + NUMBER_SECURITIES_TITLE);
                }
            } else {
                if (dingjinList.size() > 0) {
                    mPlatformSecuritiesPrice.setText(dingjinList.size() + NUMBER_SECURITIES_TITLE);
                } else {
                    mPlatformSecuritiesPrice.setText(NO_SECURITIES_TITLE);
                }
            }
        }
    }

    /**
     * 设置订金金额
     */
    @SuppressLint("SetTextI18n")
    private void setDeposit() {
        //订金劵价格
        float dingjinDeposit = 0;
        if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
            dingjinDeposit = Float.parseFloat(youHuiCoupons.getMoney());
        }

        if (getDeposit() != 0) {
            float depositPrice = getDeposit() - dingjinDeposit;

            float price = new BigDecimal(depositPrice).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();

            mDepositPrice.setText("¥" + (price < 0 ? 0 : price));
        } else {
            mDepositPrice.setText("¥0");
        }
    }

    /**
     * 设置钱包余额
     */
    @SuppressLint("SetTextI18n")
    private void setWalletBalance() {
        float walletBalance = Float.parseFloat(mData.getUserWalletBalance());
        if (mDeductionSwitch.isChecked()) {

            float deposit = getDeposit();

            //订金劵价格
            float dingjinDeposit = 0;
            Log.e(TAG, "youHuiCoupons == " + youHuiCoupons);
            if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
                dingjinDeposit = Float.parseFloat(youHuiCoupons.getMoney());
            }

            //订金金额
            deposit = deposit - dingjinDeposit;
            mDeductionAmount.setVisibility(View.VISIBLE);
            //抵扣金额
            float walletPrice = walletBalance < deposit ? walletBalance : deposit;
            mDeductionAmount.setText("抵扣" + (walletPrice > 0 ? walletPrice : 0));

            if (walletPrice > 0) {
                price3 = walletPrice;
            } else {
                price3 = 0;
            }

        } else {
            mDeductionAmount.setVisibility(View.GONE);
        }
    }

    /**
     * 设置订单金额
     */
    @SuppressLint("SetTextI18n")
    private void setOrderPrice() {

        float depositPrice = getDeposit();
        if (depositPrice != 0) {
            //订金劵价格
            float dingjinDeposit = 0;
            if (!TextUtils.isEmpty(youHuiCoupons.getCard_id())) {
                dingjinDeposit = Float.parseFloat(youHuiCoupons.getMoney());
            }
            price2 = dingjinDeposit;

            //订金价格 - 订金劵
            float mPrice1 = depositPrice - dingjinDeposit;
            if (mPrice1 > 0) {
                //钱包金额
                float walletBalance;
                if (mDeductionSwitch.isChecked()) {
                    walletBalance = Float.parseFloat(mData.getUserWalletBalance());
                } else {
                    walletBalance = 0;
                }

                //订金价格 - 订金劵 - 钱包余额
                float mPrice2 = new BigDecimal(mPrice1 - walletBalance).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                float mPrice4 = new BigDecimal(mPrice2).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                mOrderPrice.setText("¥" + (mPrice4 < 0 ? 0 : mPrice4));
                orderMoney = "¥" + (mPrice4 < 0 ? 0 : mPrice4);
            } else {
                float mPrice3 = new BigDecimal(mPrice1).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                mOrderPrice.setText("¥" + (mPrice3 < 0 ? 0 : mPrice3));
                orderMoney = "¥" + (mPrice3 < 0 ? 0 : mPrice3);
            }
        } else {
            mOrderPrice.setText("¥0");
        }

    }

    /**
     * 获取订金劵列表
     */
    private ArrayList<YouHuiCoupons> getDingjinList() {
        ArrayList<YouHuiCoupons> usemember;
        if (mVipSwitch.isChecked()) {
            //开通会员
            usemember = mData.getDepositCoupons().getUsemember();
        } else {
            //关闭会员
            usemember = mData.getDepositCoupons().getNousemember();
        }

        return usemember;
    }

    /**
     * 获取不使用订金劵的订金金额
     */
    @SuppressLint("SetTextI18n")
    private float getDeposit() {
        //订金金额
        float deposit = makeSureOrderAdapter.allDeposit();
        price1 = deposit;

        //保险金额
        float insurancePrice = 0;
        for (InsuranceCheckedData data : mInsuranceHashMap.values()) {
            insurancePrice += data.getPrice();
        }

        //会员价格
        float buyPlusPrice = 0;
        if (mVipSwitch.isChecked()) {
            buyPlusPrice = Float.parseFloat(mData.getTaomember().getBuy_plus_price());
        }
        price5 = buyPlusPrice;

        //颜值币抵扣金额
        float coinDeposit = 0;
        if (coinData != null && mCoinSwitch.isChecked()) {
            coinDeposit = Integer.parseInt(coinData.getDeductionMoney());
        }
        price4 = coinDeposit;

        //首单立减价格
        float fristLijianPrice = 0;
        if (isFirstSingle()) {
            fristLijianPrice = Float.parseFloat(mData.getTaomember().getFrist_lijian_price());
        }
        price6 = fristLijianPrice;

        //颜值币抵扣订金后的订金金额
        deposit = deposit - coinDeposit;
        deposit = deposit >= 0 ? deposit : 0;

        //计算最后的价格
        deposit = deposit + insurancePrice + buyPlusPrice - fristLijianPrice;
        return deposit < 0 ? 0 : deposit;
    }

    /**
     * 获取提交订单的淘数据
     *
     * @return
     */
    private String getPayOrderTao() {
        List<PayOrderTao> payOrderTaos = new ArrayList<>();
        if (makeSureOrderAdapter != null) {
            for (int i = 0; i < makeSureOrderAdapter.getmDatas().size(); i++) {
                MakeSureOrderSkuNumberData data = makeSureOrderAdapter.getMakeSureOrderSkuNumberData(i);
                PayOrderTao payOrderTao = new PayOrderTao();
                payOrderTao.setHos_id(makeSureOrderAdapter.getMakeSureOrderSkuNumberData(i).getHos_id());
                payOrderTao.setBeizhu(makeSureOrderAdapter.getmDatas().get(i).getEditTextStr());
                if (data.getCoupons() != null && !TextUtils.isEmpty(data.getCoupons().getCard_id())) {
                    payOrderTao.setHos_hongbao_card_id(data.getCoupons().getCard_id());
                } else {
                    payOrderTao.setHos_hongbao_card_id("0");
                }

                List<PayOrderTaoSku> payOrderTaoSku = new ArrayList<>();
                for (MakeSureOrderSkuNumberTao taoData : data.getTao_data()) {
                    PayOrderTaoSku orderTaoSku = new PayOrderTaoSku();
                    orderTaoSku.setTao_id(taoData.get_id());
                    orderTaoSku.setNumber(taoData.getNumber());
                    orderTaoSku.setExtension_user(taoData.getExtension_user());
                    if (mInsuranceHashMap.get(taoData.get_id()) != null) {
                        orderTaoSku.setIs_insure("1");
                    } else {
                        orderTaoSku.setIs_insure("0");
                    }
                    orderTaoSku.setPostStr(taoData.getPostStr());
                    payOrderTaoSku.add(orderTaoSku);
                }

                payOrderTao.setTaodata(payOrderTaoSku);
                payOrderTaos.add(payOrderTao);
            }
        }
        return mGson.toJson(payOrderTaos);
    }

    /**
     * 获取省略的手机号
     *
     * @param spj
     * @return
     */
    private String getOmitPhone(String spj) {
        String a = spj.substring(0, 3);
        String b = spj.substring(spj.length() - 4, spj.length());
        return a + "****" + b;
    }

    /**
     * 如果是钱包支付获取验证码
     */
    private void getCode() {
        securityCodePop.showAtLocation(mMakeSureOrder, Gravity.BOTTOM, 0, 0);
        securityCodePop.setOnTureClickListener(new WalletSecurityCodePop.OnTrueClickListener() {
            @Override
            public void onTrueClick(String codes) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                Log.e(TAG, "22222222");
                payOrder(codes);
                securityCodePop.dismiss();
            }
        });
    }

    /**
     * 判断是否有首单立减
     *
     * @return ：true:立减是存在的，false:立减是不存在的
     */
    private boolean isFirstSingle() {
        if (isVipOrder()) {
            //是会员
            //立减88金额大于0
            //立减88金额小于等于0
            return Float.parseFloat(mData.getTaomember().getFrist_lijian_price()) > 0;
        } else {
            //不是会员
            //开通会员按钮是打开的
            //开通会员按钮是关闭的
            return mVipSwitch.isChecked();
        }
    }

    /**
     * 判断是否是从购物车过来的
     *
     * @return true:购物车，false:直接购买
     */
    private boolean buyForCart() {
        return "1".equals(buyForCart);
    }

    /**
     * 是否是拼团下单
     *
     * @return true:拼团下单，false:非拼团下单
     */
    private boolean isGroupSku() {
        //是拼团
        //不是拼团
        return "1".equals(isGroup);
    }

    /**
     * 判断是否是vip下单
     *
     * @return trur：是，false:否
     */
    private boolean isVipOrder() {
        if (mData != null) {
            return "1".equals(mData.getTaomember().getUser_is_member());
        }
        return mVipSwitch.isChecked();
    }

    @Override
    protected void onDestroy() {
        if (MakeSureOrderActivity.this != null && !MakeSureOrderActivity.this.isFinishing()) {
            securityCodePop.dismiss();
            securityCodePop = null;
        }
        super.onDestroy();
    }
}
