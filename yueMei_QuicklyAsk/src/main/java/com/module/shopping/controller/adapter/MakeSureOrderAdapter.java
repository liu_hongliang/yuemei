package com.module.shopping.controller.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.commonview.view.ScrollLayoutManager;
import com.module.my.controller.activity.OrderPreferentialActivity639;
import com.module.shopping.model.bean.BalancePaymentData;
import com.module.shopping.model.bean.DingjinCouponsRank;
import com.module.shopping.model.bean.InsuranceCheckedData;
import com.module.shopping.model.bean.MakeSureOrderSkuData;
import com.module.shopping.model.bean.MakeSureOrderSkuNumberData;
import com.module.shopping.model.bean.WkCouponsRank;
import com.module.shopping.model.bean.YouHuiCoupons;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import static com.module.shopping.controller.activity.MakeSureOrderActivity.REQUEST_CODE2;

/**
 * Created by 裴成浩 on 2018/11/27.
 */
public class MakeSureOrderAdapter extends RecyclerView.Adapter<MakeSureOrderAdapter.ViewHolder> {

    private String TAG = "MakeSureOrderAdapter";
    private boolean mIsVipOrder;
    private boolean mIsGroup;                   //是否是拼团
    private Activity mContext;
    private List<MakeSureOrderSkuData> mDatas;
    private String mBuyForCart;
    private LayoutInflater mInflater;
    private HashMap<String, MakeSureOrderSkuAdapter> skuAdapters = new LinkedHashMap<>();

    private final String WEIKUAN = "weikuan";

    public MakeSureOrderAdapter(Activity context, boolean isVipOrder, boolean isGroup, String buyForCart, List<MakeSureOrderSkuData> datas) {
        this.mContext = context;
        this.mIsVipOrder = isVipOrder;
        this.mIsGroup = isGroup;
        this.mDatas = datas;
        this.mBuyForCart = buyForCart;
        mInflater = LayoutInflater.from(mContext);
        Log.e(TAG, "mDatas == " + mDatas.size());
        Log.e(TAG, "mDatas == " + mDatas.get(0).getNousemember().getTao_data());
        Log.e(TAG, "mDatas == " + mDatas.get(0).getUsemember().getTao_data());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View inflate = mInflater.inflate(R.layout.make_sure_order_view, parent, false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(viewHolder, position);
        } else {
            for (Object payload : payloads) {
                switch ((String) payload) {
                    case WEIKUAN:
                        //尾款劵刷新
                        setWeikuanRedEnvelope(viewHolder, position, false);
                        break;
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) viewHolder.mOrderView.getLayoutParams();
        if (pos == 0) {
            layoutParams.bottomMargin = Utils.dip2px(5);
        } else if (pos == mDatas.size() - 1) {
            layoutParams.topMargin = Utils.dip2px(5);
        } else {
            layoutParams.topMargin = Utils.dip2px(5);
            layoutParams.bottomMargin = Utils.dip2px(5);
        }
        viewHolder.mOrderView.setLayoutParams(layoutParams);

        //设置备注数据
        viewHolder.mLeaveMessage.setText(mDatas.get(pos).getEditTextStr());

        mDatas.get(pos).setEditTextStr(viewHolder.mLeaveMessage.getText().toString());

        MakeSureOrderSkuNumberData data = getMakeSureOrderSkuNumberData(pos);

        float dingjin = Float.parseFloat(data.getPay_dingjin());  //订金价格

        //设置SKU适配器
        MakeSureOrderSkuAdapter sureOrderSkuAdapter;
        if (skuAdapters.size() < mDatas.size()) {
            Log.e(TAG, "1111");
            ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            scrollLinearLayoutManager.setScrollEnable(false);
            sureOrderSkuAdapter = new MakeSureOrderSkuAdapter(mContext, mBuyForCart, mDatas.get(pos).getNousemember().getTao_data());
            viewHolder.mRecycler.setLayoutManager(scrollLinearLayoutManager);
            viewHolder.mRecycler.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
            viewHolder.mRecycler.setAdapter(sureOrderSkuAdapter);

            skuAdapters.put(data.getHos_id(), sureOrderSkuAdapter);
        } else {
            Log.e(TAG, "2222");
            sureOrderSkuAdapter = skuAdapters.get(data.getHos_id());
            viewHolder.mRecycler.setAdapter(sureOrderSkuAdapter);
        }

        //监听
        viewHolder.initListener();

        //医院名称
        viewHolder.mHosName.setText(data.getHos_name());
        //订金小计
        Log.e(TAG, "dingjin == " + dingjin);
        viewHolder.mDeposit.setText("¥" + (dingjin + sureOrderSkuAdapter.getHosInsurance()));

        //设置尾款
        setWeikuanRedEnvelope(viewHolder, pos, true);
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mOrderView;                //item容器
        TextView mHosName;                      //医院名称
        RecyclerView mRecycler;                 //SKU列表
        EditText mLeaveMessage;                 //留言
        TextView mDeposit;                      //订金金额
        LinearLayout mBalancePaymentClick;      //尾款容器
        TextView mBalancePayment;               //尾款金额
        TextView mHosredDeduction;              //医院红包抵扣金额
//        RelativeLayout mPlatformSecurities;   //平台订单券容器
//        TextView mPlatformSecuritiesPrice; //平台订单券金额


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mOrderView = itemView.findViewById(R.id.make_sure_order_view);
            mHosName = itemView.findViewById(R.id.make_sure_view_hos_name);
            mRecycler = itemView.findViewById(R.id.make_sure_view_recycler);
            mLeaveMessage = itemView.findViewById(R.id.make_sure_view_leave_message);
            mDeposit = itemView.findViewById(R.id.make_sure_view_deposit);
            mBalancePayment = itemView.findViewById(R.id.make_sure_view_balance_payment);
            mHosredDeduction = itemView.findViewById(R.id.make_sure_view_hosred_deduction);
            mBalancePaymentClick = itemView.findViewById(R.id.make_sure_view_balance_payment_click);
//            mPlatformSecurities = itemView.findViewById(R.id.make_sure_order_coin_container);
//            mPlatformSecuritiesPrice = itemView.findViewById(R.id.make_sure_order_platform_securities_price);
        }

        private void initListener() {
            //保险回调
            skuAdapters.get(getMakeSureOrderSkuNumberData(getLayoutPosition()).getHos_id()).setOnEventClickListener(new MakeSureOrderSkuAdapter.OnEventClickListener() {
                @Override
                public void onInsuranceClick(InsuranceCheckedData data) {
                    if (onEventClickListener != null) {
                        data.setHosPos(getLayoutPosition());
                        onEventClickListener.onInsuranceClick(data);
                    }
                }

                @Override
                public void onNumberClick(int value) {
                    if (onEventClickListener != null) {
                        onEventClickListener.onNumberClick(value);
                    }
                }
            });

            //尾款回调
            mBalancePaymentClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onEventClickListener != null) {
                        MakeSureOrderSkuNumberData makeSureOrderSkuNumberData = getMakeSureOrderSkuNumberData(getLayoutPosition());

                        BalancePaymentData data = new BalancePaymentData();
                        data.setHosPos(getLayoutPosition());
                        data.setWk_coupons(makeSureOrderSkuNumberData.getWk_coupons());
                        data.setCoupons(makeSureOrderSkuNumberData.getCoupons());
                        onEventClickListener.onBalancePaymentClick(data);
                    }
                }
            });

            //平台定金券回调
//            mPlatformSecurities.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, OrderPreferentialActivity639.class);
//                    intent.putExtra("youhui_coupon", youHuiCoupons);                     //选中的
//                    intent.putParcelableArrayListExtra("youhui_coupons", getMakeSureOrderSkuNumberData());     //列表
//                    mContext.startActivityForResult(intent, REQUEST_CODE2);
//
//                }
//            });


            mLeaveMessage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    mDatas.get(getLayoutPosition()).setEditTextStr(mLeaveMessage.getText().toString());
                }
            });
        }
    }

    /**
     * 设置尾款红包
     *
     * @param viewHolder
     * @param isFirst    :是否是第一次加载
     */
    @SuppressLint("SetTextI18n")
    private void setWeikuanRedEnvelope(ViewHolder viewHolder, int pos, boolean isFirst) {
        MakeSureOrderSkuNumberData data = getMakeSureOrderSkuNumberData(pos);
        float weikuan = Float.parseFloat(data.getPay_weikaun());          //尾款价格
        if (mIsGroup) {
            viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._99));
            viewHolder.mHosredDeduction.setText("拼团暂不能使用尾款红包");
            //尾款小计
            viewHolder.mBalancePayment.setText("¥" + weikuan);
        } else {
            ArrayList<YouHuiCoupons> wkCoupons = data.getWk_coupons();         //尾款优惠劵列表
            YouHuiCoupons coupons = data.getCoupons();

            Collections.sort(wkCoupons, new WkCouponsRank());
            if (wkCoupons.size() > 0) {
                if (!isFirst) {
                    //非首次加载
                    if (TextUtils.isEmpty(coupons.getCard_id())) {
                        //没有选中优惠劵
                        viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                        viewHolder.mHosredDeduction.setText(wkCoupons.size() + "张医院尾款红包可用");
                        //尾款小计
                        viewHolder.mBalancePayment.setText("¥" + weikuan);
                    } else {
                        if (weikuan > 0) {
                            setWeiKuan(viewHolder, pos, weikuan, wkCoupons);
                        } else {
                            //没有选中优惠劵
                            viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                            viewHolder.mHosredDeduction.setText(wkCoupons.size() + "张医院尾款红包可用");
                            //尾款小计
                            viewHolder.mBalancePayment.setText("¥" + weikuan);
                        }
                    }
                } else {
                    //首次加载
                    if (weikuan > 0) {
                        setWeiKuan(viewHolder, pos, weikuan, wkCoupons);
                    } else {
                        //没有选中优惠劵
                        viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._33));
                        viewHolder.mHosredDeduction.setText(wkCoupons.size() + "张医院尾款红包可用");
                        //尾款小计
                        viewHolder.mBalancePayment.setText("¥" + weikuan);
                    }
                }
            } else {
                viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._99));
                viewHolder.mHosredDeduction.setText("暂无医院尾款红包可用");
                //尾款小计
                viewHolder.mBalancePayment.setText("¥" + weikuan);
            }
        }
    }

    /**
     * 设置尾款
     *
     * @param viewHolder
     * @param pos
     * @param weikuan
     * @param wkCoupons
     */
    @SuppressLint("SetTextI18n")
    private void setWeiKuan(ViewHolder viewHolder, int pos, float weikuan, ArrayList<YouHuiCoupons> wkCoupons) {
        YouHuiCoupons touHuiCoupons = wkCoupons.get(0);
        float hosRedEnvelope = Float.parseFloat(touHuiCoupons.getMoney());
        viewHolder.mHosredDeduction.setTextColor(Utils.getLocalColor(mContext, R.color._33));
        viewHolder.mHosredDeduction.setText("医院红包抵扣¥" + hosRedEnvelope);
        //尾款小计
        float weikuanPrice = weikuan - hosRedEnvelope;
        viewHolder.mBalancePayment.setText("¥" + (weikuanPrice < 0 ? 0 : weikuanPrice));
        //设置选中的尾款劵
        getMakeSureOrderSkuNumberData(pos).setCoupons(touHuiCoupons);
    }

    /**
     * 获取要使用的数据
     *
     * @param pos
     * @return
     */
    public MakeSureOrderSkuNumberData getMakeSureOrderSkuNumberData(int pos) {
        MakeSureOrderSkuNumberData data;
        if (mIsVipOrder) {
            //开通会员使用
            data = mDatas.get(pos).getUsemember();
        } else {
            //不开会员使用
            data = mDatas.get(pos).getNousemember();
            Log.e(TAG, "data === " + data.getTao_data().size());
        }
        return data;
    }

    /**
     * 设置尾款优惠
     *
     * @param hos_pos
     * @param wkqCoupon
     */
    public void setWkYouhui(int hos_pos, YouHuiCoupons wkqCoupon) {
        getMakeSureOrderSkuNumberData(hos_pos).setCoupons(wkqCoupon);
        notifyItemChanged(hos_pos, WEIKUAN);
    }

    /**
     * 开通或者关闭会员
     *
     * @param isVip true:开通，false：关闭
     */
    public void openOrDownVip(boolean isVip) {
        mIsVipOrder = isVip;
        notifyDataSetChanged();
    }

    /**
     * 计算订金价格
     *
     * @return
     */
    public float allDeposit() {
        float allDeposit = 0;
        for (int i = 0; i < mDatas.size(); i++) {
            MakeSureOrderSkuNumberData data = getMakeSureOrderSkuNumberData(i);
            Log.e(TAG, "data.getHos_id() === " + data.getHos_id());
            Log.e(TAG, "data.getHos_name() === " + data.getHos_name());
            Log.e(TAG, "data.getPay_dingjin() === " + data.getPay_dingjin());
            float dingjin = Float.parseFloat(data.getPay_dingjin());  //订金价格
            allDeposit += dingjin;
        }

        return allDeposit;
    }

    public List<MakeSureOrderSkuData> getmDatas() {
        return mDatas;
    }

    //回调
    private OnEventClickListener onEventClickListener;

    public interface OnEventClickListener {
        void onInsuranceClick(InsuranceCheckedData data);

        void onNumberClick(int value);

        void onBalancePaymentClick(BalancePaymentData data);
    }

    public void setOnEventClickListener(OnEventClickListener onEventClickListener) {
        this.onEventClickListener = onEventClickListener;
    }

}
