package com.module.shopping.controller.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.module.MainTableActivity;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.recyclerlodemore.LoadMoreRecyclerView;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.commonview.view.ScrollLayoutManager;
import com.module.community.model.bean.ExposureLoginData;
import com.module.community.other.StaggeredItemDecoretion;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebUtil;
import com.module.community.web.WebViewUrlLoading;
import com.module.other.activity.ProjectContrastActivity;
import com.module.other.netWork.netWork.ServerData;
import com.module.shopping.controller.adapter.ShoppingCarRecommendAdapter;
import com.module.shopping.controller.adapter.ShoppingCartAdapter;
import com.module.shopping.controller.adapter.ShoppingCartSkuAdapter;
import com.module.shopping.model.api.ShopcarRcommendApi;
import com.module.shopping.model.api.ShoppingCartApi;
import com.module.shopping.model.api.ShoppingCartDeleteApi;
import com.module.shopping.model.api.ShoppingCartNumberApi;
import com.module.shopping.model.api.ShoppingCartPreferentialApi;
import com.module.shopping.model.api.ShoppingCartSelectedApi;
import com.module.shopping.model.bean.CartDeleteData;
import com.module.shopping.model.bean.CartNumberData;
import com.module.shopping.model.bean.CartSelectedData;
import com.module.shopping.model.bean.PreferentialRank;
import com.module.shopping.model.bean.ShopCarBean;
import com.module.shopping.model.bean.ShopCarCounpos;
import com.module.shopping.model.bean.ShoppingCartData;
import com.module.shopping.model.bean.ShoppingCartPreferentialData;
import com.module.shopping.model.bean.ShoppingCartSkuData;
import com.module.shopping.view.HeaderAndFooterRecyclerViewAdapter;
import com.module.shopping.view.RecyclerViewUtils;
import com.module.shopping.view.SlideLayout;
import com.module.taodetail.model.bean.HomeTaoData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.TimerTextView;
import com.quicklyask.view.YueMeiDialog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

/**
 * 购物车页面
 *
 * @author 裴成浩
 */
public class ShoppingCartActivity extends YMBaseActivity {

    @BindView(R.id.shopping_cart_top)
    CommonTopBar mTop;                              //头部
    @BindView(R.id.shopping_cart_yes)
    LinearLayout shoppingCartYes;                   //购物车有数据
    @BindView(R.id.shopping_cart_no)
    LinearLayout shoppingCartNo;                    //购物车无数据
    @BindView(R.id.shopping_cart_prompt)
    TextView mPrompt;                               //降价提示
    @BindView(R.id.shopping_cart_refresh)
    SmartRefreshLayout mPullRefresh;                //刷新
    @BindView(R.id.shopping_cart_hos_sku)
    RecyclerView mHosSku;                           //SKU列表
    @BindView(R.id.shopping_cart_selected)
    CheckBox mSelected;                             //全选
    @BindView(R.id.shopping_cart_preferential)
    TextView mPreferential;                         //满减
    @BindView(R.id.shopping_cart_allprice_container)
    LinearLayout mAllpriceContainer;               //订单合计容器
    @BindView(R.id.shopping_cart_allprice)
    TextView mAllprice;                             //订单合计
    @BindView(R.id.shopping_cart_settle_accounts)
    TextView mSettleAccounts;                         //去结算
    @BindView(R.id.unlogin_layout)
    RelativeLayout mUnLoginLayout;                         //未登录提示
    @BindView(R.id.shopping_unlogin_btn)
    Button mUnLoginBtn;                         //未登录提示按钮
    @BindView(R.id.btn_gonshop)
    Button mButtonGoshop;                         //去逛逛按钮
    @BindView(R.id.shopping_car_coupons_container)
    LinearLayout mShopCarCouponsContainer;                         //优惠券容器
    @BindView(R.id.shopping_car_coupons_price)
    TextView mShopCarCouponsprice;                         //优惠券价格
    @BindView(R.id.shopping_car_coupons_desc)
    TextView mShopCarCouponsDesc;                         //优惠券描述
    @BindView(R.id.shopping_car_coupons_btn)
    Button mShopCarCouponsBtn;                         //优惠券领劵按钮
    @BindView(R.id.shopping_car_notdataz_img)
    ImageView mShopCarNoDataImg;                         //优惠券没有数据图片
    @BindView(R.id.shopping_car_notdata_text)
    TextView mShopCarNoDataText;                         //优惠券没有数据文字
    @BindView(R.id.shopping_car_notdata_desc)
    TextView mShopCarNoDataDesc;                         //优惠券没有数据文字描述
    @BindView(R.id.shopping_car_recommend_list)
    RecyclerView mShopCarRecommendList;                         //优惠券推荐列表
    @BindView(R.id.shopping_car_count_visorgone)
    RelativeLayout mShopCarCountVisorgone;                         //购物车底部领劵显示或隐藏
    @BindView(R.id.shopping_car_coupons)
    TextView mShopCarText;
    @BindView(R.id.shopping_car_countdown_time)
    TimerTextView mShopCarCountDownTime;
    private String TAG = "ShoppingCartActivity";
    private ShoppingCartApi shoppingCartApi;                            //获取购物车数据
    private ShoppingCartDeleteApi shoppingCartDeleteApi;                //删除购物车数据
    private ShoppingCartNumberApi shoppingCartNumberApi;                //购物车数据加减操作
    private ShoppingCartSelectedApi shoppingCartSelectedApi;            //购物车选中/不选中操作
    private ShoppingCartPreferentialApi shoppingCartPreferentialApi;    //购物车优惠劵获取
    private ShoppingCartAdapter mShoppingCartAdapter;
    private YueMeiDialog yueMeiDialog;                          //提示框
    private DeleteDialogClickListener deleteDialogClickListener; //删除回调类
    private String tao_id;
    private ShopcarRcommendApi mShopcarRcommendApi;
    private ShopCarCounpos mCoupons;
    private ArrayList<HomeTaoData> mData;//推荐数据
    private ShoppingCarRecommendAdapter mRecommendAdapter;
    private RecyclerView mRecommendList;
    private HeaderAndFooterRecyclerViewAdapter mHeaderAndFooterRecyclerViewAdapter = null;
    private ShopCarBean mShopCarBean;
    private boolean isShowCoupons = false;//是否显示优惠券横条

    @Override
    protected int getLayoutId() {
        return R.layout.activity_shopping_cart;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mTop.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;
        //提示框
        yueMeiDialog = new YueMeiDialog(mContext, "好东西很抢手，确定要删除？", "再想想", "删除");
        deleteDialogClickListener = new DeleteDialogClickListener();
        yueMeiDialog.setBtnClickListener(deleteDialogClickListener);

        //管理完成的点击事件
        mTop.setRightTextVisibility(View.VISIBLE);
        mTop.setBottomLineVisibility(View.GONE);
        upUITopRightImg();
        mTop.setRightImageClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                //添加埋点
//                购物车详情页{移动端自行拼装数据}
//                我的对比页-淘整形详情
//                if (mShoppingCartAdapter != null && mShoppingCartAdapter.haveSelected()) {

                ArrayList<String> taoidList = new ArrayList<>();
                if (mShoppingCartAdapter != null) {
                    for (int i = 0; i < mShoppingCartAdapter.getmDatas().size(); i++) {
                        for (int j = 0; j < mShoppingCartAdapter.getmDatas().get(i).getData().size(); j++) {
                            Log.i("tao_id", mShoppingCartAdapter.getmDatas().get(i).getData().get(j).getTao_id());
                            if (mShoppingCartAdapter.getmDatas().get(i).getData().get(j).getSelected().equals("1")) {
                                taoidList.add(mShoppingCartAdapter.getmDatas().get(i).getData().get(j).getTao_id());
                            }
                        }
                    }
                }
                tao_id = StringUtils.strip(taoidList.toString(), "[]").trim();
                //埋点
                HashMap event_params = new HashMap();
                event_params.put("to_page_type", "167");
                event_params.put("event_others", tao_id);
                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CART_TO_TAO_PK), event_params, new ActivityTypeData("93"));
                //跳转对比页
                ProjectContrastActivity.invoke(mContext, tao_id, "3");
                tao_id = "";
//                }
            }
        });
        mTop.setRightTextClickListener(new CommonTopBar.ClickCallBack() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "管理点击事件");
                if ("管理".equals(mTop.getTv_right().getText().toString())) {
                    if (mShoppingCartAdapter != null) {
                        mShoppingCartAdapter.cartState(true);
                        mSelected.setChecked(false);
                    }
                    mTop.setRightText("完成");
                    mSettleAccounts.setText("删除");
                    mAllpriceContainer.setVisibility(View.INVISIBLE);
                    mShopCarCountVisorgone.setVisibility(View.GONE);
                } else {
                    if (mShoppingCartAdapter != null) {
                        mShoppingCartAdapter.cartState(false);
                        mSelected.setChecked(mShoppingCartAdapter.allSelected());
                    }
                    mTop.setRightText("管理");
                    mSettleAccounts.setText("去结算");
                    mAllpriceContainer.setVisibility(View.VISIBLE);
                    if (isShowCoupons) {
                        mShopCarCountVisorgone.setVisibility(View.VISIBLE);
                    } else {
                        mShopCarCountVisorgone.setVisibility(View.GONE);
                    }
                }
            }
        });
        mUnLoginLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.isLoginAndBind(mContext);
            }
        });
        mUnLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.isLoginAndBind(mContext);
            }
        });
        mButtonGoshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCoupons != null) {
                    String isGet = mCoupons.getIs_get();
                    if ("1".equals(isGet)) {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mCoupons.getUse_url());
                    } else {
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(mCoupons.getReceive_url());
                    }
                } else {
                    MainTableActivity.mainBottomBar.setCheckedPos(1);
                    Intent intent447 = new Intent(mContext, MainTableActivity.class);
                    startActivity(intent447);
                    finish();
                }

            }
        });

        //刷新
        mPullRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadingData();
            }
        });

        setMultiOnClickListener(mSettleAccounts);
        ExposureLoginData exposureLoginData = new ExposureLoginData("93", "0");
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, new Gson().toJson(exposureLoginData));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveMsg(String refresh) {
        Log.e(TAG, "refresh ==" + refresh);
        switch (refresh) {
            case "refresh":
                loadRemmendData();
                break;
            case "6022":
                loadingData();
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.isLogin()) {
            mUnLoginLayout.setVisibility(View.VISIBLE);
        } else {
            mUnLoginLayout.setVisibility(View.GONE);
        }
        loadRemmendData();
    }

    private void upUITopRightImg() {
        mTop.setRightImgVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mTop.getIv_right().getLayoutParams();
        params.width = DensityUtil.dip2px(30);
        params.height = DensityUtil.dip2px(30);
        mTop.getIv_right().setLayoutParams(params);
//        if (mShoppingCartAdapter != null && mShoppingCartAdapter.haveSelected()) {
        mTop.setRightImgSrc(R.drawable.pk_shopping_no_backgroud);
//        }else{
//            mTop.setRightImgSrc(R.drawable.pk);
//        }
    }

    @Override
    protected void initData() {
        shoppingCartApi = new ShoppingCartApi();
        shoppingCartDeleteApi = new ShoppingCartDeleteApi();
        shoppingCartNumberApi = new ShoppingCartNumberApi();
        shoppingCartSelectedApi = new ShoppingCartSelectedApi();
        shoppingCartPreferentialApi = new ShoppingCartPreferentialApi();
        mShopcarRcommendApi = new ShopcarRcommendApi();
        loadRemmendData();//加载推荐数据

    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        loadRemmendData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.shopping_cart_settle_accounts:    //去结算
                if ("去结算".equals(mSettleAccounts.getText().toString())) {
                    if (Utils.isLoginAndBind(mContext)) {
                        if (mShoppingCartAdapter.haveSelected()) {
                            Bundle bundle = new Bundle();
                            bundle.putString("buy_for_cart", "1");
                            mFunctionManager.goToActivity(MakeSureOrderActivity.class, bundle);
                        } else {
                            mFunctionManager.showShort("还没有选择商品哦");
                        }
                    }
                } else {
                    List<CartDeleteData> cartDeleteData = getCartDeleteData();
                    if (cartDeleteData.size() > 0) {
                        deleteDialogClickListener.data = getCartDeleteData();
                        yueMeiDialog.show();
                    } else {
                        mFunctionManager.showShort("没有选中删除项");
                    }

                }
                break;
        }
    }

    /**
     * 获取购物车信息
     */
    private void loadingData() {
        shoppingCartApi.getCallBack(mContext, shoppingCartApi.getmShoppingCartHashMap(), new BaseCallBackListener<ServerData>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(ServerData serverData) {
                mPullRefresh.finishRefresh();
                if ("1".equals(serverData.code)) {

                    mShopCarBean = JSONUtil.TransformSingleBean(serverData.data, ShopCarBean.class);
                    mCoupons = mShopCarBean.getCoupons();
                    if (mCoupons != null) {
                        mShopCarCouponsContainer.setVisibility(View.VISIBLE);
                        mShopCarCouponsprice.setText(mCoupons.getMoney());
                        mShopCarCouponsDesc.setText(mCoupons.getUse_text());
                        final String isGet = mCoupons.getIs_get();
                        if ("1".equals(isGet)) {
                            mShopCarCouponsBtn.setText("去使用");
                        } else {
                            mShopCarCouponsBtn.setText("立即领取");
                        }
                        YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.CART_COUPONS_EXPOSURE));
                        mShopCarCouponsBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if ("1".equals(isGet)) {
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(mCoupons.getUse_url());
                                } else {
                                    WebUrlTypeUtil.getInstance(mContext).urlToApp(mCoupons.getReceive_url());
                                }
                            }
                        });
                    } else {
                        mShopCarCouponsContainer.setVisibility(View.GONE);
                    }
                    List<ShoppingCartData> data = mShopCarBean.getTao_list();
                    //数据是否存在
                    if (data != null && data.size() > 0) {
                        mTop.setRightTextVisibility(View.VISIBLE);
                        shoppingCartYes.setVisibility(View.VISIBLE);
                        shoppingCartNo.setVisibility(View.GONE);
                        final ScrollLayoutManager scrollLinearLayoutManager = new ScrollLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
                        mHosSku.setLayoutManager(scrollLinearLayoutManager);
                        mShoppingCartAdapter = new ShoppingCartAdapter(mContext, data);
                        mHosSku.setAdapter(mShoppingCartAdapter);
                        if (CollectionUtils.isNotEmpty(mData)) {
                            mShoppingCartAdapter.setData(mData);
                        }
                        setTopText();

                        //判断是否全部选中了
                        mSelected.setChecked(mShoppingCartAdapter.allSelected());
                        //订单总金额和订金劵满减优惠
                        preferentialData();

                        //是否全选回调
                        mShoppingCartAdapter.setOnEventClickListener(new ShoppingCartAdapter.OnEventClickListener() {

                            @Override
                            public void onSelectedClick(boolean allCheck, CartSelectedData data) {
                                mSelected.setChecked(allCheck);
                                selectedData(data);
                                upUITopRightImg();
                            }

                            @Override
                            public void onDeleteClick(List<CartDeleteData> datas) {
                                deleteDialogClickListener.data = datas;
                                yueMeiDialog.show();
                            }

                            @Override
                            public void onNumberClick(CartNumberData data) {
                                numberData(data);
                                upUITopRightImg();
                            }
                        });

                        //全选按钮点击
                        mSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (buttonView.isPressed()) {
                                    mShoppingCartAdapter.allSelected(isChecked);

                                    CartSelectedData cartSelectedData = new CartSelectedData();
                                    cartSelectedData.setFlag("1");
                                    if (isChecked) {
                                        cartSelectedData.setSelect("1");
                                    } else {
                                        cartSelectedData.setSelect("0");
                                    }
                                    selectedData(cartSelectedData);
                                }
                                upUITopRightImg();
                            }
                        });

                        //滚动
                        mHosSku.addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                super.onScrollStateChanged(recyclerView, newState);
                                SlideLayout slideLayout = mShoppingCartAdapter.getStateChangeListener().getSlideLayout();
                                Log.e(TAG, "newState == " + newState);
                                if (newState == 1 && slideLayout != null) {
                                    slideLayout.closeMenu();
                                }
                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                            }
                        });
                        upUITopRightImg();
                    } else {
                        shoppingCartYes.setVisibility(View.GONE);
                        shoppingCartNo.setVisibility(View.VISIBLE);
                        mTop.setRightTextVisibility(View.GONE);
                        setNoDataText();
                    }
                } else {

                }


            }
        });
    }

    /**
     * 设置没有数据的文案
     */
    private void setNoDataText() {
        ShopCarCounpos carBeanCoupons = mShopCarBean.getCoupons();
        if (carBeanCoupons != null && !TextUtils.isEmpty(carBeanCoupons.getMoney()) && !"0".equals(carBeanCoupons.getMoney())) {
            ViewGroup.LayoutParams layoutParams = mShopCarNoDataImg.getLayoutParams();
            layoutParams.height = Utils.dip2px(66);
            layoutParams.width = Utils.dip2px(66);
            mShopCarNoDataImg.setLayoutParams(layoutParams);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) mShopCarNoDataImg.getLayoutParams();
            marginLayoutParams.topMargin = Utils.dip2px(22);
            mShopCarNoDataImg.setLayoutParams(marginLayoutParams);
            mShopCarNoDataText.setTextColor(ContextCompat.getColor(mContext, R.color._66));
            mShopCarNoDataDesc.setVisibility(View.VISIBLE);

        } else {
            ViewGroup.LayoutParams layoutParams = mShopCarNoDataImg.getLayoutParams();
            layoutParams.height = Utils.dip2px(120);
            layoutParams.width = Utils.dip2px(118);
            mShopCarNoDataImg.setLayoutParams(layoutParams);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) mShopCarNoDataImg.getLayoutParams();
            marginLayoutParams.topMargin = Utils.dip2px(44);
            mShopCarNoDataImg.setLayoutParams(marginLayoutParams);
            mShopCarNoDataText.setTextColor(ContextCompat.getColor(mContext, R.color._99));
            mShopCarNoDataDesc.setVisibility(View.GONE);
        }

    }

    private boolean isAddDecoretion = false;//是否已经添加过分割线

    /**
     * 购物车推荐数据
     */
    private void loadRemmendData() {
        mShopcarRcommendApi.getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if ("1".equals(serverData.code)) {
                    mData = JSONUtil.jsonToArrayList(serverData.data, HomeTaoData.class);
                    loadingData();
                    if (CollectionUtils.isNotEmpty(mData)) {
                        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
//                        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                        mShopCarRecommendList.setItemAnimator(null);
                        mShopCarRecommendList.setLayoutManager(staggeredGridLayoutManager);
                        mShopCarRecommendList.setHasFixedSize(true);
                        mShopCarRecommendList.setNestedScrollingEnabled(false);
                        mShopCarRecommendList.setFocusableInTouchMode(false);
                        StaggeredItemDecoretion staggeredItemDecoretion = new StaggeredItemDecoretion();
                        if (!isAddDecoretion) {
                            mShopCarRecommendList.addItemDecoration(staggeredItemDecoretion);
                            isAddDecoretion = true;
                        }
                        ShoppingCarRecommendAdapter recommendAdapter = new ShoppingCarRecommendAdapter(R.layout.item_tao_staggered_view, mData);
                        mShopCarRecommendList.setAdapter(recommendAdapter);
                        recommendAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                                Intent intent = new Intent(mContext, TaoDetailActivity.class);
                                intent.putExtra("id", mData.get(position).getId());
                                intent.putExtra("source", "0");
                                intent.putExtra("objid", "0");
                                startActivity(intent);
                            }
                        });
                    }
                } else {
                    loadingData();
                }
            }
        });
    }

    /**
     * 购物车选中/不选中操作
     *
     * @param data
     */
    private void selectedData(CartSelectedData data) {
        Log.e(TAG, "data.getCart_id() == " + data.getCart_id());
        Log.e(TAG, "data.getFlag() == " + data.getFlag());
        Log.e(TAG, "data.getHos_id() == " + data.getHos_id());
        Log.e(TAG, "data.getSelect() == " + data.getSelect());
        final int hosPos = data.getHosPos();

        switch (data.getFlag()) {
            case "0":                               //单条购物车
                shoppingCartSelectedApi.addData("cart_id", data.getCart_id());
                shoppingCartSelectedApi.addData("hos_id", "");
                break;
            case "1":                               //用户所有购物车
//                shoppingCartSelectedApi.addData("cart_id", data.getCart_id());
//                shoppingCartSelectedApi.addData("hos_id", "");
                break;
            case "2":                               //医院sku
                shoppingCartSelectedApi.addData("cart_id", "");
                shoppingCartSelectedApi.addData("hos_id", data.getHos_id());
                break;
        }
        shoppingCartSelectedApi.addData("flag", data.getFlag());
        shoppingCartSelectedApi.addData("select", data.getSelect());
        shoppingCartSelectedApi.getCallBack(mContext, shoppingCartSelectedApi.getmShoppingCartSelectedHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                Log.e(TAG, "选中操作serverData == " + serverData.toString());
                if ("1".equals(serverData.code)) {
                    mShoppingCartAdapter.notifyItemLocal(hosPos, ShoppingCartAdapter.RED_PACKAGE);
                    preferentialData();
                }
            }
        });
    }

    /**
     * 删除购物车数据联网请求
     *
     * @param datas
     */
    private void deleteData(List<CartDeleteData> datas) {
        final ArrayList<String> list = new ArrayList<>();
        for (CartDeleteData data : datas) {
            list.add(data.getCart_id());
        }

        String cartJson = new Gson().toJson(list);
        Log.e(TAG, "cartJson == " + cartJson);
        shoppingCartDeleteApi.addData("cart_json", cartJson);
        shoppingCartDeleteApi.getCallBack(mContext, shoppingCartDeleteApi.getmShoppingCartDeleteHashMap(), new BaseCallBackListener<List<ShoppingCartData>>() {
            @Override
            public void onSuccess(List<ShoppingCartData> data) {
                Log.e(TAG, "删除数据 = " + data.toString());
                Utils.getCartNumber(mContext);
                //购物车订金劵获取
                preferentialData();
            }
        });

        deleteCartData(datas);
    }

    /**
     * 删除购物车数据
     *
     * @param datas
     */
    private void deleteCartData(List<CartDeleteData> datas) {
        //循环删除所有的
        for (CartDeleteData cartDeleteData : datas) {
            String hos_id = cartDeleteData.getHos_id();
            String sku_id = cartDeleteData.getSku_id();

            //如果医院只有一条SKU直接删除医院这一条
            Log.e(TAG, "hos_id == " + hos_id);

            ShoppingCartSkuAdapter skuAdapters = mShoppingCartAdapter.getSkuIdAdapters(hos_id);
            if (skuAdapters != null) {
                if (skuAdapters.getDatas().size() == 1) {
                    mShoppingCartAdapter.deleteData(hos_id);

                    //删除后购物车所有数据是否是全部选中的
                    mSelected.setChecked(mShoppingCartAdapter.allSelected());

                } else {
                    skuAdapters.deleteData(sku_id);

                    //删除后购物车所有数据是否是全部选中的
                    mSelected.setChecked(mShoppingCartAdapter.allSelected());

                    //局部刷新医院红包最优惠数据
                    mShoppingCartAdapter.notifyItemLocal(hos_id, ShoppingCartAdapter.RED_PACKAGE);
                    //删除后当前医院是否是全部选中的
                    mShoppingCartAdapter.notifyItemLocal(hos_id, ShoppingCartAdapter.SELECTED_BUTTON);
                }
            } else {
                for (int i = 0; i < mShoppingCartAdapter.getmDatas().size(); i++) {
                    if (mShoppingCartAdapter.getmDatas().get(i).getHos_id().equals(hos_id)) {
                        if (mShoppingCartAdapter.getmDatas().get(i).getData().size() == 1) {
                            mShoppingCartAdapter.deleteData(hos_id);

                            //删除后购物车所有数据是否是全部选中的
                            mSelected.setChecked(mShoppingCartAdapter.allSelected());


                        } else {

                            //删除购物车的数据
                            for (int i1 = 0; i1 < mShoppingCartAdapter.getmDatas().get(i).getData().size(); i1++) {
                                if (mShoppingCartAdapter.getmDatas().get(i).getData().get(i1).getTao_id().equals(sku_id)) {
                                    mShoppingCartAdapter.getmDatas().get(i).getData().remove(i1);
                                    break;
                                }
                            }

                            //删除后购物车所有数据是否是全部选中的
                            mSelected.setChecked(mShoppingCartAdapter.allSelected());

                            //局部刷新医院红包最优惠数据
                            mShoppingCartAdapter.notifyItemLocal(hos_id, ShoppingCartAdapter.RED_PACKAGE);
                            //删除后当前医院是否是全部选中的
                            mShoppingCartAdapter.notifyItemLocal(hos_id, ShoppingCartAdapter.SELECTED_BUTTON);
                        }
                        break;
                    }
                }
            }

        }

        setTopText();

        //数据是否还存在
        if (mShoppingCartAdapter.getmDatas().size() > 0) {
            mTop.setRightTextVisibility(View.VISIBLE);
//            mTop.setRightImgVisibility(View.VISIBLE);
            shoppingCartYes.setVisibility(View.VISIBLE);
            shoppingCartNo.setVisibility(View.GONE);
        } else {
            mTop.setRightTextVisibility(View.GONE);
//            mTop.setRightImgVisibility(View.GONE);
            shoppingCartYes.setVisibility(View.GONE);
            shoppingCartNo.setVisibility(View.VISIBLE);
            setNoDataText();
        }
        upUITopRightImg();
    }

    /**
     * 购物车数据加减操作
     *
     * @param data
     */
    private void numberData(CartNumberData data) {
        String cart_id = data.getCart_id();
        String flag = data.getFlag();
        final int hosPos = data.getHosPos();
        final int skuPos = data.getSkuPos();
        int value = data.getValue();
        Log.e(TAG, "cart_id == " + cart_id);
        Log.e(TAG, "flag == " + flag);
        shoppingCartNumberApi.addData("cart_id", cart_id);
        shoppingCartNumberApi.addData("flag", flag);
        if (value > 0) {
            shoppingCartNumberApi.addData("number", value + "");
        }
        shoppingCartNumberApi.getCallBack(mContext, shoppingCartNumberApi.getmShoppingCartNumberHashMap(), new BaseCallBackListener<ShoppingCartSkuData>() {
            @Override
            public void onSuccess(ShoppingCartSkuData data) {
                Log.e(TAG, "购物车数据加减操作serverData == " + data.toString());
                Log.e(TAG, "data == " + data.getNumber());
                preferentialData();
                mShoppingCartAdapter.getSkuAdapters(hosPos).getDatas().set(skuPos, data);
                mShoppingCartAdapter.getSkuAdapters(hosPos).notifyItemChanged(skuPos);
                mShoppingCartAdapter.notifyItemLocal(hosPos, ShoppingCartAdapter.RED_PACKAGE);
            }
        });
    }

    /**
     * 购物车订金劵获取
     */
    private void preferentialData() {
        shoppingCartPreferentialApi.getCallBack(mContext, shoppingCartPreferentialApi.getHashMap(), new BaseCallBackListener<List<ShoppingCartPreferentialData>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess(List<ShoppingCartPreferentialData> datas) {
                Collections.sort(datas, new PreferentialRank());
                for (ShoppingCartPreferentialData data : datas) {
                    Log.e(TAG, "订金满" + data.getLowest_consumption() + "减" + data.getMoney());
                }

                if (datas.size() > 0) {
                    ShoppingCartPreferentialData data = datas.get(0);
                    mPreferential.setText("订金满" + data.getLowest_consumption() + "减" + data.getMoney());
                    float allPrice = mShoppingCartAdapter.getSelectedPrice() - Float.parseFloat(data.getMoney());
                    float mPrice3 = new BigDecimal(allPrice).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                    mAllprice.setText("¥" + (mPrice3 < 0 ? 0 : mPrice3));
                    String endTime = data.getEnd_time();
                    String endTimeAct = Utils.stampToPhpDate(endTime);
                    Date date = new Date();
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String currentTime = simpleDateFormat.format(date);
                    long[] timeSubCurr = Utils.getTimeSub(currentTime, endTimeAct);
                    long[] timesAct = {timeSubCurr[0], timeSubCurr[1], timeSubCurr[2], timeSubCurr[3]};
                    if (timesAct[0] <= 1L) {
                        mShopCarCountVisorgone.setVisibility(View.VISIBLE);
                        isShowCoupons = true;
                        mShopCarText.setText("满" + data.getLowest_consumption() + "减" + data.getMoney());
                        mShopCarCountDownTime.setTimes(timesAct);
                        mShopCarCountDownTime.setCopywriting("使用倒计时 ");
                        if (!mShopCarCountDownTime.isRun()) {
                            mShopCarCountDownTime.beginRun();
                        }
                    } else {
                        mShopCarCountVisorgone.setVisibility(View.GONE);
                        isShowCoupons = false;
                    }
                } else {
                    mPreferential.setText("");
                    float mPrice3 = new BigDecimal(mShoppingCartAdapter.getSelectedPrice()).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
                    mAllprice.setText("¥" + mPrice3);
                    mShopCarCountVisorgone.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 获取到购物车要删除的数据
     *
     * @return
     */
    @NonNull
    private List<CartDeleteData> getCartDeleteData() {
        List<CartDeleteData> datas = new ArrayList<>();
        if (mShoppingCartAdapter != null) {
            for (int i = 0; i < mShoppingCartAdapter.getmDatas().size(); i++) {
                ShoppingCartSkuAdapter skuAdapters = mShoppingCartAdapter.getSkuAdapters(i);
                //有选中的SKU
                if (skuAdapters != null) {
                    if (skuAdapters.isAllNotCheck()) {
                        for (int i1 = 0; i1 < skuAdapters.getDatas().size(); i1++) {
                            ShoppingCartSkuData shoppingCartSkuData = skuAdapters.getDatas().get(i1);
                            if ("1".equals(shoppingCartSkuData.getSelected())) {
                                CartDeleteData data = new CartDeleteData();
                                data.setHos_id(mShoppingCartAdapter.getmDatas().get(i).getHos_id());
                                data.setSku_id(shoppingCartSkuData.getTao_id());
                                data.setCart_id(shoppingCartSkuData.getCart_id());
                                datas.add(data);
                            }
                        }
                    }
                } else {
                    ShoppingCartData shoppingCartData = mShoppingCartAdapter.getmDatas().get(i);
                    for (int i1 = 0; i1 < shoppingCartData.getData().size(); i1++) {
                        ShoppingCartSkuData shoppingCartSkuData = shoppingCartData.getData().get(i1);
                        if ("1".equals(shoppingCartSkuData.getSelected())) {
                            CartDeleteData data = new CartDeleteData();
                            data.setHos_id(mShoppingCartAdapter.getmDatas().get(i).getHos_id());
                            data.setSku_id(shoppingCartSkuData.getTao_id());
                            data.setCart_id(shoppingCartSkuData.getCart_id());
                            datas.add(data);
                        }
                    }
                }
            }
        }
        return datas;
    }

    /**
     * 设置顶部文案
     */
    private void setTopText() {
        //判断商品库存是否小于10件
        if (mShoppingCartAdapter.isInventoryNumber()) {
            mPrompt.setVisibility(View.VISIBLE);
            mPrompt.setText("购物车中有商品快要卖光了，快去看看吧");
        } else {
            //判断商品价格下降是否还存在
            if (mShoppingCartAdapter.isPriceFalling()) {
                mPrompt.setVisibility(View.VISIBLE);
                mPrompt.setText("购物车有降价商品，快去看看吧");
            } else {
                mPrompt.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 删除回调
     */
    private class DeleteDialogClickListener implements YueMeiDialog.BtnClickListener {
        private List<CartDeleteData> data;

        @Override
        public void leftBtnClick() {
            yueMeiDialog.dismiss();
        }

        @Override
        public void rightBtnClick() {
            deleteData(data);
            yueMeiDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        Cfg.saveStr(mContext, FinalConstant.EXPOSURE_LOGIN, "");
    }
}
