package com.module;

import android.annotation.SuppressLint;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.util.Util;
import com.module.api.VersionApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.PrivacyAgreementDialog;
import com.module.commonview.broadcast.RefreshMessageReceiver;
import com.module.commonview.broadcast.SendMessageReceiver;
import com.module.commonview.chatnet.IMManager;
import com.module.commonview.chatnet.NetBroadCastReceiver;
import com.module.commonview.chatnet.NetEvent;
import com.module.commonview.chatnet.NetStatus;
import com.module.commonview.module.api.BBsDetailUserInfoApi;
import com.module.commonview.view.MainTableButtonView;
import com.module.commonview.view.MyBDLocationListener;
import com.module.community.controller.other.YueMeiQtCallBack;
import com.module.community.model.bean.HuangDeng;
import com.module.community.model.bean.ZhuanTi;
import com.module.community.statistical.StatisticalPath;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.community.web.WebViewUrlLoading;
import com.module.event.HomeEvent;
import com.module.event.VoteMsgEvent;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.home.model.api.MessagecountApi;
import com.module.my.model.api.ShenHeApi;
import com.module.my.model.bean.NoLoginBean;
import com.module.my.model.bean.ShenHeData;
import com.module.other.netWork.imageLoaderUtil.GlideRoundTransform;
import com.module.other.netWork.netWork.FinalConstant1;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.q.Qt;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.AdertAdv;
import com.quicklyask.entity.NewsNumberData;
import com.quicklyask.entity.SneakGuest;
import com.quicklyask.entity.VersionJCData;
import com.quicklyask.service.NotificationService;
import com.quicklyask.util.AppShortCutUtil;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;
import com.quicklyask.view.AlertPopWindow;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.NewUserPopWindow;
import com.quicklyask.view.ProgDialog;
import com.quicklyask.view.YueMeiDialog;
import com.tendcloud.tenddata.TCAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.ui.KJActivityManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.jiguang.verifysdk.api.JVerificationInterface;
import cn.jiguang.verifysdk.api.RequestCallback;


@SuppressWarnings("deprecation")
public class MainTableActivity extends TabActivity implements NetEvent {

    private static final String TAG = "MainTableActivity";
    public static MainTableActivity mContext;
    private LinearLayout contentLy;
    private LinearLayout popwinView;


    // 版本升级
    private final int UPDATA_NONEED = 0;
    private final int UPDATA_CLIENT = 1;
    private final int GET_UNDATAINFO_ERROR = 2;
    private final int SDCARD_NOMOUNTED = 3;
    private final int DOWN_ERROR = 4;
    private static boolean interceptFlag = false;
    public static int acquiescencePage = 0;
    private String apkUrl;
    private int vserInt;
    private String vserState = "";
    private String isPopUpdate = "0";


    private List<HuangDeng> listTitle;

    private MessagecountApi mMainTableApi;
    private LocationClient locationClient;
    private int type;
    public static RefreshMessageReceiver mBroadcastReceiver;
    public static MainTableButtonView mainBottomBar;
    public static String tancengUrl = "";
    private NewUserPopWindow mNewUserPopWindow;                         //新人弹窗
    private AlertPopWindow alertPop;                                    //大促
    public String mTabCa = "0";   //默认选中社区的tab
    private SendMessageReceiver mSendMessageReceiver;
    private MessageFragmentActivity1 mMessageFragmentActivity1;
    private TabHost tabHost;
    private ArrayList<String> hotCity = new ArrayList<>(Arrays.asList("北京", "上海", "广州", "成都", "深圳", "南京", "杭州", "重庆", "武汉", "郑州", "西安", "长沙"));
    private String bottomViewData;
    private String mNoLoginId;

    public static void setNetStatus(NetStatus netStatus) {
        mNetStatus = netStatus;
    }

    public static NetStatus mNetStatus;
    /**
     * 监控网络的广播
     */
    private NetBroadCastReceiver netBroadcastReceiver;

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(HomeEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 1:
                mainBottomBar.setVisibility(View.GONE);
                break;
            case 2:
                mainBottomBar.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.acty_main_table);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mContext = MainTableActivity.this;
        QMUIStatusBarHelper.translucent(mContext);

        acquiescencePage = getIntent().getIntExtra("acquiescencePage", 0);

        tabHost = findViewById(android.R.id.tabhost);
        mainBottomBar = findViewById(R.id.main_bottom_bar);
        contentLy = findViewById(R.id.content_ly);
        popwinView = findViewById(R.id.potential_customer_popwin_view);

        //判断底部导航状态
        try {
            bottomViewData = Cfg.loadStr(mContext, FinalConstant.START_TABBAR, "");
            if (!TextUtils.isEmpty(bottomViewData)) {
                AdertAdv advertData = JSONUtil.TransformSingleBean(bottomViewData, AdertAdv.class);
                mainBottomBar.initNetworkView(tabHost, advertData.getTabbar());
            } else {
                String city = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
                if (hotCity.contains(city)) {
                    mainBottomBar.initDefaultView(tabHost, city, true);
                } else {
                    mainBottomBar.initDefaultView(tabHost, "值得买", false);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "e1 == " + e.toString());
            mainBottomBar.initDefaultView(tabHost, "值得买", false);
        }

        MessageFragmentActivity1.setmActivityCallBack(new MessageFragmentActivity1.ActivityCallBack() {
            @Override
            public void activityCallBack(MessageFragmentActivity1 messageFragmentActivity1) {
                mMessageFragmentActivity1 = messageFragmentActivity1;
            }
        });
        initShenHe();
        initReceiver();
        mNoLoginId = Cfg.loadStr(mContext, SplashActivity.NO_LOGIN_ID, "0");

        String action = getIntent().getAction();
        //判断是自己app跳转的，还是其他app跳转的
        Log.e(TAG, "action == " + action);
        if (!TextUtils.isEmpty(action) && Intent.ACTION_VIEW.equals(action)) {
            Uri uri = getIntent().getData();
            Log.e(TAG, "uri == " + uri);
            //获取指定参数值
            if (uri != null) {
                if (uri.toString().contains("yuemei.com/mycoupons")) {
                    //要跳转到优惠劵的
                    this.type = 0;
                    String uriType = uri.getQueryParameter("type");
                    String link = uri.getQueryParameter("link");
                    String eventName = uri.getQueryParameter("event_name");
                    String eventOthers = uri.getQueryParameter("event_others");
                    String u = uri.getQueryParameter("u");
                    Log.e(TAG, "eventName == " + eventName);
                    Log.e(TAG, "eventOthers == " + eventOthers);
                    Log.e(TAG, "link == " + link);

//                    https://user.yuemei.com/coupons/mmycoupons/?event_name=sms_open_app&event_others=3
                    if (!TextUtils.isEmpty(link)) {
                        String url = link;
                        String and = "&";
                        if (!TextUtils.isEmpty(u)) {
                            url += "?u=" + u;
                        } else {
                            and = "?";
                        }
                        if (!TextUtils.isEmpty(eventName)) {
                            url += and + "event_name=" + eventName;
                        }
                        if (!TextUtils.isEmpty(eventOthers)) {
                            url += "&event_others=" + eventOthers;
                        }
                        Log.e(TAG, "url == " + url);
                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
                    }
//
                }
//                else if (uri.toString().contains("yuemei.com/tao_zt")) {
////                    https://tao.yuemei.com/tao_zt/11686.html?u=9100341
//                    //专题
//                    String uriType = uri.getQueryParameter("type");
//                    String link = uri.getQueryParameter("link");
//                    String u = uri.getQueryParameter("u");
//
//                    if (!TextUtils.isEmpty(link)) {
//                        String url = link;
//                        String and = "&";
//                        if (!TextUtils.isEmpty(u)) {
//                            url += "?u=" + u;
//                        } else {
//                            and = "?";
//                        }
//                        WebUrlTypeUtil.getInstance(mContext).urlToApp(url);
//                    }
//
//                }
                else {
                    //要跳转到的首页的
                    String uriType = uri.getQueryParameter("type");
                    Log.e(TAG, "uriType == " + uriType);
                    if (uriType != null) {
                        this.type = Integer.parseInt(uriType);
                        String tabac = uri.getQueryParameter("tabca");
                        if (type == 2 && !TextUtils.isEmpty(tabac)) {
                            mTabCa = tabac;
                        } else {
                            mTabCa = "";
                        }
                    }
                }

            }
        } else {
            type = getIntent().getIntExtra("type", 0);
        }

        TCAgent.init(this);
        TCAgent.setReportUncaughtExceptions(true);
        //初始化
        Qt.init(MyApplication.getInstance(), FinalConstant1.YUEMEI_MARKET, Utils.getImei(), new YueMeiQtCallBack());

        //签到推送
        String localDelivery = Cfg.loadStr(MainTableActivity.this, FinalConstant.SIGN_FLAG, "0");
        if (Utils.isLogin()) {
            if ("1".equals(localDelivery)) {
                signPush();
            }
        }


        binBaidu();
        initVsersion();
        getUserInfo();

        switch (type) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                mainBottomBar.setCheckedPos(type);
                break;
            default:
                mainBottomBar.setCheckedPos(0);
                break;
        }

        if (Utils.isLogin()) {
            mHandler.sendEmptyMessage(UNREAD_MESSAGE);
        }

        Intent it = getIntent();
        String dis = it.getStringExtra("tab");
        if ("4".equals(dis)) {
            mainBottomBar.setCheckedPos(4);
        }

        //隐私协议
//        privacyAgreementDialog = new PrivacyAgreementDialog(mContext);
//        yueMeiDialog = new YueMeiDialog(mContext, PRIVACY_TEXT, "不同意", "同意");
//
//        //是否同意
//        yueMeiDialog.setBtnClickListener(new YueMeiDialog.BtnClickListener() {
//            @Override
//            public void leftBtnClick() {
//                yueMeiDialog.dismiss();
//                privacyAgreementDialog.show();
//            }
//
//            @Override
//            public void rightBtnClick() {
//                yueMeiDialog.dismiss();
//                initModule();
//            }
//        });
//
//        int privacyAgreement = Cfg.loadInt(mContext, "privacy_agreement", 0);
//        if (privacyAgreement == 0) {
//            privacyAgreementDialog.show();
//            privacyAgreementDialog.setOnEventClickListener(new PrivacyAgreementDialog.OnEventClickListener() {
//                @Override
//                public void onCancelClick(View v) {
//                    //不同意
//                    privacyAgreementDialog.dismiss();
//                    yueMeiDialog.show();
//                }
//
//                @Override
//                public void onConfirmClick(View v) {
//                    //同意
//                    privacyAgreementDialog.dismiss();
//                    Cfg.saveInt(mContext, "privacy_agreement", 1);
//                    initModule();
//                }
//            });
//        } else {
//            initModule();
//        }
        initModule();

        //极光一键登录SDK
        JVerificationInterface.init(mContext, new RequestCallback<String>() {
            @Override
            public void onResult(int code, String msg) {
                Log.e(TAG, "code = " + code + " msg = " + msg);
            }
        });

        initPotentialCustomer();

//        MiitHelper miitHelper = new MiitHelper(new MiitHelper.AppIdsUpdater() {
//            @Override
//            public void OnIdsAvalid(boolean isSupport, IdSupplier _supplier) {
//                //匿名设备标识符
//                String oaid=_supplier.getOAID();
//                //开发者匿名设备标识符
//                String vaid=_supplier.getVAID();
//                //应⽤匿名设备标识符
//                String aaid=_supplier.getAAID();
//                //设备唯一标识符
//                String udid=_supplier.getUDID();
//                StringBuilder builder=new StringBuilder();
//                builder.append("support: ").append(isSupport?"true":"false").append("\n");
//                builder.append("UDID: ").append(udid).append("\n");
//                builder.append("OAID: ").append(oaid).append("\n");
//                builder.append("VAID: ").append(vaid).append("\n");
//                builder.append("AAID: ").append(aaid).append("\n");
//                String idstext=builder.toString();
//                Log.e(TAG,"idstext =="+idstext);
//            }
//        });
//        miitHelper.getDeviceIds(mContext);


    }




    /**
     * 初始化潜客
     */
    private void initPotentialCustomer() {

        String loadStr = Cfg.loadStr(mContext, FinalConstant.POTENTIAL_CUSTOMER, "");
        Log.e(TAG, "loadStr == " + loadStr);
        if (!TextUtils.isEmpty(loadStr)) {
            popwinView.setVisibility(View.VISIBLE);
            ImageView mImage = findViewById(R.id.potential_customer_image);
            TextView mName = findViewById(R.id.potential_customer_name);
            TextView mContent = findViewById(R.id.potential_customer_content);
            ImageView mClose = findViewById(R.id.potential_customer_close);

            final SneakGuest mData = JSONUtil.TransformSingleBean(loadStr, SneakGuest.class);

            Glide.with(mContext).load(mData.getAvatar()).diskCacheStrategy(DiskCacheStrategy.SOURCE).transform(new GlideRoundTransform(mContext, Utils.dip2px(5))).placeholder(R.drawable.home_other_placeholder).error(R.drawable.home_other_placeholder).into(mImage);
            mName.setText(mData.getHos_name());
            mContent.setText(mData.getContent());
            popwinView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String app_url = mData.getApp_url();
                    if (!TextUtils.isEmpty(app_url)) {
                        WebViewUrlLoading.getInstance().showWebDetail(mContext, app_url);
                        popwinView.setVisibility(View.GONE);
                    }
                }
            });

            //关闭
            mClose.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popwinView.setVisibility(View.GONE);
                }
            });

            Cfg.saveStr(mContext, FinalConstant.POTENTIAL_CUSTOMER, "");
        } else {
            popwinView.setVisibility(View.GONE);
        }
    }


    /**
     * 初始化模版
     */
    private void initModule() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (!isFinishing()) {
                    String isclose = Cfg.loadStr(mContext, "alert_close", "");
                    String img_ = Cfg.loadStr(mContext, "alder_img_", "");
                    String img = Cfg.loadStr(mContext, "ad_img", "");
                    HashMap<String, String> event_params = (HashMap<String, String>) Cfg.getHashMapData(mContext, "ad_event_params");
                    if (isclose.length() > 0) {
                        if (img.equals(img_)) {
                            int cur1 = (int) System.currentTimeMillis();
                            int cur2 = Cfg.loadInt(mContext, "time_alert", 0);
                            if ((cur1 - cur2) >= (3600 * 1000 * 24 * 10)) {
                                ZhuanTi zt1 = new ZhuanTi();
                                zt1.set_id(Cfg.loadStr(mContext, "ad_id", ""));
                                zt1.setImg(Cfg.loadStr(mContext, "ad_imgurl", ""));
                                zt1.setLink(Cfg.loadStr(mContext, "ad_link", ""));
                                zt1.setTitle(Cfg.loadStr(mContext, "ad_title", ""));
                                zt1.setType(Cfg.loadStr(mContext, "ad_type", ""));

                                if (img.length() > 0) {
                                    tancengUrl = FinalConstant.HOMENEW_TANCENG;
                                    Cfg.saveStr(mContext, "alder_img_", img);
                                    alertPop = new AlertPopWindow(mContext, zt1);
                                    alertPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                                    Cfg.saveStr(mContext, "alert_close", "");
                                    EventParamData eventParamData = new EventParamData();
                                    eventParamData.setEvent_pos("show");
                                    YmStatistics.getInstance().tongjiApp(eventParamData, event_params);

                                }
                            }
                        } else {
                            ZhuanTi zt1 = new ZhuanTi();
                            zt1.set_id(Cfg.loadStr(mContext, "ad_id", ""));
                            zt1.setImg(Cfg.loadStr(mContext, "ad_imgurl", ""));
                            zt1.setLink(Cfg.loadStr(mContext, "ad_link", ""));
                            zt1.setTitle(Cfg.loadStr(mContext, "ad_title", ""));
                            zt1.setType(Cfg.loadStr(mContext, "ad_type", ""));

                            if (img.length() > 0) {
                                tancengUrl = FinalConstant.HOMENEW_TANCENG;
                                Cfg.saveStr(mContext, "alder_img_", img);
                                alertPop = new AlertPopWindow(mContext, zt1);
                                alertPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                                Cfg.saveStr(mContext, "alert_close", "");
                                EventParamData eventParamData = new EventParamData();
                                eventParamData.setEvent_pos("show");
                                YmStatistics.getInstance().tongjiApp(eventParamData, event_params);
                            }
                        }


                    } else {
                        ZhuanTi zt1 = new ZhuanTi();
                        zt1.set_id(Cfg.loadStr(mContext, "ad_id", ""));
                        zt1.setImg(Cfg.loadStr(mContext, "ad_imgurl", ""));
                        zt1.setLink(Cfg.loadStr(mContext, "ad_link", ""));
                        zt1.setTitle(Cfg.loadStr(mContext, "ad_title", ""));
                        zt1.setType(Cfg.loadStr(mContext, "ad_type", ""));
                        String new_user = Cfg.loadStr(mContext, "new_user", "");
                        if (img.length() > 0) {

                            tancengUrl = FinalConstant.HOMENEW_TANCENG;

                            if (new_user.length() == 0) {
                                Cfg.saveStr(mContext, "new_user", "1");
                                mNewUserPopWindow = new NewUserPopWindow(mContext, "https://m.yuemei.com/tao_zt/6025.html");
                                mNewUserPopWindow.showAtLocation(contentLy, Gravity.CENTER, 0, 0);

                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", "6025");
                                hashMap.put("to_page_type", "16");
                                hashMap.put("to_page_id", "6025");
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "show"), hashMap);
                            } else {

                                Cfg.saveStr(mContext, "alder_img_", img);
                                alertPop = new AlertPopWindow(mContext, zt1);
                                alertPop.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                                Cfg.saveStr(mContext, "alert_close", "");
                                EventParamData eventParamData = new EventParamData();
                                eventParamData.setEvent_pos("show");
                                YmStatistics.getInstance().tongjiApp(eventParamData, event_params);

                            }

                        } else {
                            if (new_user.length() == 0) {
                                tancengUrl = FinalConstant.HOMENEW_TANCENG;
                                Cfg.saveStr(mContext, "new_user", "1");
                                mNewUserPopWindow = new NewUserPopWindow(mContext, "https://m.yuemei.com/tao_zt/6025.html");
                                mNewUserPopWindow.showAtLocation(contentLy, Gravity.CENTER, 0, 0);
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", "6025");
                                hashMap.put("to_page_type", "16");
                                hashMap.put("to_page_id", "6025");
                                YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.NEWPEOPLEALERT, "show"), hashMap);
                            }
                        }
                    }
                }
            }

        }, 3000);
        if (Utils.isLogin()) {
            //初始化私信长连接
            IMManager.getInstance(mContext).getIMNetInstance().connWebSocket(FinalConstant.baseService);
        }else {
            if (!TextUtils.isEmpty(mNoLoginId)){
                IMManager.getInstance(mContext).getIMNetInstance().connWebSocket(FinalConstant.baseService);
            }
        }

        //初始化百度定位
        initLocation();

        //获取要统计的接口
        if (StatisticalPath.getInstance().getPathData().size() == 0) {
            StatisticalPath.getInstance().getStatistical();
        }
    }



    private void initShenHe() {
        new ShenHeApi().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ShenHeData>() {

            @Override
            public void onSuccess(ShenHeData shenHeData) {
                if (shenHeData != null) {
                    Log.e(TAG,"nologinchat == "+shenHeData.getNologinchat());
                    Cfg.saveStr(mContext, SplashActivity.NO_LOGIN_CHAT,shenHeData.getNologinchat());
                    NoLoginBean user = shenHeData.getUser();
                    if (null != user){
                        String userId = user.getUser_id();//游客用户id
                        mNoLoginId = userId;
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_ID,userId);
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_IMG,user.getImg());
                    }else {
                        Cfg.saveStr(mContext,SplashActivity.NO_LOGIN_ID,"");
                    }

                }
            }
        });
    }

    /**
     * 初始化两个广播
     */
    private void initReceiver() {
        //注册广播
        if (netBroadcastReceiver == null) {
            netBroadcastReceiver = new NetBroadCastReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(netBroadcastReceiver, filter);
            /**
             * 设置监听
             */
            netBroadcastReceiver.setNetEvent(this);
        }

        if (mBroadcastReceiver == null) {
            mBroadcastReceiver = new RefreshMessageReceiver();
            IntentFilter myIntentFilter = new IntentFilter();
            myIntentFilter.addAction(FinalConstant.REFRESH_MESSAGE);
            //注册广播
            registerReceiver(mBroadcastReceiver, myIntentFilter);
        }
        if (mSendMessageReceiver == null) {
            IntentFilter intentFilter = new IntentFilter(SendMessageReceiver.ACTION);
            mSendMessageReceiver = new SendMessageReceiver();
            mContext.registerReceiver(mSendMessageReceiver, intentFilter);
        }

    }


    @Override
    public void onNetChange(int netMobile) {
        Log.d("aaaaaaaaaaa", "netMobile===========>" + netMobile);
        if (Utils.isLogin() && mNetStatus != null) {
            mNetStatus.netStatus(netMobile);
        }
    }


    //签到推送star
    private void signPush() {
        SharedPreferences sign = MyApplication.getContext().getSharedPreferences("sign", Context.MODE_PRIVATE);
        String signFlag = sign.getString(FinalConstant.SIGN_FLAG, "0");
        if ("1".equals(signFlag)) {
            if (!NotificationService.isRunning(this)) {
                Log.e(TAG, "NotificationService == " + NotificationService.isRunning(this));
                Intent startIntent = new Intent(this, NotificationService.class);
                startService(startIntent);
            }
        }
    }

    //签到推送end
    void initVsersion() {
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("uid", Utils.getUid());
        VersionApi versionApi = new VersionApi();
        versionApi.getCallBack(mContext, keyValues, new BaseCallBackListener<VersionJCData>() {
            @Override
            public void onSuccess(VersionJCData vrData) {
                listTitle = vrData.getTitle();
                String vserCode = vrData.getVer();// 4.3.1
                apkUrl = vrData.getUrl();
                vserState = vrData.getStatus();

                String vserIntNet = vserCode.replace(".", "");
                vserInt = Integer.parseInt(vserIntNet);

                String versionName = Utils.getVersionName();
                String versionCode = versionName.length() == 3 ? versionName + "0" : versionName;
                int vserIntLoc = Integer.parseInt(versionCode);
                Cfg.saveInt(mContext, "versionCode", vserIntLoc);
                Log.e(TAG, "vserIntLoc === " + vserIntLoc);
                Log.e(TAG, "vserInt === " + vserInt);
                if (vserIntLoc == 6410) {
                    String updateTip = Cfg.loadStr(mContext, "updateTip", "");
                    if (updateTip.length() == 0) {
                        Cfg.saveStr(mContext, "updateTip", "1");
                    }
                }

                if (vserIntLoc < vserInt) {
                    // 提示更新
                    Log.e(TAG, "提示更新....");
                    mainBottomBar.setPersonalPrompt(View.VISIBLE);
                    CheckVersionTask cv = new CheckVersionTask();
                    new Thread(cv).start();
                } else {
                    mainBottomBar.setPersonalPrompt(View.GONE);
                }
            }
        });
    }

    private final int UNREAD_MESSAGE = 100;


    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UNREAD_MESSAGE:
                    if (Utils.isLogin()) {
                        initTabNum();
                    }
                    mHandler.sendEmptyMessageDelayed(UNREAD_MESSAGE, 30000);
                    break;

            }
        }
    };

    void getUserInfo() {
        Map<String, Object> params = new HashMap<>();
        params.put("id", Utils.getUid());
        new BBsDetailUserInfoApi().getCallBack(mContext, params, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
            }
        });
    }

    /**
     * 消息数（通知数）
     */
    void initTabNum() {
        mMainTableApi = new MessagecountApi();
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("uid", Utils.getUid());
        mMainTableApi.getCallBack(mContext, keyValues, new BaseCallBackListener<NewsNumberData>() {

            @Override
            public void onSuccess(NewsNumberData newsNumberData) {
                if (newsNumberData != null) {

                    String ask_num = newsNumberData.getAsk_num();//问题未读消息数
                    String zong_num = newsNumberData.getZong_num();
                    String share_num = newsNumberData.getShare_num();//评论未读消息数
                    String notice_num = newsNumberData.getNotice_num();//通知未读消息数
                    String sixin_num = newsNumberData.getSixin_num();//私信未读消息数
                    String agree_num = newsNumberData.getAgree_num();//赞未读消息数
                    String follow_num = newsNumberData.getFollow_num();//关注未读消息数
                    Log.e(TAG, "zong_num == " + zong_num);
                    Log.e(TAG, "share_num == " + share_num);
                    Log.e(TAG, "notice_num == " + notice_num);
                    Log.e(TAG, "sixin_num == " + sixin_num);
                    Log.e(TAG, "agree_num == " + agree_num);
                    Log.e(TAG, "follow_num == " + follow_num);

                    int zongNum = Integer.parseInt(zong_num);
                    int siXinNum = Integer.parseInt(sixin_num);
                    int pingLunNum = Integer.parseInt(share_num);
                    int zanNum = Integer.parseInt(agree_num);
                    int guanZhuNum = Integer.parseInt(follow_num);
                    int noticeNum = Integer.parseInt(notice_num);

                    Cfg.saveInt(mContext, FinalConstant.ZONG_ID, zongNum);
                    Cfg.saveInt(mContext, FinalConstant.SIXIN_ID, siXinNum);
                    Cfg.saveInt(mContext, FinalConstant.PINGLUN_ID, pingLunNum);
                    Cfg.saveInt(mContext, FinalConstant.ZAN_ID, zanNum);
                    Cfg.saveInt(mContext, FinalConstant.GUANZHU_ID, guanZhuNum);
                    Cfg.saveInt(mContext, FinalConstant.NOTICE_ID, noticeNum);

                    int cc = siXinNum + pingLunNum + zanNum + guanZhuNum + noticeNum;
                    Log.e(TAG, "cc == " + cc);
                    if (mMessageFragmentActivity1 != null) {
                        if (pingLunNum > 0) {
                            mMessageFragmentActivity1.mBadgeCountList.set(1, pingLunNum);
                            mMessageFragmentActivity1.setUpTabBadge2(1);
                        }
                        if (zanNum > 0) {
                            mMessageFragmentActivity1.mBadgeCountList.set(2, zanNum);
                            mMessageFragmentActivity1.setUpTabBadge2(2);
                        }
                        if (guanZhuNum > 0) {
                            mMessageFragmentActivity1.mBadgeCountList.set(3, guanZhuNum);
                            mMessageFragmentActivity1.setUpTabBadge2(3);
                        }
                        if (noticeNum > 0) {
                            mMessageFragmentActivity1.mBadgeCountList.set(4, noticeNum);
                            mMessageFragmentActivity1.setUpTabBadge2(4);
                        }
                    }
                    //  桌面显示的消息数
                    AppShortCutUtil.addNumShortCut(mContext, MainTableActivity.class, true, cc + "", true);

                    if (Utils.isLogin() && Utils.isBind()) {
                        mainBottomBar.setMessageNum(cc);
                    }
                }

            }
        });
    }

    /**
     * 绑定百度
     */
    void binBaidu() {
        Log.e(TAG, "绑定百度");
        String bd_userid = Cfg.loadStr(mContext, FinalConstant.BD_USERID, "");  //获取百度推送的uid

        if (null != bd_userid && bd_userid.length() > 0) {                      //如果已经存在

            String curCity = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");   //获取定位城市
            Log.e("dingwei", "主页获取的城市是 == " + curCity);
            String cityOne = Cfg.loadStr(mContext, "citytohttp", "");           //获取变换key值后的唯一标识

            if (cityOne.length() > 0) {
                if (curCity.equals(cityOne)) {
                } else {
                    Cfg.saveStr(mContext, "citytohttp", curCity);               //保存定位城市（key值变）
                    Utils.getCityOneToHttp(mContext, "0");
                }
            } else {
                Cfg.saveStr(mContext, "citytohttp", curCity);
                Utils.getCityOneToHttp(mContext, "0");
            }

        } else {                                                                    //如果不存在
            PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY, Utils.BAIDU_PUSHE_KEY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (Utils.getNewOrOldUser() && !Utils.isLogin()) {
//            mainBottomBar.setMessagePrompt(View.VISIBLE);
//        } else {
//            mainBottomBar.setMessagePrompt(View.GONE);
//        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "===============onRestart");

        if (Utils.isLogin()) {
            Log.e(TAG, "isHandlerMsgEmpty() = " + isHandlerMsgEmpty());
            if (!isHandlerMsgEmpty()) {
                mHandler.sendEmptyMessage(UNREAD_MESSAGE);
            }
        }

    }

    /**
     * 判断消息是否存在
     *
     * @return
     */
    private boolean isHandlerMsgEmpty() {
        boolean isEmpty = mHandler.hasMessages(UNREAD_MESSAGE);
        return isEmpty;
    }


    /*
     * 从服务器获取xml解析并进行比对版本号
     */
    public class CheckVersionTask implements Runnable {

        @Override
        public void run() {
            try {
                if (0 >= vserInt) {
                    // Log.i(TAG, "版本号相同无需升级");
                    Message msg = new Message();
                    msg.what = UPDATA_NONEED;
                    handler.sendMessage(msg);
                } else {
                    // Log.i(TAG, "版本号不同 ,提示用户升级 ");
                    Message msg = new Message();
                    msg.what = UPDATA_CLIENT;
                    handler.sendMessage(msg);
                }
            } catch (Exception e) {
                // 待处理
                Message msg = new Message();
                msg.what = GET_UNDATAINFO_ERROR;
                handler.sendMessage(msg);
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case UPDATA_NONEED:
                case UPDATA_CLIENT:
                    if (0 < vserInt) {
                        new DialogUpdatePopupwindows(MainTableActivity.this, contentLy).setOnDismissListener(new PopupWindow.OnDismissListener() {
                            @Override
                            public void onDismiss() {

                                if (vserState.equals("1")) {

                                    if (isPopUpdate.equals("0")) {
                                        KJActivityManager.create().AppExit(mContext);
                                    }
                                }
                            }
                        });
                    }
                    break;
                case GET_UNDATAINFO_ERROR:
                    // 服务器超时
                    Toast.makeText(getApplicationContext(), "获取服务器更新信息失败", Toast.LENGTH_SHORT).show();
                    break;
                case SDCARD_NOMOUNTED:
                    // sdcard不可用
                    Toast.makeText(getApplicationContext(), "SD卡不可用", Toast.LENGTH_SHORT).show();
                    break;
                case DOWN_ERROR:
                    // 下载apk失败
                    Toast.makeText(getApplicationContext(), "下载新版本失败", Toast.LENGTH_SHORT).show();
                    // LoginMain();
                    break;
            }
        }
    };

    ProgressBar pd;
    Button cancelBt;

    /*
     * 从服务器中下载APK
     */
    protected void downLoadApk() {

        final ProgDialog editDialog = new ProgDialog(mContext, R.style.mystyle, R.layout.dialog_progress);
        editDialog.setCanceledOnTouchOutside(false);


        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Message msg = new Message();
            msg.what = SDCARD_NOMOUNTED;
            handler.sendMessage(msg);
        } else {
            editDialog.show();
            pd = editDialog.findViewById(R.id.progress_aaaaaa);

            cancelBt = editDialog.findViewById(R.id.cancel_btn_aa);

            if (vserState.equals("1")) {
                cancelBt.setVisibility(View.GONE);

                if (editDialog.isShowing()) {
                    editDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            KJActivityManager.create().AppExit(mContext);
                        }
                    });
                }
            }

            cancelBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    editDialog.dismiss();
                    interceptFlag = true;
                }
            });

            new Thread() {
                @Override
                public void run() {
                    try {
                        File file = getFileFromServer(apkUrl, pd);
                        sleep(1000);
                        if (!interceptFlag) {
                            installApk(file);
                        }
                        editDialog.dismiss(); // 结束掉进度条对话框
                    } catch (Exception e) {
                        Message msg = new Message();
                        msg.what = DOWN_ERROR;
                        handler.sendMessage(msg);
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    // 安装apk
    protected void installApk(File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {//判读版本是否在7.0以上
            Uri apkUri = FileProvider.getUriForFile(this, getPackageName() + ".FileProvider", file);//在AndroidManifest中的android:authorities值
            Intent install = new Intent(Intent.ACTION_VIEW);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            install.setDataAndType(apkUri, "application/vnd.android.package-archive");
            startActivity(install);
        } else {
            Intent intent = new Intent();
            // 执行动作
            intent.setAction(Intent.ACTION_VIEW);
            intent.addCategory("android.intent.category.DEFAULT");
            // 执行的数据类型
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    public static File getFileFromServer(String path, ProgressBar pd) throws Exception {
        // 如果相等的话表示当前的sdcard挂载在手机上并且是可用的
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.setConnectTimeout(5000);
            // 获取到文件的大小
            pd.setMax(conn.getContentLength());
            pd.getMax();

            InputStream is = conn.getInputStream();
            File file = new File(Environment.getExternalStorageDirectory(), "updata.apk");
            FileOutputStream fos = new FileOutputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);
            byte[] buffer = new byte[1024];
            int len;
            int total = 0;
            while ((len = bis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
                total += len;
                // 获取当前下载量
                pd.setProgress(total);
                if (interceptFlag) {
                    break;
                }
            }
            fos.close();
            bis.close();
            is.close();
            return file;
        } else {
            return null;
        }
    }

    public class DialogUpdatePopupwindows extends PopupWindow {

        public DialogUpdatePopupwindows(final Context mContext, View v) {

            final View view = View.inflate(mContext, R.layout.dialog_update_new, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_ins));

            LinearLayout listTitleLy = view.findViewById(R.id.update_tips_lly);

            setWidth(LayoutParams.MATCH_PARENT);
            setHeight(LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);

            int tilesize = listTitle.size();
            TextView[] titletv = new TextView[tilesize];

            LayoutInflater mInflater = getLayoutInflater();
            for (int i = 0; i < tilesize; i++) {
                View headView = mInflater.inflate(R.layout.item_update_title, null);
                titletv[i] = headView.findViewById(R.id.update_item_tv);

                titletv[i].setText(listTitle.get(i).getTitle());
                listTitleLy.addView(headView);
            }


            showAtLocation(v, Gravity.BOTTOM, 0, 0);
            update();

            Button cancleBt = view.findViewById(R.id.cancel_bt);

            if (vserState.equals("1")) {
                cancleBt.setTextColor(Color.parseColor("#bbbbbb"));
            }

            cancleBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (vserState.equals("1")) {
                        MyToast.makeText(mContext, "按钮不可点,请升级新版本", Toast.LENGTH_SHORT).show();
                    } else {
                        dismiss();
                    }
                }
            });
            Button sureBt = view.findViewById(R.id.zixun_bt);
            sureBt.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Acp.getInstance(mContext).request(new AcpOptions.Builder().setPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).build(), new AcpListener() {
                        @Override
                        public void onGranted() {
                            isPopUpdate = "1";
                            interceptFlag = false;
                            downLoadApk();
                            dismiss();
                        }

                        @Override
                        public void onDenied(List<String> permissions) {

                        }
                    });

                }
            });
        }
    }

    /**
     * 初始话百度的api定位
     */
    private void initLocation() {
        locationClient = new LocationClient(this);
        // 设置监听函数（定位结果）
        locationClient.registerLocationListener(new MyBDLocationListener(mContext));
        LocationClientOption option = new LocationClientOption();
        option.setAddrType("all");// 返回的定位结果包含地址信息
        option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
        locationClient.setLocOption(option);
        locationClient.start();
        // 请求定位
        locationClient.requestLocation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainTableButtonView.NEWS_CODE && resultCode == 10) {
            if (Utils.isLogin()) {
                mainBottomBar.setCheckedPos(3);
            }
        } else if (requestCode == 1004) {
            Log.e(TAG, "HHHHHHHH");
            if (TextUtils.isEmpty(bottomViewData)) {
                String city = Cfg.loadStr(mContext, FinalConstant.DWCITY, "");
                if (hotCity.contains(city)) {
                    mainBottomBar.initDefaultView(tabHost, city, true);
                } else {
                    mainBottomBar.initDefaultView(tabHost, "值得买", true);
                }
                if (resultCode == 5) {
                    mainBottomBar.setCheckedPos(1);
                }

            }


        }


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);
        Utils.clearVideoCacheFile();                            //清除视频缓存
        Cfg.saveStr(mContext, FinalConstant.NOT_WIFI_PLAY, "0");
        if (mContext != null && !mContext.isFinishing()) {
            if (alertPop != null) {
                alertPop.dismiss();
                alertPop = null;
            }
            if (mNewUserPopWindow != null) {
                mNewUserPopWindow.dismiss();
                mNewUserPopWindow = null;
            }
        }

        // 不需要的时候移除listener，如在activity的onDestroy()时
        if (locationClient != null) {
            locationClient.unRegisterLocationListener(new MyBDLocationListener(mContext));
            locationClient.stop();
        }
        if (Util.isOnMainThread() && !this.isFinishing()) {
            Glide.with(MyApplication.getContext()).pauseRequests();
        }
        if (netBroadcastReceiver != null) {
            //注销广播
            unregisterReceiver(netBroadcastReceiver);
        }

        try {
            if (mBroadcastReceiver != null) {
                //注销广播
                unregisterReceiver(mBroadcastReceiver);
            }
        } catch (Exception e) {
            Log.e(TAG, "e2 == " + e.toString());
            e.printStackTrace();
        }
        if (mSendMessageReceiver != null) {
            unregisterReceiver(mSendMessageReceiver);
        }
        super.onDestroy();
    }


}
