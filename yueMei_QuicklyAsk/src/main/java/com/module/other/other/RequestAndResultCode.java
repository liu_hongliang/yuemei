package com.module.other.other;

/**
 * Created by 裴成浩 on 2019/8/31
 */
public class RequestAndResultCode {

    //WebView页面请求码
    public static final int WEB_VIEW_REQUEST_CODE = 1001;

    //城市页面返回码
    public static final int CITY_RESULT_CODE = 1002;

    //视频回调
    public static final int VIDEO_REQUEST_CODE = 1003;

    //图片回调
    public static final int IMAGE_REQUEST_CODE = 1004;

    //图片添加贴纸后的回调
    public static final int ACTION_REQUEST_EDITIMAGE = 1005;

    //日记、帖子等编辑后返回文字回调
    public static final int POSTING_NOTE_FONT = 1006;
}
