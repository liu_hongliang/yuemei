package com.module.other.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * 文 件 名: TickDataInfo
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-24 17:38
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class TickDataInfo implements Parcelable {


    /**
     * tao_id : 123456
     * tick_time : 1582514195
     */
    private List<TickData> data;

    public List<TickData> getData() {
        return data;
    }

    public void setData(List<TickData> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.data);
    }

    public TickDataInfo() {
    }

    protected TickDataInfo(Parcel in) {
        this.data = new ArrayList<TickData>();
        in.readList(this.data, TickData.class.getClassLoader());
    }

    public static final Parcelable.Creator<TickDataInfo> CREATOR = new Parcelable.Creator<TickDataInfo>() {
        @Override
        public TickDataInfo createFromParcel(Parcel source) {
            return new TickDataInfo(source);
        }

        @Override
        public TickDataInfo[] newArray(int size) {
            return new TickDataInfo[size];
        }
    };
}
