package com.module.other.module.bean;

import java.util.List;

/**
 * Created by dwb on 16/10/10.
 */
public class CityPartData {

    private List<IdName> city;
    private List<IdName> tag;

    public List<IdName> getCity() {
        return city;
    }

    public void setCity(List<IdName> city) {
        this.city = city;
    }

    public List<IdName> getTag() {
        return tag;
    }

    public void setTag(List<IdName> tag) {
        this.tag = tag;
    }
}
