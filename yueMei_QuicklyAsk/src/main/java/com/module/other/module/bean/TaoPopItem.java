package com.module.other.module.bean;

import java.util.List;

public class TaoPopItem {

	private String code;
	private String message;
	private List<TaoPopItemData> data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<TaoPopItemData> getData() {
		return data;
	}

	public void setData(List<TaoPopItemData> data) {
		this.data = data;
	}

}
