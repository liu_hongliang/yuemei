package com.module.other.module.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 裴成浩 on 2019/2/20
 */
public class MakeTagListData implements Parcelable {
    private String id;
    private String name;
    private String img;
    private String level;
    private String channel_title;
    private ArrayList<MakeTagListListData> list;
    private HashMap<String, String> event_params;
//    private HashMap<String, String> event_params;

    protected MakeTagListData(Parcel in) {
        id = in.readString();
        name = in.readString();
        img = in.readString();
        level = in.readString();
        channel_title = in.readString();
        list = in.createTypedArrayList(MakeTagListListData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(img);
        dest.writeString(level);
        dest.writeString(channel_title);
        dest.writeTypedList(list);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MakeTagListData> CREATOR = new Creator<MakeTagListData>() {
        @Override
        public MakeTagListData createFromParcel(Parcel in) {
            return new MakeTagListData(in);
        }

        @Override
        public MakeTagListData[] newArray(int size) {
            return new MakeTagListData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getChannel_title() {
        return channel_title;
    }

    public void setChannel_title(String channel_title) {
        this.channel_title = channel_title;
    }

    public ArrayList<MakeTagListListData> getList() {
        return list;
    }

    public void setList(ArrayList<MakeTagListListData> list) {
        this.list = list;
    }

    public HashMap<String, String> getEvent_params() {
        return event_params;
    }

    public void setEvent_params(HashMap<String, String> event_params) {
        this.event_params = event_params;
    }
}
