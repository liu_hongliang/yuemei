package com.module.other.module.bean;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * 文 件 名: TickData
 * 创 建 人: 原成昊
 * 创建日期: 2020-02-24 17:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：
 */

public class TickData implements Parcelable {

    /**
     * tao_id : 123456
     * tick_time : 1582514195
     */

    private String tao_id;
    private String tick_time;

    public String getTao_id() {
        return tao_id;
    }

    public void setTao_id(String tao_id) {
        this.tao_id = tao_id;
    }

    public String getTick_time() {
        return tick_time;
    }

    public void setTick_time(String tick_time) {
        this.tick_time = tick_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tao_id);
        dest.writeString(this.tick_time);
    }

    public TickData() {
    }

    protected TickData(Parcel in) {
        this.tao_id = in.readString();
        this.tick_time = in.readString();
    }

    public static final Parcelable.Creator<TickData> CREATOR = new Parcelable.Creator<TickData>() {
        @Override
        public TickData createFromParcel(Parcel source) {
            return new TickData(source);
        }

        @Override
        public TickData[] newArray(int size) {
            return new TickData[size];
        }
    };
}
