package com.module.other.module.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by 裴成浩 on 2017/7/14.
 */

public class SerializableMap implements Serializable {
    public Map<String,String> map;
    public SerializableMap(){

    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;

    }
}