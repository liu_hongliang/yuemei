package com.module.other.module.bean;

public class Data {
    private String integral;
    private String experience;
    private RewardAlertData rewardAlertData;

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public RewardAlertData getRewardAlertData() {
        return rewardAlertData;
    }

    public void setRewardAlertData(RewardAlertData rewardAlertData) {
        this.rewardAlertData = rewardAlertData;
    }
}
