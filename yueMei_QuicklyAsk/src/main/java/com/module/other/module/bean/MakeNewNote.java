package com.module.other.module.bean;

import java.util.List;

/**
 * Created by dwb on 16/3/22.
 */
public class MakeNewNote {

    private String code;
    private String message;
    private List<MakeNewNoteData> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MakeNewNoteData> getData() {
        return data;
    }

    public void setData(List<MakeNewNoteData> data) {
        this.data = data;
    }
}
