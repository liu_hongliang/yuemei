package com.module.other.receiver;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.alibaba.sdk.android.push.MessageReceiver;
import com.alibaba.sdk.android.push.notification.CPushMessage;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.Map;

/**
 * @author: 正纬
 * @since: 15/4/9
 * @version: 1.1
 * @feature: 用于接收推送的通知和消息
 */
public class MyMessageReceiver extends MessageReceiver {
    public static final String TAG = "MyMessageReceiver";
    // 消息接收部分的LOG_TAG
    public static final String REC_TAG = "receiver";

    /**
     * 推送通知的回调方法
     *
     * @param context
     * @param title
     * @param summary
     * @param extraMap
     */
    @Override
    public void onNotification(Context context, String title, String summary, Map<String, String> extraMap) {
        // TODO 处理推送通知
        if (null != extraMap) {
            for (Map.Entry<String, String> entry : extraMap.entrySet()) {
                Log.e(TAG, "@Get diy param : Key=" + entry.getKey() + " , Value=" + entry.getValue());
            }
        } else {
            Log.e(TAG, "@收到通知 && 自定义消息为空");
        }
        Log.e(TAG, "收到一条推送通知 ： " + title);

    }

    /**
     * 推送消息的回调方法
     *
     * @param context
     * @param cPushMessage
     */
    @Override
    public void onMessage(Context context, CPushMessage cPushMessage) {
        try {

            Log.e(TAG, "收到一条推送消息 ： " + cPushMessage.getTitle());

            // 持久化推送的消息到数据库
            // new MessageDao(context).add(new MessageEntity(cPushMessage.getMessageId().substring(6,16), Integer.valueOf(cPushMessage.getAppId()), cPushMessage.getTitle(), cPushMessage.getContent(), new SimpleDateFormat("HH:mm:ss").format(new Date())));

            // 刷新下消息列表
            //ActivityBox.CPDMainActivity.initMessageView();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * 从通知栏打开通知的扩展处理
     *
     * @param context
     * @param title
     * @param summary
     * @param extraMap
     */
    @Override
    public void onNotificationOpened(Context context, String title, String summary, String extraMap) {
        Log.e(TAG, "onNotificationOpened ： " + " : " + title + " : " + summary + " : " + extraMap);
//        ViewInject.toast("消息推送点击");
        updateContent(context,extraMap);
    }

    @Override
    public void onNotificationRemoved(Context context, String messageId) {
        Log.e(TAG, "onNotificationRemoved ： " + messageId);
    }


    private void updateContent(Context context, String content) {
        // Log.d(TAG, "updateContent");

        // Log.d(TAG, "updateContent");
        Log.e("Recevier", "===" + content);

        String eid = "";
        String uid = "";
        String link = "";
        String type = "";

        type = JSONUtil.resolveJson(content, "type");

        if (type.equals("1")) {
            eid = JSONUtil.resolveJson(content, "e_id");
            link = JSONUtil.resolveJson(content, "link");
            Intent it = new Intent();
            it.putExtra("url", link);
            it.putExtra("qid", eid);
            it.setClass(context,
                    DiariesAndPostsActivity.class);

            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(it);
        } else if (type.equals("418")) {
            eid = JSONUtil.resolveJson(content, "e_id");
//			link = JSONUtil.resolveJson(content, "link");
            Intent it = new Intent();
            // it.putExtra("url", link);
            it.putExtra("id", eid);
            it.putExtra("source", "0");
            it.putExtra("objid", "0");
            it.setClass(context.getApplicationContext(),
                    TaoDetailActivity.class);

            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it);
        } else if (type.equals("999")) {
            // eid = JSONUtil.resolveJson(content, "e_id");
            link = JSONUtil.resolveJson(content, "link");
            Intent it = new Intent();
            // it.putExtra("url", link);
            it.putExtra("url", link);
            it.putExtra("shareTitle", "0");
            it.putExtra("sharePic", "0");
            it.setClass(context.getApplicationContext(),
                    SlidePicTitieWebActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it);
        } else if (type.equals("522")) {// 我的咨询
            // eid = JSONUtil.resolveJson(content, "e_id");
            // link = JSONUtil.resolveJson(content, "link");
            Intent it = new Intent();
            // it.putExtra("url", link);
            it.putExtra("type", "2");
            it.setClass(context.getApplicationContext(), MessageFragmentActivity1.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it);
        } else if("6312".equals(type)){             //私信推送

            boolean background = Utils.isBackground(context.getApplicationContext());
            Log.e(TAG, "background == " + background);

            Intent it6 = new Intent();
            it6.setClass(context.getApplicationContext(), MessageFragmentActivity1.class);
            it6.putExtra("acquiescencePage", 0);
            it6.putExtra("type", 1);
            it6.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it6);

        } else if (type.equals("1000")) {// 快速专题
            eid = JSONUtil.resolveJson(content, "e_id");
            link = JSONUtil.resolveJson(content, "link");
            Intent it = new Intent();
            it.putExtra("url", link);
            // it.putExtra("link", link);
            it.putExtra("title", "0");
            it.putExtra("ztid", eid);
            it.setClass(context.getApplicationContext(),
                    ZhuanTiWebActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it);
        } else if (type.equals("5471")) {// 客服咨询
            // eid = JSONUtil.resolveJson(content, "e_id");
//            link = JSONUtil.resolveJson(content, "link");// 未读消息数
//            Cfg.saveStr(context, FinalConstant.MCNUM, link);


        }else if (type.equals("431")) {// 个人信息

            eid = JSONUtil.resolveJson(content, "e_id");
            Intent it = new Intent();
            it.setClass(context.getApplicationContext(), PersonCenterActivity641.class);
            it.putExtra("id", eid);
            context.startActivity(it);

        }else if (type.equals("10000")) {// web

            link = JSONUtil.resolveJson(content, "link");
            WebUrlTypeUtil.getInstance(context.getApplicationContext()).urlToApp(link,"0","0");

        } else {
            eid = JSONUtil.resolveJson(content, "e_id");
            link = JSONUtil.resolveJson(content, "link");
            uid = JSONUtil.resolveJson(content, "u_id");

            Intent it = new Intent();
            it.putExtra("url", link);
            it.putExtra("qid", eid);
            it.putExtra("louc", "0");
            it.putExtra("userid", uid);
            it.putExtra("typeroot", "1");
            it.setClass(context.getApplicationContext(),
                    BBsFinalWebActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.getApplicationContext().startActivity(it);
        }

    }

}