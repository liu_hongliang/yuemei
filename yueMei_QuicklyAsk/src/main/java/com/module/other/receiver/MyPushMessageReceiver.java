package com.module.other.receiver;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.baidu.android.pushservice.PushMessageReceiver;
import com.module.MainTableActivity;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.controller.activity.PersonCenterActivity641;
import com.module.community.controller.activity.SlidePicTitieWebActivity;
import com.module.home.controller.activity.MessageFragmentActivity1;
import com.module.home.controller.activity.ZhuanTiWebActivity;
import com.module.my.controller.activity.BBsFinalWebActivity;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.quicklyask.util.WebUrlTypeUtil;

import java.util.List;

/*
 * Push消息处理receiver。请编写您需要的回调函数， 一般来说： onBind是必须的，用来处理startWork返回值；
 *onMessage用来接收透传消息； onSetTags、onDelTags、onListTags是tag相关操作的回调；
 *onNotificationClicked在通知被点击时回调； onUnbind是stopWork接口的返回值回调

 * 返回值中的errorCode，解释如下：
 *0 - Success
 *10001 - Network Problem
 *10101  Integrate Check Error
 *30600 - Internal Server Error
 *30601 - Method Not Allowed
 *30602 - Request Params Not Valid
 *30603 - Authentication Failed
 *30604 - Quota Use Up Payment Required
 *30605 -Data Required Not Found
 *30606 - Request Time Expires Timeout
 *30607 - Channel Token Timeout
 *30608 - Bind Relation Not Found
 *30609 - Bind Number Too Many

 * 当您遇到以上返回错误时，如果解释不了您的问题，请用同一请求的返回值requestId和errorCode联系我们追查问题。
 *
 */

public class MyPushMessageReceiver extends PushMessageReceiver {


	/** TAG to Log */
	public static final String TAG = "MyPushMessageReceiver";

	/**
	 * 调用PushManager.startWork后，sdk将对push
	 * server发起绑定请求，这个过程是异步的。绑定请求的结果通过onBind返回。 如果您需要用单播推送，需要把这里获取的channel
	 * id和user id上传到应用server中，再调用server接口用channel id和user id给单个手机或者用户推送。
	 * 
	 * @param context
	 *            BroadcastReceiver的执行Context
	 * @param errorCode
	 *            绑定接口返回值，0 - 成功
	 * @param appid
	 *            应用id。errorCode非0时为null
	 * @param userId
	 *            应用user id。errorCode非0时为null
	 * @param channelId
	 *            应用channel id。errorCode非0时为null
	 * @param requestId
	 *            向服务端发起的请求id。在追查问题时有用；
	 * @return none
	 */
	@Override
	public void onBind(Context context, int errorCode, String appid,
			String userId, String channelId, String requestId) {


		String responseString = "onBind errorCode=" + errorCode + " appid="
				+ appid + " userId=" + userId + " channelId=" + channelId
				+ " requestId=" + requestId;


		Utils.userId = userId;
		Utils.channelId = channelId;

		if(null!=userId) {
			Cfg.saveStr(context, FinalConstant.BD_USERID, userId);
			Cfg.saveStr(context, FinalConstant.BD_CHID, channelId);
		}

		Log.e(TAG, "推送的字符串===" + responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, responseString);
	}



	/**
	 * 接收透传消息的函数。
	 * 
	 * @param context
	 *            上下文
	 * @param message
	 *            推送的消息
	 * @param customContentString
	 *            自定义内容,为空或者json字符串
	 */
	@Override
	public void onMessage(Context context, String message,
			String customContentString) {
		String messageString = "透传消息 message=\"" + message
				+ "\" customContentString=" + customContentString;

		 Log.d(TAG, "onMessage ===" + messageString);

		// 自定义内容获取方式，mykey和myvalue对应透传消息推送时自定义内容中设置的键和值
		// if (!TextUtils.isEmpty(customContentString)) {
		// JSONObject customJson = null;
		// try {
		// customJson = new JSONObject(customContentString);
		// String myvalue = null;
		// if (!customJson.isNull("mykey")) {
		// myvalue = customJson.getString("mykey");
		// }
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, messageString);
	}

	/**
	 * 接收通知点击的函数。
	 *
	 * @param context
	 *            上下文
	 * @param title
	 *            推送的通知的标题
	 * @param description
	 *            推送的通知的描述
	 * @param customContentString
	 *            自定义内容，为空或者json字符串
	 */
	@Override
	public void onNotificationClicked(Context context, String title,
			String description, String customContentString) {
		String notifyString = "通知点击 title=\"" + title + "\" description=\""
				+ description + "\" customContent=" + customContentString;

		 Log.e(TAG, "onNotificationClicked===" + notifyString);

		if ("".equals(customContentString) || customContentString == null
				|| "0".equals(customContentString)) {
			uptoMain(context);
		} else {
			updateContent(context, customContentString);
		}

	}

	/**
	 * 接收通知到达的函数。
	 * 
	 * @param context
	 *            上下文
	 * @param title
	 *            推送的通知的标题
	 * @param description
	 *            推送的通知的描述
	 * @param customContentString
	 *            自定义内容，为空或者json字符串
	 */

	@Override
	public void onNotificationArrived(Context context, String title,
			String description, String customContentString) {

		String notifyString = "onNotificationArrived  title=\"" + title
				+ "\" description=\"" + description + "\" customContent="
				+ customContentString;
		 Log.d(TAG, "onNotificationArrived === " + notifyString);

		// 自定义内容获取方式，mykey和myvalue对应通知推送时自定义内容中设置的键和值
		// if (!TextUtils.isEmpty(customContentString)) {
		// JSONObject customJson = null;
		// try {
		// customJson = new JSONObject(customContentString);
		// String myvalue = null;
		// if (!customJson.isNull("mykey")) {
		// myvalue = customJson.getString("mykey");
		// }
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// 你可以參考 onNotificationClicked中的提示从自定义内容获取具体值
		// updateContent(context, notifyString);
	}

	/**
	 * setTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示某些tag已经设置成功；非0表示所有tag的设置均失败。
	 * @param
	 *
	 * @param failTags
	 *            设置失败的tag
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onSetTags(Context context, int errorCode,
			List<String> sucessTags, List<String> failTags, String requestId) {
		String responseString = "onSetTags errorCode=" + errorCode
				+ " sucessTags=" + sucessTags + " failTags=" + failTags
				+ " requestId=" + requestId;

		// Log.d(TAG, responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, responseString);
	}

	/**
	 * delTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示某些tag已经删除成功；非0表示所有tag均删除失败。
	 * @param
	 *
	 * @param failTags
	 *            删除失败的tag
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onDelTags(Context context, int errorCode,
			List<String> sucessTags, List<String> failTags, String requestId) {
		String responseString = "onDelTags errorCode=" + errorCode
				+ " sucessTags=" + sucessTags + " failTags=" + failTags
				+ " requestId=" + requestId;

		 Log.e(TAG, responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, responseString);
	}

	/**
	 * listTags() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示列举tag成功；非0表示失败。
	 * @param tags
	 *            当前应用设置的所有tag。
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onListTags(Context context, int errorCode, List<String> tags,
			String requestId) {
		String responseString = "onListTags errorCode=" + errorCode + " tags="
				+ tags;

		// Log.d(TAG, responseString);

		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, responseString);
	}

	/**
	 * PushManager.stopWork() 的回调函数。
	 * 
	 * @param context
	 *            上下文
	 * @param errorCode
	 *            错误码。0表示从云推送解绑定成功；非0表示失败。
	 * @param requestId
	 *            分配给对云推送的请求的id
	 */
	@Override
	public void onUnbind(Context context, int errorCode, String requestId) {
		String responseString = "onUnbind errorCode=" + errorCode
				+ " requestId = " + requestId;

		// Log.d(TAG, responseString);

		if (errorCode == 0) {
			// 解绑定成功
			Utils.setBind(context, false);
		}
		// Demo更新界面展示代码，应用请在这里加入自己的处理逻辑
		// updateContent(context, responseString);
	}

	private void updateContent(Context context, String content) {
		// Log.d(TAG, "updateContent");

		 Log.e(TAG, "===" + content);

		String eid = "";
		String uid = "";
		String link = "";
		String type = "";

		type = JSONUtil.resolveJson(content, "type");

		if (type.equals("1")) {
			eid = JSONUtil.resolveJson(content, "e_id");
			link = JSONUtil.resolveJson(content, "link");
			Intent it = new Intent();
			it.putExtra("url", link);
			it.putExtra("qid", eid);
			it.setClass(context,
					DiariesAndPostsActivity.class);

			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(it);
		} else if (type.equals("418")) {
			eid = JSONUtil.resolveJson(content, "e_id");
//			link = JSONUtil.resolveJson(content, "link");
			Intent it = new Intent();
			// it.putExtra("url", link);
			it.putExtra("id", eid);
			it.putExtra("source", "0");
			it.putExtra("objid","0" );
			it.setClass(context.getApplicationContext(),
					TaoDetailActivity.class);

			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
		} else if (type.equals("999")) {
			// eid = JSONUtil.resolveJson(content, "e_id");
			link = JSONUtil.resolveJson(content, "link");
			Intent it = new Intent();
			// it.putExtra("url", link);
			it.putExtra("url", link);
			it.putExtra("shareTitle", "0");
			it.putExtra("sharePic", "0");
			it.setClass(context.getApplicationContext(),
					SlidePicTitieWebActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
		} else if (type.equals("522")) {// 我的咨询
			// eid = JSONUtil.resolveJson(content, "e_id");
			// link = JSONUtil.resolveJson(content, "link");
			Intent it = new Intent();
			// it.putExtra("url", link);
			it.putExtra("type", "2");
			it.setClass(context.getApplicationContext(), MessageFragmentActivity1.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
		} else if("6312".equals(type)){					//私信

			boolean background = Utils.isBackground(context.getApplicationContext());
			Log.e(TAG, "background == " + background);

			Intent it6 = new Intent();
			it6.setClass(context.getApplicationContext(), MainTableActivity.class);
			it6.putExtra("acquiescencePage", 0);
			it6.putExtra("type", 1);
			it6.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			context.getApplicationContext().startActivity(it6);

		}else if (type.equals("1000")) {// 快速专题
			eid = JSONUtil.resolveJson(content, "e_id");
			link = JSONUtil.resolveJson(content, "link");
			Intent it = new Intent();
			it.putExtra("url", link);
			// it.putExtra("link", link);
			it.putExtra("title", "0");
			it.putExtra("ztid", eid);
			it.setClass(context.getApplicationContext(),
					ZhuanTiWebActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
		} else if (type.equals("5471")) {// 客服咨询
			// eid = JSONUtil.resolveJson(content, "e_id");
//			link = JSONUtil.resolveJson(content, "link");// 未读消息数
//			Cfg.saveStr(context, FinalConstant.MCNUM, link);
			// 启动客服对话界面


		}else if (type.equals("431")) {// 个人信息

			eid = JSONUtil.resolveJson(content, "e_id");
			Intent it = new Intent();
			it.setClass(context.getApplicationContext(), PersonCenterActivity641.class);
			it.putExtra("id", eid);
			context.startActivity(it);

		}else if (type.equals("10000")) {// web

			link = JSONUtil.resolveJson(content, "link");
			WebUrlTypeUtil.getInstance(context.getApplicationContext()).urlToApp(link,"0","0");

		} else {
			eid = JSONUtil.resolveJson(content, "e_id");
			link = JSONUtil.resolveJson(content, "link");
			uid = JSONUtil.resolveJson(content, "u_id");

			Intent it = new Intent();
			it.putExtra("url", link);
			it.putExtra("qid", eid);
			it.putExtra("louc", "0");
			it.putExtra("userid", uid);
			it.putExtra("typeroot", "1");
			it.setClass(context.getApplicationContext(),
					BBsFinalWebActivity.class);
			it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.getApplicationContext().startActivity(it);
		}


	}

	private void uptoMain(Context context) {

		Log.e(TAG,"BBBBBBBBBBBBBB");

		Intent it1 = new Intent();
		it1.setClass(context.getApplicationContext(), MainTableActivity.class);
		context.getApplicationContext().startActivity(it1);
	}



}
