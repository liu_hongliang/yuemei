package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.home.controller.adapter.TaoAdpter623;
import com.module.home.view.LoadingProgress;
import com.module.other.module.api.LodHotIssueDataApi;
import com.module.other.module.api.LodHotIssueDataApi2;
import com.module.other.netWork.netWork.ServerData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.SearchResultData1;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.view.DropDownListView2;
import com.quicklyask.view.DropDownListView2.OnDropDownListener2;
import com.umeng.analytics.MobclickAgent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class LimitTaoFragment extends ListFragment {

    private Context mCotext;
    private int position;
    private String[] part2Id;
    private String partIds = "";
    private String type = "";

    private String id = "";

    // List
    private DropDownListView2 mlist;
    private int mCurPage = 1;
    private Handler mHandler;

    private List<HomeTaoData> lvHotIssueData = new ArrayList();
    private List<HomeTaoData> lvHotIssueMoreData = new ArrayList();
    private TaoAdpter623 hotAdpter;

    private LinearLayout nodataTv;
    private LoadingProgress mDialog;

    public static LimitTaoFragment newInstance(int position, String[] partId, String type) {
        LimitTaoFragment f = new LimitTaoFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putStringArray("part2Id", partId);
        b.putString("type", type);
        f.setArguments(b);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_collect_tao, container, false);
        nodataTv = v.findViewById(R.id.my_collect_post_tv_nodata1);

        mDialog = new LoadingProgress(getActivity());

        if (isAdded()) {
            position = getArguments().getInt("position");
            part2Id = getArguments().getStringArray("part2Id");
            type = getArguments().getString("type");

            partIds = part2Id[position];
            id = partIds;
        }


        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {// 执行两次
        super.onActivityCreated(savedInstanceState);
        mCotext = getActivity();
        mlist = (DropDownListView2) getListView();

        initList();
    }

    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new OnDropDownListener2() {

            @Override
            public void onDropDown() {
                lvHotIssueData = null;
                lvHotIssueMoreData = null;
                mCurPage = 1;
                mDialog.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos, long arg3) {
                if (lvHotIssueData.size() < 8) {
                    String id = lvHotIssueData.get(pos).get_id();
                    Intent it1 = new Intent();
                    it1.putExtra("id", id);
                    it1.putExtra("source", "0");
                    it1.putExtra("objid", "0");
                    it1.setClass(getActivity(), TaoDetailActivity.class);
                    startActivity(it1);
                } else {
                    String id = lvHotIssueData.get(pos - 1).get_id();
                    Intent it1 = new Intent();
                    it1.putExtra("id", id);
                    it1.putExtra("source", "0");
                    it1.putExtra("objid", "0");
                    it1.setClass(getActivity(), TaoDetailActivity.class);
                    startActivity(it1);
                }
            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put("id", id);
                maps.put("page", mCurPage + "");
                if (isDonwn) {
                    if (mCurPage == 1) {
                        if (type.equals("1")) {
                            new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                                @Override
                                public void onSuccess(ServerData serverData) {
                                    if ("1".equals(serverData.code)) {
                                        try {
                                            SearchResultData1 searchResultData1 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData1.class);
                                            lvHotIssueData = searchResultData1.getList();
                                            Message message = mHandler.obtainMessage(1);
                                            message.sendToTarget();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                        } else {
                            new LodHotIssueDataApi2().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                                @Override
                                public void onSuccess(ServerData serverData) {
                                    if ("1".equals(serverData.code)) {
                                        try {
                                            SearchResultData1 searchResultData1 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData1.class);
                                            lvHotIssueData = searchResultData1.getList();
                                            Message message = mHandler.obtainMessage(1);
                                            message.sendToTarget();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            });
                        }
                    }

                } else {
                    mCurPage++;
                    maps.put("page", mCurPage + "");
                    if (type.equals("1")) {
                        new LodHotIssueDataApi().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    try {
                                        SearchResultData1 searchResultData1 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData1.class);
                                        lvHotIssueMoreData = searchResultData1.getList();
                                        Message message = mHandler.obtainMessage(2);
                                        message.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });

                    } else {
                        new LodHotIssueDataApi2().getCallBack(mCotext, maps, new BaseCallBackListener<ServerData>() {
                            @Override
                            public void onSuccess(ServerData serverData) {
                                if ("1".equals(serverData.code)) {
                                    try {
                                        SearchResultData1 searchResultData1 = JSONUtil.TransformSingleBean(serverData.data, SearchResultData1.class);
                                        lvHotIssueMoreData = searchResultData1.getList();
                                        Message message = mHandler.obtainMessage(2);
                                        message.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        });
                    }
                }
            }
        }).start();

    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({"NewApi", "SimpleDateFormat"})
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
                            nodataTv.setVisibility(View.GONE);

                            if (lvHotIssueData.size() < 8) {
                                mlist.setDropDownStyle(false);
                                mlist.setOnBottomStyle(false);
                            } else {
                                mlist.setDropDownStyle(true);
                                mlist.setOnBottomStyle(true);
                            }

                            mDialog.stopLoading();

                            if (null != getActivity()) {
                                Log.e("TaoAdpter623", "55555");
                                hotAdpter = new TaoAdpter623(getActivity(), lvHotIssueData);
                                mlist.setAdapter(hotAdpter);
                            }

                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss");
                            mlist.onDropDownComplete(getString(R.string.update_at) + dateFormat.format(new Date()));

                            mlist.onBottomComplete();
                        } else {
                            mDialog.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        if (null != lvHotIssueMoreData && lvHotIssueMoreData.size() > 0) {
                            hotAdpter.add(lvHotIssueMoreData);
                            hotAdpter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainScreen"); // 统计页面
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainScreen");
    }
}
