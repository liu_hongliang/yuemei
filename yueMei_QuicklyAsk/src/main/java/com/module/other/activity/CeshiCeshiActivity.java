package com.module.other.activity;


import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;

import com.module.base.view.CeshiAdapter;
import com.module.base.view.YMBaseActivity;
import com.module.base.view.YMRecyclerViewAdapter;
import com.quicklyask.activity.R;

import java.util.ArrayList;

import butterknife.BindView;


public class CeshiCeshiActivity extends YMBaseActivity {

    @BindView(R.id.activity_ceshi_ceshi1)
    Button listH;
    @BindView(R.id.activity_ceshi_ceshi2)
    Button listS;
    @BindView(R.id.activity_ceshi_ceshi3)
    Button biaoge;
    @BindView(R.id.activity_ceshi_ceshi4)
    Button pubu;
    @BindView(R.id.activity_ceshi_ceshi5)
    Button start;
    @BindView(R.id.activity_ceshi_list)
    RecyclerView mList;

    private String type = "1";
    ArrayList<String> arrayList = new ArrayList<>();
    private CeshiAdapter ceshiAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ceshi_ceshi;
    }

    @Override
    protected void initView() {

        //列表横点击事件
        listH.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                type = "1";
            }
        });

        //列表竖点击事件
        listS.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                type = "2";
            }
        });

        //表格点击事件
        biaoge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "3";
            }
        });
        //瀑布流点击事件
        pubu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "4";
            }
        });

        for (int i = 0; i < 50; i++) {
            arrayList.add("测试测试" + i);
        }

        //开始点击事件
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case "1":
                        LinearLayoutManager lm1 = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
                        ceshiAdapter = new CeshiAdapter(arrayList);
                        mList.setLayoutManager(lm1);
                        mList.setAdapter(ceshiAdapter);
                        ceshiAdapter.setOnItemClickListener(new YMRecyclerViewAdapter.OnItemClickListener<String>() {
                            @Override
                            public void onItemClick(View view, int position, String data) {
                                mFunctionManager.showShort("点击的是第" + position + "个，文案是：" + data);
                            }
                        });
                        break;
                    case "2":
                        LinearLayoutManager lm2 = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
                        ceshiAdapter = new CeshiAdapter(arrayList);
                        mList.setLayoutManager(lm2);
                        mList.setAdapter(ceshiAdapter);
                        ceshiAdapter.setOnItemClickListener(new YMRecyclerViewAdapter.OnItemClickListener<String>() {
                            @Override
                            public void onItemClick(View view, int position, String data) {
                                mFunctionManager.showShort("点击的是第" + position + "个，文案是：" + data);
                            }
                        });
                        break;
                    case "3":
                        GridLayoutManager lm3 = new GridLayoutManager(mContext, 2);
                        ceshiAdapter = new CeshiAdapter(arrayList);
                        mList.setLayoutManager(lm3);
                        mList.setAdapter(ceshiAdapter);
                        ceshiAdapter.setOnItemClickListener(new YMRecyclerViewAdapter.OnItemClickListener<String>() {
                            @Override
                            public void onItemClick(View view, int position, String data) {
                                mFunctionManager.showShort("点击的是第" + position + "个，文案是：" + data);
                            }
                        });
                        break;
                    case "4":
                        StaggeredGridLayoutManager lm4 = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
                        ceshiAdapter = new CeshiAdapter(arrayList);
                        mList.setLayoutManager(lm4);
                        mList.setAdapter(ceshiAdapter);
                        ceshiAdapter.setOnItemClickListener(new YMRecyclerViewAdapter.OnItemClickListener<String>() {
                            @Override
                            public void onItemClick(View view, int position, String data) {
                                mFunctionManager.showShort("点击的是第" + position + "个，文案是：" + data);
                            }
                        });
                        break;
                }
            }
        });
    }

    @Override
    protected void initData() {

    }
}
