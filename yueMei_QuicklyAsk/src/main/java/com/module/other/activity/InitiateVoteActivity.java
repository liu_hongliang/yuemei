package com.module.other.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.view.CommonTopBar;
import com.module.event.VoteMsgEvent;
import com.module.my.controller.activity.WriteSuibianLiaoActivity647;
import com.module.other.adapter.InitiateVoteAdapter;
import com.module.other.adapter.InitiateVoteAdapter2;
import com.module.other.fragment.InitiateVoteFragment;
import com.module.other.module.bean.SearchTaoDate;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;
import com.quicklyask.view.YueMeiDialog;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.xutils.common.util.DensityUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 文 件 名: ProjectContrastActivity
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-18 19:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：发起投票页面
 */

public class InitiateVoteActivity extends YMBaseActivity {
    @BindView(R.id.topBar)
    CommonTopBar topBar;
    @BindView(R.id.ed_title)
    EditText edTitle;
    @BindView(R.id.tv_edit_num)
    TextView tvEditNum;
    @BindView(R.id.switch_if_multiple)
    Switch switchIfMultiple;
    @BindView(R.id.tl)
    CommonTabLayout tl;
    @BindView(R.id.fl_change)
    FrameLayout flChange;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.tv_vote)
    TextView tvVote;
    @BindView(R.id.ll_tab_bottom)
    LinearLayout ll_tab_bottom;
    @BindView(R.id.tv_tab1)
    TextView tvTab1;
    @BindView(R.id.iv_tab1)
    ImageView ivTab1;
    @BindView(R.id.tv_tab2)
    TextView tvTab2;
    @BindView(R.id.iv_tab2)
    ImageView ivTab2;
    @BindView(R.id.ll_tab1_top)
    LinearLayout ll_tab1_top;
    @BindView(R.id.ll_tab2_top)
    LinearLayout ll_tab2_top;
    //标记上级页面
    private String mFrom;
    private List<SearchTaoDate.TaoListBean> tao_list = new ArrayList<>();
    private List<String> txtList;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    //SKU评论列表
    private InitiateVoteAdapter initiateVoteAdapter;
    //文字评论列表
    private InitiateVoteAdapter2 initiateVoteAdapter2;
    private ArrayList<Fragment> mFragments;
    private String[] mTitles = {"购物车", "收藏", "足迹"};
    //提示框
    private YueMeiDialog yueMeiDialog;
    private YueMeiDialog yueMeiDialog2;
    private DeleteDialogClickListener deleteDialogClickListener;
    private DeleteDialogClickListener2 deleteDialogClickListener2;
    public static ArrayList<String> selectList1;
    public static ArrayList<SearchTaoDate.TaoListBean> selectList2;
    public boolean isMultiple = false;
    //限制的最大字数
    public int num = 20;
    //标题
    private String mVoteTitle;
    //单选多选
    private String mIsMultiple;
    private ArrayList<SearchTaoDate.TaoListBean> rvList;
    // sku 文字 下标
    private int curTab = 1;

    @Override
    protected void onResume() {
        super.onResume();
        upUiSwitch();
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(VoteMsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case 1:
                selectList1 = (ArrayList<String>) msgEvent.getData();
                break;
            case 2:
                selectList2 = (ArrayList<SearchTaoDate.TaoListBean>) msgEvent.getData();
                tao_list = selectList2;
                initiateVoteAdapter.updata(selectList2);
                upUiSwitch();
                break;
            case 7:
                upUiSwitch();
                break;
            default:
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_initiate_vote;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) topBar.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        mFrom = getIntent().getStringExtra("from");

        //回传回来的值
        mVoteTitle = getIntent().getStringExtra("voteTitle");
        mIsMultiple = getIntent().getStringExtra("isMultiple");
        rvList = getIntent().getParcelableArrayListExtra("votedata");
        txtList = getIntent().getStringArrayListExtra("txtList");
        if (!TextUtils.isEmpty(mVoteTitle)) {
            edTitle.setText(mVoteTitle);
            tvEditNum.setText(mVoteTitle.length() + "/" + 20);
        }
        if (!TextUtils.isEmpty(mIsMultiple) && mIsMultiple.equals("2")) {
            //多选
            switchIfMultiple.setChecked(true);
            isMultiple = true;
        } else {
            //单选
            switchIfMultiple.setChecked(false);
            isMultiple = false;
        }

        selectList1 = new ArrayList<>();
        selectList2 = new ArrayList<>();

        //为了判断回传的时候
        if (txtList == null) {
            txtList = new ArrayList<>();
            curTab = 1;
        } else {
            curTab = 0;
        }

        if (rvList != null) {
            tao_list = rvList;
            selectList2 = rvList;
            for (int i = 0; i < rvList.size(); i++) {
                selectList1.add(rvList.get(i).getId().trim());
            }
        }

        yueMeiDialog = new YueMeiDialog(mContext, "差一步即可发起投票\n确认退出么？", "确定返回", "继续发起");
        deleteDialogClickListener = new DeleteDialogClickListener();
        yueMeiDialog.setBtnClickListener(deleteDialogClickListener);

        yueMeiDialog2 = new YueMeiDialog(mContext, "更改投票类型\n会清空当前输入的投票内容", "取消", "确定");
        deleteDialogClickListener2 = new DeleteDialogClickListener2();
        yueMeiDialog2.setBtnClickListener(deleteDialogClickListener2);


        topBar.getIv_left().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //返回
                if (mFrom.equals("PostingAndNoteFragment")) {
                    Intent i = new Intent();
                    setResult(1003, i);
                    finish();
                } else {
                    yueMeiDialog.show();
                }
            }
        });
        topBar.getTv_right().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edTitle.getText().toString().trim())) {
                    MyToast.makeTextToast2(mContext, "请填写标题", 1000).show();
                    return;
                }
                //新加一个逻辑 判断登录
                if (Utils.isLoginAndBind(mContext)) {
                    if (curTab == 1) {
                        if (tao_list != null && tao_list.size() < 2) {
                            MyToast.makeTextToast2(mContext, "最少输入两个投票项目", 1000).show();
                            return;
                        } else {
                            if (!mFrom.equals("PostingAndNoteFragment")) {
                                Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
                                //不知道为啥传cateid
                                it3.putExtra("cateid", "0" + ",1090");
                                if (isMultiple) {
                                    //多选
                                    it3.putExtra("isMultiple", "2");
                                } else {
                                    //单选
                                    it3.putExtra("isMultiple", "1");
                                }
                                //editText内容
                                it3.putExtra("voteTitle", edTitle.getText().toString());
                                //选中的skuid ,分隔
                                it3.putExtra("sku_id", StringUtils.strip(selectList1.toString(), "[]").trim());
                                it3.putParcelableArrayListExtra("sku_data", selectList2);
                                startActivity(it3);
                                finish();
                            } else {
                                Intent intent = new Intent();
                                //不知道为啥传cateid
                                intent.putExtra("cateid", "0" + ",1090");
                                if (isMultiple) {
                                    //多选
                                    intent.putExtra("isMultiple", "2");
                                } else {
                                    //单选
                                    intent.putExtra("isMultiple", "1");
                                }
                                //editText内容
                                intent.putExtra("voteTitle", edTitle.getText().toString());
                                //选中的skuid ,分隔
                                intent.putExtra("sku_id", StringUtils.strip(selectList1.toString(), "[]").trim());
                                intent.putParcelableArrayListExtra("sku_data", selectList2);
                                setResult(1002, intent);
                                finish();
                            }
                        }
                    } else {
                        if (txtList != null && txtList.size() < 2) {
                            MyToast.makeTextToast2(mContext, "最少输入两个投票项目", 1000).show();
                            return;
                        } else {
                            if (!mFrom.equals("PostingAndNoteFragment")) {
                                Intent it3 = new Intent(mContext, WriteSuibianLiaoActivity647.class);
                                //不知道为啥传cateid
                                it3.putExtra("cateid", "0" + ",1090");
                                if (isMultiple) {
                                    //多选
                                    it3.putExtra("isMultiple", "2");
                                } else {
                                    //单选
                                    it3.putExtra("isMultiple", "1");
                                }
                                //editText内容
                                it3.putExtra("voteTitle", edTitle.getText().toString());
                                it3.putStringArrayListExtra("txtList", (ArrayList<String>) initiateVoteAdapter2.getData());
                                startActivity(it3);
                                finish();
                            } else {
                                Intent intent = new Intent();
                                //不知道为啥传cateid
                                intent.putExtra("cateid", "0" + ",1090");
                                if (isMultiple) {
                                    //多选
                                    intent.putExtra("isMultiple", "2");
                                } else {
                                    //单选
                                    intent.putExtra("isMultiple", "1");
                                }
                                //editText内容
                                intent.putExtra("voteTitle", edTitle.getText().toString());
                                intent.putStringArrayListExtra("txtList", (ArrayList<String>) initiateVoteAdapter2.getData());
                                setResult(1002, intent);
                                finish();
                            }
                        }
                    }
                }
            }
        });

        //设置头部tab
        setTopTab();
        //设置底部tab
        setBottomTab();

        switchIfMultiple.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //选中时
                    isMultiple = true;
                } else {
                    //非选中时
                    isMultiple = false;
                }
            }
        });


        edTitle.addTextChangedListener(watcher);

    }

    private void setBottomTab() {
        mFragments = new ArrayList<>();
        mFragments.add(InitiateVoteFragment.newInstance(2 + ""));
        mFragments.add(InitiateVoteFragment.newInstance(3 + ""));
        mFragments.add(InitiateVoteFragment.newInstance(1 + ""));
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], 0, 0));
        }
        tl.setTabData(mTabEntities, this, R.id.fl_change, mFragments);
        //0.45
        ViewGroup.LayoutParams params = tl.getLayoutParams();
        params.width = (int) (DensityUtil.getScreenWidth() * 0.45);
        params.height = DensityUtil.dip2px(30);
        tl.setLayoutParams(params);
        tl.setIndicatorCornerRadius((float) 1.25);
        tl.setCurrentTab(0);
        //指示器宽3 长20
        tl.setIndicatorWidth(20);
        tl.setIndicatorHeight(3);
        tl.getTitleView(0).setTextSize(16);
        tl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                tl.setCurrentTab(position);
                for (int i = 0; i < mTitles.length; i++) {
                    if (position == i) {
                        tl.getTitleView(position).setTextSize(16);
                    } else {
                        tl.getTitleView(i).setTextSize(14);
                    }
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    private void setTopTab() {
        setAdapter();
        setAdapter2();
        if (curTab == 0) {
            setAdapter2();
            setTab1TopView();
            ll_tab_bottom.setVisibility(View.GONE);
            edTitle.setHint("输入您的投票标题（必填）");
        } else {
            setAdapter();
            setTab2TopView();
            edTitle.setHint("输入标题，例如，这两个项目哪个效果好");
            ll_tab_bottom.setVisibility(View.VISIBLE);
        }
        ll_tab1_top.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //列表有数据 或者 标题不为空
                    if (initiateVoteAdapter.getData().size() > 0 || !TextUtils.isEmpty(edTitle.getText().toString())) {
                        yueMeiDialog2.show();
                    } else {
                        curTab = 0;
                        edTitle.setText("");
                        setTab1TopView();
                        edTitle.setHint("输入您的投票标题（必填）");
                        ll_tab_bottom.setVisibility(View.GONE);
                        upUiSwitch();
                        setAdapter2();
                    }
                    return true;
                }
                return false;
            }
        });

        ll_tab2_top.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //列表有数据 或者 标题不为空
                    if (initiateVoteAdapter2.getData().size() > 0 || !TextUtils.isEmpty(edTitle.getText().toString())) {
                        yueMeiDialog2.show();
                    } else {
                        curTab = 1;
                        edTitle.setText("");
                        setTab2TopView();
                        ivTab1.setVisibility(View.INVISIBLE);
                        edTitle.setHint("输入标题，例如，这两个项目哪个效果好");
                        ll_tab_bottom.setVisibility(View.VISIBLE);
                        upUiSwitch();
                        setAdapter();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    //切换到文字投票更新视图方法
    private void setTab1TopView() {
        tvTab1.setTextColor(Color.parseColor("#333333"));
        tvTab1.setTextSize(16);
        ivTab1.setVisibility(View.VISIBLE);
        tvTab2.setTextColor(Color.parseColor("#999999"));
        tvTab2.setTextSize(14);
        ivTab2.setVisibility(View.INVISIBLE);
    }

    //切换到sku投票更新视图方法
    private void setTab2TopView() {
        tvTab1.setTextColor(Color.parseColor("#999999"));
        tvTab1.setTextSize(14);
        ivTab1.setVisibility(View.INVISIBLE);
        tvTab2.setTextColor(Color.parseColor("#333333"));
        tvTab2.setTextSize(16);
        ivTab2.setVisibility(View.VISIBLE);
    }

    //输入框监听
    TextWatcher watcher = new TextWatcher() {
        private CharSequence temp;
        private int selectionStart;
        private int selectionEnd;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //只要编辑框内容有变化就会调用该方法，s为编辑框变化后的内容
            temp = s;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //编辑框内容变化之前会调用该方法，s为编辑框内容变化之前的内容
        }

        @Override
        public void afterTextChanged(Editable s) {
            //编辑框内容变化之后会调用该方法，s为编辑框内容变化后的内容X
            tvEditNum.setText(s.length() + "/" + 20);
            selectionStart = edTitle.getSelectionStart();
            selectionEnd = edTitle.getSelectionEnd();
            if (temp.length() > num) {
                s.delete(selectionStart - 1, selectionEnd);
                int tempSelection = selectionStart;
                edTitle.setText(s);
                edTitle.setSelection(tempSelection);//设置光标在最后
            }
        }
    };


    @Override
    protected void initData() {

    }

    private void setAdapter2() {
        initiateVoteAdapter2 = new InitiateVoteAdapter2(mContext, txtList);
        YMLinearLayoutManager linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setStackFromEnd(true);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(initiateVoteAdapter2);

        initiateVoteAdapter2.setOnTextChangeListener(new InitiateVoteAdapter2.onTextChangeListener() {
            @Override
            public void onTextChanged(int pos, String str, EditText ed, HashMap<Integer, String> contents) {
                Log.i("302", "pos///" + pos + "content///" + contents);
                for (Map.Entry<Integer, String> entry : contents.entrySet()) {
                    if (initiateVoteAdapter2.getData().size() > 0) {
                        if (initiateVoteAdapter2.getData().size() - 1 >= entry.getKey()
                                && !TextUtils.isEmpty(initiateVoteAdapter2.getData().get(entry.getKey()))) {
                                initiateVoteAdapter2.getData().set(entry.getKey(), entry.getValue());
                        } else {
                            if(!TextUtils.isEmpty(entry.getValue())){
                                initiateVoteAdapter2.getData().add(initiateVoteAdapter2.getData().size(), entry.getValue());
                            }
                        }
                    } else {
                        if(!TextUtils.isEmpty(entry.getValue())) {
                            initiateVoteAdapter2.getData().add(initiateVoteAdapter2.getData().size(), entry.getValue());
                        }
                    }
                }
                upUiSwitch();
            }
        });

        initiateVoteAdapter2.setOnItemDeleteClickListener(new InitiateVoteAdapter2.OnItemDeleteClickListener() {
            @Override
            public void onItemDeleteClick(View view, int pos) {
                txtList.remove(pos);
                initiateVoteAdapter2.updata(txtList);
                upUiSwitch();
            }
        });
    }

    private void setAdapter() {
        initiateVoteAdapter = new InitiateVoteAdapter(mContext, tao_list);
        YMLinearLayoutManager linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(initiateVoteAdapter);

        initiateVoteAdapter.setOnItemClickListener(new InitiateVoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (initiateVoteAdapter.getItemViewType(pos) == initiateVoteAdapter.ITEM3_TYPE_TAG) {
                    //详情页
//                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
//                    intent.putExtra("id", initiateVoteAdapter.getData().get(pos).get_id());
//                    intent.putExtra("source", "0");
//                    intent.putExtra("objid", "0");
//                    mContext.startActivity(intent);
                } else {
                    //添加
                    Intent intent = new Intent(InitiateVoteActivity.this, SearchProjectActivity.class);
                    intent.putExtra("from", "InitiateVoteActivity");
                    intent.putStringArrayListExtra("taoIdList", selectList1);
                    intent.putExtra("taoData", selectList2);
                    startActivityForResult(intent, 1000);
                }
            }
        });

        initiateVoteAdapter.setOnItemDeleteClickListener(new InitiateVoteAdapter.OnItemDeleteClickListener() {
            @Override
            public void onItemDeleteClick(View view, int pos) {
                selectList1.remove(pos);
                selectList2.remove(pos);
                initiateVoteAdapter.removeItem(pos);
                upUiSwitch();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 10010) {
            EventBus.getDefault().post(new VoteMsgEvent(1, data.getStringArrayListExtra("id")));
            EventBus.getDefault().post(new VoteMsgEvent(2, data.getStringArrayListExtra("taoData")));
        }
    }

    //更新switch
    private void upUiSwitch() {
        if (curTab == 1) {
            if (selectList2 != null && selectList2.size() > 2) {
                switchIfMultiple.setEnabled(true);
                tvVote.setTextColor(Color.parseColor("#666666"));
            } else {
                if (isMultiple) {
                    switchIfMultiple.setChecked(false);
                }
                switchIfMultiple.setEnabled(false);
                tvVote.setTextColor(Color.parseColor("#999999"));
            }
        } else {
            if (initiateVoteAdapter2.getData() != null && initiateVoteAdapter2.getData().size() > 2) {
                switchIfMultiple.setEnabled(true);
                tvVote.setTextColor(Color.parseColor("#666666"));
            } else {
                if (isMultiple) {
                    switchIfMultiple.setChecked(false);
                }
                switchIfMultiple.setEnabled(false);
                tvVote.setTextColor(Color.parseColor("#999999"));
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    /**
     * @param context
     */
    public static void invoke(Context context) {
        Intent intent = new Intent(context, InitiateVoteActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (yueMeiDialog != null) {
            yueMeiDialog.dismiss();
        }
        if (yueMeiDialog2 != null) {
            yueMeiDialog2.dismiss();
        }
    }

    class TabEntity implements CustomTabEntity {
        public String title;
        public int selectedIcon;
        public int unSelectedIcon;

        public TabEntity(String title, int selectedIcon, int unSelectedIcon) {
            this.title = title;
            this.selectedIcon = selectedIcon;
            this.unSelectedIcon = unSelectedIcon;
        }

        @Override
        public String getTabTitle() {
            return title;
        }

        @Override
        public int getTabSelectedIcon() {
            return selectedIcon;
        }

        @Override
        public int getTabUnselectedIcon() {
            return unSelectedIcon;
        }
    }

    private class DeleteDialogClickListener implements YueMeiDialog.BtnClickListener {

        @Override
        public void leftBtnClick() {
            Intent i = new Intent();
            setResult(1003, i);
            finish();
        }

        @Override
        public void rightBtnClick() {
            yueMeiDialog.dismiss();
        }
    }

    private class DeleteDialogClickListener2 implements YueMeiDialog.BtnClickListener {

        @Override
        public void leftBtnClick() {
            yueMeiDialog2.dismiss();
        }

        @Override
        public void rightBtnClick() {
            if (curTab == 1) {
                //清空
                selectList2.clear();
                tao_list = selectList2;
                setAdapter2();
                //切换
                curTab = 0;
                setTab1TopView();
                ll_tab_bottom.setVisibility(View.GONE);
                edTitle.setHint("输入您的投票标题（必填）");
            } else {
                //清空
                txtList.clear();
                setAdapter();
                //切换
                curTab = 1;
                setTab2TopView();
                ll_tab_bottom.setVisibility(View.VISIBLE);
                edTitle.setHint("输入标题，例如，这两个项目哪个效果好");
            }
            edTitle.setText("");
            upUiSwitch();
            yueMeiDialog2.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (mFrom.equals("PostingAndNoteFragment")) {
                Intent i = new Intent();
                setResult(1003, i);
                finish();
            } else {
                yueMeiDialog.show();
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

}
