package com.module.other.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.mobstat.StatService;
import com.module.base.api.BaseCallBackListener;
import com.module.commonview.activity.DiariesAndPostsActivity;
import com.module.commonview.adapter.TaoPopAdapter;
import com.module.community.controller.adapter.BBsListAdapter;
import com.module.community.model.api.PartListApi;
import com.module.community.model.bean.BBsListData550;
import com.module.doctor.model.bean.GroupDiscData;
import com.module.doctor.model.bean.TaoPopItemIvData;
import com.module.doctor.view.ProjectDetailListFragment;
import com.module.home.view.LoadingProgress;
import com.module.other.adapter.MyAdapter8;
import com.module.other.api.LookRijiApi;
import com.module.other.module.bean.TaoPopItemData;
import com.quicklyask.activity.R;
import com.quicklyask.activity.interfaces.Project2ListSelectListener;
import com.quicklyask.util.Utils;
import com.quicklyask.view.DropDownListView;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;

import org.kymjs.aframe.ui.ViewInject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 社区首页更多日记 看日记
 *
 * Created by dwb on 16/3/8.
 */
public class LookRijiActivity extends FragmentActivity {

    private final String TAG = "LookRijiActivity";

    private Context mContex;

    // List
    private DropDownListView mlist;
    private int mCurPage = 1;
    private Handler mHandler;
    private List<BBsListData550> lvHotIssueData = new ArrayList<>();
    private List<BBsListData550> lvHotIssueMoreData = new ArrayList<>();
    private BBsListAdapter hotAdpter;

    private LinearLayout nodataTv;// 数据为空时候的显示

    private RelativeLayout partRly;
    private RelativeLayout sortRly;
    private TextView partTv;
    private TextView sortTv;
    private ImageView partIv;
    private ImageView sortIv;

    private RelativeLayout otherRly;
    private LinearLayout partSearchLy;

    private List<TaoPopItemIvData> lvGroupData = new ArrayList<>();
    private TaoPopAdapter mSort2Adapter;
    private List<TaoPopItemData> lvSortData = new ArrayList<>();
    private String sortStr = "1";// 筛选
    private int sort_po = 0;

    SortPopupwindows sortPop;

    private String partId = "0";
    private String p_partId = "0";
    private String partName;// 部位名

    public static int mPosition;
    private MyAdapter8 adapter;
    private ListView partlist;
    private ProjectDetailListFragment mFragment;

    private TextView titleTv;
    private LinearLayout backRly;

    private String type="1";
    private LoadingProgress mDialog;
    private LookRijiApi lookRijiApi;
    private HashMap<String, Object> lookRijiMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acty_look_riji);
        mContex = LookRijiActivity.this;

        lookRijiApi = new LookRijiApi();


        mDialog = new LoadingProgress(LookRijiActivity.this);

        findView();
        initList();
        setListner();
    }

    void findView() {
        titleTv=findViewById(R.id.title_name);
        backRly=findViewById(R.id.title_bar_rly);

        mlist =  findViewById(R.id.my_doc_list_view);

        partRly =  findViewById(R.id.project_part_pop_rly1);
        sortRly =  findViewById(R.id.project_sort_pop_rly);
        partTv =  findViewById(R.id.project_part_pop_tv);
        sortTv =  findViewById(R.id.project_sort_pop_tv);
        partIv =  findViewById(R.id.project_part_pop_iv);
        sortIv =  findViewById(R.id.project_sort_pop_iv);

        otherRly =  findViewById(R.id.ly_content_ly1);
        partSearchLy =  findViewById(R.id.part_search_ly);
        partlist =  findViewById(R.id.pop_project_listview);

        nodataTv =  findViewById(R.id.my_collect_post_tv_nodata);

        sortPop = new SortPopupwindows(mContex, partRly);

        titleTv.setText("看日记");

        loadPartList();
    }

    void setListner() {

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        sortRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                partSearchLy.setVisibility(View.GONE);
                if (sortPop.isShowing()) {
                    sortPop.dismiss();
                } else {
                    sortPop.showAsDropDown(partRly, 0, 0);
                }
                initpop();
            }
        });

        otherRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                partSearchLy.setVisibility(View.GONE);
                initpop();
            }
        });

        partRly.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                loadPartList();
                if (partSearchLy.getVisibility() == View.GONE) {
                    partSearchLy.setVisibility(View.VISIBLE);
                } else {
                    partSearchLy.setVisibility(View.GONE);
                }
                initpop();
            }
        });

        backRly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    void initpop() {
        if (partSearchLy.getVisibility() == View.VISIBLE) {
            partTv.setTextColor(Color.parseColor("#E95165"));
            partIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            partTv.setTextColor(Color.parseColor("#414141"));
            partIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
        if (sortPop.isShowing()) {
            sortTv.setTextColor(Color.parseColor("#E95165"));
            sortIv.setBackgroundResource(R.drawable.red_tao_tab);
        } else {
            sortTv.setTextColor(Color.parseColor("#414141"));
            sortIv.setBackgroundResource(R.drawable.gra_tao_tab);
        }
    }

    void loadPartList() {
        mPosition = 0;
        Map<String,Object> maps=new HashMap<>();
        maps.put("flag","3");
        new PartListApi().getCallBack(mContex, maps, new BaseCallBackListener<List<TaoPopItemIvData>>() {

            @Override
            public void onSuccess(List<TaoPopItemIvData> taoPopItemIvData) {
                if (taoPopItemIvData != null){
                    lvGroupData=taoPopItemIvData;
                    for (int i = 0; i < lvGroupData.size(); i++) {
                        if (p_partId.equals(lvGroupData.get(i)
                                .get_id())) {
                            mPosition = i;
                        }
                    }
                    adapter = new MyAdapter8(
                            LookRijiActivity.this,
                            lvGroupData);
                    partlist.setAdapter(adapter);

                    // 创建MyFragment对象
                    mFragment = new ProjectDetailListFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                            .beginTransaction();
                    mFragment
                            .setCascadingMenuViewOnSelectListener(new NMProject2ListSelectListener());
                    fragmentTransaction
                            .replace(R.id.pop_fragment_container2,
                                    mFragment);

                    // 通过bundle传值给MyFragment
                    Bundle bundle = new Bundle();
                    bundle.putString("id",
                            lvGroupData.get(mPosition).get_id());
                    bundle.putString("z_id", partId);
                    mFragment.setArguments(bundle);
                    fragmentTransaction.commitAllowingStateLoss();
                }else{
                    ViewInject.toast("请求错误");
                }
            }
        });
    }

    // 级联菜单选择回调接口
    class NMProject2ListSelectListener implements Project2ListSelectListener {

        @Override
        public void getValue(GroupDiscData projectItem) {
            partSearchLy.setVisibility(View.GONE);
            partName = projectItem.getCate_name();
            partId = projectItem.get_id();
            partTv.setText(partName);
            initpop();
            onreshData();
        }
    }

    void onreshData() {
        lvHotIssueData = null;
        lvHotIssueMoreData = null;
        mCurPage = 1;
        mDialog.startLoading();
        lodHotIssueData(true);
        mlist.setHasMore(true);
    }

    /**
     * 顺序下拉选择
     *
     * @author Rubin
     *
     */
    public class SortPopupwindows extends PopupWindow {

        @SuppressWarnings("deprecation")
        public SortPopupwindows(Context mContext, View v) {

            final View view = View.inflate(mContext,
                    R.layout.pop_tao_zx_project, null);

            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_ins));

            setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
            setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            update();

            final ListView prlist1 = view
                    .findViewById(R.id.pop_list_tao_project_list);
            RelativeLayout otherRly1 = view
                    .findViewById(R.id.ly_content_ly1);
            RelativeLayout otherRly2 = view
                    .findViewById(R.id.ly_content_ly2);
            otherRly1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    sortPop.dismiss();
                    initpop();
                }
            });
            otherRly2.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    if (Utils.isFastDoubleClick()) {
                        return;
                    }
                    sortPop.dismiss();
                    initpop();
                }
            });
            TaoPopItemData a1 = new TaoPopItemData();
            a1.set_id("1");
            a1.setName("最新日记");
            TaoPopItemData a2 = new TaoPopItemData();
            a2.set_id("2");
            a2.setName("最新回复");
            TaoPopItemData a3 = new TaoPopItemData();
            a3.set_id("3");
            a3.setName("精华日记");
            lvSortData.add(a1);
            lvSortData.add(a2);
            lvSortData.add(a3);

            mSort2Adapter = new TaoPopAdapter(
                    LookRijiActivity.this.mContex, lvSortData, sort_po);
            prlist1.setAdapter(mSort2Adapter);

            prlist1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int pos, long arg3) {
                    sortStr = lvSortData.get(pos).get_id();
                    sort_po = pos;

                    sortTv.setText(lvSortData.get(pos).getName());
                    sortPop.dismiss();
                    initpop();
                    onreshData();

                    mSort2Adapter = new TaoPopAdapter(
                            LookRijiActivity.this.mContex, lvSortData,
                            sort_po);
                    prlist1.setAdapter(mSort2Adapter);

                }
            });
        }
    }



    void initList() {

        mHandler = getHandler();
        mDialog.startLoading();
        lodHotIssueData(true);

        mlist.setOnDropDownListener(new DropDownListView.OnDropDownListener() {

            @Override
            public void onDropDown() {
                lvHotIssueData = null;
                lvHotIssueMoreData = null;
                mCurPage = 1;
                mDialog.startLoading();
                lodHotIssueData(true);
                mlist.setHasMore(true);
            }
        });

        mlist.setOnBottomListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lodHotIssueData(false);
            }
        });

        mlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adpter, View v, int pos,
                                    long arg3) {

                if (null != lvHotIssueData && lvHotIssueData.size() > 0) {
                    String url = lvHotIssueData.get(pos).getUrl();
                    String qid = lvHotIssueData.get(pos).getQ_id();
                    Intent it2 = new Intent();
                    it2.putExtra("url", url);
                    it2.putExtra("qid", qid);
                    it2.setClass(mContex, DiariesAndPostsActivity.class);
                    startActivity(it2);

                }
            }
        });
    }

    void lodHotIssueData(final boolean isDonwn) {

        lookRijiMap.put("cateid",partId);
        lookRijiMap.put("page",mCurPage+"");
        lookRijiMap.put("sort",sortStr);
        lookRijiMap.put("type",type);
        lookRijiApi.getCallBack(mContex, lookRijiMap, new BaseCallBackListener<List<BBsListData550>>() {
            @Override
            public void onSuccess(List<BBsListData550> docListDatas) {
                Message msg = null;
                if (isDonwn) {
                    if (mCurPage == 1) {
                        lvHotIssueData = docListDatas;

                        msg = mHandler.obtainMessage(1);
                        msg.sendToTarget();
                    }
                } else {
                    mCurPage++;
                    lvHotIssueMoreData = docListDatas;
                    msg = mHandler.obtainMessage(2);
                    msg.sendToTarget();
                }
            }
        });

    }

    @SuppressLint("HandlerLeak")
    private Handler getHandler() {
        return new Handler() {
            @SuppressLint({ "NewApi", "SimpleDateFormat" })
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:

                        if (null!=lvHotIssueData && lvHotIssueData.size() > 0) {

                            nodataTv.setVisibility(View.GONE);
                            mlist.setVisibility(View.VISIBLE);

                            mDialog.stopLoading();

                            hotAdpter = new BBsListAdapter(
                                    LookRijiActivity.this, lvHotIssueData);
                            mlist.setAdapter(hotAdpter);

                            SimpleDateFormat dateFormat = new SimpleDateFormat(
                                    "MM-dd HH:mm:ss");

                            mlist.onDropDownComplete(getString(R.string.update_at)
                                    + dateFormat.format(new Date()));
                            mlist.onBottomComplete();
                        } else {
                            mDialog.stopLoading();
                            nodataTv.setVisibility(View.VISIBLE);
                            mlist.setVisibility(View.GONE);
                        }
                        break;
                    case 2:
                        if (null!=lvHotIssueMoreData && lvHotIssueMoreData.size() > 0) {
                            hotAdpter.add(lvHotIssueMoreData);
                            hotAdpter.notifyDataSetChanged();
                            mlist.onBottomComplete();
                        } else {
                            mlist.setHasMore(false);
                            mlist.setShowFooterWhenNoMore(true);
                            mlist.onBottomComplete();
                        }
                        break;
                }

            }
        };
    }

    /*
     *
     * @see android.support.v4.app.FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        StatService.onResume(this);
        TCAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        StatService.onPause(this);
        TCAgent.onPause(this);
    }
}

