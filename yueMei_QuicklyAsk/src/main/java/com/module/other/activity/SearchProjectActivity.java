package com.module.other.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;

import com.baidu.mapapi.model.inner.GeoPoint;
import com.module.api.AddTaoPKApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.commonview.adapter.ProjectRightListAdapter;
import com.module.commonview.module.api.LoadSearchTaoDataApi;
import com.module.commonview.module.api.LoadTwoTreeListApi;
import com.module.commonview.view.BaseCityPopwindows;
import com.module.commonview.view.BaseProjectPopupwindows;
import com.module.commonview.view.BaseSortPopupwindows;
import com.module.commonview.view.SortScreenPopwin;
import com.module.community.controller.adapter.TaoListAdapter;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.doctor.model.api.FilterDataApi;
import com.module.my.model.bean.ProjcetData;
import com.module.my.model.bean.ProjcetList;
import com.module.other.adapter.SearchProjectAdapter;
import com.module.other.module.bean.MakeTagData;
import com.module.other.module.bean.SearchTaoDate;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.module.bean.TickData;
import com.module.other.module.bean.TickDataInfo;
import com.module.other.netWork.netWork.ServerData;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.Location;
import com.quicklyask.entity.SearchResultBoard;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.Utils;
import com.quicklyask.view.EditExitDialog;
import com.quicklyask.view.MyToast;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.apache.commons.lang.StringUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 文 件 名: ProjectContrastActivity
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-18 19:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：添加项目页面
 */

public class SearchProjectActivity extends YMBaseActivity {
    @BindView(R.id.rl)//标题栏
            RelativeLayout rl;
    @BindView(R.id.iv_back)//返回键
            ImageView iv_back;
    @BindView(R.id.ll_back_contract)//底部按钮
            LinearLayout ll_back_contract;
    @BindView(R.id.rv_project)//筛选列表
            RecyclerView rv_project;
    @BindView(R.id.refresh)//刷新
            SmartRefreshLayout refresh;

    @BindView(R.id.project_part_pop_rly1)//项目
            RelativeLayout project_part_pop_rly1;
    @BindView(R.id.project_part_pop_tv)
    TextView project_part_pop_tv;
    @BindView(R.id.project_part_pop_iv)
    ImageView project_part_pop_iv;

    @BindView(R.id.project_diqu_pop_rly)//地区
            RelativeLayout project_diqu_pop_rly;
    @BindView(R.id.project_diqu_pop_tv)
    TextView project_diqu_pop_tv;
    @BindView(R.id.project_diqu_pop_iv)
    ImageView project_diqu_pop_iv;

    @BindView(R.id.project_sort_pop_rly)//智能排序
            RelativeLayout project_sort_pop_rly;
    @BindView(R.id.project_sort_pop_tv)
    TextView project_sort_pop_tv;
    @BindView(R.id.project_sort_pop_iv)
    ImageView project_sort_pop_iv;

    @BindView(R.id.project_kind_pop_rly)//筛选
            RelativeLayout project_kind_pop_rly;
    @BindView(R.id.project_kind_pop_tv)
    TextView project_kind_pop_tv;
    @BindView(R.id.project_kind_pop_iv)
    ImageView project_kind_pop_iv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_hint_title)
    TextView tvHintTitle;

    //    @BindView(R.id.view_borad)
//    LinearLayout mRecycler;
//    private BaseNetWorkCallBackApi getScreenBoard;
//    private View headerView;
//    private SearhBoardRecyclerAdapter mBoardRecyclerAdapter;
    //城市选择
    private BaseCityPopwindows cityPop;
    //项目选择
    private BaseProjectPopupwindows projectPop;
    //智能排序
    private BaseSortPopupwindows sortPop;
    //筛选
    private SortScreenPopwin kindPop;
    //智能排序数据
    private List<TaoPopItemData> lvSortData = new ArrayList<>();
    //筛选数据
    private ArrayList<ProjcetList> kindStr = new ArrayList<>();
    //项目详情数据
    private List<MakeTagData> mData = new ArrayList<>();
    private ArrayList<String> selectList;
    private SearchProjectAdapter searchProjectAdapter;
    private TaoListAdapter mProjectSkuAdapter;
    private SearchResultBoard searchActivity;                       //活动
    private LoadSearchTaoDataApi loadSearchTaoDataApi;
    private YMLinearLayoutManager linearLayoutManager;
    //活动的recycleview 目前没用上
//    private SearchResultBoard searchResultBoard;
    // 筛选
    private String sortStr = "1";
    private String curPid = "0";
    private String partId = "0";
    private int sortPos = 0;
    //页数
    private int mDataPage = 1;
    public static final int SHOW_PROJECT_POP = 0x0111;
    private AddTaoPKApi addTaoPKApi;
    private String last_time = "";
    private String mCity = "全国";
    private LocationManager lm;
    private int GPS_REQUEST_CODE = 100;
    private String provinceLocation;
    private LocationClient locationClient;
    private static String tao_id;
    private String from = "";
    private ArrayList<String> VoteIdList;//投票页已经选择的
    //项目对比选择
//    private List<String> selectDatas;
    public ArrayList<SearchTaoDate.TaoListBean> selectList2;
    //    List<SearchTaoDate.TaoListBean> tao_list;
    private TickDataInfo tickDataInfo;
    private List<TickData> list;
    private Handler mHandler = new MyHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private static class MyHandler extends Handler {
        private final WeakReference<SearchProjectActivity> mActivity;

        private MyHandler(SearchProjectActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SearchProjectActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    case SHOW_PROJECT_POP:
                        //判断是否首次进入
                        if (!TextUtils.isEmpty(theActivity.from)) {
                            //投票进入
                            if (Cfg.loadStr(theActivity, "first_click_vote", "0").equals("0")) {
                                if (theActivity.projectPop != null && theActivity.projectPop.isShowing()) {
                                    theActivity.projectPop.dismiss();
                                } else {
                                    theActivity.projectPop.showPop();
                                    Cfg.saveStr(theActivity, "first_click_vote", "1");
                                }
                                theActivity.initpop();
                            }
                        } else {
                            //对比进入
                            if (Cfg.loadStr(theActivity, "first_click_add_contrast", "0").equals("0")) {
                                if (theActivity.projectPop != null && theActivity.projectPop.isShowing()) {
                                    theActivity.projectPop.dismiss();
                                } else {
                                    theActivity.projectPop.showPop();
                                    Cfg.saveStr(theActivity, "first_click_add_contrast", "1");
                                }
                            }
                            theActivity.initpop();
                        }
                        break;
                    default:
                        break;

                }
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_project_serch;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        from = getIntent().getStringExtra("from");
        if (!TextUtils.isEmpty(from)) {
            tvTitle.setText("我的投票");
            tvHintTitle.setText("点击商品即可添加到投票");
            ll_back_contract.setVisibility(View.GONE);
            VoteIdList = getIntent().getStringArrayListExtra("taoIdList");
            selectList2 = getIntent().getParcelableArrayListExtra("taoData");
            if (VoteIdList == null) {
                VoteIdList = new ArrayList<>();
            }
            if (selectList2 == null) {
                selectList2 = new ArrayList<>();
            }
        } else {
//            selectDatas = getIntent().getStringArrayListExtra("taoIdList");
//            if (selectDatas == null) {
//                selectDatas = new ArrayList<>();
//            }
            tvTitle.setText("我的对比");
            tvHintTitle.setText("点击商品即可添加到对比");
            ll_back_contract.setVisibility(View.VISIBLE);
        }

        lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        selectList = new ArrayList<>();
        setPopData();
        initListener();
        //加载更多和刷新
        refresh.setEnableFooterFollowWhenLoadFinished(true);
        refresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                lodHotIssueData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                reshData();
            }
        });

        tickDataInfo = new TickDataInfo();
        list = new ArrayList<>();
//        if (tao_list == null) {
//            tao_list = new ArrayList<>();
//        }
//        searchProjectAdapter = new SearchProjectAdapter(mContext, tao_list);
//        linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
//        rv_project.setLayoutManager(linearLayoutManager);
//        rv_project.setAdapter(searchProjectAdapter);
    }

    private void initListener() {

        setMultiOnClickListener(iv_back, ll_back_contract, project_part_pop_rly1, project_diqu_pop_rly, project_sort_pop_rly, project_kind_pop_rly);

        //城市点击回调
        cityPop.setOnAllClickListener(new BaseCityPopwindows.OnAllClickListener() {
            @Override
            public void onAllClick(String city) {
                mCity = city;
                project_diqu_pop_tv.setText(mCity);
                Cfg.saveStr(SearchProjectActivity.this, FinalConstant.DWCITY, mCity);
                reshData();
                cityPop.dismiss();
                initpop();
            }
        });

        //智能排序点击回调
        sortPop.setOnSequencingClickListener(new BaseSortPopupwindows.OnSequencingClickListener() {
            @Override
            public void onSequencingClick(int pos, String sortId, String sortName) {
                sortPos = pos;
                if (pos == 5) {
                    boolean ok = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    Log.e(TAG, "ok == " + ok);
                    if (ok) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Acp.getInstance(SearchProjectActivity.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                                @Override
                                public void onGranted() {
                                    initLocation();
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    sortPop.dismiss();
                                }
                            });
                        } else {
                            initLocation();
                        }
                    } else {
                        sortPop.dismiss();
                        showDialogExitEdit3();
                    }
                } else {
                    intelligentSorting(sortPos);
                }
            }
        });

        sortPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

        cityPop.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                initpop();
            }
        });

    }


    private void intelligentSorting(int pos) {
        sortPop.setAdapter(pos);
        sortStr = lvSortData.get(pos).get_id();
        project_sort_pop_tv.setText(lvSortData.get(pos).getName());
        sortPop.dismiss();
        initpop();
        reshData();
    }


    /**
     * 设置智能排序和筛选所需要的数据
     */
    private void setPopData() {
        loadPart();             //全部项目
        loadCity();             //城市
        loadIntelligentSort();  //智能排序
        loadSXData();           //获取筛选列表数据
//        lodBoardData();         //获取活动
        lodHotIssueData();      //列表
    }

    private void loadPart() {
        loadTwoTreeList();      //获取项目列表数据
    }


    private void loadTwoTreeList() {
        Map<String, Object> maps = new HashMap<>();
        new LoadTwoTreeListApi().getCallBack(mContext, maps, new BaseCallBackListener<List<MakeTagData>>() {
            @Override
            public void onSuccess(List<MakeTagData> serverData) {
                if (serverData != null && serverData.size() != 0) {
                    mData = serverData;

                    projectPop = new BaseProjectPopupwindows(mContext, project_part_pop_rly1, serverData, "");
                    projectPop.setmServerData(serverData);

                    projectPop.setOneid("");
                    projectPop.setTwoid("");
                    projectPop.setThreeid("");

                    projectPop.setLeftView();
                    projectPop.setRightView(projectPop.getmPos());

                    setProPopTitle();

                    //滚动到相应位置
                    projectPop.getmLeftRecy().scrollToPosition(projectPop.getmPos());
                    ProjectRightListAdapter rightListAdapter = projectPop.getRightListAdapter();
                    int selectedPos = 0;
                    if (rightListAdapter != null) {
                        selectedPos = rightListAdapter.getmSelectedPos();
                    }
                    projectPop.getRightList().setSelection(selectedPos);

                    projectPop.setOnItemSelectedClickListener(new BaseProjectPopupwindows.OnItemSelectedClickListener() {
                        @Override
                        public void onItemSelectedClick(String id, String name) {
                            partId = id;
                            if (!TextUtils.isEmpty(name)) {
                                project_part_pop_tv.setText(name);
                            }
                            reshData();
                        }
                    });

                    //筛选关闭
                    projectPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            initpop();
                        }
                    });
                }
            }
        });
    }

    private void setProPopTitle() {
        //设置所选项目id和和title1
        if (projectPop.getmTwoPos() > 0) {
            if (projectPop.getmThreePos() > 0) {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getList().get(projectPop.getmThreePos()).getName();
                project_part_pop_tv.setText(name);
            } else {
                String name = mData.get(projectPop.getmPos()).getList().get(projectPop.getmTwoPos()).getName();
                project_part_pop_tv.setText(name);
            }

        } else {
            if ("0".equals("0")) {
                project_part_pop_tv.setText("全部项目");
            } else {
                String name = mData.get(projectPop.getmPos()).getName();
                project_part_pop_tv.setText(name);
            }
            partId = "0";
        }
    }

    private void loadCity() {
        mCity = Cfg.loadStr(SearchProjectActivity.this, FinalConstant.DWCITY, "");
        if (mCity.length() > 0) {
            if (mCity.equals("失败")) {
                mCity = "全国";
            } else {
            }
        } else {
            mCity = "全国";
        }
        project_diqu_pop_tv.setText(mCity);
        cityPop = new BaseCityPopwindows(SearchProjectActivity.this, project_part_pop_rly1);
    }

    private void loadIntelligentSort() {
        TaoPopItemData a1 = new TaoPopItemData();
        a1.set_id("1");
        a1.setName("智能排序");
        TaoPopItemData a2 = new TaoPopItemData();
        a2.set_id("2");
        a2.setName("销量最高");
        TaoPopItemData a3 = new TaoPopItemData();
        a3.set_id("3");
        a3.setName("日记和案例最多");
        TaoPopItemData a4 = new TaoPopItemData();
        a4.set_id("4");
        a4.setName("最新上架");
        TaoPopItemData a5 = new TaoPopItemData();
        a5.set_id("5");
        a5.setName("价格从低到高");
        TaoPopItemData a6 = new TaoPopItemData();
        a6.set_id("7");
        a6.setName("离我最近");
        lvSortData.add(a1);
        lvSortData.add(a2);
        lvSortData.add(a3);
        lvSortData.add(a4);
        lvSortData.add(a5);
        lvSortData.add(a6);
        sortPop = new BaseSortPopupwindows(SearchProjectActivity.this, project_part_pop_rly1, lvSortData);

    }

    /**
     * 筛选数据请求
     */
    private void loadSXData() {
        FilterDataApi filterDataApi = new FilterDataApi();
        filterDataApi.getHashMap().clear();
        filterDataApi.getHashMap().put("partId", curPid);
        filterDataApi.getCallBack(mContext, filterDataApi.getHashMap(), new BaseCallBackListener<ArrayList<ProjcetData>>() {
            @Override
            public void onSuccess(ArrayList<ProjcetData> lvkindData) {
                if (lvkindData != null && lvkindData.size() != 0) {
                    kindPop = new SortScreenPopwin(SearchProjectActivity.this, project_part_pop_rly1, lvkindData);

                    kindPop.setOnButtonClickListener(new SortScreenPopwin.OnButtonClickListener() {
                        @Override
                        public void onResetListener(View view) {
                            if (kindPop != null) {
                                kindStr.clear();
                                kindPop.resetData();
                            }
                        }

                        @Override
                        public void onSureListener(View view, ArrayList data) {
                            if (kindPop != null) {
                                kindStr = kindPop.getSelectedData();
                                reshData();
                                kindPop.dismiss();
                            }
                        }
                    });

                    kindPop.setOnDismissListener(new PopupWindow.OnDismissListener() {
                        @Override
                        public void onDismiss() {
                            initpop();
                        }
                    });
                }
            }
        });
    }

    /**
     * 刷新数据
     */
    private void reshData() {
        searchProjectAdapter = null;
        mDataPage = 1;
        lodHotIssueData();
    }


    @Override
    protected void initData() {

    }

    /**
     * 设置头布局
     */
//    private void setHeadView(ArrayList<SearchResultBoard> resultBoards) {
//        if (headerView != null) {
//            mRecycler.removeView(headerView);
//        }
//
//        headerView = View.inflate(mContext, R.layout.project_tao_top_view, null);
//        RecyclerView mActivityRecycler = headerView.findViewById(R.id.project_activity_recycler);
//
//        headerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//
//        mRecycler.addView(headerView);
//
//        if (mProjectSkuAdapter != null) {
//            mProjectSkuAdapter.isHeadView(headerView != null);
//        }
//
//        //活动列表设置
//        if (mBoardRecyclerAdapter == null) {
//            LinearLayoutManager activityLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
//            ((DefaultItemAnimator) mActivityRecycler.getItemAnimator()).setSupportsChangeAnimations(false);   //取消局部刷新动画效果(是因为局部刷新的是每一个item,而不是item的部分)
//            mBoardRecyclerAdapter = new SearhBoardRecyclerAdapter(mContext, resultBoards);
//            mActivityRecycler.setLayoutManager(activityLinearLayoutManager);
//            mActivityRecycler.setAdapter(mBoardRecyclerAdapter);
//            mBoardRecyclerAdapter.setOnEventClickListener(new SearhBoardRecyclerAdapter.OnEventClickListener() {
//                @Override
//                public void onItemClick(SearchResultBoard data) {
//                    searchActivity = data;
//                    mProjectSkuAdapter = null;
//                    mDataPage = 1;
//                    lodHotIssueData();
//                }
//            });
//        }
//    }

    /**
     * 获取活动数据
     */
//    private void lodBoardData() {
//        getScreenBoard = new BaseNetWorkCallBackApi(FinalConstant1.BOARD, "getScreenBoard");
//        if (mBoardRecyclerAdapter == null) {
//            getScreenBoard.addData("flag", "20");
//            getScreenBoard.startCallBack(new BaseCallBackListener<ServerData>() {
//                @Override
//                public void onSuccess(ServerData data) {
//                    ProjectDetailBoard projectDetailBoard = JSONUtil.TransformSingleBean(data.data, ProjectDetailBoard.class);
//                    ArrayList<SearchResultBoard> resultBoards = projectDetailBoard.getScreen_board();
//                    if (resultBoards.size() > 0) {
//                        setHeadView(resultBoards);
//                    }
//                }
//            });
//        }
//    }
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_back:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                onBack();
                break;
            case R.id.ll_back_contract:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                onBack();
                break;
            case R.id.project_part_pop_rly1:
                if (projectPop.isShowing()) {
                    projectPop.dismiss();
                } else {
                    projectPop.showPop();
                }
                initpop();
                break;
            case R.id.project_diqu_pop_rly:
                if (cityPop.isShowing()) {
                    cityPop.dismiss();
                } else {
                    cityPop.showPop();
                }
                initpop();
                break;
            case R.id.project_sort_pop_rly:
                if (sortPop.isShowing()) {
                    sortPop.dismiss();
                } else {
                    sortPop.showPop();
                }
                initpop();
                break;
            case R.id.project_kind_pop_rly:
                if (kindPop.isShowing()) {
                    kindPop.dismiss();
                } else {
                    kindPop.showPop();
                }
                initpop();
                break;
            default:
                break;
        }
    }

    void initpop() {
        if (projectPop != null && projectPop.isShowing()) {
            project_part_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_part_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_part_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_part_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
        if (sortPop != null && sortPop.isShowing()) {
            project_sort_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_sort_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_sort_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_sort_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
        if (kindPop != null && kindPop.isShowing()) {
            project_kind_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_kind_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_kind_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_kind_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
        if (cityPop != null && cityPop.isShowing()) {
            project_diqu_pop_tv.setTextColor(Color.parseColor("#FF527F"));
            project_diqu_pop_iv.setBackgroundResource(R.drawable.open_pink_project);
        } else {
            project_diqu_pop_tv.setTextColor(Color.parseColor("#333333"));
            project_diqu_pop_iv.setBackgroundResource(R.drawable.open_gray_project);
        }
    }


    private void lodHotIssueData() {
        loadSearchTaoDataApi = new LoadSearchTaoDataApi();
        loadSearchTaoDataApi.getHashMap().clear();

        //把数组中的数据拼接成字符串
        loadSearchTaoDataApi.addData("partId", partId);
        loadSearchTaoDataApi.addData("sort", sortStr);
        //上传时间
        if (!TextUtils.isEmpty(last_time)) {
            loadSearchTaoDataApi.addData("last_request_time", last_time);
        }
        for (ProjcetList data : kindStr) {
            loadSearchTaoDataApi.addData(data.getPostName(), data.getPostVal());
        }

//        if (searchActivity != null) {
//            loadSearchTaoDataApi.addData(searchActivity.getPostName(), searchActivity.getPostVal());
//        }

        loadSearchTaoDataApi.addData("page", mDataPage + "");

        loadSearchTaoDataApi.getCallBack(mContext, loadSearchTaoDataApi.getHashMap(), new BaseCallBackListener<SearchTaoDate>() {
            @Override
            public void onSuccess(SearchTaoDate data) {
                if (data != null) {
                    mDataPage++;
                    List<SearchTaoDate.TaoListBean> tao_list = data.getTao_list();
                    last_time = data.getLast_request_time() + "";
                    refresh.finishRefresh();
                    if (tao_list.size() == 0) {
                        refresh.finishLoadMoreWithNoMoreData();
                    } else {
                        refresh.finishLoadMore();
                    }

                    if (searchProjectAdapter == null) {
                        if (data.getTao_list().size() != 0) {
                            setRecyclerData(tao_list);
                        }
                    } else {
                        searchProjectAdapter.addList(tao_list);
                    }
                }
            }
        });
    }

    /**
     * 设置列表数据
     */
    private void setRecyclerData(final List<SearchTaoDate.TaoListBean> tao_list) {
        searchProjectAdapter = new SearchProjectAdapter(mContext, tao_list);
        linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rv_project.setLayoutManager(linearLayoutManager);
        rv_project.setAdapter(searchProjectAdapter);
//        searchProjectAdapter.addList(tao_list);


        searchProjectAdapter.setOnItemClickListener(new SearchProjectAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (TextUtils.isEmpty(from)) {
//                    if (selectDatas != null && selectDatas.contains(searchProjectAdapter.getData().get(pos).get_id().trim())) {
//                        MyToast.makeTextToast2(mContext, "不能重复添加", 1000).show();
//                        return;
//                    }
//                    if(selectList.size() > 5-selectDatas.size()-1){
//                        MyToast.makeTextToast2(mContext, "最多添加5个", 1000).show();
//                        return;
//                    }
                    addContrastPK(tao_list, pos);
                } else {
                    //发起投票--SKU添加点击
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("tao_id", searchProjectAdapter.getData().get(pos).get_id());
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.VOTE_TAO_ADD, pos + 1 + "", "4"), hashMap, new ActivityTypeData("170"));
                    if (VoteIdList != null && VoteIdList.contains(searchProjectAdapter.getData().get(pos).get_id().trim())) {
                        MyToast.makeTextToast2(mContext, "不能重复添加", 1000).show();
                        return;
                    }
                    if (VoteIdList != null && VoteIdList.size() < 5) {
                        if (!VoteIdList.contains(searchProjectAdapter.getData().get(pos).get_id().trim())) {
                            VoteIdList.add(searchProjectAdapter.getData().get(pos).get_id().trim());
                            selectList2.add(searchProjectAdapter.getData().get(pos));
                        }
                        searchProjectAdapter.removeItem(pos);
                    } else {
                        MyToast.makeTextToast2(mContext, "最多添加5个", 1000).show();
                    }
                }
            }
        });

    }


    //添加项目对比
    private void addContrastPK(final List<SearchTaoDate.TaoListBean> tao_list, final int pos) {
        HashMap maps = new HashMap();
        maps.put("tao_id", tao_list.get(pos).get_id());
        maps.put("tao_source", "5");
        addTaoPKApi = new AddTaoPKApi();
        addTaoPKApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null && "1".equals(serverData.code)) {
                    tao_id = tao_list.get(pos).getId();
//                    Log.i("302", "选中的" + tao_id);
                    searchProjectAdapter.removeItem(pos);
                    MyToast.makeTextToast2(mContext, serverData.message, 1000).show();
                    selectList.add(tao_id);
                    setTickData(tao_id);
                } else {
                    MyToast.makeTextToast2(mContext, serverData.message, 1000).show();
                }
            }
        });
    }

    private void setTickData(String tao_id) {
        TickData tickData = new TickData();
        tickData.setTao_id(tao_id);
        tickData.setTick_time(String.format("%010d", (System.currentTimeMillis() / 1000)));
        list.add(tickData);
        tickDataInfo.setData(list);
    }

    /**
     * @param context
     */
    public static void invoke(Context context) {
        Intent intent = new Intent(context, SearchProjectActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            mHandler.sendEmptyMessageDelayed(SHOW_PROJECT_POP,500);
        }
    }

    @Override
    public void onBackPressed() {
        if (Utils.isFastDoubleClick()) {
            return;
        }
        onBack();
    }

    private void onBack() {
        if (TextUtils.isEmpty(from)) {
            //反转
            if (selectList != null && selectList.size() > 0) {
                Collections.reverse(selectList);
                Collections.reverse(tickDataInfo.getData());
            }
            Intent intent = new Intent();
            intent.putExtra("sku_id", StringUtils.strip(selectList.toString(), "[]").trim());
            intent.putExtra("tao_source", "5");
            intent.putExtra("tick_data", tickDataInfo);
            mContext.setResult(10020, intent);
            selectList.clear();
            finish();
        } else {
            Intent i = new Intent();
            i.putStringArrayListExtra("id", VoteIdList);
            i.putExtra("taoData", selectList2);
            mContext.setResult(10010, i);
            finish();
        }
    }

    private void initLocation() {
        try {
            locationClient = new LocationClient(getApplicationContext());
            // 设置监听函数（定位结果）
            locationClient.registerLocationListener(new MyBDLocationListener());
            LocationClientOption option = new LocationClientOption();
            option.setAddrType("all");// 返回的定位结果包含地址信息
            option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,此处和百度地图相结合
            locationClient.setLocOption(option);
            locationClient.start();
            // 请求定位

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定位结果的响应函数
     *
     * @author
     */
    public class MyBDLocationListener implements BDLocationListener {
        @Override
        public void onReceiveLocation(BDLocation bdLocation) {
            /**
             * 此处需联网
             */
            // 设置editTextLocation为返回的百度地图获取到的街道信息
            try {
                // 自定义的位置类保存街道信息和经纬度（地图中使用）
                Location location = new Location();
                location.setAddress(bdLocation.getAddrStr());

                GeoPoint geoPoint = new GeoPoint((int) (bdLocation.getLatitude() * 1E6), (int) (bdLocation.getLongitude() * 1E6));
                location.setGeoPoint(geoPoint);

                String latitude = bdLocation.getLatitude() + "";
                String longitude = bdLocation.getLongitude() + "";
                //判断是否定位失败
                if (!"4.9E-324".equals(latitude) && !"4.9E-324".equals(longitude)) {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, latitude);
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, longitude);
                } else {
                    Cfg.saveStr(mContext, FinalConstant.DW_LATITUDE, "0");
                    Cfg.saveStr(mContext, FinalConstant.DW_LONGITUDE, "0");
                    MyToast.makeTextToast2(mContext, "定位失败，请查看手机是否开启了定位权限", MyToast.SHOW_TIME).show();
                }

                intelligentSorting(sortPos);
                String ss = bdLocation.getCity();

                if (ss != null && ss.length() > 1) {

                    if (ss.contains("省")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("市")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 1);
                    }
                    if (ss.contains("自治区")) {
                        provinceLocation = bdLocation.getCity().substring(0, bdLocation.getCity().length() - 3);
                    }

                    Cfg.saveStr(SearchProjectActivity.this, "city_dingwei", provinceLocation);

                    Cfg.saveStr(SearchProjectActivity.this, FinalConstant.TAOCITY, provinceLocation);

                    Cfg.saveStr(SearchProjectActivity.this, FinalConstant.DWCITY, provinceLocation);

                    locationClient.unRegisterLocationListener(new MyBDLocationListener());

                } else {
                    provinceLocation = "失败";
                    Cfg.saveStr(SearchProjectActivity.this, "city_dingwei", "全国");

                    Cfg.saveStr(SearchProjectActivity.this, FinalConstant.TAOCITY, "全国");

                    Cfg.saveStr(SearchProjectActivity.this, FinalConstant.DWCITY, "全国");

                    locationClient.unRegisterLocationListener(new MyBDLocationListener());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * dialog提示
     */
    void showDialogExitEdit3() {
        final EditExitDialog editDialog = new EditExitDialog(mContext, R.style.mystyle, R.layout.dilog_newuser_yizhuce1);
        editDialog.setCanceledOnTouchOutside(false);
        Log.e(TAG, "editDialog == " + editDialog);
        editDialog.show();

        TextView titleTv77 = editDialog.findViewById(R.id.dialog_exit_title_tv);
        titleTv77.setVisibility(View.VISIBLE);
        titleTv77.setText("您未打开定位权限");

        TextView titleTv88 = editDialog.findViewById(R.id.dialog_exit_content_tv);
        titleTv88.setText("请允许悦美获取您当前位置，以帮您查询附近医院");
        titleTv88.setHeight(Utils.dip2px(mContext, 35));

        LinearLayout llFengexian = editDialog.findViewById(R.id.ll_fengexian);
        llFengexian.setVisibility(View.VISIBLE);


        //确定
        Button cancelBt88 = editDialog.findViewById(R.id.confirm_btn1_edit);
        cancelBt88.setText("去设置");
        cancelBt88.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // 转到手机设置界面，用户设置GPS
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, GPS_REQUEST_CODE); // 设置完成后返回到原来的界面
                editDialog.dismiss();
            }
        });

        //取消
        Button cancelBt99 = editDialog.findViewById(R.id.cancel_btn1_edit);
        cancelBt99.setVisibility(View.VISIBLE);
        cancelBt99.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                editDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GPS_REQUEST_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                Acp.getInstance(SearchProjectActivity.this).request(new AcpOptions.Builder().setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION).build(), new AcpListener() {
                    @Override
                    public void onGranted() {
                        initLocation();
                    }

                    @Override
                    public void onDenied(List<String> permissions) {
                        sortPop.dismiss();
                    }
                });
            } else {
                initLocation();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        if (projectPop != null) {
            projectPop.dismiss();
        }
        if (cityPop != null) {
            cityPop.dismiss();
        }
        if (sortPop != null) {
            sortPop.dismiss();
        }
        if (kindPop != null) {
            kindPop.dismiss();
        }
    }
}
