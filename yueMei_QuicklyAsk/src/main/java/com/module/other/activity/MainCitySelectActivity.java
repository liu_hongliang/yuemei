/**
 * 
 */
package com.module.other.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.base.api.BaseCallBackListener;
import com.module.commonview.adapter.TaoPopAdapter;
import com.module.doctor.model.api.LoadCityApi;
import com.module.home.controller.activity.HomeActivity;
import com.module.home.controller.activity.HomeActivity623;
import com.module.other.module.api.InitHotCity;
import com.module.other.module.bean.TaoPopItemData;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.util.Cfg;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;

import org.kymjs.aframe.ui.BindView;
import org.kymjs.aframe.ui.activity.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 标题栏城市选择
 * 
 * @author Rubin
 * 
 */
public class MainCitySelectActivity extends BaseActivity {

	@BindView(id = R.id.pop_list_tao_project_list)
	private ListView prlist;
	@BindView(id = R.id.write_que_back)
	private RelativeLayout back;
	@BindView(id = R.id.main_city_title)
	private TextView title;

	private String cityStr;

	private List<TaoPopItemData> lvPart6Data = new ArrayList<TaoPopItemData>();
	private List<TaoPopItemData> lvPart2Data = new ArrayList<TaoPopItemData>();
	private List<TaoPopItemData> lvPart3Data = new ArrayList<TaoPopItemData>();// 热门城市
	private List<TaoPopItemData> lvPart4Data = new ArrayList<TaoPopItemData>();
	private List<TaoPopItemData> lvPart5Data = new ArrayList<TaoPopItemData>();

	private TaoPopAdapter mPart2Adapter;
	private Context mContext;
	private String dwCity;
	private String dwCityId;

	private String type;

	private String partId;
	private String partName;

	@Override
	public void setRootView() {
		setContentView(R.layout.acty_main_city_select);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = MainCitySelectActivity.this;
		Intent it = getIntent();
		cityStr = it.getStringExtra("curcity");
		type = it.getStringExtra("type");

		if (type.equals("3")) {
			partId = it.getStringExtra("partId");
			partName = it.getStringExtra("partName");
		}

		title.setText("当前所选-" + cityStr);

		dwCity = Cfg.loadStr(mContext, "city_dw", "");
		dwCityId = Cfg.loadStr(mContext, "city_id", "");
		initHotCity();

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	void initHotCity() {
		TaoPopItemData a1 = new TaoPopItemData();
		a1.set_id("a");
		a1.setName("自动定位");
		TaoPopItemData a2 = new TaoPopItemData();
		if (dwCity.length() > 0) {
			a2.set_id(dwCityId);
			a2.setName(dwCity);
		} else {
			a2.set_id("b");
			a2.setName("获取位置失败");
		}
		TaoPopItemData b1 = new TaoPopItemData();
		b1.set_id("a");
		b1.setName("全国");
		TaoPopItemData b2 = new TaoPopItemData();
		b2.set_id("0");
		b2.setName("全国");

		lvPart4Data.add(a1);
		lvPart4Data.add(a2);
		lvPart4Data.add(b1);
		lvPart4Data.add(b2);
		new InitHotCity().getCallBack(mContext, new HashMap<String, Object>(), new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				TaoPopItemData a3 = JSONUtil.TransformTaoPop(serverData.data);

				a3.set_id("a");
				a3.setName("热门省市");
				lvPart3Data.add(a3);
				lvPart5Data.add(a3);
				lvPart5Data.remove(0);
				lvPart3Data.addAll(lvPart5Data);

				loadCityData();
			}

		});
	}

	void loadCityData() {
		Map<String,Object> maps=new HashMap<>();
		maps.put("flag","1");
		new LoadCityApi().getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
			@Override
			public void onSuccess(ServerData serverData) {
				TaoPopItemData a4 = JSONUtil.TransformTaoPop(serverData.data);

				a4.set_id("a");
				a4.setName("全部");
				lvPart2Data.add(a4);
				lvPart2Data.addAll(lvPart6Data);

				lvPart3Data.addAll(lvPart2Data);
				lvPart4Data.addAll(lvPart3Data);

				mPart2Adapter = new TaoPopAdapter(
						MainCitySelectActivity.this, lvPart4Data, -1);
				prlist.setAdapter(mPart2Adapter);

				prlist.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0,
											View arg1, int pos, long arg3) {
						String id = lvPart4Data.get(pos).get_id();
						String cityName = lvPart4Data.get(pos)
								.getName();
						if (id.equals("a")) {

						} else {
							Cfg.saveStr(mContext, FinalConstant.DWCITY, cityName);
							Utils.getCityOneToHttp(mContext,"1");

							if (type.equals("1")) {
								// Intent it1 = new Intent();
								// it1.putExtra("city", cityName);
								// it1.setClass(mContext,
								// TabHomeTaoListActivity.class);
								// setResult(4, it1);
								// finish();
							} else if (type.equals("2")) {
								Intent it1 = new Intent();
								it1.putExtra("city", cityName);
								it1.setClass(mContext,
										HomeActivity.class);
								setResult(4, it1);
								finish();
							}
						}
					}
				});
			}

		});
	}
}
