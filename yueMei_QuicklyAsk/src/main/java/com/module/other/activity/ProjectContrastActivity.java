package com.module.other.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.module.api.AddTaoPKApi;
import com.module.api.DeleteTaoPKApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.view.YMBaseActivity;
import com.module.community.web.WebData;
import com.module.community.web.WebUtil;
import com.module.event.MsgEvent;
import com.module.other.fragment.ProjectContrastFragment;
import com.module.other.module.bean.TickDataInfo;
import com.module.other.netWork.netWork.ServerData;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.quicklyack.constant.FinalConstant;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoPKBean;
import com.quicklyask.util.Utils;
import com.quicklyask.view.YueMeiDialog;
import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import butterknife.BindView;


/**
 * 文 件 名: ProjectContrastActivity
 * 创 建 人: 原成昊
 * 创建日期: 2019-11-18 19:44
 * 邮   箱: 188897876@qq.com
 * 修改备注：项目对比页面
 */

public class ProjectContrastActivity extends YMBaseActivity {
    @BindView(R.id.rl)//标题栏
            RelativeLayout rl;
    @BindView(R.id.iv_back)//返回
            ImageView iv_back;
    @BindView(R.id.rl_edit)//编辑
            RelativeLayout rl_edit;
    @BindView(R.id.tv_edit)
    TextView tv_edit;
    @BindView(R.id.tl)
    CommonTabLayout tl;
    @BindView(R.id.vp)
    ViewPager vp;

    @BindView(R.id.ll_add_contrast_bottom)//添加和对比布局
            LinearLayout ll_add_contrast_bottom;
    @BindView(R.id.ll_add)//底部添加按钮
            LinearLayout ll_add;
    @BindView(R.id.ll_contrast)//底部对比按钮
            LinearLayout ll_contrast;
    @BindView(R.id.tv_select_num)
    TextView tv_select_num;//选择数

    @BindView(R.id.ll_delete_contrast_bottom)
    LinearLayout ll_delete_contrast_bottom;//点击编辑后删除布局
    @BindView(R.id.tv_delete)//删除按钮
            TextView tv_delete;

    //其他三个tab底部布局
    @BindView(R.id.ll_add_other_bottom)
    LinearLayout ll_add_other_bottom;
    @BindView(R.id.tv_add)
    TextView tv_add;//添加按钮
    @BindView(R.id.tv_add_other_number)
    TextView tv_add_other_number;//后三个按钮选择数


    private ArrayList<Fragment> mFragments;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private ProjectContrastPagerAdapter mAdapter;
    private DeleteTaoPKApi deleteTaoPKApi;
    private String[] mTitles = {"我的对比", "浏览历史", "我的购物车", "我的收藏"};
    private String tao_id;
    private String tao_source;
    private String curTag = "0";
    private String index;
    private AddTaoPKApi addTaoPKApi;
    //切换选项卡
    public static final int TOGGLE_TAB = 0x1010;
    //刷新
    public static final int REFRESH = 0x1011;
    //刷新项目对比
    public static final int REFRESHCONTRAST = 0x0111;
    //编辑模式
    public static final int EDIT = 0x1111;
    //非编辑模式
    public static final int NOTEDIT = 0x1110;
    //获得后三个选中的集合
    public static final int GET_SELECT_DATA = 10000;
    //获得选中的我的对比集合
    public static final int GET_SELETE_CONTRAST_DATA = 10001;
    //获得选中的删除的集合
    public static final int GET_SELETE_DELETE_DATA = 10002;
    //清空选择集合
    public static final int CLEAR_SELECT = 10003;
    //清空删除集合
    public static final int CLEAR_DELETE = 10004;
    //对比详情页取消通知
    public static final int DELETE_CUR = 10005;
    //获得项目对比里所有数据
    public static final int GET_CONTRAST_DATA = 10006;

    public static final int DELETE_UPDATA_CONTRASTlIST = 10007;
    //对比页面是否有数据
    public static final int MY_CONTRAST_DATA = 10008;
    //TAB顺序
    public static final int TAB_LIST = 10009;

    public static final int TICK_DATA = 10011;
    //后三个选中
    private List<String> selectDatas = new ArrayList<>();
    //项目对比选择
    private List<String> selectDatas_contrast = new ArrayList<>();
    //删除选择
    private List<String> selectDatas_delete = new ArrayList<>();
    //提示框
    private YueMeiDialog yueMeiDialog;
    //删除回调类
    private DeleteDialogClickListener deleteDialogClickListener;
    private String DeleteFlag = "0";
    private List<TaoPKBean.TaoListBean> taoPK_list;
    private List<TaoPKBean.TaoListBean> taoPK_list2;
    public static List<String> tabList = new ArrayList<>();
    public static boolean isFirstInitTab;
    private TickDataInfo tickDataInfo;
    private Handler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<ProjectContrastActivity> mActivity;

        private MyHandler(ProjectContrastActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ProjectContrastActivity theActivity = mActivity.get();
            if (theActivity != null) {
                switch (msg.what) {
                    //切换选项卡到我的对比
                    case TOGGLE_TAB:
                        theActivity.vp.setCurrentItem(0);
                        theActivity.tl.setCurrentTab(0);
                        EventBus.getDefault().post(new MsgEvent(REFRESH));
                        break;
                    default:
                        break;

                }
            }

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        mHandler.removeCallbacksAndMessages(null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(MsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case GET_SELECT_DATA:
                selectDatas = (List<String>) msgEvent.getData();
                upUIOtherSeleteButton();
                break;
            case GET_SELETE_CONTRAST_DATA:
                //我的项目
                selectDatas_contrast = (List<String>) msgEvent.getData();
                upUIContrastButton();
                break;
            case GET_SELETE_DELETE_DATA:
                //删除
                selectDatas_delete = (List<String>) msgEvent.getData();
                upUiDeleteButton();
                break;
            case GET_CONTRAST_DATA:
                //返回适配器数据
                taoPK_list = (List<TaoPKBean.TaoListBean>) msgEvent.getData();
                break;
            case NOTEDIT:
                tv_edit.setText("编辑");
                ll_add_contrast_bottom.setVisibility(View.VISIBLE);
                ll_delete_contrast_bottom.setVisibility(View.GONE);
                break;
            case MY_CONTRAST_DATA:
                taoPK_list2 = (List<TaoPKBean.TaoListBean>) msgEvent.getData();
                break;
            case TAB_LIST:
                List<String> list = (List<String>) msgEvent.getData();
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals("1")) {
                        vp.setCurrentItem(i, false);
                        tl.setCurrentTab(i);
                        return;
                    }
                }
                break;
        }
    }

    private void upUIContrastButton() {
        if (selectDatas_contrast != null && selectDatas_contrast.size() > 0) {
            tv_select_num.setText("已选" + selectDatas_contrast.size() + "个项目");
            if (selectDatas_contrast.size() >= 2 && selectDatas_contrast.size() <= 5) {
                ll_contrast.setBackgroundResource(R.drawable.shape_gradient2_ff6fb7_ffbeae);
            } else {
                ll_contrast.setBackgroundColor(Color.parseColor("#CCCCCC"));
            }
            tv_select_num.setVisibility(View.VISIBLE);
        } else {
            ll_contrast.setBackgroundColor(Color.parseColor("#CCCCCC"));
            tv_select_num.setVisibility(View.GONE);
        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        EventBus.getDefault().post(new MsgEvent(REFRESH));
////        upUIOtherSeleteButton();
////        upUiDeleteButton();
////        upUIContrastButton();
//    }

    private void upUIOtherSeleteButton() {
        if (selectDatas != null && selectDatas.size() > 0) {
            tv_add_other_number.setVisibility(View.VISIBLE);
            tv_add_other_number.setText("已选" + selectDatas.size() + "个项目");
            ll_add_other_bottom.setBackgroundResource(R.drawable.shape_gradient2_ff6fb7_ffbeae);
        } else {
            tv_add_other_number.setVisibility(View.GONE);
            ll_add_other_bottom.setBackgroundColor(Color.parseColor("#CCCCCC"));
        }
    }

    private void upUiDeleteButton() {
        if (selectDatas_delete != null && !selectDatas_delete.isEmpty() && selectDatas_delete.size() > 0) {
            tv_delete.setTextColor(Color.parseColor("#FF527F"));
            tv_delete.setBackgroundResource(R.drawable.shape_button_ff527f);
        } else {
            tv_delete.setTextColor(Color.parseColor("#BBBBBB"));
            tv_delete.setBackgroundResource(R.drawable.shape_button_cccccc);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_project_contrast;
    }

    @Override
    protected void initView() {
        int statusbarHeight = QMUIStatusBarHelper.getStatusbarHeight(mContext);
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rl.getLayoutParams();
        layoutParams.topMargin = statusbarHeight;

        yueMeiDialog = new YueMeiDialog(mContext, "好东西很抢手，确定要删除？", "再想想", "删除");
        deleteDialogClickListener = new DeleteDialogClickListener();
        yueMeiDialog.setBtnClickListener(deleteDialogClickListener);


        tao_id = getIntent().getStringExtra("sku_id");
        tao_source = getIntent().getStringExtra("tao_source");
        index = getIntent().getStringExtra("index");
        isFirstInitTab = true;

        setMultiOnClickListener(iv_back, rl_edit, tv_delete, ll_add, ll_contrast, ll_add_other_bottom);

        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], 0, 0));
        }

        mFragments = new ArrayList<>();
        for (int i = 0; i < mTitles.length; i++) {
            mFragments.add(ProjectContrastFragment.newInstance(i + "", tao_id, tao_source, index));
        }

        mAdapter = new ProjectContrastPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(mAdapter);
        tl.setTabData(mTabEntities);
        tl.setIndicatorWidth(23);
        tl.setIndicatorCornerRadius((float) 1.25);
        vp.setOffscreenPageLimit(4);
        if (!TextUtils.isEmpty(index)) {
            vp.setCurrentItem(Integer.parseInt(index), false);
            tl.setCurrentTab(Integer.parseInt(index));
        } else {
            vp.setCurrentItem(0, false);
            tl.setCurrentTab(0);
        }

        tabList.clear();
        for (int i = 0; i < 4; i++) {
            tabList.add("3");
        }

        tl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                EventBus.getDefault().post(new MsgEvent(TOGGLE_TAB));
                curTag = position + 1 + "";
                vp.setCurrentItem(position);
                if (position == 0) {
                    ll_add_other_bottom.setVisibility(View.GONE);
                    if (tv_edit.getText().equals("编辑")) {
                        ll_add_contrast_bottom.setVisibility(View.VISIBLE);
                        upUIContrastButton();
                        ll_delete_contrast_bottom.setVisibility(View.GONE);
                    } else {
                        ll_add_contrast_bottom.setVisibility(View.GONE);
                        upUiDeleteButton();
                        ll_delete_contrast_bottom.setVisibility(View.VISIBLE);
                    }
                    rl_edit.setVisibility(View.VISIBLE);
                } else {
                    if (DeleteFlag.equals("1")) {
                        selectDatas_delete.clear();
                        //取消删除模式
                        tv_edit.setText("编辑");
                        EventBus.getDefault().post(new MsgEvent(NOTEDIT));
                    }
                    rl_edit.setVisibility(View.INVISIBLE);
                    ll_add_other_bottom.setVisibility(View.VISIBLE);
                    upUIOtherSeleteButton();
                    ll_delete_contrast_bottom.setVisibility(View.GONE);
                    ll_add_contrast_bottom.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                EventBus.getDefault().post(new MsgEvent(TOGGLE_TAB));
                curTag = position + 1 + "";
                tl.setCurrentTab(position);
                if (position == 0) {
                    ll_add_other_bottom.setVisibility(View.GONE);
                    if (tv_edit.getText().equals("编辑")) {
                        upUIContrastButton();
                        ll_add_contrast_bottom.setVisibility(View.VISIBLE);
                        ll_delete_contrast_bottom.setVisibility(View.GONE);
                    } else {
                        ll_add_contrast_bottom.setVisibility(View.GONE);
                        upUiDeleteButton();
                        ll_delete_contrast_bottom.setVisibility(View.VISIBLE);
                    }
                    rl_edit.setVisibility(View.VISIBLE);
                } else {
                    if (DeleteFlag.equals("1")) {
                        //取消删除模式
                        tv_edit.setText("编辑");
                        selectDatas_delete.clear();
                        EventBus.getDefault().post(new MsgEvent(NOTEDIT));
                    }
                    rl_edit.setVisibility(View.INVISIBLE);
                    ll_add_other_bottom.setVisibility(View.VISIBLE);
                    upUIOtherSeleteButton();
                    ll_delete_contrast_bottom.setVisibility(View.GONE);
                    ll_add_contrast_bottom.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        tao_id = intent.getStringExtra("sku_id");
        tao_source = intent.getStringExtra("tao_source");
        index = intent.getStringExtra("index");
        ArrayList<String> list = new ArrayList<>();
        list.add(tao_id);
        list.add(tao_source);
        EventBus.getDefault().post(new MsgEvent(REFRESHCONTRAST, list));
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_edit:
                if (taoPK_list2 != null && !taoPK_list2.isEmpty() && taoPK_list2.size() > 0) {
                    if (tv_edit.getText().toString().trim().equals("编辑")) {
                        DeleteFlag = "1";
                        tv_edit.setText("取消");
                        ll_add_contrast_bottom.setVisibility(View.GONE);
                        upUiDeleteButton();
                        ll_delete_contrast_bottom.setVisibility(View.VISIBLE);
                        EventBus.getDefault().post(new MsgEvent(EDIT));
                    } else {
                        DeleteFlag = "0";
                        tv_edit.setText("编辑");
                        ll_add_contrast_bottom.setVisibility(View.VISIBLE);
                        ll_delete_contrast_bottom.setVisibility(View.GONE);
                        EventBus.getDefault().post(new MsgEvent(NOTEDIT));
                    }
                }
                break;
            case R.id.tv_delete:
                if (selectDatas_delete != null && !selectDatas_delete.isEmpty() && selectDatas_delete.size() > 0) {
                    yueMeiDialog.show();
                }
//                else {
//                    Toast.makeText(mContext, "还没有选择哦", Toast.LENGTH_SHORT).show();
//                }
                break;
            case R.id.ll_add:
                Intent intent = new Intent(mContext, SearchProjectActivity.class);
//                intent.putStringArrayListExtra("taoIdList", (ArrayList<String>) selectDatas_contrast);
                startActivityForResult(intent, 0);
                break;
            case R.id.ll_contrast:
                if (selectDatas_contrast != null && selectDatas_contrast.size() <= 1) {
                    Toast.makeText(mContext, "请添加", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selectDatas_contrast != null && !selectDatas_contrast.isEmpty() && selectDatas_contrast.size() > 5) {
                    Toast.makeText(mContext, "不能超过5个", Toast.LENGTH_SHORT).show();
                    return;
                }
                //跳H5
                WebData webData = new WebData();
                webData.setUrl(FinalConstant.TAOPK + "tao_id" + "/" + StringUtils.strip(selectDatas_contrast.toString(), "[]").trim());
                webData.setTitle("项目对比");
                webData.setShowRefresh(false);
                WebUtil.getInstance().startWebActivity(mContext, webData);
                break;
            case R.id.ll_add_other_bottom:
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (selectDatas != null && !selectDatas.isEmpty() && selectDatas.size() > 0) {
                    addContrastPK(StringUtils.strip(selectDatas.toString(), "[]").trim());
                } else {
                    Toast.makeText(mContext, "还没有选择哦", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    //添加项目对比
    private void addContrastPK(String tao_id) {
        HashMap maps = new HashMap();
        maps.put("tao_id", tao_id);
        maps.put("tao_source", curTag);
        addTaoPKApi = new AddTaoPKApi();
        addTaoPKApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null && "1".equals(serverData.code)) {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    mHandler.sendEmptyMessage(TOGGLE_TAB);
                    selectDatas.clear();
                    EventBus.getDefault().post(new MsgEvent(CLEAR_SELECT));
                } else {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //删除项目对比接口
    private void deleteProjectContrast(final String pk_id) {
        deleteTaoPKApi = new DeleteTaoPKApi();
        HashMap<String, Object> maps = new HashMap<String, Object>();
        maps.put("pk_id", pk_id);
        deleteTaoPKApi.getCallBack(mContext, maps, new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null && serverData.code.equals("1")) {
                    //删除成功
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < taoPK_list.size(); i++) {
                        if (selectDatas_delete.contains(taoPK_list.get(i).getPk_id() + "")) {
                            selectDatas_contrast.remove(taoPK_list.get(i).getId() + "");
                        }
                    }
                    tv_delete.setTextColor(Color.parseColor("#BBBBBB"));
                    tv_delete.setBackgroundResource(R.drawable.shape_button_cccccc);
                    EventBus.getDefault().post(new MsgEvent(DELETE_UPDATA_CONTRASTlIST, selectDatas_contrast));
                    EventBus.getDefault().post(new MsgEvent(CLEAR_DELETE));
                } else {
                    //删除失败
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * @param context
     */
    public static void invoke(Context context, String sku_id, String tao_source) {
        Intent intent = new Intent(context, ProjectContrastActivity.class);
        //添加淘整形（来源为淘整形详情、购物车页携带的选中sku_id）
        intent.putExtra("sku_id", sku_id);
        //淘整形来源 1:淘整形详情页, 2:浏览历史页, 3:购物车页, 4:我的收藏页, 5:对比项目搜索页
        intent.putExtra("tao_source", tao_source);
        context.startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            switch (resultCode) {
//                case 1001:
//                    tao_id = data.getStringExtra("sku_id");
//                    tao_source = data.getStringExtra("tao_source");
//                    EventBus.getDefault().post(new MsgEvent(REFRESH));
//                    break;
                case 10020:
                    tao_id = data.getStringExtra("sku_id");
                    tao_source = data.getStringExtra("tao_source");
                    tickDataInfo = data.getParcelableExtra("tick_data");
                    ArrayList<String> list = new ArrayList<>();
                    list.add(tao_id);
                    list.add(tao_source);
                    EventBus.getDefault().post(new MsgEvent(REFRESHCONTRAST, list));
                    EventBus.getDefault().post(new MsgEvent(TICK_DATA,tickDataInfo));
//                    EventBus.getDefault().post(new MsgEvent(REFRESH));
                    break;
                default:
                    break;
            }
        }
    }


    private class ProjectContrastPagerAdapter extends FragmentPagerAdapter {
        public ProjectContrastPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

    class TabEntity implements CustomTabEntity {
        public String title;
        public int selectedIcon;
        public int unSelectedIcon;

        public TabEntity(String title, int selectedIcon, int unSelectedIcon) {
            this.title = title;
            this.selectedIcon = selectedIcon;
            this.unSelectedIcon = unSelectedIcon;
        }

        @Override
        public String getTabTitle() {
            return title;
        }

        @Override
        public int getTabSelectedIcon() {
            return selectedIcon;
        }

        @Override
        public int getTabUnselectedIcon() {
            return unSelectedIcon;
        }
    }

    /**
     * 删除回调
     */
    private class DeleteDialogClickListener implements YueMeiDialog.BtnClickListener {

        @Override
        public void leftBtnClick() {
            //是否清空选中状态
//            selectDatas_delete.clear();
//            EventBus.getDefault().post(new MsgEvent(CLEAR_DELETE));
            yueMeiDialog.dismiss();
        }

        @Override
        public void rightBtnClick() {
            deleteProjectContrast(StringUtils.strip(selectDatas_delete.toString(), "[]").trim());
            yueMeiDialog.dismiss();
        }
    }
}
