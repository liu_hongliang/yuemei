package com.module.other.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.module.event.VoteMsgEvent;
import com.quicklyask.activity.R;
import com.quicklyask.util.Utils;
import com.quicklyask.view.MyToast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class InitiateVoteAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<String> mList;
    private onTextChangeListener mTextListener;
    private ArrayList<TextWatcher> mListeners;
    public HashMap<Integer, String> contents;
    private Handler handler = new Handler();

    /**
     * 延迟线程，看是否还有下一个字符输入
     */
    private Runnable delayRun = new Runnable() {
        @Override
        public void run() {
            Utils.hideSoftKeyboard((Activity) mContext);
        }
    };

    public InitiateVoteAdapter2(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
        inflater = LayoutInflater.from(mContext);
        mListeners = new ArrayList<>();
        contents = new LinkedHashMap<>();
        for (int i = 0; i < 5; i++) {
            contents.put(i, "");
        }
        if (mList.size() > 0) {
            for (int i = 0; i < mList.size(); i++) {
                contents.put(i, mList.get(i));
            }
        }
    }

    //设置自定义接口成员变量
    public void setOnTextChangeListener(onTextChangeListener onTextChangeListener) {
        this.mTextListener = onTextChangeListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int pos) {
        return new viewHolder3(inflater.inflate(R.layout.item3_initiate_vote_ordinary, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        setView3((viewHolder3) holder, i);
    }

    @Override
    public int getItemCount() {
        if (mList != null && mList.size() <= 2) {
            return 3;
        } else if (mList != null && mList.size() > 2 && mList.size() < 5) {
            return mList.size() + 1;
        } else {
            return mList.size();
        }
    }

    public List<String> getData() {
        for (int i = 0; i < mList.size(); i++) {
            if (TextUtils.isEmpty(mList.get(i))) {
                mList.remove(i);
                i--;
            }
        }
        return mList;
    }

    //删除单条数据
    public void removeItem(int position) {
        notifyItemRemoved(position);
        //局部刷新
        notifyItemRangeChanged(position, getItemCount());
    }

    //添加单条数据
    public void addItem(int position, String title) {
        mList.add(position, title);
        notifyItemInserted(position);
        //局部刷新
        notifyItemRangeChanged(position, mList.size() - position);
    }

    public void updata(List<String> mTao) {
        mList = mTao;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
//                为了解决 Cannot call this method while RecyclerView is computing a layout or scrolling
                notifyDataSetChanged();
            }
        });
    }


    class viewHolder3 extends RecyclerView.ViewHolder {
        //        ImageView iv_add;
        EditText ed;
        ImageView iv_delete;
        LinearLayout ll_root;

        public viewHolder3(View itemView) {
            super(itemView);
//            iv_add = itemView.findViewById(R.id.iv_add);
            ed = itemView.findViewById(R.id.ed);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            ll_root = itemView.findViewById(R.id.ll_root);
        }
    }

    /**
     * item删除监听器
     */
    public interface OnItemDeleteClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemDeleteClick(View view, int pos);
    }

    private OnItemDeleteClickListener onItemDeleteClickListener;

    public void setOnItemDeleteClickListener(OnItemDeleteClickListener onItemDeleteClickListener) {
        this.onItemDeleteClickListener = onItemDeleteClickListener;
    }

    private void setView3(final viewHolder3 holder, final int position) {
        holder.setIsRecyclable(false);
        if (mList != null && mList.size() == 0) {
            if (position == 0 || position == 1) {
                setStyle1(holder, position);
            } else {
                setStyle2(holder, position);
            }
        } else if (mList != null && mList.size() == 1) {
            if (position == 0) {
                setStyle3(holder, position);
            } else if (position == 1) {
                setStyle1(holder, position);
            } else {
                setStyle2(holder, position);
            }
        } else if (mList != null && mList.size() >= 2 && mList.size() < 5) {
            if (getItemCount() == position + 1) {
                setStyle2(holder, position);
            } else {
                setStyle3(holder, position);
            }
        } else {
            setStyle3(holder, position);
        }

//        holder.ed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                /*判断是否是“回车”键*/
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    if (!TextUtils.isEmpty(holder.ed.getText().toString()) && !mList.contains(holder.ed.getText().toString())) {
//                        if (mList.size() > 0) {
//                            if (mList.size() - 1 >= position && !TextUtils.isEmpty(mList.get(position))) {
//                                mList.set(position, holder.ed.getText().toString());
//                                updata(mList);
//                                holder.ed.clearFocus();
//                                EventBus.getDefault().post(new VoteMsgEvent(7));
//                            } else {
//                                mList.add(holder.ed.getText().toString());
//                                updata(mList);
//                                holder.ed.clearFocus();
//                                EventBus.getDefault().post(new VoteMsgEvent(7));
//                            }
//                        } else {
//                            mList.add(holder.ed.getText().toString());
//                            updata(mList);
//                            holder.ed.clearFocus();
////                        Utils.hideSoftKeyboard((Activity) mContext);
//                            EventBus.getDefault().post(new VoteMsgEvent(7));
//                        }
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });

        final TextWatcher textWatcher = new TextWatcher() {
            private CharSequence temp;
            private int selectionStart;
            private int selectionEnd;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                selectionStart = holder.ed.getSelectionStart();
                selectionEnd = holder.ed.getSelectionEnd();
                if (temp.length() > 0) {
                    holder.iv_delete.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_delete.setVisibility(View.INVISIBLE);
                }
                if (temp.length() > 8) {
                    s.delete(selectionStart - 1, selectionEnd);
                    int tempSelection = selectionStart;
                    holder.ed.setText(s);
                    //设置光标在最后
                    holder.ed.setSelection(tempSelection);
                    MyToast.makeTextToast2(mContext, "最多8个字", 1000).show();
                }
                contents.put(holder.getAdapterPosition(), s.toString());

                if (holder.ed.hasFocus()) {//判断当前EditText是否有焦点在
                    //通过接口回调将数据传递到Activity中
                    if (position >= 2) {
                        if (delayRun != null) {
                            //每次editText有变化的时候，则移除上次发出的延迟线程
                            handler.removeCallbacks(delayRun);
                        }
                        //延迟800ms，如果不再输入字符，则执行该线程的run方法
                        handler.postDelayed(delayRun, 800);
                    }
                    mTextListener.onTextChanged(holder.getAdapterPosition(), holder.ed.getText().toString(), holder.ed, contents);
                }
            }
        };

        holder.ed.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    holder.ed.addTextChangedListener(textWatcher);
                } else {
                    holder.ed.removeTextChangedListener(textWatcher);
                }
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (onItemDeleteClickListener != null) {
//                    onItemDeleteClickListener.onItemDeleteClick(v, position);
//                }
                holder.iv_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.iv_delete.setVisibility(View.GONE);
                        contents.remove(position);
                        holder.ed.requestFocus();
                        holder.ed.setText("");
                        EventBus.getDefault().post(new VoteMsgEvent(7));
//                        mList.remove(position);
//                        contents.put(holder.getAdapterPosition(),"");
//                        notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void setStyle3(viewHolder3 holder, final int position) {
        holder.ll_root.setBackgroundResource(R.drawable.shape_ff527f_solid_line);
        holder.iv_delete.setVisibility(View.VISIBLE);
//        holder.iv_add.setVisibility(View.GONE);
        if (TextUtils.isEmpty(mList.get(position))) {
            holder.ed.setText("");
        } else {
            holder.ed.setText(mList.get(position));
        }
        holder.ed.setHint("+ 添加选项（必填）");
    }

    private void setStyle2(viewHolder3 holder, int position) {
        holder.ed.setHint("+ 继续添加");
        holder.ll_root.setBackgroundResource(R.drawable.shape_ff527f_dotted_line);
        holder.iv_delete.setVisibility(View.INVISIBLE);
//        holder.iv_add.setVisibility(View.GONE);
    }

    private void setStyle1(viewHolder3 holder, int position) {
        holder.ed.setHint("+ 添加选项（必填）");
        holder.ll_root.setBackgroundResource(R.drawable.shape_ff527f_solid_line);
        holder.iv_delete.setVisibility(View.INVISIBLE);
//        holder.iv_add.setVisibility(View.GONE);
    }


    public interface onTextChangeListener {
        void onTextChanged(int pos, String str, EditText ed, HashMap<Integer, String> contents);
    }

    public void addTextChangedListener(TextWatcher watcher) {
        if (mListeners == null) {
            mListeners = new ArrayList<TextWatcher>();
        }
        mListeners.add(watcher);
    }
}