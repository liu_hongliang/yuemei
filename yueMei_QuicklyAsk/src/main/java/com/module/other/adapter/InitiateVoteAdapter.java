package com.module.other.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.module.base.view.FunctionManager;
import com.module.commonview.view.NiceImageView;
import com.module.other.module.bean.SearchTaoDate;
import com.module.shopping.model.bean.ShopCarTaoData;
import com.module.taodetail.model.bean.HomeTaoData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoPKBean;
import com.quicklyask.util.Utils;

import java.util.List;


public class InitiateVoteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<SearchTaoDate.TaoListBean> mTaoList;
    public final int ITEM1_TYPE_TAG = 1;
    public final int ITEM2_TYPE_TAG = 2;
    public final int ITEM3_TYPE_TAG = 3;
    private FunctionManager functionManager;

    public InitiateVoteAdapter(Context context, List<SearchTaoDate.TaoListBean> tao_list) {
        this.mContext = context;
        this.mTaoList = tao_list;
        inflater = LayoutInflater.from(mContext);
        functionManager = new FunctionManager(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int pos) {
        if (ITEM1_TYPE_TAG == pos) {
            return new viewHolder1(inflater.inflate(R.layout.item1_initiate_vote, parent, false));
        } else if (ITEM2_TYPE_TAG == pos) {
            return new viewHolder2(inflater.inflate(R.layout.item2_initiate_vote, parent, false));
        } else {
            return new viewHolder3(inflater.inflate(R.layout.item3_initiate_vote, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof viewHolder1) {
            setView1((viewHolder1) holder, i);
        } else if (holder instanceof viewHolder2) {
            setView2((viewHolder2) holder, i);
        } else if (holder instanceof viewHolder3) {
            setView3((viewHolder3) holder, i);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mTaoList != null && mTaoList.size() == 0) {
            if (position == 0 || position == 1) {
                return ITEM1_TYPE_TAG;
            } else {
                return ITEM2_TYPE_TAG;
            }
        } else if (mTaoList != null && mTaoList.size() == 1) {
            if (position == 0) {
                return ITEM3_TYPE_TAG;
            } else if (position == 1) {
                return ITEM1_TYPE_TAG;
            } else {
                return ITEM2_TYPE_TAG;
            }
        } else if (mTaoList != null && mTaoList.size() >= 2 && mTaoList.size() < 5) {
            if (getItemCount() == position + 1) {
                return ITEM2_TYPE_TAG;
            } else {
                return ITEM3_TYPE_TAG;
            }
        } else {
            return ITEM3_TYPE_TAG;
        }
    }

    @Override
    public int getItemCount() {
        if (mTaoList != null && mTaoList.size() <= 2) {
            return 3;
        } else if (mTaoList != null && mTaoList.size() > 2 && mTaoList.size() < 5) {
            return mTaoList.size() + 1;
        } else {
            return mTaoList.size();
        }
    }

    public List<SearchTaoDate.TaoListBean> getData() {
        return mTaoList;
    }

    //删除单条数据
    public void removeItem(int position) {
//        mTaoList.remove(position);
        notifyItemRemoved(position);
        //局部刷新
        notifyItemRangeChanged(position, getItemCount());
    }

    //添加单条数据
    public void addItem(int position, SearchTaoDate.TaoListBean mTao) {
        mTaoList.add(position, mTao);
        notifyItemInserted(position);
        //局部刷新
        notifyItemRangeChanged(position, mTaoList.size() - position);
    }

    public void updata(List<SearchTaoDate.TaoListBean> mTao) {
        mTaoList = mTao;
        notifyDataSetChanged();
    }


    class viewHolder1 extends RecyclerView.ViewHolder {
        ImageView iv_add;
        TextView tv_add;

        public viewHolder1(View itemView) {
            super(itemView);
            iv_add = itemView.findViewById(R.id.iv_add);
            tv_add = itemView.findViewById(R.id.tv_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

        }
    }

    class viewHolder2 extends RecyclerView.ViewHolder {
        ImageView iv_add;
        TextView tv_add;

        public viewHolder2(View itemView) {
            super(itemView);
            iv_add = itemView.findViewById(R.id.iv_add);
            tv_add = itemView.findViewById(R.id.tv_add);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(v, getLayoutPosition());
                    }
                }
            });

        }
    }

    class viewHolder3 extends RecyclerView.ViewHolder {
        NiceImageView iv_pic;
        TextView tv_sku_title;
        TextView tv_sku_hos;
        ImageView iv_delete;

        public viewHolder3(View itemView) {
            super(itemView);
            iv_pic = itemView.findViewById(R.id.iv_pic);
            tv_sku_title = itemView.findViewById(R.id.tv_sku_title);
            tv_sku_hos = itemView.findViewById(R.id.tv_sku_hos);
            iv_delete = itemView.findViewById(R.id.iv_delete);
        }
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemClick(View view, int pos);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * item删除监听器
     */
    public interface OnItemDeleteClickListener {

        /**
         * 当点击某条的时候回调该方法
         *
         * @param view 被点击的视图
         * @param pos  点击的是哪个
         */
        void onItemDeleteClick(View view, int pos);
    }

    private OnItemDeleteClickListener onItemDeleteClickListener;

    public void setOnItemDeleteClickListener(OnItemDeleteClickListener onItemDeleteClickListener) {
        this.onItemDeleteClickListener = onItemDeleteClickListener;
    }


    private void setView1(viewHolder1 holder, int position) {

    }

    private void setView2(viewHolder2 holder, int position) {

    }

    private void setView3(viewHolder3 holder, final int position) {
        if (TextUtils.isEmpty(mTaoList.get(position).getImg())) {
            Glide.with(mContext).load(R.drawable.sall_null_2x)
                    .into(holder.iv_pic);
//            functionManager.setRoundImageSrc(holder.iv_pic, R.drawable.sall_null_2x, Utils.dip2px(5));
        } else {
            Glide.with(mContext).load(mTaoList.get(position).getImg())
                    .into(holder.iv_pic);
//            functionManager.setRoundImageSrc(holder.iv_pic, mTaoList.get(position).getImg(), Utils.dip2px(5));
        }
        if (TextUtils.isEmpty(mTaoList.get(position).getSubtitle())) {
            holder.tv_sku_title.setText("");
        } else {
            holder.tv_sku_title.setText(mTaoList.get(position).getSubtitle());
        }
        if (TextUtils.isEmpty(mTaoList.get(position).getHos_name())) {
            holder.tv_sku_hos.setText("");
        } else {
            holder.tv_sku_hos.setText(mTaoList.get(position).getHos_name());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(v, position);
                }
            }
        });

        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemDeleteClickListener != null) {
                    onItemDeleteClickListener.onItemDeleteClick(v, position);
                }
            }
        });
    }
}