package com.module.other.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.module.doctor.model.bean.PartAskData;
import com.module.doctor.model.bean.PartAskDataList;
import com.module.other.adapter.ProjectRightListAdapter2.ViewHolder;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * 项目选择pop右边数据
 * Created by 裴成浩 on 2018/2/5.
 */

public class ProjectRightListAdapter2 extends RecyclerView.Adapter<ViewHolder> {

    private LayoutInflater inflater;
    private String mTwoId;
    private final List<PartAskData> mOneDatas;
    private List<PartAskDataList> mData;
    private final int mOnePos;
    private String TAG = "ProjectRightListAdapter2";

    public ProjectRightListAdapter2(Activity mActivity, List<PartAskData> mData, int onePos, String twoId) {
        this.mOnePos = onePos;
        this.mTwoId = twoId;
        this.mOneDatas = mData;
        this.mData = mOneDatas.get(mOnePos).getList();

        inflater = LayoutInflater.from(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_make_right2, parent, false);
        return new ViewHolder(itemView);            //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (!TextUtils.isEmpty(mTwoId) && mTwoId.equals(mData.get(position).get_id())) {
            holder.tvXiangmu.setTextColor(Color.parseColor("#ff5c77"));
        } else {
            holder.tvXiangmu.setTextColor(Color.parseColor("#666666"));
        }
        holder.tvXiangmu.setText(mData.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvXiangmu;

        public ViewHolder(View itemView) {
            super(itemView);
            tvXiangmu = itemView.findViewById(R.id.make_right2_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClickListener != null) {
                        PartAskData oneData = mOneDatas.get(mOnePos);
                        PartAskDataList twoData = mData.get(getLayoutPosition());
                        onItemClickListener.onItemClick(v,oneData ,twoData);
                    }
                }
            });
        }
    }

    public void setmTwoPos(String twoId) {
        this.mTwoId = twoId;
        notifyDataSetChanged();
    }

    /**
     * item的监听器
     */
    public interface OnItemClickListener {

        /**
         * @param view     当点击某条的时候回调该方法
         * @param oneData  :一级标签数据
         * @param twoData: 二级标签数据
         */
        void onItemClick(View view, PartAskData oneData, PartAskDataList twoData);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
