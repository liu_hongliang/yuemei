package com.module.other.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.module.other.adapter.MakeTopAdapter.ViewHolder;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;

import java.util.ArrayList;

/**
 * Created by 裴成浩 on 2017/6/30.
 */

public class MakeTopAdapter extends RecyclerView.Adapter<ViewHolder>{

    private final Context mContext;
    private final ArrayList<MakeTagListListData> topData;

    public MakeTopAdapter(Context mContext, ArrayList<MakeTagListListData> topData) {
        this.mContext = mContext;
        this.topData = topData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_make_top, parent, false);
        return new ViewHolder(itemView) ;        //把这个布局传到ViewHolder中
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvXiangMu.setText(topData.get(position).getName());

        holder.rlTopDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemDeleteListener.onDeleteClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return topData.size();
    }

    private onItemDeleteListener mOnItemDeleteListener;

    public void setOnItemDeleteClickListener(onItemDeleteListener mOnItemDeleteListener) {
        this.mOnItemDeleteListener = mOnItemDeleteListener;
    }

    /**
     * 删除按钮的监听接口
     */
    public interface onItemDeleteListener {
        void onDeleteClick(int i);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvXiangMu;
        public RelativeLayout rlTopDelete;

        public ViewHolder(View itemView) {
            super (itemView);
            tvXiangMu = itemView.findViewById(R.id.tv_xiang_mu);
            rlTopDelete = itemView.findViewById(R.id.rl_top_delete);
        }
    }
}