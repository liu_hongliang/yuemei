package com.module.other.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.module.other.module.bean.IdName;
import com.quicklyask.activity.R;

import java.util.List;

/**
 * Created by dwb on 16/10/11.
 */
public class DiaryCityAdapter extends BaseAdapter {

    private final String TAG = "DiaryCityAdapter";

    private List<IdName> mGroupData;
    private Context mContext;
    private LayoutInflater inflater;
    private IdName groupData;
    ViewHolder viewHolder;
    private int cur;

    public DiaryCityAdapter(Context mContext, List<IdName> mGroupData, int cur) {
        this.mContext = mContext;
        this.mGroupData = mGroupData;
        this.cur = cur;
        inflater = LayoutInflater.from(mContext);
    }

    static class ViewHolder {
        public TextView groupNameTV;
    }

    @Override
    public int getCount() {
        return mGroupData.size();
    }

    @Override
    public Object getItem(int position) {
        return mGroupData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_pop_home_xiala, null);
            viewHolder = new ViewHolder();
            viewHolder.groupNameTV = convertView
                    .findViewById(R.id.home_pop_part_name_tv);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        groupData = mGroupData.get(position);

        viewHolder.groupNameTV.setText(groupData.getName());

        if (cur == position) {
            viewHolder.groupNameTV
                    .setBackgroundResource(R.drawable.shape_tuiyuan_ff5b74);
            viewHolder.groupNameTV.setTextColor(mContext.getResources()
                    .getColor(R.color.red_part));
        } else {
            viewHolder.groupNameTV
                    .setBackgroundResource(R.drawable.shape_tuiyuan_cdcdcd);
            viewHolder.groupNameTV.setTextColor(mContext.getResources()
                    .getColor(R.color._33));
        }

        return convertView;
    }

    public void add(List<IdName> infos) {
        mGroupData.addAll(infos);
    }
}
