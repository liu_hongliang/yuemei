package com.module.other.adapter;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.module.other.activity.MakeNewNoteActivity2;
import com.module.other.module.bean.MakeTagListData;
import com.module.other.module.bean.MakeTagListListData;
import com.quicklyask.activity.R;
import com.quicklyask.view.FlowLayout;
import com.quicklyask.view.MyToast;

import org.xutils.common.util.DensityUtil;

import java.util.List;


/**
 * Created by 裴成浩 on 2017/6/30.
 */

public class RightListAdapter extends BaseAdapter {


    private final MakeNewNoteActivity2 mContext;
    private final List<MakeTagListData> mData;
    private final LayoutInflater inflater;

    public RightListAdapter(MakeNewNoteActivity2 mContext, List<MakeTagListData> mData) {
        this.mContext = mContext;
        this.mData = mData;
        Log.e(mContext.TAG, "mData ==  " + mData.toString());
        Log.e(mContext.TAG, "mData ==  " + mData.size());
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_make_right, null);
            holder.tvRightTitle = convertView.findViewById(R.id.tv_right_title);
            holder.flowlayout = convertView.findViewById(R.id.flowlayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final MakeTagListData beanX = mData.get(pos);
        Log.e(mContext.TAG, "pos == " + pos);
        Log.e(mContext.TAG, "beanX == " + beanX);
        Log.e(mContext.TAG, "beanX.getName() == " + beanX.getName());
        Log.e(mContext.TAG, "holder.tvListWenan == " + holder.tvRightTitle);
        holder.tvRightTitle.setText(beanX.getName());            //设置上边的数据

        //点击
        holder.tvRightTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext.topData.size() < 3) {
                    String id = beanX.getId();
                    boolean isHave = false; //是否是选中的
                    if (!TextUtils.isEmpty(id) && !"0".equals(id)) {
                        for (int i1 = 0; i1 < mContext.topData.size(); i1++) {
                            if (id.equals(mContext.topData.get(i1).getId())) {                              //如果有
                                isHave = true;
                                break;
                            }
                        }

                        if (!isHave) {
                            mContext.topData.add(new MakeTagListListData(id, beanX.getName()));
                            mContext.setTopRecyvlerView();
                        }
                    }
                } else {
                    MyToast.makeTextToast2(mContext, "最多可以选择3个项目", MyToast.SHOW_TIME).show();
                }
            }
        });

        setRightView(holder.flowlayout, beanX.getList(), pos);
        return convertView;
    }

    /**
     * 设置右边下边的标签
     *
     * @param flowlayout
     * @param list
     */
    private void setRightView(FlowLayout flowlayout, List<MakeTagListListData> list, int pos) {
        flowlayout.removeAllViews();

        ViewGroup.MarginLayoutParams lp = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.leftMargin = DensityUtil.dip2px(5);
        lp.rightMargin = DensityUtil.dip2px(5);
        lp.topMargin = DensityUtil.dip2px(5);
        lp.bottomMargin = DensityUtil.dip2px(5);

        boolean isHave = false;

        for (int i = 0; i < list.size(); i++) {
            MakeTagListListData listBean = list.get(i);
            Log.e(mContext.TAG, "data == " + listBean);

            for (int i1 = 0; i1 < mContext.topData.size(); i1++) {
                if (listBean.getId().equals(mContext.topData.get(i1).getId())) {                              //如果有
                    isHave = true;
                    break;
                } else {
                    isHave = false;
                }
            }

            View view = inflater.inflate(R.layout.item_make_right_below, null);
            LinearLayout fxContextYes = view.findViewById(R.id.fx_context_yes);

            TextView fxContextYesYes = view.findViewById(R.id.fx_context_yes_yes);

            TextView fxContextNo = view.findViewById(R.id.fx_context_no);

            if (isHave) {
                fxContextYes.setVisibility(View.VISIBLE);
                fxContextNo.setVisibility(View.GONE);
            } else {
                fxContextYes.setVisibility(View.GONE);
                fxContextNo.setVisibility(View.VISIBLE);
            }

            fxContextYesYes.setText(listBean.getName());
            fxContextNo.setText(listBean.getName());

            flowlayout.addView(view, lp);
            view.setTag(i);
            view.setOnClickListener(new onRightView(pos));
        }
    }


    class onRightView implements View.OnClickListener {

        private final int mPos;
        private MakeTagListListData deleteListBean;

        public onRightView(int pos) {
            this.mPos = pos;
        }

        @Override
        public void onClick(View v) {
            boolean isHave = false; //是否是选中的
            int selected = (int) v.getTag();
            List<MakeTagListListData> list = mData.get(mPos).getList();
            for (int i = 0; i < list.size(); i++) {
                if (i == selected) {
                    LinearLayout fxContextYes = v.findViewById(R.id.fx_context_yes);
                    TextView fxContextNo = v.findViewById(R.id.fx_context_no);

                    MakeTagListListData listBean = list.get(i);
                    for (int i1 = 0; i1 < mContext.topData.size(); i1++) {
                        if (listBean.getId().equals(mContext.topData.get(i1).getId())) {                              //如果有
                            isHave = true;
                            deleteListBean = mContext.topData.get(i1);
                            break;
                        } else {
                            isHave = false;
                        }
                    }
                    Log.e(mContext.TAG, "isHave == " + isHave);
                    Log.e(mContext.TAG, "topData == " + mContext.topData.toString());
                    if (isHave) {
                        fxContextYes.setVisibility(View.GONE);
                        fxContextNo.setVisibility(View.VISIBLE);        //改为未选中
                        mContext.topData.remove(deleteListBean);
                    } else {
                        if (mContext.topData.size() < 3) {
                            fxContextYes.setVisibility(View.VISIBLE);
                            fxContextNo.setVisibility(View.GONE);       //改为选中状态
                            mContext.topData.add(listBean);
                        } else {
                            MyToast.makeTextToast2(mContext, "最多可以选择3个项目", MyToast.SHOW_TIME).show();
                        }
                    }
                    mContext.setTopRecyvlerView();
                }
            }
        }
    }


    class ViewHolder {
        public TextView tvRightTitle;
        public FlowLayout flowlayout;
    }

}
