package com.module.other.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.module.api.TaoPKApi;
import com.module.base.api.BaseCallBackListener;
import com.module.base.refresh.smart.YMLoadMore;
import com.module.base.view.YMBaseFragment;
import com.module.commonview.activity.TaoDetailActivity;
import com.module.community.statistical.statistical.ActivityTypeData;
import com.module.community.statistical.statistical.EventParamData;
import com.module.community.statistical.statistical.FinalEventName;
import com.module.community.statistical.statistical.YmStatistics;
import com.module.event.MsgEvent;
import com.module.other.adapter.ProjectContrastAdapter;
import com.module.other.module.bean.TickData;
import com.module.other.module.bean.TickDataInfo;
import com.module.other.netWork.netWork.ServerData;
import com.quicklyask.activity.R;
import com.quicklyask.entity.TaoPKBean;
import com.quicklyask.util.JSONUtil;
import com.quicklyask.util.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zfdang.multiple_images_selector.YMLinearLayoutManager;

import org.apache.commons.lang.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.kymjs.aframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

import static com.module.other.activity.ProjectContrastActivity.CLEAR_DELETE;
import static com.module.other.activity.ProjectContrastActivity.CLEAR_SELECT;
import static com.module.other.activity.ProjectContrastActivity.DELETE_CUR;
import static com.module.other.activity.ProjectContrastActivity.DELETE_UPDATA_CONTRASTlIST;
import static com.module.other.activity.ProjectContrastActivity.EDIT;
import static com.module.other.activity.ProjectContrastActivity.GET_CONTRAST_DATA;
import static com.module.other.activity.ProjectContrastActivity.GET_SELECT_DATA;
import static com.module.other.activity.ProjectContrastActivity.GET_SELETE_CONTRAST_DATA;
import static com.module.other.activity.ProjectContrastActivity.GET_SELETE_DELETE_DATA;
import static com.module.other.activity.ProjectContrastActivity.MY_CONTRAST_DATA;
import static com.module.other.activity.ProjectContrastActivity.NOTEDIT;
import static com.module.other.activity.ProjectContrastActivity.REFRESH;
import static com.module.other.activity.ProjectContrastActivity.REFRESHCONTRAST;
import static com.module.other.activity.ProjectContrastActivity.TAB_LIST;
import static com.module.other.activity.ProjectContrastActivity.TICK_DATA;
import static com.module.other.activity.ProjectContrastActivity.TOGGLE_TAB;
import static com.module.other.activity.ProjectContrastActivity.isFirstInitTab;
import static com.module.other.activity.ProjectContrastActivity.tabList;

/**
 *
 */
public class ProjectContrastFragment extends YMBaseFragment {
    private String TAG = "ProjectContrastFragment";
    @BindView(R.id.refresh)
    SmartRefreshLayout refresh;
    @BindView(R.id.ll_empty)
    LinearLayout ll_empty;
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.YMLoadMore)
    YMLoadMore ymLoadMore;
    private ProjectContrastAdapter projectContrastAdapter;
    private TaoPKApi taoPKApi;
    private String mType;
    //type跳转进来的 预留
    private String mIndex;
    private String mTaoid;
    private String mTaoSource;
    private TaoPKBean taoPK;
    private List<TaoPKBean.TaoListBean> tao_list;
    private int mDataPage = 1;
    private List<String> selectDatas;
    private List<String> deleteDatas;
    private List<String> selectDatas_contrast;
    private String DeleteFlag = "0";
    private TickDataInfo tickDataInfo;
    private List<TickData> tickDataList;


    public static ProjectContrastFragment newInstance(String type, String tao_id, String tao_source, String index) {
        ProjectContrastFragment fragment = new ProjectContrastFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        bundle.putString("tao_id", tao_id);
        bundle.putString("tao_source", tao_source);
        bundle.putString("index", index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //在ui线程执行
    public void onEventMainThread(MsgEvent msgEvent) {
        switch (msgEvent.getCode()) {
            case REFRESHCONTRAST:
                mTaoid = ((ArrayList<String>) msgEvent.getData()).get(0);
//                if (mType.equals("0")) {
//                    Log.i("302", "传递的" + mTaoid);
//                }
                mTaoSource = ((ArrayList<String>) msgEvent.getData()).get(1);
                if (!TextUtils.isEmpty(mTaoid)) {
                    List<String> result = Arrays.asList(mTaoid.split(","));
                    for (int i = 0; i < result.size(); i++) {
                        if (!selectDatas_contrast.contains(result.get(i).trim())) {
                            selectDatas_contrast.add(result.get(i).trim());
                        }
                    }
                }
                if (mType.equals("0")) {
                    EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
                }
                reshData();
                break;
            case REFRESH:
                reshData();
                break;
            case EDIT:
                DeleteFlag = "1";
                deleteDatas = new ArrayList<>();
                projectContrastAdapter.initDeleteList(deleteDatas, DeleteFlag);
                break;
            case NOTEDIT:
                DeleteFlag = "0";
                deleteDatas.clear();
                projectContrastAdapter.initDeleteList(deleteDatas, DeleteFlag);
                break;
            case TOGGLE_TAB:
//                selectDatas_contrast.clear();
                selectDatas.clear();
                projectContrastAdapter.upDataList(selectDatas_contrast, selectDatas);
                break;
            case CLEAR_SELECT:
                selectDatas.clear();
                projectContrastAdapter.upDataList(selectDatas_contrast, selectDatas);
                break;
            case CLEAR_DELETE:
                deleteDatas.clear();
                projectContrastAdapter.upDataList(selectDatas_contrast, selectDatas);
                break;
            case DELETE_CUR:
                if (mType.equals("0")) {
                    selectDatas_contrast.remove((String) msgEvent.getData());
                    EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
                    reshData();
                }
                break;
            case DELETE_UPDATA_CONTRASTlIST:
                if (mType.equals("0")) {
                    selectDatas_contrast = (List<String>) msgEvent.getData();
                    EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
                    reshData();
                }
                break;
            case TICK_DATA:
                if (mType.equals("0")) {
                    TickDataInfo t = (TickDataInfo) msgEvent.getData();
//                    Log.i("302", tickDataInfo + "");
                    if (tickDataInfo != null && tickDataInfo.getData() != null && tickDataInfo.getData().size() > 0 && t.getData() != null) {
                        tickDataInfo.getData().addAll(t.getData());
                    }
                    if (tickDataInfo.getData().size() > 5) {
                        tickDataInfo.setData(tickDataInfo.getData().subList(0, tickDataInfo.getData().size()));
                    }
                    reshData();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getArguments() != null) {
            mType = getArguments().getString("type");
            mTaoid = getArguments().getString("tao_id");
            mTaoSource = getArguments().getString("tao_source");
            mIndex = getArguments().getString("index");
        }
        selectDatas = new ArrayList<>();
        selectDatas_contrast = new ArrayList<>();
        //可能type值跳转进来的
        if (!TextUtils.isEmpty(mIndex)) {
            mType = mIndex;
        }
        if (mType.equals("0")) {
            tickDataInfo = new TickDataInfo();
            tickDataList = new ArrayList<TickData>();
            if (!TextUtils.isEmpty(mTaoid)) {
                List<String> result = Arrays.asList(mTaoid.split(","));
                for (int i = 0; i < result.size(); i++) {
                    if (!selectDatas_contrast.contains(result.get(i).trim())) {
                        selectDatas_contrast.add(result.get(i).trim());
                    }
                }
            }
            EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
        }
    }


    /**
     * 判断是否有网络
     */
    private void isCheckNet() {
        //有网络
        if (SystemTool.checkNet(mContext)) {
            loadData();
        } else {
            Toast.makeText(mContext, "请检查网络", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData() {
        taoPKApi = new TaoPKApi();
        taoPKApi.getHashMap().clear();
        taoPKApi.addData("type", Integer.parseInt(mType) + 1 + "");
        if (selectDatas_contrast != null && !selectDatas_contrast.isEmpty() && DeleteFlag.equals("0") && mType.equals("0")) {
            if (tickDataInfo != null && selectDatas_contrast.size() > 5) {
                taoPKApi.addData("tao_id", StringUtils.strip(selectDatas_contrast.subList(0, 5).toString().trim(), "[]"));
//                Log.i("302", "请求的" + StringUtils.strip(selectDatas_contrast.subList(0, 5).toString().trim(), "[]"));
            } else {
                taoPKApi.addData("tao_id", StringUtils.strip(selectDatas_contrast.toString().trim(), "[]"));
//                Log.i("302", "请求的" + StringUtils.strip(selectDatas_contrast.toString().trim(), "[]"));
            }
        }
        taoPKApi.addData("tao_source", mTaoSource);
        taoPKApi.addData("page", mDataPage + "");
        if (mType.equals("0") && tickDataInfo.getData() != null && tickDataList.size() > 0) {
            taoPKApi.addData("tick_data", new Gson().toJson(tickDataInfo.getData()));
        }

        taoPKApi.getCallBack(mContext, taoPKApi.getHashMap(), new BaseCallBackListener<ServerData>() {
            @Override
            public void onSuccess(ServerData serverData) {
                if (serverData != null && "1".equals(serverData.code)) {
                    if (getActivity() != null) {
                        taoPK = JSONUtil.TransformSingleBean(serverData.data, TaoPKBean.class);
                        tao_list = taoPK.getTao_list();
                        if (null != refresh) {
                            refresh.finishRefresh();
                        }
                        if (tao_list.size() == 0) {
                            refresh.finishLoadMoreWithNoMoreData();
                        } else {
                            refresh.finishLoadMore();
                        }
                        if (projectContrastAdapter == null) {
                            sortTab(tao_list);
                            if (mType.equals("0")) {
                                tickDataList.clear();
                                EventBus.getDefault().post(new MsgEvent(MY_CONTRAST_DATA, tao_list));
                                for (int i = 0; i < tao_list.size(); i++) {
                                    if (tao_list.get(i).getTao_pk_select() != null && tao_list.get(i).getTao_pk_select().equals("1")) {
                                        setTickData(tao_list.get(i).getId().trim(), tao_list.get(i).getTick_time().trim());
                                    }
                                }
                                //可能购物车数据大于5个
                                if (selectDatas_contrast != null && selectDatas_contrast.size() > 5) {
                                    selectDatas_contrast.clear();
                                    for (int i = 0; i < tao_list.size(); i++) {
                                        if (tao_list.get(i).getTao_pk_select().equals("1")) {
                                            if (!selectDatas_contrast.contains(tao_list.get(i).getId().trim())) {
                                                selectDatas_contrast.add(tao_list.get(i).getId().trim());
                                            }
                                        }
                                    }
                                    EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
//                                    Log.i("302", "服务器返回" + StringUtils.strip(selectDatas_contrast.toString().trim(), "[]"));
                                }
                            }
                            if (tao_list.size() != 0) {
                                rv.setVisibility(View.VISIBLE);
                                ll_empty.setVisibility(View.GONE);
                            } else {
                                rv.setVisibility(View.GONE);
                                ll_empty.setVisibility(View.VISIBLE);
                                ymLoadMore.setVisibility(View.GONE);
                                if (mType.equals("0")) {
                                    EventBus.getDefault().post(new MsgEvent(NOTEDIT));
                                }
                            }
                        }
                        setAdapter();
                    }
                } else {
                    Toast.makeText(mContext, serverData.message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //为了判断哪个tab列表下有数据
    private void sortTab(List<TaoPKBean.TaoListBean> tao_list) {
        if (isFirstInitTab) {
            if (mType.equals("0")) {
                //我的对比
                tabList.set(0, tao_list.size() == 0 ? "0" : "1");
            } else if (mType.equals("1")) {
                //浏览历史
                tabList.set(1, tao_list.size() == 0 ? "0" : "1");
            } else if (mType.equals("2")) {
                //购物车
                tabList.set(2, tao_list.size() == 0 ? "0" : "1");
            } else {
                //我的收藏
                tabList.set(3, tao_list.size() == 0 ? "0" : "1");
            }
            if (!tabList.contains("3")) {
                EventBus.getDefault().post(new MsgEvent(TAB_LIST, tabList));
                isFirstInitTab = false;
            }
        }
    }

    /**
     * 刷新数据
     */
    private void reshData() {
        mDataPage = 1;
        projectContrastAdapter = null;
        isCheckNet();
    }

    private void setAdapter() {
        if (projectContrastAdapter == null) {
            projectContrastAdapter = new ProjectContrastAdapter(mContext, tao_list, selectDatas, selectDatas_contrast, mType, DeleteFlag, deleteDatas);
            YMLinearLayoutManager linearLayoutManager = new YMLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
            rv.setLayoutManager(linearLayoutManager);
            rv.setAdapter(projectContrastAdapter);
        } else {
            //1可以继续加载 0没有更多了
            if (taoPK.getIs_has_more().equals("1")) {
                projectContrastAdapter.addList(tao_list, selectDatas);
            }
        }


        //item点击回调
        projectContrastAdapter.setOnItemClickListener(new ProjectContrastAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int pos) {
                if (Utils.isFastDoubleClick()) {
                    return;
                }
                if (!TextUtils.isEmpty(projectContrastAdapter.getmDatas().get(pos).getId()) && mType.equals("0")) {
                    //埋点  我的对比页{tao_list.event_params}  我的对比页-淘整形详情
                    HashMap event_params = new HashMap();
                    event_params.put("to_page_type", "2");
                    event_params.put("to_page_id", projectContrastAdapter.getmDatas().get(pos).getId());
                    YmStatistics.getInstance().tongjiApp(new EventParamData(FinalEventName.TAO_PK_SELECT_TO_TAO, (pos + 1) + ""), event_params, new ActivityTypeData("167"));
                }
                if (!TextUtils.isEmpty(projectContrastAdapter.getmDatas().get(pos).getId())) {
                    Intent intent = new Intent(mContext, TaoDetailActivity.class);
                    intent.putExtra("id", projectContrastAdapter.getmDatas().get(pos).getId());
                    intent.putExtra("source", "0");
                    intent.putExtra("objid", "0");
                    mContext.startActivity(intent);
                }
            }
        });

        //选中点击回调
        projectContrastAdapter.setOnItemCheckClickListener(new ProjectContrastAdapter.OnItemCheckClickListener() {
            @Override
            public void onItemCheckClick(View view, int pos) {
                //删除模式
                if (DeleteFlag.equals("1") && mType.equals("0")) {
                    if (!TextUtils.isEmpty(projectContrastAdapter.getmDatas().get(pos).getPk_id() + "".trim())) {
                        if (!deleteDatas.contains(projectContrastAdapter.getmDatas().get(pos).getPk_id() + "".trim())) {
                            deleteDatas.add(projectContrastAdapter.getmDatas().get(pos).getPk_id() + "".trim());
                        } else {
                            deleteDatas.remove(projectContrastAdapter.getmDatas().get(pos).getPk_id() + "".trim());
                        }
                        EventBus.getDefault().post(new MsgEvent(GET_SELETE_DELETE_DATA, deleteDatas));
                        EventBus.getDefault().post(new MsgEvent(GET_CONTRAST_DATA, projectContrastAdapter.getmDatas()));
                    }
                } else {
                    //对比
                    if (mType.equals("0")) {
                        if (!TextUtils.isEmpty(projectContrastAdapter.getmDatas().get(pos).getId().trim())) {
                            if (!selectDatas_contrast.contains(projectContrastAdapter.getmDatas().get(pos).getId().trim())) {
                                if (selectDatas_contrast != null && selectDatas_contrast.size() < 5) {
                                    selectDatas_contrast.add(projectContrastAdapter.getmDatas().get(pos).getId().trim());
                                    setTickData(projectContrastAdapter.getmDatas().get(pos).getId().trim());
                                } else {
                                    Toast.makeText(mContext, "最多只能选择5个哦", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                removeTickData(pos);
                                selectDatas_contrast.remove(projectContrastAdapter.getmDatas().get(pos).getId().trim());
                            }
                            EventBus.getDefault().post(new MsgEvent(GET_SELETE_CONTRAST_DATA, selectDatas_contrast));
                        }
                    } else {
                        //后三个页面
                        if (!TextUtils.isEmpty(projectContrastAdapter.getmDatas().get(pos).getId().trim())) {
                            if (!selectDatas.contains(projectContrastAdapter.getmDatas().get(pos).getId().trim())) {
                                selectDatas.add(projectContrastAdapter.getmDatas().get(pos).getId().trim());
                            } else {
                                selectDatas.remove(projectContrastAdapter.getmDatas().get(pos).getId().trim());
                            }
                            EventBus.getDefault().post(new MsgEvent(GET_SELECT_DATA, selectDatas));
                        }
                    }
                }
                projectContrastAdapter.notifyItemChanged(pos, "select");
            }
        });
    }


    private void setTickData(String tao_id) {
        if (mType.equals("0")) {
            TickData tickData = new TickData();
            tickData.setTao_id(tao_id);
            tickData.setTick_time(String.format("%010d", (System.currentTimeMillis() / 1000)));
            tickDataList.add(tickData);
            tickDataInfo.setData(tickDataList);
        }
    }

    private void setTickData(String tao_id, String time) {
        if (mType.equals("0")) {
            TickData tickData = new TickData();
            tickData.setTao_id(tao_id);
            tickData.setTick_time(time);
            tickDataList.add(tickData);
            tickDataInfo.setData(tickDataList);
        }
    }

    private void removeTickData(int pos) {
        if (tickDataList != null && tickDataList.size() > 0) {
            for (int i = 0; i < tickDataList.size(); i++) {
                if (projectContrastAdapter.getmDatas().get(pos).getId().trim().equals(tickDataList.get(i).getTao_id())) {
                    tickDataList.remove(i);
                }
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_project_contrast;
    }

    @Override
    protected void initView(View view) {

        setMultiOnClickListener(ll_empty);
//        //加载更多和刷新
//        refresh.setEnableFooterFollowWhenLoadFinished(true);
        //下拉刷新
        refresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                reshData();
            }
        });

        //上拉加载更多
        refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mDataPage++;
                isCheckNet();
            }
        });
    }

    @Override
    protected void initData(View view) {
        reshData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_empty:
                isCheckNet();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getFocus();
    }

    private void getFocus() {
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
                    // 监听到返回按钮点击事件
                    ProjectContrastFragment.this.getActivity().finish();
                    return true;// 未处理
                }
                return false;
            }
        });
    }
}
