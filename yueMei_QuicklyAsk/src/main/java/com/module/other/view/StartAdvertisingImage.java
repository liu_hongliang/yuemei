package com.module.other.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.quicklyask.activity.R;

/**
 * 首页启动广告图
 *
 * @author 裴成浩
 * @data 2019/10/24
 */
public class StartAdvertisingImage extends View {

    private Bitmap mBackgroundBitmap;
    private Bitmap mSrcBitmap;
    private Paint mPaint;

    public StartAdvertisingImage(Context context) {
        this(context, null);
    }

    public StartAdvertisingImage(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StartAdvertisingImage(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        mPaint = new Paint();
        mBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.yuemei_log);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mSrcBitmap != null) {
            canvas.drawBitmap(mBackgroundBitmap,0,0,mPaint);
            canvas.drawBitmap(mSrcBitmap,0,0,mPaint);
        }
    }

    public void setSrcBitmap(){
        mBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.yuemei_log);
        invalidate();
    }
}
